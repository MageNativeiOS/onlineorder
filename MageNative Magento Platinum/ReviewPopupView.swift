/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */

import UIKit

class ReviewPopupView: UIView {

    // Our custom view from the XIB file
    var view: UIView!
    
    //outlets
    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var numOfReviews: UILabel!
    @IBOutlet weak var ratingPoints: UILabel!
    @IBOutlet weak var ratingStarImage: UIImageView!
    @IBOutlet weak var writeReviewButton: UIButton!
    
    
    override init(frame: CGRect) {
        // 1. setup any properties here
        
        // 2. call super.init(frame:)
        super.init(frame: frame)
        
        // 3. Setup view from .xib file
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        // 1. setup any properties here
        
        // 2. call super.init(coder:)
        super.init(coder: aDecoder)
        
        // 3. Setup view from .xib file
        xibSetup()
    }
    
    func xibSetup() {
        view = loadViewFromNib()
        
        // use bounds not frame or it'll be offset
        view.frame = bounds
        
        // Make the view stretch with containing view
        view.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        topLabel.fontColorTool()
        numOfReviews.fontColorTool()
        ratingPoints.fontColorTool()
        ratingStarImage.fontColorTool()
        writeReviewButton.setTitleColor(Settings.themeTextColor, for: .normal)
        
        //extra setup
        topLabel.text = "Review Status".localized
        topLabel.font = UIFont(fontName: "HelveticaNeue-Medium", fontSize: CGFloat(15.0));
        numOfReviews.font = UIFont(fontName: "", fontSize: CGFloat(15.0));
        ratingPoints.font = UIFont(fontName: "", fontSize: CGFloat(15.0));
        writeReviewButton.setTitle("View & Write Review", for: UIControl.State.normal);
        writeReviewButton.titleLabel!.font = UIFont(fontName: "HelveticaNeue-Medium", fontSize: CGFloat(15.0));
        writeReviewButton.setThemeColor();
//        self.makeCard(self, cornerRadius: 2, color: UIColor.black, shadowOpacity: 0.4);
        //extra setup
        
        
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "ReviewPopupView", bundle: bundle)
        
        // Assumes UIView is top level and only object in CustomView.xib file
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
   
}
