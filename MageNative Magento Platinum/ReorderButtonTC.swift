//
//  ReorderButtonTC.swift
//  MageNative Magento Platinum
//
//  Created by Hamza Usmani on 04/09/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

protocol OrderViewDelegate: class {
    func handleReorder()
}

class ReorderButtonTC: UITableViewCell {
    
    //MARK:- Properties
    
    static var reuseID:String = "ReorderButtonTC"
    weak var delegate: OrderViewDelegate?
    
    lazy var reorderButton:UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Reorder Product(s)", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 16, weight: .semibold)
        button.backgroundColor = Settings.themeColor
        button.translatesAutoresizingMaskIntoConstraints = false
        button.layer.cornerRadius = 5.0
        button.addTarget(self, action: #selector(handleReorderTap), for: .touchUpInside)
        return button
    }()
    
    //MARK:- Lifecycle
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        addSubview(reorderButton)
        reorderButton.anchor(top: topAnchor, left: leadingAnchor, right: trailingAnchor, paddingTop: 16, paddingLeft: 32 , paddingRight: 32, height: 35)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK:- Selectors
    
    @objc private func handleReorderTap() {
        delegate?.handleReorder()
    }
    
}
