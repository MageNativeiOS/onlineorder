//
//  vendorComposeMsgController.swift
//  MageNative Magento Platinum
//
//  Created by cedcoss on 12/02/21.
//  Copyright © 2021 CEDCOSS Technologies Private Limited. All rights reserved.
//

import Foundation

import UIKit

class vendorComposeMsgController: MagenativeUIViewController {
    @IBOutlet weak var vendorHeaderLabel: UILabel!
    @IBOutlet weak var subjectTextField: UITextField!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var addBtn: UIButton!
    var currentTagForFileUpload = Int()
    @IBOutlet weak var uncheckedButton: UIButton!
    @IBOutlet weak var sendButton: UIButton!
    var imageNameToSend = String()
    var isChecked = true
    var  checked = 0
    var convertString = ""
    var picker = UIImagePickerController()
    var isFromAdmin = false
    var imagePicked=String()
    var imagViewArray = [String:UIImageView]()
    @IBOutlet weak var vendorNameButton: UIButton!
    @IBOutlet weak var imageNameLabel: UILabel!
    @IBOutlet weak var vendorButtonHeight: NSLayoutConstraint!
    var convertedFileString = ""
    var vendorName = [vendorNameList]()
    var attachedData = [[String : String]]()
    var selectedVendorEmail = String()
    var selectedVendorId = String()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.imageNameLabel.isHidden = true
        if !isFromAdmin{
            sendRequestForVendorNames()
        }
        
        setupViewController()
    }
    
    @IBAction func toggleButtonAction(_ sender: Any) {
       isChecked = !isChecked
        if isChecked{
            uncheckedButton.setImage(UIImage(named: "UncheckedCheckbox"), for: .normal)
            checked = 0
        }
        else{
             uncheckedButton.setImage(UIImage(named: "CheckedCheckbox"), for: .normal)
            checked = 1
        }
        uncheckedButton.tintColor = themecolor
    }
    
    func sendRequestForVendorNames() {
        self.getRequest(url: "mobimessaging/vendornames/", store: false)
    }
    
    override func recieveResponse(data: Data?, requestUrl: String?, response: URLResponse?) {
        guard let data = data else{return}
        guard var json = try? JSON(data: data) else {return}
        print(json)
        
        if requestUrl == "mobimessaging/compose"        // for send data through api
        {
            if json[0]["response"]["status"].stringValue == "true"{
                self.view.makeToast(json[0]["response"]["message"].stringValue, duration: 1.0, position: .center)
                self.vendorNameButton.setTitle("--Select--", for: .normal)
                self.vendorNameButton.backgroundColor = themecolor
                self.descriptionTextView.text = ""
                self.subjectTextField.text = ""
                self.attachedData.removeAll()
            }
            print("send")
        }
            
        else if requestUrl == "mobimessaging/admincompose"
        {
            if json[0]["response"]["status"].stringValue == "true"{
                self.view.makeToast(json[0]["response"]["message"].stringValue, duration: 1.0, position: .center)
                self.descriptionTextView.text = ""
                self.subjectTextField.text = ""
                self.attachedData.removeAll()
            }
            print("admin Send")
        }
        else{
            for list in json[0]["response"]["vendors"].arrayValue{        // for vendor name dropdown
                let temp = vendorNameList(name: list["name"].stringValue,
                                          email: list["email"].stringValue,
                                          id: list["id"].stringValue)
                vendorName.append(temp)
                
            }
            print(vendorName)
        }
    }
    
    func setupViewController() {
        descriptionTextView.cardView()
//        uncheckedButton.layer.borderWidth = 0.5
//        uncheckedButton.layer.cornerRadius = 5
        addBtn.layer.cornerRadius = 5
        sendButton.layer.cornerRadius = 5
        sendButton.setThemeColor()
        vendorNameButton.layer.cornerRadius = 5
        vendorNameButton.setThemeColor()
        addBtn.setThemeColor()
        addBtn.addTarget(self, action: #selector(addButtonTapped(_:)), for: .touchUpInside)
        vendorNameButton.addTarget(self, action: #selector(dropdownTapped(_:)), for: .touchUpInside)
        sendButton.addTarget(self, action: #selector(sendVendorInboxData(_:)), for: .touchUpInside)
        if isFromAdmin{
            vendorHeaderLabel.text = "Admin Compose"
            vendorNameButton.isHidden = true
            vendorButtonHeight.constant = 0
        }
        else{
            vendorHeaderLabel.text = "Vendor Compose"
            vendorNameButton.isHidden = false
            vendorButtonHeight.constant = 40
        }
        
        
        
    }
    @objc func sendVendorInboxData(_ sender: UIButton){
        
        let name = self.vendorNameButton.currentTitle!
        let subject = self.subjectTextField.text!
        let message = self.descriptionTextView.text!
        print(subject)
        print(message)
     //   let image = self.addBtn.text!
        if   subject != "" && message != ""
        {
            guard let userinfo = UserDefaults.standard.value(forKey: "userInfoDict") as? [String:String] else {print("user nil");return}
            guard let customerId = userinfo["customerId"] else {print("empty customer");return}
            if isFromAdmin {
                self.sendRequest(url: "mobimessaging/admincompose", params: ["message":message,"email":selectedVendorEmail,"subject":subject,"send_email":"\(checked)","customer_id":customerId,"store_id":"1","image":"[\(convertedFileString)]"])
            } else {
                if name != "--Select--"{
                    self.sendRequest(url: "mobimessaging/compose", params: ["message":message,"email":selectedVendorEmail,"subject":subject,"send_email":"\(checked)","customer_id":customerId,"store_id":"1","vendor_id":selectedVendorId,"image":"[\(convertedFileString)]"])  //
                }
            }
           
        }
        else{
            self.view.makeToast("Enter all data!", duration: 1.0, position: .center)
        }
        
        
        
        
    }
           
    @objc func dropdownTapped(_ sender: UIButton) {

    var vendorNames = [String]()
    var vendorEmails = [String]()
        var vendorId = [String]()
    for name in vendorName {
        vendorNames.append(name.name)
        vendorEmails.append(name.email)
        vendorId.append(name.id)
    }
       
    dropDown.dataSource = vendorNames;
    dropDown.selectionAction = {(index, item) in
    sender.setTitle(item, for: .normal)
    self.selectedVendorEmail = vendorEmails[index]
        self.selectedVendorId = vendorId[index]
    }
    dropDown.anchorView = sender
    dropDown.bottomOffset = CGPoint(x: 0, y:sender.bounds.height)

    if dropDown.isHidden {
    _=dropDown.show();
    } else {
    dropDown.hide();
    }

    }
    
    @objc func addButtonTapped(_ sender: UIButton) {
        addBtn.tag = 777
    let actionSheetControllerIOS8: UIAlertController = UIAlertController(title: "Please select", message: nil, preferredStyle: .actionSheet)

    let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { _ in
    print("Cancel")
    }
    actionSheetControllerIOS8.addAction(cancelActionButton)

    let saveActionButton = UIAlertAction(title: "Upload Image", style: .default)
    { _ in
    print("upload image")
        self.image_picker(_:sender)
    }
    actionSheetControllerIOS8.addAction(saveActionButton)

    let uploadpdf = UIAlertAction(title: "Upload Document", style: .default)
    { _ in
    print("Delete")
    self.browseButtonPressed()
    }
    actionSheetControllerIOS8.addAction(uploadpdf)
    self.present(actionSheetControllerIOS8, animated: true, completion: nil)
    }
    
    func image_picker(_ sender : UIButton){
    print("browseButtonPressed")
        print(sender.tag)
        currentTagForFileUpload = sender.tag
    picker.delegate=self
    picker.allowsEditing = false
    picker.sourceType = .photoLibrary
    self.present(picker, animated: true, completion: nil)
    }
    func browseButtonPressed(){
        
        let doc = UIDocumentPickerViewController(documentTypes: ["public.text","public.content","public.image","public.data"], in: .import)
        doc.delegate=self
        doc.modalPresentationStyle = .popover
        self.present(doc, animated: true, completion: nil)
    }
    

}
extension vendorComposeMsgController:UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIDocumentPickerDelegate {
    
    
//   UIDocumentMenuDelegate
    

//func documentMenu(_ documentMenu: UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
//documentPicker.delegate=self
//documentPicker.modalPresentationStyle = .fullScreen
//self.present(documentPicker, animated: true, completion: nil)
//}
    
    
    
    
    
func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {

let fileName=url.absoluteString.components(separatedBy: "/")
// docUrlLabel.text=url.absoluteString
print("didPickDocumentAt")
// FileSelectedOrNot.text = fileName[0]

let isSecuredURL = url.startAccessingSecurityScopedResource() == true
let coordinator = NSFileCoordinator()
var error: NSError? = nil
coordinator.coordinate(readingItemAt: url, options: [], error: &error) { (url) -> Void in
do{
let temp=try Data(contentsOf: url)
//self.trainingDocData=temp.base64EncodedString()

let temp1=fileName[fileName.count-1].components(separatedBy: ".")
print("The file you have choosed is not in correct format")
// print(temp1)
if (temp1[1] == "pdf" || temp1[1] == "zip" )
{
print(temp1)
let dataPdf = temp.base64EncodedString()
self.attachedData.append(["name":"xyz.pdf", "base64_encoded_data": dataPdf])
}
else
{
let msg = "The file you have choosed is not in correct format"
print(msg)
return

}
}catch let error{

print(error.localizedDescription)

}
}
if (isSecuredURL) {
url.stopAccessingSecurityScopedResource()
}
print(url)
controller.dismiss(animated: true, completion: nil)
}
func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
   // print(info)
//    if let image = info[UIImagePickerController.InfoKey.originalImage.rawValue] as? UIImage {
//
//
//        let imgData=image.pngData()
//let imageData=(imgData?.base64EncodedString())!
//        let values = info[UIImagePickerController.InfoKey.referenceURL.rawValue] as? NSURL
//let imgName = values?.lastPathComponent

/*let image = info[UIImagePickerControllerOriginalImage] as? UIImage
let imgsize:NSData = UIImagePNGRepresentation(image!)! as NSData
let fileSize = Double(imgsize.length / 1024 / 1024)
totalSize += fileSize
if totalSize > 5.00 {
picker.dismiss(animated: true, completion: nil)
self.view.makeToast("Please upload a file of less than 5 Mb", duration: 2.0, position: .center)
}else{ */

//attachedData.append(["name":imgName ?? "xyz.png", "base64_encoded_data": imageData])
//        print(attachedData)
//picker.dismiss(animated: true, completion: nil)
    //}
    
    let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)
    print(info)
    self.imageNameLabel.isHidden = false
    self.imageNameLabel.text = "Image Added"
    self.imageNameToSend = self.imageNameLabel.text!
    let image = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage
    if let tempData=(image ?? UIImage()).jpegData(compressionQuality: 0.5)
    {
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy hh:mm a"
        var selectedDate: String = dateFormatter.string(from: Date())
        selectedDate = selectedDate.replacingOccurrences(of: " ", with: "")
        let chat_file=(tempData.base64EncodedString())
        let dictionary = ["name":"124\(selectedDate).jpg","base64_encoded_data":chat_file]//,"type":"file"
        self.convertedFileString=convertDicTostring(str: dictionary)
        print(convertedFileString)

    }
    //self.imageNameLabel.text = info
    //Showing image in imageview after image is selected in ImagePickerController
  //  if let img = self.view.viewWithTag(currentTagForFileUpload) as? UIImageView
//    {
//        img.image = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage
//
//        if let tempData=(img.image ?? UIImage()).jpegData(compressionQuality: 0.5)
//        {
//            let dateFormatter: DateFormatter = DateFormatter()
//            dateFormatter.dateFormat = "MM/dd/yyyy hh:mm a"
//            var selectedDate: String = dateFormatter.string(from: Date())
//            selectedDate = selectedDate.replacingOccurrences(of: " ", with: "")
//            let chat_file=(tempData.base64EncodedString())
//            let dictionary = ["name":"124\(selectedDate).jpg","base64_encoded_data":chat_file,"type":"image"]
//            self.convertString=convertDicTostring(str: dictionary)
//            print(convertString)
//
//        }
//        imagViewArray.updateValue(img, forKey: "image_document")
//        print(imagViewArray["name"])
//    }
    self.dismiss(animated: true, completion: nil);

}
 
func documentMenuWasCancelled(_ documentMenu: UIDocumentMenuViewController) {
documentMenu.dismiss(animated: true, completion: nil)
}
func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
controller.dismiss(animated: true, completion: nil)
}
func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
picker.dismiss(animated: true, completion: nil)
}

}
   
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
            return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
        }

fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
    return input.rawValue
}


extension UIViewController
{
    @objc func convertDicTostring(str:Dictionary<String, String>) -> String
    {
        let dictionary = str
        let jsonData = try? JSONSerialization.data(withJSONObject: dictionary, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)
        return jsonString!
    }
}
