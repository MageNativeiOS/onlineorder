//
//  SelectStoreViewController.swift
//  MageNative Magento Platinum
//
//  Created by cedcoss on 10/16/17.
//  Copyright © 2017 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class SelectStoreViewController: MagenativeUIViewController {
    
    @IBOutlet weak var superView: UIView!
    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var lable: UILabel!
    @IBOutlet weak var stackView1: UIStackView!
    @IBOutlet weak var stackViewHeight: NSLayoutConstraint!
    var Stores = [[String:String]]()
    override func viewDidLoad() {
        super.viewDidLoad()
        lable.setThemeColor()
        lable.fontColorTool()
        topLabel.setThemeColor()
        topLabel.fontColorTool()
        superView.setThemeColor()
        stackViewHeight.constant=0
        self.sendRequest(url:  "mobiconnectstore/getlist", params: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("123456789")
    }
    
    
    override func recieveResponse(data:Data?,requestUrl:String?,response:URLResponse?){
        
        
        
        
        
        if (requestUrl == "mobiconnectstore/getlist") {
            self.Stores.removeAll()
            guard let json = try? JSON(data: data!) else{return}
            print("^*^%^&%^&%&^%^&\(json)")
            if json[0]["store_data"].arrayValue.count == 1{
                let store_id = json[0]["store_data"][0]["store_id"].stringValue
                UserDefaults.standard.setValue(store_id, forKey: "storeId")
                selectStore(store: store_id)
            }else{
                for store in  json[0]["store_data"].arrayValue{
                    let group_id = store["group_id"].stringValue
                    let code = store["code"].stringValue
                    let store_id = store["store_id"].stringValue
                    let name = store["name"].stringValue
                    let is_active = store["is_active"].stringValue
                    let storeobject = ["group_id":group_id,"code":code,"store_id":store_id,"name":name,"is_active":is_active]
                    self.Stores.append(storeobject)
                }
                self.showStores()
            }
        }
        else
        {
            if let data = data{
                guard let json = try? JSON(data: data) else{return;}
                print(json)
                if json[0]["success"] == true{
                
                    let view = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "mainView") as? cedMageSideDrawer
                    self.view.window?.rootViewController = view
                    //                    self.present(view!, animated: true, completion: nil)
                    
                }
            }
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.view.window?.rootViewController?.dismiss(animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func showStores(){
        stackView1.superview?.backgroundColor=UIColor.black
        stackView1.isHidden = false
        stackView1.superview?.layer.borderWidth=1
        stackView1.superview?.layer.borderColor=UIColor.lightGray.cgColor
        for i in 0..<self.Stores.count {
            let button=UIButton(frame: CGRect(x: 0, y: 0, width: stackView1.frame.width, height: 40))
            button.setTitle(self.Stores[i]["name"], for: .normal)
            //            button.setTitleColor(UIColor.black, for: .normal)
            button.setThemeColor()
            button.fontColorTool()
            button.tag=i
            button.addTarget(self, action: #selector(storeSelected(_:)), for: .touchUpInside)
            stackView.distribution = .equalSpacing
            stackView.spacing=5
            stackViewHeight.constant+=45
            stackView.addArrangedSubview(button)
        }
    }
    
    @objc func storeSelected(_ sender: UIButton){
        UserDefaults.standard.setValue(self.Stores[sender.tag]["store_id"], forKey: "storeId")
        self.selectStore(store: self.Stores[sender.tag]["store_id"])
    }
    
    //
    func selectStore(store:String?){
        self.sendRequest(url: "mobiconnectstore/setstore/"+store!, params: nil)
    }
    
}
