/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */


import UIKit

class CheckoutStep4ViewController: MagenativeUIViewController {
    
    
    @IBOutlet weak var imageViewOnTop: UIImageView!
    
    @IBOutlet weak var imageViewOnTopHeight: NSLayoutConstraint!
    
    var checkoutAs = "USER";
    var email_id = "test@test.com";
    
    var shippingMethods = [String: String]();
    //var paymentMethods = [String: String]();
    var payment_method = "";
    var shipping_method = "";
    var cart_id = "0";
    
    //Native Payments
    private var nativePayments = ["RazorPay":"RazorPay"]
    
    //Order data
    var totalOrderData = [String:String]()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        if(defaults.object(forKey: "cartId") != nil){
            cart_id = (defaults.object(forKey: "cartId") as? String)!;
        }else{
            cart_id = "0"
        }
        if(defaults.object(forKey: "cartId") != nil){
            cart_id = (defaults.object(forKey: "cartId") as? String)!;
            self.sendRequest(url:"mobiconnect/checkout/viewcart",params: ["cart_id":cart_id]);
        }
        else{
            self.sendRequest(url:"mobiconnect/checkout/viewcart",params: ["cart_id":cart_id]);
        }
        self.basicFoundationToRenderView(bottomMargin: CGFloat(0.0));
        self.makeRequestToAPI("mobiconnect/checkout/getshippingpayament",dataToPost: ["email":email_id, "Role":checkoutAs, "cart_id":cart_id]);
        
    }
    
    
    
    override func parseResponseReceivedFromAPI(_ jsonResponse:JSON){
        print("PAYMENT");
        print(jsonResponse);
        let jsonResponse = jsonResponse[0]
        
        
        if(jsonResponse["success"].stringValue == "true")
        {
            
            let shipMethods = jsonResponse["shipping"]["methods"].arrayValue;
            for d in 0..<shipMethods.count{
                let value = shipMethods[d]["value"].stringValue;
                let key = shipMethods[d]["label"].stringValue;
                self.shippingMethods[key] = value;
            }
        }
        //print(paymentMethods);
        print(shippingMethods);
        self.renderCheckoutStep3View();
    }
    func renderCheckoutStep3View(){
        
        self.makeSomeSpaceInStackView(height: CGFloat(0.0));
        
        
        let shippingOptionDropDownView = CustomOptionDropDownView();
        shippingOptionDropDownView.translatesAutoresizingMaskIntoConstraints = false;
        shippingOptionDropDownView.topLabel.text = "Select Shipping Method".localized;
        shippingOptionDropDownView.topLabel.fontColorTool()
        shippingOptionDropDownView.dropDownButton.setTitle("-- Select --".localized, for: UIControl.State());
        shippingOptionDropDownView.dropDownButton.tag = 200;
        //shippingOptionDropDownView.dropDownButton.setTitleColor(.black, for: .normal)
        shippingOptionDropDownView.dropDownButton.addTarget(self, action: #selector(showOptionsDropdown(_:)), for: UIControl.Event.touchUpInside);
        stackView.addArrangedSubview(shippingOptionDropDownView);
        let shippingOptionDropDownViewHeight = translateAccordingToDevice(CGFloat(100.0));
        shippingOptionDropDownView.heightAnchor.constraint(equalToConstant: shippingOptionDropDownViewHeight).isActive = true;
        self.setLeadingAndTralingSpaceFormParentView(shippingOptionDropDownView,parentView:stackView);
        
        let proceddToNextStepButton = UIButton();
        proceddToNextStepButton.translatesAutoresizingMaskIntoConstraints = false;
        proceddToNextStepButton.setThemeColor();
        proceddToNextStepButton.setTitleColor(Settings.themeTextColor, for: UIControl.State.normal);
        proceddToNextStepButton.setTitle("Proceed".localized, for: UIControl.State.normal);
        proceddToNextStepButton.titleLabel?.font = UIFont(fontName: "", fontSize: CGFloat(17.0));
        proceddToNextStepButton.addTarget(self, action: #selector(proceddToNextStepButtonPressed(_:)), for: UIControl.Event.touchUpInside);
        stackView.addArrangedSubview(proceddToNextStepButton);
        proceddToNextStepButton.makeCornerRounded(cornerRadius: translateAccordingToDevice(CGFloat(5.0)));
        let proceddToNextStepButtonHeight = translateAccordingToDevice(CGFloat(40.0));
        proceddToNextStepButton.heightAnchor.constraint(equalToConstant: proceddToNextStepButtonHeight).isActive = true;
        self.setLeadingAndTralingSpaceFormParentView(proceddToNextStepButton, parentView:stackView);
        
        self.makeSomeSpaceInStackView(height: CGFloat(0.0));
    }
    
    @objc func showOptionsDropdown(_ sender:UIButton){
        
        
        var ArrayToUse = ["-- Select --".localized];
        
        
        let tempArray = Array(self.shippingMethods.keys);
        ArrayToUse += tempArray;
        
        dropDown.dataSource = ArrayToUse;
        dropDown.selectionAction = {(index, item) in
            sender.setTitle(item, for: UIControl.State());
        }
        
        dropDown.anchorView = sender
        dropDown.bottomOffset = CGPoint(x: 0, y:sender.bounds.height)
        if dropDown.isHidden {
            let _ = dropDown.show();
        } else {
            dropDown.hide();
        }
        
    }
    
    
    @objc func proceddToNextStepButtonPressed(_ sender:UIButton){
        print("proceddToNextStepButtonPressed");
        
        for view in stackView.subviews{
            if let customOptionDropDownView = view as? CustomOptionDropDownView{
                let textOnButton = customOptionDropDownView.dropDownButton.titleLabel?.text!;
                
                if(customOptionDropDownView.dropDownButton.tag == 200){
                    
                        
                        if(self.shippingMethods[textOnButton!] == nil){
                            let msg = "Please Select Shipping Method".localized;
                            self.view.makeToast(msg, duration: 2.0, position: .bottom, title: nil, image: nil, style: nil, completion: nil);
                            return;
                        }
                        self.shipping_method = self.shippingMethods[textOnButton!]!;
                    
                }
            }
        }
        
        let storyboard = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil);
        let vc=storyboard.instantiateViewController(withIdentifier: "checkoutStep3ViewController") as! CheckoutStep3ViewController
        vc.shipping_method=self.shipping_method
        vc.email_id=self.email_id
        vc.checkoutAs=self.checkoutAs
        vc.totalOrderData = totalOrderData
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
    func savePaymentData(_ paymentId:String){
        
        var urlToRequest = "mobiconnect/checkout/saveshippingpayament";
        let requestHeader = Settings.headerKey;
        let baseURL = Settings.baseUrl;
        urlToRequest = baseURL+urlToRequest;
        
        var postString = "";
        var postData = [String:String]()
        postData["cart_id"] = cart_id;
        if self.payment_method.lowercased() == "Braintree".lowercased(){
            postData["payment_method"] = "apppayment";
        }else{
            postData["payment_method"] = payment_method;
        }
        postData["shipping_method"] = shipping_method;
        postData["Role"] = checkoutAs;
        postData["email"] = email_id;
        if paymentId != "" {
            postData["additional_information"] = paymentId
        }
        if(defaults.object(forKey: "userInfoDict") != nil){
            postData["hashkey"] = userInfoDict["hashKey"]!
            postData["customer_id"] = userInfoDict["customerId"]!;
        }
        postString = ["parameters":postData].convtToJson() as String
        print(postString);
        var request = URLRequest(url: URL(string: "\(urlToRequest)")!);
        request.httpMethod = "POST";
        request.httpBody = postString.data(using: String.Encoding.utf8);
        if UserDefaults.standard.bool(forKey: "isLogin"){
            if let user = UserDefaults.standard.object(forKey: "userInfoDict") as? [String:String] {
                request.setValue(user["hashKey"], forHTTPHeaderField: "hashkey")
            }
            
        }
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        cedMageLoaders.addDefaultLoader(me: self);
        let task = URLSession.shared.dataTask(with: request){
            
            // check for fundamental networking error
            data, response, error in
            guard error == nil && data != nil else{
                print("error=\(error)")
                DispatchQueue.main.async{
                    print(error?.localizedDescription as Any);
                    cedMageLoaders.removeLoadingIndicator(me: self);
                }
                return;
            }
            
            // check for http errors
            if let httpStatus = response as? HTTPURLResponse , httpStatus.statusCode != 200{
                
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(response)")
                DispatchQueue.main.async{
                    cedMageLoaders.removeLoadingIndicator(me: self);
                }
                return;
            }
            
            // code to fetch values from response :: start
            guard var jsonResponse = try? JSON(data: data!) else{return;}
            jsonResponse = jsonResponse[0]
            if(jsonResponse != nil){
                
                DispatchQueue.main.async{
                    cedMageLoaders.removeLoadingIndicator(me: self);
                    print(jsonResponse);
                    
                  
                    
                }
            }
        }
        
        task.resume();
        
    }
    
    override func basicFoundationToRenderView(bottomMargin:CGFloat){
        
        self.imageViewOnTopHeight.constant = translateAccordingToDevice(CGFloat(40.0));
        
        _ = self.tabBarController?.tabBar.frame.size.height;
        // adding scrollview
        scrollView = UIScrollView();
        scrollView.translatesAutoresizingMaskIntoConstraints = false;
        scrollView.showsHorizontalScrollIndicator = false;
        scrollView.showsVerticalScrollIndicator = false;
        view.addSubview(scrollView);
        self.view.addConstraint(NSLayoutConstraint(item: scrollView, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 0));
        self.view.addConstraint(NSLayoutConstraint(item: scrollView, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 0));
        self.view.addConstraint(NSLayoutConstraint(item: scrollView, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.imageViewOnTop, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: 0));
        self.view.addConstraint(NSLayoutConstraint(item: scrollView, attribute: NSLayoutConstraint.Attribute.bottom, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.bottomLayoutGuide, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: -(bottomMargin)));
        
        // adding stackview
        stackView = UIStackView();
        stackView.translatesAutoresizingMaskIntoConstraints = false;
        stackView.axis  = NSLayoutConstraint.Axis.vertical;
        stackView.distribution  = UIStackView.Distribution.equalSpacing;
        stackView.alignment = UIStackView.Alignment.center;
        stackView.spacing   = 10.0;
        scrollView.addSubview(stackView);
        self.view.addConstraint(NSLayoutConstraint(item: stackView, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 0));
        self.view.addConstraint(NSLayoutConstraint(item: stackView, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 0));
        scrollView.addConstraint(NSLayoutConstraint(item: stackView, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: scrollView, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1, constant: 0));
        scrollView.addConstraint(NSLayoutConstraint(item: stackView, attribute: NSLayoutConstraint.Attribute.bottom, relatedBy: NSLayoutConstraint.Relation.equal, toItem: scrollView, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: 0));
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    //custom make order
    override func recieveResponse(data: Data?, requestUrl: String?, response: URLResponse?) {
        if let data  = data {
            guard var json = try? JSON(data:data) else{return;}
            json = json[0]
            if requestUrl ==  "mobiconnect/checkout/saveorder" {
                if(json["success"].stringValue == "true"){
                    self.defaults.removeObject(forKey: "guestEmail");
                    self.defaults.removeObject(forKey: "cartId");
                    self.defaults.setValue("0", forKey: "items_count")
                    let order_id = json["order_id"].stringValue;
                    let storyboard = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil);
                    let viewController = storyboard.instantiateViewController(withIdentifier: "orderPlacedViewController") as! OrderPlacedViewController;
                    viewController.order_id = order_id;
                    self.navigationController?.viewControllers = [viewController];
                    
                }
            }else if requestUrl == "mobiconnect/checkout/viewcart" {
                let jsonResponse = json
                if(jsonResponse["success"].stringValue.lowercased() == "false".lowercased()){
                    self.defaults.removeObject(forKey: "cartId")
                }
                self.totalOrderData["currency_code"] = jsonResponse["data"]["currency_code"].stringValue
                self.totalOrderData["currency_symbol"] = jsonResponse["data"]["currency_symbol"].stringValue
                self.totalOrderData["amounttopay"] = jsonResponse["data"]["total"][0]["amounttopay"].stringValue;
                self.totalOrderData["tax_amount"] = jsonResponse["data"]["total"][0]["tax_amount"].stringValue;
                self.totalOrderData["shipping_amount"] = jsonResponse["data"]["total"][0]["shipping_amount"].stringValue;
                self.totalOrderData["discount_amount"] = jsonResponse["data"]["total"][0]["discount_amount"].stringValue;
                self.totalOrderData["appliedcoupon"] = jsonResponse["data"]["total"][0]["coupon"].stringValue
                self.totalOrderData["grandtotal"] = jsonResponse["data"]["grandtotal"].stringValue;
            }
        }
    }
    
}
