/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */

import UIKit

var imageCacheContainer = [String:UIImage]();
func clearFilterRelatedData(){
    let defaults = UserDefaults.standard;
    if(defaults.object(forKey: "previousSelectedFilters") != nil){
        defaults.removeObject(forKey: "previousSelectedFilters");
    }
    if(defaults.object(forKey: "filtersToSend") != nil){
        defaults.removeObject(forKey: "filtersToSend");
    }
}

func translateAccordingToDevice(_ value:CGFloat)->CGFloat{
    if (UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad) {
        // iPad
        return value*1.5;
    }
    return value;
}

func isValidEmail(email:String) -> Bool{
    let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}";
    let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx);
    return emailTest.evaluate(with: email);
    
}
func isValidNumber(phone:String) -> Bool{
    let regEx = "[0-9]";
    let phoneText = NSPredicate(format:"SELF MATCHES %@", regEx);
    return phoneText.evaluate(with: phone);
    
}

func downloadImageFromServer(_ imgView:UIImageView,urlToRequest:String){
    
    var activityIndicator = UIActivityIndicatorView();
    activityIndicator.translatesAutoresizingMaskIntoConstraints = false;
    activityIndicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.whiteLarge);
    activityIndicator.frame = CGRect(x: 0, y: 0, width: 50, height: 50);
    activityIndicator.startAnimating();
    activityIndicator.color = UIColor.black;
    activityIndicator.center.x = imgView.bounds.width/2;
    activityIndicator.center.y = imgView.bounds.height/2;
    imgView.addSubview(activityIndicator);
    
    let imgRequest = URLRequest(url: URL(string: urlToRequest)!);
    let task = URLSession.shared.dataTask(with: imgRequest){
        // check for fundamental networking error
        data, response, error in
        guard error == nil && data != nil else{
            print("error=\(String(describing: error))")
            return;
        }
        
        // check for http errors
        if let httpStatus = response as? HTTPURLResponse , httpStatus.statusCode != 200{
            print("statusCode should be 200, but is \(httpStatus.statusCode)")
            print("response = \(String(describing: response))")
            return;
        }
        
        // Store the image in to our cache
        //self.imageCache[urlString] = image
        // Update the cell
        DispatchQueue.main.async{
            // Convert the downloaded data in to a UIImage object
            let image = UIImage(data: data!);
            imageCacheContainer[urlToRequest] = image;
            
            activityIndicator.removeFromSuperview();
            imgView.image = image;
        }
        
    }
    task.resume();
    
}
