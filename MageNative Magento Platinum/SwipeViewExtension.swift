/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */


import UIKit

extension CartViewController {
    
    @objc func productCardSwiped(_ recognizer: UITapGestureRecognizer){
        
        print("productCardSwiped");
        
        if let cartProductListView = recognizer.view as? CartProductListView{
            
            let imageView = UIImageView();
            imageView.frame = cartProductListView.bounds;
            cartProductListView.addSubview(imageView);
            imageView.backgroundColor = UIColor.lightGray;
            imageView.alpha = 0.5;
            
            var productInfo = [String:String]();
            if let index = stackView.arrangedSubviews.index(of: cartProductListView) {
                productInfo = products[index-2];
                print(productInfo["product_id"] as Any);
            }
            
            let swipeEditAndDeleteView = SwipeEditAndDeleteView();
            swipeEditAndDeleteView.translatesAutoresizingMaskIntoConstraints = false;
            let cartProductListViewHeight = translateAccordingToDevice(CGFloat(150.0));
            
            swipeEditAndDeleteView.deleteCartItemButton.addTarget(self, action: #selector(CartViewController.deleteCartItem(_:)), for: UIControl.Event.touchUpInside);
            
            swipeEditAndDeleteView.productQty.text = productInfo["quantity"];
            swipeEditAndDeleteView.editCartItemButton.addTarget(self, action: #selector(CartViewController.editCartItemQuantityFunction(_:)), for: UIControl.Event.touchUpInside);
            
            cartProductListView.addSubview(swipeEditAndDeleteView);
            
            cartProductListView.addConstraint(NSLayoutConstraint(item: swipeEditAndDeleteView, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: cartProductListView, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 0));
            cartProductListView.addConstraint(NSLayoutConstraint(item: swipeEditAndDeleteView, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: cartProductListView, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 0));
            
            cartProductListView.addConstraint(NSLayoutConstraint(item: swipeEditAndDeleteView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: cartProductListViewHeight));
            cartProductListView.addConstraint(NSLayoutConstraint(item: swipeEditAndDeleteView, attribute: NSLayoutConstraint.Attribute.centerY, relatedBy: NSLayoutConstraint.Relation.equal, toItem: cartProductListView, attribute: NSLayoutConstraint.Attribute.centerY, multiplier: 1, constant: 0));
            
            swipeEditAndDeleteView.center.x += self.view.bounds.width;
            UIView.animate(withDuration: 0.5, delay: 0, options: UIView.AnimationOptions.curveEaseIn, animations: {
                swipeEditAndDeleteView.center.x = self.view.center.x;
            }, completion: nil
            );
            
            /* magento 2 delete and edit on swipe*/
            let swipeGesture = UISwipeGestureRecognizer(target: self, action: #selector(CartViewController.swipeEditAndDeleteViewSwiped(_:)));
            swipeGesture.direction = UISwipeGestureRecognizer.Direction.right;
            swipeEditAndDeleteView.addGestureRecognizer(swipeGesture);
            swipeGesture.delegate=self;
            /* magento 2 delete and edit on swipe*/
            
            
            for recognizer in cartProductListView.gestureRecognizers! {
                //subview.removeGestureRecognizer(recognizer)
                print(recognizer);
                if recognizer is UISwipeGestureRecognizer{
                    cartProductListView.removeGestureRecognizer(recognizer);
                }
                if recognizer is UITapGestureRecognizer{
                    cartProductListView.removeGestureRecognizer(recognizer);
                }
            }
            
        }
        
    }
    
    @objc func swipeEditAndDeleteViewSwiped(_ recognizer: UITapGestureRecognizer){
        print("swipeEditAndDeleteViewSwiped");
        if let swipeEditAndDeleteView = recognizer.view as? SwipeEditAndDeleteView{
            if let cartProductListView = swipeEditAndDeleteView.superview as? CartProductListView{
                let swipeGesture = UISwipeGestureRecognizer(target: self, action: #selector(CartViewController.productCardSwiped(_:)));
                swipeGesture.direction = UISwipeGestureRecognizer.Direction.left;
                cartProductListView.addGestureRecognizer(swipeGesture);
                swipeGesture.delegate=self;
                
                let tapGesture = UITapGestureRecognizer(target: self, action: #selector(CartViewController.productCardTapped(_:)));
                cartProductListView.addGestureRecognizer(tapGesture);
                tapGesture.delegate=self;
                
                UIView.animate(withDuration: 0.5, delay: 0, options: UIView.AnimationOptions.curveEaseIn, animations: {
                    swipeEditAndDeleteView.center.x += self.view.bounds.width;
                }, completion: {completed in
                    swipeEditAndDeleteView.removeFromSuperview();
                    for view in cartProductListView.subviews{
                        if view is UIImageView{
                            view.removeFromSuperview();
                            break;
                        }
                    }
                }
                );
            }
        }
    }
    
    @objc func deleteCartItem(_ sender:UIButton){
        
        let showTitle = "Confirmation".localized;
        let showMsg = "Operation can't be undone!".localized;
        
        let confirmationAlert = UIAlertController(title: showTitle, message: showMsg, preferredStyle: UIAlertController.Style.alert);
        
        confirmationAlert.addAction(UIAlertAction(title: "Done".localized, style: .default, handler: { (action: UIAlertAction!) in
            self.performDeleteItemFunction(sender: sender);
        }));
        
        confirmationAlert.addAction(UIAlertAction(title: "Cancel".localized, style: .default, handler: { (action: UIAlertAction!) in
        }));
        confirmationAlert.modalPresentationStyle = .fullScreen;
        present(confirmationAlert, animated: true, completion: nil)
    }
    
    func performDeleteItemFunction(sender:UIButton){
        var item_id = "";
        if let swipeEditAndDeleteView = sender.superview?.superview as? SwipeEditAndDeleteView{
            if let cartProductListView = swipeEditAndDeleteView.superview as? CartProductListView{
                if let index = stackView.arrangedSubviews.index(of: cartProductListView) {
                    print(index);
                    let productInfo = products[index-2];
                    print(productInfo["product_id"] as Any);
                    item_id = productInfo["item_id"]!;
                }
            }
        }
        item_id = String(sender.tag)
        var urlToRequest = "mobiconnect/checkout/delete/";
        let requestHeader = cedMage.getInfoPlist(fileName:"cedMage",indexString: "requestheader") as! String
        let baseURL = Settings.baseUrl;
        urlToRequest = baseURL+urlToRequest;
        var postString = "";
        var postData = [String:String]()
        if(defaults.object(forKey: "userInfoDict") != nil){
            userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
            postData["hashkey"]=userInfoDict["hashKey"]!
            postData["customer_id"]=userInfoDict["customerId"]!;
            if let cartID = UserDefaults.standard.value(forKey: "cartId") as? String {
                postData["cart_id"] = cartID
            }
            
            postData["item_id"]=item_id;
        }
        else{
            if let cartID = UserDefaults.standard.value(forKey: "cartId") as? String {
                postData["cart_id"] = cartID
            }
            postData["item_id"]=item_id;
        }
        postString = ["parameters":postData].convtToJson() as String
        var request = URLRequest(url: URL(string: "\(urlToRequest)")!);
        request.httpMethod = "POST";
        request.httpBody = postString.data(using: String.Encoding.utf8);
        if UserDefaults.standard.bool(forKey: "isLogin"){
            if let user = UserDefaults.standard.object(forKey: "userInfoDict") as? [String:String] {
                request.setValue(user["hashKey"], forHTTPHeaderField: "hashkey")
            }
            
        }
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        cedMageLoaders.addDefaultLoader(me: self);
        let task = URLSession.shared.dataTask(with: request){
            
            // check for fundamental networking error
            data, response, error in
            guard error == nil && data != nil else{
                print("error=\(String(describing: error))")
                DispatchQueue.main.async{
                    print(error?.localizedDescription as Any);
                    cedMageLoaders.removeLoadingIndicator(me: self);
                }
                return;
            }
            
            // check for http errors
            if let httpStatus = response as? HTTPURLResponse , httpStatus.statusCode != 200{
                
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(String(describing: response))")
                DispatchQueue.main.async{
                    cedMageLoaders.removeLoadingIndicator(me: self);
                    
                }
                return;
            }
            
            // code to fetch values from response :: start
            guard var jsonResponse = try? JSON(data: data!) else{return;}
            jsonResponse = jsonResponse[0]
            if(jsonResponse != nil){
                
                DispatchQueue.main.async{
                    print(jsonResponse)
                    cedMageLoaders.removeLoadingIndicator(me: self);
                    if(jsonResponse["success"].stringValue == "removed_successfully"){
                        self.resetViewAndVariables();
                        /*if self.cart_id !=  ""{
                            self.makeRequestToAPI("mobiconnect/checkout/viewcart",dataToPost: ["cart_id":self.cart_id])
                        }else{
                            
                            self.makeRequestToAPI("mobiconnect/checkout/viewcart",dataToPost: [:])
                        }*/
                        self.getCartData()
                        
                    }
                }
            }
        }
        task.resume();
    }
    
    
    @objc func editCartItemQuantityFunction(_ sender:UIButton){
        print("editCartItemQuantityFunction");
        if let swipeEditAndDeleteView = sender.superview?.superview as? CartProductListView{
            let newProductQty = swipeEditAndDeleteView.qty.text!;
            print(newProductQty);
            let cartProductListView = swipeEditAndDeleteView as CartProductListView
                if let index = stackView.arrangedSubviews.index(of: cartProductListView) {
                    print(index);
                    let productInfo = products[index-3];
                    print(productInfo["product_id"] as Any);
                    print(productInfo);
                    if(newProductQty != productInfo["quantity"]!){
                        if self.cart_id == ""{
                           self.cartUpdationRequestToServer("mobiconnect/checkout/updateqty/",dataToPost: ["item_id":productInfo["item_id"]!, "qty":newProductQty,"cart_id":"0"]);
                            return
                        }
                        self.cartUpdationRequestToServer("mobiconnect/checkout/updateqty/",dataToPost: ["cart_id":self.cart_id, "item_id":productInfo["item_id"]!, "qty":newProductQty]);
                    }
                    else{
                        let msg = "Please Make Some Change In Quantity First";
                        self.view.makeToast(msg, duration: 2.0, position: .bottom, title: nil, image: nil, style: nil, completion: nil);
                    }
                }
            
        }
    }
    
    
    func cartUpdationRequestToServer(_ urlToRequest:String, dataToPost:[String:String]){
        var urlToRequest = urlToRequest
        
        if(defaults.object(forKey: "userInfoDict") != nil){
            userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
        }
        
        let requestHeader = Settings.headerKey//cedMage.getInfoPlist(fileName:"cedMage",indexString: "requestheader") as! String
        let baseURL = Settings.baseUrl//cedMage.getInfoPlist(fileName:"cedMage",indexString: "cedBaseUrl") as! String;
        
        urlToRequest = baseURL+urlToRequest;
        
        var postString = "";
        var postData = [String:String]()
        for (key,val) in dataToPost{
            postData[key] = val;
        }
        if defaults.bool(forKey: "isLogin") {
            postData["hashkey"] = userInfoDict["hashKey"]
            postData["customer_id"] = userInfoDict["customerId"]
            
            
        }
        if let cartID = UserDefaults.standard.value(forKey: "cartId") as? String {
            postData["cart_id"] = cartID
        }
        postString = ["parameters":postData].convtToJson() as String
        
        print("cartUpdationRequestToServer");
        print(urlToRequest);
        print(postString);
        
        var request = URLRequest(url: URL(string: "\(urlToRequest)")!);
        request.httpMethod = "POST";
        request.httpBody = postString.data(using: String.Encoding.utf8);
        if UserDefaults.standard.bool(forKey: "isLogin"){
            if let user = UserDefaults.standard.object(forKey: "userInfoDict") as? [String:String] {
                request.setValue(user["hashKey"], forHTTPHeaderField: "hashkey")
            }
            
        }
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        cedMageLoaders.addDefaultLoader(me: self);
        let task = URLSession.shared.dataTask(with: request){
            
            // check for fundamental networking error
            data, response, error in
            guard error == nil && data != nil else{
                print("error=\(error)")
                DispatchQueue.main.async{
                    print(error?.localizedDescription as Any);
                    cedMageLoaders.removeLoadingIndicator(me: self);
                }
                return;
            }
            
            // check for http errors
            if let httpStatus = response as? HTTPURLResponse , httpStatus.statusCode != 200{
                
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(response)")
                DispatchQueue.main.async{
                    cedMageLoaders.removeLoadingIndicator(me: self);
                    
                }
                return;
            }
            
            // code to fetch values from response :: start
            
            let datastring = NSString(data: data!, encoding: String.Encoding.utf8.rawValue);
            //NSString(data: NSData!, encoding: String.Encoding.utf8)
            print("datastring");
            print(datastring as Any);
            
            guard var jsonResponse = try? JSON(data: data!) else{return;}
            jsonResponse = jsonResponse[0]
            if(jsonResponse != nil){
                
                DispatchQueue.main.async{
                    
                    //print(jsonResponse);
                    cedMageLoaders.removeLoadingIndicator(me: self);
                    
                    let msg = jsonResponse["message"].stringValue;
                    if(jsonResponse["success"].stringValue == "true"){
                        /*if self.defaults.bool(forKey: "isLogin") {
                            self.makeRequestToAPI("mobiconnect/checkout/viewcart",dataToPost: [:]);
                        }
                        else{
                            if(self.defaults.object(forKey: "cartId") != nil){
                                self.cart_id = (self.defaults.object(forKey: "cartId") as? String)!;
                                self.makeRequestToAPI("mobiconnect/checkout/viewcart",dataToPost: ["cart_id":self.cart_id]);
                            }
                        }*/
                        self.getCartData()
                        /*if(self.defaults.object(forKey: "cartId") != nil){
                            self.cart_id = (self.defaults.object(forKey: "cartId") as? String)!;
                            self.makeRequestToAPI("mobiconnect/checkout/viewcart",dataToPost: ["cart_id":self.cart_id]);
                        }else{
                            
                        }*/
                        
                    }
                    self.view.makeToast(msg, duration: 2.0, position: .center, title: nil, image: nil, style: nil, completion: nil);
                }
            }
            
        }
        task.resume();
    }
    
}
