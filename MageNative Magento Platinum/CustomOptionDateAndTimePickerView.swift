/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */

import UIKit

class CustomOptionDateAndTimePickerView: UIView {

    // Our custom view from the XIB file
    var view: UIView!
    
    //outlets
    @IBOutlet weak var introLabel: UILabel!
    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var timePicker: UIDatePicker!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var selectButton: UIButton!
    
    override init(frame: CGRect)
    {
        // 1. setup any properties here
        
        // 2. call super.init(frame:)
        super.init(frame: frame)
        
        // 3. Setup view from .xib file
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        // 1. setup any properties here
        
        // 2. call super.init(coder:)
        super.init(coder: aDecoder)
        
        // 3. Setup view from .xib file
        xibSetup()
    }
    
    func xibSetup()
    {
        view = loadViewFromNib()
        
        // use bounds not frame or it'll be offset
        view.frame = bounds
        
        // Make the view stretch with containing view
        view.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        
        //extra setup
        
        introLabel.backgroundColor = UIColor(hexString: "#22A7F0");
        
        //datePicker.backgroundColor = UIColor.white;
        datePicker.makeCard(datePicker, cornerRadius: 2, color: UIColor(hexString: "#006442")!, shadowOpacity: 0.6);
        
        //timePicker.backgroundColor = UIColor.white;
        timePicker.makeCard(timePicker, cornerRadius: 2, color: UIColor(hexString: "#006442")!, shadowOpacity: 0.6);
        
        datePicker.datePickerMode = UIDatePicker.Mode.date;
        timePicker.datePickerMode = UIDatePicker.Mode.time;
        
//        self.makeCard(self, cornerRadius: 2, color: UIColor.black, shadowOpacity: 0.4);
        
        cancelButton.setTitle("Cancel".localized, for: UIControl.State());
        selectButton.setTitle("Done".localized, for: UIControl.State());
        cancelButton.backgroundColor = UIColor(hexString: "#CF3A24");
        selectButton.backgroundColor = UIColor(hexString: "#407A52");
        
        //extra setup
        
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView
    {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "CustomOptionDateAndTimePickerView", bundle: bundle)
        
        // Assumes UIView is top level and only object in CustomView.xib file
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    
    
}

