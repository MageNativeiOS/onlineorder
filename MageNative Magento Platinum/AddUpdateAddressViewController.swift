/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */

import UIKit
//import SkyFloatingLabelTextField
class AddUpdateAddressViewController: MagenativeUIViewController {
    
    var isEditingAddress = false;
    var address_id = "";
    //var current_country_code = "";
    var indexOfStateField = -1;
    var addressFieldsArray = ["email","prefix", "firstname","middlename", "lastname","suffix","country", "state", "street", "city", "pincode", "mobile","company"];
    
    var addressInfoArray = ["email":"Email".localized,"prefix":"Prefix(*)".localized, "firstname":"First Name(*)".localized,"middlename":"Middle Name(*)".localized, "lastname":"Last Name(*)".localized,"suffix":"Suffix(*)".localized, "country":"Country(*)".localized, "state":"State(*)".localized, "street":"Street(*)".localized, "city":"City(*)".localized, "pincode":"Postcode(*)".localized, "mobile":"Phone Number(*)".localized,"company":"Company(*)".localized];
    var countryDict = [String: String]();
    var stateDict = [String:String]();
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("()*()*()*()*()*()\(addressInfoArray)")
        let c_id=UserDefaults.standard.value(forKey: "userInfoDict") as! [String:String]
        self.sendRequest(url: "mobiconnect/customer/getRequiredFields/\(c_id["customerId"]!)", params: nil)
        
        // Do any additional setup after loading the view.
        userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
        self.basicFoundationToRenderView(bottomMargin: translateAccordingToDevice(CGFloat(40.0))+10);
        self.getRequestToAPI("mobiconnect/module/getcountry/",dataToPost: [:]);
    }
    var prefix_data:Dictionary<String,String> = [:]
    var suffix_data:Dictionary<String,String> = [:]
    var values_fields:Dictionary<String,JSON>=[:]
    var dropdown_options:Dictionary<String,JSON>=[:]
    var hide_check:Dictionary<String,String> = [:]
    
    
    override func recieveResponse(data: Data?, requestUrl: String?, response: URLResponse?) {
        guard let json = try? JSON(data: data!)else{return;}
        print(json)
        if json[0]["success"] == true
        {
            
            //first_name_data=String(describing: json[0]["firstname"])
            //last_name_data=String(describing: json[0]["lastname"])
            for (_,field) in json[0]["data"]
            {
                
                for (keys,values) in field
                {
                    if keys=="prefix"
                    {
                        let v=String(describing: values);
                        if v=="true"
                        {
                            values_fields.updateValue(field["value"], forKey: "prefix")
                        }
                    }else if keys=="middlename"
                    {
                        let v=String(describing: values);
                        if v=="true"
                        {
                            values_fields.updateValue(field["value"], forKey: "middlename")
                        }
                    }else if keys=="taxvat"
                    {
                        let v=String(describing: values);
                        if v=="true"
                        {
                            values_fields.updateValue(field["value"], forKey: "company")
                        }
                    }else if keys=="suffix"
                    {
                        let v=String(describing: values);
                        if v=="true"
                        {
                            values_fields.updateValue(field["value"], forKey: "suffix")
                        }
                    }
                    
                    
                    if(keys=="name")
                    {
                        let v=String(describing: values);
                        if(field[v]==false)
                        {
                            hide_check.updateValue("false", forKey: v)
                        }
                        else
                        {
                            hide_check.updateValue("true", forKey: v)
                            
                            if(field["type"]=="dropdown")
                            {
                                if(values=="suffix")
                                {
                                    //suffix_data=field["suffix_options"] as Dictionary<String,String>
                                    for (index,val) in field["suffix_options"]
                                    {
                                        suffix_data.updateValue(String(describing: val), forKey: index)
                                    }
                                    dropdown_options.updateValue(field["suffix_options"], forKey: "suffix")
                                }
                                else if(values=="prefix")
                                {
                                    for (index,val) in field["prefix_options"]
                                    {
                                        prefix_data.updateValue(String(describing: val), forKey: index)
                                    }
                                    dropdown_options.updateValue(field["prefix_options"], forKey: "prefix")
                                }
                            }
                        }
                    }
                }
            }
        }
        print("prefix_data:::::::::\(prefix_data)")
        print("suffix_data:::::::::\(suffix_data)")
        print("values_fields:::::::::\(values_fields)")
        print("dropdown_options:::::::::\(dropdown_options)")
        print("hide_check:::::::::\(hide_check)")
        
        if(!isEditingAddress)
        {
            addressInfoArray=[:]
            addressFieldsArray=[]
            addressFieldsArray = ["email"];
            addressInfoArray=["email":"Email"]
            if hide_check["prefix"]=="true"
            {
                //indexOfStateField+=1
                addressInfoArray.updateValue("Prefix(*)".localized, forKey: "prefix")
                addressFieldsArray.append("prefix")
                
            }
            addressFieldsArray.append("firstname")
            addressInfoArray.updateValue("First Name(*)".localized, forKey: "firstname")
            if hide_check["middlename"]=="true"
            {
                //indexOfStateField+=1
                addressInfoArray.updateValue("Middle Name(*)".localized, forKey: "middlename")
                addressFieldsArray.append("middlename")
            }
            addressFieldsArray.append("lastname")
            addressInfoArray.updateValue("Last Name(*)".localized, forKey: "lastname")
            if hide_check["suffix"]=="true"
            {
                //indexOfStateField+=1
                addressInfoArray.updateValue("Suffix(*)".localized, forKey: "suffix")
                addressFieldsArray.append("suffix")
            }
            addressFieldsArray.append("country")
            addressInfoArray.updateValue("Country(*)".localized, forKey: "country")
            addressFieldsArray.append("state")
            addressInfoArray.updateValue("State(*)".localized, forKey: "state")
            addressFieldsArray.append("street")
            addressInfoArray.updateValue("Street(*)".localized, forKey: "street")
            addressFieldsArray.append("city")
            addressInfoArray.updateValue("City(*)".localized, forKey: "city")
            addressFieldsArray.append("pincode")
            addressInfoArray.updateValue("Postcode(*)".localized, forKey: "pincode")
            addressFieldsArray.append("mobile")
            addressInfoArray.updateValue("Mobile(*)".localized, forKey: "mobile")
            
            if hide_check["taxvat"]=="true"
            {
                addressInfoArray.updateValue("Company(*)".localized, forKey: "company")
                addressFieldsArray.append("company")
            }
        }else{
           
            addressFieldsArray=[]
            addressFieldsArray = ["email"];
           
            if hide_check["prefix"]=="true"
            {
               
                addressFieldsArray.append("prefix")
                
            }
            addressFieldsArray.append("firstname")
           
            if hide_check["middlename"]=="true"
            {
                
                addressFieldsArray.append("middlename")
            }
            addressFieldsArray.append("lastname")
           
            if hide_check["suffix"]=="true"
            {
                
                addressFieldsArray.append("suffix")
            }
            addressFieldsArray.append("country")
           
            addressFieldsArray.append("state")
    
            addressFieldsArray.append("street")
        
            addressFieldsArray.append("city")
          
            addressFieldsArray.append("pincode")
         
            addressFieldsArray.append("mobile")
           
            
            if hide_check["taxvat"]=="true"
            {
              
                addressFieldsArray.append("company")
            }
        }
        self.renderAddUpdateAddressPage();
    }
    
    
    override func parseResponseReceivedFromAPI(_ jsonResponse:JSON){
        print("-----addupdate----\(jsonResponse)")
        let countryListing = jsonResponse[0]["country"].arrayValue;
        for d in 0..<countryListing.count {
            let key = countryListing[d]["value"].stringValue;
            let value = countryListing[d]["label"].stringValue;
            self.countryDict[value] = key;
        }
        //self.renderAddUpdateAddressPage();
    }
    
    func renderAddUpdateAddressPage(){
        
        self.makeSomeSpaceInStackView(height: CGFloat(0.0));
        stackView.spacing = 10.0
        let topLabelOnView = UILabel();
        
        topLabelOnView.translatesAutoresizingMaskIntoConstraints = false;
        if isEditingAddress==true{
            topLabelOnView.text = "Update Address".localized;
        }
        else
        {
            topLabelOnView.text = "Add An Address".localized;
        }
        topLabelOnView.setThemeColor();
        topLabelOnView.textColor = Settings.themeTextColor;
        topLabelOnView.textAlignment = NSTextAlignment.center;
        topLabelOnView.font = UIFont(fontName: "HelveticaNeue-Medium", fontSize: CGFloat(18.0));
        stackView.addArrangedSubview(topLabelOnView);
        
        let topLabelOnViewHeight = translateAccordingToDevice(CGFloat(40.0));
        topLabelOnView.heightAnchor.constraint(equalToConstant: topLabelOnViewHeight).isActive = true;
        self.setLeadingAndTralingSpaceFormParentView(topLabelOnView, parentView:stackView);
        
        
        for (key) in addressFieldsArray {
            if (hide_check[key]=="false")
            {
                continue;
            }
            if(key == "prefix" || key=="suffix")
             {
                let dropDownButtonView = DropDownButtonView();
                //dropDownButtonView.font = UIFont.systemFont(ofSize: 12)
                //dropDownButtonView.dropDownButton.titleLabel?.font = UIFont.systemFont(ofSize: 12)
                dropDownButtonView.translatesAutoresizingMaskIntoConstraints = false;
                dropDownButtonView.dropDownButton.titleLabel?.font = UIFont.systemFont(ofSize: 12)
//                dropDownButtonView.dropDownButton.fontColorTool()
                dropDownButtonView.dropDownButton.setTitle(addressInfoArray[key], for: UIControl.State.normal);
                if key=="prefix"
                {
                    dropDownButtonView.dropDownButton.addTarget(self, action: #selector(show_prefix_option(_:)), for: UIControl.Event.touchUpInside);
                }
                else
                {
                    dropDownButtonView.dropDownButton.addTarget(self, action: #selector(show_suffix_option(_:)), for: UIControl.Event.touchUpInside);
                }
                
                
                stackView.addArrangedSubview(dropDownButtonView);
                let dropDownButtonViewHeight = translateAccordingToDevice(CGFloat(40.0));
                dropDownButtonView.heightAnchor.constraint(equalToConstant: dropDownButtonViewHeight).isActive = true;
                
                self.setLeadingAndTralingSpaceFormParentView(dropDownButtonView, parentView:stackView);
                continue;
            }
            if(key == "country"){
                let dropDownButtonView = DropDownButtonView();
                
            
                dropDownButtonView.translatesAutoresizingMaskIntoConstraints = false;
                dropDownButtonView.dropDownButton.titleLabel?.font = UIFont.systemFont(ofSize: 12)
//                dropDownButtonView.dropDownButton.fontColorTool()
                dropDownButtonView.dropDownButton.setTitle(addressInfoArray[key], for: UIControl.State.normal);
                dropDownButtonView.dropDownButton.addTarget(self, action: #selector(AddUpdateAddressViewController.showCountryDropdown(_:)), for: UIControl.Event.touchUpInside);
                
                stackView.addArrangedSubview(dropDownButtonView);
                let dropDownButtonViewHeight = translateAccordingToDevice(CGFloat(40.0));
                dropDownButtonView.heightAnchor.constraint(equalToConstant: dropDownButtonViewHeight).isActive = true;
                
                self.setLeadingAndTralingSpaceFormParentView(dropDownButtonView, parentView:stackView);
                if let index = stackView.arrangedSubviews.firstIndex(of: dropDownButtonView) {
                    self.indexOfStateField = index+1;
                    print("self.indexOfStateField");
                    print(self.indexOfStateField);
                }
                continue;
            }
            
            let textField = SkyFloatingLabelTextField();
            
            if #available(iOS 13.0, *) {
                textField.textColor=UIColor.label
            } else {
                textField.textColor=UIColor.black
            }
            
            let value=UserDefaults.standard.value(forKey: "mageAppLang") as! [String]
            if value[0]=="ar"
            {
                textField.textAlignment = .right
            }
            else
            {
                textField.textAlignment = .left
            }
//            textField.textColor=UIColor.lightGray
            textField.fontColorTool()
            textField.tintColor=UIColor.systemGray
            textField.selectedLineColor=UIColor.systemGray
            textField.selectedTitleColor=UIColor.systemGray
            //textField.borderStyle = UITextBorderStyle.roundedRect;
            textField.translatesAutoresizingMaskIntoConstraints = false;
            if(!isEditingAddress){
                textField.placeholder = addressInfoArray[key];
                if(key == "email"){
                    textField.text = userInfoDict["email"]!;
                    textField.isUserInteractionEnabled = false;
                }
            }
            else{
                textField.placeholder = key;
                if(key == "email"){
                    textField.text = userInfoDict["email"]!;
                    textField.isUserInteractionEnabled = false;
                }
                else{
                    print("$$$$$$$$$$$$$\(addressInfoArray)")
                    textField.text = addressInfoArray[key];
                    if key == "company" {
                         textField.text = values_fields[key]?.stringValue;
                    }
                }
            }
            textField.font = UIFont.systemFont(ofSize: 12)
            stackView.addArrangedSubview(textField);
            let textFieldHeight = translateAccordingToDevice(CGFloat(40.0));
            textField.heightAnchor.constraint(equalToConstant: textFieldHeight).isActive = true;
            self.setLeadingAndTralingSpaceFormParentView(textField, parentView:stackView);
        }
        
        self.makeSomeSpaceInStackView(height: CGFloat(0.0));
        
        self.renderSaveUpdateAddressButton();
    }
    @objc func show_prefix_option(_ sender : UIButton)
    {
        //print(self.countryDict);
        let temp=dropdown_options["prefix"]
        var temp_arr=[String]()
       
        for (key,_) in temp!
        {
            temp_arr.append(key)
        }
        var tempDataSource = Array(temp_arr);
        print(tempDataSource);
        tempDataSource = tempDataSource.sorted { $0.localizedCaseInsensitiveCompare($1) == ComparisonResult.orderedAscending }
        dropDown.dataSource = tempDataSource;
        
        dropDown.selectionAction = {(index, item) in
//            sender.fontColorTool()
            sender.setTitle(item, for: UIControl.State());
            //self.fetchStatesCorrespondingToCountry(current_country_code: self.countryDict[item]!);
        }
        
        dropDown.anchorView = sender
        dropDown.bottomOffset = CGPoint(x: 0, y:sender.bounds.height)
        if dropDown.isHidden {
            let _ = dropDown.show();
        } else {
            dropDown.hide();
        }
    }
    @objc func show_suffix_option(_ sender : UIButton)
    {
        let temp=dropdown_options["suffix"]
        var temp_arr=[String]()
        
        for (key,_) in temp!
        {
            temp_arr.append(key)
        }
        var tempDataSource = Array(temp_arr);
        print(tempDataSource);
        tempDataSource = tempDataSource.sorted { $0.localizedCaseInsensitiveCompare($1) == ComparisonResult.orderedAscending }
        dropDown.dataSource = tempDataSource;
        
        dropDown.selectionAction = {(index, item) in
//            sender.fontColorTool()
            sender.setTitle(item, for: UIControl.State());
            //self.fetchStatesCorrespondingToCountry(current_country_code: self.countryDict[item]!);
        }
        
        dropDown.anchorView = sender
        dropDown.bottomOffset = CGPoint(x: 0, y:sender.bounds.height)
        if dropDown.isHidden {
            let _ = dropDown.show();
        } else {
            dropDown.hide();
        }
    }
    func renderSaveUpdateAddressButton(){
        let submitButton = UIButton();
        submitButton.translatesAutoresizingMaskIntoConstraints = false;
        submitButton.titleLabel?.font = UIFont(fontName: "", fontSize: CGFloat(17.0));
//        submitButton.fontColorTool()
        if(isEditingAddress){
            submitButton.setTitle("Update Address".localized, for: UIControl.State.normal);
        }
        else{
            submitButton.setTitle("Save Address".localized, for: UIControl.State.normal);
        }
        submitButton.setThemeColor();
        submitButton.makeCornerRounded(cornerRadius: 5);
//        submitButton.titleLabel?.textColor = UIColor.white;
        submitButton.titleLabel?.textAlignment = NSTextAlignment.center;
        
        submitButton.addTarget(self, action: #selector(AddUpdateAddressViewController.addOrUpdateAddress(_:)), for: UIControl.Event.touchUpInside);
        
        self.view.addSubview(submitButton);
        submitButton.makeCornerRounded(cornerRadius: translateAccordingToDevice(CGFloat(5.0)));
        let submitButtonHeight = translateAccordingToDevice(CGFloat(40.0));
        submitButton.heightAnchor.constraint(equalToConstant: submitButtonHeight).isActive = true;
        self.setLeadingAndTralingSpaceFormParentView(submitButton, parentView:self.view);
        self.view.addConstraint(NSLayoutConstraint(item: submitButton, attribute: NSLayoutConstraint.Attribute.bottom, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: -5));
    }
    
    @objc func showCountryDropdown(_ sender:UIButton){
        
        print(self.countryDict);
        var tempDataSource = Array(self.countryDict.keys);
        print(tempDataSource);
        tempDataSource = tempDataSource.sorted { $0.localizedCaseInsensitiveCompare($1) == ComparisonResult.orderedAscending }
        dropDown.dataSource = tempDataSource;
        
        dropDown.selectionAction = {(index, item) in
            sender.setTitle(item, for: UIControl.State());
//            sender.fontColorTool()
            self.fetchStatesCorrespondingToCountry(current_country_code: self.countryDict[item]!);
        }
        
        dropDown.anchorView = sender
        dropDown.bottomOffset = CGPoint(x: 0, y:sender.bounds.height)
        if dropDown.isHidden {
            let _ = dropDown.show();
        } else {
            dropDown.hide();
        }
        
    }
    @objc func showStateDropdown(_ sender:UIButton){
        
        print(self.stateDict);
        var tempDataSource = Array(self.stateDict.keys);
        print(tempDataSource);
        tempDataSource = tempDataSource.sorted { $0.localizedCaseInsensitiveCompare($1) == ComparisonResult.orderedAscending }
        dropDown.dataSource = tempDataSource;
        
        dropDown.selectionAction = {(index, item) in
            sender.setTitle(item, for: UIControl.State());
//            sender.fontColorTool()
        }
        
        dropDown.anchorView = sender
        dropDown.bottomOffset = CGPoint(x: 0, y:sender.bounds.height)
        if dropDown.isHidden {
            let _ = dropDown.show();
        } else {
            dropDown.hide();
        }
        
    }
    
    @objc func addOrUpdateAddress(_ sender:UIButton){
        var currentIndex = 2;
        var postString = "";
        var postData = [String:String]()
        postData["hashkey"] = userInfoDict["hashKey"]
        postData["customer_id"] = userInfoDict["customerId"]
        //postString += "hashkey="+userInfoDict["hashKey"]!+"&customer_id="+userInfoDict["customerId"]!;
        
        for key in addressFieldsArray{
            if currentIndex < stackView.arrangedSubviews.count   {
                
            
            if let textView = stackView.arrangedSubviews[currentIndex] as? SkyFloatingLabelTextField {
                if(textView.text == ""){
                    let msg = "All Fields Are Required !".localized;
                    self.view.makeToast(msg, duration: 2.0, position: .bottom, title: nil, image: nil, style: nil, completion: nil);
                    return;
                }
                if(key=="state")
                {
                    postData["region"] = textView.text;
                }
                else if(key=="pincode")
                {
                    postData["postcode"] = textView.text;
                }
                else if(key=="mobile")
                {
                    postData["telephone"] = textView.text;
                }
                else
                {
                    postData[key] = textView.text;
                }
                print("----addre--\(key)----\(String(describing: textView.text))")
                //postString += "&"+key+"="+textView.text!;
            }
            else {
                
                if let dropDownButtonView = stackView.arrangedSubviews[currentIndex] as? DropDownButtonView {
                    let text = dropDownButtonView.dropDownButton.titleLabel?.text;
                    print(key);
                    if key=="prefix"
                    {
                        if text=="Prefix(*)"
                        {
                            let msg = "All Fields Are Required !".localized;
                            self.view.makeToast(msg, duration: 2.0, position: .bottom, title: nil, image: nil, style: nil, completion: nil);
                            return;
                        }
                        postData["prefix"] = prefix_data[text!]
                    }
                    if key=="suffix"
                    {
                        if text=="Suffix(*)"
                        {
                            let msg = "All Fields Are Required !".localized;
                            self.view.makeToast(msg, duration: 2.0, position: .bottom, title: nil, image: nil, style: nil, completion: nil);
                            return;
                        }
                        postData["suffix"] = suffix_data[text!]
                    }
                    if(key == "country"){
                        if(countryDict[text!] == nil){
                            let msg = "All Fields Are Required !".localized;
                            self.view.makeToast(msg, duration: 2.0, position: .bottom, title: nil, image: nil, style: nil, completion: nil);
                            return;
                        }
                        if(key == "country")
                        {
                            postData["country_id"] = countryDict[text!]
                        }
                        print("---\(key)")
                        //postString += "&"+key+"="+countryDict[text!]!;
                    }
                    else{
                        postData["region_id"] = stateDict[text!]
                        print("----state--\(key)")
                        //postString += "&"+key+"="+stateDict[text!]!;
                    }
                }
            }
                }
            currentIndex = currentIndex+1;
        }
        if(isEditingAddress){
            postData["address_id"] = self.address_id
            //postString += "&address_id="+self.address_id;
        }
        postData["store_id"] = UserDefaults.standard.value(forKey: "storeId") as! String?
        print(postString);
        
        postString = ["parameters":postData].convtToJson() as String
        var urlToRequest = "mobiconnect/customer/saveaddress/";
        //let requestHeader = cedMage.getInfoPlist(fileName:"cedMage",indexString: "requestheader") as! String
        let baseURL = Settings.baseUrl
        urlToRequest = baseURL+urlToRequest;
        print(urlToRequest)
        print(postString)
        var request = URLRequest(url: URL(string: "\(urlToRequest)")!);
        request.httpMethod = "POST";
        request.httpBody = postString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue));
        //request.setValue(requestHeader, forHTTPHeaderField: "Mobiconnectheader")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        if UserDefaults.standard.bool(forKey: "isLogin"){
            if let user = UserDefaults.standard.object(forKey: "userInfoDict") as? [String:String] {
                request.setValue(user["hashKey"], forHTTPHeaderField: "hashkey")
            }
            
        }
        cedMageLoaders.addDefaultLoader(me: self);
        let task = URLSession.shared.dataTask(with: request){
            
            // check for fundamental networking error
            data, response, error in
            guard error == nil && data != nil else{
                print("error=\(String(describing: error))")
                DispatchQueue.main.async{
                    print(error?.localizedDescription as Any);
                    cedMageLoaders.removeLoadingIndicator(me: self);
                }
                return;
            }
            
            // check for http errors
            if let httpStatus = response as? HTTPURLResponse , httpStatus.statusCode != 200{
                
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(String(describing: response))")
                DispatchQueue.main.async{
                    cedMageLoaders.removeLoadingIndicator(me: self);
                }
                return;
            }
            
            // code to fetch values from response :: start
            guard let jsonResponse = try? JSON(data: data!) else{return;}
            print(jsonResponse)
            if(jsonResponse != nil){
                print(response as Any)
                DispatchQueue.main.async{
                    print(jsonResponse);
                    cedMageLoaders.removeLoadingIndicator(me: self);
                    let msg = jsonResponse[0]["data"]["customer"][0]["message"].stringValue;
                    if(jsonResponse[0]["data"]["customer"][0]["status"].stringValue == "success"){
                        self.view.makeToast(msg, duration: 2, position: .center, title: nil, image: nil, style: nil, completion: {
                            Void in
                            self.navigationController!.popViewController(animated: true);
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "newAddressAddedId"), object: nil);
                        })
                        
                        
                        
                    }
                    else{
                        self.view.makeToast(msg, duration: 2.0, position: .bottom, title: nil, image: nil, style: nil, completion: nil);
                    }
                }
            }
        }
        
        task.resume();
        
    }
    
    
    func fetchStatesCorrespondingToCountry(current_country_code:String){
        
        var urlToRequest = "mobiconnect/module/getcountry/";
        //let requestHeader = cedMage.getInfoPlist(fileName:"cedMage",indexString: "requestheader") as! String
        let baseURL = Settings.baseUrl
        urlToRequest = baseURL+urlToRequest+current_country_code;
        
        /*var postString = "";
         postString += "hashkey="+userInfoDict["hashKey"]!+"&customer_id="+userInfoDict["customerId"]!;
         postString += "&country_code="+current_country_code;
         */
        /*var postString = "";
         var postData = [String:String]()
         postData["hashkey"] = userInfoDict["hashKey"]
         postData["customer_id"] = userInfoDict["customerId"]
         postData["country_code"] = current_country_code;
         postString = ["parameters":postData].convtToJson() as String*/
        var request = URLRequest(url: URL(string: "\(urlToRequest)")!);
        request.httpMethod = "GET";
        //request.httpBody = postString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue));
        //request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        cedMageLoaders.addDefaultLoader(me: self);
        let task = URLSession.shared.dataTask(with: request){
            
            // check for fundamental networking error
            data, response, error in
            guard error == nil && data != nil else{
                print("error=\(String(describing: error))")
                DispatchQueue.main.async{
                    print(error?.localizedDescription as Any);
                    cedMageLoaders.removeLoadingIndicator(me: self);
                }
                return;
            }
            
            // check for http errors
            if let httpStatus = response as? HTTPURLResponse , httpStatus.statusCode != 200{
                
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(String(describing: response))")
                DispatchQueue.main.async{
                    cedMageLoaders.removeLoadingIndicator(me: self);
                    
                }
                return;
            }
            
            // code to fetch values from response :: start
            guard let jsonResponse = try? JSON(data: data!) else{return;}
            if(jsonResponse != nil){
                
                DispatchQueue.main.async{
                    print(jsonResponse);
                    cedMageLoaders.removeLoadingIndicator(me: self);
                    if(jsonResponse[0]["success"].stringValue == "true"){
                        let stateListing = jsonResponse[0]["states"].arrayValue;
                        
                        for d in 0..<stateListing.count {
                            let key = stateListing[d]["region_id"].stringValue;
                            let value = stateListing[d]["default_name"].stringValue;
                            self.stateDict[value] = key;
                        }
                        print(self.stateDict);
                        if let textField = self.stackView.arrangedSubviews[self.indexOfStateField] as? SkyFloatingLabelTextField {
                            print(textField.text as Any);
                            self.stackView.removeArrangedSubview(textField);
                            textField.removeFromSuperview();
                            
                            let dropDownButtonView = DropDownButtonView();
                            dropDownButtonView.translatesAutoresizingMaskIntoConstraints = false;
                            dropDownButtonView.dropDownButton.titleLabel?.font = UIFont.systemFont(ofSize: 12)
//                            dropDownButtonView.dropDownButton.fontColorTool()
                            dropDownButtonView.dropDownButton.setTitle(self.addressInfoArray["state"], for: UIControl.State.normal);
                            dropDownButtonView.dropDownButton.addTarget(self, action: #selector(AddUpdateAddressViewController.showStateDropdown(_:)), for: UIControl.Event.touchUpInside);
                            
                            self.stackView.insertArrangedSubview(dropDownButtonView, at: self.indexOfStateField)
                            let dropDownButtonViewHeight = translateAccordingToDevice(CGFloat(40.0));
                            dropDownButtonView.heightAnchor.constraint(equalToConstant: dropDownButtonViewHeight).isActive = true;
                            self.setLeadingAndTralingSpaceFormParentView(dropDownButtonView, parentView:self.stackView);
                            
                        }
                        else if let dropDownButtonView = self.stackView.arrangedSubviews[self.indexOfStateField] as? DropDownButtonView {
//                           dropDownButtonView.dropDownButton.fontColorTool()
                            dropDownButtonView.dropDownButton.setTitle(self.addressInfoArray["state"], for: UIControl.State.normal);
                        }
                    }
                    else{
                        if let dropDownButtonView = self.stackView.arrangedSubviews[self.indexOfStateField] as? DropDownButtonView {
                            self.stackView.removeArrangedSubview(dropDownButtonView);
                            dropDownButtonView.removeFromSuperview();
                            let textField = SkyFloatingLabelTextField();
                            textField.borderStyle = UITextField.BorderStyle.roundedRect;
                            textField.translatesAutoresizingMaskIntoConstraints = false;
                            textField.font = UIFont.systemFont(ofSize: 12);
//                            textField.textColor=UIColor.lightGray
                            textField.fontColorTool()
                            textField.tintColor=UIColor(hexString: "#E5AD01")!
                            textField.selectedLineColor=UIColor(hexString: "#E5AD01")!
                            textField.selectedTitleColor=UIColor(hexString: "#E5AD01")!
                            textField.text = "";
                            textField.placeholder = "State";
                           
                            
                            
                            
                            
                            self.stackView.insertArrangedSubview(textField, at: self.indexOfStateField)
                            let textFieldHeight = translateAccordingToDevice(CGFloat(40.0));
                            textField.heightAnchor.constraint(equalToConstant: textFieldHeight).isActive = true;
                            self.setLeadingAndTralingSpaceFormParentView(textField, parentView:self.stackView);
                        }
                        else if let textField = self.stackView.arrangedSubviews[self.indexOfStateField] as? SkyFloatingLabelTextField {
                            print("((((((((()))))))))")
                            print(self.isEditingAddress)
//                            textField.textColor=UIColor.lightGray
                            textField.fontColorTool()
                            textField.tintColor=UIColor(hexString: "#E5AD01")!
                            textField.selectedLineColor=UIColor(hexString: "#E5AD01")!
                            textField.selectedTitleColor=UIColor(hexString: "#E5AD01")!
                            textField.font = UIFont.systemFont(ofSize: 12);
                            textField.text = "";
                            textField.placeholder = "State";
                        }
                    }
                }
            }
        }
        task.resume();
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
