/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */

import Foundation
import UIKit
@objc class cedMageImageLoader : NSObject {
    
    private var cache = NSCache<AnyObject, AnyObject>()
    
    class var shared : cedMageImageLoader {
        struct Static {
            static let instance : cedMageImageLoader = cedMageImageLoader()
        }
        return Static.instance
    }
    
    func loadImgFromUrl(urlString: String, completionHandler:@escaping (_ image: UIImage?, _ url: String) -> ()) {
        
        DispatchQueue.global(qos: .background).async {
            let data: UIImage? = self.cache.object(forKey: urlString as AnyObject) as? UIImage
            
            if let goodData = data {
                print(data as Any)
                DispatchQueue.main.async {
                    completionHandler(goodData, urlString)
                      return
                }
              
            }
        }
        
        if(urlString == ""){
            return
        }
        let downloadtask = URLSession.shared.dataTask(with: URL(string:urlString)!, completionHandler: { data,response,error in
            if (error != nil) {
                completionHandler(nil, urlString)
                return
            }
            if let data = data {
                let image = UIImage(data: data)
                self.cache.setObject(image as AnyObject, forKey: urlString as AnyObject)
                DispatchQueue.main.async{
                    completionHandler(image, urlString)
                }
                return
            }
        })
        downloadtask.resume()
        
    }
    
    
}
