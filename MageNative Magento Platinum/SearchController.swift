//
//  SearchController.swift
//  MageNative Magento Platinum
//
//  Created by Hamza Usmani on 28/01/21.
//  Copyright © 2021 CEDCOSS Technologies Private Limited. All rights reserved.
//

import Foundation
import UIKit

final class SearchController: MagenativeUIViewController {
    
    // MARK:- PROPERTIES
    
    struct AutocompleteSuggestion {
        let product_name: String
        let product_id: String
        
        init(json: JSON) {
            product_name = json["product_name"].stringValue
            product_id   = json["product_id"].stringValue
        }
    }
    
    struct SearchProduct {
        let stock_status: String
        let wishlist_item_id: String
        let type: String
        let description: String
        let Inwishlist: String
        let regular_price: String
        let review: String
        let special_price: String
        let product_name: String
        let product_id: String
        let product_image: String
        
        init(json: JSON) {
            stock_status = json["stock_status"].stringValue
            wishlist_item_id = json["wishlist_item_id"].stringValue
            type = json["type"].stringValue
            description = json["description"].stringValue
            Inwishlist = json["Inwishlist"].stringValue
            regular_price = json["regular_price"].stringValue
            review = json["review"].stringValue
            special_price = json["special_price"].stringValue
            product_name = json["product_name"].stringValue
            product_id = json["product_id"].stringValue
            product_image = json["product_image"].stringValue
        }
    }
    
    private var autocompleteSuggestions: [AutocompleteSuggestion] = [] { didSet { showDropdown() } }
    private var searchProducts: [SearchProduct] = [] { didSet { searchResultCollectionView.reloadData() } }
    private var currentPage: Int = 1
    private var loadMoreProducts: Bool = true
    
    // MARK:- VIEWS
    
    lazy private var searchBar: UISearchBar = {
        let bar = UISearchBar()
        bar.searchBarStyle  = .minimal
        bar.placeholder     = "Enter Search term"
        bar.delegate        = self
        return bar
    }()
    
    lazy private var searchResultCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing       = 8
        layout.minimumInteritemSpacing  = 0
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.backgroundColor = .mageSystemBackground
        collectionView.register(SearchProductCC.self, forCellWithReuseIdentifier: SearchProductCC.reuseID)
        collectionView.dataSource   = self
        collectionView.delegate     = self
        return collectionView
    }()
    
    
    
    // MARK:- INITIALIZERS
    
    
    // MARK:- LIFE CYCLE
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
    }
    
    // MARK:- API CALLS
    
    private func getProductWith(searchText: String) {
        let param: [String:String] = ["q":searchText, "page": "\(currentPage)", "theme":"1"]
        
        ApiHandler.handle.request(with: "mobiconnect/catalog/search/", params: param, requestType: .POST, controller: self) { (data, error) in
            guard let data = data else { return }
            guard let json = try? JSON(data: data) else { return }
            print(json)
            
            if json[0].stringValue == "NO_PRODUCTS" { self.loadMoreProducts = false; return }
            
            if self.currentPage > 1 {
                let products = json[0]["data"]["products"].arrayValue.map({ SearchProduct(json: $0) })
                self.searchProducts.append(contentsOf: products)
            } else {
                self.searchProducts = json[0]["data"]["products"].arrayValue.map({ SearchProduct(json: $0) })
            }
        }
    }
    
    // MARK:- HELPERS
    
    private func configureUI() {
        view.backgroundColor = .mageSystemBackground
        
        view.addSubview(searchBar)
        searchBar.anchor(top: view.safeAreaLayoutGuide.topAnchor, left: view.leadingAnchor, right: view.trailingAnchor)
        
        view.addSubview(searchResultCollectionView)
        searchResultCollectionView.anchor(top: searchBar.bottomAnchor, left: view.leadingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.trailingAnchor,
                                          paddingTop: 16)
    }
    
    private func showDropdown() {
        let datasource = autocompleteSuggestions.map({ $0.product_name })
        
        dropDown.dataSource = datasource
        dropDown.anchorView = searchBar
        dropDown.bottomOffset = .init(x: 0, y: searchBar.bounds.height)
        
        let _ = dropDown.show()
        
        dropDown.selectionAction = { index, item in
            let vc = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "productSinglePageViewController") as! ProductSinglePageViewController
            vc.product_id = self.autocompleteSuggestions[index].product_id
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    // MARK:- SELECTORS
    
    @objc private func sendRequestForSearchData() {
        let searchText: String = searchBar.text ?? ""
        
        if searchText.count >= 3 {
            let keyword: String = searchText.trimmingCharacters(in: .whitespacesAndNewlines)
            
            ApiHandler.handle.request(with: "mobiconnectadvcart/search/autocomplete", params: ["keyword":keyword], requestType: .POST, controller: self) { (data, error) in
                guard let data = data else { return }
                guard let json = try? JSON(data:data) else { return }
                print(json)
                self.autocompleteSuggestions = json[0]["data"]["suggestion"].arrayValue.map({ AutocompleteSuggestion(json: $0) })
            }
        }
    }
    
    
}

// MARK:- UISEARCHBAR EXTENSION

// UISearchBarDelegate
extension SearchController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(sendRequestForSearchData), object: nil)
        self.perform(#selector(sendRequestForSearchData), with: nil, afterDelay: 2)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        let searchText: String = searchBar.text ?? ""
        getProductWith(searchText: searchText)
    }
}

// MARK:- UICOLLECTIONVIEW EXTENSION

// UICollectionViewDataSource
extension SearchController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return searchProducts.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SearchProductCC.reuseID, for: indexPath) as? SearchProductCC else { return .init() }
        cell.populate(with: searchProducts[indexPath.item])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if !loadMoreProducts { return }
        if indexPath.item == searchProducts.count - 1 {
            let searchText = searchBar.text ?? ""
            currentPage += 1
            getProductWith(searchText: searchText)
        }
    }
}

// UICollectionViewDelegate/ UICollectionViewDelegateFlowLayout
extension SearchController: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return .init(width: collectionView.frame.width/2 - 8, height: collectionView.frame.width/2 + 20)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .init(top: 4, left: 4, bottom: 4, right: 4)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "productSinglePageViewController") as! ProductSinglePageViewController
        vc.product_id = searchProducts[indexPath.item].product_id
        navigationController?.pushViewController(vc, animated: true)
    }
}

final class SearchProductCC: UICollectionViewCell {
  
    //MARK: - Properties
    
    static var reuseID: String = "SearchProductCC"
    
    //MARK: - Views
    
    lazy private var productImageView: UIImageView = {
        let imageview = UIImageView()
        imageview.contentMode = .scaleAspectFit
        return imageview
    }()
    
    lazy private var productNameLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 2
        label.textAlignment = .center
        label.font = .avenirMedium(withSize: 13)
        return label
    }()
    
    lazy private var productPriceLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        return label
    }()
    
    lazy private var starReview: FloatRatingView = {
        let starView = FloatRatingView()
        starView.emptyImage = UIImage(named: "StarEmpty")
        starView.fullImage  = UIImage(named: "StarFilled")
        starView.tintColor  = Settings.themeColor
        starView.maxRating  = 5
        starView.editable   = false
        return starView
    }()
    
    
    
    //MARK: - Life Cycle
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        contentView.layer.borderWidth = 0.5
        contentView.layer.borderColor = UIColor.mageSecondarySystemBackground.cgColor
        
        addSubview(productImageView)
        productImageView.anchor(top: topAnchor, left: leadingAnchor, right: trailingAnchor, paddingTop: 4, height: 120)
        
        addSubview(productNameLabel)
        productNameLabel.anchor(top: productImageView.bottomAnchor, left: leadingAnchor, right: trailingAnchor, paddingTop: 8, paddingLeft: 4, paddingRight: 4)
        
        addSubview(productPriceLabel)
        productPriceLabel.anchor(top: productNameLabel.bottomAnchor, left: leadingAnchor, right: trailingAnchor, paddingTop: 8, paddingLeft: 4, paddingRight: 4)
        
        addSubview(starReview)
        starReview.anchor(left: leadingAnchor, bottom: bottomAnchor, right: trailingAnchor, paddingLeft: 32, paddingBottom: 8, paddingRight: 32, height: 15)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Selectors
    
    
    
    //MARK: - Helper Functions
    
    func populate(with data: SearchController.SearchProduct) {
        productImageView.sd_setImage(with: URL(string: data.product_image), placeholderImage: UIImage(named: "placeholder"))
        productNameLabel.text = data.product_name
        
        let attributedString: NSMutableAttributedString = .init(string: "")
        if data.special_price == "no_special" {
            attributedString.append(NSAttributedString(string: data.regular_price, attributes: [.font: UIFont.avenirSemibold(withSize: 13)]))
        } else {
            attributedString.append(NSAttributedString(string: data.special_price, attributes: [.font: UIFont.avenirSemibold(withSize: 13)]))
            attributedString.append(NSAttributedString(string: "  "))
            attributedString.append(NSAttributedString(string: data.regular_price, attributes: [.font: UIFont.avenirSemibold(withSize: 12), .strikethroughStyle: 1]))
        }
        
        productPriceLabel.attributedText = attributedString
        
        starReview.rating = Float(data.review) ?? 0.0
    }
    
    
}
