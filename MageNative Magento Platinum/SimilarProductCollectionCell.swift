/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */

import UIKit

class SimilarProductCollectionCell: UICollectionViewCell {

    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var productRegularPrice: UILabel!
    @IBOutlet weak var productSpecialPrice: UILabel!
    
    @IBOutlet weak var stockView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        productName.font = UIFont(fontName: "HelveticaNeue-Medium", fontSize: CGFloat(12.0));
        productRegularPrice.font = UIFont(fontName: "", fontSize: CGFloat(12.0));
        productSpecialPrice.font = UIFont(fontName: "", fontSize: CGFloat(12.0));
        
        if #available(iOS 13.0, *) {
            self.backgroundColor = UIColor.systemBackground
        } else {
            self.backgroundColor = UIColor.white
        };
        self.makeCard(self, cornerRadius: 2, color: UIColor.gray, shadowOpacity: 0.4);
        
    }

}
