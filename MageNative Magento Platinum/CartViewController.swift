/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */


import UIKit

class CartViewController: MagenativeUIViewController,UIGestureRecognizerDelegate {
  
  
  var isOrderReview = false;// to be used in case of order review
  var orderEmail = "";// to be used in case of order review
  var all_pro_qty=[String]()
  var cart_id = "";
  
  var products = [[String: String]]();
  var options_selected = [String:[String:String]]();
  var bundle_options = [String:[String:String]]();
  var bundle_options_values = [String:[String:[String]]]();
  var total = [String: String]();
  var items_count = "";
  var items_qty = "";
  var allowed_guest_checkout = "";
  //PayMent method
  var selectedPaymentMethod = String()
  
  var orderStatusData = [String:String]()
  var flag=true
  var isVirtual=true
  //payment checkout
  let clientTokenUrl  =     "mobiconnect/mobibrain/generatetokenpost"
  let trasactionUrl   =     "mobiconnect/mobibrain/transaction"
  let finalOrderCheck =     "mobiconnect/checkout/additionalinfo"
  let saveOrder       =     "mobiconnect/checkout/saveorder"
 // var apiClient : BTAPIClient!
  //var braintreeClient: BTAPIClient!
  var previousMethods = [[String:String]]()
  var priceArray = [[String:String]]()
  var hasError: Bool = false
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    // Do any additional setup after loading the view.
    self.basicFoundationToRenderView(bottomMargin: translateAccordingToDevice(CGFloat(40.0))+85);
    
   /* if(defaults.bool(forKey: "isLogin") || (defaults.object(forKey: "cartId") == nil)){
        self.makeRequestToAPI("mobiconnect/checkout/viewcart",dataToPost: [:]);
    }
    else{
        cart_id = (defaults.object(forKey: "cartId") as? String)!;
        self.makeRequestToAPI("mobiconnect/checkout/viewcart",dataToPost: ["cart_id":cart_id]);
    } */
    /*if(defaults.object(forKey: "cartId") != nil && defaults.object(forKey: "isLogin") == nil){
      cart_id = (defaults.object(forKey: "cartId") as? String)!;
      self.makeRequestToAPI("mobiconnect/checkout/viewcart",dataToPost: ["cart_id":cart_id]);
    }else{
      self.makeRequestToAPI("mobiconnect/checkout/viewcart",dataToPost: [:]);
    }*/
    getCartData()
    self.navigationItem.rightBarButtonItems?.first?.isEnabled = false
    
  }
  
    func getCartData() {
        var param = [String:String]()
        
        // If cartID is Available
        if let cartID = UserDefaults.standard.value(forKey: "cartId") as? String {
            param["cart_id"] = cartID
        }
        
        // If User is Login
        if UserDefaults.standard.bool(forKey: "isLogin") {
            let userInfoDict = UserDefaults.standard.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
            param["hashkey"] = userInfoDict["hashKey"]!;
            param["customer_id"] = userInfoDict["customerId"]!;
        }
        
        self.makeRequestToAPI("mobiconnect/checkout/viewcart",dataToPost: param)
    }
    
    
  override func viewDidDisappear(_ animated: Bool) {
    self.dismiss(animated: true, completion: {})
  }
  
  
  override func parseResponseReceivedFromAPI(_ jsonResponse:JSON){
    print(jsonResponse);
    let jsonResponse = jsonResponse[0]
    if(jsonResponse["success"].stringValue.lowercased() == "false".lowercased()){
      self.defaults.removeObject(forKey: "cartId")
    }
    self.resetViewAndVariables();
    self.items_count = jsonResponse["data"]["items_count"].stringValue;
    self.items_qty = jsonResponse["data"]["items_qty"].stringValue;
    self.defaults.set(items_qty, forKey: "cart_items")
    self.allowed_guest_checkout = jsonResponse["data"]["allowed_guest_checkout"].stringValue;
    self.total["currency_code"] = jsonResponse["data"]["currency_code"].stringValue
    self.total["currency_symbol"] = jsonResponse["data"]["currency_symbol"].stringValue
    self.total["amounttopay"] = jsonResponse["data"]["total"][0]["amounttopay"].stringValue;
    self.total["tax_amount"] = jsonResponse["data"]["total"][0]["tax_amount"].stringValue;
    self.total["shipping_amount"] = jsonResponse["data"]["total"][0]["shipping_amount"].stringValue;
    self.total["discount_amount"] = jsonResponse["data"]["total"][0]["discount_amount"].stringValue;
    self.total["appliedcoupon"] = jsonResponse["data"]["total"][0]["coupon"].stringValue
    self.total["grandtotal"] = jsonResponse["data"]["grandtotal"].stringValue;
    if let jsonAmount = jsonResponse["data"]["segments"].array{
        for index in jsonAmount{
            priceArray.append(["value":index["value"].stringValue,"label":index["label"].stringValue])
        }
    }
    for cartProInfo in jsonResponse["data"]["products"].arrayValue{
      var tempData = [String:String]();
      
        if jsonResponse["data"]["cart_error"].stringValue != "" {
            hasError = true
        }
        
      tempData["product_type"] = cartProInfo["product_type"].stringValue;
      if(tempData["product_type"] != "downloadable")
      {
        flag=false
      }
      if(tempData["product_type"] != "virtual")
      {
        isVirtual=false
      }
      tempData["quantity"] = cartProInfo["quantity"].stringValue;
      let temp=cartProInfo["quantity"].stringValue;
      all_pro_qty.append(temp)
      tempData["product-name"] = cartProInfo["product-name"].stringValue;
      tempData["sub-total"] = cartProInfo["sub-total"].stringValue;
      tempData["product_image"] = cartProInfo["product_image"].stringValue;
      tempData["product_id"] = cartProInfo["product_id"].stringValue;
      tempData["item_id"] = cartProInfo["item_id"].stringValue;
      if(cartProInfo["options_selected"] != ""){
        var temp_options_selected = [String:String]();
        for (_,val) in cartProInfo["options_selected"]{
          temp_options_selected[val["label"].stringValue] = val["value"].stringValue;
        }
        self.options_selected[tempData["item_id"]!] = temp_options_selected;
      }
      
      if(cartProInfo["bundle_options"] != ""){
        var temp_bundle_options = [String:String]();
        var temp_bundle_options_values = [String:[String]]();
        for (_,val) in cartProInfo["bundle_options"]{
          temp_bundle_options[val["option_id"].stringValue] = val["label"].stringValue;
          var temp_inr_bundle_options_values = [String]();
          for (_,inrVal) in val["value"]{
            print(inrVal);
            let data = inrVal["title"].stringValue+"<<?>>"+inrVal["price"].stringValue+"<<?>>"+inrVal["qty"].stringValue;
            temp_inr_bundle_options_values.append(data);
          }
          temp_bundle_options_values[val["option_id"].stringValue] = temp_inr_bundle_options_values;
        }
        if(temp_bundle_options.count > 0){
          self.bundle_options[tempData["product_id"]!] = temp_bundle_options;
        }
        if(temp_bundle_options_values.count > 0){
          self.bundle_options_values[tempData["product_id"]!] = temp_bundle_options_values;
        }
      }
      self.products.append(tempData);
    }
    
    UserDefaults.standard.set(flag, forKey: "flag_downloadable")
    UserDefaults.standard.set(isVirtual, forKey: "flag_virtual")
    
    if(self.items_qty != ""){
      self.setCartCount(view: self,items: items_qty)
      defaults.setValue(items_qty, forKey: "items_count")
    }else{
      self.setCartCount(view: self,items: "0")
      defaults.setValue("0", forKey: "items_count")
    }
    
    self.renderCartPage();
  }
  
  
  
  func renderNoDataImage(imageName:String){
    
    let noDataImageView = nodataImageView();
    noDataImageView.frame = self.view.frame
    
    noDataImageView.topLabel.text = "You have no items in your shopping cart.".localized.uppercased()
    noDataImageView.continueShopping.setTitle("Continue Shopping".localized, for: .normal)
    //noDataImageView.continueShopping.fontColorTool()
    noDataImageView.continueShopping.addTarget(self, action: #selector(CartViewController.continueShopping), for: UIControl.Event.touchUpInside)
    noDataImageView.continueShopping.setThemeColor()
    noDataImageView.continueShopping.setTitleColor(Settings.themeTextColor, for: .normal)
    noDataImageView.translatesAutoresizingMaskIntoConstraints = false;
    noDataImageView.contentMode = UIView.ContentMode.scaleAspectFit;
    self.view.addSubview(noDataImageView);
    self.view.addConstraint(NSLayoutConstraint(item: noDataImageView, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 0));
    self.view.addConstraint(NSLayoutConstraint(item: noDataImageView, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 0));
    self.view.addConstraint(NSLayoutConstraint(item: noDataImageView, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1, constant: 0));
    self.view.addConstraint(NSLayoutConstraint(item: noDataImageView, attribute: NSLayoutConstraint.Attribute.bottom, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: 0));
    
    for view in self.view.subviews{
      if view is UIButton{
        view.isHidden = true;
      }
    }
    
  }
  
  override func continueShopping(_ sender: UIButton){
    /*if let viewController = UIStoryboard(name: "homeLayouts", bundle: nil).instantiateViewController(withIdentifier: "baseHomepageController") as? baseHomepageController {
      self.navigationController?.setViewControllers([viewController], animated: true)
    } */
    navigationController?.setViewControllers([HomeController()], animated: true)
  }
  
  
  func renderCartPage(){
    
    if(products.count == 0){
      self.renderNoDataImage(imageName:"emptyCart");
      return;
    }
    //cedMage().setcartCount(tabBar: self.tabBarController)
    
    self.makeSomeSpaceInStackView(height: CGFloat(5.0));
    
    let clearCart = clearCartView()
    stackView.addArrangedSubview(clearCart);
    let clearCartHeight = translateAccordingToDevice(CGFloat(60))
    clearCart.clearCart.backgroundColor = UIColor.clear
    clearCart.clearCart.setTitle("Clear Cart".localized, for: .normal)
    self.setLeadingAndTralingSpaceFormParentView(clearCart, parentView:stackView);
    clearCart.clearCart.addTarget(self, action: #selector(makeRequestToClearCart(_:)), for: .touchUpInside)
    clearCart.topLabel.text = "You Have ".localized + self.items_count + " Items in your Cart".localized
    clearCart.topLabel.textAlignment = .center
    clearCart.heightAnchor.constraint(equalToConstant: clearCartHeight).isActive = true;
    self.makeSomeSpaceInStackView(height: CGFloat(5.0));
    
    for product in products {
      
      let cartProductListView = CartProductListView();
        cartProductListView.updateCart.setTitle("UPDATE".localized, for: .normal)
        cartProductListView.delete.setTitle("DELETE".localized, for: .normal)
      if(!isOrderReview){
        cartProductListView.quantityEditView.isHidden = false
        cartProductListView.delete.isHidden = false
        cartProductListView.updateCart.isHidden = false
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(CartViewController.productCardTapped(_:)));
        cartProductListView.addGestureRecognizer(tapGesture);
        tapGesture.delegate=self;
        cartProductListView.delete.tag = Int(product["item_id"]!)!
        cartProductListView.delete.addTarget(self, action: #selector(CartViewController.deleteCartItem(_:)), for: .touchUpInside)
        cartProductListView.updateCart.addTarget(self, action: #selector(CartViewController.editCartItemQuantityFunction), for: .touchUpInside)
      }
      
      
      //            /* magento 2 delete and edit on swipe*/
      //            if(!isOrderReview){
      //                let swipeGesture = UISwipeGestureRecognizer(target: self, action: #selector(CartViewController.productCardSwiped(_:)));
      //                swipeGesture.direction = UISwipeGestureRecognizerDirection.left;
      //                cartProductListView.addGestureRecognizer(swipeGesture);
      //                swipeGesture.delegate=self;
      //            }
      //            /* magento 2 delete and edit on swipe*/
      
      cartProductListView.translatesAutoresizingMaskIntoConstraints = false;
      
      var productDescription = product["product-name"]!+"\n";
      productDescription += "Subtotal : "+product["sub-total"]!+"\n";
      // productDescription += "Qty : "+"1*"+product["quantity"]!+"="+product["quantity"]!+"\n";
      cartProductListView.qty.text = product["quantity"]
      if(product["product_type"] == "configurable"){
        productDescription += "\n";
        if let dataToUse = options_selected[product["item_id"]!]  {
          for (key,val) in dataToUse{
            productDescription += key+" : "+val+"\n";
          }
        }
        
        
        /* magento 2 */
        //                if(!isOrderReview){
        //                    cartProductListView.editProductOptionsButton.isHidden = false;
        //                    cartProductListView.editProductOptionsButton.addTarget(self, action: #selector(CartViewController.goToProductSinglePageToEditOptions(_:)), for: UIControlEvents.touchUpInside);
        //                }
        /* magento 2 */
        
      }
      else if(product["product_type"] == "bundle"){
        productDescription += "\n";
        let bundle_options_to_use = bundle_options[product["product_id"]!]!;
        let bundle_options_values_to_use = bundle_options_values[product["product_id"]!]!;
        
        for (key,val) in bundle_options_to_use{
          productDescription += val+"\n";
          let dataToRender = bundle_options_values_to_use[key]!;
          for inrVal in dataToRender{
            let inrValArray = inrVal.components(separatedBy: "<<?>>");
            productDescription += inrValArray[0]+"\n";
            productDescription +=
              "Price"+" : "+inrValArray[1]+"\n";
            productDescription +=
              "Qty"+" : "+inrValArray[2]+"\n";
          }
          productDescription += val+"\n\n";
        }
        
        
      }
      if(productDescription.last! == "\n"){
        productDescription = productDescription.substring(to: productDescription.index(before: productDescription.endIndex));
      }
      
      cartProductListView.productDescription.text = productDescription;
      let fontToApply = UIFont(fontName: "", fontSize: CGFloat(12.0))!;
      cartProductListView.productDescription.font = fontToApply;
      let textAttributes = [NSAttributedString.Key.font: fontToApply];
      let rect = productDescription.boundingRect(with: CGSize(width: screenSize.width/2, height: CGFloat.greatestFiniteMagnitude), options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: textAttributes, context: nil);
      
      
      if(product["product_image"] != nil){
        cartProductListView.productImage.sd_setImage(with: URL(string: product["product_image"]!), placeholderImage: UIImage(named: "placeholder"))
      }
      
      stackView.addArrangedSubview(cartProductListView);
      if(rect.height > 150.0){
        let cartProductListViewHeight = translateAccordingToDevice(CGFloat((rect.height)+80));
        cartProductListView.heightAnchor.constraint(equalToConstant: cartProductListViewHeight).isActive = true;
        cartProductListView.productDescription.layoutIfNeeded()
        cartProductListView.layoutIfNeeded()
      }
      else{
        let cartProductListViewHeight = translateAccordingToDevice(CGFloat(200.0));
        cartProductListView.heightAnchor.constraint(equalToConstant: cartProductListViewHeight).isActive = true;
        cartProductListView.productDescription.layoutIfNeeded()
        cartProductListView.layoutIfNeeded()
      }
      
      self.setLeadingAndTralingSpaceFormParentView(cartProductListView, parentView:stackView);
      
    }
    
    self.renderCouponAndPriceSectionOfCartPage();
    self.makeSomeSpaceInStackView(height: CGFloat(0.0));
    self.renderProceedToCheckoutButton();
  }
  
  
  
  func renderProceedToCheckoutButton(){
    let proceedToCheckoutButton = UIButton();
    proceedToCheckoutButton.translatesAutoresizingMaskIntoConstraints = false;
    proceedToCheckoutButton.setThemeColor();
    proceedToCheckoutButton.setTitleColor(Settings.themeTextColor, for: .normal)
    //        proceedToCheckoutButton.setTitleColor(UIColor.white, for: UIControlState.normal);
    
    if(isOrderReview){
      proceedToCheckoutButton.setTitle("Place Order".localized, for: UIControl.State.normal);
    }
    else{
      proceedToCheckoutButton.setTitle("Proceed To Checkout".localized, for: UIControl.State.normal);
    }
    proceedToCheckoutButton.addTarget(self, action: #selector(CartViewController.proceedToCheckoutButtonPressed(_:)), for: UIControl.Event.touchUpInside);
    
    proceedToCheckoutButton.titleLabel?.font = UIFont(fontName: "HelveticaNeue-Medium", fontSize: CGFloat(15.0));
    self.view.addSubview(proceedToCheckoutButton);
    proceedToCheckoutButton.makeCornerRounded(cornerRadius: translateAccordingToDevice(CGFloat(5.0)));
    let proceddToNextStepButtonHeight = translateAccordingToDevice(CGFloat(40.0));
    proceedToCheckoutButton.heightAnchor.constraint(equalToConstant: proceddToNextStepButtonHeight).isActive = true;
    self.setLeadingAndTralingSpaceFormParentView(proceedToCheckoutButton, parentView:self.view);
    /*if #available(iOS 11.0, *) {
      let window = UIApplication.shared.keyWindow
      if let bottomPadding = window?.safeAreaInsets.bottom {
        self.view.addConstraint(NSLayoutConstraint(item: proceedToCheckoutButton, attribute: NSLayoutConstraint.Attribute.bottom, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: -(bottomPadding)));
      }
    }else {
      self.view.addConstraint(NSLayoutConstraint(item: proceedToCheckoutButton, attribute: NSLayoutConstraint.Attribute.bottom, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: 0));
    } */
    proceedToCheckoutButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
  }
  /* magento 2 */
  
  func goToProductSinglePageToEditOptions(_ sender:UIButton){
    print("goToProductSinglePageToEditOptions");
    print("productCardTapped");
    print(sender.superview?.superview as Any);
    
    if let cartProductListView = sender.superview?.superview as? CartProductListView{
      if let index = stackView.arrangedSubviews.firstIndex(of: cartProductListView) {
        print(index);
        let productInfo = self.products[index-2];
        print(productInfo["product_id"] as Any);
        
        let storyboard = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil);
        let viewController = storyboard.instantiateViewController(withIdentifier: "productSinglePageViewController") as! ProductSinglePageViewController;
        viewController.product_id = productInfo["product_id"]!;
        viewController.item_id = productInfo["item_id"]!;
        viewController.previousQty = productInfo["quantity"]!;
        
        if(productInfo["product_type"] == "configurable"){
          let options_selected = self.options_selected[productInfo["product_id"]!]!;
          print(options_selected);
          viewController.previous_options_selected = options_selected;
        }
        
        self.navigationController?.pushViewController(viewController, animated: true);
      }
    }
  }
  
  @objc func makeRequestToClearCart(_ sender:UIButton){
    print("makeRequestToClearCart");
    let showTitle = "Confirmation".localized;
    let showMsg = "Are You Sure To Clear Your Cart?".localized;
    let confirmationAlert = UIAlertController(title: showTitle, message: showMsg, preferredStyle: UIAlertController.Style.alert);
    confirmationAlert.addAction(UIAlertAction(title: "Done".localized, style: .default, handler: { (action: UIAlertAction!) in
        
      self.cartUpdationRequestToServer("mobiconnect/checkout/emptycart/",dataToPost: ["cart_id":self.cart_id]);
    }));
    confirmationAlert.addAction(UIAlertAction(title: "Cancel".localized, style: .default, handler: { (action: UIAlertAction!) in
    }));
    confirmationAlert.modalPresentationStyle = .fullScreen;
    present(confirmationAlert, animated: true, completion: nil)
  }
  
  
  @objc func productCardTapped(_ recognizer: UITapGestureRecognizer){
    if let index = stackView.arrangedSubviews.firstIndex(of: recognizer.view!) {
      print(index);
      let productInfo = products[index-3];
      print(productInfo["product_id"] ?? "productId");
      let productview = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "rootPageViewController") as! cedMageProductViewRoot
      productview.pageData = products as NSArray
      let instance = cedMage.singletonInstance
      instance.storeParameterInteger(parameter: index-3)
      self.navigationController?.pushViewController(productview
        , animated: true)
    }
  }
  @objc func proceedToCheckoutButtonPressed(_ sender:UIButton){
    
    if hasError {
        view.makeToast("Cart has Some error")
        return
    }
    
    if(!isOrderReview){
      print("inside order")
      if(defaults.object(forKey: "userInfoDict") != nil){
        print(cedMage.checkModule(string:"MageNative_Mobicheckout"))
        //if(cedMage.checkModule(string:"MageNative_Mobicheckout")){
        if defaults.value(forKey: "checkoutEnable") as! String == "1"{
          print("inside check")
          let storyboard = UIStoryboard(name: "cedMageAccounts", bundle: nil);
          let viewController = storyboard.instantiateViewController(withIdentifier: "cedMageAdvancedCheckout") as! cedMageAdvancedCheckout;
          self.navigationController?.pushViewController(viewController, animated: true);
          //                    self.present(viewController, animated: true, completion: nil)
        }
        else{
            
          let storyboard = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil);
          let viewController = storyboard.instantiateViewController(withIdentifier: "checkoutStep2ViewController") as! CheckoutStep2ViewController;
          viewController.totalOrderData = total
          viewController.checkoutAs = "USER";
          self.navigationController?.pushViewController(viewController, animated: true);
        }
      }
      else{
        let actionControl = UIAlertController(title: "Select Type", message: nil, preferredStyle: .alert)
        actionControl.addAction(UIAlertAction(title: "Proceed as guest", style: .default, handler: {
          Void in
          //if(cedMage.checkModule(string:"MageNative_Mobicheckout")){
            if UserDefaults.standard.value(forKey: "checkoutEnable") as! String == "1"{
            let storyboard = UIStoryboard(name: "cedMageAccounts", bundle: nil);
            let viewController = storyboard.instantiateViewController(withIdentifier: "cedMageAdvancedCheckout") as! cedMageAdvancedCheckout;
            self.navigationController?.pushViewController(viewController, animated: true);
            //                        self.present(viewController, animated: true, completion: nil)
          }else{
            let storyboard = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil);
            let viewController = storyboard.instantiateViewController(withIdentifier: "checkoutStep2ViewController") as! CheckoutStep2ViewController;
            viewController.totalOrderData = self.total
            viewController.checkoutAs = "GUEST";
            self.navigationController?.pushViewController(viewController, animated: true);
            //                        self.present(viewController, animated: true, completion: nil)
          }
          
        }))
        actionControl.addAction(UIAlertAction(title: "Proceed as user", style: .default, handler: {
          Void in
          let viewcontroll = LoginController()//UIStoryboard(name: "cedMageLogin", bundle: nil).instantiateViewController(withIdentifier: "cedMageLogin") as! cedMageLogin
          //viewcontroll.isFromCheckOut = true
          //viewcontroll.total = self.total
          
          self.navigationController?.pushViewController(viewcontroll, animated: true)
          //                    self.present(viewcontroll, animated: true, completion: nil)
        }))
        actionControl.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(actionControl, animated: true, completion: nil)
      }
    }
    else
    {
      var urlToRequest = "mobiconnect/checkout/saveorder";
        let requestHeader = Settings.headerKey;
        let baseURL = Settings.baseUrl
      urlToRequest = baseURL+urlToRequest;
      var postString = "";
      var postData = [String:String]()
      if cart_id == ""{
        postData["cart_id"] = "0";
      }else{
        postData["cart_id"] = cart_id;
      }
      
      postData["email"] = orderEmail;
      if defaults.bool(forKey: "isLogin") {
        postData["hashkey"] = userInfoDict["hashKey"]!
        postData["customer_id"] = userInfoDict["customerId"]!;
      }
      postString = ["parameters":postData].convtToJson() as String
      print(postString)
      var request = URLRequest(url: URL(string: "\(urlToRequest)")!);
      request.httpMethod = "POST";
      request.httpBody = postString.data(using: String.Encoding.utf8);
        if UserDefaults.standard.bool(forKey: "isLogin"){
            if let user = UserDefaults.standard.object(forKey: "userInfoDict") as? [String:String] {
                request.setValue(user["hashKey"], forHTTPHeaderField: "hashkey")
            }
            
        }
      request.setValue("application/json", forHTTPHeaderField: "Content-Type")
      cedMageLoaders.addDefaultLoader(me: self);
      let task = URLSession.shared.dataTask(with: request){
        
        // check for fundamental networking error
        data, response, error in
        guard error == nil && data != nil else{
          print("error=\(String(describing: error))")
          DispatchQueue.main.async{
            print(error?.localizedDescription ?? "localizedDescription");
            cedMageLoaders.removeLoadingIndicator(me: self);
          }
          return;
        }
        // check for http errors
        if let httpStatus = response as? HTTPURLResponse , httpStatus.statusCode != 200{
          print("statusCode should be 200, but is \(httpStatus.statusCode)")
          print("response = \(String(describing: response))")
          DispatchQueue.main.async{
            cedMageLoaders.removeLoadingIndicator(me: self);
            
          }
          return;
        }
        
        // code to fetch values from response :: start
        guard var jsonResponse = try? JSON(data: data!)else{return;}
        jsonResponse = jsonResponse[0]
        if(jsonResponse != nil){
          
          DispatchQueue.main.async{
            
            print(jsonResponse);
            cedMageLoaders.removeLoadingIndicator(me: self);
            if(jsonResponse["success"].stringValue == "true"){
              self.orderStatusData["orderId"] = jsonResponse["order_id"].stringValue
              self.orderStatusData["orderStatus"] = jsonResponse["success"].stringValue
              self.total["grandtotal"] = jsonResponse["grandtotal"].stringValue
              print(self.selectedPaymentMethod)
              if self.selectedPaymentMethod.lowercased() == "Braintree".lowercased(){
                // self.getClientToken()
              }else{
                self.defaults.removeObject(forKey: "guestEmail");
                self.defaults.removeObject(forKey: "cartId");
                self.defaults.setValue("0", forKey: "items_count")
                self.view.makeToast("Order Placed Successfully.", duration: 2, position: .center, title: nil, image: nil, style: nil, completion: {
                  Void in
                  let storyboard = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil);
                  let viewController = storyboard.instantiateViewController(withIdentifier: "orderPlacedViewController") as! OrderPlacedViewController;
                  viewController.order_id = self.orderStatusData["orderId"]!
                  self.navigationController?.viewControllers = [viewController];
                })
              }
              //self.navigationController?.present(viewController, animated: true, completion: {});
            }
          }
        }
      }
      task.resume();
    }
  }
  
  func resetViewAndVariables(){
    stackView.subviews.forEach({ $0.removeFromSuperview() });
    
    products = [[String: String]]();
    options_selected = [String:[String:String]]();
    total = [String: String]();
    items_count = "";
    items_qty = "";
    allowed_guest_checkout = "";
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  func saveShippingAndPaymentOption(){
    if self.selectedPaymentMethod.lowercased() == "razorPay".lowercased(){
      
      
    }else if self.selectedPaymentMethod.lowercased() == "Braintree".lowercased() {
      
    }
    
  }
/*  func drop(inViewControllerDidCancel viewController: BTDropInViewController) {
    // ...
  } */
  
  
}
