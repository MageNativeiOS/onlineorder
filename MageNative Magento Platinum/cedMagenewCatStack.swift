/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */


import UIKit

class cedMagenewCatStack: MagenativeUIViewController,UIGestureRecognizerDelegate,KIImagePagerDelegate,KIImagePagerDataSource,UISearchBarDelegate {
    
    @IBOutlet weak var searchBar: UISearchBar!
    var serachedSubCategoryData = [[String:String]]()
    var searchedData =  [[String:String]]()
    var categoryMainData    = [Int:[[String:String]]]()
    var categoryId = String()
    var bannerArray = [[String:String]]()
    var product_array = [[String:String]]()
    var category_array = [[String:String]]()
    var banner_image_array = [String]()
    var selectedCategory = String()
    let bounds = UIScreen.main.bounds
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.basicFoundationToRenderView(topMargin: 110)
        searchBar.delegate = self
        self.sendRequest(url: "mobiconnectadvcart/category/getallcategories/theme/1/", params: nil)
        let colorString = Settings.themeColor
        self.searchBar.barTintColor = colorString
        self.sendScreenView(name: "Category")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func recieveResponse(data: Data?, requestUrl: String?, response: URLResponse?) {
        guard let json = try? JSON(data: data!) else{return;}
        if(requestUrl == "mobiconnectadvcart/category/getallcategories/theme/1/"){
            self.parse_product(json: json, index: "categories")
        }else{
            self.parse_product(json: json, index: "categories")
            print(json)
        }
        self.banner_image_array = [String]()
        for image in self.bannerArray{
            self.banner_image_array.append(image["banner_image"]!)
        }
        
        self.loadcateStack(category_array: category_array)
        
    }
    
    //Mark: SearchBar Functions
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchedData =  category_array.filter({ result in
            return  (result["category_name"]?.lowercased().contains(searchText.lowercased()))!
            
        })
         stackView.subviews.forEach({ $0.removeFromSuperview() });
        if(searchedData.count != 0){
        loadcateStack(category_array: searchedData)
        }else{
             self.loadcateStack(category_array: category_array)
        }
    }
    
    
    func loadcateStack(category_array:[[String:String]]){
        
        let banner = bannerCell()
        banner.heightAnchor.constraint(equalToConstant: 250).isActive = true;
        banner.bannerView.addKeneffect = true
        banner.bannerView.delegate = self
        banner.bannerView.dataSource = self
        banner.bannerView.slideshowTimeInterval = UInt(2.5)
        banner.bannerView.imageCounterDisabled = true
        banner.bannerView.indicatorDisabled = true
        stackView.addArrangedSubview(banner)
        self.setLeadingAndTralingSpaceFormParentView(banner, parentView:stackView)
        for item in category_array{
            let headingLabel = headinglabel()
            headingLabel.heightAnchor.constraint(equalToConstant: 50).isActive = true
            stackView.spacing = 10
            headingLabel.fontColorTool()
            headingLabel.categoryTitle.text = item["category_name"]
            cedMageImageLoader.shared.loadImgFromUrl(urlString: item["category_image"]!,completionHandler: { (image: UIImage?, url: String) in
                DispatchQueue.main.async {
                  
                }
            })
           // headingLabel.cardView()
            let horizonine = horizonLine()
            horizonine.heightAnchor.constraint(equalToConstant: 1).isActive = true;
            stackView.addArrangedSubview(horizonine)
            self.setLeadingAndTralingSpaceFormParentView(horizonine, parentView: stackView)
            stackView.addArrangedSubview(headingLabel)
            setLeadingAndTralingSpaceFormParentView(headingLabel, parentView: stackView)
            let cateId = item["categoryId"]
            let testing  = categoryMainData[Int(cateId!)!]!
            var subCount:Int
            if(testing.count/3 == 0){
                subCount = testing.count/3
            }else{
                subCount = (testing.count+1)/3
            }
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(cedMagenewCatStack.subcateTap(_:)));
             headingLabel.tag = Int(cateId!)!
             headingLabel.addGestureRecognizer(tapGesture)
            var i = 0
            for _ in 0..<subCount{
                let view = testView()
//                view.productOne.cardView()
//                view.productTwo.cardView()
               
//                view.productThree.cardView()
                    if testing[i]["sub_category_name"] != nil {
                          view.productname1.text = testing[i]["sub_category_name"]
                        cedMageImageLoader.shared.loadImgFromUrl(urlString: testing[i]["sub_category_image"]!,completionHandler: { (image: UIImage?, url: String) in
                            DispatchQueue.main.async {
                                view.productImg1.image = image
                            }
                        })
                         let tapGesture = UITapGestureRecognizer(target: self, action: #selector(cedMagenewCatStack.subcateTap(_:)));
                       view.productOne?.tag = Int(testing[i]["sub_category_id"]!)!
                        view.productOne.addGestureRecognizer(tapGesture)
                        
                    }else{
                        view.productImg1.isHidden = true
                        view.productname1.isHidden = true
                    }
                     if testing.count>i+1 && testing[i+1]["sub_category_name"] != nil {
                        view.productName2.text = testing[i+1]["sub_category_name"]
                        cedMageImageLoader.shared.loadImgFromUrl(urlString: testing[i+1]["sub_category_image"]!,completionHandler: { (image: UIImage?, url: String) in
                            DispatchQueue.main.async {
                                view.productImage2.image = image
                            }
                        })
                         let tapGesture = UITapGestureRecognizer(target: self, action: #selector(cedMagenewCatStack.subcateTap(_:)));
                         view.productTwo?.tag = Int(testing[i+1]["sub_category_id"]!)!
                          view.productTwo.addGestureRecognizer(tapGesture)
                     }else{
                        view.productImage2.isHidden = true
                        view.productName2.isHidden = true
                    }
                if testing.count>i+2 && testing[i+2]["sub_category_name"] != nil {
                    view.productName3.text = testing[i+2]["sub_category_name"]
                    cedMageImageLoader.shared.loadImgFromUrl(urlString: testing[i+2]["sub_category_image"]!,completionHandler: { (image: UIImage?, url: String) in
                        DispatchQueue.main.async {
                            view.productImage3.image = image
                        }
                    })
                     let tapGesture = UITapGestureRecognizer(target: self, action: #selector(cedMagenewCatStack.subcateTap(_:)));
                    view.productThree?.tag = Int(testing[i+2]["sub_category_id"]!)!
                      view.productThree.addGestureRecognizer(tapGesture)
                }else{
                    view.productImage3.isHidden = true
                    view.productName3.isHidden = true
                    }
                let cartProductListViewHeight = CGFloat(150.0);
                view.heightAnchor.constraint(equalToConstant: cartProductListViewHeight).isActive = true;
                stackView.spacing = 1
                let horizonine = horizonLine()
                horizonine.heightAnchor.constraint(equalToConstant: 1).isActive = true;
                stackView.addArrangedSubview(horizonine)
                self.setLeadingAndTralingSpaceFormParentView(horizonine, parentView: stackView)
                stackView.addArrangedSubview(view)
                self.setLeadingAndTralingSpaceFormParentView(view, parentView: stackView)
                   i += 3
              
               
            }
        }
        
    }
    
    //Mark: Tap gesture subcategoryCard Tap
    
    @objc func subcateTap(_ recognizer: UITapGestureRecognizer){
        let contol = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "productListingViewController") as! ProductListingViewController
        contol.categoryId = String(describing: recognizer.view!.tag)
        self.navigationController?.pushViewController(contol, animated: true)
    }
    
    //Mark :Touch up outside
    
   override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    

    
    //MARK: Delegate for KIImage Pager
    
    public func array(withImages pager: KIImagePager!) -> [Any]! {
        if banner_image_array.count == 0 {
            return ["placeholder" as AnyObject]
        }
        return banner_image_array as [AnyObject]?
    }
    
    
    func contentMode(forImage image: UInt, in pager: KIImagePager!) -> UIView.ContentMode {
        return UIView.ContentMode.scaleToFill
    }
    
    func placeHolderImage(for pager: KIImagePager!) -> UIImage! {
        return UIImage(named: "placeholder")
    }
    
    func imagePager(_ imagePager: KIImagePager!, didSelectImageAt index: UInt) {
        let banner = bannerArray[Int(index)]
        if(banner["link_to"] == "category"){
            let viewcontoller = UIStoryboard(name: "categorylayouts", bundle: nil).instantiateViewController(withIdentifier: "categoryTheme2") as? cedMageCategoryTheme2
            viewcontoller?.categoryId = banner["product_id"]!
            self.navigationController?.pushViewController(viewcontoller!, animated: true)
        }else if (banner["link_to"] == "product"){
            let productview = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "rootPageViewController") as! cedMageProductViewRoot
            productview.pageData = [["product_id":banner["product_id"]!]]
            let instance = cedMage.singletonInstance
            instance.storeParameterInteger(parameter: 0)
            self.navigationController?.pushViewController(productview
                , animated: true)
            
        }else if (banner["link_to"] == "website"){
            let view = UIStoryboard(name: "cedMageAccounts", bundle: nil)
            let viewControl = view.instantiateViewController(withIdentifier: "cmsWebView") as! cedMageCmsWebView
            let url = banner["product_id"]
            viewControl.pageUrl = url!
            self.navigationController?.pushViewController(viewControl, animated: true)
            
            
        }
    }
    
    
    
    func parse_product(json:JSON,index:String){
        if(json["data"]["banner"].arrayValue.count > 0){
            for result in json["data"]["banner"].arrayValue {
                let product_id = result["product_id"].stringValue
                let id = result["id"].stringValue
                let title = result["title"].stringValue
                let link_to = result["link_to"].stringValue
                let banner_image = result["banner_image"].stringValue
                let products_obj = ["product_id":product_id, "title":title,"link_to":link_to,"banner_image":banner_image,"id":id]
                bannerArray.append(products_obj)
            }
        }
        //  print(json["data"][index].arrayValue)
        var i = 0
        for (_,subJson) in json["data"]["categories"][0] {
            
            let maincatename = subJson["main_category_name"].stringValue
            let maincateimage = subJson["main_category_image"].stringValue
            let maincateId = subJson["main_category_id"].stringValue
            category_array.append(["category_name":maincatename,"category_image":maincateimage,"categoryId":maincateId])
            
            //print(subJson["sub_cats"][0])
            var temp = [[String:String]]()
            for (_,subcategory)  in subJson["sub_cats"]{
                
                var tem2 = [String:String]()
                tem2["sub_category_id"] = subcategory["sub_category_id"].stringValue
                tem2["sub_category_name"] = subcategory["sub_category_name"].stringValue
                tem2["sub_category_image"] = subcategory["sub_category_image"].stringValue
                temp.append(tem2)
                
                
            }
            
            categoryMainData[Int(maincateId)!] = temp
            temp.removeAll()
            i += 1
            //Do something you want
        }
        
        
        if(json["data"]["product"].arrayValue.count > 0){
            
            for result in json["data"]["product"].arrayValue {
                let product_id = result["product_id"].stringValue
                let product_name = result["product_name"].stringValue
                let product_price = result["product_price"].stringValue
                let product_image = result["product_image"].stringValue
                let products_obj = ["product_id":product_id, "product_name":product_name,"product_image":product_image,"product_price":product_price]
                product_array.append(products_obj)
                
            }
        }
        return
    }
    
    
    
}
