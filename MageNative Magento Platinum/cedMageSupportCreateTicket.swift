//
//  cedMageSupportCreateTicket.swift
//  ZeoMarket
//
//  Created by cedcoss on 02/04/19.
//  Copyright © 2019 MageNative. All rights reserved.
//

import UIKit
import Photos

class cedMageSupportCreateTicket: cedMageViewController {
  
  @IBOutlet weak var tableView: UITableView!
  var dropDown = DropDown()
  var departments = [[String:String]]()
  var departmentValues = [String]()
  var priorities  = [[String:String]]()
  var priorityValues = [String]()
  var params = [String:String]()
  var orderIds = [String]()
  var browseByRows = Int()
  var fileExtension=".pdf"
  let picker = UIImagePickerController()
  var isDoc=false
  var attachmentNameArray = [String]()
  var totalSize = Double()
  var imageData = ""
  var imagesData = [[String:String]]()
  var img=[UIImage]()
  
  var pdfData = NSString()
  override func viewDidLoad() {
    super.viewDidLoad()
    tableView.delegate = self
    tableView.dataSource = self
    tableView.tableFooterView = UIView()
    var param = [String:String]()
    let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
    
    if let cust_id = userInfoDict["customerId"] {
      param["customer_id"] = cust_id
      params["customer_id"] = cust_id
    }
    self.sendRequest(url: "helpdeskapi/getallfieldoptions", params: param)
    //  self.getRequest(url: "helpdeskapi/getallfieldoptions", store: false)
  }
  
  
  override func recieveResponse(data: Data?, requestUrl: String?, response: URLResponse?) {
    if let data = data {
      do {
        var json = try JSON(data:data)
        json = json[0]
        print(NSString(data:data,encoding:String.Encoding.utf8.rawValue))
        print(json)
        if requestUrl == "helpdeskapi/createticket" {
          if json["status"].stringValue == "true" {
            cedMageHttpException.showAlertView(me: self, msg: json["message"].stringValue, title: "Success".localized)
          }else {
            cedMageHttpException.showAlertView(me: self, msg: json["message"].stringValue, title: "Error".localized)
          }
          
        }else {
          if json["success"].stringValue == "true" {
            
            
            for department in json["departments"].arrayValue {
              
              self.departmentValues.append(department["label"].stringValue)
              self.departments.append(["label":department["label"].stringValue,"value":department["value"].stringValue])
            }
            
            for priority in json["priorities"].arrayValue {
              self.priorityValues.append(priority["label"].stringValue)
              self.priorities.append(["label":priority["label"].stringValue,"value":priority["value"].stringValue])
            }
            if json["orders"].count > 0 {
              for order in json["orders"].arrayValue {
                self.orderIds.append(order["label"].stringValue)
              }
            }
            
            self.tableView.reloadData()
          }else {
            cedMageHttpException.showAlertView(me: self, msg: json["message"].stringValue, title: "Error".localized)
          }
        }
      }catch let error {
        print(error.localizedDescription)
      }
    }
  }
  
}


extension cedMageSupportCreateTicket:UITableViewDelegate,UITableViewDataSource {
  
  func numberOfSections(in tableView: UITableView) -> Int {
    return 4
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    switch section  {
    case 0:
      if(!defaults.bool(forKey: "isLogin")){
        return 2
      }
      return 0
    case 1:
      return 5
      
    case 2:
      return browseByRows+1
    default:
      return 1
    }
    
  }
  
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    switch indexPath.section {
    case 0 :
      switch indexPath.row {
      case 0:
        if let cell = tableView.dequeueReusableCell(withIdentifier: "supportnamecell") as? cedMageSupportSubjectCell {
          cell.textField.delegate = self
          return cell
        }
      default:
        if let cell = tableView.dequeueReusableCell(withIdentifier: "supportemailcell") as? cedMageSupportSubjectCell {
          cell.textField.delegate = self
          return cell
        }
      }
    case 1:
      switch indexPath.row {
      case 0:
        if let cell = tableView.dequeueReusableCell(withIdentifier: "supportdepartmentcell") as? cedMageSupportDepartmentCell {
          cell.departmentButton.addTarget(self, action: #selector(self.departmentlisting), for: .touchUpInside)
          return cell
        }
      case 1:
        if let cell = tableView.dequeueReusableCell(withIdentifier: "supportprioritycell") as? cedMageSupportpriorityCell {
          cell.priorityListButton.addTarget(self, action: #selector(self.departmentPriority), for: .touchUpInside)
          return cell
        }
      case 2:
        if let cell = tableView.dequeueReusableCell(withIdentifier: "supportcell") as? cedMageSupportSubjectCell {
          cell.textField.delegate = self
          return cell
        }
      case 3:
        if let cell = tableView.dequeueReusableCell(withIdentifier: "supportordercell") as? cedMageSupportOrderCell  {
          cell.orderButtonlist.addTarget(self, action: #selector(self.orderListing(sender:)), for: .touchUpInside)
          return cell
        }
      case 4:
        if let cell = tableView.dequeueReusableCell(withIdentifier: "supportmsgcell") as? cedMageSupportMsgCell  {
          cell.msgTextView.delegate = self
          return cell
        }
      default:
        if let cell = tableView.dequeueReusableCell(withIdentifier: "")  {
          return cell
        }
      }
    case 2:
      switch indexPath.row {
      case 0 :
        if let cell = tableView.dequeueReusableCell(withIdentifier: "supportAddAttach")  {
          if let button = cell.viewWithTag(325) as? UIButton {
            button.setThemeColor()
            button.addTarget(self, action: #selector(self.addAttachmentCell(sender:)), for: .touchUpInside)
          }
          return cell
        }
      default:
        if let cell = tableView.dequeueReusableCell(withIdentifier: "brwoseCell")  {
          if  let browseBy =   cell.viewWithTag(241) as? UIButton {
            
            browseBy.addTarget(self , action: #selector(self.browseButtonPressed(sender:)), for: .touchUpInside)
            //                        if img.count > 0 {
            //                            browseBy.setTitle("\(img[indexPath.row - 1])", for: .normal)
            //                        }
            if let attachmentName = cell.viewWithTag(242) as? UILabel {
              
              //                            if indexPath.row == 1 {
              //                                if imagesData.count > 0{
              //                                    attachmentName.text = imagesData[indexPath.row - 1]["name"]
              //                                }else{
              //                                    attachmentName.text = "No File Attached"
              //                                }
              //                            }else if indexPath.row == 2 {
              //                                if imagesData.count > 1{
              //                                    attachmentName.text = imagesData[indexPath.row - 1]["name"]
              //                                }else{
              //                                    attachmentName.text = "No File Attached"
              //                                }
              //                            }else if indexPath.row == 3 {
              //                                if imagesData.count > 2{
              //                                    attachmentName.text = imagesData[indexPath.row - 1]["name"]
              //                                }else{
              //                                    attachmentName.text = "No File Attached"
              //                                }
              //                            }
              
              //                            if imagesData.count == 0 {
              //                            }else{
              //                                if indexPath.row == browseByRows {
              //                                    if imagesData.count > indexPath.row - 1{
              //                                        attachmentName.text = imagesData[indexPath.row - 1]["name"]
              //                                    }
              //                                }else{
              //                                    attachmentName.text = imagesData[indexPath.row - 1]["name"]
              //                                }
              //                            }
              
              
            }
          }
          
          if let removeBtn = cell.viewWithTag(317) as? UIButton {
            removeBtn.tag = indexPath.row
            removeBtn.addTarget(self, action: #selector(removeAttachmentCell(_:)), for: .touchUpInside)
          }
          cell.tag = 1001
          return cell
        }
      }
      
      
    default:
      if let cell = tableView.dequeueReusableCell(withIdentifier: "submitButtonCell")  {
        if let submitButton  = cell.viewWithTag(2250) as? UIButton  {
          submitButton.addTarget(self, action: #selector(self.submitForm(sender:)), for: .touchUpInside)
          submitButton.setThemeColor()
        }
        return cell
      }
    }
    
    
    return UITableViewCell()
  }
  
//  func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
//    if editingStyle == .delete {
//      tableView.deleteRows(at: [indexPath], with: .right)
//    }
//  }
  
  @objc func addAttachmentCell(sender:UIButton) {
    browseByRows += 1
    print(browseByRows)
    let indexPath = IndexPath(row: browseByRows, section: 2)
    tableView.insertRows(at: [indexPath], with: .left)
  }
  
  @objc func removeAttachmentCell(_ sender: UIButton) {
    print(sender.tag)
    let indexPath = IndexPath(row: sender.tag, section: 2)
    browseByRows -= 1
    print(browseByRows)
    tableView.deleteRows(at: [indexPath], with: .right)
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    switch indexPath.section {
    case 1:
      if indexPath.row == 3 {
        if(!defaults.bool(forKey: "isLogin")){
          return 0
        }
      }
      return UITableView.automaticDimension
    default:
      return UITableView.automaticDimension
    }
    
    
  }
  
  
  @objc func departmentlisting(sender:UIButton){
    dropDown.dataSource = departmentValues
    dropDown.selectionAction = {(index, item) in
      sender.setTitle(item, for: .normal);
      self.params["department"] = item
    }
    dropDown.anchorView = sender
    dropDown.bottomOffset = CGPoint(x: 0, y:sender.bounds.height)
    if dropDown.isHidden {
      _=dropDown.show();
    } else {
      dropDown.hide();
    }
  }
  
  @objc func orderListing(sender:UIButton){
    dropDown.dataSource = self.orderIds
    dropDown.selectionAction = {(index, item) in
      sender.setTitle(item, for: .normal);
      self.params["order_id"] = item
    }
    dropDown.anchorView = sender
    dropDown.bottomOffset = CGPoint(x: 0, y:sender.bounds.height)
    if dropDown.isHidden {
      _=dropDown.show();
    } else {
      dropDown.hide();
    }
  }
  
  
  
  @objc func departmentPriority(sender:UIButton){
    dropDown.dataSource = priorityValues
    dropDown.selectionAction = {(index, item) in
      sender.setTitle(item, for: .normal);
      self.params["priority"] = item
    }
    dropDown.anchorView = sender
    dropDown.bottomOffset = CGPoint(x: 0, y:sender.bounds.height)
    if dropDown.isHidden {
      _=dropDown.show();
    } else {
      dropDown.hide();
    }
  }
  
  
  
  @objc  func submitForm(sender:UIButton){
    var imgDatas="["
    if imagesData.count != 0{
      for i in 0..<imagesData.count{
        if(imagesData[i] != [String:String]())
        {
          imgDatas+=imagesData[i].convtToJson() as String
          if i != imagesData.count-1{
            imgDatas+=","
          }
        }
        
      }
    }
    if  imgDatas.last == "," {
      imgDatas =  imgDatas.removeLast().description
    }
    imgDatas+="]"
    print(imgDatas)
    params["attachments"] = imgDatas
    self.sendRequest(url: "helpdeskapi/createticket", params: params,store: false)
    
  }
  
  
  
  //    @nonobjc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
  //
  ////        let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)
  //
  //        //Showing image in imageview after image is selected in ImagePickerController
  //
  //
  //        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
  //
  //            img.append(image)
  //            let imgData=UIImagePNGRepresentation(image)
  //            imageData=(imgData?.base64EncodedString())!
  //
  //            imagesData.append(["type":"image/png", "name":"xyz.png", "base64_encoded_data": self.imageData])
  //            tableView.reloadSections(NSIndexSet(index: 2) as IndexSet, with: .bottom)
  //
  //        }
  //
  //
  //
  //        //Removing imagePickerController after selecting image
  //        self.dismiss(animated: true, completion: nil);
  //    }
  
  
  
  //    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
  //        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
  //
  //            img.append(image)
  //            let imgData=UIImagePNGRepresentation(image)
  //            imageData=(imgData?.base64EncodedString())!
  //            let values = info[UIImagePickerControllerReferenceURL] as? NSURL
  //            let imgName = values?.lastPathComponent
  //            let image = info[UIImagePickerControllerOriginalImage] as? UIImage
  //            let imgsize:NSData = UIImagePNGRepresentation(image!)! as NSData
  //            let fileSize = Double(imgsize.length / 1024 / 1024)
  //            totalSize += fileSize
  //            if totalSize > 5.00 {
  //                picker.dismiss(animated: true, completion: nil)
  //                self.view.makeToast("Please upload a file of less than 5 Mb", duration: 2.0, position: .center)
  //            }else{
  //                imagesData.append(["name":imgName ?? "xyz.png", "base64_encoded_data": self.imageData])
  //                tableView.reloadSections(NSIndexSet(index: 2) as IndexSet, with: .bottom)
  //                picker.dismiss(animated: true, completion: nil)
  //            }
  //
  //        }
  //
  //    }
  
  
  
  @objc func browseButtonPressed(sender:UIButton)
  {
    let actionSheetControllerIOS8: UIAlertController = UIAlertController(title: "Please select", message: nil, preferredStyle: .actionSheet)
    
    let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { _ in
      print("Cancel")
    }
    actionSheetControllerIOS8.addAction(cancelActionButton)
    
    let saveActionButton = UIAlertAction(title: "Upload Image", style: .default)
    { _ in
      print("upload image")
      self.image_picker()
    }
    actionSheetControllerIOS8.addAction(saveActionButton)
    
    let uploadpdf = UIAlertAction(title: "Upload Document", style: .default)
    { _ in
      print("Delete")
      self.browseButtonPressed()
    }
    actionSheetControllerIOS8.addAction(uploadpdf)
    self.present(actionSheetControllerIOS8, animated: true, completion: nil)
  }
  
  
  func image_picker()
  {
    isDoc=true
    print("browseButtonPressed")
    picker.delegate=self
    picker.allowsEditing = false
    picker.sourceType = .photoLibrary
    self.present(picker, animated: true, completion: nil)
  }
  //    [String(kUTTypePDF), String(kUTTypeImage), String(kUTTypeArchive)
  func browseButtonPressed(){
    let doc=UIDocumentMenuViewController(documentTypes: ["public.text","public.content","public.image","public.data"], in: .import)
    //        let doc = UIDocumentMenuViewController(documentTypes: [String(kUTTypeImage),String(kUTTypePDF),String(kUTTypeArchive)], in: .import)
    doc.delegate=self
    doc.modalPresentationStyle = .popover
    self.present(doc, animated: true, completion: nil)
  }
  
  func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
    if let image = info[.originalImage] as? UIImage {
      
      img.append(image)
      let imgData=image.pngData()
      imageData=(imgData?.base64EncodedString())!
      let values = info[UIImagePickerController.InfoKey.referenceURL] as? NSURL
      let imgName = values?.lastPathComponent
      let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
      let imgsize:NSData = image!.pngData()! as NSData
      let fileSize = Double(imgsize.length / 1024 / 1024)
      totalSize += fileSize
      if totalSize > 5.00 {
        picker.dismiss(animated: true, completion: nil)
        self.view.makeToast("Please upload a file of less than 5 Mb", duration: 2.0, position: .center)
      }else{
        imagesData.append(["name":imgName ?? "xyz.png", "base64_encoded_data": self.imageData])
        let ip = IndexPath(row: browseByRows, section: 2)
        //                            let cell = tableView.viewWithTag(1001) as! UITableViewCell
        let cell = tableView.cellForRow(at: ip)
        let label = cell!.viewWithTag(242) as! UILabel
        label.text = imgName
        print(label.text)
        tableView.reloadSections(NSIndexSet(index: 2) as IndexSet, with: .bottom)
        picker.dismiss(animated: true, completion: nil)
      }
    }
  }
}


extension cedMageSupportCreateTicket:UITextFieldDelegate,UITextViewDelegate{
  func textFieldDidEndEditing(_ textField: UITextField) {
    if textField.tag == 255 {
      self.params["name"] = textField.text
    }else if textField.tag == 260 {
      self.params["email"] = textField.text
    }else {
      self.params["subject"] = textField.text
    }
  }
  
  func textViewDidEndEditing(_ textView: UITextView) {
    self.params["message"] = textView.text
  }
}


extension cedMageSupportCreateTicket:UIDocumentPickerDelegate,UIDocumentInteractionControllerDelegate,UIDocumentMenuDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
  
  func documentMenu(_ documentMenu: UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
    documentPicker.delegate=self
    documentPicker.modalPresentationStyle = .fullScreen
    self.present(documentPicker, animated: true, completion: nil)
    print("didPickDocumentPicker")
  }
  
  
  func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
    
    let fileName=url.absoluteString.components(separatedBy: "/")
    // docUrlLabel.text=url.absoluteString
    print("didPickDocumentAt")
    // FileSelectedOrNot.text = fileName[0]
    
    let isSecuredURL = url.startAccessingSecurityScopedResource() == true
    let coordinator = NSFileCoordinator()
    var error: NSError? = nil
    coordinator.coordinate(readingItemAt: url, options: [], error: &error) { (url) -> Void in
      do{
        let temp=try Data(contentsOf: url)
        //self.trainingDocData=temp.base64EncodedString()
        
        let temp1=fileName[fileName.count-1].components(separatedBy: ".")
        print("The file you have choosed is not in correct format")
        //  print(temp1)
        if (temp1[1] == "pdf" || temp1[1] == "zip" )
        {
          print(temp1)
          let dataPdf = temp.base64EncodedString()
          self.imagesData.append(["name":"xyz.pdf", "base64_encoded_data": dataPdf])
          let cell = tableView.viewWithTag(1001) as! UITableViewCell
          let label = cell.viewWithTag(242) as! UILabel
          label.text = "xyz.pdf"
          tableView.reloadSections(NSIndexSet(index: 2) as IndexSet, with: .bottom)
        }
        else
        {
          let msg = "The file you have choosed is not in correct format"
          print(msg)
          return
          
        }
      }catch let error{
        
        print(error.localizedDescription)
        
      }
    }
    if (isSecuredURL) {
      url.stopAccessingSecurityScopedResource()
    }
    print(url)
    controller.dismiss(animated: true, completion: nil)
  }
  
  
  
  
  
  
  func documentMenuWasCancelled(_ documentMenu: UIDocumentMenuViewController) {
    print("documentMenuWasCancelled")
    documentMenu.dismiss(animated: true, completion: nil)
  }
  
  func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
    print("documentPickerWasCancelled")
    controller.dismiss(animated: true, completion: nil)
  }
  
  
  
  
  func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
    picker.dismiss(animated: true, completion: nil)
  }
  
  
  //MARK: URLSessionDownloadDelegate
  // 1
  func urlSession(_ session: URLSession,
                  downloadTask: URLSessionDownloadTask,
                  didFinishDownloadingTo location: URL)
  {
    
    let path = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
    let documentDirectoryPath:String = path[0]
    let fileManager = FileManager()
    let destinationURLForFile = URL(fileURLWithPath: documentDirectoryPath.appendingFormat("/file."+fileExtension))
    
    if fileManager.fileExists(atPath: destinationURLForFile.path){
      showFileWithPath(path: destinationURLForFile.path)
    }
    else{
      do {
        try fileManager.moveItem(at: location, to: destinationURLForFile)
        // show file
        print(destinationURLForFile.path)
        showFileWithPath(path: destinationURLForFile.path)
      }catch{
        print("An error occurred while moving file to destination url")
      }
    }
  }
  // 2
  func urlSession(_ session: URLSession,
                  downloadTask: URLSessionDownloadTask,
                  didWriteData bytesWritten: Int64,
                  totalBytesWritten: Int64,
                  totalBytesExpectedToWrite: Int64){
    print("asdfgh")
  }
  
  //MARK: URLSessionTaskDelegate
//  func urlSession(_ session: URLSession,
//                  task: URLSessionTask,
//                  didCompleteWithError error: Error?){
//    //        downloadTask = nil
//    if (error != nil) {
//      print(error!.localizedDescription)
//      cedMageHttpException.showAlertView(me: self, msg:  "Error", title: "Can't Download in Process")
//
//    }else{
//      print("The task finished transferring data successfully")
//    }
//  }
  
  //MARK: UIDocumentInteractionControllerDelegate
  func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController
  {
    UINavigationBar.appearance().barTintColor = UIColor.black
    UINavigationBar.appearance().tintColor = UIColor.black
    UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.bold)]
    return self
  }
  
  
  func showFileWithPath(path: String){
    let isFileFound:Bool? = FileManager.default.fileExists(atPath: path)
    if isFileFound == true{
      let viewer = UIDocumentInteractionController(url: URL(fileURLWithPath: path))
      
      viewer.delegate = self
      viewer.presentPreview(animated: true)
    }
  }
  
  
  
  // Helper function inserted by Swift 4.2 migrator.
  //    fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
  //
  //        return Dictionary(uniqueKeysWithValues: input.map {key,value in (key.rawValue, value)})
  //    }
  //
  //    // Helper function inserted by Swift 4.2 migrator.
  //    fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
  //        let view = input.
  //        return input
  //    }
}
extension Collection where Iterator.Element == [String:String] {
  func toJSONString(options: JSONSerialization.WritingOptions = .prettyPrinted) -> String {
    if let arr = self as? [[String:AnyObject]],
      let dat = try? JSONSerialization.data(withJSONObject: arr, options: options),
      let str = String(data: dat, encoding: String.Encoding.utf8) {
      return str
    }
    return "[]"
  }
}
