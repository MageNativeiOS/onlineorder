//
//  enc.h
//  Assignment
//
//  Created by Manohar Singh Rawat on 17/02/21.
//  Copyright © 2021 ab. All rights reserved.
//
#import <Foundation/Foundation.h>
#import <CommonCrypto/CommonCrypto.h>

@interface NSData (PBEEncryption)
- (NSData *)encryptPBEWithMD5AndDESUsingPassword:(NSData *)password;
- (NSData *)encryptPBEWithMD5AndDESUsingPassword:(NSData *)password salt:(NSData *)salt iterations:(NSUInteger)iterations;
-(NSData*) cryptPBEWithMD5AndDES:(CCOperation)op usingData:(NSData*)data withPassword:(NSString*)password andSalt:(NSData*)salt andIterating:(int)numIterations;
@end
