/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */


import UIKit

class cedMageDefaultCategory: cedMageViewController,UITableViewDelegate,UITableViewDataSource,KIImagePagerDelegate,KIImagePagerDataSource {
 
  
 

    @IBOutlet weak var catDefaultTable: UITableView!
    var bannerArray = [[String:String]]()
    var product_array = [[String:String]]()
    var category_array = [[String:String]]()
    var banner_image_array = [String]()
    var selectedCategory = String()
    let bounds = UIScreen.main.bounds
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        clearFilterRelatedData();
        catDefaultTable.delegate = self
        catDefaultTable.dataSource = self
        if(selectedCategory != ""){
           // self.sendRequest(url: "mobiconnect/category/getmaincategories?id=\(selectedCategory)", params: nil)
        }else{
            //self.sendRequest(url: "mobiconnect/category/getmaincategories", params: nil)
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func recieveResponse(data: Data?, requestUrl: String?, response: URLResponse?) {
        guard let json = try? JSON(data: data!) else {
            return;
        }
        if(selectedCategory != ""){
            self.parse_product(json: json, index: "subcategory")
        }else{
            self.parse_product(json: json, index: "categories")
        }
        self.banner_image_array = [String]()
        for image in self.bannerArray{
            self.banner_image_array.append(image["banner_image"]!)
        }
        print(json)
        catDefaultTable.reloadData()
    }
    
    //MARK: Delegate Function for TableView
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(section == 0){
            if(bannerArray.count == 0 && product_array.count == 0){
                return 0
            }
            return 1
        }
        return category_array.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.section == 0){
            
            if(indexPath.row == 0 && selectedCategory != "" && product_array.count != 0){
                let cell = catDefaultTable.dequeueReusableCell(withIdentifier: "direct") as! cate_product_cell
                    cell.viewcontrol = self
                    cell.product_array = self.product_array
                    cell.cat_id = selectedCategory
                return cell
            }
            let cell = catDefaultTable.dequeueReusableCell(withIdentifier: "bannerCell") as! homePageBannerCell
            
            cell.banner_image_array = banner_image_array
            cell.bannerPage.delegate = self
            cell.bannerPage.dataSource = self
            cell.bannerPage.slideshowTimeInterval = UInt(2.5)
            cell.bannerPage.imageCounterDisabled = true
            return cell
            
        }else{
            
            
       let cell = catDefaultTable.dequeueReusableCell(withIdentifier: "categoryCell") as! cedMageDefaultCatecell
        cell.colordBackground.backgroundColor = UIColor.black//cedMage.getRandomColor()
            cell.categoryName.fontColorTool()
        if(category_array[indexPath.row]["category_count"] != nil){
          cell.categoryName.text = category_array[indexPath.row]["category_name"]!+"\n"+category_array[indexPath.row]["category_count"]!
            
        }else{
             cell.categoryName.text = category_array[indexPath.row]["category_name"]
        }
        
        cedMageImageLoader.shared.loadImgFromUrl(urlString: category_array[indexPath.row]["category_image"]!,completionHandler: { (image: UIImage?, url: String) in
            DispatchQueue.main.async {
                cell.categoryImage.image = image
            }
            
        })
        return cell
        }
    }
    

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(indexPath.section == 0){
            return bounds.height/3
        }
        return 100
    }
   
    //MARK: Delegate for KIImage Pager
    
    public func array(withImages pager: KIImagePager!) -> [Any]! {
        if banner_image_array.count == 0 {
            return ["placeholder" as AnyObject]
        }
        return banner_image_array as [AnyObject]?
    }

   /* func array(withImages pager: KIImagePager!) -> [AnyObject]! {
        if banner_image_array.count == 0 {
            return ["placeholder" as AnyObject]
        }
        return banner_image_array as [AnyObject]!
    }*/
    
    func contentMode(forImage image: UInt, in pager: KIImagePager!) -> UIView.ContentMode {
        return UIView.ContentMode.scaleToFill
    }
    
    func placeHolderImage(for pager: KIImagePager!) -> UIImage! {
        return UIImage(named: "placeholder")
    }
    
    func imagePager(_ imagePager: KIImagePager!, didSelectImageAt index: UInt) {
        
    }
    
    
    func parse_product(json:JSON,index:String){
        if(json["data"]["banner"].arrayValue.count > 0){
            
            for result in json["data"]["banner"].arrayValue {
                let product_id = result["product_id"].stringValue
                let id = result["id"].stringValue
                let title = result["title"].stringValue
                let link_to = result["link_to"].stringValue
                let banner_image = result["banner_image"].stringValue
                let products_obj = ["prod_id":product_id, "title":title,"link_to":link_to,"banner_image":banner_image,"id":id]
                bannerArray.append(products_obj)
            }
        }
        for result in json["data"][index].arrayValue {
            let category_id = result["category_id"].stringValue
          
            let category_name = result["category_name"].stringValue
            let has_child = result["has_child"].stringValue
        
            let category_image = result["category_image"].stringValue
            let category_count = result["product_count"].stringValue
            let products_obj = ["category_id":category_id, "category_name":category_name,"category_image":category_image,"has_child":has_child,"category_count":category_count]
          
            category_array.append(products_obj)
            
        }
        if(json["data"]["product"].arrayValue.count > 0){
            
            for result in json["data"]["product"].arrayValue {
                let product_id = result["product_id"].stringValue
                let product_name = result["product_name"].stringValue
                let product_price = result["product_price"].stringValue
                let product_image = result["product_image"].stringValue
                let products_obj = ["product_id":product_id, "product_name":product_name,"product_image":product_image,"product_price":product_price]
                product_array.append(products_obj)
                
            }
        }
        return
    }


    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selected_cat = category_array[indexPath.row]
        if(indexPath.section == 1){
            if(selectedCategory == ""){
                
                if(selected_cat["has_child"]=="NO"){
                    var selected_cat = [String:String]()
                    if(product_array.count != 0){
                        selected_cat = category_array[indexPath.row]
                    }else{
                        selected_cat = category_array[indexPath.row]
                    }
                    
                         let contol = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "productListingViewController") as! ProductListingViewController
                     contol.categoryId = selected_cat["category_id"]!
                     self.navigationController?.pushViewController(contol, animated: true)
                    
                }else{
                    let view = storyboard?.instantiateViewController(withIdentifier: "defaultCategory") as! cedMageDefaultCategory
                    view.selectedCategory = selected_cat["category_id"]!
                    view.navigationItem.title = selected_cat["category_name"]
                    self.navigationController?.pushViewController(view, animated: true)
                }
            }
            else{
               
                let contol =  UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "productListingViewController") as! ProductListingViewController
                contol.categoryId = selected_cat["category_id"]!
                self.navigationController?.pushViewController(contol, animated: true)
                
            }
        }
        
    }

    
    
}
