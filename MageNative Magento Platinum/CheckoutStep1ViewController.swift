/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */

import UIKit

class CheckoutStep1ViewController: MagenativeUIViewController {
    
    @IBOutlet weak var imageViewOnTop: UIImageView!
    @IBOutlet weak var imageViewOnTopHeight: NSLayoutConstraint!
    
    var checkoutAs = "";
    var isPasswordVisible = false;
    var allowed_guest_checkout = "";
    var email_id = "";
    
    //Order data
    var totalOrderData = [String:String]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.basicFoundationToRenderView(bottomMargin: CGFloat(0.0));
        self.renderCheckoutStep1View();
    }
    
    func renderCheckoutStep1View(){
        
        self.makeSomeSpaceInStackView(height: CGFloat(0.0));
        let selectLoginTypeForCheckoutView = SelectLoginTypeForCheckoutView();
        selectLoginTypeForCheckoutView.translatesAutoresizingMaskIntoConstraints = false;
//        selectLoginTypeForCheckoutView.leftSideButton.layer.borderWidth =  CGFloat(1);
//        selectLoginTypeForCheckoutView.leftSideButton.layer.borderColor = UIColor(hexString: "#3498DB")?.cgColor;
        selectLoginTypeForCheckoutView.leftSideButton.makeCornerRounded(cornerRadius: translateAccordingToDevice(CGFloat(5)));

        selectLoginTypeForCheckoutView.rightSideButton.makeCornerRounded(cornerRadius: translateAccordingToDevice(CGFloat(5)));
        
        
        selectLoginTypeForCheckoutView.leftSideLabel.font = UIFont(fontName: "HelveticaNeue-Medium", fontSize: CGFloat(13.0));
        selectLoginTypeForCheckoutView.leftSideLabel.text = "Proceed As Guest".localized;
//        selectLoginTypeForCheckoutView.leftSideLabel.fontColorTool()
        selectLoginTypeForCheckoutView.leftSideButton.addTarget(self, action: #selector(CheckoutStep1ViewController.leftSideButtonPressed(_:)), for: UIControl.Event.touchUpInside);
        selectLoginTypeForCheckoutView.leftSideInnerButton.addTarget(self, action: #selector(CheckoutStep1ViewController.leftSideButtonPressed(_:)), for: UIControl.Event.touchUpInside);
        
        selectLoginTypeForCheckoutView.rightSideLabel.font = UIFont(fontName: "HelveticaNeue-Medium", fontSize: CGFloat(13.0));
        selectLoginTypeForCheckoutView.rightSideLabel.text = "Proceed As User".localized;
//        selectLoginTypeForCheckoutView.rightSideLabel.fontColorTool()
        selectLoginTypeForCheckoutView.rightSideButton.addTarget(self, action: #selector(CheckoutStep1ViewController.rightSideButtonPressed(_:)), for: UIControl.Event.touchUpInside);
        selectLoginTypeForCheckoutView.rightSideInnerButton.addTarget(self, action: #selector(CheckoutStep1ViewController.rightSideButtonPressed(_:)), for: UIControl.Event.touchUpInside);
        
        stackView.addArrangedSubview(selectLoginTypeForCheckoutView);
        let selectLoginTypeForCheckoutViewHeight = translateAccordingToDevice(CGFloat(40.0));
        selectLoginTypeForCheckoutView.heightAnchor.constraint(equalToConstant: selectLoginTypeForCheckoutViewHeight).isActive = true;
        self.setLeadingAndTralingSpaceFormParentView(selectLoginTypeForCheckoutView, parentView:stackView);
        
        // not allowing guest checkout
        if(allowed_guest_checkout != "1"){
            selectLoginTypeForCheckoutView.leftSideButton.isHidden = true;
        }
        
        
        let textFieldHeight = translateAccordingToDevice(CGFloat(40.0));
        
        let textFieldEmail = UITextField();
        textFieldEmail.borderStyle = UITextField.BorderStyle.roundedRect;
        textFieldEmail.translatesAutoresizingMaskIntoConstraints = false;
        textFieldEmail.text = "textFieldEmail";
//        textFieldEmail.fontColorTool()
        textFieldEmail.font = UIFont(fontName: "", fontSize: CGFloat(15.0));
        stackView.addArrangedSubview(textFieldEmail);
        textFieldEmail.heightAnchor.constraint(equalToConstant: textFieldHeight).isActive = true;
        self.setLeadingAndTralingSpaceFormParentView(textFieldEmail, parentView:stackView);
        textFieldEmail.isHidden = true;
        
        let textFieldPassword = UITextField();
        textFieldPassword.borderStyle = UITextField.BorderStyle.roundedRect;
        textFieldPassword.translatesAutoresizingMaskIntoConstraints = false;
        textFieldPassword.font = UIFont(fontName: "", fontSize: CGFloat(15.0));
        stackView.addArrangedSubview(textFieldPassword);
        textFieldPassword.heightAnchor.constraint(equalToConstant: textFieldHeight).isActive = true;
        self.setLeadingAndTralingSpaceFormParentView(textFieldPassword, parentView:stackView);
        textFieldPassword.isHidden = true;
        textFieldPassword.isSecureTextEntry = true;
        
        let showPasswordAndRegisterView = SelectLoginTypeForCheckoutView();
        showPasswordAndRegisterView.translatesAutoresizingMaskIntoConstraints = false;
        showPasswordAndRegisterView.leftSideButton.addTarget(self, action: #selector(CheckoutStep1ViewController.showHidePasswordPressed(_:)), for: UIControl.Event.touchUpInside);
        showPasswordAndRegisterView.leftSideInnerButton.addTarget(self, action: #selector(CheckoutStep1ViewController.showHidePasswordPressed(_:)), for: UIControl.Event.touchUpInside);
        
        showPasswordAndRegisterView.rightSideButton.addTarget(self, action: #selector(CheckoutStep1ViewController.goToRegistrationPage(_:)), for: UIControl.Event.touchUpInside);
        showPasswordAndRegisterView.rightSideInnerButton.addTarget(self, action: #selector(CheckoutStep1ViewController.goToRegistrationPage(_:)), for: UIControl.Event.touchUpInside);
        
        showPasswordAndRegisterView.leftSideLabel.text = "Show Password".localized;
//        showPasswordAndRegisterView.leftSideLabel.fontColorTool()
        showPasswordAndRegisterView.rightSideImage.isHidden = true;
        showPasswordAndRegisterView.rightSideLabel.isHidden = true;
        showPasswordAndRegisterView.rightSideInnerButton.setTitle("Click To Register".localized, for: UIControl.State.normal);
//        showPasswordAndRegisterView.rightSideInnerButton.fontColorTool()
        stackView.addArrangedSubview(showPasswordAndRegisterView);
        let showPasswordAndRegisterViewHeight = translateAccordingToDevice(CGFloat(40.0));
        showPasswordAndRegisterView.heightAnchor.constraint(equalToConstant: showPasswordAndRegisterViewHeight).isActive = true;
        self.setLeadingAndTralingSpaceFormParentView(showPasswordAndRegisterView, parentView:stackView);
        showPasswordAndRegisterView.isHidden = true;
        
        let proceddToNextStepButton = UIButton();
        proceddToNextStepButton.translatesAutoresizingMaskIntoConstraints = false;
        proceddToNextStepButton.setThemeColor();
        proceddToNextStepButton.setTitleColor(Settings.themeTextColor, for: UIControl.State.normal);
        proceddToNextStepButton.setTitle("Proceed As Guest".localized, for: UIControl.State.normal);
//        proceddToNextStepButton.fontColorTool()
        proceddToNextStepButton.titleLabel?.font = UIFont(fontName: "", fontSize: CGFloat(17.0));
        proceddToNextStepButton.addTarget(self, action: #selector(CheckoutStep1ViewController.proceddToNextStepButtonPressed(_:)), for: UIControl.Event.touchUpInside);
        stackView.addArrangedSubview(proceddToNextStepButton);
        proceddToNextStepButton.makeCornerRounded(cornerRadius: translateAccordingToDevice(CGFloat(5.0)));
        let proceddToNextStepButtonHeight = translateAccordingToDevice(CGFloat(40.0));
        proceddToNextStepButton.heightAnchor.constraint(equalToConstant: proceddToNextStepButtonHeight).isActive = true;
        self.setLeadingAndTralingSpaceFormParentView(proceddToNextStepButton, parentView:stackView);
        proceddToNextStepButton.isHidden = true;
    }
    
    
    @objc func goToRegistrationPage(_ sender:UIButton){
        print("goToRegistrationPage");
        //let storyboard = UIStoryboard(name: "cedMageLogin", bundle: nil);
        //let viewController = storyboard.instantiateViewController(withIdentifier: "cedMageRegister") as! cedMageRegister;
        self.navigationController?.pushViewController(SignupController(), animated: true);
    }
    
    @objc func leftSideButtonPressed(_ sender:UIButton){ // proceed as guest condition
        print("leftSideButtonPressed");
        if(self.checkoutAs == "GUEST"){
            return;
        }
        if let selectLoginTypeForCheckoutView = stackView.arrangedSubviews[1] as? SelectLoginTypeForCheckoutView {
            selectLoginTypeForCheckoutView.leftSideImage.image = UIImage(named: "checked-radio");
            selectLoginTypeForCheckoutView.rightSideImage.image = UIImage(named: "unchecked-radio");
        }
        self.checkoutAs = "GUEST";
        if let textFieldEmail = stackView.arrangedSubviews[2] as? UITextField {
            textFieldEmail.isHidden = false;
            textFieldEmail.text = "";
            textFieldEmail.fontColorTool()
            textFieldEmail.placeholder = "Enter Guest Email".localized;
        }
        if let textFieldPassword = stackView.arrangedSubviews[3] as? UITextField {
            textFieldPassword.isHidden = true;
            textFieldPassword.fontColorTool()
            textFieldPassword.isSecureTextEntry = true;
            textFieldPassword.text = "";
        }
        if let showPasswordAndRegisterView = stackView.arrangedSubviews[4] as? SelectLoginTypeForCheckoutView {
            showPasswordAndRegisterView.isHidden = true;
            showPasswordAndRegisterView.leftSideImage.image = UIImage(named: "hidePassword");
        }
        if let proceddToNextStepButton = stackView.arrangedSubviews[5] as? UIButton {
            proceddToNextStepButton.isHidden = false;
            proceddToNextStepButton.setTitle("Proceed As Guest".localized, for: UIControl.State.normal);
            proceddToNextStepButton.fontColorTool()
        }
    }
    
    @objc func rightSideButtonPressed(_ sender:UIButton){ // proceed as user condition
        if(self.checkoutAs == "USER"){
            return;
        }
        if let selectLoginTypeForCheckoutView = stackView.arrangedSubviews[1] as? SelectLoginTypeForCheckoutView {
            selectLoginTypeForCheckoutView.leftSideImage.image = UIImage(named: "unchecked-radio");
            selectLoginTypeForCheckoutView.rightSideImage.image = UIImage(named: "checked-radio");
        }
        self.checkoutAs = "USER";
        if let textFieldEmail = stackView.arrangedSubviews[2] as? UITextField {
            textFieldEmail.isHidden = false;
            textFieldEmail.text = "";
            textFieldEmail.fontColorTool()
            textFieldEmail.placeholder = "Enter User Email".localized;
        }
        if let textFieldPassword = stackView.arrangedSubviews[3] as? UITextField {
            textFieldPassword.isHidden = false;
            textFieldPassword.isSecureTextEntry = true;
            textFieldPassword.placeholder = "Enter User Password".localized;
        }
        if let showPasswordAndRegisterView = stackView.arrangedSubviews[4] as? SelectLoginTypeForCheckoutView {
            showPasswordAndRegisterView.isHidden = false;
            showPasswordAndRegisterView.leftSideImage.image = UIImage(named: "hidePassword");
        }
        if let proceddToNextStepButton = stackView.arrangedSubviews[5] as? UIButton {
            proceddToNextStepButton.isHidden = false;
            proceddToNextStepButton.setTitle("Proceed As Registered User".localized, for: UIControl.State.normal);
            proceddToNextStepButton.fontColorTool()
        }
    }
    
    @objc func proceddToNextStepButtonPressed(_ sender:UIButton){
        
        var emailEntered = "";
        var passwordEntered = "";
        if let textFieldEmail = stackView.arrangedSubviews[2] as? UITextField {
            emailEntered = textFieldEmail.text!;
            emailEntered = emailEntered.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines);
        }
        if let textFieldPassword = stackView.arrangedSubviews[3] as? UITextField {
            passwordEntered = textFieldPassword.text!;
        }
        if(emailEntered == ""){

            self.view.makeToast("Please Enter Email".localized, duration: 2.0, position: .bottom, title: nil, image: nil, style: nil, completion: nil);
            return;
        }
        if(!isValidEmail(email: emailEntered)){
            self.view.makeToast("Please Enter Correct Email".localized, duration: 2.0, position: .bottom, title: nil, image: nil, style: nil, completion: nil);
            return;
        }
        
        print(self.checkoutAs);
        print(self.checkoutAs);
        if(self.checkoutAs == "GUEST"){

            self.defaults.set(emailEntered, forKey: "guestEmail");
//            if(cedMage.checkModule(string:"Ced_Mobiconnectcheckout")){
//                let storyboard = UIStoryboard(name: "cedMageAccounts", bundle: nil);
//                let viewController = storyboard.instantiateViewController(withIdentifier: "cedMageAdvancedCheckout") as! cedMageAdvancedCheckout;
//                self.navigationController?.pushViewController(viewController, animated: true);
//            }
//            else{
                let storyboard = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil);
                let viewController = storyboard.instantiateViewController(withIdentifier: "checkoutStep2ViewController") as! CheckoutStep2ViewController;
                viewController.email_id = emailEntered;
                viewController.totalOrderData = totalOrderData
                viewController.checkoutAs = "GUEST";
                self.navigationController?.pushViewController(viewController, animated: true);
           // }
        }
        else{
            if(passwordEntered == ""){
                self.view.makeToast("Please Enter Password".localized, duration: 2.0, position: .bottom, title: nil, image: nil, style: nil, completion: nil);
                return;
            }
            self.makeUserLoginToApp(emailEntered:emailEntered,passwordEntered:passwordEntered);
        }
        
        
    }
    
    func makeUserLoginToApp(emailEntered:String,passwordEntered:String){
        cedMageLoaders.addDefaultLoader(me: self);
        
        if(defaults.object(forKey: "userInfoDict") != nil){
            userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
        }
        
        var urlToRequest = "mobiconnect/customer_account/login/";
        let requestHeader = Settings.headerKey
        let baseURL = Settings.baseUrl

        urlToRequest = baseURL+urlToRequest;
        
        let postString = "email="+emailEntered+"&password="+passwordEntered;
        self.email_id = emailEntered;
        print(baseURL);
        print(postString);
        
        var request = URLRequest(url: URL(string: "\(urlToRequest)")!);
        request.httpMethod = "POST";
        request.httpBody = postString.data(using: String.Encoding.utf8);
        if UserDefaults.standard.bool(forKey: "isLogin"){
            if let user = UserDefaults.standard.object(forKey: "userInfoDict") as? [String:String] {
                request.setValue(user["hashKey"], forHTTPHeaderField: "hashkey")
            }
            
        }
        
        print("NSURLSession");
        let task = URLSession.shared.dataTask(with: request){
            
            // check for fundamental networking error
            data, response, error in
            guard error == nil && data != nil else{
                print("error=\(error)")
                DispatchQueue.main.async{
                    print(error?.localizedDescription as Any);
                    cedMageLoaders.removeLoadingIndicator(me: self);
                }
                return;
            }
            
            // check for http errors
            if let httpStatus = response as? HTTPURLResponse , httpStatus.statusCode != 200{
                
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(response)")
                DispatchQueue.main.async{
                    cedMageLoaders.removeLoadingIndicator(me: self);
                    
                }
                return;
            }
            
            // code to fetch values from response :: start
            
            let datastring = NSString(data: data!, encoding: String.Encoding.utf8.rawValue);
            //NSString(data: NSData!, encoding: String.Encoding.utf8)
            print("datastring");
            print(datastring as Any);
            
            guard let jsonResponse = try? JSON(data: data!) else{return;}
            if(jsonResponse != nil){
                
                DispatchQueue.main.async{
                    
                    print(jsonResponse);
                    cedMageLoaders.removeLoadingIndicator(me: self);
                    let msg = jsonResponse["data"]["customer"][0]["message"].stringValue;
                    if(jsonResponse["data"]["customer"][0]["status"].stringValue == "exception"){
                        self.view.makeToast(msg, duration: 2.0, position: .bottom, title: nil, image: nil, style: nil, completion: nil);
                        return;
                    }
                    else if(jsonResponse["data"]["customer"][0]["status"].stringValue == "success"){
                        self.view.makeToast(msg, duration: 2.0, position: .bottom, title: nil, image: nil, style: nil, completion: nil);
                        let customer_id = jsonResponse["data"]["customer"][0]["customer_id"].stringValue;
                        let hashKey = jsonResponse["data"]["customer"][0]["hash"].stringValue;
                        let cart_summary = jsonResponse["data"]["customer"][0]["cart_summary"].intValue;
                        let dict = ["email": emailEntered, "customerId": customer_id, "hashKey": hashKey];
                        self.defaults.set(true, forKey: "isLogin")
                        self.defaults.set(dict, forKey: "userInfoDict");
                        self.defaults.set(cart_summary, forKey: "cart_summary");
                        
                        
                        if(cedMage.checkModule(string:"Ced_Mobiconnectcheckout")){
                            let storyboard = UIStoryboard(name: "cedMageAccounts", bundle: nil);
                            let viewController = storyboard.instantiateViewController(withIdentifier: "cedMageAdvancedCheckout") as! cedMageAdvancedCheckout;
                            self.navigationController?.pushViewController(viewController, animated: true);
                        }
                        else{
                            let storyboard = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil);
                            let viewController = storyboard.instantiateViewController(withIdentifier: "checkoutStep2ViewController") as! CheckoutStep2ViewController;
                            viewController.checkoutAs = "USER";
                            viewController.email_id = self.email_id;
                            self.navigationController?.pushViewController(viewController, animated: true);
                        }
                        
                    }
                    
                }
            }
            
        }
        task.resume();
    }

    
    @objc func showHidePasswordPressed(_ sender:UIButton){
        print("showHidePasswordPressed");
        
        if(!isPasswordVisible){
            isPasswordVisible = true;
            if let textFieldPassword = stackView.arrangedSubviews[3] as? UITextField {
                textFieldPassword.isSecureTextEntry = false;
            }
            if let showPasswordAndRegisterView = stackView.arrangedSubviews[4] as? SelectLoginTypeForCheckoutView {
                showPasswordAndRegisterView.leftSideImage.image = UIImage(named: "showPassword");
                showPasswordAndRegisterView.leftSideLabel.text = "Hide Password".localized;
                showPasswordAndRegisterView.leftSideLabel.fontColorTool()
            }
        }
        else{
            isPasswordVisible = false;
            if let textFieldPassword = stackView.arrangedSubviews[3] as? UITextField {
                textFieldPassword.isSecureTextEntry = true;
            }
            if let showPasswordAndRegisterView = stackView.arrangedSubviews[4] as? SelectLoginTypeForCheckoutView {
                showPasswordAndRegisterView.leftSideImage.image = UIImage(named: "hidePassword");
                showPasswordAndRegisterView.leftSideLabel.text = "Show Password".localized;
                showPasswordAndRegisterView.leftSideLabel.fontColorTool()
            }
        }
        
    }
    
    override func basicFoundationToRenderView(bottomMargin:CGFloat){
        
        self.imageViewOnTopHeight.constant = translateAccordingToDevice(CGFloat(40.0));
        
        let tabBarHeight = CGFloat(50.0);
        // adding scrollview
        scrollView = UIScrollView();
        scrollView.translatesAutoresizingMaskIntoConstraints = false;
        scrollView.showsHorizontalScrollIndicator = false;
        scrollView.showsVerticalScrollIndicator = false;
        view.addSubview(scrollView);
        self.view.addConstraint(NSLayoutConstraint(item: scrollView, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 0));
        self.view.addConstraint(NSLayoutConstraint(item: scrollView, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 0));
        self.view.addConstraint(NSLayoutConstraint(item: scrollView, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.imageViewOnTop, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: 0));
        self.view.addConstraint(NSLayoutConstraint(item: scrollView, attribute: NSLayoutConstraint.Attribute.bottom, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.bottomLayoutGuide, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: -(bottomMargin+tabBarHeight)));
        
        // adding stackview
        stackView = UIStackView();
        stackView.translatesAutoresizingMaskIntoConstraints = false;
        stackView.axis  = NSLayoutConstraint.Axis.vertical;
        stackView.distribution  = UIStackView.Distribution.equalSpacing;
        stackView.alignment = UIStackView.Alignment.center;
        stackView.spacing   = 10.0;
        scrollView.addSubview(stackView);
        self.view.addConstraint(NSLayoutConstraint(item: stackView, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 0));
        self.view.addConstraint(NSLayoutConstraint(item: stackView, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 0));
        scrollView.addConstraint(NSLayoutConstraint(item: stackView, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: scrollView, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1, constant: 0));
        scrollView.addConstraint(NSLayoutConstraint(item: stackView, attribute: NSLayoutConstraint.Attribute.bottom, relatedBy: NSLayoutConstraint.Relation.equal, toItem: scrollView, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: 0));
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
