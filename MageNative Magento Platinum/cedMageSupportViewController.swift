//
//  cedMageSupportViewController.swift
//  ZeoMarket
//
//  Created by cedcoss on 02/04/19.
//  Copyright © 2019 MageNative. All rights reserved.
//

import UIKit

class cedMageSupportViewController: cedMageViewController {
  
  
  @IBOutlet weak var tableView: UITableView!
  var ticket = [String:String]()
  var conversations = [[String:String]]()
  var ticketDetails = [String:String]()
  var browseByRows = Int()
  var params = [String:String]()
  var fileExtension=".pdf"
  var encodedImagesData = [[String:String]]()
  var totalSize = Double()
  let picker = UIImagePickerController()
  var isDoc=false
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    tableView.delegate = self
    tableView.dataSource = self
    checkStatus()
    
  }
  
  func checkStatus(){
    var param = ["id":ticket["ticket_id"]]
    if ticket["email"] != nil{
      param["email"] = ticket["email"]
      params["email"] = ticket["email"]
    }
    params["id"] = ticket["ticket_id"]
    let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
    if let cust_id = userInfoDict["customerId"] {
      param["customer_id"] = cust_id
      params["customer_id"] = cust_id
    }else {
      params["customer_id"] = "guest"
    }
    self.sendRequest(url: "helpdeskapi/checkticketstatus", params: param as? Dictionary<String, String>)
  }
  
  override func recieveResponse(data: Data?, requestUrl: String?, response: URLResponse?) {
    if let data = data {
      do  {
        var json = try JSON(data:data)
        json = json[0]
        print(json)
        if requestUrl == "helpdeskapi/replyTicket" {
          if json["success"].stringValue == "true"{
            checkStatus()
          }else {
            cedMageHttpException.showAlertView(me: self, msg: json["message"].stringValue, title: "Error".localized)
          }
        }else {
          if json["success"].stringValue == "true"{
            
            ticketDetails["created_time"] = json["list"]["created_time"].stringValue
            ticketDetails["department"] = json["list"]["department"].stringValue
            ticketDetails["status"] = json["list"]["status"].stringValue
            ticketDetails["subject"] = json["list"]["subject"].stringValue
            ticketDetails["ticket_id"] = json["list"]["ticket_id"].stringValue
            ticketDetails["customer_name"] = json["list"]["customer_name"].stringValue
            if conversations.count > 0 {
              conversations.removeAll()
            }
            for conversation in  json["list"]["conversation"].arrayValue {
              self.conversations.append(["id":conversation["id"].stringValue,"message":conversation["message"].stringValue,"from":conversation["from"].stringValue,"to":conversation["to"].stringValue,"ticket_id":conversation["ticket_id"].stringValue,"created":conversation["created"].stringValue])
            }
            tableView.reloadData()
          }else {
            cedMageHttpException.showAlertView(me: self, msg: json["message"].stringValue, title: "Error".localized)
          }
        }
      }catch let error {
        print(error.localizedDescription)
      }
    }
  }
  
  
  
  
}

extension cedMageSupportViewController:UITableViewDelegate,UITableViewDataSource{
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    switch indexPath.section {
    case 0:
      if let cell = tableView.dequeueReusableCell(withIdentifier: "departmentStatus") as? cedMageTicketdepartmentStatusCell  {
        cell.departmentValue.text = ticketDetails["department"]
        cell.ticketCreated.text = ticketDetails["created_time"]
        cell.order.text = ticketDetails[""]
        return cell
      }
    case 1:
      if indexPath.row < conversations.count {
        let conversation = conversations[indexPath.row]
        if let msg = tableView.dequeueReusableCell(withIdentifier: "replyMsgCell") as? cedMageConversationReplycell{
          msg.titleLabel.text = conversation["from"]! + " | " + conversation["created"]!
          msg.labeltext.text = conversation["message"]
          return msg
        }
      }else {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "supportmsgcell") as? cedMageSupportMsgCell  {
          cell.msgTextView.delegate = self
          return cell
        }
      }
    case 2:
      switch indexPath.row {
      case 0:
        if let cell = tableView.dequeueReusableCell(withIdentifier: "supportAddAttach")  {
          if let button = cell.viewWithTag(325) as? UIButton {
            button.setThemeColor()
            button.addTarget(self, action: #selector(self.addAttachmentCell(sender:)), for: .touchUpInside)
          }
          return cell
        }
      default:
        if let cell = tableView.dequeueReusableCell(withIdentifier: "brwoseCell")  {
          if  let browseBy =   cell.viewWithTag(241) as? UIButton {
            browseBy.addTarget(self , action: #selector(self.browseButtonPressed(sender:)), for: .touchUpInside)
          }
          if let attachmentName = cell.viewWithTag(242) as? UILabel {
            if indexPath.row == browseByRows {
              if encodedImagesData.count > indexPath.row - 1{
                attachmentName.text = encodedImagesData[indexPath.row - 1]["name"]
              }
            }else{
              attachmentName.text = encodedImagesData[indexPath.row - 1]["name"]
            }
          }
          if let removeBtn = cell.viewWithTag(317) as? UIButton {
            removeBtn.addTarget(self, action: #selector(removeAttachmentCell(_:)), for: .touchUpInside)
            removeBtn.tag = indexPath.row
          }
          return cell
        }
      }
    case 3:
      
      if let cell = tableView.dequeueReusableCell(withIdentifier: "submitButtonCell")  {
        if let submitButton  = cell.viewWithTag(2250) as? UIButton  {
          submitButton.setThemeColor()
          submitButton.addTarget(self, action: #selector(self.submitForm(sender:)), for: .touchUpInside)
        }
        return cell
      }
    default:
      break
      
    }
    
    return UITableViewCell()
  }
  
  func numberOfSections(in tableView: UITableView) -> Int {
    return 4
  }
  
  
  @objc func addAttachmentCell(sender:UIButton) {
    browseByRows += 1
    let indexPath = IndexPath(row: browseByRows, section: 2)
    tableView.insertRows(at: [indexPath], with: .left)
  }
  
  @objc func removeAttachmentCell(_ sender: UIButton) {
    let indexPath = IndexPath(row: sender.tag, section: 2)
    browseByRows -= 1
    tableView.deleteRows(at: [indexPath], with: .right)
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    switch section {
    case 0:
      return self.ticketDetails.count > 0 ? 1 : 0
    case 1:
      return self.ticketDetails.count > 0 ? self.conversations.count + 1 : 0
    case 2:
      return self.ticketDetails.count > 0 ? self.browseByRows + 1 : 0
    case 3:
      return self.ticketDetails.count > 0 ? 1 : 0
    default:
      return self.ticketDetails.count > 0 ? 2 : 0
    }
    
  }
  
  @objc func browseButtonPressed(sender:UIButton)
  {
    let actionSheetControllerIOS8: UIAlertController = UIAlertController(title: "Please select", message: nil, preferredStyle: .actionSheet)
    
    let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { _ in
      print("Cancel")
    }
    actionSheetControllerIOS8.addAction(cancelActionButton)
    
    let saveActionButton = UIAlertAction(title: "Upload Image", style: .default)
    { _ in
      print("upload image")
      self.image_picker()
    }
    actionSheetControllerIOS8.addAction(saveActionButton)
    
    let uploadpdf = UIAlertAction(title: "Upload Document", style: .default)
    { _ in
      print("Delete")
      self.browseButtonPressed()
    }
    actionSheetControllerIOS8.addAction(uploadpdf)
    self.present(actionSheetControllerIOS8, animated: true, completion: nil)
  }
  
  
  
  func image_picker()
  {
    isDoc=true
    print("browseButtonPressed")
    picker.delegate=self
    picker.allowsEditing = false
    picker.sourceType = .photoLibrary
    self.present(picker, animated: true, completion: nil)
  }
  
  func browseButtonPressed(){
    let doc=UIDocumentMenuViewController(documentTypes: ["public.text","public.content","public.image","public.data"], in: .import)
    doc.delegate=self
    doc.modalPresentationStyle = .popover
    self.present(doc, animated: true, completion: nil)
  }
  
  
  @objc func submitForm(sender:UIButton){
    //        print(params)
    //        // params["attachments"] = pdfData as String
    //
    //        self.sendRequest(url: "helpdeskapi/replyTicket", params: params,store: false)
    var imgDatas="["
    if encodedImagesData.count != 0{
      for i in 0..<encodedImagesData.count{
        if(encodedImagesData[i] != [String:String]())
        {
          imgDatas+=encodedImagesData[i].convtToJson() as String
          if i != encodedImagesData.count-1{
            imgDatas+=","
          }
        }
        
      }
    }
    if  imgDatas.last == "," {
      imgDatas =  imgDatas.removeLast().description
    }
    imgDatas+="]"
    print(imgDatas)
    params["attachments"] = imgDatas
    print(params)
    self.sendRequest(url: "helpdeskapi/replyTicket", params: params,store: false)
  }
  
  
}

extension cedMageSupportViewController:UITextViewDelegate {
  func textViewDidEndEditing(_ textView: UITextView) {
    params["message"] = textView.text
  }
}

extension cedMageSupportViewController:UIDocumentPickerDelegate,UIDocumentInteractionControllerDelegate,UIDocumentMenuDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
  
  func documentMenu(_ documentMenu: UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
    documentPicker.delegate=self
    documentPicker.modalPresentationStyle = .fullScreen
    self.present(documentPicker, animated: true, completion: nil)
    print("didPickDocumentPicker")
  }
  
  
  func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
    
    let fileName=url.absoluteString.components(separatedBy: "/")
    // docUrlLabel.text=url.absoluteString
    print("didPickDocumentAt")
    // FileSelectedOrNot.text = fileName[0]
    
    let isSecuredURL = url.startAccessingSecurityScopedResource() == true
    let coordinator = NSFileCoordinator()
    var error: NSError? = nil
    coordinator.coordinate(readingItemAt: url, options: [], error: &error) { (url) -> Void in
      do{
        let temp=try Data(contentsOf: url)
        //                let t = try NSData(contentsOf: url)
        //self.trainingDocData=temp.base64EncodedString()
        
        let temp1=fileName[fileName.count-1].components(separatedBy: ".")
        print("The file you have choosed is not in correct format")
        //  print(temp1)
        if (temp1[1] != "pdf" || temp1[1] != "zip")
        {
          let msg = "The file you have choosed is not in correct format"
          print(msg)
          return
        }
        else
        {
          let size = Double(temp.count / 1048576)
          if size > 5.00 {
            controller.dismiss(animated: true, completion: nil)
            self.view.makeToast("File should not be more tha 5Mb", duration: 2.0, position: .center)
          }else{
            let dataPdf = temp.base64EncodedString()
            self.encodedImagesData.append(["name":"xyz.pdf", "base64_encoded_data": dataPdf])
            tableView.reloadSections(NSIndexSet(index: 2) as IndexSet, with: .bottom)
          }
        }
      }catch let error{
        print(error.localizedDescription)
      }
    }
    if (isSecuredURL) {
      url.stopAccessingSecurityScopedResource()
    }
    print(url)
    controller.dismiss(animated: true, completion: nil)
  }
  
  func documentMenuWasCancelled(_ documentMenu: UIDocumentMenuViewController) {
    print("documentMenuWasCancelled")
    documentMenu.dismiss(animated: true, completion: nil)
  }
  
  func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
    print("documentPickerWasCancelled")
    controller.dismiss(animated: true, completion: nil)
  }
  
  func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
    picker.dismiss(animated: true, completion: nil)
  }
  
  
  func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
    
    //        let image = info[UIImagePickerControllerOriginalImage] as? UIImage
    //       print(image)
    //        picker.dismiss(animated: true, completion: nil)
    let values = info[UIImagePickerController.InfoKey.referenceURL] as? NSURL
    let imageName = values!.lastPathComponent
    let image = info[.originalImage] as? UIImage
    if let imageData:NSData = image!.pngData() as? NSData {
      let fileSize = Double(imageData.length / 1024 / 1024)
      print(fileSize)
      totalSize += fileSize
      if totalSize > 5.00 {
        picker.dismiss(animated: true, completion: nil)
        self.view.makeToast("Please upload a file of less than 5 Mb", duration: 2.0, position: .center)
      }else{
        let imageStr = imageData.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        self.encodedImagesData.append(["name": imageName ?? "xyz.png" ,"base64_encoded_data": imageStr])
        tableView.reloadSections(NSIndexSet(index: 2) as IndexSet, with: .bottom)
        if self.encodedImagesData.count != 0 {
          print("has data")
        }
        picker.dismiss(animated: true, completion: nil)
      }
    }
  }
  
  //MARK: URLSessionDownloadDelegate
  // 1
  func urlSession(_ session: URLSession,
                  downloadTask: URLSessionDownloadTask,
                  didFinishDownloadingTo location: URL)
  {
    
    let path = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
    let documentDirectoryPath:String = path[0]
    let fileManager = FileManager()
    let destinationURLForFile = URL(fileURLWithPath: documentDirectoryPath.appendingFormat("/file."+fileExtension))
    
    if fileManager.fileExists(atPath: destinationURLForFile.path){
      showFileWithPath(path: destinationURLForFile.path)
    }
    else{
      do {
        try fileManager.moveItem(at: location, to: destinationURLForFile)
        // show file
        print(destinationURLForFile.path)
        showFileWithPath(path: destinationURLForFile.path)
      }catch{
        print("An error occurred while moving file to destination url")
      }
    }
  }
  // 2
  func urlSession(_ session: URLSession,
                  downloadTask: URLSessionDownloadTask,
                  didWriteData bytesWritten: Int64,
                  totalBytesWritten: Int64,
                  totalBytesExpectedToWrite: Int64){
    print("asdfgh")
  }
  
  //MARK: URLSessionTaskDelegate
//  func urlSession(_ session: URLSession,
//                  task:URLSessionTask,
//                  didCompleteWithError error: Error?){
//    //        downloadTask = nil
//    if (error != nil) {
//      print(error!.localizedDescription)
//      cedMageHttpException.showAlertView(me: self, msg:  "Error", title: "Can't Download in Process")
//      
//    }else{
//      print("The task finished transferring data successfully")
//    }
//  }
  
  //MARK: UIDocumentInteractionControllerDelegate
  func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController
  {
    UINavigationBar.appearance().barTintColor = UIColor.black
    UINavigationBar.appearance().tintColor = UIColor.black
    UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.bold)]
    return self
  }
  
  
  func showFileWithPath(path: String){
    let isFileFound:Bool? = FileManager.default.fileExists(atPath: path)
    if isFileFound == true{
      let viewer = UIDocumentInteractionController(url: URL(fileURLWithPath: path))
      
      viewer.delegate = self
      viewer.presentPreview(animated: true)
    }
  }
  
  
  
  // Helper function inserted by Swift 4.2 migrator.
  //    fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
  //
  //        return Dictionary(uniqueKeysWithValues: input.map {key,value in (key.rawValue, value)})
  //    }
  //
  //    // Helper function inserted by Swift 4.2 migrator.
  //    fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
  //        let view = input.
  //        return input
  //    }
}
