//
//  HomeSearchTC.swift
//  MageNative Magento Platinum
//
//  Created by Hamza Usmani on 02/09/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class HomeSearchTC: UITableViewCell {
    
    //MARK:- properties
    
    static var reuseId:String = "HomeSearchTC"
    weak var parent: UIViewController?
    
    //MARK:- UIControls
    
    lazy var outerView:UIView = {
        let view = UIView()
        view.backgroundColor = .mageSecondarySystemBackground
        return view
    }()
    lazy var searchField:UITextField = {
        let field = UITextField()
        field.borderStyle = .none
        field.textColor = .mageLabel
        field.font = .systemFont(ofSize: 16, weight: .semibold)
        field.placeholder = "Search Here ...."
        //field.delegate = self
        return field
    }()
    lazy var searchButton:UIButton = {
        let button = UIButton(type: .system)
        button.setImage(UIImage(named: "searchIcon"), for: .normal)
        button.backgroundColor = UIColor(hexString: "#FCBE11")
        button.tintColor = .black
        return button
    }()
    
    lazy var tapToSearch: UIButton = {
        let button = UIButton()
        button.backgroundColor = .clear
        button.addTarget(self, action: #selector(navigateToSearch), for: .touchUpInside)
        return button
    }()
    
    //MARK:- Lifecycle
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        selectionStyle         = .none
        backgroundColor        = Settings.themeColor
        contentView.isMultipleTouchEnabled  = true
        
        addSubview(outerView)
        outerView.anchor(top: topAnchor, left: leadingAnchor, bottom: bottomAnchor, right: trailingAnchor, paddingTop: 12, paddingLeft: 12, paddingBottom: 12, paddingRight: 12)
        
        outerView.addSubview(searchField)
        searchField.anchor(top: outerView.topAnchor, left: outerView.leadingAnchor, bottom: outerView.bottomAnchor, right: outerView.trailingAnchor, paddingLeft: 15)
        
        outerView.addSubview(searchButton)
        searchButton.anchor(top: outerView.topAnchor, bottom: outerView.bottomAnchor, right: outerView.trailingAnchor, width: 44)
        
        outerView.addSubview(tapToSearch)
        tapToSearch.addConstraintsToFillView(outerView)
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        outerView.layer.cornerRadius = 8
        
        searchButton.layer.maskedCorners = [.layerMaxXMaxYCorner,.layerMaxXMinYCorner]
        searchButton.layer.cornerRadius = 8
    }
    
    func populate(with parent: UIViewController) {
        self.parent = parent
    }
    
    @objc private func navigateToSearch() {
        print("navigate")
        parent?.navigationController?.pushViewController(SearchController(), animated: true)
    }
    
    
    
}

//MARK:- UITextFieldDelegate

extension HomeSearchTC: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.endEditing(true)
      //  let searchPage = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "searchView") as! cedMageSearchPage
        parent?.navigationController?.pushViewController(SearchController(), animated: true)
        
    }
}
