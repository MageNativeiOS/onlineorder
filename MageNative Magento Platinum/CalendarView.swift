//
//  CalendarView.swift
//  MageNative Magento Platinum
//
//  Created by cedcoss on 19/01/21.
//  Copyright © 2021 CEDCOSS Technologies Private Limited. All rights reserved.
//

import Foundation
import FSCalendar
class CalendarView : UIView
{
   
    @IBOutlet weak var done: UIButton!
    
    @IBOutlet weak var cancel: UIButton!
    @IBOutlet weak var calendarView: FSCalendar!
    @IBOutlet weak var selectDate: UILabel!
    var root = newProductCardSection()
  //  var datePickerView = FSCalendar();
    
    var view = UIView()
    override init(frame: CGRect) {
           super.init(frame: frame)
          xibSetup()
       }
       
       required init?(coder: NSCoder) {
           super.init(coder: coder)
           xibSetup()
       }
    
    func xibSetup()
    {
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        selectDate.backgroundColor = Settings.themeColor
        cancel.setThemeColor()
        done.backgroundColor = .black
       
        addSubview(view)
    }
    
    @objc func loadViewFromNib() -> UIView
     {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "CalendarView", bundle: bundle)
        
        // Assumes UIView is top level and only object in CustomView.xib file
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
     }
    
   
    
//}
}
