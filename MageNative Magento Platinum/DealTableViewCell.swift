//
//  DealTableViewCell.swift
//  MageNative Magento Platinum
//
//  Created by Macmini on 30/10/18.
//  Copyright © 2018 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class DealTableViewCell: UITableViewCell,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var collectionview: UICollectionView!
//    var dealTouple = (group_start_date:String(),created_time:String(),group_end_date:String(),deal_duration:String(),group_image_name:String(),group_status:String(),title:String(),group_id:String(),store_id:String(),timer_status:String(),view_all_status:String(),is_static:String(),deal_link:String(),update_time:String())
    
      var subDealToupleArray = [(id:"", deal_image_name: "", deal_link: "", start_date: "", relative_link: "" ,deal_type: "", end_date: "", deal_title: "", product_link:"", store_id: "", category_link: "", offer_text: "", status: "")]
    
    var parentView = UIViewController()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        parseDealData()
    }
    
    func parseDealData() {
        collectionview.delegate = self
        collectionview.dataSource = self
        collectionview.isScrollEnabled = false
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return subDealToupleArray.count
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "dealCollectionViewhomePage", for: indexPath) as? dealCollectionViewhomePage
        
        if let url = URL(string: subDealToupleArray[indexPath.row].deal_image_name){
            cell?.imageView.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "placeholder"))
        }
        cell?.label.text = subDealToupleArray[indexPath.row].deal_title
         let color = cedMage.getInfoPlist(fileName: "cedMage", indexString: "themeColor") as! String
        cell?.label.backgroundColor = UIColor(hexString: color)
        cell?.label.textColor = .white
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if(subDealToupleArray[indexPath.row].deal_type ==  "1"){
            let productview = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "productSinglePageViewController") as! ProductSinglePageViewController
            productview.product_id = subDealToupleArray[indexPath.row].relative_link
            parentView.navigationController?.pushViewController(productview, animated: true)
        }else if(subDealToupleArray[indexPath.row].deal_type ==  "2"){
            let productLink = subDealToupleArray[indexPath.row].relative_link
            if  productLink != "" {
                if let contol = UIStoryboard(name: "categorylayouts", bundle: nil).instantiateViewController(withIdentifier: "cedMageDefaultCollection") as?  cedMageDefaultCollection{
                    contol.selectedCategory = productLink
                    parentView.navigationController?.pushViewController(contol, animated: true)
                }
            }
            
        }else if(subDealToupleArray[indexPath.row].deal_type ==  "3"){
            let view = UIStoryboard(name: "cedMageAccounts", bundle: nil)
            let viewControl = view.instantiateViewController(withIdentifier: "cmsWebView") as! cedMageCmsWebView
            viewControl.pageUrl = subDealToupleArray[indexPath.row].category_link
            parentView.navigationController?.pushViewController(viewControl, animated: true)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if UIDevice.current.userInterfaceIdiom == .pad{
            if self.subDealToupleArray.count % 2 == 0 {
                let height = CGFloat(190)
                let width = parentView.view.bounds.size.width/3 - 12
                print(CGSize(width:width, height:height))
                return CGSize(width:width, height:height);
            }else{
                if subDealToupleArray.last?.id == subDealToupleArray[indexPath.row].id{
                    let height = CGFloat(190)
                    let width = collectionView.frame.size.width - 12
                    print(CGSize(width:width, height:height))
                    return CGSize(width:width, height:height);
                }else{
                    let height = CGFloat(190)
                    let width = collectionView.frame.size.width/3 - 5
                    print(CGSize(width:width, height:height))
                    return CGSize(width:width, height:height);
                }
            }
        }else{
            if self.subDealToupleArray.count % 2 == 0 {
                let height = CGFloat(190)
                let width = parentView.view.bounds.size.width/2 - 12
                print(CGSize(width:width, height:height))
                return CGSize(width:width, height:height);
            }else{
                if subDealToupleArray.last?.id == subDealToupleArray[indexPath.row].id{
                    let height = CGFloat(190)
                    let width = collectionView.frame.size.width - 12
                    print(CGSize(width:width, height:height))
                    return CGSize(width:width, height:height);
                }else{
                    let height = CGFloat(190)
                    let width = collectionView.frame.size.width/2 - 5
                    print(CGSize(width:width, height:height))
                    return CGSize(width:width, height:height);
                }
            }
        }

        
        
        
        
//        if subDealToupleArray.count % 2 == 0{
//            if self.subDealToupleArray.count % 2 == 0 {
//                let height = CGFloat(250)
//                let width = parentView.view.bounds.size.width/3 - 12
//                print(CGSize(width:width, height:height))
//                return CGSize(width:width, height:height);
//            }else{
//                if subDealToupleArray.last?.id == subDealToupleArray[indexPath.row].id{
//                    let height = CGFloat(250)
//                    let width = collectionView.frame.size.width - 12
//                    print(CGSize(width:width, height:height))
//                    return CGSize(width:width, height:height);
//                }else{
//                    let height = CGFloat(250)
//                    let width = collectionView.frame.size.width/3 - 5
//                    print(CGSize(width:width, height:height))
//                    return CGSize(width:width, height:height);
//                }
//            }
//        }else{
//            if self.subDealToupleArray.count % 2 == 0 {
//                let height = CGFloat(250)
//                let width = parentView.view.bounds.size.width/3 - 12
//                print(CGSize(width:width, height:height))
//                return CGSize(width:width, height:height);
//            }else{
//                if subDealToupleArray.last?.id == subDealToupleArray[indexPath.row].id{
//                    let height = CGFloat(250)
//                    let width = collectionView.frame.size.width - 12
//                    print(CGSize(width:width, height:height))
//                    return CGSize(width:width, height:height);
//                }else{
//                    let height = CGFloat(250)
//                    let width = collectionView.frame.size.width/2 - 5
//                    print(CGSize(width:width, height:height))
//                    return CGSize(width:width, height:height);
//                }
//            }
//        }
        
        
        
        
        
        
        
    }
}



class dealCollectionViewhomePage: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var label: UILabel!
}



class DealBannerCell:UITableViewCell,FSPagerViewDataSource,FSPagerViewDelegate {
    @IBOutlet weak var topLabel: UILabel!
    var parentView = UIViewController()
     var subDealToupleArray2nd = [(id:"", deal_image_name: "", deal_link: "", start_date: "", relative_link: "" ,deal_type: "", end_date: "", deal_title: "", product_link:"", store_id: "", category_link: "", offer_text: "", status: "")]
    @IBOutlet weak var pagerView: FSPagerView! {
        didSet {
            self.pagerView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell")
            self.pagerView.itemSize = .zero
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        pagerView.dataSource = self
        pagerView.delegate = self
        pagerView.automaticSlidingInterval = 3.0
//        pagerView.isInfinite = true
//        pagerView.
        print(subDealToupleArray2nd)
        pagerView.itemSize = .zero
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    
    
    
    // MARK:- FSPagerView DataSource
    
    public func numberOfItems(in pagerView: FSPagerView) -> Int {
        return subDealToupleArray2nd.count
    }
    
    public func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index)
        cell.imageView?.contentMode = .scaleAspectFill
        cell.imageView?.clipsToBounds = true
        if let url = URL(string: subDealToupleArray2nd[index].deal_image_name){
            cell.imageView?.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "placeholder"))
        }
        cell.textLabel?.text = subDealToupleArray2nd[index].offer_text
        
        return cell
    }
    
    // MARK:- FSPagerView Delegate
    
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        if(subDealToupleArray2nd[index].deal_type ==  "1"){
            let productview = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "productSinglePageViewController") as! ProductSinglePageViewController
            productview.product_id = subDealToupleArray2nd[index].relative_link
            parentView.navigationController?.pushViewController(productview, animated: true)
        }else if(subDealToupleArray2nd[index].deal_type ==  "2"){
            let productLink = subDealToupleArray2nd[index].relative_link
            if  productLink != "" {
                if let contol = UIStoryboard(name: "categorylayouts", bundle: nil).instantiateViewController(withIdentifier: "cedMageDefaultCollection") as?  cedMageDefaultCollection{
                    contol.selectedCategory = productLink
                    parentView.navigationController?.pushViewController(contol, animated: true)
                }
            }
        }else if(subDealToupleArray2nd[index].deal_type ==  "3"){
            let view = UIStoryboard(name: "cedMageAccounts", bundle: nil)
            let viewControl = view.instantiateViewController(withIdentifier: "cmsWebView") as! cedMageCmsWebView
            viewControl.pageUrl = subDealToupleArray2nd[index].category_link
            parentView.navigationController?.pushViewController(viewControl, animated: true)
        }
    }
    
    func pagerViewWillEndDragging(_ pagerView: FSPagerView, targetIndex: Int) {
        
    }
    
    func pagerViewDidEndScrollAnimation(_ pagerView: FSPagerView) {
        
        
    }
    
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
//        if no_pro_check==false
//        {
//            if flag==true{
//                flag=false
//                currentpage += 1
//                self.send_req_pagination(url: "mobiconnect/home/featured/page/\(currentpage)/store/", store: true)
//            }
//        }
        
        
    }
    
    

}


