/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */


import UIKit
import WebKit
class cedMageAdvancedCheckout: cedMageViewController,WKScriptMessageHandler,WKNavigationDelegate {
    //    MARK: JUGAD
    var count = 0
    var checkOut = WKWebView()
    var currentUrl = String()
    required init?(coder aDecoder: NSCoder) {
        self.checkOut = WKWebView(frame: CGRect.zero)
        super.init(coder: aDecoder)
        self.checkOut.navigationDelegate = self
    }
    override func viewDidLoad() {
        
        super.viewDidLoad()
        beforeCheckOutCheck()
        // Do any additional setup after loading the view.
    }
    
    func beforeCheckOutCheck(){
        
        var baseUrl   = Settings.sellerUrl+"mobiconnectcheckout/onepage/index"
        if(defaults.bool(forKey: "isLogin")){
            
            let currentuser = defaults.object(forKey: "userInfoDict") as! NSDictionary
            
            let userid = currentuser["customerId"]
            baseUrl   += "/customer_id/"+"\(userid!)"
        }else{
            baseUrl   += "/cart_id/"
            if(defaults.object(forKey: "cartId") != nil)
            {
                let cart_id = defaults.object(forKey: "cartId") as? String;
                baseUrl += "\(cart_id!)"
            }
            //            http://hairsuppliersindia.com/mobiconnectcheckout/onepage/index/customer_id/39/store_id/2/check/true
            
        }
        loadData(url:baseUrl)
        /*baseUrl += "/store_id/"
        if  let storeId = UserDefaults.standard.value(forKey: "storeId")  as? String {
            baseUrl += "\(storeId)"
        }
        baseUrl += "/check/true"
        print(baseUrl)
        self.sendWheelRequest(url: baseUrl, params: "")*/
        // self.sendCheckoutRequest(url: baseUrl, params: nil)
    }
    
    override func recieveResponse(data: Data?, requestUrl: String?, response: URLResponse?) {
        if let data = data{
            guard let json = try? JSON(data:data) else{return;}
            print(json)
            if json["success"].stringValue == "false"{
                cedMageHttpException.showAlertView(me: self, msg: json["message"].stringValue, title: "Error".localized)
                //               _ = self.navigationController?.popViewController(animated: true)
            }else{
                let url = json["message"].stringValue
                loadData(url:url)
            }
            
        }
    }
    
    
    func loadData(url:String){
        var baseUrl = url
        //        var baseUrl = cedMage.getInfoPlist(fileName:"cedMage",indexString: "cedAppBaseUrl") as! String
        //        baseUrl   += "mobiconnectcheckout/onepage/index"
        //        if(defaults.bool(forKey: "isLogin")){
        //            let currentuser = defaults.object(forKey: "userInfoDict") as! NSDictionary
        //            let userid = currentuser["customerId"]
        //            baseUrl   += "/customer_id/"+"\(userid!)"
        //        }else{
        //            baseUrl   += "/cart_id/"
        //            if(defaults.object(forKey: "cartId") != nil)
        //            {
        //                let cart_id = defaults.object(forKey: "cartId") as? String;
        //                baseUrl += "\(cart_id!)"
        //            }
        //        }
        print(baseUrl)
        
        var request = URLRequest(url: URL(string:baseUrl)!)
        let requestHeader = Settings.headerKey//cedMage.getInfoPlist(fileName:"cedMage",indexString: "requestheader") as! String
        request.setValue(requestHeader, forHTTPHeaderField: "Mobiconnectheader")
        let webconfgi = WKWebViewConfiguration()
        webconfgi.userContentController.add(self,name: "redir")
        
        let bounds = self.view.bounds
        let toplayoutguide = self.navigationController!.view.frame.origin.y ;
        let frame  = CGRect(x: 0, y: toplayoutguide, width: bounds.width, height: bounds.height)
        checkOut = WKWebView(frame: frame, configuration: webconfgi)
        checkOut.load(request)
        self.view.addSubview(checkOut)
        checkOut.navigationDelegate = self
        cedMageLoaders.addDefaultLoader(me: self)
        
    }
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        cedMageLoaders.removeLoadingIndicator(me: self)
    }
    
    func userContentController(_ userContentController:
        WKUserContentController,
                               didReceive message: WKScriptMessage) {
        print(message.body)
        if let message = message.body as? String {
            if message.contains("orderview"){
                if let orderId = message.components(separatedBy: "-").last {
                    let viewcontroll = UIStoryboard(name: "cedMageAccounts", bundle: nil).instantiateViewController(withIdentifier: "cedMageSingleOrder") as! cedMageSingleOrder
                    viewcontroll.orderId = orderId
                    self.navigationController?.pushViewController(viewcontroll, animated: true)
                }
            }else if message.contains("success"){
                defaults.setValue("0", forKey: "items_count")
                self.setCartCount(view: self, items: "0")
            }else{
//                if checkOut.url?.absoluteString.contains("onepage/success") ?? false{
                    if message.lowercased().contains("redirect"){
                        if count >= 1 {
                            _ =  self.navigationController?.popToRootViewController(animated: true)
                        }else{
                            count = count + 1
                        }
                    }
//                }else{
////                    _ =  self.navigationController?.popToRootViewController(animated: true)
//                }
                
            }
        }
    }
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        if let urlStr = navigationAction.request.url?.absoluteString {
            currentUrl = urlStr
        }
        decisionHandler(.allow)
    }
    
}
