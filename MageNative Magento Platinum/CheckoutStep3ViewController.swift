/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */


import UIKit

class CheckoutStep3ViewController: MagenativeUIViewController {
    
    @IBOutlet weak var imageViewOnTop: UIImageView!
    
    
    @IBOutlet weak var imageViewOnTopHeight: NSLayoutConstraint!
    
    var checkoutAs = "USER";
    var email_id = "test@test.com";
    
    //var shippingMethods = [String: String]();
    var paymentMethods = [String: String]();
    var payment_method = "";
    var shipping_method = "";
    var cart_id = "0";
    
    //Native Payments
    private var nativePayments = [String:String]()//"Debit / Credit card":"Razorpay"]
   
    //Order data
    var totalOrderData = [String:String]()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        if(defaults.object(forKey: "cartId") != nil){
            cart_id = (defaults.object(forKey: "cartId") as? String)!;
        }else{
            cart_id = "0"
        }

        self.basicFoundationToRenderView(bottomMargin: CGFloat(0.0));
        self.makeRequestToAPI("mobiconnect/checkout/getshippingpayament",dataToPost: ["email":email_id, "Role":checkoutAs, "cart_id":cart_id]);
        
    }
    
    
    
    override func parseResponseReceivedFromAPI(_ jsonResponse:JSON){
        print("PAYMENT");
        print(jsonResponse);
        let jsonResponse = jsonResponse[0]
        
        
        if(jsonResponse["success"].stringValue == "true")
        {
            let payMethods = jsonResponse["payments"]["methods"]//.arrayValue;
            print(payMethods)
            if payMethods.stringValue.lowercased() != "No Payment Method Availabile.".lowercased() {
                for (_,result) in payMethods{
                    let value = result["value"].stringValue;
                    let key = result["label"].stringValue;
                    self.paymentMethods[key] = value;
                }
            }
            if nativePayments.count > 0 {
                for (key,val) in nativePayments {
                    self.paymentMethods[key] = val
                }
                
            }
            

        }
        print(paymentMethods);
        //print(shippingMethods);
        self.renderCheckoutStep3View();
    }
    func renderCheckoutStep3View(){
        
        self.makeSomeSpaceInStackView(height: CGFloat(0.0));
        
        let paymentOptionDropDownView = CustomOptionDropDownView();
        paymentOptionDropDownView.translatesAutoresizingMaskIntoConstraints = false;
        paymentOptionDropDownView.topLabel.text = "Select Payment Method".localized;
         paymentOptionDropDownView.topLabel.fontColorTool()
        paymentOptionDropDownView.dropDownButton.setTitle("-- Select --".localized, for: UIControl.State());
        paymentOptionDropDownView.dropDownButton.tag = 100;
//        paymentOptionDropDownView.dropDownButton.fontColorTool()
        //paymentOptionDropDownView.dropDownButton.setTitleColor(.black, for: .normal)
        paymentOptionDropDownView.dropDownButton.addTarget(self, action: #selector(CheckoutStep3ViewController.showOptionsDropdown(_:)), for: UIControl.Event.touchUpInside);
        stackView.addArrangedSubview(paymentOptionDropDownView);
        let paymentOptionDropDownViewHeight = translateAccordingToDevice(CGFloat(100.0));
        paymentOptionDropDownView.heightAnchor.constraint(equalToConstant: paymentOptionDropDownViewHeight).isActive = true;
        self.setLeadingAndTralingSpaceFormParentView(paymentOptionDropDownView,parentView:stackView);
        

        
        let proceddToNextStepButton = UIButton();
        proceddToNextStepButton.translatesAutoresizingMaskIntoConstraints = false;
        proceddToNextStepButton.setThemeColor();
        proceddToNextStepButton.setTitleColor(Settings.themeTextColor, for: UIControl.State.normal);
        proceddToNextStepButton.setTitle("Proceed".localized, for: UIControl.State.normal);
        proceddToNextStepButton.titleLabel?.font = UIFont(fontName: "", fontSize: CGFloat(17.0));
        proceddToNextStepButton.addTarget(self, action: #selector(CheckoutStep3ViewController.proceddToNextStepButtonPressed(_:)), for: UIControl.Event.touchUpInside);
        stackView.addArrangedSubview(proceddToNextStepButton);
        proceddToNextStepButton.makeCornerRounded(cornerRadius: translateAccordingToDevice(CGFloat(5.0)));
        let proceddToNextStepButtonHeight = translateAccordingToDevice(CGFloat(40.0));
        proceddToNextStepButton.heightAnchor.constraint(equalToConstant: proceddToNextStepButtonHeight).isActive = true;
        self.setLeadingAndTralingSpaceFormParentView(proceddToNextStepButton, parentView:stackView);
        
        self.makeSomeSpaceInStackView(height: CGFloat(0.0));
    }
    
    @objc func showOptionsDropdown(_ sender:UIButton){
        
        
        var ArrayToUse = ["-- Select --".localized];
        if(sender.tag == 100){
            let tempArray = Array(self.paymentMethods.keys);
            ArrayToUse += tempArray;
        }
//        else{
//            let tempArray = Array(self.shippingMethods.keys);
//            ArrayToUse += tempArray;
//        }
        dropDown.dataSource = ArrayToUse;
        dropDown.selectionAction = {(index, item) in
            sender.setTitle(item, for: UIControl.State());
        }
        
        dropDown.anchorView = sender
        dropDown.bottomOffset = CGPoint(x: 0, y:sender.bounds.height)
        if dropDown.isHidden {
            let _ = dropDown.show();
        } else {
            dropDown.hide();
        }
        
    }
    
    
    @objc func proceddToNextStepButtonPressed(_ sender:UIButton){
        print("proceddToNextStepButtonPressed");
        
        for view in stackView.subviews{
            if let customOptionDropDownView = view as? CustomOptionDropDownView{
                let textOnButton = customOptionDropDownView.dropDownButton.titleLabel?.text!;
                if(customOptionDropDownView.dropDownButton.tag == 100){
                    if(self.paymentMethods[textOnButton!] == nil){
                        let msg = "Please Select Payment Method".localized;
                        self.view.makeToast(msg, duration: 2.0, position: .bottom, title: nil, image: nil, style: nil, completion: nil);
                        return;
                    }
                    self.payment_method = self.paymentMethods[textOnButton!]!;
                    
                   
                        self.savePaymentData("");
                    
                }
            }
        }
//        let storyboard = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil);
//        let viewController = storyboard.instantiateViewController(withIdentifier: "checkoutStep4ViewController") as! CheckoutStep4ViewController;
//        viewController.email_id = self.email_id;
//        viewController.totalOrderData = self.totalOrderData
//        viewController.checkoutAs = self.checkoutAs;
//        viewController.payment_method=self.payment_method
////        viewController.isOrderReview = true;
////        viewController.selectedPaymentMethod = self.payment_method
////        viewController.orderEmail = self.email_id;
//        self.navigationController?.pushViewController(viewController, animated: true);
        //self.savePaymentData("");
        
    }
    
   
    func savePaymentData(_ paymentId:String){
        
        var urlToRequest = "mobiconnect/checkout/saveshippingpayament";
        let requestHeader = Settings.headerKey;
        let baseURL = Settings.baseUrl;
        urlToRequest = baseURL+urlToRequest;
        
        var postString = "";
        var postData = [String:String]()
        postData["cart_id"] = cart_id;
         if self.payment_method.lowercased() == "Razorpay".lowercased(){
                   postData["payment_method"] = "apppayment";
               }else{
            postData["payment_method"] = payment_method;
        }
        if shipping_method != "" {
             postData["shipping_method"] = shipping_method;
        }else{
            postData["shipping_method"] = "false"
        }
       
        postData["Role"] = checkoutAs;
        postData["email"] = email_id;
        if paymentId != "" {
            postData["additional_information"] = paymentId
        }
        if(defaults.object(forKey: "userInfoDict") != nil){
            postData["hashkey"] = userInfoDict["hashKey"]!
            postData["customer_id"] = userInfoDict["customerId"]!;
        }
        postString = ["parameters":postData].convtToJson() as String
        print(postString);
        var request = URLRequest(url: URL(string: "\(urlToRequest)")!);
        request.httpMethod = "POST";
        request.httpBody = postString.data(using: String.Encoding.utf8);
        if UserDefaults.standard.bool(forKey: "isLogin"){
            if let user = UserDefaults.standard.object(forKey: "userInfoDict") as? [String:String] {
                request.setValue(user["hashKey"], forHTTPHeaderField: "hashkey")
            }
            
        }
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        cedMageLoaders.addDefaultLoader(me: self);
        let task = URLSession.shared.dataTask(with: request){
            
            // check for fundamental networking error
            data, response, error in
            guard error == nil && data != nil else{
                print("error=\(error)")
                DispatchQueue.main.async{
                    print(error?.localizedDescription as Any);
                    cedMageLoaders.removeLoadingIndicator(me: self);
                }
                return;
            }
            
            // check for http errors
            if let httpStatus = response as? HTTPURLResponse , httpStatus.statusCode != 200{
                
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(response)")
                DispatchQueue.main.async{
                    cedMageLoaders.removeLoadingIndicator(me: self);
                }
                return;
            }
            
            // code to fetch values from response :: start
            guard var jsonResponse = try? JSON(data: data!) else{return;}
            jsonResponse = jsonResponse[0]
            if(jsonResponse != nil){
                
                DispatchQueue.main.async{
                    cedMageLoaders.removeLoadingIndicator(me: self);
                    print(jsonResponse);
                    
                    self.navigationController?.pushViewController(OrderReviewController(payment_method: self.payment_method, email_id: self.email_id, totalOrderData: self.totalOrderData), animated: true);
                    
                    }
                }
            }
        
        task.resume();
        
    }
    
    override func basicFoundationToRenderView(bottomMargin:CGFloat){
        
        self.imageViewOnTopHeight.constant = translateAccordingToDevice(CGFloat(40.0));
        
        _ = self.tabBarController?.tabBar.frame.size.height;
        // adding scrollview
        scrollView = UIScrollView();
        scrollView.translatesAutoresizingMaskIntoConstraints = false;
        scrollView.showsHorizontalScrollIndicator = false;
        scrollView.showsVerticalScrollIndicator = false;
        view.addSubview(scrollView);
        self.view.addConstraint(NSLayoutConstraint(item: scrollView, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 0));
        self.view.addConstraint(NSLayoutConstraint(item: scrollView, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 0));
        self.view.addConstraint(NSLayoutConstraint(item: scrollView, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.imageViewOnTop, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: 0));
        self.view.addConstraint(NSLayoutConstraint(item: scrollView, attribute: NSLayoutConstraint.Attribute.bottom, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.bottomLayoutGuide, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: -(bottomMargin)));
        
        // adding stackview
        stackView = UIStackView();
        stackView.translatesAutoresizingMaskIntoConstraints = false;
        stackView.axis  = NSLayoutConstraint.Axis.vertical;
        stackView.distribution  = UIStackView.Distribution.equalSpacing;
        stackView.alignment = UIStackView.Alignment.center;
        stackView.spacing   = 10.0;
        scrollView.addSubview(stackView);
        self.view.addConstraint(NSLayoutConstraint(item: stackView, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 0));
        self.view.addConstraint(NSLayoutConstraint(item: stackView, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 0));
        scrollView.addConstraint(NSLayoutConstraint(item: stackView, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: scrollView, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1, constant: 0));
        scrollView.addConstraint(NSLayoutConstraint(item: stackView, attribute: NSLayoutConstraint.Attribute.bottom, relatedBy: NSLayoutConstraint.Relation.equal, toItem: scrollView, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: 0));
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    //custom make order
    override func recieveResponse(data: Data?, requestUrl: String?, response: URLResponse?) {
        if let data  = data {
            guard var json = try? JSON(data:data) else{return;}
            json = json[0]
            if requestUrl ==  "mobiconnect/checkout/saveorder" {
                if(json["success"].stringValue == "true"){
                    self.defaults.removeObject(forKey: "guestEmail");
                    self.defaults.removeObject(forKey: "cartId");
                    self.defaults.setValue("0", forKey: "items_count")
                    let order_id = json["order_id"].stringValue;
                    let storyboard = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil);
                    let viewController = storyboard.instantiateViewController(withIdentifier: "orderPlacedViewController") as! OrderPlacedViewController;
                    viewController.order_id = order_id;
                    self.navigationController?.viewControllers = [viewController];
                    
                }
            }else if requestUrl == "mobiconnect/checkout/viewcart" {
                let jsonResponse = json
                if(jsonResponse["success"].stringValue.lowercased() == "false".lowercased()){
                    self.defaults.removeObject(forKey: "cartId")
                }
                self.totalOrderData["currency_code"] = jsonResponse["data"]["currency_code"].stringValue
                self.totalOrderData["currency_symbol"] = jsonResponse["data"]["currency_symbol"].stringValue
                self.totalOrderData["amounttopay"] = jsonResponse["data"]["total"][0]["amounttopay"].stringValue;
                self.totalOrderData["tax_amount"] = jsonResponse["data"]["total"][0]["tax_amount"].stringValue;
                self.totalOrderData["shipping_amount"] = jsonResponse["data"]["total"][0]["shipping_amount"].stringValue;
                self.totalOrderData["discount_amount"] = jsonResponse["data"]["total"][0]["discount_amount"].stringValue;
                self.totalOrderData["appliedcoupon"] = jsonResponse["data"]["total"][0]["coupon"].stringValue
                self.totalOrderData["grandtotal"] = jsonResponse["data"]["grandtotal"].stringValue;
            }
        }
    }
    
}
