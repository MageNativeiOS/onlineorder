/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */


import UIKit

class cedMageMyOrders: cedMageViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var emptyOrders: UIImageView!
    @IBOutlet weak var myOrders: UITableView!
    var page = 1
    var ordersData = [[String:String]]()
    var loading = true
    override func viewDidLoad() {
        super.viewDidLoad()
        topLabel.setThemeColor()
        topLabel.textColor = Settings.themeTextColor
        //myOrders.backgroundColor = .mageSecondarySystemBackground
        myOrders.register(OrderListTC.self, forCellReuseIdentifier: OrderListTC.reuseID)
        myOrders.delegate = self
        myOrders.dataSource = self
           let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
        let hashKey = userInfoDict["hashKey"]!;
      let  customerId = userInfoDict["customerId"]!;
        let postString = ["hashkey":hashKey,"customer_id":customerId,"page":"\(page)"];
        
        self.sendRequest(url: "mobiconnect/customer/order", params: postString)
        myOrders.rowHeight = UITableView.automaticDimension
        myOrders.estimatedRowHeight = 400
        // Do any additional setup after loading the view.
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func recieveResponse(data: Data?, requestUrl: String?, response: URLResponse?) {
        if let data = data {
            guard var json = try? JSON(data:data) else{
                return;
            }
        print(json)
        json = json[0]
        if(json["data"]["status"].stringValue == "no_order"){
            
            if(ordersData.count == 0){
                self.renderNoDataImage(view: self, imageName: "no_order")
          
            self.loading = false
            return
            }
        }
        let jsonCart = json["data"]["orderdata"];
        if(json["data"]["status"].stringValue == "success"){
        for c in 0..<jsonCart.arrayValue.count
        {
            let shipTo=jsonCart[c]["ship_to"].stringValue;
            let number=jsonCart[c]["number"].stringValue;
            let orderId=jsonCart[c]["order_id"].stringValue;
            let totalAmount=jsonCart[c]["total_amount"].stringValue;
            let orderStatus=jsonCart[c]["order_status"].stringValue;
            let date=jsonCart[c]["date"].stringValue;
            let v = ["shipTo":shipTo,"number":number,"orderId":orderId,"totalAmount":totalAmount,"orderStatus":orderStatus,"date":date];
            self.ordersData.append(v);
        }
            myOrders.reloadData()
        }else{
            if(ordersData.count != 0){
                loading = false
            }
        }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ordersData.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       /* let cell = tableView.dequeueReusableCell(withIdentifier: "myOrders") as! cedMageMyOrdersCell
        let shipTo = ordersData[indexPath.row]["shipTo"];
        let orderId = ordersData[indexPath.row]["orderId"];
        let totalAmmount = ordersData[indexPath.row]["totalAmount"];
        cell.orderIdHeading.text = "ORDER ID"
        cell.costheading.text = "COST".localized
        cell.shiptoHeading.text = "SHIP TO".localized
        cell.shipTo?.text = shipTo
        cell.orderId?.text = orderId
        cell.cost?.text = totalAmmount
        cell.calenderButton.setTitle(ordersData[indexPath.row]["date"], for: UIControl.State.normal);
        cell.calenderButton.fontColorTool()
        cell.statusButton.setTitle(ordersData[indexPath.row]["orderStatus"], for: UIControl.State.normal);
        cell.statusButton.fontColorTool()
        return cell */
        let cell = tableView.dequeueReusableCell(withIdentifier: OrderListTC.reuseID, for: indexPath) as! OrderListTC
        cell.populate(withOrder: ordersData[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140//UITableView.automaticDimension
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let orderId = ordersData[indexPath.row]["orderId"]
        let viewcontroll = UIStoryboard(name: "cedMageAccounts", bundle: nil).instantiateViewController(withIdentifier: "cedMageSingleOrder") as! cedMageSingleOrder
        viewcontroll.orderId = orderId
        self.navigationController?.pushViewController(viewcontroll, animated: true)
        
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        
        if (maximumOffset - currentOffset) <= 40 {
            if loading{
                page += 1
                let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
                let hashKey = userInfoDict["hashKey"]!;
                let  customerId = userInfoDict["customerId"]!;
                let postString = ["hashkey":hashKey,"customer_id":customerId,"page":"\(page)"];
                self.sendRequest(url: "mobiconnect/customer/order", params: postString)
              
            }
            
        }
        
    }
   
    
}
