//
//  cedMageSupportBrowseByCell.swift
//  ZeoMarket
//
//  Created by cedcoss on 04/04/19.
//  Copyright © 2019 MageNative. All rights reserved.
//

import UIKit

class cedMageSupportBrowseByCell: UITableViewCell {

    @IBOutlet weak var removeButton: UIButton!
    @IBOutlet weak var fileNamelabel: UILabel!
    @IBOutlet weak var browseByButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
