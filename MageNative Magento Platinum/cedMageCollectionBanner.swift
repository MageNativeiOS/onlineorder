//
//  cedMageCollectionBanner.swift
//  MageNative Magento Platinum
//
//  Created by CEDCOSS Technologies Private Limited on 07/02/17.
//  Copyright © 2017 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class cedMageCollectionBanner: UICollectionViewCell,KIImagePagerDataSource,KIImagePagerDelegate {
    
    @IBOutlet weak var bannerImage: KIImagePager!
    var dataSource = [String]()
    var fullData = [[String:String]]()
    var parent:UIViewController?
    var filled = true
    override func awakeFromNib() {
        super.awakeFromNib()
        bannerImage.delegate = self
        bannerImage.dataSource = self
    }
    //MARK: Delegate for KIImage Pager
    
    public func array(withImages pager: KIImagePager!) -> [Any]! {
        if dataSource.count == 0 {
            return ["placeholder" as AnyObject]
        }
        return dataSource as [AnyObject]?
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
    }
    
    func contentMode(forPlaceHolder pager: KIImagePager!) -> UIView.ContentMode {
        return UIView.ContentMode.scaleToFill
    }
    
    func contentMode(forImage image: UInt, in pager: KIImagePager!) -> UIView.ContentMode {
        return UIView.ContentMode.scaleToFill
    }
    
    func placeHolderImage(for pager: KIImagePager!) -> UIImage! {
        return UIImage(named: "bannerplaceholder")
    }
    
    func imagePager(_ imagePager: KIImagePager!, didSelectImageAt index: UInt) {
        let banner = fullData[Int(index)]
        if(banner["link_to"] == "category"){
            let viewcontoller = UIStoryboard(name: "categorylayouts", bundle: nil).instantiateViewController(withIdentifier: "cedMageDefaultCollection") as? cedMageDefaultCollection
            viewcontoller?.selectedCategory = banner["product_id"]!
            self.parent?.navigationController?.pushViewController(viewcontoller!, animated: true)
        }else if (banner["link_to"] == "product"){
            let productview = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "rootPageViewController") as! cedMageProductViewRoot
            productview.pageData = [["product_id":banner["product_id"]!]]
            let instance = cedMage.singletonInstance
            instance.storeParameterInteger(parameter: 0)
            self.parent?.navigationController?.pushViewController(productview
                , animated: true)
            
        }else if (banner["link_to"] == "website"){
            let view = UIStoryboard(name: "cedMageAccounts", bundle: nil)
            let viewControl = view.instantiateViewController(withIdentifier: "cmsWebView") as! cedMageCmsWebView
            let url = banner["product_id"]
            viewControl.pageUrl = url!
            self.parent?.navigationController?.pushViewController(viewControl, animated: true)
            
            
        }

    }
    

}
