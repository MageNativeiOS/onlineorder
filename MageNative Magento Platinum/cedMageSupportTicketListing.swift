//
//  cedMageSupportTicketListing.swift
//  ZeoMarket
//
//  Created by cedcoss on 01/04/19.
//  Copyright © 2019 MageNative. All rights reserved.
//

import UIKit

class cedMageSupportTicketListing: cedMageViewController {
    @IBOutlet weak var tableView: UITableView!
    
    var tickets = [[String:String]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTable()
        var url = "helpdeskapi/getalltickets/"
        let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]()
        if let cust_id = userInfoDict["customerId"] {
            url += cust_id
        }
        self.getRequest(url: url, store: false)
    }
    
    func setupTable(){
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        tableView.rowHeight = 150
    }
    

   override func recieveResponse(data: Data?, requestUrl: String?, response: URLResponse?) {
        if let data = data {
            do {
                var json = try JSON(data:data)
                json = json[0]
                print(json)
                if json["success"].stringValue == "true" {
                    for ticket in json["list"].arrayValue {
                tickets.append(["ticket_id":ticket["ticket_id"].stringValue,"department":ticket["department"].stringValue,  "created_time":ticket["created_time"].stringValue,"subject":ticket["subject"].stringValue,"status":ticket["status"].stringValue])
                    
                    }
                    tableView.reloadData()
                }else {
                    cedMageHttpException.showAlertView(me: self, msg: json["message"].stringValue, title: "Error")
                }
            }catch let error {
                print(error.localizedDescription)
            }
            
        }
    }
   

}

extension cedMageSupportTicketListing:UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tickets.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "cedMageSupportTicketView") as? cedMageSupportTicketView {
            let ticket = tickets[indexPath.row]
            cell.createdTime.text = ticket["created_time"]
            cell.department.text = ticket["department"]
            cell.status.text = ticket["status"]
            cell.subject.text = ticket["subject"]
            cell.ticketId.text = ticket["ticket_id"]
            
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let ticket = tickets[indexPath.row]
        if let viewcontrol = UIStoryboard(name: "cedMageAccounts", bundle: nil).instantiateViewController(withIdentifier: "cedMageSupportViewController") as? cedMageSupportViewController {
            viewcontrol.ticket = ticket
            self.navigationController?.pushViewController(viewcontrol, animated: true)
        }
    }
}
