//
//  HomeBannerTC.swift
//  MageNative Magento Platinum
//
//  Created by Hamza Usmani on 02/09/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class HomeBannerTC: UITableViewCell {
    
    //MARK:- Properties
    
    static var reuseId:String = "HomeWidgetBannerTC"
    var widgets:[HomeBanner]? = nil
    weak var parent:UIViewController?
    
    lazy var widgetView:FSPagerView = {
        let view = FSPagerView()
        view.automaticSlidingInterval = 4.0
        view.isInfinite = true
        view.dataSource = self
        view.delegate = self
        view.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell")
        return view
    }()
    
    lazy var pageControl:FSPageControl = {
        let view = FSPageControl()
        view.backgroundColor = .clear
        view.numberOfPages = 5
        view.interitemSpacing = 30.0
        view.hidesForSinglePage = true
        view.setImage(UIImage(named: "normalPageControl"), for: .normal)
        view.setImage(UIImage(named: "selectedPageControl"), for: .selected)
        return view
    }()
    
    //MARK:- Lifecycle
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor         = .clear
        selectionStyle          = .none
        contentView.isMultipleTouchEnabled  = true
        
        addSubview(widgetView)
        widgetView.anchor(top: topAnchor, left: leadingAnchor, bottom: bottomAnchor, right: trailingAnchor, paddingBottom: 20)
        
        addSubview(pageControl)
        pageControl.anchor(bottom: bottomAnchor, paddingBottom: 5)
        pageControl.centerX(inView: self)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func populate(with data:[HomeBanner], isMediumBanner: Bool, parent: UIViewController) {
        self.widgets = data
        self.parent = parent
        pageControl.numberOfPages = widgets?.count ?? 0
        if(widgets?.count == 1){
            widgetView.isInfinite = false;
        }
        else{
            widgetView.isInfinite = !isMediumBanner
        }
        widgetView.reloadData()
    }
}

//MARK:- FSPagerViewDataSource

extension HomeBannerTC: FSPagerViewDataSource {
    
    func numberOfItems(in pagerView: FSPagerView) -> Int {
        
        return widgets?.count ?? 0
    }
    
    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index)
        cell.imageView?.contentMode = .scaleAspectFit
        cell.imageView?.clipsToBounds = true
        if let url = URL(string: widgets?[index].banner_image ?? ""){
            cell.imageView?.sd_setImage(with: url, placeholderImage: UIImage(named: "placeholder"))
            cell.imageView?.contentMode = .scaleToFill
        }
        pageControl.currentPage = index
        return cell
    }
    
}

//MARK:- FSPagerViewDelegate
extension HomeBannerTC: FSPagerViewDelegate {
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        switch widgets?[index].link_to {
        case "category":
            let vc = UIStoryboard(name: "categorylayouts", bundle: nil).instantiateViewController(withIdentifier: "cedMageDefaultCollection") as! cedMageDefaultCollection
            vc.selectedCategory = widgets![index].product_id!
            parent?.navigationController?.pushViewController(vc, animated: true)
            return
        case "product":
            let vc = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "productSinglePageViewController") as! ProductSinglePageViewController
            vc.product_id = widgets![index].product_id!
            parent?.navigationController?.pushViewController(vc, animated: true)
            return
        default:
            let view = UIStoryboard(name: "cedMageAccounts", bundle: nil)
            let vc = view.instantiateViewController(withIdentifier: "cmsWebView") as! cedMageCmsWebView
            let url = widgets![index].product_id!
            vc.pageUrl = url
            parent?.navigationController?.pushViewController(vc, animated: true)
        }
    }
}
