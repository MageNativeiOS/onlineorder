/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */


import UIKit
//import SkyFloatingLabelTextField
class cedMageProfileView: cedMageViewController {
    let dropDown = DropDown()
    var hide_check:Dictionary<String,String> = [:]
    @IBOutlet weak var stack_height: NSLayoutConstraint!
    
    @IBOutlet weak var main_stack_height: NSLayoutConstraint!
    
    @IBOutlet weak var editProfileHeading: UILabel!
    
    var count=0;
    @IBOutlet weak var showPass: UIButton!
    var drop_option =   [String]();
    var datepicker:Dictionary<String,Any> = [:]
    var text:Dictionary<String,Any> = [:]
    var dropdown:Dictionary<String,Any> = [:]
    @IBOutlet weak var stack_view: UIStackView!
    @IBOutlet weak var topLabel: UILabel!
    //var oldPassword: SkyFloatingLabelTextField!
    //var confirmPassword: SkyFloatingLabelTextField!
    //var newPassword: SkyFloatingLabelTextField!
    @IBOutlet weak var update: UIButton!
    //var userEmail: SkyFloatingLabelTextField!
    var email = "";
    var hashKey = "";
    var customerId = "";
    var show = false
    var clr = cedMage.UIColorFromRGB(colorCode: "#3d3d3d")
    var first_name_data=""
    var last_name_data=""
    var values_fields:Dictionary<String,JSON>=[:]
    var dropdown_options:Dictionary<String,JSON>=[:]
    var gender = "";
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //topLabel.setThemeColor()
        update.setThemeColor()
        if #available(iOS 13.0, *) {
            clr = .label
        } else {
            clr = cedMage.UIColorFromRGB(colorCode: "#3d3d3d")
        }
        // Do any additional setup after loading the view.
        let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
        print(userInfoDict)
        email = userInfoDict["email"]!;
        
        hashKey = userInfoDict["hashKey"]!;
        customerId = userInfoDict["customerId"]!;
        editProfileHeading.text = "Edit Profile".localized
        //male.addTarget(self, action: #selector(cedMageProfileView.selectGenders(sender:)), for: .touchUpInside)
        //female.addTarget(self, action: #selector(cedMageProfileView.selectGenders(sender:)), for: .touchUpInside)
        //showPass.addTarget(self, action: #selector(cedMageProfileView.showPassWord(sender:)), for: UIControlEvents.touchUpInside)
        update.setTitle("Update".localized, for: .normal)
        update.addTarget(self, action: #selector(cedMageProfileView.updatePass(sender:)), for: UIControl.Event.touchUpInside)
        if let c_id=UserDefaults.standard.value(forKey: "userInfoDict") as? [String:String] {
            
            self.sendRequest(url: "mobiconnect/customer/getRequiredFields/\(c_id["customerId"]!)", params: nil)
        }
        
        
    }
    
    //    override func setupNav() {
    //        let backView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 20))
    //        backView.backgroundColor = UIColor.green
    //        self.view.insertSubview(backView, aboveSubview: (self.navigationController?.view)!)
    //        self.navigationController?.navigationBar.setBackgroundImage(UIImage.imageFromColor(color: UIColor.white, frame: CGRect(x: 0, y: 0, width: 300, height: 50)), for: UIBarMetrics.default)
    //        self.navigationController?.navigationBar.tintColor = .gray
    //    }
    
    
    
    
    
    //Mark: Select gender
    public  func selectGenders(sender:UIButton){
        //let colorString = cedMage.getInfoPlist(fileName: "cedMage", indexString: "themeColor") as? String
        //let imageView = UIImageView()
        //imageView.frame.size = CGSize(width: 20, height: 20)
        //imageView.image = UIImage(named: "check")
        //imageView.center = sender.center
        //imageView.tag = 112
        //if(sender == self.male){
        //male.layer.borderColor = cedMage.UIColorFromRGB(colorCode: colorString!).cgColor
        //male.layer.borderWidth = 4
        //male.layer.cornerRadius = male.frame.width/2
        //gender = "1"
        //female.layer.borderWidth = 0
        //}else if(sender == self.female){
        //female.layer.borderColor = cedMage.UIColorFromRGB(colorCode: colorString!).cgColor
        //female.layer.cornerRadius = male.frame.width/2
        //female.layer.masksToBounds = false
        //gender = "2"
        //female.layer.borderWidth = 4
        //male.layer.borderWidth = 0
        //}
        
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //    func showPassWord(sender:UIButton){
    //        if(!show){
    //            self.newPassword.isSecureTextEntry = false
    //            self.confirmPassword.isSecureTextEntry = false
    //            sender.setImage(#imageLiteral(resourceName: "hidePassword"), for: UIControlState.normal)
    //            show = true
    //        }else{
    //            self.newPassword.isSecureTextEntry = true
    //            self.confirmPassword.isSecureTextEntry = true
    //            sender.setImage(#imageLiteral(resourceName: "showPassword"), for: UIControlState.normal)
    //            show = false
    //        }
    //    }
    
    @objc func updatePass(sender:UIButton){
        
      
        
        if let prefix_button=dropdown["prefix"] as? UIButton {
            var prefix=prefix_button.currentTitle
            prefix=prefix?.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
        }
        
        if let suffix_button=dropdown["suffix"] as? UIButton {
            var suffix=suffix_button.currentTitle
            suffix=suffix?.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
        }
        
        if let gender_button=dropdown["gender"] as? UIButton {
            
            
            var gender=gender_button.currentTitle
            if gender=="Male"
            {
                gender=gender_data["Male"]
            }
            else if gender=="Female"
            {
                gender=gender_data["Female"]
            }
            else if gender=="Not Specified"
            {
                gender=gender_data["Not Specified"]
            }
            
        }
        
        
        let firstname_text=text["firstname"] as! SkyFloatingLabelTextField
        var firstname=firstname_text.text
        firstname=firstname?.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
    
        let lastname_text=text["lastname"] as! SkyFloatingLabelTextField
        var lastname=lastname_text.text
        lastname=lastname?.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
    
   
        
        let email_text=text["email"] as! SkyFloatingLabelTextField
        var email=email_text.text
        email=email?.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
  
        
        
         var middlename = ""
        if let middlename_text=text["middlename"] as? SkyFloatingLabelTextField {
         middlename=middlename_text.text!
        middlename=middlename.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
        }
        var taxvat = ""
        if let taxvat_text=text["taxvat"] as? SkyFloatingLabelTextField {
         taxvat=taxvat_text.text!
        taxvat=taxvat.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
        }
        var dob = ""
        if let dob_text=datepicker["dob"] as? SkyFloatingLabelTextField {
       
        let inputFormatter = DateFormatter()
        inputFormatter.dateFormat = "DD/MM/yyyy"
            if let showDate = inputFormatter.date(from: dob_text.text!) {
        inputFormatter.dateFormat = "MM/DD/YYYY"
        let resultString = inputFormatter.string(from: showDate)
       
        
         dob=resultString
        dob=dob.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
            }
        }
        let confirmPassword_text=text["confirm"] as! SkyFloatingLabelTextField
        var confirmPassword=confirmPassword_text.text
        confirmPassword=confirmPassword?.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
      
        
        let password_text=text["new"] as! SkyFloatingLabelTextField
        var password=password_text.text
        password=password?.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
        
        let oldPassword_text=text["old"] as! SkyFloatingLabelTextField
        var oldPassword=oldPassword_text.text

        oldPassword=oldPassword?.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)

        
        if(firstname == "" || lastname == ""  ||  email == "" || password == "" || confirmPassword == "" || oldPassword == "")
        {
            let title = "Error".localized
            let msg = "All fields are required!".localized
            cedMageHttpException.showAlertView(me: self, msg: msg, title: title)
            return;
        }
    
        if(password != confirmPassword)
        {
            let title = "Error".localized
            let msg = "Passwords not matched... Try again!".localized;
            cedMageHttpException.showAlertView(me: self, msg: msg, title: title)
            return;
        }
        let postString = ["hashkey":hashKey as String,"customer_id":customerId as String,"email":email! ,"firstname":firstname! as String,"lastname":lastname! as String,"middlename":middlename as String,"gender":gender as String,"new_password":password! as String,"confirm_password":confirmPassword! as String,"old_password":oldPassword! as String,"change_password":"1","taxvat":taxvat as String,"dob":dob ];
        
        self.sendRequest(url: "mobiconnect/customer/update", params: postString)
        
    }
    
    func show_fields()
    {
        if(hide_check["prefix"]=="true")
        {
            let prefix=["label":"Prefix".localized,"name":"prefix"]
            make_field(type: "dropdown", values: prefix)
        }
        
        
        let firstname=["label":"First Name".localized,"name":"firstname","value":first_name_data]
        make_field(type: "text", values: firstname)
        
        
        if(hide_check["middlename"]=="true")
        {
            let middlename=["label":"Middle Name".localized,"name":"middlename","value":String(describing: values_fields["middlename"]!)]
            make_field(type: "text", values: middlename)
        }
        
        
        let lastname=["label":"Last Name".localized,"name":"lastname","value":last_name_data]
        make_field(type: "text", values: lastname)
        
        
        if(hide_check["suffix"]=="true")
        {
            let suffix=["label":"Suffix".localized,"name":"suffix"]
            make_field(type: "dropdown", values: suffix)
        }
        
        if(hide_check["dob"]=="true")
        {
            let dob=["label":"DOB".localized,"name":"dob","value":String(describing: values_fields["dob"]!)]
            make_field(type: "datepicker", values: dob)
        }
        
        if(hide_check["gender"]=="true")
        {
            let gender=["label":"Gender".localized,"name":"gender"]
            make_field(type: "dropdown", values: gender)
        }
        
        if(hide_check["taxvat"]=="true")
        {
            let taxvat=["label":"Tax/Vat".localized,"name":"taxvat","value":String(describing: values_fields["taxvat"]!)]
            make_field(type: "text", values: taxvat)
        }
        
        let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
        print(userInfoDict)
        let email = userInfoDict["email"]!;
        print(email)
        let email1=["label":"Email".localized,"name":"email","value":email]
        print(email1)
        make_field(type: "text", values: email1)
   
        
        let old=["label":"Old Password(*)".localized,"name":"old","value":""]
        make_field(type: "text", values: old)
        
        let new=["label":"New Password(*)".localized,"name":"new","value":""]
        make_field(type: "text", values: new)
        
        let confirm=["label":"Confirm Password(*)".localized,"name":"confirm","value":""]
        make_field(type: "text", values: confirm)
    }
    
    
    
    
    
    var button_tag=100;
    func make_field(type: String,values: Dictionary<String,String>)
    {
        let ty = type
        //print(values)
        if(ty=="datepicker")
        {
            
            //scroll_view.contentSize.height+=CGFloat(160)
            stack_height.constant += CGFloat(60)
            main_stack_height.constant+=CGFloat(60)
            
            let f1=SkyFloatingLabelTextField(frame: CGRect(x: 0, y: count+20, width: Int(stack_view.frame.width), height: 40))
            f1.font = UIFont.systemFont(ofSize: 12)
            f1.placeholder=values["label"]
            if values["label"] == "DOB" {
                f1.placeholder = values["label"]! + "-DD/MM/YYYY"
            }
            f1.titleLabel.text=values["label"]
            f1.titleColor=clr
            f1.textColor=clr
            f1.fontColorTool()
            f1.tintColor=clr
            f1.selectedLineColor=clr
            f1.selectedTitleColor=clr
            let value=UserDefaults.standard.value(forKey: "mageAppLang") as! [String]
            if value[0]=="ar"
            {
                f1.textAlignment = .right
            }
            else
            {
                f1.textAlignment = .left
            }
              f1.text=values["value"]
            if values["label"] == "DOB" {
                if let dob_text=values["value"] {
                    
                    let inputFormatter = DateFormatter()
                    inputFormatter.dateFormat = "YYYY-MM-DD"
                    if let showDate = inputFormatter.date(from: dob_text) {
                        inputFormatter.dateFormat = "DD/MM/YYYY"
                        let resultString = inputFormatter.string(from: showDate)
                        
                        
                        f1.text=resultString
            }
                }
            }
            datepicker.updateValue(f1, forKey: values["name"]!)
            
            stack_view.addSubview(f1)
            
            count+=60
            
        }
        if(ty=="text")
        {
            //scroll_view.contentSize.height+=CGFloat(60)
            stack_height.constant += CGFloat(60)
            main_stack_height.constant+=CGFloat(60)
            
            let f1=SkyFloatingLabelTextField(frame: CGRect(x: 0, y: count+20, width: Int(stack_view.frame.width), height: 40))
            f1.font = UIFont.systemFont(ofSize: 12)
            f1.placeholder=values["label"]
            f1.titleLabel.text=values["label"]
            f1.text=values["value"]
            f1.titleColor=clr
            f1.fontColorTool()
            f1.textColor=clr
            f1.tintColor=clr
            f1.selectedLineColor=clr
            f1.selectedTitleColor=clr
            let value=UserDefaults.standard.value(forKey: "mageAppLang") as! [String]
            if value[0]=="ar"
            {
                f1.textAlignment = .right
                f1.titleLabel.textAlignment = .right
            }
            else
            {
                f1.textAlignment = .left
                f1.titleLabel.textAlignment = .left
            }
            
            if values["name"]=="email"
            {
                f1.isEnabled=false
                f1.text=email
            }
            if values["name"]=="new" || values["name"]=="confirm" || values["name"]=="old"{
                f1.isSecureTextEntry = true
            }
            text.updateValue(f1, forKey: values["name"]!)
            stack_view.addSubview(f1)
            count+=60
        }
        if(ty=="dropdown")
        {
            //scroll_view.contentSize.height+=CGFloat(80)
            let gender=CustomOptionDropDownView();
            gender.dropDownButton.setTitleColor(clr, for: .normal)
            gender.dropDownButton.fontColorTool()
            stack_height.constant += CGFloat(80)
            main_stack_height.constant+=CGFloat(80)
//            gender.topLabel.textColor=clr
//            gender.fontColorTool()
            gender.topLabel.text=values["label"]
            var temp=""
            gender.backgroundColor=UIColor.clear
            gender.frame = CGRect(x: 0, y: count+10, width: Int(stack_view.frame.width), height: 70)
            if gender.topLabel.text=="Prefix".localized
            {
                temp="Prefix"
                gender.dropDownButton.tag=444
            }
            else if gender.topLabel.text=="Suffix".localized
            {
                temp="Suffix"
                gender.dropDownButton.tag=555
            }
            else if gender.topLabel.text=="Gender".localized
            {
                temp="Gender"
                gender.dropDownButton.tag=666
            }
            if temp=="Prefix"
            {
                gender.dropDownButton.setTitle(String(describing: values_fields["prefix"]!), for: UIControl.State());
            }
            if temp=="Suffix"
            {
                gender.dropDownButton.setTitle(String(describing: values_fields["suffix"]!), for: UIControl.State());
            }
            if temp=="Gender"
            {
                
                let temp_g=String(describing: values_fields["gender"]!)
                if temp_g==""
                {
                    gender.dropDownButton.setTitle("", for: UIControl.State());
                }
                else if temp_g=="1"
                {
                    gender.dropDownButton.setTitle("Male", for: UIControl.State());
                }
                else if temp_g=="2"
                {
                    gender.dropDownButton.setTitle("Female", for: UIControl.State());
                }
                else if temp_g=="3"
                {
                    
                    gender.dropDownButton.setTitle("Not Specified", for: UIControl.State());
                }
                
            }
            gender.dropDownButton.addTarget(self, action: #selector(showOptionsDropdown(_:)), for: UIControl.Event.touchUpInside);
            dropdown.updateValue(gender.dropDownButton, forKey: values["name"]!)
            stack_view.addSubview(gender);
            count+=80
            button_tag+=1
        }
    }
    
    
    
    
    @objc func showOptionsDropdown(_ sender:UIButton){
        var ArrayToUse=[String]();
        if(sender.tag==666)
        {
            let options=dropdown_options["gender"]
            for (_,value) in options!
            {
                print(value)
                
                ArrayToUse.append(String(describing: value["label"]))
            }
        }
        if(sender.tag==444)
        {
            let options=dropdown_options["prefix"]
            print(options as Any)
            for (key,_) in options!
            {
                ArrayToUse.append(String(describing: options![key]))
            }
            
        }
        if(sender.tag==555)
        {
            let options=dropdown_options["suffix"]
            print(options as Any)
            for (key,_) in options!
            {
                ArrayToUse.append(String(describing: options![key]))
            }
            
        }
        
        ArrayToUse+=drop_option
        dropDown.dataSource = ArrayToUse;
        dropDown.selectionAction = {(index, item) in
            sender.fontColorTool()
            sender.setTitle(item, for: UIControl.State());
        }
        
        dropDown.anchorView = sender
        dropDown.bottomOffset = CGPoint(x: 0, y:sender.bounds.height)
        if dropDown.isHidden {
            let _ = dropDown.show();
        } else {
            dropDown.hide();
        }
    }
    
    
    
    
    
    
    func datePickerValueChanged(_ sender: UIDatePicker){
        // Create date formatter
        let dateFormatter: DateFormatter = DateFormatter()
        // Set date format
        dateFormatter.dateFormat = "YYYY-MM-dd"
        // Apply date format
        let selectedDate: String = dateFormatter.string(from: sender.date)
        let textfield=datepicker["dob"] as! SkyFloatingLabelTextField
        textfield.text=selectedDate;
        
    }
    
    var prefix_data:Dictionary<String,String> = [:]
    var suffix_data:Dictionary<String,String> = [:]
    var gender_data:Dictionary<String,String> = [:]
    override func recieveResponse(data: Data?, requestUrl: String?, response: URLResponse?) {
        guard var json = try? JSON(data:data!) else{return;}
        if requestUrl=="mobiconnect/customer/update"
        {
            print(json)
            json = json[0]
            if(json["data"]["customer"][0]["status"].stringValue == "error"){
                cedMageHttpException.showAlertView(me: self, msg: json["data"]["customer"][0]["message"].stringValue, title: "Error".localized)
            }
            
            if(json["data"]["customer"][0]["status"].stringValue == "success"){
                cedMageHttpException.showAlertView(me: self, msg: json["data"]["customer"][0]["message"].stringValue, title: "Success".localized)
            }
            if(json["data"]["customer"][0]["status"].stringValue == "false"){
                cedMageHttpException.showAlertView(me: self, msg: json["data"]["customer"][0]["message"].stringValue, title: "Error".localized)
            }
        }
        else
        {
            print(json)
            if json[0]["success"] == true
            {
                
                first_name_data=String(describing: json[0]["firstname"])
                last_name_data=String(describing: json[0]["lastname"])
                for (_,field) in json[0]["data"]
                {
                    
                    for (keys,values) in field
                    {
                        if keys=="prefix"
                        {
                            let v=String(describing: values);
                            if v=="true"
                            {
                                values_fields.updateValue(field["value"], forKey: "prefix")
                            }
                        }else if keys=="middlename"
                        {
                            let v=String(describing: values);
                            if v=="true"
                            {
                                values_fields.updateValue(field["value"], forKey: "middlename")
                            }
                        }else if keys=="dob"
                        {
                            let v=String(describing: values);
                            if v=="true"
                            {
                                values_fields.updateValue(field["value"], forKey: "dob")
                            }
                        }else if keys=="taxvat"
                        {
                            let v=String(describing: values);
                            if v=="true"
                            {
                                values_fields.updateValue(field["value"], forKey: "taxvat")
                            }
                        }else if keys=="suffix"
                        {
                            let v=String(describing: values);
                            if v=="true"
                            {
                                values_fields.updateValue(field["value"], forKey: "suffix")
                            }
                        }else if keys=="gender"
                        {
                            let v=String(describing: values);
                            if v=="true"
                            {
                                values_fields.updateValue(field["value"], forKey: "gender")
                            }
                        }
                        
                        
                        if(keys=="name")
                        {
                            let v=String(describing: values);
                            if(field[v]==false)
                            {
                                hide_check.updateValue("false", forKey: v)
                            }
                            else
                            {
                                hide_check.updateValue("true", forKey: v)
                                
                                if(field["type"]=="dropdown")
                                {
                                    if(values=="suffix")
                                    {
                                        //suffix_data=field["suffix_options"] as Dictionary<String,String>
                                        for (index,val) in field["suffix_options"]
                                        {
                                            suffix_data.updateValue(String(describing: val), forKey: index)
                                        }
                                        dropdown_options.updateValue(field["suffix_options"], forKey: "suffix")
                                    }
                                    else if(values=="prefix")
                                    {
                                        for (index,val) in field["prefix_options"]
                                        {
                                            prefix_data.updateValue(String(describing: val), forKey: index)
                                        }
                                        dropdown_options.updateValue(field["prefix_options"], forKey: "prefix")
                                    }
                                    else if(values=="gender")
                                    {
                                        for (_,val) in field["gender_options"]
                                        {
                                            gender_data.updateValue(String(describing: String(describing: val["value"])), forKey: String(describing: val["label"]))
                                        }
                                        dropdown_options.updateValue(field["gender_options"], forKey: "gender")
                                    }
                                    
                                }
                            }
                        }
                    }
                }
                show_fields()
            }
            
        }
    }
    
}
