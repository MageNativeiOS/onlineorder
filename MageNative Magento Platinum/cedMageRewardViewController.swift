//
//  cedMageRewardViewController.swift
//  MageNative Magento Platinum
//
//  Created by Macmini on 13/12/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class cedMageRewardViewController: MagenativeUIViewController,UITableViewDelegate,UITableViewDataSource, UIScrollViewDelegate {
  
  @IBOutlet weak var tableView: UITableView!
  var loading = true;
  //    var page = 1
  var page = 1
  var rewardPoints = [[String:String]]()
  var rewardDetail = [String:String]()
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    tableView.delegate = self
    tableView.dataSource = self
    tableView.separatorStyle = .none
    // Do any additional setup after loading the view.
    if defaults.bool(forKey: "isLogin") {
      var url = "mobireward/reward/list/"
      let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
      if let cust_id = userInfoDict["customerId"] {
        url += cust_id
      }
        
        let storeID = UserDefaults.standard.value(forKey: "storeId") as? String ?? ""
        url += "/\(storeID)"
        
      url += "/\(page)"
      self.getRequest(url: url, store: false)
    }
  }
  
  override func recieveResponse(data: Data?, requestUrl: String?, response: URLResponse?) {
    if let data = data {
      do {
        var json = try JSON(data:data)
        json = json[0]
        
        print(json)
        
        if json["points"].arrayValue.count >  0 {
          for point in json["points"].arrayValue {
            var tempArray = [String:String]()
            tempArray["refer_code"] = point["refer_code"].stringValue
            tempArray["title"] = point["title"].stringValue
            tempArray["point"] = point["point"].stringValue
            tempArray["creating_date"] = point["creating_date"].stringValue
            tempArray["status"] = point["status"].stringValue
            self.rewardPoints.append(tempArray)
            
          }
          rewardDetail["point_price"] = json["point_price"].stringValue
          rewardDetail["total"] = json["total"].stringValue
          tableView.reloadData()
        }
        else
        {
          //  cedMageHttpException.showAlertView(me: self, msg: "No reward points.", title: "Error".localized )
          let alert = UIAlertController(title: "Error", message: "No reward points", preferredStyle: .alert)
          let ok = UIAlertAction(title: "Ok", style: .destructive) { (UIAlertAction) in
            self.navigationController?.popViewController(animated: true)
          }
          alert.addAction(ok)
          self.present(alert, animated: true)
          
          return
        }
      }catch let error {
        print(error.localizedDescription)
      }
    }
  }
  
  
  func numberOfSections(in tableView: UITableView) -> Int {
    if rewardPoints.count > 0 {
      return 1
    }
    return 0
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return rewardPoints.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "cedMageRewardCell", for: indexPath) as! cedMageRewardCell
    let reward = self.rewardPoints[indexPath.row]
    cell.pointlabel.text = reward["point"]
    cell.titleLabel.text = reward["title"]
    cell.titleLabel.numberOfLines = 0
    cell.creatingDateLabel.text = reward["creating_date"]
    cell.statusLabel.text = reward["status"]
    cell.refercodeLabel.text = reward["refer_code"]
    cell.outerView.cardView()
    return cell
  }
  
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return UITableView.automaticDimension
  }
  
  func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    let cell = tableView.dequeueReusableCell(withIdentifier: "cedMageheaderCell") as! cedMageheaderCell
    cell.leftLabel.setThemeColor()
    cell.rightLabel.setThemeColor()
    cell.leftLabel.text = "1 Point = " +   rewardDetail["point_price"]!
    cell.rightLabel.text = "Total Points =" + rewardDetail["total"]!
    return cell.contentView
  }
  
  func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    return 45
  }
  
  func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
    let currentOffset = scrollView.contentOffset.y
    let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
    if (maximumOffset - currentOffset) <= 40 {
      if loading{
        self.page += 1;
        self.reloadData();
      }
    }
  }
  
  @objc func reloadData(){
    var url = "mobireward/reward/list/"
    let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
    if let cust_id = userInfoDict["customerId"] {
      url += cust_id
    }
    url += "/\(page)"
    self.getRequest(url: url, store: false)
  }
}
