//
//  ApiHandler.swift
//  MageNative Magento Platinum
//
//  Created by Hamza Usmani on 16/07/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//


import Foundation


class ApiHandler {
    
    
    static let handle = ApiHandler()
    private init() {}
    
    enum RequestType {
        case GET
        case POST
    }
    
    func request(with url: String, params: [String:String]?=nil, requestType: RequestType, controller: UIViewController, completion:(@escaping (_ json: Data?,_ error: Error?)-> Void)) {
        
        
        cedMageLoaders.addDefaultLoader(me: controller)
        
        
        DispatchQueue.global(qos: .background).async {
            
            let baseString      = Settings.baseUrl
            
            let endPoint        = baseString + url
            guard let finalUrl  = URL(string: endPoint) else {return}
            
            var stringToPost = [String:[String:String]]()
            var postJsonString  = ""
            
            var request = URLRequest(url: finalUrl)
            
            // Checking for Request Type
            
            switch requestType {
            case .GET:
                request.httpMethod    = "GET"
            default:
                request.httpMethod    = "POST"
                if let param          = params {
                    
                    stringToPost        = ["parameters":[:]]
                    for (key,value) in param
                    {
                        _ = stringToPost["parameters"]?.updateValue(value, forKey:key)
                    }
                    postJsonString      = stringToPost.convtToJson() as String
                    request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                    //request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
                    request.httpBody = postJsonString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
                    if UserDefaults.standard.bool(forKey: "isLogin"){
                        if let user = UserDefaults.standard.object(forKey: "userInfoDict") as? [String:String] {
                            request.setValue(user["hashKey"], forHTTPHeaderField: "hashkey")
                        }
                        
                    }
                }
            }
            
            
            print(finalUrl)
            print(postJsonString)
            
            
            
            URLSession.shared.dataTask(with: request) { (data, response, error) in
                
                if let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode != 200 {
                    
                    print("status code should be 200 but is \(httpResponse.statusCode)")
                    print("Bad response is \(httpResponse)")
                    completion(data,error)
                }
                
                guard error == nil && data != nil else {
                    
                    DispatchQueue.main.async {
                        cedMageLoaders.removeLoadingIndicator(me: controller)
                        controller.view.makeToast("Error is " + error!.localizedDescription, duration: 2.0, position: .center)
                        
                    }
                    return
                }
                
                DispatchQueue.main.async
                    {
                        cedMageLoaders.removeLoadingIndicator(me: controller)
                        completion(data,error)
                }
                
                
                
                
            }.resume()
            
        }  
        
    }
    func requestDict(with url: String, params: [String:Any]?=nil, requestType: RequestType, controller: UIViewController, completion:(@escaping (_ json: Data?,_ error: Error?)-> Void)) {
        
        
        cedMageLoaders.addDefaultLoader(me: controller)
        
        
        DispatchQueue.global(qos: .background).async {
            
            let baseString      = Settings.baseUrl
            
            let endPoint        = baseString + url
            guard let finalUrl  = URL(string: endPoint) else {return}
            
            var stringToPost = [String:[String:Any]]()
            var postJsonString  = ""
            
            var request = URLRequest(url: finalUrl)
            
            // Checking for Request Type
            
            switch requestType {
            case .GET:
                request.httpMethod    = "GET"
            default:
                request.httpMethod    = "POST"
                if let param          = params {
                    
                    stringToPost        = ["parameters":[:]]
                    for (key,value) in param
                    {
                        _ = stringToPost["parameters"]?.updateValue(value, forKey:key)
                    }
                    postJsonString      = stringToPost.convtToJson() as String
                    request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                    //request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
                    request.httpBody = postJsonString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
                    if UserDefaults.standard.bool(forKey: "isLogin"){
                        if let user = UserDefaults.standard.object(forKey: "userInfoDict") as? [String:String] {
                            request.setValue(user["hashKey"], forHTTPHeaderField: "hashkey")
                        }
                        
                    }
                }
            }
            
            
            print(finalUrl)
            print(postJsonString)
            
            
            
            URLSession.shared.dataTask(with: request) { (data, response, error) in
                
                if let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode != 200 {
                    
                    print("status code should be 200 but is \(httpResponse.statusCode)")
                    print("Bad response is \(httpResponse)")
                    completion(data,error)
                }
                
                guard error == nil && data != nil else {
                    
                    DispatchQueue.main.async {
                        cedMageLoaders.removeLoadingIndicator(me: controller)
                        controller.view.makeToast("Error is " + error!.localizedDescription, duration: 2.0, position: .center)
                        
                    }
                    return
                }
                
                DispatchQueue.main.async
                    {
                        cedMageLoaders.removeLoadingIndicator(me: controller)
                        completion(data,error)
                }
                
                
                
                
            }.resume()
            
        }
        
    
    
}
}
