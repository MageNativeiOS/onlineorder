/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */

import UIKit
import QuartzCore

extension UIView {
    func rotate360Degrees(duration: CFTimeInterval = 1.5) {
        let rotateAnimation = CABasicAnimation(keyPath: "transform.rotation")
        rotateAnimation.fromValue = 0.0
        rotateAnimation.toValue = CGFloat(M_PI * 2.0)
        rotateAnimation.duration = duration
        self.layer.add(rotateAnimation, forKey: nil)
    }
}

extension UIView {
    func shake() {
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.07;
        animation.repeatCount = 3;
        animation.autoreverses = true;
        animation.fromValue = NSValue(cgPoint: CGPoint(x: self.center.x - 10, y: self.center.y));
        animation.toValue = NSValue(cgPoint: CGPoint(x: self.center.x + 10, y: self.center.y));
        self.layer.add(animation, forKey: "position");
    }
}

extension String {
    var capitalizingFirstLetter: String {
        let first = String(self.prefix(1)).capitalized
        let other = String(self.dropFirst())
        return first + other
    }
    
    var localized: String {
        let lang = "en";//let lang = "fr";
        let path = Bundle.main.path(forResource: lang, ofType: "lproj");
        let bundle = Bundle(path: path!);
        return NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: "");
    }
}


extension UIFont{
    public convenience init?(fontName: String, fontSize: CGFloat) {
        var fontName = fontName
        if(fontName == ""){
            fontName = "Helvetica Neue";
        }
        self.init(name: fontName, size: translateAccordingToDevice(CGFloat(fontSize)));
        return;
    }
}

extension UIColor {
    public convenience init?(hexString: String) {
        var cString:String = hexString.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines as NSCharacterSet as CharacterSet).uppercased();
        if (cString.hasPrefix("#")) {
            cString = cString.substring(from: cString.index(cString.startIndex, offsetBy: 1))
        }
        if ((cString.count) != 6) {
            return nil;
        }
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        let r = CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0;
        let g = CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0;
        let b = CGFloat(rgbValue & 0x0000FF) / 255.0;
        let a =  CGFloat(1.0);
        self.init(red: r, green: g, blue: b, alpha: a)
        return
    }
}
extension UILabel {
    func resizeHeightToFit(_ text:String) {
        let attributes = [NSAttributedString.Key.font : self.font as AnyObject];
        numberOfLines = 0
        lineBreakMode = NSLineBreakMode.byWordWrapping
        let rect = text.boundingRect(with: CGSize(width: self.bounds.width, height: CGFloat.greatestFiniteMagnitude), options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: attributes, context: nil)
        self.text = text;
        self.fontColorTool()
        self.backgroundColor = UIColor.green;
        let heightConstraint = NSLayoutConstraint(item: self, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: rect.height)
        self.superview!.addConstraint(heightConstraint);
        setNeedsLayout();
    }
}
extension UINavigationController {
    func pushToViewController(_ viewController: UIViewController, animated:Bool = true, completion: @escaping ()->()) {
        CATransaction.begin()
        CATransaction.setCompletionBlock(completion)
        self.pushViewController(viewController, animated: animated)
        CATransaction.commit()
    }
    
    func popViewController(animated:Bool = true, completion: @escaping ()->()) {
        CATransaction.begin()
        CATransaction.setCompletionBlock(completion)
        self.popViewController(animated: true)
        CATransaction.commit()
    }
    
    func popToViewController(_ viewController: UIViewController, animated:Bool = true, completion: @escaping ()->()) {
        CATransaction.begin()
        CATransaction.setCompletionBlock(completion)
        self.popToViewController(viewController, animated: animated)
        CATransaction.commit()
    }
    
    func popToRootViewController(animated:Bool = true, completion: @escaping ()->()) {
        CATransaction.begin()
        CATransaction.setCompletionBlock(completion)
        self.popToRootViewController(animated: animated)
        CATransaction.commit()
    }
}
