
    /**
     * CedCommerce
     *
     * NOTICE OF LICENSE
     *
     * This source file is subject to the End User License Agreement (EULA)
     * that is bundled with this package in the file LICENSE.txt.
     * It is also available through the world-wide-web at this URL:
     * http://cedcommerce.com/license-agreement.txt
     *
     * @category  Ced
     * @package   MageNative
     * @author    CedCommerce Core Team <connect@cedcommerce.com >
     * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
     * @license      http://cedcommerce.com/license-agreement.txt
     */


import UIKit
import LocalAuthentication
import AuthenticationServices

class cedMageLogin: cedMageViewController,LoginButtonDelegate,GIDSignInDelegate {
    @IBOutlet weak var statusBarView: UIView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var cartButton: UIButton!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var loginBackground: UIImageView!
    @IBOutlet weak var userName: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var fbLogin: FBLoginButton!
    @IBOutlet weak var googleLogin: GIDSignInButton!
    @IBOutlet weak var registerHere: UIButton!
    @IBOutlet weak var forgetPassword: UIButton!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var orLabel: UILabel!
    @IBOutlet weak var socialLoginViw: UIView!
    
    
    
    var total=[String:String]()
    //@IBOutlet weak var applesignInview: UIView!
    
    var email = String()
    var isFromCheckOut = Bool()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        userName.layer.cornerRadius = 8
        password.layer.cornerRadius = 8
        userName.clipsToBounds = true
        password.clipsToBounds = true
        loginButton.backgroundColor=themecolor
        createAppleSignInButton()
        
        userName.placeholder = "UserName".localized
        password.placeholder = "Password".localized
        loginButton.setTitle("Login".localized, for: .normal)
        registerHere.setTitle("Sign Up With Us".localized, for: .normal)
        forgetPassword.setTitle("Forgot Password".localized, for: .normal)
        orLabel.text = "OR".localized
       
        
        
        /*userName.placeholderColor(UIColor.black)
        password.placeholderColor(UIColor.black)
        backButton.tintColor = .white
        loginButton.backgroundColor=themecolor
        cartButton.tintColor = themecolor
        cartButton.backgroundColor = .white
        
        searchButton.tintColor = themecolor
        searchButton.backgroundColor = .white
        
        backButton.tintColor = themecolor
        backButton.backgroundColor = .white*/
        
        loginButton.setTitleColor( .white,for: .normal)
        backButton.addTarget(self, action: #selector(showNavigationBar), for: .touchUpInside)
        searchButton.addTarget(self, action: #selector(showNavigationBar), for: .touchUpInside)
        cartButton.addTarget(self, action: #selector(showNavigationBar), for: .touchUpInside)
        if(self.navigationController?.viewControllers.count == 1){
            backButton.isHidden = true
        }
        loginButton.layer.cornerRadius = 5
        let googlesignin = GIDSignIn.sharedInstance()
        googlesignin?.presentingViewController = self;
        googlesignin?.clientID = cedMage.getInfoPlist(fileName: "GoogleService-Info", indexString: "CLIENT_ID") as? String
        googlesignin?.scopes.append("https://www.googleapis.com/auth/plus.login")
        googlesignin?.scopes.append("https://www.googleapis.com/auth/plus.me")
        fbLogin.delegate = self
        fbLogin.permissions = ["email","public_profile"]
        
        googlesignin?.shouldFetchBasicProfile = true
        googlesignin?.delegate = self
        loginButton.addTarget(self, action: #selector(cedMageLogin.addLogin(sender:)), for: UIControl.Event.touchUpInside)
        backButton.addTarget(self,action: #selector(cedMageViewController.backfunc(sender:)),for:.touchUpInside)
        searchButton.addTarget(self,action: #selector(cedMageViewController.gotoSearch(sender:)),for:.touchUpInside)
        cartButton.addTarget(self, action:#selector(cedMageViewController.gotoCart(sender:)) , for: UIControl.Event.touchUpInside)
//        statusBarView.setThemeColor()
         self.navigationController?.navigationBar.isHidden = true
        let label = UILabel();
        label.tag = 123;
        label.font = label.font.withSize(10)
        label.textAlignment = .center
        label.textColor = .white
        label.backgroundColor = themecolor
        if let cartval = defaults.value(forKey: "items_count") as? String{
            label.text = cartval
        }
        else{
            label.text = "0"
        }
        label.frame.origin.x = 18
        label.frame.origin.y = -5
        label.frame.size = CGSize(width: 15, height: 15)
        label.layer.cornerRadius = 8
        label.layer.masksToBounds = true
        cartButton.addSubview(label)
    }
    
    @objc func showNavigationBar(){
        self.navigationController?.navigationBar.isHidden = false;
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false;
    }
 
    
    //Mark: LOgin Function
    @objc func addLogin(sender:UIButton){
        email = self.userName.text!
        email = email.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
        var password = self.password.text
        password = password!.trimmingCharacters(in:NSCharacterSet.whitespacesAndNewlines);
        if(email == "" || password == "")
        {
            let title = "Error".localized
            let msg = "Email Or Password is Empty.".localized
            cedMageHttpException.showAlertView(me: self, msg: msg, title: title)
            return;
        }
        var postString = ["email":email,"password":password!]
        if(defaults.object(forKey: "cartId") != nil)
        {
            let cart_id = defaults.object(forKey: "cartId") as? String
            postString.updateValue(cart_id!, forKey: "cart_id")
        }
        self.sendRequest(url: "mobiconnect/customer/login/", params: postString)

    }
    
    override  func recieveResponse(data: Data?, requestUrl: String?, response: URLResponse?) {
        print(NSString(data: data!, encoding: String.Encoding.utf8.rawValue) as Any)
        print("!@!@!@!@!@")
        guard let response = try? JSON(data:data!) else{return;}
        if(response[0]["data"]["customer"][0]["status"].stringValue=="success"){
            print(response)
            if response[0]["data"]["customer"][0]["isConfirmationRequired"].stringValue.lowercased() == "YES".lowercased(){
                LoginManager().logOut()
                cedMageHttpException.showAlertView(me: self, msg: response[0]["data"]["customer"][0]["message"].stringValue, title: "Error")
                return
            }
            let customer_id = response[0]["data"]["customer"][0]["customer_id"].stringValue;
            let hashKey = response[0]["data"]["customer"][0]["hash"].stringValue;
            let cart_summary = response[0]["data"]["customer"][0]["cart_summary"].intValue;
            let name=response[0]["data"]["customer"][0]["name"].stringValue;
            //saving value in NSUserDefault to use later on :: start
            let dict = ["email": email, "customerId": customer_id, "hashKey": hashKey];
            print(dict)
            self.setNotification(email: email)
            print("@$@$@$@$@$@$@$@$")
            self.defaults.set(name, forKey: "name")
            self.defaults.set(true, forKey: "isLogin")
            self.defaults.set(dict, forKey: "userInfoDict");
            self.defaults.set(String(cart_summary), forKey: "items_count");
            self.setCartCount(view: self, items: String(cart_summary))
            defaults.removeObject(forKey: "cartId")
            NotificationCenter.default.post(name: NSNotification.Name("loadDrawerAgain"), object: nil)
            self.navigationController?.view.makeToast("Login Successful", duration: 2.0, position: .center, title: nil, image: nil, style: nil, completion: nil)
            cedMage.delay(delay: 2, closure: {
                 NotificationCenter.default.post(name: NSNotification.Name("loadDrawerAgain"), object: nil);
                self.navigationController?.navigationBar.isHidden = false
                if !self.isFromCheckOut {
                    let viewControl = HomeController()//UIStoryboard(name: "homeLayouts", bundle: nil).instantiateViewController(withIdentifier: "baseHomepageController") as? baseHomepageController
                    self.navigationController?.navigationBar.isHidden = false
                    self.navigationController?.setViewControllers([viewControl], animated: true)
                }else{
                    if(cedMage.checkModule(string:"MageNative_Mobicheckout")){
                        print("inside check")
                        let storyboard = UIStoryboard(name: "cedMageAccounts", bundle: nil);
                        let viewController = storyboard.instantiateViewController(withIdentifier: "cedMageAdvancedCheckout") as! cedMageAdvancedCheckout;
                        self.navigationController?.pushViewController(viewController, animated: true);
                    }
                    else{
                        let storyboard = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil);
                        let viewController = storyboard.instantiateViewController(withIdentifier: "checkoutStep2ViewController") as! CheckoutStep2ViewController;
                        viewController.totalOrderData = self.total
                        viewController.checkoutAs = "USER";
                        self.navigationController?.pushViewController(viewController, animated: true);
                    }
                }
                //_ =  self.navigationController?.popToRootViewController(animated: true)
            })
        }else{
            cedMageHttpException.showAlertView(me: self, msg: "Invalid Login or Password", title: "Error".localized)
        }
    }
    
    //mark: delegates from google and facebook login
    
    func loginButton(_ loginButton: FBLoginButton!, didCompleteWith result: LoginManagerLoginResult!, error: Error!) {
        self.navigationController?.navigationBar.isHidden = true;
        if(result == nil){
            return
        }
        let graphRequest : GraphRequest = GraphRequest(graphPath: "me", parameters: ["fields":"id,email,name,first_name,last_name"])
        graphRequest.start(completionHandler: { (connection, result, error) -> Void in
            if ((error) != nil)
            {
                print("Error: \(String(describing: error))")
            }
            else
            {
               //let user_id = AccessToken.current?.userID
                let user_id = AccessToken.current!.appID
                let firstname = (result as AnyObject).value(forKey: "first_name") as! String
                let lastname = (result as AnyObject).value(forKey: "last_name") as! String
                if let email = (result as AnyObject).value(forKey: "email") as? String {
                    let fbuser = ["firstname":firstname,"lastname":lastname,"email":email,"type":"facebook", "token":user_id]
                    print(fbuser)
                    self.doLOGIn(data: fbuser)
                }
                else {
                    let storyboard = UIStoryboard(name: "cedMageLogin", bundle: nil);
                    let viewController = storyboard.instantiateViewController(withIdentifier: "cedMageRegister") as! cedMageRegister;
                    self.navigationController?.pushViewController(viewController, animated: true);
                    viewController.Firstname=firstname
                    viewController.Lastname=lastname
                }
            }
        })
    }
    
    func doLOGIn(data:[String:String]){
        email=data["email"]!
        var postString = ["firstname":data["firstname"]!,"lastname":data["lastname"]!,"type":data["type"]!, "token":data["token"]! ];
        //postString += "&email="+data["email"]!
        postString.updateValue(data["email"]!, forKey: "email")
        if(defaults.object(forKey: "cartId") != nil)
        {
            let cart_id = defaults.object(forKey: "cartId") as? String;
            //postString += "&cart_id="+cart_id!;
            postString.updateValue(cart_id!, forKey: "cart_id")
        }
        print(postString);
//        defaults.setValue(data["firstname"], forKey: "name")
        
        self.sendRequest(url:"mobiconnect/sociallogin/create/", params: postString)
    }
    
    func loginButtonDidLogOut(_ loginButton: FBLoginButton!) {
        print("User Logged Out")
        defaults.setValue("MageNative App For Magento 2",forKey: "name");
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        
    }
    
  func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
    viewController.modalPresentationStyle = .fullScreen;
    self.present(viewController, animated: true, completion: nil)
    
  }
    
    func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController!) {
        self.navigationController?.navigationBar.isHidden = true;
        self.dismiss(animated: true, completion: nil)
        
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        self.navigationController?.navigationBar.isHidden = true;

        if (error == nil) {
            let user_id=user.userID!
            // Perform any operations on signed in user here.
            let givenName = user.profile.givenName as String
            let familyName = user.profile.familyName as String
            let email = user.profile.email as String
            let user = ["firstname":givenName,"lastname":familyName,"email":email,"type":"google", "token":user_id]
            print("--------Google------")
            print(email)
            self.doLOGIn(data: user)
            // ...
        } else {
            print("\(error.localizedDescription)")
        }
    }
    
    @IBAction func linkedInSignInClicked(_ sender: UIButton) {
        
        let vc=self.storyboard?.instantiateViewController(withIdentifier: "LinkedInWebViewController") as? LinkedInWebViewController;
        vc?.modalPresentationStyle = .fullScreen;
        self.present(vc!, animated: true, completion: {
            //
        })
    }
    
    func checkForExistingAccessToken() {
        if UserDefaults.standard.object(forKey: "LIAccessToken") != nil {
            //btnSignIn.isEnabled = false
            //btnGetProfileInfo.isEnabled = true
            if let accessToken = UserDefaults.standard.object(forKey: "LIAccessToken") {
                // Specify the URL string that we'll get the profile info from.
                let targetURLString = "https://api.linkedin.com/v1/people/~:(id,public-profile-url,first-name,last-name,email-address)?format=json"
                // Initialize a mutable URL request object.
                let request = NSMutableURLRequest(url: NSURL(string: targetURLString)! as URL)
                
                // Indicate that this is a GET request.
                request.httpMethod = "GET"
                
                // Add the access token as an HTTP header field.
                request.addValue("Bearer  \(accessToken)", forHTTPHeaderField: "Authorization")
                
                // Initialize a NSURLSession object.
                let session = URLSession(configuration: URLSessionConfiguration.default)
                
                // Make the request.
                let task: URLSessionDataTask = session.dataTask(with: request as URLRequest) { (data, response, error) -> Void in
                    // Get the HTTP status code of the request.
                    let statusCode = (response as! HTTPURLResponse).statusCode
                    
                    if statusCode == 200 {
                        // Convert the received JSON data into a dictionary.
                        do {
                            let dataDictionary = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                            print("response dictionary ::: \(dataDictionary)")
                            
                            DispatchQueue.main.async(execute: { () -> Void in
                                if let profileURLString = dataDictionary["publicProfileUrl"] {
                                    print(profileURLString);
                                }
                                if let firstName = dataDictionary["firstName"] {
                                    print(firstName);
                                }
                                if let lastName = dataDictionary["lastName"] {
                                    print(lastName);
                                }
                                if let userId = dataDictionary["id"] {
                                    print(userId);
                                }
                                //print(dataDictionary["emailAddress"])
                                if let email = dataDictionary["emailAddress"] {
                                    print(email);
                                }
                                let linkedInUser = ["firstname":dataDictionary["firstName"],"lastname":dataDictionary["lastName"],"email":dataDictionary["emailAddress"]]
                                print(linkedInUser)
                                self.doLOGIn(data: linkedInUser as! [String : String])

                            })
                        }
                        catch {
                            print("Could not convert JSON data into a dictionary.")
                        }
                    }
                }
                
                task.resume()
            }

        }
    }
   
    func authenticateUser() {
        let authContext : LAContext = LAContext()
        var error: NSError?
        
        if authContext.canEvaluatePolicy(LAPolicy.deviceOwnerAuthenticationWithBiometrics, error: &error){
            authContext.evaluatePolicy(LAPolicy.deviceOwnerAuthenticationWithBiometrics, localizedReason: "Biometric Check for application".localized, reply: {successful, error -> Void in
                if successful{
                    print("successfull........")
                    //self.auth_button.isEnabled=false
                    OperationQueue.main.addOperation({ () -> Void in
                        self.login_touch_id()
                        //self.auth_button.isHidden=true
                        //self.view.backgroundColor=UIColor.blue
                    })
                    
                    
                }
                else{
                    print(error?.localizedDescription as Any)
                    /*let viewControl = UIStoryboard(name: "homeLayouts", bundle: nil).instantiateViewController(withIdentifier: "baseHomepageController") as? baseHomepageController */
                    let viewControl = HomeController()
                    self.navigationController?.navigationBar.isHidden = false
                    self.navigationController?.setViewControllers([viewControl], animated: true)
                    
                }
            }
            )
        }
        else{
            /*let viewControl = UIStoryboard(name: "homeLayouts", bundle: nil).instantiateViewController(withIdentifier: "baseHomepageController") as? baseHomepageController*/
            let viewControl = HomeController()
            self.navigationController?.navigationBar.isHidden = false
            self.navigationController?.setViewControllers([viewControl], animated: true)
            switch error!.code{
                
            case LAError.Code.touchIDNotEnrolled.rawValue:
                print("TouchID is not enrolled")
                
            case LAError.Code.passcodeNotSet.rawValue:
                print("A passcode has not been set")
                
            default:
                // The LAError.TouchIDNotAvailable case.
                print("TouchID not available")
            }
            
            // Optionally the error description can be displayed on the console.
            print(error?.localizedDescription as Any)
            
            // Show the custom alert view to allow users to enter the password.
            
        }
    }
    func login_touch_id()
    {
        let temp=UserDefaults.standard.value(forKey: "touch_id") as! [String:Any]
        email=temp["email"] as! String
        print("*********************%%%%%%%%%%%^%^%^%^^%^%")
        let email1=temp["email"] as! String
        let password1=temp["password"] as! String
        var postString = ["email":email1,"password":password1,"iswindow":"true"];
        print(postString)
        if(defaults.object(forKey: "cartId") != nil)
        {
            let cart_id = defaults.object(forKey: "cartId") as? String;
            postString.updateValue(cart_id!, forKey: "cart_id")
        }
        
        self.sendRequest(url: "mobiconnect/customer/login/", params: postString)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "cedRegister" {
            if let viewController = segue.destination as? cedMageRegister {
                viewController.isCheckOut = isFromCheckOut
                viewController.total = total
            }
        }
    }
}

extension UIImageView{
    func loadBlur(){
        let blurEffect = UIBlurEffect(style: .light)
        let visualEffectView = UIVisualEffectView(effect: blurEffect)
        visualEffectView.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        visualEffectView.frame = self.frame
        let vibrancyEffect = UIVibrancyEffect(blurEffect:  blurEffect)
        let vibrancyEffectView = UIVisualEffectView(effect: vibrancyEffect)
        vibrancyEffectView.frame = self.bounds
        vibrancyEffectView.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        visualEffectView.contentView.addSubview(vibrancyEffectView)
        self.addSubview(visualEffectView)
    }
}

extension UITextField{
    func setleftView(image:String){
        self.leftViewMode = UITextField.ViewMode.always
        let leftImageView = UIImageView()
        leftImageView.image = UIImage(named:image)
        leftImageView.contentMode = .scaleAspectFit
        let leftView = UIView()
        leftView.addSubview(leftImageView)
        leftView.frame = CGRect(x:0,y:0,width:30,height:20)
        leftImageView.frame = CGRect(x:10,y:0, width:15,height:20)
        self.leftView = leftView
    }
    func loadBlur(){
        self.layer.cornerRadius = 8
        
        if !UIAccessibility.isReduceTransparencyEnabled {
            let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.light)
            let blurEffectView = UIVisualEffectView(effect: blurEffect)
            blurEffectView.frame = self.bounds
            blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            self.backgroundColor = UIColor.clear
            
            //Set the blurred image made from blurred view as textfield's background
            self.background = imageWithView(view: blurEffectView).applyTintEffect(with: UIColor.lightGray)
        }
    }
    
    func imageWithView(view:UIView)->UIImage{
        UIGraphicsBeginImageContextWithOptions(view.bounds.size, false, 0.0)
        view.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
    
    
}

extension UINavigationBar{
    func loadBlur(){
        self.layer.cornerRadius = 5
        
        if !UIAccessibility.isReduceTransparencyEnabled {
            let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.light)
            let blurEffectView = UIVisualEffectView(effect: blurEffect)
            blurEffectView.frame = self.bounds
            blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            self.backgroundColor = UIColor.clear
            //Set the blurred image made from blurred view as textfield's background
            self.setBackgroundImage(imageWithView(view: blurEffectView), for: UIBarMetrics.default)
        }
    }
    func imageWithView(view:UIView)->UIImage{
        UIGraphicsBeginImageContextWithOptions(view.bounds.size, false, 0.0)
        view.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
}
extension UITextField {
    func placeholderColor(_ color: UIColor){
        var placeholderText = ""
        if self.placeholder != nil{
            placeholderText = self.placeholder!
        }
        self.attributedPlaceholder = NSAttributedString(string: placeholderText, attributes: [NSAttributedString.Key.foregroundColor : color])
    }
}
//MARK:- Apple sign in functions
extension cedMageLogin : ASAuthorizationControllerDelegate, ASAuthorizationControllerPresentationContextProviding
{
    func createAppleSignInButton()
    {
        if #available(iOS 13.0, *) {
            let isDarkTheme = view.traitCollection.userInterfaceStyle == .dark
            let style: ASAuthorizationAppleIDButton.Style = isDarkTheme ? .white : .black
            let authorizationButton = ASAuthorizationAppleIDButton(type: .default, style: style)
            self.view.addSubview(authorizationButton)
            authorizationButton.translatesAutoresizingMaskIntoConstraints = false;
            authorizationButton.topAnchor.constraint(equalTo: googleLogin.bottomAnchor, constant: 10).isActive = true;
            authorizationButton.leadingAnchor.constraint(equalTo: googleLogin.leadingAnchor, constant: 0).isActive = true;
            authorizationButton.trailingAnchor.constraint(equalTo:  googleLogin.trailingAnchor, constant: 0).isActive = true;
            authorizationButton.heightAnchor.constraint(equalToConstant: 50).isActive = true;
            
            
            //authorizationButton.translatesAutoresizingMaskIntoConstraints = false;
            authorizationButton.center = self.view.center
            authorizationButton.addTarget(self, action: #selector(handleLogInWithAppleIDButtonPress), for: .touchUpInside)
        } else {
            // Fallback on earlier versions
        }
    }
    
    
    @objc func handleLogInWithAppleIDButtonPress() {
        if #available(iOS 13.0, *) {
            let appleIDProvider = ASAuthorizationAppleIDProvider()
            let request = appleIDProvider.createRequest()
            request.requestedScopes = [.fullName, .email]
            
            let authorizationController = ASAuthorizationController(authorizationRequests: [request])
            authorizationController.delegate = self
            authorizationController.presentationContextProvider = self
            authorizationController.performRequests()
        } else {
            // Fallback on earlier versions
        }
        
    }
    
    @available(iOS 13.0, *)
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return self.view.window!
    }
    
    private func performExistingAccountSetupFlows() {
        // Prepare requests for both Apple ID and password providers.
        if #available(iOS 13.0, *) {
            let requests = [ASAuthorizationAppleIDProvider().createRequest(), ASAuthorizationPasswordProvider().createRequest()]
            // Create an authorization controller with the given requests.
            let authorizationController = ASAuthorizationController(authorizationRequests: requests)
            authorizationController.delegate = self
            authorizationController.presentationContextProvider = self
            authorizationController.performRequests()
        } else {
            // Fallback on earlier versions
        }
        
        
    }
    
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        //Handle error here
        print("error = \(error.localizedDescription)")
    }
    
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        
        if let appleIDCredential = authorization.credential as? ASAuthorizationAppleIDCredential {
            let userName = appleIDCredential.fullName?.description
            let userEmail = appleIDCredential.email
            
            print(userName)
            print(userEmail)
            // Create an account in your system.
            print("User Id - \(appleIDCredential.user)")
            print("User Name - \(appleIDCredential.fullName?.description ?? "N/A")")
            print("User Email - \(appleIDCredential.email ?? "N/A")")
            print("Real User Status - \(appleIDCredential.realUserStatus.rawValue)")
            
            if let identityTokenData = appleIDCredential.identityToken,
                let identityTokenString = String(data: identityTokenData, encoding: .utf8) {
                print("Identity Token \(identityTokenString)")
            }
            
            if userName == "" || userEmail == nil || userEmail == ""
            {
                self.signInWithAppleRequest(params: ["token":appleIDCredential.user,"type":"apple"])
            }
            else
            {
                self.signInWithAppleRequest(params: ["token":appleIDCredential.user,"firstname":appleIDCredential.fullName?.givenName ?? "","lastname":appleIDCredential.fullName?.familyName ?? "","email":appleIDCredential.email ?? "","type":"apple"])
            }
            
            /* -- old
             self.signInWithAppleRequest(params: ["token":appleIDCredential.user,"firstname":appleIDCredential.fullName?.givenName ?? "","lastname":appleIDCredential.fullName?.familyName ?? "","email":appleIDCredential.email ?? "","type":"apple"])
             
             */
            
            //Show Home View Controller
            
        } else if let passwordCredential = authorization.credential as? ASPasswordCredential {
            // Sign in using an existing iCloud Keychain credential.
            let username = passwordCredential.user
            let password = passwordCredential.password
            
            // For the purpose of this demo app, show the password credential as an alert.
            DispatchQueue.main.async {
                let message = "The app has received your selected credential from the keychain. \n\n Username: \(username)\n Password: \(password)"
                let alertController = UIAlertController(title: "Keychain Credential Received",
                                                        message: message,
                                                        preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: nil))
                self.present(alertController, animated: true, completion: nil)
            }
        }
    }
    
    
    func signInWithAppleRequest(params: [String:String])
    {
        print("inside signInWithAppleRequest")
        print("my params = \(params)")
        if let emailId = params["email"]
        {
            email = emailId
        }
        print("my email : \(email)")
        self.sendRequest(url:"mobiconnect/sociallogin/create", params: params)
        /*
         rest/V1/mobiconnect/sociallogin/create
         {"parameters":{"email":"evamorgan008@gmail.com","firstname":"Eva","gender":"1","type":"google","token":"32523423423423"}}
         */
    }
    
    
    func setNotification(email: String = ""){
        if let notificationDetails = UserDefaults.standard.value(forKey: "notificationdetails") as? [String:String]{
            let tokenString = notificationDetails["token"]!
            let uniqueId = notificationDetails["unique"]!
            if UserDefaults.standard.bool(forKey: "isLogin"){
                if(tokenString != ""){
                    var url = Settings.baseUrl
                url += "mobinotifications/setdevice"
                var postData = [String:String]()
                postData["Token"] = tokenString
                postData["type"] = "1"
                postData["email"] = email
                postData["unique_id"] = uniqueId
                let postString = ["parameters":postData].convtToJson() as String
                var request = URLRequest(url: URL(string:url)!)
                request.httpMethod = "POST";
                    print(postString)
                    if UserDefaults.standard.bool(forKey: "isLogin"){
                        if let user = UserDefaults.standard.object(forKey: "userInfoDict") as? [String:String] {
                            request.setValue(user["hashKey"], forHTTPHeaderField: "hashkey")
                        }
                        
                    }
                request.httpBody = postString.data(using: String.Encoding.utf8)
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                let task = URLSession.shared.dataTask(with: request) {
                    data, response, error in
                    print("TestingAfter");
                    print(url);
                    print(postString);
                    
                    // check for http errors
                    if let httpStatus = response as? HTTPURLResponse , httpStatus.statusCode != 200
                    {
                        print("statusCode should be 200, but is \(httpStatus.statusCode)")
                        print("response = \(String(describing: response))")
                        return;
                    }
                    DispatchQueue.main.async{
                        if let data  = data
                        {
                            let dataTemp = NSString(data: data, encoding:String.Encoding.utf8.rawValue);
                            print(dataTemp as Any)
                            guard let json = try? JSON(data: data)else{return;}
                            print(json);
                        }
                    };
                }
                
                task.resume()
                }
            }
        }
        
    }
    
}
