/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */

import UIKit

class BrowseByViewController: MagenativeUIViewController
{
    @IBOutlet weak var clearFilterButton: UIButton!
    @IBOutlet weak var applyFilterButton: UIButton!
    @IBOutlet weak var mainContentScrollView: UIScrollView!
    var topHeight : CGFloat = 0;
    let fixedHeight : CGFloat = 30;
    var browseByJSON : JSON!;
    var browseByFiltersArray = [String:[String]]();
    
    var selectedFiltersIds = [Int]();
    var prevSelectedFiltersIds = [Int]();
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        
        self.clearFilterButton.setTitle("Clear".localized, for: UIControl.State.normal);
        self.clearFilterButton.fontColorTool()
        self.clearFilterButton.layer.cornerRadius = 2
        self.clearFilterButton.layer.borderWidth = 2
        self.clearFilterButton.addTarget(self, action: #selector(BrowseByViewController.performClearFilterFunction(_:)), for: .touchUpInside);
        
        
        self.applyFilterButton.setTitle("Apply".localized, for: UIControl.State.normal);
        self.applyFilterButton.setTitleColor(Settings.themeTextColor, for: .normal)
        self.applyFilterButton.setThemeColor()
        self.applyFilterButton.addTarget(self, action: #selector(BrowseByViewController.performApplyFilterFunction(_:)), for: UIControl.Event.touchUpInside);
        
        
        self.fectchParentAndChildFilters();
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func fectchParentAndChildFilters()
    {
        
        if(defaults.object(forKey: "previousBrowseByIds") != nil)
        {
            self.prevSelectedFiltersIds = defaults.object(forKey: "previousBrowseByIds") as! [Int];
            self.selectedFiltersIds = self.prevSelectedFiltersIds;
        }
       // print(browseByJSON)
        if(browseByJSON.count > 0)
        {
            for val in browseByJSON.arrayValue
            {
               
                var temp = [String]();
                
                    for inrVal in val["sub_categories"].arrayValue
                    {
                     
                        temp.append(inrVal["main_category"].stringValue);
                    }
                self.browseByFiltersArray[val["main_category"].stringValue] = temp;
            }
            self.showBrowseByFiltersToFrontEnd();
        }
    }
    
    @objc func performClearFilterFunction(_ sender:UIButton)
    {
        defaults.removeObject(forKey: "previousBrowseByIds");
        _=self.navigationController?.popViewController(animated: true);
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "loadProductsAgainId"), object: nil);
    }
    
    @objc func performApplyFilterFunction(_ sender:UIButton)
    {
        defaults.set(self.selectedFiltersIds, forKey: "previousBrowseByIds");
        _=self.navigationController?.popViewController(animated: true);
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "loadProductsAgainId"), object: nil);
    }
    
    
    @objc func makeFilterSelected(_ sender:UIButton)
    {
        if(sender.imageView!.image == UIImage(named:"unchecked_checkbox"))
        {
            sender.setImage(UIImage(named: "checked_checkbox"), for: UIControl.State.normal);
            self.selectedFiltersIds.append(sender.tag);
        }
        else
        {
            sender.setImage(UIImage(named: "unchecked_checkbox"), for: UIControl.State.normal);
            if let indexToRemove = selectedFiltersIds.firstIndex(of: sender.tag) {
                self.selectedFiltersIds.remove(at: indexToRemove);
            }
        }
        
        print(self.selectedFiltersIds);
        print("");
    }
    
    
    func showBrowseByFiltersToFrontEnd()
    {
        for (key,val) in browseByFiltersArray
        {
            let parentCategoryView = ParentCategoryView();
            parentCategoryView.backgroundColor = UIColor.black;
            parentCategoryView.frame = CGRect(x:0, y:CGFloat(topHeight), width:screenSize.width, height:fixedHeight);
            parentCategoryView.autoresizingMask = [UIView.AutoresizingMask.flexibleLeftMargin,UIView.AutoresizingMask.flexibleRightMargin];
            
            
            let impArray = key.components(separatedBy: "#")
            
            parentCategoryView.parentFilterButton.setTitle(impArray.last!, for: UIControl.State.normal);
            parentCategoryView.parentFilterButton.fontColorTool()
            if let _ = self.prevSelectedFiltersIds.firstIndex(of: Int(impArray.first!)!)
            {
                parentCategoryView.parentFilterButton.setImage(UIImage(named: "checked_checkbox"), for: UIControl.State.normal);
            }
            else
            {
                parentCategoryView.parentFilterButton.setImage(UIImage(named: "unchecked_checkbox"), for: UIControl.State.normal);
            }
            
            parentCategoryView.parentFilterButton.tag = Int(impArray.first!)!;
            parentCategoryView.parentFilterButton.addTarget(self, action: #selector(BrowseByViewController.makeFilterSelected(_:)), for: UIControl.Event.touchUpInside);
            
            self.mainContentScrollView.addSubview(parentCategoryView);
            topHeight += fixedHeight;
            
            for inrVal in val
            {
                let subCategoryView = SubCategoryView();
                subCategoryView.backgroundColor = UIColor.black;
                subCategoryView.frame = CGRect(x:20, y:CGFloat(topHeight), width:screenSize.width, height:fixedHeight);
                subCategoryView.autoresizingMask = [UIView.AutoresizingMask.flexibleRightMargin];
                
                
                let impArray = inrVal.components(separatedBy: "#")
                
                subCategoryView.childFilterButton.setTitle(impArray.last!, for: UIControl.State.normal);
                subCategoryView.childFilterButton.fontColorTool()
                if let _ = self.prevSelectedFiltersIds.firstIndex(of: Int(impArray.first!)!)
                {
                    subCategoryView.childFilterButton.setImage(UIImage(named: "checked_checkbox"), for: UIControl.State.normal);
                }
                else
                {
                    subCategoryView.childFilterButton.setImage(UIImage(named: "unchecked_checkbox"), for: UIControl.State.normal);
                }
                
                subCategoryView.childFilterButton.tag = Int(impArray.first!)!;
                subCategoryView.childFilterButton.addTarget(self, action: #selector(BrowseByViewController.makeFilterSelected(_:)), for: UIControl.Event.touchUpInside);
                
                self.mainContentScrollView.addSubview(subCategoryView);
                topHeight += fixedHeight;
            }
            
        }
        
        self.mainContentScrollView.contentSize = CGSize(width:self.view.frame.size.width, height:self.topHeight);
    }
    
    
}
