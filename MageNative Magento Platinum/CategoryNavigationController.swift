//
//  CategoryNavigationController.swift
//  MageNative Magento Platinum
//
//  Created by Hamza Usmani on 27/01/21.
//  Copyright © 2021 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class CategoryNavigationController: homedefaultNavigation {

    override func viewDidLoad() {
        super.viewDidLoad()
        setViewControllers([TreeViewController()], animated: true)
    }
    
}
