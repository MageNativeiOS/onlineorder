/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */


import UIKit
//import SkyFloatingLabelTextField
class cedMageRegister: MagenativeUIViewController,UITextFieldDelegate{
    
    var hide_check:Dictionary<String,String> = [:]
    @IBOutlet weak var parent_stack_height: NSLayoutConstraint!
    @IBOutlet weak var scroll_view: UIScrollView!
    @IBOutlet weak var stack_v: UIStackView!
    var additional_fields:JSON?=nil
    @IBOutlet weak var signUpForNewsLetter: UIButton!
    @IBOutlet weak var checkBoxImage: UIImageView!
    var drop_option =   [String]();
    let dobText     =   SkyFloatingLabelTextField()
    let taxvat      =   SkyFloatingLabelTextField()
    var datepicker:Dictionary<String,Any> = [:]
    var text:Dictionary<String,Any> = [:]
    var dropdown:Dictionary<String,Any> = [:]
    @IBOutlet weak var loginView: UIImageView!
    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var doRegister: UIButton!
    @IBOutlet weak var loginRedir: UIButton!
    
    @IBOutlet weak var alreadyAccountLabel: UILabel!
    
    
    var count=0;
    var count_items=0;
    var numberofelements=0;
    
    var clr = cedMage.UIColorFromRGB(colorCode: "#3d3d3d")
    @IBOutlet weak var stack_height: NSLayoutConstraint!
    let heig =  50;
    //var gender = String()
    var newLetter = false
    var signUp  = 0
    var isCheckOut = false
    var name=""
    var Firstname = ""
    var Lastname = ""
    var total = [String:String]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        parent_stack_height.constant += 200
        if #available(iOS 13.0, *) {
            clr = .label
        } else {
            clr = cedMage.UIColorFromRGB(colorCode: "#3d3d3d")
        }
        self.sendRequest(url: "mobiconnect/customer/getRequiredFields/", params: nil)
        print("*******\(hide_check)")
        self.navigationController?.navigationBar.isHidden=false
        doRegister.setThemeColor()
        self.sendScreenView(name: "Registration Page".localized)
        doRegister.addTarget(self, action: #selector(cedMageRegister.registerUser(sender:)), for: UIControl.Event.touchUpInside)
        loginRedir.addTarget(self, action: #selector(cedMageRegister.loginRedir(sender:)), for: UIControl.Event.touchUpInside)
        signUpForNewsLetter.addTarget(self,action: #selector(cedMageRegister.signUpNewsLetter),for: .touchUpInside)
        let visualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .light))
        visualEffectView.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        visualEffectView.frame = loginView.frame
        //loginView.addSubview(visualEffectView)
        // Do any additional setup after loading the view.
        
        topLabel.text = "Create Account".localized
        signUpForNewsLetter.setTitle("Signup Newsletter".localized, for: .normal)
        doRegister.setTitle("Submit".localized, for: .normal)
        loginRedir.setTitle("Login".localized, for: .normal)
        alreadyAccountLabel.text = "Already Have An Account?".localized
        
       
        
        
    }
    
    @objc func loginRedir(sender:UIButton){
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @objc func signUpNewsLetter(sender:UIButton){
        if(!newLetter){
            newLetter = true
            signUp  = 1
            checkBoxImage.image = UIImage(named:"CheckedCheckbox")
        }else{
            signUp = 0
            newLetter = false
            checkBoxImage.image = UIImage(named:"UncheckedCheckbox")
        }
    }
    
    @objc func registerUser(sender:UIButton){
        var firstName = ""
        if let firstNametextfield = text["firstname"] as? SkyFloatingLabelTextField{
            firstName=firstNametextfield.text!
            firstName = firstName.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        }
        var lastName = ""
        if let lastNametextfield = text["lastname"] as? SkyFloatingLabelTextField{
            lastName=lastNametextfield.text!
            lastName = lastName.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        }
        var email = ""
        if let emailtextfield = text["email"] as? SkyFloatingLabelTextField {
            email = emailtextfield.text!;
            email = email.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        }
        var password  = ""
        if let passwordtextfield=text["password"] as? SkyFloatingLabelTextField {
            password = passwordtextfield.text!;
            password = password.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        }
        
        var confirmpassword = ""
        if let confirmpasswordtextfield=text["confirmpassword"] as? SkyFloatingLabelTextField {
            confirmpassword = confirmpasswordtextfield.text!;
            confirmpassword = confirmpassword.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        }
        
        var middlename=""
        var taxvat=""
        for (index,value) in text
        {
            if index == "middlename"
            {
                let middlenametextfield=value as! SkyFloatingLabelTextField
                middlename=middlenametextfield.text!
                if(middlename=="")
                {
                    let title = "Error".localized
                    let msg = "All fields are required!".localized
                    cedMageHttpException.showAlertView(me: self, msg: msg, title: title)
                    return;
                    
                }
            }
            else if index == "taxvat"
            {
                let taxvattextfield=value as! SkyFloatingLabelTextField
                taxvat=taxvattextfield.text!
                
                
            }
        }
        var gender=""
        var prefix=""
        var suffix=""
        for (index,value) in dropdown
        {
            if index=="gender"
            {
                let gender_button=value as! UIButton
                gender=gender_button.currentTitle!
                print("****************############**************\(gender)")
                if(gender=="")
                {
                    let title = "Error".localized
                    let msg = "All fields are required!".localized
                    cedMageHttpException.showAlertView(me: self, msg: msg, title: title)
                    return;
                }
                else if(gender=="Male")
                {
                    gender="1"
                }
                else if(gender=="Female")
                {
                    gender="2"
                }
                else if(gender=="---Select Gender---")
                {
                    let title = "Error".localized
                    let msg = "All fields are required!".localized
                    cedMageHttpException.showAlertView(me: self, msg: msg, title: title)
                    return;
                }
                else
                {
                    gender="3"
                }
                
            }
            if index=="prefix"
            {
                let prefix_button=value as! UIButton
                prefix=prefix_button.currentTitle!
                if(prefix=="---Select Prefix---")
                {
                    let title = "Error".localized
                    let msg = "All fields are required!".localized
                    cedMageHttpException.showAlertView(me: self, msg: msg, title: title)
                    return;
                }
                
            }
            if index=="suffix"
            {
                let suffix_button=value as! UIButton
                suffix=suffix_button.currentTitle!
                if(suffix=="---Select Suffix---")
                {
                    let title = "Error".localized
                    let msg = "All fields are required!".localized
                    cedMageHttpException.showAlertView(me: self, msg: msg, title: title)
                    return;
                }
            }
        }
        var dob=""
        for (index,value) in datepicker
        {
            if index=="dob"
            {
                let dob_text=value as! SkyFloatingLabelTextField
                let inputFormatter = DateFormatter()
                inputFormatter.dateFormat = "dd-mm-yyyy"
                if let dobString = dob_text.text?.replacingOccurrences(of: "/", with: "-") {
                    if let showDate = inputFormatter.date(from: dobString) {
                        inputFormatter.dateFormat = "MM/DD/YYYY"
                        let resultString = inputFormatter.string(from: showDate)
                        print(resultString)
                        dob=resultString
                    }
                }
                if(dob=="")
                {
                    let title = "Error".localized
                    let msg = "All fields are required!".localized
                    cedMageHttpException.showAlertView(me: self, msg: msg, title: title)
                    return;
                    
                }
            }
        }
        
        if(firstName == "" || lastName == "" || email == "" || password == "" || confirmpassword == "")
        {
            let title = "Error".localized
            let msg = "All fields are required!".localized
            cedMageHttpException.showAlertView(me: self, msg: msg, title: title)
            return;
        }
        
        // code to check email format is proper or not :: start
        let validEmail = EmailVerifier.isValidEmail(testStr: email);
        if(!validEmail)
        {
            let title = "Error".localized
            let msg = "Invalid Email Address.".localized
            cedMageHttpException.showAlertView(me: self, msg: msg, title: title)
            return;
        }
        // code to check email format is proper or not :: end
        
        //code to check both passwords matched :: start
        if(password.count < 6)
        {
            let title = "Error".localized
            let msg = "Password must be minimun 6 characters long!".localized
            cedMageHttpException.showAlertView(me: self, msg: msg, title: title)
            return;
        }
        
        if(password != confirmpassword)
        {
            let title = "Error".localized
            let msg = "Both passwords not matched... Try again!".localized
            cedMageHttpException.showAlertView(me: self, msg: msg, title: title)
            return;
        }
        
        //code to check both passwords matched :: start
        
        let postString = ["firstname":firstName as String,"lastname":lastName as String,"email":email as String,"password":password as String,"is_subscribed":"\(signUp)","gender":gender,"dob":dob as String,"taxvat":taxvat as String,"prefix":prefix,"suffix":suffix,"middlename":middlename];
        name = firstName
        NotificationCenter.default.post(name: NSNotification.Name("loadDrawerAgain"), object: nil)
        self.sendRequest(url: "mobiconnect/customer/register", params: postString)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @objc func showOptionsDropdown(_ sender:UIButton){
        var ArrayToUse=[String]();
        if(sender.tag==333)
        {
            let options=dropdown_options["gender"]
            for (_,value) in options!
            {
                print(value)
                
                ArrayToUse.append(String(describing: value["label"]))
            }
        }
        if(sender.tag==111)
        {
            let options=dropdown_options["prefix"]
            print(options as Any)
            for (key,_) in options!
            {
                ArrayToUse.append(String(describing: options![key]))
            }
            
        }
        if(sender.tag==222)
        {
            let options=dropdown_options["suffix"]
            print(options as Any)
            for (key,_) in options!
            {
                ArrayToUse.append(String(describing: options![key]))
            }
            
        }
        
        ArrayToUse+=drop_option
        dropDown.dataSource = ArrayToUse;
        dropDown.selectionAction = {(index, item) in
            sender.fontColorTool()
            sender.setTitle(item, for: UIControl.State());
        }
        
        dropDown.anchorView = sender
        dropDown.bottomOffset = CGPoint(x: 0, y:sender.bounds.height)
        if dropDown.isHidden {
            let _ = dropDown.show();
        } else {
            dropDown.hide();
        }
    }
    
    func datePickerValueChanged(_ sender: UIDatePicker){
        // Create date formatter
        let dateFormatter: DateFormatter = DateFormatter()
        // Set date format
        dateFormatter.dateFormat = "MM/dd/yyyy"
        // Apply date format
        let selectedDate: String = dateFormatter.string(from: sender.date)
        let textfield=datepicker["dob"] as! SkyFloatingLabelTextField
        textfield.text=selectedDate;
        textfield.fontColorTool()
        
    }
    var button_tag=100;
    
    
    func make_field(type: String,values: Dictionary<String,String>)
    {
        let ty = type
        print(values)
        if(ty=="datepicker")
        {
            
            //scroll_view.contentSize.height+=CGFloat(160)
            stack_height.constant += CGFloat(60)
            parent_stack_height.constant+=CGFloat(30)
            let f1=SkyFloatingLabelTextField(frame: CGRect(x: 0, y: count+20, width: Int(stack_v.frame.width), height: 40))
            
            f1.font = UIFont.systemFont(ofSize: 12)
            f1.placeholder=values["label"]
            if values["label"] == "DOB" {
                f1.placeholder = values["label"]! + "-DD/MM/YYYY"
            }
            f1.titleLabel.text=values["label"]
            f1.titleColor=clr
            f1.fontColorTool()
            f1.textColor=clr
            f1.tintColor=clr
            f1.selectedLineColor=clr
            f1.selectedTitleColor=clr
            if #available(iOS 13.0, *) {
                f1.placeholderColor = .label
            } else {
                f1.placeholderColor = .black
            }
            datepicker.updateValue(f1, forKey: values["name"]!)
            
            stack_v.addSubview(f1)
            
            count+=60
            
        }
        if(ty=="text")
        {
            //scroll_view.contentSize.height+=CGFloat(60)
            stack_height.constant += CGFloat(60)
            //parent_stack_height.constant+=CGFloat(60)
            let f1=SkyFloatingLabelTextField(frame: CGRect(x: 0, y: count+20, width: Int(stack_v.frame.width), height: 40))
            
            f1.font = UIFont.systemFont(ofSize: 12)
            if values["name"]?.lowercased() == "password".lowercased() || values["name"]?.lowercased() == "confirmpassword".lowercased() {
                f1.isSecureTextEntry = true
            }
            
            if values["name"]?.lowercased() == "firstname".lowercased(){
                f1.text=Firstname
                f1.delegate=self
            }
            
            if values["name"]?.lowercased() == "lastname".lowercased() {
                f1.text=Lastname
                f1.delegate=self
            }            
          
            f1.font = UIFont.systemFont(ofSize: 12)
            f1.placeholder = values["label"]
            f1.titleLabel.text=values["label"]
            f1.titleColor=clr
            f1.textColor=clr
            //f1.placeholderColor = .black
            f1.fontColorTool()
            f1.tintColor=clr
            f1.selectedLineColor=clr
            f1.selectedTitleColor=clr
            //f1.loadBlur()
            text.updateValue(f1, forKey: values["name"]!)
            if #available(iOS 13.0, *) {
                f1.placeholderColor = .label
            } else {
                f1.placeholderColor = .black
            }
            stack_v.addSubview(f1)
            count+=60
        }
        if(ty=="dropdown")
        {
            //scroll_view.contentSize.height+=CGFloat(80)
            let gender=CustomOptionDropDownView();
            //gender.backgroundColor=UIColor.lightGray
            stack_height.constant += CGFloat(80)
//            gender.topLabel.textColor=clr
            
            //parent_stack_height.constant += CGFloat(80)
            gender.topLabel.text = values["label"]
            gender.dropDownButton.setTitle("---Select \(values["label"]!)---", for: UIControl.State());
            gender.dropDownButton.setTitleColor(clr, for: .normal)
            //gender.backgroundColor=UIColor.darkGray
            gender.frame = CGRect(x: 0, y: count+10, width: Int(stack_v.frame.width), height: 70)
            if gender.topLabel.text=="Prefix"
            {
                gender.dropDownButton.tag=111
            }
            else if gender.topLabel.text=="Suffix"
            {
                gender.dropDownButton.tag=222
            }
            else if gender.topLabel.text=="Gender"
            {
                gender.dropDownButton.tag=333
            }
            gender.dropDownButton.addTarget(self, action: #selector(showOptionsDropdown(_:)), for: UIControl.Event.touchUpInside);
            dropdown.updateValue(gender.dropDownButton, forKey: values["name"]!)
            stack_v.addSubview(gender);
            count+=80
            button_tag+=1
        }
    }
    
    
    func show_fields()
    {
        if(hide_check["prefix"]=="true")
        {
            let prefix=["label":"Prefix".localized,"name":"prefix"]
            make_field(type: "dropdown", values: prefix)
        }
        
        
        let firstname=["label":"First Name".localized,"name":"firstname"]
        make_field(type: "text", values: firstname)
        
        
        if(hide_check["middlename"]=="true")
        {
            let middlename=["label":"Middle Name".localized,"name":"middlename"]
            make_field(type: "text", values: middlename)
        }
        
        
        let lastname=["label":"Last Name".localized,"name":"lastname"]
        make_field(type: "text", values: lastname)
        
        
        if(hide_check["suffix"]=="true")
        {
            let suffix=["label":"Suffix".localized,"name":"suffix"]
            make_field(type: "dropdown", values: suffix)
        }
        
        
        let email=["label":"Email".localized,"name":"email"]
        make_field(type: "text", values: email)
        
        
        let password=["label":"Password".localized,"name":"password"]
        make_field(type: "text", values: password)
        
        
        
        let confirmpassword=["label":"Confirm Password".localized,"name":"confirmpassword"]
        make_field(type: "text", values: confirmpassword)
        
        
        if(hide_check["dob"]=="true")
        {
            let dob=["label":"DOB".localized,"name":"dob"]
            make_field(type: "datepicker", values: dob)
        }
        
        if(hide_check["gender"]=="true")
        {
            let gender=["label":"Gender".localized,"name":"gender"]
            make_field(type: "dropdown", values: gender)
        }
        
        if(hide_check["taxvat"]=="true")
        {
            let taxvat=["label":"Tax/Vat".localized,"name":"taxvat"]
            make_field(type: "text", values: taxvat)
        }
    }
    
    var dropdown_options:Dictionary<String,JSON>=[:]
    
    override func recieveResponse(data: Data?, requestUrl: String?, response: URLResponse?) {
        if requestUrl == "mobiconnect/customer/getRequiredFields/"
        {
            guard let json = try? JSON(data: data!) else{return;}
            additional_fields=json;
            print("!@#$%\(additional_fields)")
            if(additional_fields?[0]["success"] == true)
            {
                for (_,field) in (additional_fields?[0]["data"])!
                {
                    for (key,values) in field
                    {
                        if(key=="name")
                        {
                            let v=String(describing: values);
                            if(field[v]==false)
                            {
                                hide_check.updateValue("false", forKey: v)
                            }
                            else
                            {
                                hide_check.updateValue("true", forKey: v)
                                if(field["type"]=="dropdown")
                                {
                                    if(values=="suffix")
                                    {
                                        dropdown_options.updateValue(field["suffix_options"], forKey: "suffix")
                                    }
                                    else if(values=="prefix")
                                    {
                                        dropdown_options.updateValue(field["prefix_options"], forKey: "prefix")
                                    }
                                    else if(values=="gender")
                                    {
                                        dropdown_options.updateValue(field["gender_options"], forKey: "gender")
                                    }
                                }
                            }
                        }
                    }
                }
            }
            show_fields()
        }
        else
        {
            guard let json = try? JSON(data: data!) else{return;}
            print(json)
            if requestUrl != "mobiconnect/customer/login" {
                let status = json[0]["data"]["customer"][0]["status"].stringValue;
                if(status == "success")
                {
                    let isConfirm = json[0]["data"]["customer"][0]["isConfirmationRequired"].stringValue
                    print(isConfirm)
                    if(isConfirm == "NO"){
//                        defaults.setValue(name, forKey: "name")
//                        defaults.set(true,forKey: "isLogin")
//                        let vc=cedMageMainDrawer()
//                        let table = vc.mainTable;
//                        table?.reloadData();
                        let email=text["email"] as! SkyFloatingLabelTextField
                        let password=text["password"] as! SkyFloatingLabelTextField
                        self.setNotification(email: email.text!)
                        self.loginFunction(email: email.text!, password: password.text!)
                        
                    }else{
                        cedMageHttpException.showAlertView(me: self, msg: "Registration Successful".localized, title: "Success".localized)
                        cedMage.delay(delay: 2, closure: {
                            _ = self.navigationController?.popToRootViewController(animated: true)
                        })
                    }
                    return;
                }else{
                    let title = "Error!".localized
                    let errorMsg = json[0]["data"]["customer"][0]["message"].string;
                    cedMageHttpException.showAlertView(me: self, msg: errorMsg, title: title);
                    return
                }
            }else{
                if json[0]["data"]["customer"][0]["status"].stringValue.localized ==  "exception".localized {
                    let message = json[0]["data"]["customer"][0]["message"].stringValue
                    cedMageHttpException.showAlertView(me: self, msg: message, title: "Error".localized)
                    return
                }else {
                    defaults.setValue(name, forKey: "name")
                    defaults.set(true,forKey: "isLogin")
                    let vc=cedMageMainDrawer()
                    let table = vc.mainTable;
                    table?.reloadData();
                    let customer_id = json[0]["data"]["customer"][0]["customer_id"].stringValue;
                    let hashKey = json[0]["data"]["customer"][0]["hash"].stringValue;
                    let cart_summary = json[0]["data"]["customer"][0]["cart_summary"].intValue;
                    //saving value in NSUserDefault to use later on :: start
                    let email=text["email"] as! SkyFloatingLabelTextField
                    let dict = ["email": email.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines), "customerId": customer_id, "hashKey": hashKey];
                    self.defaults.set(true, forKey: "isLogin")
                    self.defaults.set(dict, forKey: "userInfoDict");
                    self.defaults.set(cart_summary, forKey: "cart_summary");
                    self.defaults.setValue( json[0]["data"]["customer"][0]["gender"].stringValue, forKey: "gender")
                    self.view.makeToast("Login Successfull", duration: 1.5, position: .center, title: nil, image: nil, style: nil) { (true) in
                        if !self.isCheckOut {
                            /*let viewControl = UIStoryboard(name: "homeLayouts", bundle: nil).instantiateViewController(withIdentifier: "baseHomepageController") as? baseHomepageController*/
                            let viewControl = HomeController()
                            self.navigationController?.navigationBar.isHidden = false
                            self.navigationController?.setViewControllers([viewControl], animated: true)
                        }else{
                            
                            if(cedMage.checkModule(string:"MageNative_Mobicheckout")){
                                print("inside check")
                                let storyboard = UIStoryboard(name: "cedMageAccounts", bundle: nil);
                                let viewController = storyboard.instantiateViewController(withIdentifier: "cedMageAdvancedCheckout") as! cedMageAdvancedCheckout;
                                self.navigationController?.pushViewController(viewController, animated: true);
                            }
                            else{
                                let storyboard = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil);
                                let viewController = storyboard.instantiateViewController(withIdentifier: "checkoutStep2ViewController") as! CheckoutStep2ViewController;
                                viewController.email_id = email.text!
                                viewController.totalOrderData = self.total
                                viewController.checkoutAs = "USER";
                                self.navigationController?.pushViewController(viewController, animated: true);
                            }
                        }
                    }
                    
                }
            }
        }
    }
    
    
    func loginFunction(email:String,password:String){
        var email = email;
        email = email.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        
        var password = password;
        password = password.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        
        var postString = ["email":email,"password":password];
        if(defaults.object(forKey: "cartId") != nil)
        {
            let cart_id = defaults.object(forKey: "cartId") as? String;
            //postString += "&cart_id="+cart_id!;
            postString.updateValue(cart_id!, forKey: "cart_id")
        }
        self.sendRequest(url: "mobiconnect/customer/login", params: postString)
    }
    
    func setNotification(email: String = ""){
        if let notificationDetails = UserDefaults.standard.value(forKey: "notificationdetails") as? [String:String]{
            let tokenString = notificationDetails["token"]!
            let uniqueId = notificationDetails["unique"]!
            if UserDefaults.standard.bool(forKey: "isLogin"){
                if(tokenString != ""){
                    var url = Settings.baseUrl
                url += "mobinotifications/setdevice"
                var postData = [String:String]()
                postData["Token"] = tokenString
                postData["type"] = "1"
                postData["email"] = email
                postData["unique_id"] = uniqueId
                let postString = ["parameters":postData].convtToJson() as String
                var request = URLRequest(url: URL(string:url)!)
                request.httpMethod = "POST";
                    //let requestHeader = Settings.headerKey
                //request.setValue(requestHeader, forHTTPHeaderField: "Mobiconnectheader")
                print(postString)
                if UserDefaults.standard.bool(forKey: "isLogin"){
                    if let user = UserDefaults.standard.object(forKey: "userInfoDict") as? [String:String] {
                        request.setValue(user["hashKey"], forHTTPHeaderField: "hashkey")
                    }
                    
                }
                request.httpBody = postString.data(using: String.Encoding.utf8)
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                let task = URLSession.shared.dataTask(with: request) {
                    data, response, error in
                    print("TestingAfter");
                    print(url);
                    print(postString);
                    
                    // check for http errors
                    if let httpStatus = response as? HTTPURLResponse , httpStatus.statusCode != 200
                    {
                        print("statusCode should be 200, but is \(httpStatus.statusCode)")
                        print("response = \(String(describing: response))")
                        return;
                    }
                    DispatchQueue.main.async{
                        if let data  = data
                        {
                            let dataTemp = NSString(data: data, encoding:String.Encoding.utf8.rawValue);
                            print(dataTemp as Any)
                            guard let json = try? JSON(data: data)else{return;}
                            print(json);
                        }
                    };
                }
                
                task.resume()
                }
            }
        }
        
    }
    
}

