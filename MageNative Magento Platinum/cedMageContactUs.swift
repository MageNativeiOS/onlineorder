/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */


import UIKit
import MediaPlayer
import AVKit
class cedMageContactUs: cedMageViewController {

    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var addressLabel: UILabel!
      var storeAddress = [String:String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.sendRequest(url: "mobiconnectadvcart/index/storeAddress", params: nil)
        addressLabel.cardView()
        addressLabel.fontColorTool()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func recieveResponse(data: Data?, requestUrl: String?, response: URLResponse?) {
        guard let json = try? JSON(data:data!)else{return;}
        if(json["data"]["status"] == "success"){
            let address = json["data"]["address"]["store_address"].stringValue
            let contact = json["data"]["address"]["store_contact"].stringValue
            let workinghours = json["data"]["address"]["hours_operating"].stringValue
            let storename  = json["data"]["address"]["store_name"].stringValue
            let storeAddres = ["address":address,"contact":contact,"working":workinghours,"name":storename]
            self.storeAddress = storeAddres
            setAddressInMap(map:mapView,address: self.storeAddress)
        }else{
            self.view.makeToast(json["data"]["message"].stringValue, duration: 2, position: .center, title: "Error", image: nil, style: nil, completion: nil)
        }

    }
    
    
    func setAddressInMap(map:MKMapView,address: [String:String]){
        let addres = address["address"]!
        var baseURl = "https://maps.googleapis.com/maps/api/geocode/json?address=\(addres))"
     baseURl =    baseURl.addingPercentEncoding (withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        let request = URLRequest(url: NSURL(string: baseURl)! as URL)
        
        let task = URLSession.shared.dataTask(with: request as URLRequest)
        {
            data, response, error in
            guard error == nil && data != nil else
            {
                print("error=\(error)")
                return;
            }
            
            // check for http errors
            if let httpStatus = response as? HTTPURLResponse , httpStatus.statusCode != 200
            {
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(response)")
                return;
            }
            guard let json = try? JSON(data: data!) else{return;}
            DispatchQueue.main.async
                {
                    if(json["results"].count != 0){
                let addresslat = json["results"][0]["geometry"]["location"]["lat"].stringValue
                let addresslog = json["results"][0]["geometry"]["location"]["lng"].stringValue
                
                let coordinate = CLLocationCoordinate2D(latitude: Double(addresslat)!, longitude: Double(addresslog)!)
                let point = MKPointAnnotation()
                point.coordinate = coordinate
                point.title = address["name"]
                        map.addAnnotation(point)
                        map.showAnnotations([point], animated: true)
                    }
                self.addressLabel.text = address["name"]!+"\n"+address["contact"]!+"\n"+address["address"]!
            }
        }
        task.resume()
    }

    override func intializeExternalScreen(external: UIScreen) {
        self.externalWindow = UIWindow(frame: external.bounds)
        self.externalWindow?.screen = external
        self.externalWindow?.isHidden = false
        
        let view  = UIView(frame: (self.externalWindow?.frame)!)
        let url:URL = URL(string: "http://79d01bccf573d1a4f5d4-4cf32c4c32c6b144c5cd3c9107757a73.r95.cf3.rackcdn.com/sb1571b.mp4")!
        
        let player = AVPlayer(url:url)
        let playerController = AVPlayerViewController()
        player.isClosedCaptionDisplayEnabled = false
        playerController.player = player
        
        player.allowsExternalPlayback = true
         player.usesExternalPlaybackWhileExternalScreenIsActive = true
      
        playerController.view.frame = (self.view.frame)
        //self.addChildViewController(playerController)
        //self.view.addSubview(playerController.view)
      
        
        player.play()
        let mapView = MKMapView(frame: (self.externalWindow?.frame)!)
        mapView.showsUserLocation = true
        //view.addSubview(mapView)
        self.externalWindow?.addSubview(view)
        self.externalWindow?.isHidden = false
        if(storeAddress.count != 0){
        self.setAddressInMap(map: mapView, address: self.storeAddress)
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
