//
//  defaultGridCell.swift
//  MageNative Magento Platinum
//
//  Created by CEDCOSS Technologies Private Limited on 18/01/17.
//  Copyright © 2017 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class defaultGridCell: UICollectionViewCell {
    
    @IBOutlet weak var subCategoryView: UIView!
    @IBOutlet weak var subCategoryLabel: UILabel!
    @IBOutlet weak var offerView: UIView!
    @IBOutlet weak var starLabel: UIImageView!
    @IBOutlet weak var productTitle: UILabel!
    @IBOutlet weak var rating: UILabel!
    @IBOutlet weak var ratingView: UIView!
    @IBOutlet weak var wishlistButton: UIButton!
    @IBOutlet weak var wrapperView: UIView!
    
    @IBOutlet weak var offerText: UILabel!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var errorImg: UIImageView!
    @IBOutlet weak var stockView: UIView!
    @IBOutlet weak var outOfStock: UILabel!
}
