/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */

import UIKit

class cedMageTestHomeTheme: cedMageViewController,UICollectionViewDelegate,UICollectionViewDataSource,KIImagePagerDelegate,KIImagePagerDataSource,UICollectionViewDelegateFlowLayout {
    @IBOutlet weak var wholeContainer: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var featuredProducts = [[String:String]]()
    var newArrivalProducts = [[String:String]]()
    var mostViewedProducts = [[String:String]]()
    var bestSellingProducts = [[String:String]]()
    var bannerImages       = [[String:String]]()
    var banner_image_array = [String]()
    var productData     = [String:[[String:String]]]()
    var dealGroup = [[String:String]]()
    var deals     = [[String:String]]()
    var dealsPro     = [String:[[String:String]]]()
    var bounds = UIScreen.main.bounds
    var selectedDataSouce =  [[String:String]]()
    var selectedIndex = Int()
    var selectedIndexx = Int()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        collectionView.delegate = self
        collectionView.dataSource = self
        //  saveAccess()
        self.sendRequest(url: "mobiconnect/index/gethomepagebanner", params: nil)
        self.sendRequest(url: "mobiconnect/index/gethomepageNewArrival", params: nil)
        self.sendRequest(url: "mobiconnect/index/gethomepagebestselling", params: nil)
        self.sendRequest(url: "mobiconnect/index/gethomepagemostview", params: nil)
        self.sendRequest(url: "mobiconnect/index/gethomepagefeaturedproduct", params: nil)
        if(cedMage.checkModule(string: "Ced_Mobiconnectdeals")){
            self.sendRequest(url: "mobiconnectdeals/mobideals/getdealgroup", params: nil)
        }
      self.sendScreenView(name: "Home")
    
     
    
        // segmentedView.setThemeColor()
        //cedMageLoaders.addImageFliploader(me: self)
    }
    
    func saveAccess(){
        var url = Settings.baseUrl
        url += "mobinotification/notification/save"
        let postString = "Token=576d892d2ae3f8140a7a6f5742dcacfabf6448d73081eb1b0c27882a70f14dec" + "&type=1"
        var request = URLRequest(url: URL(string:url)!)
        request.httpMethod = "POST";
        let requestHeader = Settings.headerKey
        if UserDefaults.standard.bool(forKey: "isLogin"){
            if let user = UserDefaults.standard.object(forKey: "userInfoDict") as? [String:String] {
                request.setValue(user["hashKey"], forHTTPHeaderField: "hashkey")
            }
            
        }
        request.httpBody = postString.data(using: String.Encoding.utf8)
        
        print("Testing");
        
        print(url);
        print(postString);
        
        let task = URLSession.shared.dataTask(with: request) {
            data, response, error in
            print("TestingAfter");
            print(url);
            print(postString);
            
            // check for http errors
            if let httpStatus = response as? HTTPURLResponse , httpStatus.statusCode != 200
            {
                
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(response)")
                return;
            }
            
            DispatchQueue.main.async{
                
                print(response as Any);
                
                let dataTemp = NSString(data: data!, encoding:String.Encoding.utf8.rawValue);
                print(dataTemp as Any);
                
                guard let json = try? JSON(data: data!) else{return;}
                print(json);
            };
        }
        
        task.resume()
    }
    
    
    
    @objc func clickedProductChange(sender:UISegmentedControl){
        let title = sender.titleForSegment(at: sender.selectedSegmentIndex)
        if(title?.lowercased() == "BEST SELLER".lowercased() ){
            selectedIndexx = 0
            selectedDataSouce = bestSellingProducts
            collectionView.reloadSections(NSIndexSet(index: 4) as IndexSet)
        } else if(title?.lowercased() == "FEATURED".lowercased() ){
                selectedIndexx = 1
            selectedDataSouce = featuredProducts
            collectionView.reloadSections(NSIndexSet(index: 4) as IndexSet)
        }else if(title?.lowercased() == "MOST VIEWED".lowercased() ){
                selectedIndexx = 2
            selectedDataSouce = mostViewedProducts
            collectionView.reloadSections(NSIndexSet(index: 4) as IndexSet)
        }else if(title?.lowercased() == "NEW ARRIVALS".lowercased() ){
                selectedIndexx = 3
            selectedDataSouce = newArrivalProducts
            collectionView.reloadSections(NSIndexSet(index: 4) as IndexSet)
        }
        
    }
    
    @objc func clickedDealSegment(sender:UISegmentedControl){
        print("hsdjghdsjdsdss")
        switch sender.selectedSegmentIndex
        {
        case 0:
            selectedIndex = 0
            collectionView.reloadData()
            
        case 1:
            selectedIndex = 1
            collectionView.reloadData()
            
            
        default:
            break;
        }
    }
    
    override func recieveResponse(data: Data?, requestUrl:String? , response: URLResponse?) {
        guard let json = try? JSON(data: data!) else{return;}
        print(json)
        if(requestUrl == "mobiconnect/index/gethomepageNewArrival"){
            self.parseJsonData(json: json, index: "new_arrival")
        }else if(requestUrl == "mobiconnect/index/gethomepagebestselling"){
            self.parseJsonData(json: json, index: "best_selling")
        }
        else if(requestUrl == "mobiconnect/index/gethomepagemostview"){
            self.parseJsonData(json: json, index: "most_view")
        }else if(requestUrl == "mobiconnect/index/gethomepagefeaturedproduct"){
            self.parseJsonData(json: json, index: "featured_products")
        }else if(requestUrl == "mobiconnect/index/gethomepagebanner"){
            self.parseJsonData(json: json, index: "banner")
            self.banner_image_array = [String]()
            for image in self.bannerImages{            self.banner_image_array.append(image["banner_image"]!)
            }
            print(banner_image_array)
            
        }else if(requestUrl == "mobiconnectdeals/mobideals/getdealgroup"){
            self.parseJsonData(json: json, index: "deal_products")
        }
        if(bestSellingProducts.count != 0){
            selectedDataSouce = bestSellingProducts
            collectionView.reloadData()
        } else if(featuredProducts.count != 0){
            selectedDataSouce = featuredProducts
            collectionView.reloadData()
        }else if(mostViewedProducts.count != 0){
            selectedDataSouce = mostViewedProducts
            collectionView.reloadData()
        }else if(newArrivalProducts.count != 0){
            selectedDataSouce = newArrivalProducts
            collectionView.reloadData()
        }
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(section == 0){
            if banner_image_array.count == 0 {
                return 0
            }
            return 1
        }else if(section == 1){
            if(dealGroup.count != 0){
                return 1
            }
            return 0
        }else if(section == 2){
            if(selectedIndex == 1){
                return 1
            }
            return 0
        }else if(section == 3){
            if(selectedIndex == 0){
                return dealGroup.count
            }
            return 0
        }
        else {
            if(selectedIndex != 0){
                if(selectedDataSouce.count == 0){
                    return 1
                }
                return selectedDataSouce.count
            }else{
                return 0
            }
        }
        
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        print(indexPath.section)
        if(indexPath.section == 0){
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "bannerCell", for: indexPath) as! cedMageBannercell
            cell.imagePager.delegate = self
            cell.imagePager.dataSource = self
            cell.imagePager.slideshowTimeInterval = UInt(2.5)
            cell.imagePager.indicatorDisabled = true
            cell.imagePager.imageCounterDisabled = true
            cell.cardView()
            return cell
        }else if(indexPath.section == 3){
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "dealCell", for: indexPath) as! cedMageTestDealCell
           // if(cell.data){
               // cell.data = false
                let string : String = dealGroup[indexPath.row]["dealGroupDuration"]!
                let timeinterval : TimeInterval = (string as NSString).doubleValue
                cell.parent = self
                if(dealGroup[indexPath.row]["timer_status"] != "1"){
                    cell.timerStatus = "2"
                    
                }else{
                    cell.startTime = (timeinterval)
                }
                //cell.deals = deals
                cell.deals = dealsPro[String(indexPath.row)]!;
                cell.parent = self
                cell.prevtext = dealGroup[indexPath.row]["groupTitle"]!
                
           // }
            
            
            return cell
        }
        else if(indexPath.section == 1){
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "segmentedview", for: indexPath) as! cedMageSegmentedView
            let color  = cedMage.getInfoPlist(fileName: "cedMage", indexString: "themeColor") as! String
            cell.segmentedView.tintColor = cedMage.UIColorFromRGB(colorCode: color)
            cell.segmentedView.removeAllSegments()
            if(dealGroup.count != 0){
                cell.segmentedView.insertSegment(withTitle: "Hot Deals", at: 0, animated: true)
            }
            if(bestSellingProducts.count != 0){
                cell.segmentedView.insertSegment(withTitle: "Products", at: 1, animated: true)
            }
            cell.segmentedView.selectedSegmentIndex = selectedIndex
            cell.segmentedView.addTarget(self, action: #selector(cedMageTestHomeTheme.clickedDealSegment(sender:)), for: .valueChanged)
            
            
            cell.cardView()
            return cell
        } else if(indexPath.section == 2){
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "segmentedview", for: indexPath) as! cedMageSegmentedView
            let color  = cedMage.getInfoPlist(fileName: "cedMage", indexString: "themeColor") as! String
            cell.segmentedView.tintColor = cedMage.UIColorFromRGB(colorCode: color)
            
            cell.segmentedView.removeAllSegments()
            if(bestSellingProducts.count != 0){
                cell.segmentedView.insertSegment(withTitle: "BEST SELLER", at: 0, animated: true)
            }
            if(featuredProducts.count != 0){
                cell.segmentedView.insertSegment(withTitle: "FEATURED", at: 1, animated: true)
            }
            if(mostViewedProducts.count != 0){
                cell.segmentedView.insertSegment(withTitle: "MOST VIEWED", at: 2, animated: true)
            }
            if(newArrivalProducts.count != 0){
                cell.segmentedView.insertSegment(withTitle: "NEW ARRIVALS", at: 3, animated: true)
            }
            cell.segmentedView.addTarget(self, action: #selector(cedMageTestHomeTheme.clickedProductChange(sender:)), for: .valueChanged)
            cell.segmentedView.selectedSegmentIndex = selectedIndexx
            cell.cardView()
            return cell
        }else {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionCell", for: indexPath) as! cedMageCollectionCell
            cell.label.fontColorTool()
            if(selectedDataSouce.count == 0 ){
                cell.label.isHidden = true
                cell.labelBackground.isHidden = true
                
                cell.imageViewProduct.image = UIImage(named: "noProducts")
            }else{
                cell.label.isHidden = false
                cell.labelBackground.isHidden = false
                cell.label?.text = selectedDataSouce[indexPath.row]["product_name"]
                cedMageImageLoader.shared.loadImgFromUrl(urlString: selectedDataSouce[indexPath.row]["product_image"]!,completionHandler: { (image: UIImage?, url: String) in
                    DispatchQueue.main.async {
                        cell.imageViewProduct.image = image
                    }
                })
            }
            cell.cardView()
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if(indexPath.section == 0){
            return CGSize(width:collectionView.frame.width, height: bounds.height/2.5)
        }else if(indexPath.section == 3){
           
                let dealgoup = dealsPro[String(indexPath.row)]!
                let dealsCount:CGFloat = CGFloat(dealgoup.count)
           
            return CGSize(width:collectionView.frame.width , height: bounds.height/3.5 + bounds.height/2.5*dealsCount/2)
        }
        else if(indexPath.section == 1){
            return CGSize(width:collectionView.frame.width , height: 45)
        }
        else if(indexPath.section == 2){
            return CGSize(width:collectionView.frame.width , height: 45)
        }
        if selectedDataSouce.count == 0 {
            return CGSize(width:bounds.width , height: bounds.height/2)
        }
        return CGSize(width:bounds.width/2 - 5 , height: bounds.height/2.5)
    }
    
    //MARK: Collection View Selection
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if(indexPath.section == 4 && selectedDataSouce.count != 0){
            
            let productview = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "rootPageViewController") as! cedMageProductViewRoot
            productview.pageData = selectedDataSouce as NSArray
            let instance = cedMage.singletonInstance
            instance.storeParameterInteger(parameter: indexPath.row)
            self.navigationController?.pushViewController(productview
                , animated: true)
        }
    }
    
    
    //MARK: Parse Product Data from json response
    private func parseJsonData(json:JSON,index:String){
        
        if(index == "banner"){
            for result in json["data"][index].arrayValue {
                let product_id = result["product_id"].stringValue
                let id = result["id"].stringValue
                let title = result["title"].stringValue
                let link_to = result["link_to"].stringValue
                let banner_image = result["banner_image"].stringValue
                let products_obj = ["product_id":product_id, "title":title,"link_to":link_to,"banner_image":banner_image,"id":id]
                bannerImages.append(products_obj)
                
            }
            return
                print("^\(bannerImages)")
        }
        
        if(index == "deal_products"){
            var j = 0
            for result in json["data"]["deal_products"].arrayValue {
                let dealgropstart = result["group_start_date"].stringValue
                let groupcreated = result["created_time"].stringValue
                let group_end_date = result["group_end_date"].stringValue
                let dealGroupDuration = result["deal_duration"].stringValue
                let groupImagename = result["group_image_name"].stringValue
                let group_status = result ["group_status"].stringValue
                let groupTitle  = result["title"].stringValue
                let group_id  = result["group_id"].stringValue
                let timer_status = result["timer_status"].stringValue
                let view_all_status = result["view_all_status"].stringValue
                let update_time = result["update_time"].stringValue
                let is_static  = result["is_static"].stringValue
                let deal_link  = result["deal_link"].stringValue
                let dealObject = ["dealgropstart":dealgropstart,"groupcreated":groupcreated,"group_end_date":group_end_date,"dealGroupDuration":dealGroupDuration,"groupImagename":groupImagename,"group_status":group_status,"groupTitle":groupTitle,"group_id":group_id,"timer_status":timer_status,"view_all_status":view_all_status,"update_time":update_time,"is_static":is_static,"deal_link":deal_link]
                dealGroup.append(dealObject)
                
                var i = 0
                for deal in result["content"].arrayValue{
                    let id = deal["id"].stringValue
                    let start_date = deal["start_date"].stringValue
                    let deal_image_name = deal["deal_image_name"].stringValue
                    let relative_link = deal["relative_link"].stringValue
                    let deal_type = deal["deal_type"].stringValue
                    let deal_title = deal["deal_title"].stringValue
                    let category_link = deal["category_link"].stringValue
                    let offer_text = deal["offer_text"].stringValue
                    let status  = deal["status"].stringValue
                    let product_link = deal["product_link"].stringValue
                    
                    let deaks_obj = ["id":id, "start_date":start_date,"deal_image_name":deal_image_name,"relative_link":relative_link,"deal_type":deal_type,"deal_title":deal_title,"category_link":category_link,"offer_text":offer_text,"status":status,"product_link":product_link]
                    deals.append(deaks_obj)
                    i += 1
                }
                dealsPro["\(j)"] = deals;
                deals.removeAll()
                j += 1
            }
        }
        else {
            let status = json["data"]["status"].stringValue
            for result in json["data"][index].arrayValue {
                let product_id = result["product_id"].stringValue
                let product_name = result["product_name"].stringValue
                let product_price = result["product_price"].stringValue
                let product_image = result["product_image"].stringValue
                let products_obj = ["product_id":product_id, "product_price":product_price,"product_image":product_image,"product_name":product_name,"status":status]
                if(index == "best_selling"){
                    bestSellingProducts.append(products_obj)
                }
                else if(index == "most_view"){
                    mostViewedProducts.append(products_obj)
                }
                else if(index == "new_arrival"){
                    newArrivalProducts.append(products_obj)
                }else if(index == "featured_products"){
                    
                    featuredProducts.append(products_obj)
                    print("^ \(featuredProducts)")
                }
                
            }
        }
        
        
    }
    
    //MARK: Delegate for KIImage Pager
    
    public func array(withImages pager: KIImagePager!) -> [Any]! {
        if banner_image_array.count == 0 {
            return ["placeholder"]
        }
        return banner_image_array
    }
   /* func array(withImages pager: KIImagePager!) -> [AnyObject]! {
        if banner_image_array.count == 0 {
            return ["placeholder"]
        }
        return banner_image_array
    }*/
    
    func contentMode(forImage image: UInt, in pager: KIImagePager!) -> UIView.ContentMode {
        return UIView.ContentMode.scaleToFill
    }
    
    func placeHolderImage(for pager: KIImagePager!) -> UIImage! {
        return UIImage(named: "placeholder")
    }
    
    func imagePager(_ imagePager: KIImagePager!, didSelectImageAt index: UInt) {
        let banner = bannerImages[Int(index)]
        if(banner["link_to"] == "category"){
            let viewcontoller = UIStoryboard(name: "categorylayouts", bundle: nil).instantiateViewController(withIdentifier: "categoryTheme2") as? cedMageCategoryTheme2
            viewcontoller?.categoryId = banner["product_id"]!
            self.navigationController?.pushViewController(viewcontoller!, animated: true)
        }else if (banner["link_to"] == "product"){
            let productview = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "rootPageViewController") as! cedMageProductViewRoot
            productview.pageData = [["product_id":banner["product_id"]!]]
            let instance = cedMage.singletonInstance
            instance.storeParameterInteger(parameter: 0)
            self.navigationController?.pushViewController(productview
                , animated: true)

        }else if (banner["link_to"] == "website"){
            let view = UIStoryboard(name: "cedMageAccounts", bundle: nil)
            let viewControl = view.instantiateViewController(withIdentifier: "cmsWebView") as! cedMageCmsWebView
            let url = banner["product_id"]
            viewControl.pageUrl = url!
            self.navigationController?.pushViewController(viewControl, animated: true)
            
            
        }
    }
    
    
    
    
}
