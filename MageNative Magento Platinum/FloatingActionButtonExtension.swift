/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */


import UIKit

extension ProductSinglePageViewController{
    
    /* floating button section:: start */
    func renderFloatingActionButton(){
        let color = Settings.themeColor
        let theme_color = color
        
        self.view.addConstraint(NSLayoutConstraint(item: imageViewAsOverlay, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 0));
        self.view.addConstraint(NSLayoutConstraint(item: imageViewAsOverlay, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 0));
        self.view.addConstraint(NSLayoutConstraint(item: imageViewAsOverlay, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1, constant: 0));
        self.view.addConstraint(NSLayoutConstraint(item: imageViewAsOverlay, attribute: NSLayoutConstraint.Attribute.bottom, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: 0));
        
        // rendering floatingActionButton
        floatingActionButton = UIButton();
        floatingActionButton.translatesAutoresizingMaskIntoConstraints = false;
        self.view.addSubview(floatingActionButton);
        floatingActionButton.layer.borderWidth =  CGFloat(2);
        floatingActionButton.layer.borderColor = theme_color.cgColor;
        floatingActionButton.setImage(UIImage(named:"plusSign"), for: UIControl.State.normal)
        floatingActionButton.imageEdgeInsets = UIEdgeInsets.init(top: 5, left: 5, bottom: 5, right: 5);
        floatingActionButton.addTarget(self, action: #selector(ProductSinglePageViewController.showChildFloatingActionButtons(_:)), for: UIControl.Event.touchUpInside);
        self.view.addConstraint(NSLayoutConstraint(item: floatingActionButton, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: translateAccordingToDevice(50)));
        self.view.addConstraint(NSLayoutConstraint(item: floatingActionButton, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: translateAccordingToDevice(50)));
        self.view.addConstraint(NSLayoutConstraint(item: floatingActionButton, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: -10));
        self.view.addConstraint(NSLayoutConstraint(item: floatingActionButton, attribute: NSLayoutConstraint.Attribute.bottom, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: -(translateAccordingToDevice(50)+10)));
        floatingActionButton.makeViewCircled(size: translateAccordingToDevice(50));
        
        
        
        stackViewForChildFloatingButton = UIStackView();
        stackViewForChildFloatingButton.translatesAutoresizingMaskIntoConstraints = false;
        stackViewForChildFloatingButton.axis  = NSLayoutConstraint.Axis.vertical;
        stackViewForChildFloatingButton.distribution  = UIStackView.Distribution.equalSpacing;
        stackViewForChildFloatingButton.alignment = UIStackView.Alignment.center;
        stackViewForChildFloatingButton.spacing   = 5.0;
        self.view.addSubview(stackViewForChildFloatingButton);
        self.view.addConstraint(NSLayoutConstraint(item: stackViewForChildFloatingButton, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: translateAccordingToDevice(200)));
        self.view.addConstraint(NSLayoutConstraint(item: stackViewForChildFloatingButton, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: -20));
        self.view.addConstraint(NSLayoutConstraint(item: stackViewForChildFloatingButton, attribute: NSLayoutConstraint.Attribute.bottom, relatedBy: NSLayoutConstraint.Relation.equal, toItem: floatingActionButton, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1, constant: -10));
        
        for (_,optionInfo) in self.childOptionsArray {
            let childFloatingButtonView = ChildFloatingButtonView();
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(ProductSinglePageViewController.childFloatingButtonViewTapped(_:)));
            childFloatingButtonView.addGestureRecognizer(tapGesture);
            tapGesture.delegate=self
            childFloatingButtonView.childOptionLabel.text = optionInfo.keys.first;
//            childFloatingButtonView.childOptionLabel.textColor = theme_color;
            childFloatingButtonView.childOptionLabel.font = UIFont(fontName: "HelveticaNeue-Medium", fontSize: CGFloat(14.0));
            
            childFloatingButtonView.childOptionImage.image = UIImage(named:optionInfo.values.first!);
            childFloatingButtonView.childOptionImage.layer.cornerRadius = 0.5 * translateAccordingToDevice(CGFloat(35));
            childFloatingButtonView.childOptionImage.contentMode = UIView.ContentMode.scaleAspectFit;
            childFloatingButtonView.childOptionImage.layer.borderWidth =  CGFloat(2);
            childFloatingButtonView.childOptionImage.layer.borderColor = theme_color.cgColor;
            
            childFloatingButtonView.heightAnchor.constraint(equalToConstant: 35).isActive = true;
            stackViewForChildFloatingButton.addArrangedSubview(childFloatingButtonView);
            self.setLeadingAndTralingSpaceFormParentView(childFloatingButtonView, parentView:stackViewForChildFloatingButton);
            
            if(!cedMage.checkModule(string:"Ced_Mobiconnectreview")){
                if(optionInfo.values.first == "WriteReview"){
                    childFloatingButtonView.isHidden = true;
                }
            }
            
        }
        stackViewForChildFloatingButton.isHidden = true;
        isChildOptionsVisible = false;
    }
    
    @objc func showChildFloatingActionButtons(_ sender:UIButton){
        print("isChildOptionsVisible");
        print(isChildOptionsVisible);
        self.rotateFloatingActionButton();
        if(isChildOptionsVisible){
            isChildOptionsVisible = false;
            imageViewAsOverlay.isHidden = true;
            stackViewForChildFloatingButton.isHidden = true;
            return;
        }
        isChildOptionsVisible = true;
        imageViewAsOverlay.isHidden = false;        
        stackViewForChildFloatingButton.isHidden = false;
        isChildOptionsVisible = true;
        self.animateChildOptionButtonAppearance();
    }
    
    @objc func childFloatingButtonViewTapped(_ recognizer:UITapGestureRecognizer){
        
        if let childFloatingButtonView = recognizer.view as? ChildFloatingButtonView{

            if(childFloatingButtonView.childOptionImage.image == UIImage(named:"Share")){
              //  self.shareProductTapped();
            }
            else if(childFloatingButtonView.childOptionImage.image == UIImage(named:"DetailDescription")){
              //  self.viewDetailDescriptionTapped(sender: );
            }
            else if(childFloatingButtonView.childOptionImage.image == UIImage(named:"WriteReview")){
                self.showReviewPopup();
            }
            
        }
        
    }
    
    @objc func shareProductTapped(sender:UIButton){
        
//        self.floatingActionButton.sendActions(for: .touchUpInside);
        
        let url =  self.productInfoArray["product-url"];
        if(url == nil || url == ""){
            let msg = "Product URL not found".localized;
            self.view.makeToast(msg, duration: 2.0, position: .bottom, title: nil, image: nil, style: nil, completion: nil);
            return;
        }
        let vc = UIActivityViewController(activityItems: [url!], applicationActivities: nil);
        if(UIDevice().model.lowercased() == "ipad".lowercased()){
            vc.popoverPresentationController?.sourceView = sender
        }
        vc.modalPresentationStyle = .fullScreen;
        self.present(vc, animated: true, completion: nil);
    }
    
    @objc func viewDetailDescriptionTapped(sender:Any){
        
//        self.floatingActionButton.sendActions(for: .touchUpInside);
        
        let storyboard = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil);
        let viewController = storyboard.instantiateViewController(withIdentifier: "productDetailDescriptionViewController") as! ProductDetailDescriptionViewController;
        viewController.productDetail = self.productInfoArray["short-description"]!;
        viewController.productDescription = self.productInfoArray["description"]!;
        viewController.cmsPage = self.productInfoArray["cms-title"]!
        viewController.cmsUrl = self.productInfoArray["cms-content"]!;
        self.navigationController?.pushViewController(viewController, animated: true);
    }
    
    func showReviewPopup(){
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(ProductSinglePageViewController.dismissReviewPopupSection(_:)));
        imageViewAsOverlay.addGestureRecognizer(tapGesture);
        tapGesture.delegate=self
        
        self.floatingActionButton.sendActions(for: .touchUpInside);
        
        self.imageViewAsOverlay.isHidden = false;
        self.floatingActionButton.isHidden = true;
        
        self.reviewPopupView.isHidden = false;
        self.reviewPopupView.alpha = 0.0;
        
        UIView.animate(withDuration: 1.0, delay: 0, options: UIView.AnimationOptions.curveLinear, animations: {
            self.reviewPopupView.alpha = 1.0;
            }, completion: nil
        );
    }
    
    @objc func dismissReviewPopupSection(_ recognizer: UITapGestureRecognizer){
        
        self.imageViewAsOverlay.isHidden = true;
        self.floatingActionButton.isHidden = false;
        
        self.reviewPopupView.isHidden = true;
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(ProductSinglePageViewController.dismissFloatingButtonSection(_:)));
        imageViewAsOverlay.addGestureRecognizer(tapGesture);
        tapGesture.delegate=self
    }
    
    @objc func dismissFloatingButtonSection(_ recognizer: UITapGestureRecognizer){
        print("dismissFloatingButtonSection");
        self.dismissFloatingActionButtonSetup();
    }
    
    func dismissFloatingActionButtonSetup(){
        self.floatingActionButton.sendActions(for: .touchUpInside);
    }
    
    func animateChildOptionButtonAppearance(){
        print("animateChildOptionButtonAppearance???");
        var index = stackViewForChildFloatingButton.subviews.count;
        print(index);
        print("++++");
        for view in stackViewForChildFloatingButton.subviews{
            view.alpha = 0.0;
        }
        for view in stackViewForChildFloatingButton.subviews{
            print(index);
            UIView.animate(withDuration: 1.0, delay: 0.1 * Double(index), options: UIView.AnimationOptions.curveEaseIn, animations: {
                view.alpha = 1.0;
                }, completion: nil
            );
            index -= 1;
        }
    }
    
    func rotateFloatingActionButton(){
        let rotation = self.isChildOptionsVisible ? 0 : CGFloat(Double.pi/4)
        UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 0.2, initialSpringVelocity: 10, options: [], animations: {
            self.floatingActionButton.transform = CGAffineTransform(scaleX: 0.8, y: 0.8);
            self.floatingActionButton.transform = CGAffineTransform(scaleX: 1, y: 1);
            }, completion: { completed in
                UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 0.8, options: UIView.AnimationOptions.allowAnimatedContent, animations: {
                    self.floatingActionButton.transform = CGAffineTransform(rotationAngle: rotation);
                    }, completion: nil
                );
        });
    }
    /* floating button section:: end */
    @objc func addToCompareTapped(_ sender:UIButton){
        var param = [String:String]()
               
        param["prodID"] = self.productInfoArray["product-id"]!
        if UserDefaults.standard.bool(forKey: "isLogin"){
            if(UserDefaults.standard.object(forKey: "userInfoDict") != nil){
                let userInfoDict = UserDefaults.standard.object(forKey: "userInfoDict") as! [String:String]
            param["customer_id"] = userInfoDict["customerId"] ?? ""
            }
        }
        sendRequest(url: "mobiconnect/addtocompare", params: param)
    }
}
