/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */

import UIKit

class cedMageSingleOrder: cedMageViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var orderStatusLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    var orderId:String?
    var otherData = [String:String]()
    var orderItemsDict = [[String:String]]()
    var optionArray = [Int:[Int:[String:String]]]();
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(ReorderButtonTC.self, forCellReuseIdentifier: ReorderButtonTC.reuseID)
        tableView.delegate = self
        tableView.dataSource = self
        orderStatusLabel.fontColorTool()
        let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
        let hashKey = userInfoDict["hashKey"]!;
        let  customerId = userInfoDict["customerId"]!;
         let postString = ["hashkey":hashKey,"customer_id":customerId,"order_id":orderId!];
        print(postString)
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 250
        self.sendRequest(url: "mobiconnect/customer/orderview", params: postString)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func recieveResponse(data: Data?, requestUrl: String?, response: URLResponse?) {
        if let data = data {
            guard var json = try? JSON(data: data) else{return;}
        json = json[0]
        print(json);
        //print(json["data"]["address"]);
        
        otherData["nameOnCard"] = json["data"]["orderview"][0]["name_on_card"].stringValue;
            otherData["country"] = json["data"]["orderview"][0]["country"].stringValue;
          otherData["shipping"] = json["data"]["orderview"][0]["shipping"].stringValue;
            otherData["orderDate"] = json["data"]["orderview"][0]["orderdate"].stringValue;
            otherData["street"] = json["data"]["orderview"][0]["street"].stringValue;
           otherData["methodTitle"] = json["data"]["orderview"][0]["method_title"].stringValue;
            otherData["discount"] = json["data"]["orderview"][0]["discount"].stringValue;
            otherData["shippingMethod"] = json["data"]["orderview"][0]["shipping_method"].stringValue;
          otherData["city"] = json["data"]["orderview"][0]["city"].stringValue;
            otherData["state"] = json["data"]["orderview"][0]["state"].stringValue;
          otherData["taxAmount"] = json["data"]["orderview"][0]["tax_amount"].stringValue;
             otherData["grandtotal"] = json["data"]["orderview"][0]["grandtotal"].stringValue;
           otherData["pincode"] = json["data"]["orderview"][0]["pincode"].stringValue
            otherData["subtotal"] = json["data"]["orderview"][0]["subtotal"].stringValue;
            
            
           otherData["mobile"] = json["data"]["orderview"][0]["mobile"].stringValue;
            otherData["method_code"] = json["data"]["orderview"][0]["method_code"].stringValue;
            
            
             otherData["credit_card_type"] = json["data"]["orderview"][0]["credit_card_type"].stringValue;
            
            
          otherData["ship_to"] = json["data"]["orderview"][0]["ship_to"].stringValue;
            
            
         otherData["credit_card_number"] = json["data"]["orderview"][0]["credit_card_number"].stringValue;
            
            
            otherData["orderlabel"] = json["data"]["orderview"][0]["orderlabel"].stringValue;
            
           self.title =  otherData["orderlabel"]
        
            let orderedItemsJSON = json["data"]["orderview"][0]["ordered_items"];
             print(orderedItemsJSON)
            var tempOption = [String:String]();
            var indexedTempOption = [Int:[String:String]]();
            var inrCounter = 0;
        
            for c in (0..<orderedItemsJSON.count)          {
                print("source");
                tempOption = [String:String]();
            
                print(orderedItemsJSON[c]["option"])
                
                inrCounter = 0;
                for val in orderedItemsJSON[c]["option"].arrayValue
                {
                
                    for (inrKey,inrVal) in val
                    {
                        print(inrKey);
                            print(inrVal);
                        tempOption[inrKey] = inrVal.stringValue;
                    }
                    
                    indexedTempOption[inrCounter] = tempOption;
                    inrCounter += 1;
                }
                self.optionArray[c] = indexedTempOption;
                let product_id=orderedItemsJSON[c]["product_id"].stringValue;
                let product_qty=orderedItemsJSON[c]["product_qty"].stringValue;
                let rowsubtotal=orderedItemsJSON[c]["rowsubtotal"].stringValue;
                let phone=orderedItemsJSON[c]["phone"].stringValue;
                let item_gift_detail=orderedItemsJSON[c]["item-gift-detail"].stringValue;
                let product_image=orderedItemsJSON[c]["product_image"].stringValue;
                let product_price=orderedItemsJSON[c]["product_price"].stringValue;
                let product_name=orderedItemsJSON[c]["product_name"].stringValue;
                let product_type=orderedItemsJSON[c]["product_type"].stringValue;
                let v = ["product_id":product_id,"product_qty":product_qty,"rowsubtotal":rowsubtotal,"phone":phone,"item_gift_detail":item_gift_detail,"product_image":product_image,"product_price":product_price,"product_name":product_name,"product_type":product_type];
                self.orderItemsDict.append(v);
                
            }
        orderStatusLabel.text =  otherData["orderlabel"]
        tableView.reloadData()
        }
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            if(orderItemsDict.count != 0) {
                return 5+orderItemsDict.count
            }
            return 0;
        default:
            return 1
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            if(indexPath.row == 0) {
                let cell = tableView.dequeueReusableCell(withIdentifier: "orderCell") as! cedMageSingleOrderCell
                cell.headingLabel.text = "ORDER DATE".localized
                cell.label.text = otherData["orderDate"]!
                
                return cell
            }
            else if(indexPath.row == 1) {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "orderCell") as! cedMageSingleOrderCell
                
                var shippingAddress = otherData["ship_to"]!+"\n";
                shippingAddress += otherData["street"]!+"\n";
                shippingAddress += otherData["city"]!+"\n";
                shippingAddress += otherData["state"]!+"\n";
                shippingAddress += otherData["pincode"]!+"\n";
                shippingAddress += otherData["country"]!+"\n";
                shippingAddress += otherData["mobile"]!+"\n";
                
                cell.headingLabel.text = "SHIPPING ADDRESS".localized
                cell.label.text = shippingAddress
                cell.contentView.cardView()
                return cell
            }
            else if(indexPath.row == 2) {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "orderCell") as! cedMageSingleOrderCell
                cell.headingLabel.text = "SHIPPING METHOD".localized
                
                cell.label.text  = self.otherData["shippingMethod"]!;
                cell.contentView.cardView()
                return cell
            }
            else if(indexPath.row == 3) {
                let cell = tableView.dequeueReusableCell(withIdentifier: "orderCell") as! cedMageSingleOrderCell
                
                cell.headingLabel.text = "PAYMENT METHOD".localized
                cell.label.text  = self.otherData["methodTitle"]!;
                cell.contentView.cardView()
                return cell
                
            }
            else if(indexPath.row == 4+orderItemsDict.count) {
                let cell = tableView.dequeueReusableCell(withIdentifier: "orderCell") as! cedMageSingleOrderCell
                cell.headingLabel.text = "Total Price".localized
                var stringToShow = "Sub Total :".localized+"\t"
                stringToShow  += self.otherData["subtotal"]!+"\n";
                stringToShow += "Tax Price : ".localized+"\t"
                stringToShow  += self.otherData["taxAmount"]!+"\n";
                stringToShow += "Shipping : ".localized+"\t"
                stringToShow  += self.otherData["shipping"]!+"\n";
                stringToShow += "Discount : ".localized+"\t"
                stringToShow  += self.otherData["discount"]!+"\n";
                stringToShow += "Grand Total:".localized+"\t"
                stringToShow  += self.otherData["grandtotal"]!;
                cell.label.text = stringToShow
                
                return cell
            }
            else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "productCell") as! cedMageOrdrProductCell
                print(optionArray )
                let title = orderItemsDict[indexPath.row-4]["product_name"]!+"\n";
                var proInfo = "Price : ".localized
                proInfo += orderItemsDict[indexPath.row-4]["product_price"]!+"\n";
                proInfo += "Qty : ".localized+orderItemsDict[indexPath.row-4]["product_qty"]!+"\n";
                proInfo += "Amount : ".localized+orderItemsDict[indexPath.row-4]["rowsubtotal"]!+"\n";
                
                if(orderItemsDict[indexPath.row-4]["product_type"]! == "bundle")
                {
                    proInfo += "\n";
                    if(self.optionArray[indexPath.row-4] != nil)
                    {
                        let loopArr = self.optionArray[indexPath.row-4]!;
                        print(loopArr);
                        for (key,val) in loopArr
                        {
                            print(key);
                            print(val);
                            proInfo += val["option_title"]!+" : ";
                            proInfo += val["option_value"]!+"\n";
                            proInfo += "\n";
                        }
                        
                    }
                }
                if(orderItemsDict[indexPath.row-4]["product_type"]! == "configurable")
                {
                    proInfo += "\n";
                    if(self.optionArray[indexPath.row-4] != nil)
                    {
                        let loopArr = self.optionArray[indexPath.row-4]!;
                        for (_,val) in loopArr
                        {
                            proInfo += val["option_title"]!+" : ";
                            proInfo += val["option_value"]!+"\n";
                            proInfo += "\n";
                        }
                        
                    }
                }
                
                if(orderItemsDict[indexPath.row-4]["product_type"]! == "downloadable")
                {
                    proInfo += "\n";
                    if(self.optionArray[indexPath.row-4] != nil)
                    {
                        let loopArr = self.optionArray[indexPath.row-4]!;
                        
                        print(loopArr);
                        
                        for (key,val) in loopArr
                        {
                            print(key);
                            print(val);
                            if(val["link_title"] != nil){
                                proInfo += val["link_title"]!
                            }
                            proInfo += "\n";
                        }
                        
                    }
                }
                cell.productTitle.setTitle(title, for: UIControl.State.normal)
                cell.productTitle.fontColorTool()
                cell.productDesc.text = proInfo;
                cedMageImageLoader.shared.loadImgFromUrl(urlString: orderItemsDict[indexPath.row-4]["product_image"]!,completionHandler: { (image: UIImage?, url: String) in
                    DispatchQueue.main.async {
                        cell.productImage.image = image
                    }
                })
                
                return cell;
                
                
            }
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: ReorderButtonTC.reuseID, for: indexPath) as! ReorderButtonTC
            cell.delegate = self
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            if(indexPath.row == 0 || indexPath.row == 2 || indexPath.row == 3) {
                return 70;
            }
            else if(indexPath.row == 1) {
                return 180;
            }
            else if(indexPath.row >= 4 && indexPath.row <= 3+orderItemsDict.count) {
                if(orderItemsDict[indexPath.row-4]["product_type"]! == "bundle" || orderItemsDict[indexPath.row-4]["product_type"]! == "configurable") {
                    return 200;
                }
                if(orderItemsDict[indexPath.row-4]["product_type"]! == "downloadable") {
                    return 180;
                }
                return 140;
            }
            else {
                return UITableView.automaticDimension
            }
        default:
            return 80
        }
    }
    
}

//MARK:- OrderViewDelegate

extension cedMageSingleOrder: OrderViewDelegate {
    func handleReorder() {
        let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]()
        let hashKey = userInfoDict["hashKey"]!
        let customerId = userInfoDict["customerId"]!
        
        let param = ["hashkey":hashKey, "customer_id": customerId, "order_id": orderId ?? ""]
        
        ApiHandler.handle.request(with: "mobiconnect/customer/reorder", params: param, requestType: .POST, controller: self, completion: { data, error in
            guard let data = data else { return }
            guard let json = try? JSON(data: data) else { return }
            print(json)
        })
    }
}
