/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */


import UIKit

class MagenativeUIViewController : cedMageViewController{
     let dropDown = DropDown()
    var userInfoDict = [String:String]();
    var scrollView: UIScrollView! //main scrollview to provide scrolling option to stackview
    var stackView: UIStackView! // stackview as core wrapperview
    var padding = CGFloat(10);
    
    
    let screenSize: CGRect = UIScreen.main.bounds;
    var responseReceivedFromAPI : JSON!;
    
    let floatButtonRadius = CGFloat(50);
    
    
    func parseResponseReceivedFromAPI(_ jsonResponse:JSON){
        
        responseReceivedFromAPI = jsonResponse;
    }
    
    func getRequestToAPI(_ urlToRequest:String, dataToPost:[String:String]){
        var urlToRequest = urlToRequest
        
        cedMageLoaders.addDefaultLoader(me: self);
        if(defaults.object(forKey: "userInfoDict") != nil){
            userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
        }
        
        //let requestHeader = cedMage.getInfoPlist(fileName:"cedMage",indexString: "requestheader") as! String
        let baseURL = Settings.baseUrl;
        
        urlToRequest = baseURL+urlToRequest;
        
        var postString = "";
        var postData = [String:String]()
        if defaults.bool(forKey: "isLogin") {
            postData["hashkey"] = userInfoDict["hashKey"]
            postData["customer_id"] = userInfoDict["customerId"]
            for (key,val) in dataToPost{

               postData[key] = val;
            }
        }
        
        else{
            
            for (key,val) in dataToPost{
                postData[key] = val;
            }
        }
        postString = ["parameters":postData].convtToJson() as String
        //urlToRequest = urlToRequest + postString;
        print(urlToRequest);
        print(postString);
        
        var request = URLRequest(url: URL(string: "\(urlToRequest)")!);
        request.httpMethod = "GET";
        print("NSURLSession");
        let task = URLSession.shared.dataTask(with: request){
            
            // check for fundamental networking error
            data, response, error in
            guard error == nil && data != nil else{
                print("error=\(error)")
                DispatchQueue.main.async{
                    print(error?.localizedDescription as Any);
                    cedMageLoaders.removeLoadingIndicator(me: self);
                    cedMageHttpException.showAlertView(me: self, msg: error?.localizedDescription, title: "Error".localized )
                }
                return;
            }
            
            // check for http errors
            if let httpStatus = response as? HTTPURLResponse , httpStatus.statusCode != 200{
                
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(response)")
                DispatchQueue.main.async{
                    cedMageLoaders.removeLoadingIndicator(me: self);
                    cedMageHttpException.showHttpErrorImage(me: self, img: "no_module")
                    
                }
                return;
            }
            
            // code to fetch values from response :: start
            
            let datastring = NSString(data: data!, encoding: String.Encoding.utf8.rawValue);
                //NSString(data: NSData!, encoding: String.Encoding.utf8)
            print("datastring");
            print(datastring as Any);
            
            guard let jsonResponse = try? JSON(data: data!) else{return;}
            if(jsonResponse != nil){
                
                DispatchQueue.main.async{
                    
                    //print(jsonResponse);
                    cedMageLoaders.removeLoadingIndicator(me: self);
                    self.parseResponseReceivedFromAPI(jsonResponse);
                    
                }

            }
            
        }

        postString = ["parameters":postData].convtToJson() as String

        task.resume();
    }
    
    
    func makeRequestToAPI(_ urlToRequest:String, dataToPost:[String:String]?){
        var urlToRequest = urlToRequest
        
        cedMageLoaders.addDefaultLoader(me: self);
        if(defaults.object(forKey: "userInfoDict") != nil){
            userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
        }
        
        let requestHeader = Settings.headerKey
        let baseURL = Settings.baseUrl;
        
        urlToRequest = baseURL+urlToRequest;
        
        var postString = "";
        var postData = [String:String]()
        if defaults.bool(forKey: "isLogin") {
            postData["hashkey"] = userInfoDict["hashKey"]
            postData["customer_id"] = userInfoDict["customerId"]
            if let data = dataToPost{
                for (key,val) in data{
                    postData[key] = val;
                }
            }
            
        }
        else{
            if let data = dataToPost{
                for (key,val) in data{
                    postData[key] = val;
                }
            }
        }
        postString = ["parameters":postData].convtToJson() as String
        

        print(urlToRequest);
        print(postString);
        
        var request = URLRequest(url: URL(string: "\(urlToRequest)")!);
        request.httpMethod = "POST";

        request.httpBody = postString.data(using: String.Encoding.utf8);
        //request.setValue(requestHeader, forHTTPHeaderField: "Mobiconnectheader")
        if UserDefaults.standard.bool(forKey: "isLogin"){
            if let user = UserDefaults.standard.object(forKey: "userInfoDict") as? [String:String] {
                request.setValue(user["hashKey"], forHTTPHeaderField: "hashkey")
            }
            
        }
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        print("NSURLSession");
        let task = URLSession.shared.dataTask(with: request){
            
            // check for fundamental networking error
            data, response, error in
            guard error == nil && data != nil else{
                print("error=\(String(describing: error))")
                DispatchQueue.main.async{
                    print(error?.localizedDescription as Any);
                    cedMageLoaders.removeLoadingIndicator(me: self);
                    cedMageHttpException.showAlertView(me: self, msg: error?.localizedDescription, title: "Error".localized )
                }
                return;
            }
            
            // check for http errors
            if let httpStatus = response as? HTTPURLResponse , httpStatus.statusCode != 200{
                
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(response)")
                DispatchQueue.main.async{
                    cedMageLoaders.removeLoadingIndicator(me: self);
                    cedMageHttpException.showHttpErrorImage(me: self, img: "no_module")
                    
                }
                return;
            }
            
            // code to fetch values from response :: start
            
            let datastring = NSString(data: data!, encoding: String.Encoding.utf8.rawValue);
            //NSString(data: NSData!, encoding: String.Encoding.utf8)
            print("datastring");
            print(datastring as Any);
            
            guard let jsonResponse = try? JSON(data: data!) else{return;}
            if(jsonResponse != nil){
                
                DispatchQueue.main.async{
                    
                    //print(jsonResponse);
                    cedMageLoaders.removeLoadingIndicator(me: self);
                    self.parseResponseReceivedFromAPI(jsonResponse);
                    
                }
            }
            
        }
        task.resume();
    }

    
    func downloadImageFromServer(_ imgView:UIImageView,urlToRequest:String){
        
        var activityIndicator = UIActivityIndicatorView();
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false;
        activityIndicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.whiteLarge);
        activityIndicator.frame = CGRect(x: 0, y: 0, width: 50, height: 50);
        activityIndicator.startAnimating();
        activityIndicator.color = UIColor.red;
        activityIndicator.center.x = imgView.bounds.width/2;
        activityIndicator.center.y = imgView.bounds.height/2;
        imgView.addSubview(activityIndicator);
        
        let imgRequest = URLRequest(url: URL(string: urlToRequest)!);
        let task = URLSession.shared.dataTask(with: imgRequest){
            // check for fundamental networking error
            data, response, error in
            guard error == nil && data != nil else
            {
                print("error=\(error)")
                return;
            }
                
            // check for http errors
            if let httpStatus = response as? HTTPURLResponse , httpStatus.statusCode != 200{
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(response)")
                return;
            }
                
            // Convert the downloaded data in to a UIImage object
            let image = UIImage(data: data!)
            // Store the image in to our cache
            //self.imageCache[urlString] = image
            // Update the cell
            DispatchQueue.main.async
            {
                activityIndicator.removeFromSuperview();
                imgView.image = image;
            }
                
        }
        task.resume();

    }
    
    func basicFoundationToRenderView(bottomMargin:CGFloat){
        
        // adding scrollview
        scrollView = UIScrollView();
        scrollView.translatesAutoresizingMaskIntoConstraints = false;
        scrollView.showsHorizontalScrollIndicator = false;
        scrollView.showsVerticalScrollIndicator = false;
        view.addSubview(scrollView);
        self.view.addConstraint(NSLayoutConstraint(item: scrollView, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 0));
        self.view.addConstraint(NSLayoutConstraint(item: scrollView, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 0));
        self.view.addConstraint(NSLayoutConstraint(item: scrollView, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1, constant: 0));
        self.view.addConstraint(NSLayoutConstraint(item: scrollView, attribute: NSLayoutConstraint.Attribute.bottom, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: -(bottomMargin)));
        
        // adding stackview
        stackView = UIStackView();
        stackView.translatesAutoresizingMaskIntoConstraints = false;
        stackView.axis  = NSLayoutConstraint.Axis.vertical;
        stackView.distribution  = UIStackView.Distribution.equalSpacing;
        stackView.alignment = UIStackView.Alignment.center;
        stackView.spacing   = 1.0;
        scrollView.addSubview(stackView);
        self.view.addConstraint(NSLayoutConstraint(item: stackView, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 0));
        self.view.addConstraint(NSLayoutConstraint(item: stackView, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 0));
        scrollView.addConstraint(NSLayoutConstraint(item: stackView, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: scrollView, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1, constant: 0));
        scrollView.addConstraint(NSLayoutConstraint(item: stackView, attribute: NSLayoutConstraint.Attribute.bottom, relatedBy: NSLayoutConstraint.Relation.equal, toItem: scrollView, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: 0));
    }
    func basicFoundationToRenderView(topMargin:CGFloat){
        
        // adding scrollview
        scrollView = UIScrollView();
        scrollView.translatesAutoresizingMaskIntoConstraints = false;
        scrollView.showsHorizontalScrollIndicator = false;
        scrollView.showsVerticalScrollIndicator = false;
        view.addSubview(scrollView);
        self.view.addConstraint(NSLayoutConstraint(item: scrollView, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 0));
        self.view.addConstraint(NSLayoutConstraint(item: scrollView, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 0));
        
        self.view.addConstraint(NSLayoutConstraint(item: scrollView, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1, constant: topMargin));
        self.view.addConstraint(NSLayoutConstraint(item: scrollView, attribute: NSLayoutConstraint.Attribute.bottom, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: 0));
        
        // adding stackview
        stackView = UIStackView();
        stackView.translatesAutoresizingMaskIntoConstraints = false;
        stackView.axis  = NSLayoutConstraint.Axis.vertical;
        stackView.distribution  = UIStackView.Distribution.equalSpacing;
        stackView.alignment = UIStackView.Alignment.center;
        stackView.spacing   = 1.0;
        scrollView.addSubview(stackView);
        self.view.addConstraint(NSLayoutConstraint(item: stackView, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 0));
        self.view.addConstraint(NSLayoutConstraint(item: stackView, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 0));
        scrollView.addConstraint(NSLayoutConstraint(item: stackView, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: scrollView, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1, constant: 0));
        scrollView.addConstraint(NSLayoutConstraint(item: stackView, attribute: NSLayoutConstraint.Attribute.bottom, relatedBy: NSLayoutConstraint.Relation.equal, toItem: scrollView, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: 0));
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews();
        if let stack = stackView {
        scrollView.contentSize = CGSize(width: stack.frame.width, height: stack.frame.height);
        }
    }
    
    func makeSomeSpaceInStackView(height:CGFloat){
        let fakeView = UIView();
        fakeView.translatesAutoresizingMaskIntoConstraints = false;
        stackView.addArrangedSubview(fakeView);
        let fakeViewHeight = translateAccordingToDevice(CGFloat(height));
        fakeView.heightAnchor.constraint(equalToConstant: fakeViewHeight).isActive = true;
        self.setLeadingAndTralingSpaceFormParentView(fakeView, parentView:stackView);
    }
    
    func setLeadingAndTralingSpaceFormParentView(_ view:UIView,parentView:UIView){
        parentView.addConstraint(NSLayoutConstraint(item: view, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: parentView, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: padding/2));
        parentView.addConstraint(NSLayoutConstraint(item: view, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: parentView, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: -padding/2));
    }
    
    func setLeadingAndTralingSpaceFormParentView(_ view:UIView,parentView:UIView,padding:CGFloat){
        parentView.addConstraint(NSLayoutConstraint(item: view, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: parentView, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: padding));
        parentView.addConstraint(NSLayoutConstraint(item: view, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: parentView, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: -padding));
    }
}
