/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */


import UIKit

class ProductListInfoOverImageView: UIView {

    // Our custom view from the XIB file
    var view: UIView!
    
    //outlets
    
    
    //@IBOutlet weak var productInfoView: ProductInfoView!
    
    //@IBOutlet weak var showProductInfoButton: UIButton!
    //@IBOutlet weak var hideProductInfoButton: UIButton!
    
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var wishlistButton: UIButton!
    
    
    override init(frame: CGRect)
    {
        // 1. setup any properties here
        
        // 2. call super.init(frame:)
        super.init(frame: frame)
        
        // 3. Setup view from .xib file
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        // 1. setup any properties here
        
        // 2. call super.init(coder:)
        super.init(coder: aDecoder)
        
        // 3. Setup view from .xib file
        xibSetup()
    }
    
    func xibSetup()
    {
        view = loadViewFromNib()
        
        // use bounds not frame or it'll be offset
        view.frame = bounds
        
        // Make the view stretch with containing view
        view.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        
        //extra setup
        self.makeCard(self, cornerRadius: 2, color: UIColor.black, shadowOpacity: 0.4);
        
        /*let darkBlur = UIBlurEffect(style: UIBlurEffectStyle.extraLight);
        let blurView = UIVisualEffectView(effect: darkBlur);
        blurView.frame = self.productInfoView.bounds;
        self.productInfoView.addSubview(blurView);
        
        //hideProductInfoButton.isHidden = true;
        //productInfoView.isHidden = true;
        //showProductInfoButton.isHidden = false;
        
        if(!isProductSpecialPriceVisible){
            productInfoView.productSpecialPrice.isHidden = true;
        }*/
        
        /*showProductInfoButton.addTarget(self, action: #selector(ProductListInfoOverImageView.showProductInfoSection(_:)), for: UIControlEvents.touchUpInside);
        hideProductInfoButton.addTarget(self, action: #selector(ProductListInfoOverImageView.hideProductInfoSection(_:)), for: UIControlEvents.touchUpInside);*/
        //extra setup
        
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(view)
   
    }
    
    func loadViewFromNib() -> UIView{
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "ProductListInfoOverImageView", bundle: bundle)
        
        // Assumes UIView is top level and only object in CustomView.xib file
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    
    /*func showProductInfoSection(_ sender:UIButton){
        print("showProductInfoSection");
        
        hideProductInfoButton.isHidden = false;
        productInfoView.isHidden = false;
        showProductInfoButton.isHidden = true;
        
        self.productInfoView.center.y += self.view.bounds.height;
        UIView.animate(withDuration: 1.0, delay: 0, options: UIViewAnimationOptions.curveLinear, animations: {
            self.productInfoView.center.y -= self.view.bounds.height;
            }, completion: nil
        );
    }
    
    func hideProductInfoSection(_ sender:UIButton){
        print("hideProductInfoSection");
        
        hideProductInfoButton.isHidden = true;
        productInfoView.isHidden = true;
        showProductInfoButton.isHidden = false;
        
    }*/
    
}
