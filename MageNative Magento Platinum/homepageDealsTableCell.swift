//
//  homepageDealsTableCell.swift
//  MageNative Magento Platinum
//
//  Created by cedcoss on 16/11/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class homepageDealsTableCell: UITableViewCell {
  
  @IBOutlet weak var topLabel: UILabel!
  @IBOutlet weak var dealCollection: UICollectionView!
  
  @IBOutlet weak var topBackgroundView: UIView!
  @IBOutlet weak var dealTimer: UILabel!
  var dealsContent: [dealContent] = []
  var parent: UIViewController!
  
  var dealtime = 100
  var timer = Timer()
  var startTime = TimeInterval()
  var timerStatus = String()
  
  override func awakeFromNib() {
    super.awakeFromNib()
    // Initialization code
    dealCollection.delegate = self
    dealCollection.dataSource = self
    dealCollection.reloadData()
    
    if(timerStatus == "2"){
      dealTimer.isHidden = true
      dealTimer.text = ""
    }else{
      Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(update), userInfo: nil, repeats: true)
    }
  }
  
  @objc func update() {
    if(startTime>0){
      var time = NSInteger(startTime)
      time = time / 1000
      let seconds = time % 60
      let minutes = (time / 60) % 60
      let hours = (time / 3600)
      let days = time / 86400
      dealTimer.text =  String(format: "%0.2d D:%0.2d H:%0.2d M:%0.2d S",days,hours,minutes,seconds)
      startTime = startTime - 1000
    }else {
      
    }
  }
}

extension homepageDealsTableCell: UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dealsContent.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "dealsCollectionCell", for: indexPath) as! dealsCollectionCell
        //cell.insideView.cardView()
        cell.dealImage.sd_setImage(with: URL(string: dealsContent[indexPath.item].deal_image_name!), placeholderImage: UIImage(named: "placeholder"))
        cell.dealTable.text = dealsContent[indexPath.row].offer_text

        cell.dealTable.clipsToBounds = true

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width/2-5, height: 160)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        switch dealsContent[indexPath.item].deal_type {
        case "1":
            let vc = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "productSinglePageViewController") as! ProductSinglePageViewController
            vc.product_id = dealsContent[indexPath.item].relative_link!
            parent.navigationController?.pushViewController(vc, animated: true)
        case "2":
            let vc = UIStoryboard(name: "categorylayouts", bundle: nil).instantiateViewController(withIdentifier: "cedMageDefaultCollection") as!  cedMageDefaultCollection
            vc.selectedCategory = dealsContent[indexPath.item].relative_link!
            parent.navigationController?.pushViewController(vc, animated: true)
        default:
            let view = UIStoryboard(name: "cedMageAccounts", bundle: nil)
            let vc = view.instantiateViewController(withIdentifier: "cmsWebView") as! cedMageCmsWebView
            vc.pageUrl = dealsContent[indexPath.row].category_link!
            parent.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
}
