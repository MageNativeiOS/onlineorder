/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */


import UIKit



class ProductListingViewController: MagenativeUIViewController, UIScrollViewDelegate,UIGestureRecognizerDelegate {
    
    var loadMoreData = true;
    var jsonResponse:JSON!;
    
    @IBOutlet weak var productListingTopSectionView: ProductListingTopSectionView!
    @IBOutlet weak var productListingTopSectionHeight: NSLayoutConstraint!
    
    var currentPageNum = 1;
    
    var searchString = ""; // variable to handle search case
    
    var categoryId:String!
    var  productArray = [[String:String]]();
    var sortByArray = [String:String]();
    
    var noDataImageView : UIImageView!;
    var floatingButton : UIButton!;
    
    override func viewDidLoad() {
        super.viewDidLoad();
        productListingTopSectionView.filterButton.setTitle("FILTERS".localized, for: .normal)
        productListingTopSectionView.sortbyButton.setTitle("SORTBY".localized, for: .normal)
        self.basicFoundationToRenderView(bottomMargin: CGFloat(0.0));
        scrollView.delegate = self;
        
        self.clearViewAndVariables();
        
        if(searchString != ""){
            self.makeRequestToAPI("mobiconnect/catalog/search",dataToPost: ["q":searchString, "page":String(currentPageNum)]);
        }
        else{
            self.makeRequestToAPI("mobiconnect/category/categoryProducts/",dataToPost: ["id":categoryId, "page":String(currentPageNum)]);
        }
        
        
        // Do any additional setup after loading the view.
        
        NotificationCenter.default.addObserver(self, selector: #selector(ProductListingViewController.loadProductsAgain(_:)), name: NSNotification.Name(rawValue: "loadProductsAgainId"), object: nil);
        
    }
    
    @objc func loadProductsAgain(_ notification: NSNotification){
        print("clearViewAndVariables");
        self.clearViewAndVariables();
        print("clearViewAndVariables  Executed");
        
        if(searchString != ""){
            self.makeRequestToAPI("mobiconnect/category/productSearch/",dataToPost: ["q":searchString, "page":String(currentPageNum)]);
        }
        else{
            self.makeRequestToAPI("mobiconnect/category/categoryProducts/",dataToPost: ["id":categoryId, "page":String(currentPageNum)]);
        }
        
        /*if(defaults.object(forKey: "filtersToSend") != nil){
            var filtersToSend = defaults.object(forKey: "filtersToSend") as! String;
            filtersToSend = filtersToSend.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlHostAllowed)!;
            defaults.removeObject(forKey: "filtersToSend");
            self.makeRequestToAPI("mobiconnect/category/categoryProducts/",dataToPost: ["id":categoryId, "page":String(currentPageNum), "multi_filter":filtersToSend]);
        }
        else{
            self.makeRequestToAPI("mobiconnect/category/categoryProducts/",dataToPost: ["id":categoryId, "page":String(currentPageNum)]);
        }*/
    }
    
    
    
    override func parseResponseReceivedFromAPI(_ jsonResponse: JSON) {
        print("jsonResponsejsonResponse");
        print(jsonResponse);
        
        let jsonResponse = jsonResponse[0]
        if jsonResponse["data"]["status"].stringValue == "enabled" {
            if self.jsonResponse.count == 0 {
                self.jsonResponse = jsonResponse[0] ;
            }
        for result in jsonResponse["data"]["products"].arrayValue {
            let product_id = result["product_id"].stringValue;
            let regular_price = result["regular_price"].stringValue;
            let special_price = result["special_price"].stringValue;
            let product_name = result["product_name"].stringValue;
            let product_image = result["product_image"].stringValue;
            let type = result["type"].stringValue;
            let review = result["review"].stringValue;
            let show_both_price = result["show-both-price"].stringValue;
            let Inwishlist = result["Inwishlist"].stringValue
            
            let starting_from = result["starting_from"].stringValue;
            let from_price = result["from_price"].stringValue;
            
            var wishlistItemId = "-1";
            if(result["wishlist-item-id"] != nil){
                wishlistItemId = result["wishlist-item-id"].stringValue;
            }
            
            self.productArray.append(["product_id":product_id,"regular_price":regular_price, "special_price":special_price,"product_name":product_name,"product_image":product_image,"type":type,"review":review,"show-both-price":show_both_price,"starting_from":starting_from,"from_price":from_price,"Inwishlist":Inwishlist,"wishlist-item-id":wishlistItemId]);
        }
        
        for (_,val) in jsonResponse["data"]["sort"]{
            for (keyInr,valInr) in val{
                self.sortByArray[keyInr] = valInr[0].stringValue;
            }
        }
        print(sortByArray)
        stackView.subviews.forEach({ $0.removeFromSuperview() });// helpful for second time product fetch
        self.renderProductListingPage();
            
        }
    }
    
  
    func renderFloatingButton(){
        floatingButton = UIButton();
        floatingButton.translatesAutoresizingMaskIntoConstraints = false;
        floatingButton.setThemeColor();
        self.view.addSubview(floatingButton);
        floatingButton.setTitle("X", for: UIControl.State.normal);
        //floatingButton.setImage(UIImage(named: "AddFileWhite"), for: UIControlState.normal);
        floatingButton.addTarget(self, action: #selector(ProductListingViewController.reloadProductListingAgain(_:)), for: UIControl.Event.touchUpInside);
        //floatingButton.imageEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10);
        self.view.addConstraint(NSLayoutConstraint(item: floatingButton, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: translateAccordingToDevice(30)));
        self.view.addConstraint(NSLayoutConstraint(item: floatingButton, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: translateAccordingToDevice(30)));
        self.view.addConstraint(NSLayoutConstraint(item: floatingButton, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: -10));
        self.view.addConstraint(NSLayoutConstraint(item: floatingButton, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1, constant: (translateAccordingToDevice(50)+30)));
        floatingButton.makeViewCircled(size: translateAccordingToDevice(30));
    }
    
    @objc func reloadProductListingAgain(_ sender:UIButton){
        self.noDataImageView.removeFromSuperview();
        self.floatingButton.removeFromSuperview();
        self.productListingTopSectionView.isHidden = false;
        self.clearViewAndVariables();
        clearFilterRelatedData();

        
        if(searchString != ""){
            self.makeRequestToAPI("mobiconnect/category/productSearch/",dataToPost: ["q":searchString, "page":String(currentPageNum)]);
        }
        else{
            self.makeRequestToAPI("mobiconnect/category/categoryProducts/",dataToPost: ["id":categoryId, "page":String(currentPageNum)]);
        }
        //self.makeRequestToAPI("mobiconnect/category/categoryProducts/",dataToPost: ["id":categoryId, "page":String(currentPageNum)]);

    }
    
    

    

    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        if(!loadMoreData){
            return;
        }
        if (maximumOffset - currentOffset) <= 40 {
            print("Cool");
            currentPageNum = currentPageNum+1;
            if(searchString != ""){
                self.makeRequestToAPI("mobiconnect/category/productSearch/",dataToPost: ["q":searchString, "page":String(currentPageNum)]);
            }
            else{
                self.makeRequestToAPI("mobiconnect/category/categoryProducts/",dataToPost: ["id":categoryId, "page":String(currentPageNum)]);
            }
            //self.makeRequestToAPI("mobiconnect/category/categoryProducts/",dataToPost: ["id":categoryId, "page":String(currentPageNum)]);
        }
        
    }
    
    @objc func productCardTapped(_ recognizer: UITapGestureRecognizer){
        
        if let index = stackView.arrangedSubviews.firstIndex(of: recognizer.view!) {
            print(index);
            _ = productArray[index-1];
            let productview = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "rootPageViewController") as! cedMageProductViewRoot;
            productview.pageData = productArray as NSArray;
            let instance = cedMage.singletonInstance;
            instance.storeParameterInteger(parameter: index-1);
            self.navigationController?.pushViewController(productview
                , animated: true);
        }
        
        /*if let index = stackView.arrangedSubviews.index(of: recognizer.view!) {
            print(index);
            let productInfo = productArray[index-1];
            print(productInfo["product_id"]);
            let productview = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "rootPageViewController") as! cedMageProductViewRoot;
            productview.pageData = productArray;
            let instance = cedMage.singletonInstance;
            instance.storeParameterInteger(parameter: index-1);
            self.navigationController?.pushViewController(productview
                , animated: true);
        }
        else{
            if let productGridView = recognizer.view as? ProductGridView{
                
                if let horizontalStackView = productGridView.superview as? UIStackView{
                    if let horizontalIndex = horizontalStackView.arrangedSubviews.index(of: productGridView) {
                        if let verticalStackView = horizontalStackView.superview as? UIStackView{
                            if let verticalIndex = verticalStackView.arrangedSubviews.index(of: horizontalStackView) {
                                let index = (((verticalIndex-1)*2)+horizontalIndex);
                                let productInfo = productArray[index];
                                print(productInfo["product_id"]);
                                let productview = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "rootPageViewController") as! cedMageProductViewRoot;
                                productview.pageData = productArray;
                                let instance = cedMage.singletonInstance;
                                instance.storeParameterInteger(parameter: index);
                                self.navigationController?.pushViewController(productview
                                    , animated: true);
                            }
                        }
                    }
                }
            }
        }*/
        
    }
    
    @objc func filterButtonPressed(_ sender:UIButton){
        print("filterButtonPressed");
        
        if(self.searchString != ""){
            let msg = "No Filters Found".localized;
            self.view.makeToast(msg, duration: 2.0, position: .bottom, title: nil, image: nil, style: nil, completion: nil);
            return;
        }
        
        let storyboard = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil);
        let viewController = storyboard.instantiateViewController(withIdentifier: "productFiltersViewController") as! ProductFiltersViewController;
        viewController.jsonForFilters = self.jsonResponse;
        self.navigationController?.pushViewController(viewController, animated: true);
    }
    
    
    override func basicFoundationToRenderView(bottomMargin:CGFloat){
        
        self.productListingTopSectionHeight.constant = translateAccordingToDevice(CGFloat(40.0));
        self.productListingTopSectionView.filterButton.addTarget(self, action: #selector(ProductListingViewController.filterButtonPressed(_:)), for: UIControl.Event.touchUpInside);
        //self.productListingTopSectionView.sortbyButton.addTarget(self, action: #selector(ProductListingViewController.sortbyButtonPressed(_:)), for: UIControlEvents.touchUpInside);

                
        // adding scrollview
        scrollView = UIScrollView();
        scrollView.translatesAutoresizingMaskIntoConstraints = false;
        scrollView.showsHorizontalScrollIndicator = false;
        scrollView.showsVerticalScrollIndicator = false;
        view.addSubview(scrollView);
        self.view.addConstraint(NSLayoutConstraint(item: scrollView, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 0));
        self.view.addConstraint(NSLayoutConstraint(item: scrollView, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 0));
        self.view.addConstraint(NSLayoutConstraint(item: scrollView, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.productListingTopSectionView, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: 0));
        self.view.addConstraint(NSLayoutConstraint(item: scrollView, attribute: NSLayoutConstraint.Attribute.bottom, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.bottomLayoutGuide, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: -(bottomMargin)));
        
        // adding stackview
        stackView = UIStackView();
        stackView.translatesAutoresizingMaskIntoConstraints = false;
        stackView.axis  = NSLayoutConstraint.Axis.vertical;
        stackView.distribution  = UIStackView.Distribution.equalSpacing;
        stackView.alignment = UIStackView.Alignment.center;
        stackView.spacing   = 10.0;
        scrollView.addSubview(stackView);
        self.view.addConstraint(NSLayoutConstraint(item: stackView, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 0));
        self.view.addConstraint(NSLayoutConstraint(item: stackView, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 0));
        scrollView.addConstraint(NSLayoutConstraint(item: stackView, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: scrollView, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1, constant: 0));
        scrollView.addConstraint(NSLayoutConstraint(item: stackView, attribute: NSLayoutConstraint.Attribute.bottom, relatedBy: NSLayoutConstraint.Relation.equal, toItem: scrollView, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: 0));
        
    }
    
    
    func clearViewAndVariables(){
        stackView.subviews.forEach({ $0.removeFromSuperview() });
        productArray = [[String:String]]();
        sortByArray = [String:String]();
        currentPageNum = 1;
        loadMoreData = true;
    }
    
    override func makeRequestToAPI(_ urlToRequest:String, dataToPost:[String:String]?){
        var urlToRequest = urlToRequest
        
        if(defaults.object(forKey: "userInfoDict") != nil){
            userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
        }
        
        let requestHeader = Settings.headerKey;
        let baseURL = Settings.baseUrl;
        
        urlToRequest = baseURL+urlToRequest;
        
        var postString = "";
        
        if defaults.bool(forKey: "isLogin") {
            //postString += "hashkey="+userInfoDict["hashKey"]!+"&customer_id="+userInfoDict["customerId"]!;
            var postData = [String:String]()
            postData["hashkey"] = userInfoDict["hashKey"]
            postData["customer_id"] = userInfoDict["customerId"]
            if let data = dataToPost{
                for (key,val) in data{
                    postData[key] = val;
                }
                postString = ["parameters":postData].convtToJson() as String
            }
            
        }
        else{
            var postData = [String:String]()
            if let data = dataToPost{
                for (key,val) in data{
                    postData[key] = val;
                }
                postString = ["parameters":postData].convtToJson() as String
            }
            
        }
        
        if(defaults.object(forKey: "filtersToSend") != nil){
            var filtersToSend = defaults.object(forKey: "filtersToSend") as! String;
            filtersToSend = filtersToSend.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlHostAllowed)!;
            defaults.removeObject(forKey: "filtersToSend");
            postString += "&multi_filter="+filtersToSend;
        }
        
        
        /*
 if(self.searchString != ""){
 postString += "&query="+searchString;
 }
 */
        print(urlToRequest);
        print(postString);
        
        var request = URLRequest(url: URL(string: "\(urlToRequest)")!);
        request.httpMethod = "POST";
        request.httpBody = postString.data(using: String.Encoding.utf8);
        if UserDefaults.standard.bool(forKey: "isLogin"){
            if let user = UserDefaults.standard.object(forKey: "userInfoDict") as? [String:String] {
                request.setValue(user["hashKey"], forHTTPHeaderField: "hashkey")
            }
            
        }
        
        cedMageLoaders.addDefaultLoader(me: self);
        let task = URLSession.shared.dataTask(with: request){
            
            // check for fundamental networking error
            data, response, error in
            guard error == nil && data != nil else{
                print("error=\(error)")
                DispatchQueue.main.async{
                    print(error?.localizedDescription as Any);
                    cedMageLoaders.removeLoadingIndicator(me: self);
                    self.makeRequestToAPI(urlToRequest, dataToPost:dataToPost);
                }
                return;
            }
            
            // check for http errors
            if let httpStatus = response as? HTTPURLResponse , httpStatus.statusCode != 200{
                
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(response)")
                DispatchQueue.main.async{
                    cedMageLoaders.removeLoadingIndicator(me: self);
                    
                }
                return;
            }
            
            // code to fetch values from response :: start
            
            let datastring = NSString(data: data!, encoding: String.Encoding.utf8.rawValue);
            print("datastring");
            print(datastring as Any);
            if(datastring == "NO_PRODUCTS"){
                DispatchQueue.main.async{
                    cedMageLoaders.removeLoadingIndicator(me: self);
                    if(self.productArray.count == 0){
                        //self.renderNoDataImage(imageName:"noProduct");
                        return;
                    }
                    else{
                        self.loadMoreData = false;
                    }
                }
                return;
            }
            
            guard let jsonResponse = try? JSON(data: data!) else{return;}
            if(jsonResponse != nil){
                DispatchQueue.main.async{
                    //print(jsonResponse);
                    cedMageLoaders.removeLoadingIndicator(me: self);
                    self.parseResponseReceivedFromAPI(jsonResponse);
                }
            }
        }
        task.resume();
    }
    
    func animateProductListingSection() {
        /*let cells = stackView.subviews
        let tableHeight: CGFloat = stackView.frame.height
        for i in cells {
            if let cell = i as? ProductListView{
                cell.transform = CGAffineTransform(translationX: 0, y: tableHeight)
            }
            if let cell = i as? ProductBigView{
                cell.transform = CGAffineTransform(translationX: 0, y: tableHeight)
            }
            if let cell = i as? UIStackView{
                cell.transform = CGAffineTransform(translationX: 0, y: tableHeight)
            }
        }
        var index = 0
        for a in cells {
            if let cell = a as? ProductListView{
                UIView.animate(withDuration: 1.5, delay: 0.05 * Double(index), usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: [], animations: {
                        cell.transform = CGAffineTransform(translationX: 0, y: 0);
                    }, completion: nil
                );
                index += 1;
            }
            if let cell = a as? ProductBigView{
                UIView.animate(withDuration: 1.5, delay: 0.05 * Double(index), usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: [], animations: {
                    cell.transform = CGAffineTransform(translationX: 0, y: 0);
                    }, completion: nil
                );
                index += 1;
            }
            if let cell = a as? UIStackView{
                UIView.animate(withDuration: 1.5, delay: 0.05 * Double(index), usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: [], animations: {
                    cell.transform = CGAffineTransform(translationX: 0, y: 0);
                    }, completion: nil
                );
                index += 1;
            }
        }*/
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
