/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */

import UIKit

class AddressListingViewController: MagenativeUIViewController {
    
    var isAddressPickMode = false;
    
    var addressDict = [String:[String: String]]();
    var floatingButton : UIButton!;
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.basicFoundationToRenderView(bottomMargin: CGFloat(0.0));
        
        self.makeRequestToAPI("mobiconnect/customer/address/",dataToPost: [:]);
        
        NotificationCenter.default.addObserver(self, selector: #selector(newAddressAdded(_:)), name: NSNotification.Name(rawValue: "newAddressAddedId"), object: nil);
    }
    
    @objc func newAddressAdded(_ notification: NSNotification){
        self.resetViewAndVariables();
        self.makeRequestToAPI("mobiconnect/customer/address/",dataToPost: [:]);
    }
    
    override func parseResponseReceivedFromAPI(_ jsonResponse:JSON){
        print("addressbook--------\(jsonResponse)");
        let jsonResponse = jsonResponse[0]
        if(jsonResponse["data"]["status"].stringValue == "success"){
           
            let jsonAddress = jsonResponse["data"]["address"];
            for c in 0..<jsonAddress.count{
                let firstname = jsonAddress[c]["firstname"].stringValue;
                let lastname = jsonAddress[c]["lastname"].stringValue;
                let street = jsonAddress[c]["street"].stringValue;
                let city = jsonAddress[c]["city"].stringValue;
                let region = jsonAddress[c]["region"].stringValue;
                let country = jsonAddress[c]["country"].stringValue;
                let pincode = jsonAddress[c]["pincode"].stringValue;
                let phone = jsonAddress[c]["phone"].stringValue;
                let address_id = jsonAddress[c]["address_id"].stringValue;
                let prefix = jsonAddress[c]["prefix"].stringValue;
                let suffix = jsonAddress[c]["suffix"].stringValue;
                let middlename = jsonAddress[c]["middlename"].stringValue;
                let taxvat = jsonAddress[c]["company"].stringValue;
                self.addressDict[address_id] = (["prefix":prefix,"suffix":suffix,"firstname":firstname,"middlename":middlename,"lastname":lastname,"street":street,"city":city,"region":region,"country":country,"pincode":pincode,"phone":phone,"address_id":address_id,"company":taxvat]);
               
            }
            self.renderAddressListingPage();
            self.renderFloatingButton();
        }
        else{
            self.renderNoDataImage(view: self, imageName: "noAddress")
            self.renderFloatingButton();
            return;
        }
    }
    
    func renderAddressListingPage(){
        
        self.makeSomeSpaceInStackView(height: CGFloat(0.0));
        
        let topLabelOnView = UILabel();
        topLabelOnView.translatesAutoresizingMaskIntoConstraints = false;
        topLabelOnView.text = "My Address Book".localized;
        topLabelOnView.setThemeColor();
        topLabelOnView.textColor = UIColor.white;
        topLabelOnView.textAlignment = NSTextAlignment.center;
        topLabelOnView.font = UIFont(fontName: "HelveticaNeue-Medium", fontSize: CGFloat(18.0));
        stackView.addArrangedSubview(topLabelOnView);
        let topLabelOnViewHeight = translateAccordingToDevice(CGFloat(40.0));
        topLabelOnView.heightAnchor.constraint(equalToConstant: topLabelOnViewHeight).isActive = true;
        self.setLeadingAndTralingSpaceFormParentView(topLabelOnView, parentView:stackView);
        
        var previousSelectedAddressId = "";
        if(self.defaults.object(forKey: "selectedAddressId") != nil){
            previousSelectedAddressId = self.defaults.object(forKey: "selectedAddressId") as! String;
        }
        
        for (key,val) in addressDict {
            
            let addressListView = AddressListView();
            if(isAddressPickMode){
                addressListView.pickAddressButton.isHidden = false;
                addressListView.pickAddressButton.tag = Int(key)!;
                addressListView.pickAddressButton.addTarget(self, action: #selector(pickAddress(_:)), for: UIControl.Event.touchUpInside);
                if(key == previousSelectedAddressId){
                    addressListView.pickAddressButton.setImage(UIImage(named:"CheckedCheckbox"), for: UIControl.State.normal);
                }
            }
            addressListView.translatesAutoresizingMaskIntoConstraints = false;
            
            var addressDescription = val["firstname"]!+" "+val["lastname"]!;
            addressDescription += "\n\n";
            addressDescription += val["street"]!+",\n"+val["city"]!+",\n"+val["region"]!+",\n"+val["country"]!+"-"+val["pincode"]!+"\n\n"+val["phone"]!;
            
            addressListView.addressDescription.text = addressDescription;
            let fontToApply = UIFont(fontName: "", fontSize: CGFloat(13.0))!;
            addressListView.addressDescription.font = fontToApply;
            let textAttributes = [NSAttributedString.Key.font: fontToApply];
            let rect = addressDescription.boundingRect(with: CGSize(width: screenSize.width, height: CGFloat.greatestFiniteMagnitude), options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: textAttributes, context: nil);
            
            addressListView.editAddressButton.tag = Int(key)!;
            addressListView.editAddressButton.addTarget(self, action: #selector(editAddress(_:)), for: UIControl.Event.touchUpInside);
            addressListView.deleteAddressButton.tag = Int(key)!;
            addressListView.deleteAddressButton.addTarget(self, action: #selector(deleteAddress(_:)), for: UIControl.Event.touchUpInside);
            
            addressListView.heightOfButtonSection.constant = translateAccordingToDevice(CGFloat(50.0));
            stackView.addArrangedSubview(addressListView);
            
            if(isAddressPickMode){
                addressListView.heightOfButtonSection.constant = 0;
                addressListView.buttonSection.removeFromSuperview();
                let addressListViewHeight = translateAccordingToDevice(CGFloat(rect.height))+20;
                addressListView.heightAnchor.constraint(equalToConstant: addressListViewHeight).isActive = true;
            }
            else{
                let addressListViewHeight = translateAccordingToDevice(CGFloat(50.0))+translateAccordingToDevice(CGFloat(rect.height))+20;
                addressListView.heightAnchor.constraint(equalToConstant: addressListViewHeight).isActive = true;
            }
            
            
            self.setLeadingAndTralingSpaceFormParentView(addressListView, parentView:stackView);
        }
        
        self.makeSomeSpaceInStackView(height: CGFloat(0.0));
        
    }
    
    func renderFloatingButton(){
        floatingButton = UIButton();
        floatingButton.translatesAutoresizingMaskIntoConstraints = false;
        floatingButton.setThemeColor();
        self.view.addSubview(floatingButton);
        floatingButton.setImage(UIImage(named: "AddFileWhite"), for: UIControl.State.normal);
        floatingButton.addTarget(self, action: #selector(addNewAddress(_:)), for: UIControl.Event.touchUpInside);
        floatingButton.imageEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10);
        self.view.addConstraint(NSLayoutConstraint(item: floatingButton, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: translateAccordingToDevice(50)));
        self.view.addConstraint(NSLayoutConstraint(item: floatingButton, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: translateAccordingToDevice(50)));
        self.view.addConstraint(NSLayoutConstraint(item: floatingButton, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: -10));
        self.view.addConstraint(NSLayoutConstraint(item: floatingButton, attribute: NSLayoutConstraint.Attribute.bottom, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: -(translateAccordingToDevice(50)+40)));
        floatingButton.makeViewCircled(size: translateAccordingToDevice(50));
        /*if(isAddressPickMode){
         floatingButton.isHidden = true;
         }*/
    }
    
    @objc func editAddress(_ sender:UIButton){
        let storyboard = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil);
        let viewController = storyboard.instantiateViewController(withIdentifier: "addUpdateAddressViewController") as! AddUpdateAddressViewController;
        let dataToUse = addressDict[String(sender.tag)]!;
        print(dataToUse);
        let addressInfoArray = ["email":"Email","prefix":dataToUse["prefix"], "firstname":dataToUse["firstname"]!,"middlename":dataToUse["middlename"], "lastname":dataToUse["lastname"]!,"suffix":dataToUse["suffix"], "country":dataToUse["country"]!, "state":dataToUse["region"]!, "street":dataToUse["street"]!, "city":dataToUse["city"]!, "pincode":dataToUse["pincode"]!, "mobile":dataToUse["phone"]!,"company":dataToUse["company"]];
        viewController.isEditingAddress = true;
        viewController.address_id = String(sender.tag);
        viewController.addressInfoArray = addressInfoArray as! [String : String];
        self.navigationController?.pushViewController(viewController, animated: true);
    }
    
    @objc func pickAddress(_ sender:UIButton){
        
        sender.setImage(UIImage(named :"CheckedCheckbox"), for: UIControl.State.normal);
        var addressDescription = "";
        if let addressListView = sender.superview?.superview as? AddressListView{
            addressDescription = addressListView.addressDescription.text!;
        }
        
        let selectedAddress = ["id": "\(sender.tag)", "address":addressDescription]
        print(selectedAddress)
        UserDefaults.standard.set(selectedAddress, forKey: "selectedAddress")
        self.defaults.set(addressDescription, forKey: "selectedAddressDescription");
        self.defaults.set(String(sender.tag), forKey: "selectedAddressId");
        let msg = "Address Selected".localized;
        self.view.makeToast(msg, duration: 2.0, position: .bottom, title: nil, image: nil, style: nil, completion: nil);
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1)) {
            //put your code which should be executed with a delay here
            self.navigationController?.popViewController(animated: true);
            
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "addressSelectedOrChangedId"), object: nil)
        }
    }
    
    @objc func addNewAddress(_ sender:UIButton){
        let storyboard = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil);
        let viewController = storyboard.instantiateViewController(withIdentifier: "addUpdateAddressViewController") as! AddUpdateAddressViewController;
        self.navigationController?.pushViewController(viewController, animated: true);
    }
    @objc func deleteAddress(_ sender:UIButton){
        let showTitle = "Confirmation".localized;
        let showMsg = "Operation can't be undone!".localized;
        
        let confirmationAlert = UIAlertController(title: showTitle, message: showMsg, preferredStyle: UIAlertController.Style.alert);
        
        confirmationAlert.addAction(UIAlertAction(title: "Done".localized, style: .default, handler: { (action: UIAlertAction!) in
            self.performDeleteItemFunction(sender: sender);
        }));
        
        confirmationAlert.addAction(UIAlertAction(title: "Cancel".localized, style: .default, handler: { (action: UIAlertAction!) in
        }));
        confirmationAlert.modalPresentationStyle = .fullScreen;
        present(confirmationAlert, animated: true, completion: nil)
    }
    func performDeleteItemFunction(sender:UIButton){
        
        var urlToRequest = "mobiconnect/customer/deleteaddress/";
        _ = cedMage.getInfoPlist(fileName:"cedMage",indexString: "requestheader") as! String
        let baseURL = Settings.baseUrl//cedMage.getInfoPlist(fileName:"cedMage",indexString: "cedBaseUrl") as! String;
        urlToRequest = baseURL+urlToRequest;
        
        
        var postString = "";
        var postData = [String:String]()
            postData["hashkey"] = userInfoDict["hashKey"]
            postData["customer_id"] = userInfoDict["customerId"]
            postData["address_id"] = String(sender.tag)
        postString = ["parameters":postData].convtToJson() as String
        
        /*var postString = "";
        postString += "hashkey="+userInfoDict["hashKey"]!+"&customer_id="+userInfoDict["customerId"]!;
        postString += "&address_id="+String(sender.tag);
        */
        var request = URLRequest(url: URL(string: "\(urlToRequest)")!);
        request.httpMethod = "POST";
        request.httpBody = postString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue));
        //request.setValue(requestHeader, forHTTPHeaderField: "Mobiconnectheader")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        if UserDefaults.standard.bool(forKey: "isLogin"){
            if let user = UserDefaults.standard.object(forKey: "userInfoDict") as? [String:String] {
                request.setValue(user["hashKey"], forHTTPHeaderField: "hashkey")
            }
            
        }
        cedMageLoaders.addDefaultLoader(me: self);
        let task = URLSession.shared.dataTask(with: request){
            
            // check for fundamental networking error
            data, response, error in
            guard error == nil && data != nil else{
                print("error=\(String(describing: error))")
                DispatchQueue.main.async{
                    print(error?.localizedDescription as Any);
                    cedMageLoaders.removeLoadingIndicator(me: self);
                }
                return;
            }
            
            // check for http errors
            if let httpStatus = response as? HTTPURLResponse , httpStatus.statusCode != 200{
                
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(String(describing: response))")
                DispatchQueue.main.async{
                    cedMageLoaders.removeLoadingIndicator(me: self);
                    
                }
                return;
            }
            
            // code to fetch values from response :: start
            guard let jsonResponse = try? JSON(data: data!) else{return;}
            if(jsonResponse != nil){
                
                DispatchQueue.main.async{
                    print(jsonResponse);
                    cedMageLoaders.removeLoadingIndicator(me: self);
                    _ = jsonResponse[0]["data"]["customer"][0]["message"].string;
                    if(jsonResponse[0]["data"]["customer"][0]["status"].stringValue == "success"){
                       
                        self.resetViewAndVariables();
                        self.makeRequestToAPI("mobiconnect/customer/address/",dataToPost: [:]);
                    }
                    else{
                       
                        return;
                    }
                }
            }
        }
        task.resume();
    }
    
    func renderNoDataImage(imageName:String){
        let noDataImageView = UIImageView();
        noDataImageView.translatesAutoresizingMaskIntoConstraints = false;
        noDataImageView.image = UIImage(named: imageName);
        noDataImageView.contentMode = UIView.ContentMode.scaleAspectFit;
        self.view.addSubview(noDataImageView);
        self.view.addConstraint(NSLayoutConstraint(item: noDataImageView, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 0));
        self.view.addConstraint(NSLayoutConstraint(item: noDataImageView, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 0));
        self.view.addConstraint(NSLayoutConstraint(item: noDataImageView, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1, constant: 0));
        self.view.addConstraint(NSLayoutConstraint(item: noDataImageView, attribute: NSLayoutConstraint.Attribute.bottom, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: 0));
        
    }
    
    func resetViewAndVariables(){
        floatingButton.removeFromSuperview();
        stackView.subviews.forEach({ $0.removeFromSuperview() });
        addressDict = [String:[String: String]]();
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
