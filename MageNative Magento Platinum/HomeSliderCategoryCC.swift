//
//  HomeSliderCategoryCC.swift
//  MageNative Magento Platinum
//
//  Created by Hamza Usmani on 02/09/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class HomeSliderCategoryCC: UICollectionViewCell {
    
    //MARK:- Properties
    
    static var reuseId:String = "HomeCategorySliderCC"
    var category: HomeCategory? = nil
    
    lazy var categoryImage:UIImageView = {
        let imageView = UIImageView()
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleToFill
        imageView.backgroundColor = .mageSecondarySystemBackground
        return imageView
    }()
    
    lazy var categoryName:UILabel = {
        let label = UILabel()
        label.textColor = .mageLabel
        label.textAlignment = .center
        label.text = "Some Category Name"
        label.font = .systemFont(ofSize: 11, weight: .semibold)
        label.numberOfLines = 3
        return label
    }()
    
    //MARK:- Lifecycle
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(categoryImage)
        categoryImage.anchor(top: topAnchor, paddingTop: 3, width: 60, height: 60)
        categoryImage.centerX(inView: self)
        
        addSubview(categoryName)
        categoryName.anchor(top: categoryImage.bottomAnchor, left: leadingAnchor, bottom: bottomAnchor, right: trailingAnchor,
                            paddingTop: 0, paddingLeft: 3, paddingBottom: 3, paddingRight: 3)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    
    func populate(with data: HomeCategory, type: String?) {
        self.category = data
        categoryName.text = category?.name ?? ""
        categoryImage.sd_setImage(with: URL(string: category?.image ?? ""), placeholderImage: UIImage(named: "placeholder"))
        
        switch type {
        case "square_slider":
            categoryImage.layer.cornerRadius = 8.0
        case "circular_slider":
            categoryImage.layer.cornerRadius = 30.0
        default:
            categoryImage.layer.cornerRadius = 8.0
        }
    }
    
    
    
    
    
    
}
