//
//  enc.m
//  Assignment
//
//  Created by Manohar Singh Rawat on 17/02/21.
//  Copyright © 2021 ab. All rights reserved.
//

#import "enc.h"

@implementation NSData (PBEEncryption)

- (NSData *)encryptPBEWithMD5AndDESUsingPassword:(NSData *)password {
    unsigned char gSalt[] =
    {

        (unsigned char)0x18, (unsigned char)0x79, (unsigned char)0x6D, (unsigned char)0x6D,
        (unsigned char)0x35, (unsigned char)0x3A, (unsigned char)0x6A, (unsigned char)0x60,
        (unsigned char)0x00
    };
    
    //        (unsigned char)0xde, (unsigned char)0x33, (unsigned char)0x10, (unsigned char)0x12,
    //        (unsigned char)0xde, (unsigned char)0x33, (unsigned char)0x10, (unsigned char)0x12,
    //        (unsigned char)0x00
    

    NSData *salt = nil;
    
    salt = [NSData dataWithBytes:gSalt length:strlen(gSalt)];
    //printf(gSalt);
    NSData* encrypted = [self encryptPBEWithMD5AndDESUsingPassword:password salt:salt iterations:20];
    
    NSMutableData* result = [NSMutableData dataWithData:salt];
    [result appendData:encrypted];
    return [NSData dataWithData:result];

}


- (NSData *)encryptPBEWithMD5AndDESUsingPassword:(NSData *)password salt:(NSData *)salt iterations:(NSUInteger)iterations {
    unsigned char md5[CC_MD5_DIGEST_LENGTH] = {};

    CC_MD5_CTX ctx;
    
    
    CC_MD5_Init(&ctx);
    CC_MD5_Update(&ctx, [password bytes], [password length]);
    CC_MD5_Update(&ctx, [salt bytes], [salt length]);
    CC_MD5_Final(md5, &ctx);

    for (NSUInteger i = 1; i < iterations; i++) {
        CC_MD5(md5, CC_MD5_DIGEST_LENGTH, md5);
    }

    // initialization vector is the second half of the MD5 from building the key
    unsigned char iv[kCCBlockSizeDES];
    assert(kCCBlockSizeDES == CC_MD5_DIGEST_LENGTH / 2);
    memcpy(iv, md5 + kCCBlockSizeDES, sizeof(iv));

    NSMutableData *output = [NSMutableData dataWithLength:([self length] + kCCBlockSize3DES)];
    size_t outputLength = 0;
    CCCryptorStatus status =
    CCCrypt(kCCEncrypt, kCCAlgorithmDES, kCCOptionPKCS7Padding, md5, kCCBlockSizeDES, iv,
            [self bytes], [self length], [output mutableBytes], [output length], &outputLength);

    if (status == kCCSuccess) { [output setLength:outputLength]; }
    else { output = nil; }

    return output;
}

-(NSData*) cryptPBEWithMD5AndDES:(CCOperation)op usingData:(NSData*)data withPassword:(NSString*)password andSalt:(NSData*)salt andIterating:(int)numIterations {
    unsigned char md5[CC_MD5_DIGEST_LENGTH];
    memset(md5, 0, CC_MD5_DIGEST_LENGTH);
    NSData* passwordData = [password dataUsingEncoding:NSUTF8StringEncoding];

    CC_MD5_CTX ctx;
    CC_MD5_Init(&ctx);
    CC_MD5_Update(&ctx, [passwordData bytes], [passwordData length]);
    CC_MD5_Update(&ctx, [salt bytes], [salt length]);
    CC_MD5_Final(md5, &ctx);

    for (int i=1; i<numIterations; i++) {
        CC_MD5(md5, CC_MD5_DIGEST_LENGTH, md5);
    }

    size_t cryptoResultDataBufferSize = [data length] + kCCBlockSizeDES;
    unsigned char cryptoResultDataBuffer[cryptoResultDataBufferSize];
    size_t dataMoved = 0;

    unsigned char iv[kCCBlockSizeDES];
    memcpy(iv, md5 + (CC_MD5_DIGEST_LENGTH/2), sizeof(iv)); //iv is the second half of the MD5 from building the key

    CCCryptorStatus status =
        CCCrypt(op, kCCAlgorithmDES, kCCOptionPKCS7Padding, md5, (CC_MD5_DIGEST_LENGTH/2), iv, [data bytes], [data length],
            cryptoResultDataBuffer, cryptoResultDataBufferSize, &dataMoved);

    if(0 == status) {
        return [NSData dataWithBytes:cryptoResultDataBuffer length:dataMoved];
    } else {
        return NULL;
    }
}

@end
