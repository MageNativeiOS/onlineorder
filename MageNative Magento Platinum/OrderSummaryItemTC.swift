//
//  OrderSummaryItemTC.swift
//  MageNative Magento Platinum
//
//  Created by Hamza Usmani on 04/09/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class OrderSummaryItemTC: UITableViewCell {
    
    //MARK:- Properies
    
    static var reuseID:String = "OrderSummaryItemTC"
    
    lazy var itemCountLabel:UILabel = {
           let label = UILabel()
           label.textAlignment = .center
           label.font = UIFont.systemFont(ofSize: 15, weight: .semibold)
           return label
       }()
       
    lazy var container:UIView = {
        let view = UIView()
        view.layer.cornerRadius = 5.0
        view.backgroundColor = .mageSystemBackground
        return view
    }()
    
    lazy var itemImage:UIImageView = {
        let imageView = UIImageView()
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleToFill
        imageView.image = UIImage(named: "placeholder")
        imageView.backgroundColor = .mageSecondarySystemBackground
        return imageView
    }()
    
    lazy var itemName:UILabel = {
        let label = UILabel()
        label.textColor = .mageLabel
        label.text = "Item name goes here"
        label.font = .systemFont(ofSize: 16, weight: .medium)
        return label
    }()
    
    lazy var verticalStack:UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.spacing = 2.0
        return stack
    }()
    
    //MARK:- Lifecycle
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        backgroundColor = .clear
        addSubview(itemCountLabel)
               itemCountLabel.anchor(top: topAnchor, left: leadingAnchor, right: trailingAnchor, paddingTop: 5, paddingLeft: 8, paddingRight: 8)
              
        addSubview(container)
        container.anchor(top: itemCountLabel.bottomAnchor, left: leadingAnchor, bottom: bottomAnchor, right: trailingAnchor, paddingTop: 8, paddingLeft: 8, paddingBottom: 2, paddingRight: 8)
        
        container.addSubview(itemImage)
        itemImage.anchor(top: container.topAnchor, left: container.leadingAnchor, bottom: container.bottomAnchor,
                         paddingTop: 2, paddingLeft: 2, paddingBottom: 2, width: 120)
        
        container.addSubview(itemName)
        itemName.anchor(top: container.topAnchor, left: itemImage.trailingAnchor, paddingTop: 8, paddingLeft: 8)
        
        container.addSubview(verticalStack)
        verticalStack.anchor(top: itemName.bottomAnchor, left: itemImage.trailingAnchor, paddingTop: 5, paddingLeft: 8)//right: container.trailingAnchor, paddingRight: 16
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK:- Helper methods
    
    func populate(with item: OrderSummaryItem, totalItems: Int) {
        verticalStack.subviews.forEach({ $0.removeFromSuperview() })
        itemCountLabel.text = "\(totalItems) items in your cart"
        
        
        itemImage.sd_setImage(with: URL(string: item.product_image), placeholderImage: UIImage(named: "placeholder"))
        itemName.text = item.product_name
        
        createValue(for: "Subtotal", value: item.sub_total)
        createValue(for: "Quantity", value: item.quantity)
        
        for option in item.options_selected {
            createValue(for: option.label, value: option.value)
        }
    }
    
    func createValue(for heading: String, value : String) {
        let label = UILabel()
        label.text = heading
        label.font = .systemFont(ofSize: 12, weight: .medium)
        
        let labelTwo = UILabel()
        labelTwo.text = value
        labelTwo.font = .systemFont(ofSize: 12, weight: .regular)
        labelTwo.textColor = .mageSecondaryLabel
        
        let stack = UIStackView(arrangedSubviews: [label,labelTwo])
        stack.axis = .horizontal
        stack.distribution = .fillEqually
        stack.spacing = 8.0
        stack.anchor(height: 15)
        
        verticalStack.addArrangedSubview(stack)
    }
    
    
}
