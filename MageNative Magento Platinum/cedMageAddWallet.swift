//
//  cedMageAddWallet.swift
//  MageNative Magento Platinum
//
//  Created by Macmini on 12/12/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class cedMageAddWallet: cedMageViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var mainTable: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.isHidden = false
        
        mainTable.delegate = self;
        mainTable.dataSource = self;
        mainTable.separatorStyle = .none
       // self.navigationItem.title = "LOCPAY"
        // Do any additional setup after loading the view.
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row{
            case 0:
                if let cell = tableView.dequeueReusableCell(withIdentifier: "headerCell") as? cedMageMyWalletCell
                {
                    return cell;
                }
            case 1:
                if let cell = tableView.dequeueReusableCell(withIdentifier: "amountCell") as? cedMageMyWalletCell
                {
                    cell.add50View.cardView()
                    cell.add100View.cardView();
                    cell.add200View.cardView();
                    cell.add300View.cardView();
                    cell.add500View.cardView();
                    cell.add1000View.cardView();
                    cell.amountTextField.tag = 5;
                    cell.amountTextField.layer.borderColor = UIColor.black.cgColor
                    cell.amountTextField.layer.borderWidth = 1;
                    cell.add50Button.tag = 50;
                    cell.add100Button.tag = 100;
                    cell.add200Button.tag = 200;
                    cell.add300Button.tag = 300;
                    cell.add500Button.tag = 500;
                    cell.add1000Button.tag = 1000;
                    
                    cell.add50Button.addTarget(self, action: #selector(moneyButtonsClicked(_:)), for: .touchUpInside);
                    cell.add100Button.addTarget(self, action: #selector(moneyButtonsClicked(_:)), for: .touchUpInside);
                    cell.add200Button.addTarget(self, action: #selector(moneyButtonsClicked(_:)), for: .touchUpInside);
                    cell.add300Button.addTarget(self, action: #selector(moneyButtonsClicked(_:)), for: .touchUpInside);
                    cell.add500Button.addTarget(self, action: #selector(moneyButtonsClicked(_:)), for: .touchUpInside);
                    cell.add1000Button.addTarget(self, action: #selector(moneyButtonsClicked(_:)), for: .touchUpInside);
                    cell.addMoneyButton.addTarget(self, action: #selector(addMoneyClicked(_:)), for: .touchUpInside);
                    cell.addMoneyButton.setThemeColor();
                    cell.addMoneyButton.layer.cornerRadius = 5
                    return cell;
                }
            case 2:
                if let cell = tableView.dequeueReusableCell(withIdentifier: "passbookCell") as? cedMageMyWalletCell
                {
                    cell.backgroundColor = UIColor.init(hexString: "#f2f2f2")
                    
                    return cell;
                }
            default:
                return UITableViewCell();
        }
        return UITableViewCell();
        
    }
    
    @objc func addMoneyClicked(_ sender: UIButton)
    {
        if let amount = (self.view.viewWithTag(5) as? UITextField)?.text {
            if amount != "" {
                let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
                let  customerId = userInfoDict["customerId"]!;
                let params = ["price":amount,"customer_id":customerId,"city":"agra","country_id":"India","state":"Uttar Pradesh"]
                self.sendRequest(url: "mobiconnect/mobiwallet/addmoney",params:params, store: false)
            }else{
                cedMageHttpException.showAlertView(me: self, msg: "Enter Amount To Add", title: "")
            }
        }else{
            cedMageHttpException.showAlertView(me: self, msg: "Enter Amount To Add", title: "")
        }
    }
    
    override func recieveResponse(data: Data?, requestUrl: String?, response: URLResponse?) {
        if let data = data {
            do {
            var json = try JSON(data:data)
            json = json[0]
            print(json)
            if json["cart_id"]["success"].stringValue == "true" {
                let storyboard = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil);
                let viewController = storyboard.instantiateViewController(withIdentifier: "cartViewController") as! CartViewController;
                self.navigationController?.pushViewController(viewController, animated: true)
            }else{
                let msg = json["cart_id"]["message"].stringValue
                cedMageHttpException.showAlertView(me: self, msg: msg, title: "")
            }
            }catch let error {
                print(error.localizedDescription)
            }
        }
    }
    
    @objc func moneyButtonsClicked(_ sender: UIButton)
    {
        if let amountTextField = self.view.viewWithTag(5) as? UITextField{
            amountTextField.text = "\(sender.tag)";
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 2{
            let viewcontroll = UIStoryboard(name: "cedMageAccounts", bundle: nil).instantiateViewController(withIdentifier: "cedMageMyWallet") as! cedMageMyWallet
            self.navigationController?.pushViewController(viewcontroll, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row{
            case 0:
                return 40;
            case 1:
                return 300;
            case 2:
                return 130;
        default:
            return 40;
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
