//
//  cedMageSignup.swift
//  MageNative Magento Platinum
//
//  Created by Manohar Singh Rawat on 25/01/21.
//  Copyright © 2021 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class cedMageSignup: MagenativeUIViewController {

    lazy var signupScrollView: UIScrollView = {
       let scroll = UIScrollView()
        scroll.translatesAutoresizingMaskIntoConstraints = false;
        return scroll;
    }()
    
    lazy var mainView: UIView = {
        let scroll = UIView()
        scroll.translatesAutoresizingMaskIntoConstraints = false;
        return scroll;
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    func loadData(){
        self.sendRequest(url: "mobiconnect/customer/getRequiredFields/", params: nil) 
    }
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
