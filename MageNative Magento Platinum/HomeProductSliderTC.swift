//
//  HomeProductSliderTC.swift
//  MageNative Magento Platinum
//
//  Created by Hamza Usmani on 02/09/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class HomeProductSliderTC: UITableViewCell {

    //MARK:- Properties
    
    static var reuseId:String = "HomeSliderProductTC"
    var products: [HomeProduct]? = nil
    var categoryId:String? = nil
    weak var parent: UIViewController?
    
    lazy var topHeading:UILabel = {
        let label = UILabel()
        label.textColor = UIColor.mageLabel
        label.text = "Trending Products"
        label.font = .systemFont(ofSize: 17, weight: .semibold)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    lazy var productCollection:UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 3
        layout.scrollDirection = .horizontal
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.backgroundColor = UIColor.clear
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.register(HomeProductSliderCC.self, forCellWithReuseIdentifier: HomeProductSliderCC.reuseId)
        collectionView.delegate = self
        collectionView.dataSource = self
        return collectionView
    }()
    
    lazy var viewAllButton:UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("View All", for: .normal)
        button.setTitleColor(UIColor.mageSystemBackground, for: .normal)
        button.backgroundColor = UIColor.mageLabel
        button.layer.cornerRadius = 5.0
        button.titleLabel?.font = UIFont.systemFont(ofSize: 15, weight: .bold)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    //MARK:- Lifecycle
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor         = .clear
        selectionStyle          = .none
        contentView.isMultipleTouchEnabled  = true
        
        addSubview(topHeading)
        topHeading.anchor(top: topAnchor, left: leadingAnchor, paddingTop: 2, paddingLeft: 8)
        
        addSubview(viewAllButton)
        viewAllButton.anchor(right: trailingAnchor, paddingRight: 8, width: 70, height: 25)
        viewAllButton.centerY(inView: topHeading)
        
        addSubview(productCollection)
        productCollection.anchor(top: topHeading.bottomAnchor, left: leadingAnchor, bottom: bottomAnchor, right: trailingAnchor,
                                 paddingTop: 10, paddingLeft: 8, paddingBottom: 8, paddingRight: 8)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func populate(with data: [HomeProduct],categoryId: String?, headingText: String, parent: UIViewController) {
        self.products   = data
        self.parent     = parent
        self.categoryId = categoryId
        topHeading.text = headingText
        productCollection.reloadData()
        viewAllButton.addTarget(self, action: #selector(viewAllTapped(_:)), for: .touchUpInside)
    }
    
    @objc func viewAllTapped(_ sender: UIButton) {
    }
    
    
}

extension HomeProductSliderTC: UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return products?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: HomeProductSliderCC.reuseId, for: indexPath) as! HomeProductSliderCC
        cell.populate(with: (products![indexPath.item]))
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return .init(width: 170, height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "productSinglePageViewController") as! ProductSinglePageViewController
        vc.product_id = products?[indexPath.item].product_id ?? ""
        parent?.navigationController?.pushViewController(vc, animated: true)
    }
}
