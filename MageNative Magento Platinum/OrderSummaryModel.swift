//
//  OrderSummaryModel.swift
//  MageNative Magento Platinum
//
//  Created by Hamza Usmani on 06/09/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import Foundation

struct OrderSummaryModel {
    let items_count:String
    let grandtotal:String
    let checkout_disable:String
    let allowed_guest_checkout:String
    let products:[OrderSummaryItem]
    let currency_symbol:String
    let items_qty:String
    let total:OrderSummaryTotal
    let segments : [summaryFields]
    let is_discount:String
    let currency_code:String
    
    init(json: JSON) {
        items_count = json[0]["data"]["items_count"].stringValue
        grandtotal = json[0]["data"]["grandtotal"].stringValue
        checkout_disable = json[0]["data"]["checkout_disable"].stringValue
        allowed_guest_checkout = json[0]["data"]["allowed_guest_checkout"].stringValue
        currency_symbol = json[0]["data"]["currency_symbol"].stringValue
        items_qty = json[0]["data"]["items_qty"].stringValue
        is_discount = json[0]["data"]["is_discount"].stringValue
        currency_code = json[0]["data"]["currency_code"].stringValue
        
        var prods = [OrderSummaryItem]()
        
        for product in json[0]["data"]["products"].arrayValue {
            print(product)
            let temp = OrderSummaryItem(json: product)
            prods.append(temp)
        }
        products = prods
        
        var vals = [summaryFields]()
        for values in json[0]["data"]["segments"].arrayValue {
            print(values)
            let temp = summaryFields(json: values)
            vals.append(temp)
        }
        segments = vals
        total = OrderSummaryTotal(json: json[0]["data"]["total"][0])
    }
    
}
struct summaryFields{
    let label : String
    let value : String
    
    init(json:JSON) {
        label = json["label"].stringValue
        value = json["value"].stringValue
    }
}
struct OrderSummaryTotal {
    let tax_amount:String
    let discount_amount:String
    let shipping_amount:String
    let amounttopay:String
    
    init(json: JSON) {
        tax_amount = json["tax_amount"].stringValue
        discount_amount = json["discount_amount"].stringValue
        shipping_amount = json["shipping_amount"].stringValue
        amounttopay = json["amounttopay"].stringValue
    }
}


struct OrderSummaryItem {
    let review_count:String
    let review:String
    let product_name:String
    let product_id:String
    let item_error:String
    let product_image:String
    let options_selected:[OrderSummaryConfigType]
    //let bundle_options:String
    let product_type:String
    let sub_total:String
    let item_id:String
    let quantity:String
    
    init(json: JSON) {
        review_count = json["review_count"].stringValue
        review = json["review"].stringValue
        product_name = json["product-name"].stringValue
        product_id = json["product_id"].stringValue
        item_error = json["item_error"].stringValue
        product_image = json["product_image"].stringValue
        product_type = json["product_type"].stringValue
        sub_total = json["sub-total"].stringValue
        item_id = json["item_id"].stringValue
        quantity = json["quantity"].stringValue
        
        var configOptions = [OrderSummaryConfigType]()
        for opt in json["options_selected"].arrayValue {
            let temp = OrderSummaryConfigType(json: opt)
            configOptions.append(temp)
        }
        options_selected = configOptions
    }
}

struct OrderSummaryConfigType {
    let label:String
    let option_value:String
    let option_id:String
    let value:String
    
    init(json: JSON) {
        label = json["label"].stringValue
        option_value = json["option_value"].stringValue
        option_id = json["option_id"].stringValue
        value = json["value"].stringValue
    }
}
