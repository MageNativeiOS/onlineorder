/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */


import UIKit
import SafariServices
class cedMageCmsWebView: cedMageViewController,WKNavigationDelegate, SFSafariViewControllerDelegate {
    
    @IBOutlet var webview:WKWebView?
    var pageUrl = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        webview?.navigationDelegate = self
        cedMageLoaders.addDefaultLoader(me: self)
      // pageUrl = "http://demo.cedcommerce.com/magento2/mobiadvance/mobiconnectcheckout/onepage/index/cart_id/29"
        loadPage()
        // Do any additional setup after loading the view.
        
    }
    func loadPage(){
        var request = URLRequest(url: URL(string: pageUrl)! )
        let requestHeader = Settings.headerKey
        request.setValue(requestHeader, forHTTPHeaderField: "Mobiconnectheader")
        webview?.load(request )
        
    }
    
     func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
          cedMageLoaders.removeLoadingIndicator(me: self)
          
      }
      
      func userContentController(_ userContentController:
          WKUserContentController,
                                 didReceive message: WKScriptMessage) {
          print(message.body)
          
      }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
