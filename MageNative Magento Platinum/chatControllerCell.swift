//
//  chatControllerCell.swift
//  MageNative Magento Platinum
//
//  Created by Macmini on 27/11/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class chatControllerCell: UITableViewCell {

    @IBOutlet weak var messageLabel: UILabel!
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var messageView: UIView!
    @IBOutlet weak var msgImage: UIButton!
    @IBOutlet weak var imageHeight: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        messageView.backgroundColor = UIColor.init(hexString: "#f2f2f2")
        messageView.layer.cornerRadius = 5
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
