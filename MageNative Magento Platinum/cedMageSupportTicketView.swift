//
//  cedMageSupportTicketView.swift
//  ZeoMarket
//
//  Created by cedcoss on 01/04/19.
//  Copyright © 2019 MageNative. All rights reserved.
//

import UIKit

class cedMageSupportTicketView: UITableViewCell {

    @IBOutlet weak var ticketId: UILabel!
    @IBOutlet weak var department: UILabel!
    @IBOutlet weak var action: UILabel!
    @IBOutlet weak var status: UILabel!
    @IBOutlet weak var subject: UILabel!
    @IBOutlet weak var createdTime: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
