/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */


import UIKit

extension cedMageDefaultCollection{
    
    func wishlistButtonPressed(_ sender:UIButton){
        print("wishlistButtonPressed");
        var urlToRequest = "";
        let indexToSubtract = 0;
        
        
        let currentIndex = sender.tag
        print(currentIndex);
        print(currentIndex);
        print(currentIndex);
        
        let productInfo = self.products[currentIndex];
        print(productInfo);
        
        if(productInfo["Inwishlist"] == "OUT"){
            urlToRequest = "mobiconnectwishlist/customer_wishlist/addtowishlist/";
        }
        else{
            urlToRequest = "mobiconnectwishlist/customer_wishlist/removeItem/";
        }
        
        if(defaults.object(forKey: "userInfoDict") != nil){
            userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
        }
        else{
            let msg = "Please Login First".localized;
            self.view.makeToast(msg, duration: 2.0, position: .bottom, title: nil, image: nil, style: nil, completion: nil);
            return;
        }
        
        
        let requestHeader = Settings.headerKey;
        let baseURL = Settings.baseUrl;
        
        urlToRequest = baseURL+urlToRequest;
        
        var postString = "";
        if(defaults.object(forKey: "userInfoDict") != nil){
            postString += "hashkey="+userInfoDict["hashKey"]!+"&customer_id="+userInfoDict["customerId"]!;
            postString += "&prodID="+productInfo["product_id"]!;
        }
        else{
            postString += "prodID="+productInfo["product_id"]!;
        }
        if(productInfo["wishlist-item-id"] != "-1"){
            postString += "&item_id="+productInfo["wishlist-item-id"]!;
        }
        
        print(urlToRequest);
        print(postString);
        
        var request = URLRequest(url: URL(string: "\(urlToRequest)")!);
        request.httpMethod = "POST";
        request.httpBody = postString.data(using: String.Encoding.utf8);
        if UserDefaults.standard.bool(forKey: "isLogin"){
            if let user = UserDefaults.standard.object(forKey: "userInfoDict") as? [String:String] {
                request.setValue(user["hashKey"], forHTTPHeaderField: "hashkey")
            }
            
        }
        
        cedMageLoaders.addDefaultLoader(me: self);
        let task = URLSession.shared.dataTask(with: request){
            
            // check for fundamental networking error
            data, response, error in
            guard error == nil && data != nil else{
                print("error=\(error)")
                DispatchQueue.main.async{
                    print(error?.localizedDescription as Any);
                    cedMageLoaders.removeLoadingIndicator(me: self);
                }
                return;
            }
            
            // check for http errors
            if let httpStatus = response as? HTTPURLResponse , httpStatus.statusCode != 200{
                
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(response)")
                DispatchQueue.main.async{
                    cedMageLoaders.removeLoadingIndicator(me: self);
                    
                }
                return;
            }
            
            // code to fetch values from response :: start
            
            let datastring = NSString(data: data!, encoding: String.Encoding.utf8.rawValue);
            print("datastring");
            print(datastring as Any);
            
            guard let jsonResponse = try? JSON(data: data!) else{return;}
            if(jsonResponse != nil){
                
                DispatchQueue.main.async{
                    
                    print(jsonResponse);
                    cedMageLoaders.removeLoadingIndicator(me: self);
                    
                    let msg = jsonResponse["message"].stringValue;
                    if(jsonResponse["status"].stringValue == "true"){
                        
                        if(productInfo["Inwishlist"] == "OUT"){
                            self.products[currentIndex-indexToSubtract]["Inwishlist"] = "IN";
                            sender.setImage(UIImage(named:"LikeFilled"), for: UIControl.State.normal);
                            self.products[currentIndex]["wishlist-item-id"] = jsonResponse["wishlist-item-id"].stringValue;
                        }
                        else{
                            self.products[currentIndex]["Inwishlist"] = "OUT";
                            sender.setImage(UIImage(named:"LikeEmpty"), for: UIControl.State.normal);
                        }
                    }
                    self.view.makeToast(msg, duration: 2.0, position: .bottom, title: nil, image: nil, style: nil, completion: nil);
                }
            }
            
        }
        task.resume();
        
    }
    
}
