//
//  adminInboxCell.swift
//  MageNative Magento Platinum
//
//  Created by Saumya Kashyap on 25/11/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class adminInboxCell: UITableViewCell {


    @IBOutlet weak var adminSenderLabel: UILabel!
    @IBOutlet weak var adminRecieverLabel: UILabel!
    @IBOutlet weak var adminCreatedByLabel: UILabel!
    @IBOutlet weak var adminUpdatedAtLabel: UILabel!
    @IBOutlet weak var adminSubjectLabel: UILabel!
    @IBOutlet weak var newMessageLabel: UILabel!
    
    @IBOutlet var cellCardView: UIView!

    @IBOutlet weak var adminViewBtn: UIButton!
  
    override func awakeFromNib() {
        super.awakeFromNib()
        adminViewBtn.layer.cornerRadius = 5
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
	
	
}
//extension UIView {
//
//
//          func addShadow(){
//            self.layer.cornerRadius = 20.0
//            self.layer.shadowColor = UIColor.gray.cgColor
//            self.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
//            self.layer.shadowRadius = 12.0
//            self.layer.shadowOpacity = 0.7
//
//         }
//}
//{
//	didSet {
//		// Make it card-like
//	layer.cornerRadius = 10
//	layer.shadowOpacity = 1
//	layer.shadowRadius = 2
//	layer.shadowColor = UIColor(named: "Orange")?.cgColor
//	layer.shadowOffset = CGSize(width: 3, height: 3)
//	backgroundColor = UIColor(named: "Red")
//	}
//}
