//
//  vendorInboxController.swift
//  MageNative Magento Platinum
//
//  Created by Saumya Kashyap on 25/11/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class vendorInboxController: MagenativeUIViewController {

	var vendorInboxList = [vendorInbox]()
	
    @IBOutlet weak var inboxHeadingLabel: UILabel!
  
    
    @IBOutlet weak var inboxTableView: UITableView!
    
    @IBOutlet weak var composeMessageBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       
		let storeId = UserDefaults.standard.value(forKey: "storeId") as! String
		let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
		let customerId = userInfoDict["customerId"]!;
		self.sendRequest(url: "mobimessaging/getinbox", params: ["customer_id":customerId,"store_id":storeId,"page":"1"])
        composeMessageBtn.setThemeColor()
        composeMessageBtn.layer.cornerRadius = 5
        
    }
    
    @IBAction func composeOnClick(_ sender: Any) {
         let viewController = UIStoryboard(name: "cedMageAccounts" ,bundle:  nil).instantiateViewController(withIdentifier: "vendorComposeMsgController") as! vendorComposeMsgController
        viewController.isFromAdmin = false
               self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    override func recieveResponse(data: Data?, requestUrl: String?, response: URLResponse?) {
		guard let data = data else{return}
        do {
            var json = try JSON(data: data)
            print(json)
            json = json[0]
            
            if json["response"]["status"].stringValue == "false" {
                self.view.makeToast(json["response"]["message"].stringValue, duration: 1.0, position: .center)
                inboxTableView.isHidden = true
                return
            }
            
            for list in json["response"]["inbox_list"].arrayValue {
                let temp = vendorInbox(sender_name: list["sender_name"].stringValue,
                                       sender: list["sender"].stringValue,
                                       id: list["id"].stringValue,
                                       vendor_id: list["vendor_id"].stringValue,
                                       created_at: list["created_at"].stringValue,
                                       subject: list["subject"].stringValue,
                                       message: list["message"].stringValue,
                                       new_message: list["new_message"].stringValue,
                                       updated_at: list["updated_at"].stringValue,
                                       receiver_name: list["receiver_name"].stringValue)
                
                vendorInboxList.append(temp)
                print(vendorInboxList)
            }
            inboxTableView.delegate = self
            inboxTableView.dataSource = self
            inboxTableView.reloadData()
            inboxTableView.isHidden = false
        } catch let error {
            print(error.localizedDescription)
        }
        
	}
}
extension vendorInboxController : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return vendorInboxList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = inboxTableView.dequeueReusableCell(withIdentifier: "vendorInboxTableCell", for: indexPath) as! vendorInboxTableCell
        cell.senderLabel.text = vendorInboxList[indexPath.row].sender
        cell.receiverLabel.text = vendorInboxList[indexPath.row].receiver_name
        cell.createdAtLabel.text = vendorInboxList[indexPath.row].created_at
        cell.updatedAtLabel.text = vendorInboxList[indexPath.row].updated_at
        cell.subjectLabel.text = vendorInboxList[indexPath.row].subject
        cell.newMessageLabel.text = vendorInboxList[indexPath.row].new_message
        cell.viewButton.setThemeColor()
        cell.viewButton.addTarget(self, action: #selector(viewButtontapped(_:)), for: .touchUpInside)
        cell.viewButton.tag = indexPath.row
        
        
    return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 260
    }
    
    @objc func viewButtontapped(_ sender: UIButton) {
       let viewController = UIStoryboard(name: "cedMageAccounts" ,bundle:  nil).instantiateViewController(withIdentifier: "chatViewController") as! chatViewController
        
        viewController.id = vendorInboxList[sender.tag].id
        viewController.vendorId = vendorInboxList[sender.tag].vendor_id
        viewController.isFromVendor = true
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}
