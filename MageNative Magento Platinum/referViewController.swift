//
//  referViewController.swift
//  MageNative Magento Platinum
//
//  Created by cedcoss on 19/03/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class referViewController: MagenativeUIViewController {

    @IBOutlet weak var referTable:        UITableView!
    var viewModel                       = referViewModel() { didSet { referTable.reloadData() } }
    
    
        override func viewDidLoad() {
        super.viewDidLoad()
        
            setUpReferController()
            let userDict                    = UserDefaults.standard.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]()
            let customerId                  = userDict["customerId"]
            viewModel.getDataFromApi(with: "mobireward/get/referalurl/\(customerId!)", for: self) { vm in self.viewModel = vm }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = false
    }
    func setUpReferController(){
        view.backgroundColor = Settings.themeColor
    }
    
    
        
}
