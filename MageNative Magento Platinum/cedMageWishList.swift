/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */


import UIKit

class cedMageWishList: cedMageViewController,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet weak var emptyWishList: UIButton!
    @IBOutlet weak var wishlistTableView: UITableView!
    @IBOutlet weak var emptywishlist: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    var item_count = 0;
    var status = "";
      var wishlistArray = [[String:String]]();
    override func viewDidLoad() {
        super.viewDidLoad()
        
        emptyWishList.fontColorTool()
        wishlistTableView.fontColorTool()
        emptywishlist.fontColorTool()
        titleLabel.fontColorTool()
        
        
        wishlistTableView.delegate = self
        wishlistTableView.dataSource = self
      
        let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
        if !userInfoDict.isEmpty{
            let hashKey = userInfoDict["hashKey"]!;
            let customerId = userInfoDict["customerId"]!;
            let postString = ["hashkey":hashKey,"customer_id":customerId]
            
            // Do any additional setup after loading the view.
            emptyWishList.addTarget(self, action: #selector(cedMageWishList.clearWishList(sender:)), for: UIControl.Event.touchUpInside)
            self.sendRequest(url: "mobiconnect/wishlist/getwishlist/", params: postString)
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @objc func clearWishList(sender:UIButton){
        let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
        let hashKey = userInfoDict["hashKey"]!;
        let customerId = userInfoDict["customerId"]!;
        let postString = ["hashkey":hashKey,"customer_id":customerId]
        self.sendRequest(url: "mobiconnect/wishlist/clear/", params: postString)
    }
    
    
   override func recieveResponse(data: Data?, requestUrl: String?, response: URLResponse?) {
    if(requestUrl == "mobiconnect/wishlist/getwishlist/"){
        guard let json = try? JSON(data: data!) else{return;}
        print(json)
        let wishlistProducts = json[0]["data"]["products"]
       //
        if(json[0]["data"]["status"] != "false"){
            wishlistArray.removeAll()
        for (value) in wishlistProducts
        {
            var temp = [String:String]();
            temp["wishlist_id"] = value.1["wishlist_id"].stringValue;
            temp["special_price"] = value.1["special_price"].stringValue;
            temp["product_image"] = value.1["product_image"].stringValue;
            temp["regular_price"] = value.1["regular_price"].stringValue;
            temp["product_id"] = value.1["product_id"].stringValue;
            temp["product_name"] = value.1["product_name"].stringValue;
            temp["qty"] = value.1["qty"].stringValue;
            temp["wishlist_item_id"] = value.1["wishlist_item_id"].stringValue;
            self.wishlistArray.append(temp);
        }
        }
        
        if(self.wishlistArray.count != 0 ){
            titleLabel.setThemeColor()
            self.titleLabel.text = json[0]["data"]["item_count"].stringValue + " Items In Your Wishlist".localized
            self.emptywishlist.isHidden = true
            self.wishlistTableView.isHidden = false
            self.titleLabel.isHidden = false
           self.wishlistTableView.reloadData()
        }else{
            self.wishlistTableView.isHidden = true
            self.titleLabel.isHidden = true
            self.emptywishlist.isHidden = false
             self.renderNoDataImage(view: self, imageName: "noWishlist")
        }
    }else if(requestUrl == "mobiconnect/wishlist/clear/"){
        guard let json = try? JSON(data: data!) else{return;}
        print(json)
        if(json["success"].stringValue == "true"){
            wishlistArray.removeAll()
              self.renderNoDataImage(view: self, imageName: "noWishlist")
            self.wishlistTableView.isHidden = true
            self.titleLabel.isHidden = true
            self.emptywishlist.isHidden = false
            self.emptyWishList.isHidden = true
        }
    }
    else{
        let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
        let hashKey = userInfoDict["hashKey"]!;
        let customerId = userInfoDict["customerId"]!;
        let postString = ["hashkey":hashKey,"customer_id":customerId]
        // Do any additional setup after loading the view.

        self.sendRequest(url: "mobiconnect/wishlist/getwishlist/", params: postString)
    }

    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return wishlistArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = wishlistTableView.dequeueReusableCell(withIdentifier: "wishlistCell", for: indexPath) as! WishlistViewCell;
        
        cell.productName.text = wishlistArray[indexPath.row]["product_name"]! + "\n" + "Price" + " : " + wishlistArray[indexPath.row]["regular_price"]!
        
        cell.deleteButton.tag = indexPath.row;
        //cell.deleteButton.addTarget(self, action: #selector(emptywishlist.deleteWishlistItemConfirmation(_:)), for: UIControlEvents.TouchUpInside);
        
        cedMageImageLoader.shared.loadImgFromUrl(urlString: wishlistArray[indexPath.row]["product_image"]!,completionHandler: { (image: UIImage?, url: String) in
            DispatchQueue.main.async {
                cell.productImg.image = image
            }
        })
        cell.containerView.cardView()
        return cell
    }
 
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        let itemId =  wishlistArray[indexPath.row]["wishlist_item_id"];
        let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
        let hashKey = userInfoDict["hashKey"]!;
        let customerId = userInfoDict["customerId"]!;
        var postString = ["hashkey":hashKey,"customer_id":customerId];
        //postString += "&item_id="+itemId!
        postString.updateValue(itemId!, forKey: "item_id")
        self.sendRequest(url: "mobiconnect/wishlist/remove/", params: postString)
        wishlistArray.remove(at: indexPath.row)
        tableView.deleteRows(at: [indexPath as IndexPath], with: .left)
        cedMage.delay(delay: 1, closure: {
           self.wishlistTableView.reloadData()
        })
       
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let productview = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "rootPageViewController") as! cedMageProductViewRoot
        productview.pageData = wishlistArray as NSArray
        let instance = cedMage.singletonInstance
        instance.storeParameterInteger(parameter: indexPath.row)
        self.navigationController?.pushViewController(productview
            , animated: true)

    }
  
}
