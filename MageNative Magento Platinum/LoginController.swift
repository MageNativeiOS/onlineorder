//
//  LoginController.swift
//  MageNative Magento Platinum
//
//  Created by Hamza Usmani on 09/10/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit
import AuthenticationServices

final class LoginController: MagenativeUIViewController {
    
    //MARK: - Properties
    
    private var email: String = ""
    private var loginWithMobile: Bool = false { didSet { loginWithMobile ? mobileSelected() : emailSelected() }}
    
    //MARK: - Views
    
    lazy var backgroundImage:UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "splash"))
        imageView.contentMode = .scaleAspectFill
        let view = UIView()
        imageView.addSubview(view)
        view.addConstraintsToFillView(imageView)
        view.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        view.isHidden = true
        return imageView
    }()
    
    lazy var scroll: UIScrollView = {
        let view = UIScrollView(frame: .zero)
        view.frame = self.view.bounds
        view.contentSize = CGSize(width: self.view.frame.width, height: 500) //self.view.frame.height + 300
        view.alwaysBounceVertical = false
        return view
    }()
    
    lazy var container:UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        view.frame.size = CGSize(width: self.view.frame.width, height: 500) //self.view.frame.height + 300
        return view
    }()
    
    lazy var logo:UIImageView = {
        let imageView = UIImageView()
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFill
        imageView.image = UIImage(named: "header-Title")
        return imageView
    }()
    
    lazy private var loginWithEmailButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Login With Email", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = .systemFont(ofSize: 13, weight: .semibold)
        button.backgroundColor = Settings.themeColor
        button.layer.cornerRadius = 5.0
        button.anchor(height: 35)
        button.addTarget(self, action: #selector(loginWithEmailTapped), for: .touchUpInside)
        return button
    }()
    
    lazy private var loginWithMobileButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Login With Mobile", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = .systemFont(ofSize: 13, weight: .semibold)
        button.backgroundColor = Settings.themeColor
        button.layer.cornerRadius = 5.0
        button.anchor(height: 35)
        button.addTarget(self, action: #selector(loginWithMobileTapped), for: .touchUpInside)
        return button
    }()
    
    lazy var emailField: SkyFloatingLabelTextField = {
        let field = createField(withPlaceholder: "Your Email".localized)
        return field
    }()
    
    lazy var passwordField: SkyFloatingLabelTextField = {
        let field = createField(withPlaceholder: "Your Password".localized)
        field.isSecureTextEntry = true
        return field
    }()
    
    lazy var loginButton:UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Login".localized, for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = .systemFont(ofSize: 15, weight: .semibold)
        button.layer.cornerRadius = 5.0
        button.backgroundColor = Settings.themeColor//.withAlphaComponent(0.7)
        button.anchor(height: 40)
        button.addTarget(self, action: #selector(loginButtonTapped), for: .touchUpInside)
        return button
    }()
    
    lazy var forgotPasswordButton:UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Forgot Password".localized, for: .normal)
        button.setTitleColor(.red, for: .normal)
        button.titleLabel?.font = .systemFont(ofSize: 13, weight: .semibold)
        button.contentHorizontalAlignment = .leading
        button.addTarget(self, action: #selector(forgetPasswordTapped), for: .touchUpInside)
        return button
    }()
    
    lazy var signupButton:UIButton = {
        let button = UIButton(type: .system)
        let attributedString: NSMutableAttributedString = .init(string: "Don't have an account? ".localized, attributes: [.foregroundColor: UIColor.mageLabel,
                                                                                                                .font: UIFont.systemFont(ofSize: 13, weight: .medium)])
        attributedString.append(NSAttributedString(string: "Sign Up".localized, attributes: [.foregroundColor: Settings.themeColor, .font: UIFont.systemFont(ofSize: 13, weight: .semibold)]))
        button.setAttributedTitle(attributedString, for: .normal)
        button.addTarget(self, action: #selector(signupTapped), for: .touchUpInside)
        return button
    }()
    
    lazy var facebookLogin:UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Login with Fb".localized, for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = UIColor(hexString: "#4267B2")
        button.titleLabel?.font = .systemFont(ofSize: 12, weight: .medium)
        button.anchor(height: 35)
        button.layer.cornerRadius = 5.0
        button.addTarget(self, action: #selector(facebookButtonTapped), for: .touchUpInside)
     //   button.isHidden = false
        return button
    }()
    
    lazy var googleLogin:UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Login with Google".localized, for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = UIColor(hexString: "#DB4437")
        button.titleLabel?.font = .systemFont(ofSize: 12, weight: .medium)
        button.anchor(height: 35)
        button.layer.cornerRadius = 5.0
        button.addTarget(self, action: #selector(googleButtonTapped), for: .touchUpInside)
        return button
    }()
    
    @available(iOS 13.0, *)
    lazy var appleLoginButton: ASAuthorizationAppleIDButton = {
        let isDarkTheme = view.traitCollection.userInterfaceStyle == .dark
        let style: ASAuthorizationAppleIDButton.Style = isDarkTheme ? .white : .black
        let button = ASAuthorizationAppleIDButton(type: .default, style: style)
        button.addTarget(self, action: #selector(signinWithAppleTapped), for: .touchUpInside)
        button.anchor(height: 35)
        return button
    }()
    
    lazy var backButton:UIButton = {
        let button = UIButton(type: .system)
        button.setImage(UIImage(named: "back_icon_login"), for: .normal)
        button.tintColor = Settings.themeColor
        button.addTarget(self, action: #selector(backButtonTapped), for: .touchUpInside)
        return button
    }()
    
    
    //MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        emailSelected()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.navigationBar.isHidden = false
    }
    
    //MARK: - Selectors
    
    @objc private func backButtonTapped() {
        navigationController?.popViewController(animated: true)
    }
    
    @objc private func loginWithEmailTapped() {
        loginWithMobile = false
    }
    
    @objc private func loginWithMobileTapped() {
        loginWithMobile = true
    }
    
    @objc private func loginButtonTapped() {
        let email    = emailField.text ?? ""
        let password = passwordField.text ?? ""
        
        // TODO: - Validate User email and Password
        
        // check login type
        // if login type is mobile
        if loginWithMobile {
            if email.isEmpty {
                view.makeToast("Please enter your mobile number".localized)
                return
            }
        } else {
            // Check if empty
            if email.isEmpty || password.isEmpty {
                view.makeToast("Email or Password is Empty".localized)
                return
            }
            // Check for Valid Email
            if !isValidEmail(email: email) {
                view.makeToast("Invalid Email".localized)
                return
            }
        }
        
        if loginWithMobile {
            verify(number: email) { [weak self] success in if success { self?.sendOTP(toNumber: email) } }
            return
        }
        let extensionString = ["login_type":"email"]
        let param = ["email": email, "password": password,"store_id": UserDefaults.standard.value(forKey: "storeId") as? String ?? "",
                     "extension_attributes": extensionString] as [String : Any]
        
        ApiHandler.handle.requestDict(with: "mobiconnect/customer/login/", params: param, requestType: .POST, controller: self) { [weak self] data, error in
            guard let data = data else { return }
            guard var json = try? JSON(data: data) else { return }
            self?.email = email
            self?.handleLogin(with: &json)
        }
    }
    
    @objc private func signupTapped() {
        //let controller = UIStoryboard(name: "cedMageLogin", bundle: nil).instantiateViewController(withIdentifier: "cedMageRegister") as! cedMageRegister
        navigationController?.pushViewController(SignupController(), animated: true)
    }
    
    @objc private func forgetPasswordTapped() {
        let controller = UIStoryboard(name: "cedMageLogin", bundle: nil).instantiateViewController(withIdentifier: "cedMageForgotPassword") as! cedMageForgotPassword
        navigationController?.pushViewController(controller, animated: true)
    }
    
    //MARK: - Helper Functions
    
    private func configureUI() {
        view.backgroundColor = .mageSystemBackground
        
        //view.addSubview(backgroundImage)
        //backgroundImage.addConstraintsToFillView(view)
        
        view.addSubview(scroll)
        scroll.addSubview(container)
        
        container.addSubview(logo)
        logo.anchor(top: container.safeAreaLayoutGuide.topAnchor, paddingTop: 16, width: 180, height: 80)
        logo.centerX(inView: container)
    
        let typeStack = UIStackView(arrangedSubviews: [loginWithEmailButton, loginWithMobileButton])
        typeStack.axis = .horizontal
        typeStack.distribution = .fillEqually
        typeStack.spacing = 8.0
        
        let socialStack = UIStackView(arrangedSubviews: [facebookLogin, googleLogin])
        socialStack.axis = .horizontal
        socialStack.distribution = .fillEqually
        socialStack.spacing = 8.0
        
        var stack = UIStackView()
        
        if #available(iOS 13.0, *) {
            stack = UIStackView(arrangedSubviews: [typeStack, emailField, passwordField, forgotPasswordButton, loginButton, signupButton, socialStack, appleLoginButton])
        } else {
            stack = UIStackView(arrangedSubviews: [typeStack, emailField, passwordField, forgotPasswordButton, loginButton, signupButton, socialStack])
        }
        
        stack.axis = .vertical
        stack.spacing = 16.0
        stack.setCustomSpacing(32, after: typeStack)
        
        container.addSubview(stack)
        stack.anchor(top: logo.bottomAnchor, left: container.leadingAnchor, right: container.trailingAnchor, paddingTop: 32, paddingLeft: 32, paddingRight: 32)
        
        view.addSubview(backButton)
        backButton.anchor(left: view.leadingAnchor, paddingLeft: 8, width: 44, height: 44)
        backButton.centerY(inView: logo)
        
        // Comment the below two lines to show the social login button , by defaault they are hidden
        /*socialStack.isHidden = true
        if #available(iOS 13.0, *) { appleLoginButton.isHidden = true } */
        
    }
    
    private func createField(withPlaceholder placeholder: String) -> SkyFloatingLabelTextField {
        let field = SkyFloatingLabelTextField()
        field.placeholder = placeholder
        field.selectedLineColor = Settings.themeColor
        field.lineColor = .mageLabel
        field.placeholderColor = .mageLabel
        field.selectedTitleColor = .mageLabel
        field.titleColor = .mageLabel
        field.textColor = .mageLabel
        field.font = .systemFont(ofSize: 14, weight: .medium)
        return field
    }
    
    private func mobileSelected() {
        loginWithEmailButton.backgroundColor = .mageSecondarySystemBackground
        loginWithEmailButton.setTitleColor(.mageLabel, for: .normal)
        
        loginWithMobileButton.backgroundColor = Settings.themeColor
        loginWithMobileButton.setTitleColor(.white, for: .normal)
        
        emailField.placeholder = "Your Mobile Number"
        passwordField.isHidden = true
    }
    
    private func emailSelected() {
        loginWithEmailButton.backgroundColor = Settings.themeColor//.mageSecondarySystemBackground
        loginWithEmailButton.setTitleColor(.white, for: .normal)
        
        loginWithMobileButton.backgroundColor = .mageSecondarySystemBackground
        loginWithMobileButton.setTitleColor(.mageLabel, for: .normal)
        
        emailField.placeholder = "Your Email"
        passwordField.isHidden = false
    }
    
    private func verify(number: String, completion:@escaping (Bool) -> Void) {
        ApiHandler.handle.request(with: "mobiconnect/customer/validateNumber", params: ["mobile": number,"type": "login"], requestType: .POST, controller: self) { [weak self] data, error in
            guard let data = data else { return }
            guard let json = try? JSON(data: data) else { return }
            print(json)
            DispatchQueue.main.async {
                self?.view.makeToast(json[0]["data"]["customer"][0]["message"].stringValue)
            }
            completion(json[0]["data"]["customer"][0]["status"].boolValue)
        }
    }
    
    private func sendOTP(toNumber number: String) {
        let countryID: String = Locale.current.regionCode ?? ""
        ApiHandler.handle.request(with: "mobiconnect/customer/sendOtp", params: ["mobile": number, "country_id": countryID], requestType: .POST, controller: self) { [weak self] data, _ in
            guard let data = data else { return }
            guard let json = try? JSON(data: data) else { return }
            print(json)
            
            self?.view.makeToast(json[0]["data"]["customer"][0]["message"].stringValue)
            // if OTP is successfully sent show the OTP popup to verify OTP
            if json[0]["data"]["customer"][0]["status"].boolValue {
                
                let popup        = AuthenticationView(otp: "1234")
                guard let window = UIApplication.shared.keyWindow else { return }
                window.addSubview(popup)
                popup.frame = window.bounds
                
                // Handle the success state of OTP validation
                popup.onSuccess = { otp in
                    self?.verifyOTP(on: number, otp: otp)
                }
                
                // Handle the failure state of OTP validation
                popup.onFailure = { message in
                    self?.view.makeToast(message)
                    return
                }
                
                // Handle the resend OTP Action
                popup.onResend = {
                    self?.sendOTP(toNumber: number)
                }
            }
        }
    }
    
    private func verifyOTP(on number: String, otp: String) {
        let param = ["mobile": number, "country_id": Locale.current.regionCode ?? "", "otp": otp]
        ApiHandler.handle.request(with: "mobiconnect/customer/verifyOtp", params: param, requestType: .POST, controller: self) { [weak self] data, _ in
            guard let data = data else { return }
            guard let json = try? JSON(data: data) else { return }
            print(json)
            
            self?.view.makeToast(json[0]["data"]["customer"][0]["message"].stringValue)
            // if OTP is successfully verified
            if json[0]["data"]["customer"][0]["status"].boolValue  {
                // Create param and send register API Request
                let extensionString = ["login_type":"otp", "mobile":number]
                
                let loginParam = ["store_id":UserDefaults.standard.value(forKey: "storeId") as? String ?? "", "extension_attributes": extensionString] as [String : Any]
                
                ApiHandler.handle.requestDict(with: "mobiconnect/customer/login/", params: loginParam, requestType: .POST, controller: self!) { [weak self] data, error in
                    guard let data = data else { return }
                    guard var json = try? JSON(data: data) else { return }
                    self?.email = number
                    self?.handleLogin(with: &json)
                }
            }
        }
    }
    
    private func doSocialLogin(for user: [String:String]) {
        ApiHandler.handle.request(with: "mobiconnect/sociallogin/create/", params: user, requestType: .POST, controller: self) { [weak self] data, error in
            guard let data = data else { return }
            guard var json = try? JSON(data: data) else { return }
            self?.email = user["email"] ?? "user@magenative.com"
            self?.handleLogin(with: &json)
        }
    }
    
    private func handleLogin(with response: inout JSON) {
        print(response)
        response = response[0]["data"]["customer"][0]
        let res = response
        
        // TODO:- Check for success
        if response["status"].stringValue != "success" {
            DispatchQueue.main.async {
                self.view.makeToast(res["message"].stringValue)
            }
            return
        }
        
        // TODO:- Check if admin confirmation is required
        if response["isConfirmationRequired"].stringValue.lowercased() == "YES".lowercased() {
            LoginManager().logOut()
            GIDSignIn.sharedInstance()?.signOut()
            
            view.makeToast(response["message"].stringValue)
            return
        }
        
        // TODO:- Saving the User Information
        let customerInfo: [String:String] = ["email": email, "customerId": response["customer_id"].stringValue, "hashKey": response["hash"].stringValue]
        let customerName: String = response["name"].stringValue
        let cartSummary : String = response["cart_summary"].stringValue
        
        UserDefaults.standard.set(customerName, forKey: "name")
        UserDefaults.standard.set(true, forKey: "isLogin")
        UserDefaults.standard.set(customerInfo, forKey: "userInfoDict")
        UserDefaults.standard.set(String(cartSummary), forKey: "items_count")
        
        // TODO:- Set the cart count
        setCartCount(view: self, items: cartSummary)
        
        view.makeToast("Login Successful")
        NotificationCenter.default.post(name: NSNotification.Name("loadDrawerAgain"), object: nil)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) { [weak self] in
            self?.navigationController?.popToRootViewController(animated: true)
        }
    }
}


//MARK: - FACEBOOK LOGIN EXTENSION

extension LoginController {
    
    @objc fileprivate func facebookButtonTapped() {
        let loginManager: LoginManager = .init()
        
        // Check if user is already Logged in,
        // if logged in then logout the user
        if let _ = AccessToken.current { loginManager.logOut(); print("DEBUG: User Logged out of Facebook"); return }
        
        loginManager.logIn(permissions: ["email","public_profile"], from: self) { [weak self] result, error in
            
            // Handle if any error exist
            if let error = error { self?.view.makeToast("Error Signing in view facebook with error : \(error.localizedDescription)"); return }
            
            guard let result = result else { self?.view.makeToast("Error getting result for Facebook login"); return }
            
            if result.isCancelled { self?.view.makeToast("Login Cancelled by User"); return }
            
            // Get Profile Data with Graph Path
            let graphRequest: GraphRequest = .init(graphPath: "me", parameters: ["fields":"id,email,name,first_name,last_name"])
            
            graphRequest.start { [weak self]connection, result, error in
                if let error = error { print("DEBUG: Error occured in Graph request, error is : \(error.localizedDescription)"); return }
                
                guard let graphResult = result else { print("DEBUG: Error getting Graph request result"); return }
                
                let token     = AccessToken.current?.tokenString ?? ""
                let firstname = (graphResult as AnyObject).value(forKey: "first_name") as? String ?? ""
                let lastname  = (graphResult as AnyObject).value(forKey: "last_name") as? String ?? ""
                let email     = (graphResult as AnyObject).value(forKey: "email") as? String ?? ""
                
                let fbUser = ["type":"facebook","token":token,"firstname":firstname,"lastname":lastname,"email":email]
                
                self?.doSocialLogin(for: fbUser)
            }
           
        }
    }
    
}

//MARK: - GOOGLE LOGIN EXTENSION

extension LoginController : GIDSignInDelegate {
   
    @objc fileprivate func googleButtonTapped() {
        GIDSignIn.sharedInstance().presentingViewController = self
        GIDSignIn.sharedInstance().delegate                 = self
        GIDSignIn.sharedInstance().shouldFetchBasicProfile  = true
        GIDSignIn.sharedInstance().clientID = cedMage.getInfoPlist(fileName: "GoogleService-Info", indexString: "CLIENT_ID") as? String
        GIDSignIn.sharedInstance().scopes.append("https://www.googleapis.com/auth/plus.login")
        GIDSignIn.sharedInstance().scopes.append("https://www.googleapis.com/auth/plus.me")
        GIDSignIn.sharedInstance().signIn()
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        // Check for error
        if let error = error {
            print("DEBUG: Error occured while signing with google, Error is \(error.localizedDescription)")
            return
        }
        
        let token     = user.authentication.idToken ?? ""
        let firstName = user.profile.name ?? ""
        //let givenName = user.profile.givenName ?? ""
        let lastName  = user.profile.familyName ?? ""
        let email     = user.profile.email ?? ""
        
        let googleUser = ["firstname":firstName,"lastname":lastName,"email":email,"type":"google", "token":token]
        print(user.profile)
        doSocialLogin(for: googleUser)
    }
    
    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
        present(viewController, animated: true, completion: nil)
    }
    
    func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController!) {
        dismiss(animated: true, completion: nil)
    }
    
    
}

// MARK:- SIGN IN WITH APPLE EXTENSION

@available(iOS 13.0, *)
extension LoginController: ASAuthorizationControllerDelegate, ASAuthorizationControllerPresentationContextProviding {
    
    
    @objc fileprivate func signinWithAppleTapped() {
        let request = ASAuthorizationAppleIDProvider().createRequest()
        request.requestedScopes = [.fullName, .email]
        
        let controller = ASAuthorizationController(authorizationRequests: [request])
        controller.delegate = self
        controller.presentationContextProvider = self
        controller.performRequests()
    }
    
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return view.window!
    }
    
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        guard let credentials = authorization.credential as? ASAuthorizationAppleIDCredential else { print("DEBUG: Error getting credential for apple ID"); return }
        let username    = credentials.fullName?.description ?? ""
        let email       = credentials.email ?? ""
        //let tokenData   = credentials.identityToken
        //let tokenString = String(data: tokenData ?? Data(), encoding: .utf8)
        print("DEBUG: Email is \(email), fullname is \(username)")
        
        var appleUser: [String:String] = [:]
        
        if username.isEmpty || email.isEmpty {
            appleUser["token"] = credentials.user
            appleUser["type"]  = "apple"
        }
        
        appleUser["token"]      = credentials.user
        appleUser["firstname"]  = credentials.fullName?.givenName ?? ""
        appleUser["lastname"]   = credentials.fullName?.familyName ?? ""
        appleUser["email"]      = credentials.email ?? ""
        appleUser["type"]       = "apple"
        
        doSocialLogin(for: appleUser)
    }
    
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        print("DEBUG: SIGN IN WITH APPLE Failed with error: \(error.localizedDescription)")
    }
    
    
}
