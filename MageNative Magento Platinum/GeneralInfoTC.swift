//
//  GeneralInfoTC.swift
//  MageNative Magento Platinum
//
//  Created by Hamza Usmani on 22/08/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

protocol RMADelegate: class {
    func optionsSelected(by button: UIButton, atCell cell: UITableViewCell)
}

class GeneralInfoTC: UITableViewCell {

   //MARK:- Properties
    
    static var reuseID:String = "GeneralInfoTC"
    weak var delegate: RMADelegate?
    var dataToShow = ["Reason Requested", "Package Condition", "Resolution Requested"]
    let drpdown = DropDown()
    var selectedReason: String? = nil
    var selectedResolution: String? = nil
    var selectedCondition: String? = nil
    
    var reasonArray = [ReturnData]()
    var resolutionArray = [ReturnData]()
    var conditionArray = [ReturnData]() {
        didSet { configureUI() }
    }
    
    lazy var generalStack:UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.distribution = .fillEqually
        stack.alignment = .fill
        stack.spacing = 8.0
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()
    
    lazy var rmaButton:UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Request RMA", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 16, weight: .semibold)
     //   button.backgroundColor = DynamicColor.darkThemeColor
        button.layer.cornerRadius = 5.0
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(returnButtonTapped), for: .touchUpInside)
        return button
    }()
    
    
    //MARK:- Lifecycle
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        addSubview(generalStack)
        generalStack.topAnchor.constraint(equalTo: topAnchor, constant: 8).isActive = true
        generalStack.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16).isActive = true
        generalStack.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16).isActive = true
        
        addSubview(rmaButton)
        rmaButton.topAnchor.constraint(equalTo: generalStack.bottomAnchor, constant: 16).isActive = true
        rmaButton.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 32).isActive = true
        rmaButton.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -32).isActive = true
        rmaButton.heightAnchor.constraint(equalToConstant: 35).isActive = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK:- Helpers
   
    func configureUI() {
        generalStack.subviews.forEach({ $0.removeFromSuperview() })
        
        for title in dataToShow {
            let heading = UILabel()
            heading.font = .systemFont(ofSize: 16, weight: .semibold)
            let button = UIButton(type: .system)
            button.setTitleColor(.white, for: .normal)
        //    button.backgroundColor = DynamicColor.darkThemeColor?.withAlphaComponent(0.8)
            button.layer.cornerRadius = 3.0
            button.contentHorizontalAlignment = .center
            button.titleLabel?.font = .systemFont(ofSize: 14, weight: .medium)
            
            let stack = UIStackView(arrangedSubviews: [heading, button])
            stack.axis = .horizontal
            stack.distribution = .fillEqually
            stack.spacing = 5.0
            
            switch title {
            case "Reason Requested":
                heading.text = title
                button.setTitle("Select Reason", for: .normal)
                
                button.addTarget(self, action: #selector(reasonButtonTapped(_:)), for: .touchUpInside)
            case "Package Condition":
                heading.text = title
                button.setTitle("Select Condition", for: .normal)
                button.addTarget(self, action: #selector(conditionTapped(_:)), for: .touchUpInside)
            case "Resolution Requested":
                heading.text = title
                button.setTitle("Select Resolution", for: .normal)
                button.addTarget(self, action: #selector(resolutionTapped(_:)), for: .touchUpInside)
            default:
                return
            }
            
            generalStack.addArrangedSubview(stack)
        }
    }

    //MARK:- Selectors
    
    @objc func reasonButtonTapped(_ sender: UIButton) {
        let reasonLabel = self.reasonArray.map({ $0.label })
        let reasonValue = self.reasonArray.map({ $0.value })
        
        
        drpdown.anchorView = sender
        
        drpdown.dataSource = reasonLabel
        drpdown.selectionAction = {[weak self] index, item in
            sender.setTitle(item, for: .normal)
            self?.selectedReason = reasonValue[index]
        }
        
        if drpdown.isHidden {
            drpdown.show()
        }
        
        
    }
    
    @objc func resolutionTapped(_ sender: UIButton) {
        let resolutionLabel = self.resolutionArray.map({ $0.label })
        let resolutionValue = self.resolutionArray.map({ $0.value })
        
        
        drpdown.anchorView = sender
        
        drpdown.dataSource = resolutionLabel
        drpdown.show()
        drpdown.selectionAction = {[weak self] index,item in
            sender.setTitle(item, for: .normal)
            self?.selectedCondition = resolutionValue[index]
        }
    }
    
    
    @objc func conditionTapped(_ sender: UIButton) {
        let conditionLabel = self.conditionArray.map({ $0.label })
        let conditionValue = self.conditionArray.map({ $0.value })
        
        
        drpdown.anchorView = sender
        
        drpdown.dataSource = conditionLabel
        drpdown.show()
        drpdown.selectionAction = {[weak self] index, item in
            sender.setTitle(item, for: .normal)
            self?.selectedResolution = conditionValue[index]
        }
    }
    
    
    @objc func returnButtonTapped() {
        delegate?.optionsSelected(by: rmaButton, atCell: self)
    }
    
}
