//
//  OrderSummaryTotalTC.swift
//  MageNative Magento Platinum
//
//  Created by Hamza Usmani on 06/09/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class OrderSummaryTotalTC: UITableViewCell {
    
    //MARK:- Properties
    
    static var reuseID: String = "OrderSummaryTotalTC"
    //var totalArray = ["Amount to pay", "Shipping Charge", "Tax amount", "Discount"]
    
    lazy var container:UIView = {
        let view = UIView()
        view.backgroundColor = .mageSystemBackground
        view.layer.cornerRadius = 5.0
        view.layer.borderColor = Settings.themeColor.cgColor
        view.layer.borderWidth = 1.0
        return view
    }()
    
   
    lazy var totalStack:UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.spacing = 3.0
        return stack
    }()
    
    
    //MARK:- Lifecycle
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        backgroundColor = .clear
        
        
        addSubview(container)
        container.anchor(top: topAnchor, left: leadingAnchor, bottom: bottomAnchor, right: trailingAnchor,
                         paddingTop: 8, paddingLeft: 8, paddingBottom: 8,paddingRight: 8)
        
        container.addSubview(totalStack)
        totalStack.anchor(top: container.topAnchor, left: container.leadingAnchor, paddingTop: 8, paddingLeft: 8)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
    //MARK:- Helper Methods
    
    func populate(with total: [summaryFields]) {
        totalStack.subviews.forEach({ $0.removeFromSuperview() })
        
        for heading in total {
            createRow(for: heading.label, value: heading.value)
//            switch heading {
//            case "Amount to pay":
//                createRow(for: heading, value: total?.amounttopay)
//            case "Shipping Charge":
//                createRow(for: heading, value: total?.shipping_amount)
//            case "Tax amount":
//                createRow(for: heading, value: total?.tax_amount)
//            case "Discount":
//                createRow(for: heading, value: total?.discount_amount)
//            default:
//                return
//            }
        }
    }
    
    private func createRow(for heading: String?, value: String?) {
        let label = UILabel()
        label.text = heading
        label.font = .systemFont(ofSize: 15, weight: .semibold)
        
        let labelTwo = UILabel()
        labelTwo.text = value
        labelTwo.font = .systemFont(ofSize: 15, weight: .regular)
        labelTwo.textColor = .mageSecondaryLabel
        
        let stack = UIStackView(arrangedSubviews: [label,labelTwo])
        stack.axis = .horizontal
        stack.distribution = .fillEqually
        stack.spacing = 8.0
        stack.anchor(height: 18)
        
        totalStack.addArrangedSubview(stack)
    }
    
}
