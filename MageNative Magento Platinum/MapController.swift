//
//  MapController.swift
//  MageNative Magento Platinum
//
//  Created by Hamza Usmani on 09/09/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

class MapController: UIViewController {
    
    //MARK - Properties
    
    private let locationManager = CLLocationManager()
    private let googleGeoder = GMSGeocoder()
    private var isDragEnabled = true
    private var currentLocation: CLLocation? = nil
    
    let mapView: GMSMapView = {
        let map = GMSMapView()
        map.isMyLocationEnabled = true
        map.settings.myLocationButton = true
        return map
    }()
    
    let mapPin: UIImageView = {
        let iv = UIImageView(image: UIImage(named: "addressPin_icon")?.withRenderingMode(.alwaysTemplate))
        iv.tintColor = .red
        iv.contentMode = .scaleAspectFit
        return iv
    }()
        
    lazy var blurView: UIVisualEffectView = {
        let effect = UIBlurEffect(style: .prominent)
        let view = UIVisualEffectView(effect: effect)
        
        view.contentView.addSubview(proceedButton)
        proceedButton.anchor(left: view.leadingAnchor, bottom: view.bottomAnchor, right: view.trailingAnchor, paddingLeft: 16, paddingBottom: 16, paddingRight: 16, height: 35)
        
        view.contentView.addSubview(changeAddressButton)
        changeAddressButton.anchor(bottom: proceedButton.topAnchor, right: view.trailingAnchor, paddingBottom: 8, paddingRight: 8)
        changeAddressButton.setDimensions(width: 44, height: 44)
        
        view.contentView.addSubview(addressLabel)
        addressLabel.anchor(left: view.leadingAnchor, right: changeAddressButton.leadingAnchor, paddingLeft: 16, paddingRight: 8)
        addressLabel.centerY(inView: changeAddressButton)
        
        return view
    }()
    
    let proceedButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Proceed", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = .systemFont(ofSize: 15, weight: .semibold)
        button.layer.cornerRadius = 5.0
        button.backgroundColor = Settings.themeColor
        button.addTarget(self, action: #selector(handleProceed), for: .touchUpInside)
        return button
    }()
    
    let addressLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 12, weight: .medium)
        label.textColor = .mageLabel
        label.numberOfLines = 3
        return label
    }()
    
    lazy var changeAddressButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(UIImage(named: "changeAddress_icon"), for: .normal)
        button.addTarget(self, action: #selector(handleChangeAddress), for: .touchUpInside)
        return button
    }()
    
    //MARK - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        checkRequestLocationPermisson()
        configureUI()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden=true
    }
    
    //MARK: - Selectors
    
    @objc func handleChangeAddress() {
        isDragEnabled = false
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        present(autocompleteController, animated: true)
    }
    
    @objc func handleProceed() {
        guard let location = currentLocation else { return }
        
        CLGeocoder().reverseGeocodeLocation(location) { (placemarks, error) in
            
            
            
            if let city = placemarks?.first?.locality,
                let state = placemarks?.first?.administrativeArea,
                let country = placemarks?.first?.country,
                let countryCode = placemarks?.first?.isoCountryCode,
                let pincode = placemarks?.first?.postalCode {
                
                let param = ["city": city, "state":state, "country": country, "countryCode":countryCode, "zipcode": pincode, "latitude": "\(location.coordinate.latitude)",
                    "longitude": "\(location.coordinate.longitude)","fullAddress": self.addressLabel.text ?? ""]
                UserDefaults.standard.set(param, forKey: "userCurrentLocation")
                
                print("DEBUG: User's current location is \(Settings.currentLocation)")
                
                
                let vc = HomeController()
                self.navigationController?.setViewControllers([vc], animated: true)
                if self.presentingViewController != nil { self.dismiss(animated: true)}
                return
            }
            print("DEBUG: User Location cannot be saved")
        }
    }
    
    //MARK: - Helper Methods
    
    fileprivate func configureUI() {
        self.navigationController?.navigationBar.isHidden=true
        view.backgroundColor = .mageSystemBackground
      //  navigationController?.navigationBar.isHidden = true
        
        view.addSubview(mapView)
        mapView.anchor(top: view.topAnchor,left: view.leadingAnchor, bottom: view.bottomAnchor, right: view.trailingAnchor, paddingBottom: 115)
        mapView.delegate = self
        
        view.addSubview(mapPin)
        mapPin.setDimensions(width: 45, height: 45)
        mapPin.center(inView: mapView)
        
        view.addSubview(blurView)
        blurView.anchor(left: view.leadingAnchor, bottom: view.bottomAnchor, right: view.trailingAnchor, height: 120)
    }
    
    fileprivate func getAddress(atLocation location: CLLocation) {
        let latitude = location.coordinate.latitude
        let longitude = location.coordinate.longitude
        
        let addressLocation = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        
        googleGeoder.reverseGeocodeCoordinate(addressLocation) { result, error in
            guard let address = result else { return }
            print("DEBUG: \(address.results()?.first?.lines ?? [])")
            
            var addressString = String()
            address.results()?.first?.lines?.forEach({ addressString += $0 })
            
            DispatchQueue.main.async {
                self.addressLabel.text = addressString
            }
        }
    }
}

//MARK: - CLLocationManagerDelegate

extension MapController: CLLocationManagerDelegate {
    fileprivate func checkRequestLocationPermisson() {
        locationManager.delegate = self
        
        switch CLLocationManager.authorizationStatus() {
        case .notDetermined:
            print("DEBUG: Location not determined")
            locationManager.requestWhenInUseAuthorization()
        case .restricted, .denied:
            break
        case .authorizedAlways:
            print("DEBUG: Location always authorized")
            locationManager.startUpdatingLocation()
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
        case .authorizedWhenInUse:
            print("DEBUG: When in use ")
            locationManager.requestAlwaysAuthorization()
            locationManager.startUpdatingLocation()
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
        @unknown default:
            break
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            print("DEBUG: Got the When in use Permisson")
            locationManager.requestAlwaysAuthorization()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else { return }
        
        currentLocation = location
        
        let latitude   = location.coordinate.latitude
        let longitude  = location.coordinate.longitude
        
        let camera     = GMSCameraPosition.camera(withLatitude: latitude, longitude: longitude, zoom: 16)
        mapView.camera = camera
        
        getAddress(atLocation: location)
        locationManager.stopUpdatingLocation()
    }
}

//MARK: - GMSMapViewDelegate

extension MapController: GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        print("DEBUG: Position is \(position.target.latitude) and longitude \(position.target.longitude)")
        let latitude  = position.target.latitude
        let longitude = position.target.longitude
        let addressLocation = CLLocation(latitude: latitude, longitude: longitude)
        currentLocation     = addressLocation
        
        getAddress(atLocation: addressLocation)
    }
}

//MARK: - GMSAutocompleteViewControllerDelegate

extension MapController: GMSAutocompleteViewControllerDelegate {
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        DispatchQueue.main.async { self.addressLabel.text = place.formattedAddress }
        dismiss(animated: true, completion: nil)
        
        let location    = CLLocation(latitude: place.coordinate.latitude, longitude: place.coordinate.longitude)
        currentLocation = location
        
        let camera = GMSCameraPosition.camera(withLatitude: place.coordinate.latitude, longitude: place.coordinate.longitude, zoom: 18.0)
        mapView.animate(to: camera)
        
        isDragEnabled = true
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print("DEBUG: Failed with error \(error)")
        dismiss(animated: true, completion: nil)
        isDragEnabled = true
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        print("DEBUG: Autocomplete cancelled by user")
        dismiss(animated: true, completion: nil)
        isDragEnabled = true
    }
}
