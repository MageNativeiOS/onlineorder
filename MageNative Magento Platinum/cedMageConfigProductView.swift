//
//  cedMageConfigProductView.swift
//  MageNative Magento Platinum
//
//  Created by cedcoss on 11/10/18.
//  Copyright © 2018 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

protocol  configProductPrice {
  func configProductSelected(seletedIDs:[String:String])
}

class cedMageConfigProductView: UIView {
  @IBOutlet weak var label: UILabel!
  @IBOutlet weak var collectionView: UICollectionView!
  
  var view: UIView!
  var configAttributes = [[String:String]]()
  var delegate:configProductPrice?
  var selectedID = String()
  
  override init(frame: CGRect)
  {
    // 1. setup any properties here
    
    // 2. call super.init(frame:)
    super.init(frame: frame)
    
    // 3. Setup view from .xib file
    xibSetup()
  }
  
  required init?(coder aDecoder: NSCoder)
  {
    // 1. setup any properties here
    
    // 2. call super.init(coder:)
    super.init(coder: aDecoder)
    
    // 3. Setup view from .xib file
    xibSetup()
  }
  
  func xibSetup()
  {
    view = loadViewFromNib()
    
    // use bounds not frame or it'll be offset
    view.frame = bounds
    
    // Make the view stretch with containing view
    view.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
    
    // Adding custom subview on top of our view (over any custom drawing > see note below)
    
    collectionView.delegate = self;
    collectionView.dataSource = self;
    collectionView.register(UINib(nibName: "cedMageConfigcollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "collectcellConfig");
    addSubview(view)
  }
  
  func loadViewFromNib() -> UIView
  {
    let bundle = Bundle(for: type(of: self))
    let nib = UINib(nibName: "cedMageConfigProductView", bundle: bundle)
    
    // Assumes UIView is top level and only object in CustomView.xib file
    let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
    return view
  }
  
}

extension cedMageConfigProductView:UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 1
  }
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return configAttributes.count
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    
    if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectcellConfig", for: indexPath) as?
      cedMageConfigCollectionCell {
      
      let option = configAttributes[indexPath.row]
        
        print(option)
        
      cell.label.text = option["label"]
      if option["value"] != "" {
        if let value = option["value"] {
          if (value.contains("#")) {
            let color = cedMage.UIColorFromRGB(colorCode: value)
            cell.label.text = ""
            cell.label.backgroundColor = color
          }
        }
      }
        

      if option["thumb"] != ""  {
        print("thumbssssssssssssssss")
        print(option)
        cell.thumbImage.isHidden = false
        if let imageUrl = option["thumb"] {
          cell.thumbImage.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: "placeholder"))
        }
      }
      
      cell.clipsToBounds=true
      cell.layoutIfNeeded()
      return cell
    }
    
    return UICollectionViewCell()
  }
  
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    
    if let cell = collectionView.cellForItem(at: indexPath) as? cedMageConfigCollectionCell {
      let option = configAttributes[indexPath.row]
      delegate?.configProductSelected(seletedIDs: [selectedID:option["id"]!])
//      cell.layer.borderWidth = 2
//      cell.layer.borderColor = UIColor(hexString: "#418bd3")?.cgColor
      if let val = option["label"] {
        self.label.text = val
      }
    }
  }
  
//  func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
//    if let cell = collectionView.cellForItem(at: indexPath) as? cedMageConfigCollectionCell {
//      cell.layer.borderWidth = 0
//      cell.layer.borderColor = UIColor.clear.cgColor
//    }
//  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    return CGSize(width: self.view.frame.width/6-10, height: 75)
  }
}
