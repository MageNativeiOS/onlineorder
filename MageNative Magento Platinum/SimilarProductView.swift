/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */


import UIKit

class SimilarProductView: UIView,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {

    var currency_symbol = "";
    var similarProductArray = [[String:String]]();
    var productSinglePageViewControllerRef = ProductSinglePageViewController();
    
    // Our custom view from the XIB file
    var view: UIView!
    
    //outlets
    
    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var similarProductsCollectionView: UICollectionView!
    
    
    override init(frame: CGRect)
    {
        // 1. setup any properties here
        
        // 2. call super.init(frame:)
        super.init(frame: frame)
        
        // 3. Setup view from .xib file
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        // 1. setup any properties here
        
        // 2. call super.init(coder:)
        super.init(coder: aDecoder)
        
        // 3. Setup view from .xib file
        xibSetup()
    }
    
    func xibSetup()
    {
        view = loadViewFromNib()
        
        // use bounds not frame or it'll be offset
        view.frame = bounds
        
        topLabel.fontColorTool()
        // Make the view stretch with containing view
        view.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        
        //extra setup
        topLabel.font = UIFont(fontName: "HelveticaNeue-Medium", fontSize: CGFloat(17.0));
        
        self.makeCard(self, cornerRadius: 2, color: UIColor.black, shadowOpacity: 0.4);
        similarProductsCollectionView.delegate = self;
        similarProductsCollectionView.dataSource = self;
        similarProductsCollectionView.register(UINib(nibName: "SimilarProductCollectionCell", bundle: nil), forCellWithReuseIdentifier: "similarProductCollectionCell");
        
        //extra setup
        
        
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView
    {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "SimilarProductView", bundle: bundle)
        
        // Assumes UIView is top level and only object in CustomView.xib file
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    
    /* colection view function for product images */
    func numberOfSections(in collectionView: UICollectionView) -> Int{
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print("similarProductArray::123");
        print(similarProductArray);
        
        return similarProductArray.count;
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "similarProductCollectionCell", for: indexPath) as! SimilarProductCollectionCell;
         cell.productImage.sd_setImage(with: URL(string: similarProductArray[indexPath.row]["related_product_image"]!), placeholderImage: UIImage(named: "placeholder"))
        if similarProductArray[indexPath.row]["stock_status"] == "false"{
            cell.stockView.isHidden = false
        }else{
            cell.stockView.isHidden = true
        }
        cell.productName.text = similarProductArray[(indexPath as NSIndexPath).row]["related_product-name"]!;
        cell.productRegularPrice.text = "Price : ".localized+currency_symbol+similarProductArray[(indexPath as NSIndexPath).row]["regular_price"]!;
        
        if(similarProductArray[(indexPath as NSIndexPath).row]["special_price"] != nil && similarProductArray[(indexPath as NSIndexPath).row]["special_price"] != ""){
            var prefixText = "Regular Price : ".localized;
            let regular_price = NSMutableAttributedString(string: prefixText+currency_symbol+similarProductArray[(indexPath as NSIndexPath).row]["regular_price"]!);
            regular_price.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 1, range: NSMakeRange(prefixText.count, regular_price.string.count-prefixText.count));
            cell.productRegularPrice.attributedText = regular_price;
            
            prefixText = "Special Price : ".localized;
            let special_price = NSMutableAttributedString(string: prefixText+currency_symbol+similarProductArray[(indexPath as NSIndexPath).row]["special_price"]!);
            let color_attribute = [ NSAttributedString.Key.foregroundColor: UIColor.red ];
            let range = NSRange(location: prefixText.count, length: special_price.string.count-prefixText.count);
            special_price.addAttributes(color_attribute, range: range);
            cell.productSpecialPrice.attributedText = special_price;
        }//similarProductArray[indexPath.row]["related_currency_symbol"]
        else{
            cell.productSpecialPrice.isHidden = true;
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (collectionView.bounds.size.width - 10 * 3) / 2 //some width
        let height = collectionView.bounds.size.height-15 //ratio
        return CGSize(width: width, height: height);
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: 5, left: 5, bottom: 5, right: 5);
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let productInfo = similarProductArray[indexPath.row];
        print(productInfo["product_id"] ?? "productId");
        let productview = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "rootPageViewController") as! cedMageProductViewRoot;
        productview.pageData = similarProductArray as NSArray;
        let instance = cedMage.singletonInstance;
        instance.storeParameterInteger(parameter: indexPath.row);
        productSinglePageViewControllerRef.navigationController?.pushViewController(productview
            , animated: true);
        
    }
    /* colection view function for product images */
    
}
