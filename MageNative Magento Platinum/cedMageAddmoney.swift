//
//  cedMageAddMoney.swift
//  MageNative Magento Platinum
//
//  Created by Macmini on 12/12/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class cedMageAddmoney: MagenativeUIViewController {
    @IBOutlet weak var addMoney: UIButton!

    @IBOutlet weak var moneyTextField: SkyFloatingLabelTextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        addMoney.setThemeColor()
        addMoney.addTarget(self,action:#selector(addMoney(sender:)),for:.touchUpInside)
    }
    
   override func recieveResponse(data: Data?, requestUrl: String?, response: URLResponse?) {
        do {
        if let data = data {
            var json = try JSON(data:data)
            json = json[0]
            print(json)
            if json["cart_id"]["success"].stringValue == "true" {
                let storyboard = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil);
                let viewController = storyboard.instantiateViewController(withIdentifier: "cartViewController") as! CartViewController;
                self.navigationController?.pushViewController(viewController, animated: true)
            }else{
                 let msg = json["cart_id"]["message"].stringValue
                    cedMageHttpException.showAlertView(me: self, msg: msg, title: "")
            }
        }
        }catch let error {
            print(error.localizedDescription)
    }
    }

    @objc func addMoney(sender:UIButton) {
        if let amount = moneyTextField.text {
            if amount != "" {
            let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
            let  customerId = userInfoDict["customerId"]!;
            let params = ["price":amount,"customer_id":customerId]
            self.sendRequest(url: "mobiconnect/mobiwallet/addmoney",params:params,store:false)
            }else{
                cedMageHttpException.showAlertView(me: self, msg: "Enter Amount To Add", title: "")
            }
        }else{
            cedMageHttpException.showAlertView(me: self, msg: "Enter Amount To Add", title: "")
        }
    }
    

}

