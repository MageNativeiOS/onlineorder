//
//  sentBoxReplyController.swift
//  MageNative Magento Platinum
//
//  Created by MacMini on 13/06/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class sentBoxReplyController: MagenativeUIViewController {
  
  
  
  @IBOutlet weak var messageViewTable: UITableView!
  @IBOutlet weak var replyTextfield: UITextField!
  @IBOutlet weak var attachmentButton: UIButton!
  @IBOutlet weak var sendButton: UIButton!
  let picker = UIImagePickerController()
  var messageViewData = [[String:String]]()
  var attachData = [[String:String]]()
  var id = String()
  var customerId = String()
  var currentPage = 1
  //    var popUpView = sentBoxReplyPopUp()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    messageViewTable.separatorStyle = .none
    
    
    attachmentButton.addTarget(self, action: #selector(attachmentButtonTapped(_:)), for: .touchUpInside)
    sendButton.addTarget(self, action: #selector(sendButtonTapped(_:)), for: .touchUpInside)
    
    let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
    guard let custId = userInfoDict["customerId"] else {return}
    self.customerId = custId
    self.sendRequest(url: "mobimessaging/adminview", params: ["customer_id":custId,"id":id,"page":"\(currentPage)"], store: true)
    // Do any additional setup after loading the view.
  }
  
  
  override func recieveResponse(data: Data?, requestUrl: String?, response: URLResponse?) {
    
    guard let data = data else{return}
    guard var jsonData = try? JSON(data: data) else {return;}
    jsonData = jsonData[0]
    
    if requestUrl == "mobimessaging/admincompose" {
      print(jsonData)
      if jsonData["response"]["status"].stringValue == "true" {
        self.attachData.removeAll()
        self.replyTextfield.text = ""
        self.sendRequest(url: "mobimessaging/adminview", params: ["customer_id":customerId,"id":id,"page":"\(currentPage)"], store: true)
      }
    } else {
      print(jsonData)
      self.messageViewData.removeAll()
      
      if jsonData["response"]["status"].stringValue == "true" {
        
        for dt in jsonData["response"]["view"].arrayValue {
          let temp = ["sender_name":dt["sender_name"].stringValue,
                      "message":dt["message"].stringValue,
                      "sender":dt["sender"].stringValue,
                      "image":dt["image"].stringValue,
                      "created_at":dt["created_at"].stringValue]
          self.messageViewData.append(temp)
        }
        
        messageViewTable.delegate = self
        messageViewTable.dataSource = self
        messageViewTable.reloadData()
        let ip = IndexPath(row: self.messageViewData.count-1, section: 0)
        messageViewTable.scrollToRow(at: ip, at: .bottom, animated: true)
      }else{
        self.view.makeToast(jsonData["data"]["message"].stringValue, duration: 2.0, position: .bottom)
      }
    }
    
  }
  
  
  @objc func attachmentButtonTapped(_ sender: UIButton) {
    let actionSheetControllerIOS8: UIAlertController = UIAlertController(title: "Please select", message: nil, preferredStyle: .actionSheet)
    
    let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { _ in
      print("Cancel")
    }
    actionSheetControllerIOS8.addAction(cancelActionButton)
    
    let saveActionButton = UIAlertAction(title: "Upload Image", style: .default)
    { _ in
      print("upload image")
      self.image_picker()
    }
    actionSheetControllerIOS8.addAction(saveActionButton)
    
    let uploadpdf = UIAlertAction(title: "Upload Document", style: .default)
    { _ in
      print("Delete")
      self.browseButtonPressed()
    }
    actionSheetControllerIOS8.addAction(uploadpdf)
    self.present(actionSheetControllerIOS8, animated: true, completion: nil)
  }
  
  @objc func sendButtonTapped(_ sender: UIButton) {
    
    var param = [String:String]()
    let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
    let storeId = defaults.value(forKey: "storeId") as? String
    guard let custId = userInfoDict["customerId"] else {return}
    
    if replyTextfield.text == "" {self.view.makeToast("Enter somemessage to send", duration: 2.0, position: .center);return}
    let messageToSend = replyTextfield.text!
    var imgDatas="["
    if attachData.count != 0{
      for i in 0..<attachData.count{
        if(attachData[i] != [String:String]())
        {
          imgDatas+=attachData[i].convtToJson() as String
          if i != attachData.count-1{
            imgDatas+=","
          }
        }
        
      }
    }
    if  imgDatas.last == "," {
      imgDatas =  imgDatas.removeLast().description
    }
    imgDatas+="]"
    print(imgDatas)
    
    param["image"] = imgDatas
    param["id"] = id
    param["message"] = messageToSend
    param["customer_id"] = custId
    param["store_id"] = storeId
    param["page"] = "1"
    param["subject"] = "Message"
    
    print(param)
    self.sendRequest(url: "mobimessaging/admincompose", params: param)
    
  }
  
  func image_picker()
  {
    print("browseButtonPressed")
    picker.delegate=self
    picker.allowsEditing = false
    picker.sourceType = .photoLibrary
    self.present(picker, animated: true, completion: nil)
  }
  
  func browseButtonPressed(){
    let doc=UIDocumentMenuViewController(documentTypes: ["public.text","public.content","public.image","public.data"], in: .import)
    //        let doc = UIDocumentMenuViewController(documentTypes: [String(kUTTypeImage),String(kUTTypePDF),String(kUTTypeArchive)], in: .import)
    doc.delegate=self
    doc.modalPresentationStyle = .popover
    self.present(doc, animated: true, completion: nil)
  }
  
  @objc func removeButtonTapped(_ sender: UIButton) {
    
    attachData.remove(at: sender.tag)
    messageViewTable.reloadSections([1], with: .automatic)
  }
}

extension sentBoxReplyController: UITableViewDelegate,UITableViewDataSource {
  
  func numberOfSections(in tableView: UITableView) -> Int {
    return 2
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
    if section == 1 {
      if attachData.count == 0 {return 0}
      else {return attachData.count }
    } else {
      return messageViewData.count
    }
    
  }
  
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
//    if indexPath.section == 1 {
//      let cell = tableView.dequeueReusableCell(withIdentifier: "adminAttachmentCell", for: indexPath) as! adminAttachmentCell
//      cell.attachmentNameLabel.text = attachData[indexPath.row]["name"]
//      cell.removeButton.tag = indexPath.row
//      cell.removeButton.addTarget(self, action: #selector(removeButtonTapped(_:)), for: .touchUpInside)
//      cell.selectionStyle = .none
//      return cell
//    }
//    else {
//      let cell = tableView.dequeueReusableCell(withIdentifier: "messageViewCell", for: indexPath) as! messageViewCell
//      cell.messageLabel.text = self.messageViewData[indexPath.row]["message"]?.replacingOccurrences(of: "<[^>]+>", with: "", options: .regularExpression, range: nil)
//      cell.senderNameLabel.text = self.messageViewData[indexPath.row]["sender_name"]
//      cell.dateTimestampLabel.text = self.messageViewData[indexPath.row]["created_at"]
//      cell.messageLabel.numberOfLines = 0
//      cell.messageView.layer.cornerRadius = 10.0
//
//      if messageViewData[indexPath.row]["sender"] == "admin" {
//        cell.messageViewTrailing.constant = 50
//        cell.messageViewLeading.constant = 0
//        cell.messageView.backgroundColor = UIColor.init(hexString: "#E0EFCC")
//      } else {
//        cell.messageViewLeading.constant = 50
//        cell.messageViewTrailing.constant = 0
//        cell.messageView.backgroundColor = UIColor.init(hexString: "#B8E1F3")
//      }
//
//      cell.selectionStyle = .none
//      return cell
//    }
    return UITableViewCell()
  }
  
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    
    if indexPath.section == 1 {
      if attachData.count == 0 {return 0}
      else {return 50}
    } else {
      return UITableView.automaticDimension
    }
  }
  
  /*  func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
   
   let currentOffset = scrollView.contentOffset.y
   let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
   
   if (maximumOffset - currentOffset) <= 10 {
   currentPage += 1
   self.sendRequest(url: "mobimessaging/adminview", params: ["customer_id":customerId,"id":id,"page":"\(currentPage)"], store: true)
   }
   } */
  
  
  
}

extension sentBoxReplyController: UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIDocumentMenuDelegate,UIDocumentPickerDelegate{
  
  func documentMenu(_ documentMenu: UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
    documentPicker.delegate=self
    documentPicker.modalPresentationStyle = .fullScreen
    self.present(documentPicker, animated: true, completion: nil)
  }
  
  func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
    
    let fileName=url.absoluteString.components(separatedBy: "/")
    // docUrlLabel.text=url.absoluteString
    print("didPickDocumentAt")
    // FileSelectedOrNot.text = fileName[0]
    
    let isSecuredURL = url.startAccessingSecurityScopedResource() == true
    let coordinator = NSFileCoordinator()
    var error: NSError? = nil
    coordinator.coordinate(readingItemAt: url, options: [], error: &error) { (url) -> Void in
      do{
        let temp=try Data(contentsOf: url)
        //self.trainingDocData=temp.base64EncodedString()
        
        let temp1=fileName[fileName.count-1].components(separatedBy: ".")
        print("The file you have choosed is not in correct format")
        //  print(temp1)
        if (temp1[1] == "pdf" || temp1[1] == "zip" )
        {
          print(temp1)
          let dataPdf = temp.base64EncodedString()
          self.attachData.append(["name":"xyz.pdf", "base64_encoded_data": dataPdf])
        }
        else
        {
          let msg = "The file you have choosed is not in correct format"
          print(msg)
          return
          
        }
      }catch let error{
        
        print(error.localizedDescription)
        
      }
    }
    if (isSecuredURL) {
      url.stopAccessingSecurityScopedResource()
    }
    print(url)
    controller.dismiss(animated: true, completion: nil)
  }
  
  func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
    picker.dismiss(animated: true)
    
    if let image = info[.originalImage] as? UIImage {
      let imgData=image.pngData()
      let imageData=(imgData?.base64EncodedString())!
      let values = info[UIImagePickerController.InfoKey.referenceURL] as? NSURL
      let imgName = values?.lastPathComponent
      
      /*let image = info[UIImagePickerControllerOriginalImage] as? UIImage
       let imgsize:NSData = UIImagePNGRepresentation(image!)! as NSData
       let fileSize = Double(imgsize.length / 1024 / 1024)
       totalSize += fileSize
       if totalSize > 5.00 {
       picker.dismiss(animated: true, completion: nil)
       self.view.makeToast("Please upload a file of less than 5 Mb", duration: 2.0, position: .center)
       }else{ */
      
      attachData.append(["name":imgName ?? "xyz.png", "base64_encoded_data": imageData])
      messageViewTable.reloadData()
      picker.dismiss(animated: true, completion: nil)
    }
    
  }
  
  //
  //    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
  //        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
  //
  //
  //            let imgData=UIImagePNGRepresentation(image)
  //            let imageData=(imgData?.base64EncodedString())!
  //            let values = info[UIImagePickerControllerReferenceURL] as? NSURL
  //            let imgName = values?.lastPathComponent
  //            let image = info[UIImagePickerControllerOriginalImage] as? UIImage
  //            let imgsize:NSData = UIImagePNGRepresentation(image!)! as NSData
  //           /* let fileSize = Double(imgsize.length / 1024 / 1024)
  //            totalSize += fileSize
  //            if totalSize > 5.00 {
  //                picker.dismiss(animated: true, completion: nil)
  //                self.view.makeToast("Please upload a file of less than 5 Mb", duration: 2.0, position: .center)
  //            }else{ */
  //                attachData.append(["name":imgName ?? "xyz.png", "base64_encoded_data": imageData])
  //            makeAttachmentTableView()
  //                picker.dismiss(animated: true, completion: nil)
  //
  //        }
  //    }
  
  func documentMenuWasCancelled(_ documentMenu: UIDocumentMenuViewController) {
    documentMenu.dismiss(animated: true, completion: nil)
  }
  
  func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
    controller.dismiss(animated: true, completion: nil)
  }
  
  func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
    picker.dismiss(animated: true, completion: nil)
  }
  
  
  func makeAttachmentTableView() {
    //        attachmentTable.delegate = self
    //        attachmentTable.dataSource = self
    //        attachmentTableHeight.constant = 100
    messageViewTable.reloadData()
  }
  
  
}
