//
//  HomeDealTC.swift
//  MageNative Magento Platinum
//
//  Created by Hamza Usmani on 02/09/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class HomeDealTC: UITableViewCell {
    
    //MARK:- Properties
    
    static var reuseId:String = "HomeDealTC"
    var dealData: [HomeDealContent]? = nil
    weak var parent: UIViewController?
    var duration: Int? = nil
    var timer: Timer?
    
    
    lazy var topHeading:UILabel = {
        let label = UILabel()
        label.textColor = UIColor.mageLabel
        label.text = "Deals For You   >"
        label.font = .systemFont(ofSize: 16, weight: .semibold)
        return label
    }()
    
    lazy var timerLabel:UILabel = {
        let label = UILabel()
        label.textColor = .mageSecondaryLabel
        label.font = .systemFont(ofSize: 12, weight: .medium)
        return label
    }()
    
    lazy var dealCollection:UICollectionView = {
        let layout              = UICollectionViewFlowLayout()
        layout.scrollDirection  = .horizontal
        let collectionView      = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.backgroundColor = UIColor.clear
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.register(HomeDealCC.self, forCellWithReuseIdentifier: HomeDealCC.reuseId)
        collectionView.delegate     = self
        collectionView.dataSource   = self
        return collectionView
    }()
    
    //MARK:- Lifecycle
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = .clear
        selectionStyle  = .none
        contentView.isMultipleTouchEnabled  = true
        
        addSubview(topHeading)
        topHeading.anchor(top: topAnchor, left: leadingAnchor, right: trailingAnchor, paddingTop: 8, paddingLeft: 8, paddingRight: 8, height: 20)
        
        addSubview(timerLabel)
        timerLabel.centerY(inView: topHeading)
        timerLabel.anchor(right: trailingAnchor, paddingRight: 16)
        
        addSubview(dealCollection)
        dealCollection.anchor(top: topHeading.bottomAnchor, left: leadingAnchor, bottom: bottomAnchor, right: trailingAnchor, paddingTop: 5, paddingLeft: 8,
                              paddingBottom: 8 ,paddingRight: 8)
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        //timer?.invalidate()
        //timer = nil
    }
    
    //MARK:- Selectors
    
    @objc func getDealTime() {
        guard let duration = duration else { return }
        let time = duration / 1000
        let formatter = DateComponentsFormatter()
        formatter.allowedUnits = [.day, .hour, .minute, .second]
        formatter.unitsStyle = .abbreviated
        
        let formattedString = formatter.string(from: TimeInterval(time))!
        DispatchQueue.main.async {
            self.timerLabel.text = "\(formattedString) Remaining"
            
        }
        
        self.duration = duration - 1000
    }
    
    
    //MARK:- Helper Functions
    
    func populate(with data: [HomeDealContent], headingText: String, parent: UIViewController, dur: Int) {
        dealData = data
        topHeading.text = headingText
        self.parent = parent
        dealCollection.reloadData()
        
        if self.duration == nil && timer == nil {
            self.duration = dur
            getTime()
        }
    }
    
    func getTime() {
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(getDealTime), userInfo: nil, repeats: true)
        RunLoop.current.add(self.timer!, forMode: .common)
    }
    
    
}

//MARK:- UICollectionViewDataSource

extension HomeDealTC: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dealData?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: HomeDealCC.reuseId, for: indexPath) as! HomeDealCC
        cell.populate(with: dealData?[indexPath.item].deal_image_name)
        return cell
    }
    
    
}

//MARK:- UICollectionViewDelegateFlowLayout/UICollectionViewDelegate

extension HomeDealTC: UICollectionViewDelegateFlowLayout, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return .init(width: collectionView.frame.width/2 - 15, height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch dealData?[indexPath.item].link_to {
        case "product":
            let vc = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "productSinglePageViewController") as! ProductSinglePageViewController
            vc.product_id = dealData?[indexPath.item].product_id ?? ""
            parent?.navigationController?.pushViewController(vc, animated: true)
        default:
            return
        }
    }
}
