/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */


import UIKit

class cedMageMainDrawer: cedMageViewController,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet weak var visualEffect: UIVisualEffectView!
    @IBOutlet weak var mainTable: UITableView!
    var Stores = [[String:String]]()
    var cmsPages = [[String:String]]()
    var dataSource = expandingCells()
    @IBOutlet weak var blurView: UIImageView!
    var previouslySelectedHeaderIndex: Int?
    var selectedHeaderIndex: Int?
    var selectedItemIndex: Int?
     var accountAraayToShow = ["Home","My Shopping Bag","My Account","My Wishlist", "Scan Products","My Stores","Visit Sellers","Invite Friends"]
      
    var AccountImages = [UIImage(named: "home")?.withRenderingMode(.alwaysTemplate),UIImage(named: "bag")?.withRenderingMode(.alwaysTemplate),UIImage(named: "user")?.withRenderingMode(.alwaysTemplate),UIImage(named: "Heart")?.withRenderingMode(.alwaysTemplate),UIImage(named: "barcode")?.withRenderingMode(.alwaysTemplate),UIImage(named: "MyStore")?.withRenderingMode(.alwaysTemplate),UIImage(named: "MyStore")?.withRenderingMode(.alwaysTemplate),UIImage(named: "invitation")?.withRenderingMode(.alwaysTemplate)]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mainTable.delegate = self
        mainTable.dataSource = self
        loadDrawerData()
        NotificationCenter.default.addObserver(self, selector: #selector(cedMageMainDrawer.loadFromNotif(_:)), name: NSNotification.Name(rawValue: "loadDrawerAgain"), object: nil);
        
        
        
        
    }
    
    @objc func loadFromNotif(_ notification: NSNotification){
        Stores = [[String:String]]()
        cmsPages = [[String:String]]()
        dataSource = expandingCells()
        loadDrawerData()
    }
    
    func loadDrawerData(){
        let param=["theme":"5"]
        self.sendRequest(url: "mobiconnectadvcart/category/getallcategories", params: param)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let value=UserDefaults.standard.value(forKey: "mageAppLang") as! [String]
        if value[0]=="ar"
        {
            self.sideDrawerViewController?.drawerDirection = .Right
        }
        else
        {
            self.sideDrawerViewController?.drawerDirection = .Left
        }
        mainTable.reloadData()
    }
    
    override func recieveResponse(data: Data?, requestUrl: String?, response: URLResponse?) {
        if let data  = data {
            print(requestUrl ?? "")
            guard let json = try? JSON(data: data) else{return;}
            print("!@#$%^&*()_+\(json)")
            if (requestUrl == "mobiconnectstore/getlist") {
                self.Stores.removeAll()
                
                print("^*^%^&%^&%&^%^&\(json)")
                for store in  json[0]["store_data"].arrayValue{
                    let group_id = store["group_id"].stringValue
                    //print(group_id)
                    let code = store["code"].stringValue
                    let store_id = store["store_id"].stringValue
                    let name = store["name"].stringValue
                    let is_active = store["is_active"].stringValue
                    let storeobject = ["group_id":group_id,"code":code,"store_id":store_id,"name":name,"is_active":is_active]
                    self.Stores.append(storeobject)
                }
                self.showStores()
            }
            else if(requestUrl == "mobiconnectadvcart/category/getallcategories"){
                print(json)
                self.parse_product(json: json, index: "categories")
            }
            else if(requestUrl == "mobiconnectcms/cms/getCmsPages"){
                let data = json
                if(data["data"]["status"].stringValue == "success"){
                    for page in data["data"]["cmsblocks"].arrayValue {
                        var temp = [String:String]()
                        temp["pageTitle"] = page["page-title"].stringValue
                        temp["pageUrl"]  = page["page-url"].stringValue
                        temp["pageId"]  = page["page-id"].stringValue
                        cmsPages.append(temp)
                    }
                }
                mainTable.reloadData()
            }else if (requestUrl?.contains("logout"))!{
                LoginManager().logOut()
                GIDSignIn.sharedInstance().signOut()
                self.defaults.set(false, forKey: "isLogin");
                self.defaults.removeObject(forKey: "userInfoDict");
                self.defaults.removeObject(forKey: "selectedAddressId");
                defaults.setValue("Magenative App For Magento 2", forKey: "name")
                UserDefaults.standard.set(nil,forKey: "LIAccessToken")
                UserDefaults.standard.synchronize();
                defaults.synchronize()
                self.defaults.removeObject(forKey: "selectedAddressDescription");
                self.defaults.removeObject(forKey: "cartId");
                self.defaults.removeObject(forKey: "cart_summary");
                self.defaults.removeObject(forKey: "guestEmail");
                self.defaults.set("0", forKey: "items_count");
                self.defaults.removeObject(forKey: "name")
                self.setCartCount(view: self, items: "0")
                self.navigationController?.navigationBar.isHidden = false
                /*if let viewController = UIStoryboard(name: "homeLayouts", bundle: nil).instantiateViewController(withIdentifier: "baseHomepageController") as? baseHomepageController {
                    self.navigationController?.setViewControllers([viewController], animated: true)
                } */
                navigationController?.setViewControllers([HomeController()], animated: true)
                
                let vc=cedMageMainDrawer()
                let table = vc.mainTable;
                table?.reloadData();
                NotificationCenter.default.post(name: NSNotification.Name("loadDrawerAgain"), object: nil)
            }else{
                
                if requestUrl?.contains("setstore") ?? false{
                    
                    let lang=json[0]["locale_code"].stringValue
                    let language=lang.components(separatedBy: "_")
                    if language[0]=="ar"
                    {
                        UserDefaults.standard.set(["ar","en","fr"], forKey: "mageAppLang")
                        UIView.appearance().semanticContentAttribute = .forceRightToLeft
                    }
                    else
                    {
                        UserDefaults.standard.set([language[0],"ar","fr"], forKey: "mageAppLang")
                        UIView.appearance().semanticContentAttribute = .forceLeftToRight
                    }
                }
                let view = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "mainView") as? cedMageSideDrawer
                view?.modalPresentationStyle = .fullScreen;
                self.present(view!, animated: true, completion: nil)
            }
        }
    }
    var otherRows=["VIEW ALL CATEGORIES".localized]
    func parse_product(json:JSON,index:String){
        var i = 0
        
        for valuee in otherRows {
            dataSource.append(expandingCells.headerValues(value: valuee, image: "", cate: "", haschild: "false"))
        }
        
        
        for (_,subJson) in json[0]["data"]["categories"]{//[0] {
            
            let maincatename = subJson["main_category_name"].stringValue
            let maincateimage = subJson["main_category_image"].stringValue
            let maincateId = subJson["main_category_id"].stringValue
            let hasChildd = subJson["has_child"].stringValue
            dataSource.append(expandingCells.headerValues(value: maincatename, image: maincateimage, cate: maincateId, haschild: hasChildd))
            print(hasChildd)
            if(hasChildd == "true")
            {
                var temp = [[String:String]]()
                for (_,subcategory)  in subJson["sub_cats"]{
                    
                    let subCateId = subcategory["sub_category_id"].stringValue
                    let subcateName = subcategory["sub_category_name"].stringValue
                    let subCateImage = subcategory["sub_category_image"].stringValue
                    let hasChildd = subcategory["has_child"].stringValue
                    dataSource.append(expandingCells.cell(value: subcateName, imageUrl: subCateImage, cateId: subCateId, hasChild: hasChildd))
                }
                temp.removeAll()
            }
             i += 1
        }
        mainTable.reloadData()
        return
    }
    
    func loadBlur(){
        
        if !UIAccessibility.isReduceTransparencyEnabled {
            
            let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.light)
            let blurEffectView = UIVisualEffectView(effect: blurEffect)
            blurEffectView.frame = self.mainTable.bounds
            blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            let effect = UIVisualEffectView(effect: UIBlurEffect(style: UIBlurEffect.Style.light))
            effect.frame = blurView.frame
            effect.autoresizingMask = [.flexibleWidth,.flexibleHeight]
            blurView.addSubview(effect)
        }
    }
    func imageWithView(view:UIView)->UIImage{
        UIGraphicsBeginImageContextWithOptions(view.bounds.size, false, 0.0)
        view.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        
        if section == 0{
            return 1
        }else if section == 1{
            return dataSource.totalitems.count
        }else if section == 2{
//            Account section
            return accountAraayToShow.count
        }else if section == 3{
            return 1
        }else{
            return 0
        }
    }
    
    func gotoAccount(){
        self.sideDrawerViewController?.toggleDrawer()
        guard let tabbar = sideDrawerViewController?.mainViewController as? cedMageTabbar else { return }
        guard let navigation = tabbar.viewControllers?[tabbar.selectedIndex] as? UINavigationController else { return }//self.sideDrawerViewController?.mainViewController as! cedMageHomeNavigation
        if(defaults.bool(forKey: "isLogin")){
            let viewcontroll = UIStoryboard(name: "cedMageAccounts", bundle: nil).instantiateViewController(withIdentifier: "accountSec") as! cedMageAccounts
            navigation.pushViewController(viewcontroll, animated: true)
        }else{
           // let viewcontroll = UIStoryboard(name: "cedMageLogin", bundle: nil).instantiateViewController(withIdentifier: "cedMageLogin") as! cedMageLogin
            //navigation.pushViewController(viewcontroll, animated: true)
            navigation.pushViewController(LoginController(), animated: true)
            
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if(indexPath.section == 0){
            
            let cell = mainTable.dequeueReusableCell(withIdentifier: "mainProfile")
            
            let name = defaults.value(forKey: "name")  as? String
            
            if(name  != ""){
                let label = cell?.viewWithTag(1258) as? UILabel
                let imageView = cell?.viewWithTag(1222) as? UIImageView
                
                // imageView?.image = UIImage(named: "login_And-logo")
                imageView?.layer.cornerRadius = ((imageView?.frame.width)!/2)
                imageView?.layer.borderWidth = 2
                if let color = cedMage.getInfoPlist(fileName: "cedMage", indexString: "themeColor") as? String {
                    imageView?.layer.borderColor = cedMage.UIColorFromRGB(colorCode: color).cgColor
                }
                let name = defaults.value(forKey: "name") as? String
                label?.fontColorTool()
                if(name != nil){
                    label?.text =  name!
                    
                }
                else
                {
//                    let infoDictionary: NSDictionary = Bundle.main.infoDictionary as NSDictionary!
//                    let appName: NSString = infoDictionary.object(forKey: "CFBundleName") as! NSString
                    label?.text="GUEST";
                }
            }
            let accountButton = cell?.viewWithTag(1265) as? UILabel
            let value=UserDefaults.standard.value(forKey: "mageAppLang") as! [String]
            if value[0]=="ar"
            {
                accountButton?.textAlignment = .right
                //accountButton?.titleLabel?.textAlignment = .right
            }
            else
            {
                accountButton?.textAlignment = .left
                //accountButton?.titleLabel?.textAlignment = .left
            }
            accountButton?.fontColorTool()
            if(defaults.bool(forKey: "isLogin")){
                accountButton?.text="My Account".localized
            }else{
                accountButton?.text="Login/SignUp".localized
            }
            
            return cell!
        }else if (indexPath.section == 1){
            let item = self.dataSource.totalitems[(indexPath as NSIndexPath).row]
            let value = item.textValue
            _ = item.imageUrl
            
            if let cell = tableView.dequeueReusableCell(withIdentifier: "account_cell") as? accountcell {
                
                if item as? expandingCells.headerValues != nil {
                    let label = cell.viewWithTag(123) as? UILabel
                    label?.text = value
                    label?.fontColorTool()
                    cell.ImageView.isHidden = true
                    //label?.text = value
                    label?.font = UIFont.systemFont(ofSize: 14)
                    if item.hasChild != "false" {
                        let value=UserDefaults.standard.value(forKey: "mageAppLang") as! [String]
                        cell.rightView.isHidden = false
                        let img1=cell.rightView.image
                        if value[0]=="ar"
                        {
                            cell.rightView.image=UIImage(cgImage: (img1?.cgImage)!, scale: (img1?.scale)!, orientation: UIImage.Orientation.upMirrored)
                        }
                        else
                        {
                            cell.rightView.image=UIImage(cgImage: (img1?.cgImage)!, scale: (img1?.scale)!, orientation: UIImage.Orientation.downMirrored)
                        }
                        if #available(iOS 13.0, *) {
                            cell.cell_view.backgroundColor = .systemBackground
                        } else {
                            cell.cell_view.backgroundColor = .white
                        }
                    }else{
                        cell.rightView.isHidden = true
                        if  item.textValue=="VIEW ALL CATEGORIES"{
//                            cell.cell_view.setThemeColor()
                            if #available(iOS 13.0, *) {
                                label?.textColor=UIColor.label
                            } else {
                                label?.textColor=UIColor.black
                            }
                            cell.lineView.isHidden=true
                        }else{
//                             label?.textColor=UIColor.black
                            if #available(iOS 13.0, *) {
                                cell.cell_view.backgroundColor = .systemBackground
                            } else {
                                cell.cell_view.backgroundColor = .white
                            }
                            cell.lineView.isHidden=false
                        }
                    }
                    return cell
                    
                }else{
                    let subMenuCell = tableView.dequeueReusableCell(withIdentifier: "subMenuCell") as? accountcell
                    let label = subMenuCell?.viewWithTag(1214) as? UILabel
                    label?.text = value
                    label?.fontColorTool()
                    
                    return subMenuCell!
                }
                
            }
        }else if indexPath.section == 2{
            if let cell = tableView.dequeueReusableCell(withIdentifier: "account_cell") as?  accountcell {
//                cell.cell_view.setThemeColor()
                let label = cell.viewWithTag(123) as? UILabel
                label?.font = UIFont.systemFont(ofSize: 15)
                label?.text = accountAraayToShow[indexPath.row]
                if #available(iOS 13.0, *) {
                    label?.textColor = .label
                } else {
                    label?.textColor = .black
                }
                cell.ImageView.isHidden = false
                cell.ImageView.image = AccountImages[indexPath.row]
                if #available(iOS 13.0, *) {
                    cell.ImageView.tintColor = .label
                } else {
                    cell.ImageView.tintColor = .black
                }
                cell.rightView.isHidden = true
                return cell
            }
        }
        else if(indexPath.section == 3){
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell")
            let footerView = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 70))
            //footerView.backgroundColor = .white
            footerView.text = "APP VERSION  " + (Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? "1.0" ) + "\n © "+(Bundle.main.object(forInfoDictionaryKey: "CFBundleName") as? String ?? "")
            footerView.contentMode = .center
            footerView.textAlignment = .center
            footerView.numberOfLines=0
            footerView.font = UIFont(fontName: "Montserrat-Bold", fontSize: 11.0)
            cell?.contentView.addSubview(footerView)
            return cell!;
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(indexPath.section == 0){
            if(indexPath.row == 1){
                return 35
            }
            return 106
        }else if (indexPath.section == 1) {
            let item = self.dataSource.totalitems[(indexPath as NSIndexPath).row]
            if item is expandingCells.headerValues {
                return 45
            } else if (item.hidden) {
                return 0
            } else {
                return 35
            }
        }else if(indexPath.section == 3){
            return 60
        } else {
            return 40
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let tabbar = sideDrawerViewController?.mainViewController as? cedMageTabbar else { return }
        guard let navigation = tabbar.viewControllers?[tabbar.selectedIndex] as?  UINavigationController else { return }// cedMageHomeNavigation
        
        if(indexPath.section == 0){
            gotoAccount()
            return
        }
        else if(indexPath.section == 1){
            let item = self.dataSource.totalitems[(indexPath as NSIndexPath).row]
            print(item)
            if item is expandingCells.headerValues {
                if item.hasChild == "false"{
                    self.sideDrawerViewController?.toggleDrawer()
                    if item.textValue=="Change Your Location"{
                        print("Change Your Location")
                        self.sendRequest(url:  "mobiconnectstore/getlist", params: nil)
                        return
                    }
                    else if item.textValue=="VIEW ALL CATEGORIES"{
                        navigation.pushViewController(TreeViewController(), animated: true)
                        return
                    }
                    if let viewController =  UIStoryboard(name: "categorylayouts", bundle: nil).instantiateViewController(withIdentifier: "cedMageDefaultCollection") as?  cedMageDefaultCollection {
                        viewController.selectedCategory = item.categoryId
                        navigation.pushViewController(viewController, animated: true)
                    }
                    return
                }
                let cell = mainTable.cellForRow(at: indexPath) as? accountcell
                if self.selectedHeaderIndex == nil {
                    self.selectedHeaderIndex = (indexPath as NSIndexPath).row
                } else {
                    self.previouslySelectedHeaderIndex = self.selectedHeaderIndex
                    self.selectedHeaderIndex = (indexPath as NSIndexPath).row
                }
                if let previouslySelectedHeaderIndex = self.previouslySelectedHeaderIndex {
                    cell?.rightView.image = UIImage(named:"IQButtonBarArrowRight")
                    let indexPath = NSIndexPath(row: previouslySelectedHeaderIndex, section: 1)
                    let prevcell = self.mainTable.cellForRow(at: indexPath as IndexPath) as? accountcell
                    let value=UserDefaults.standard.value(forKey: "mageAppLang") as! [String]
                    //cell?.rightView.isHidden = false
                    let img1=UIImage(named:"IQButtonBarArrowRight")
                    if value[0]=="ar"
                    {
                        cell?.rightView.image=UIImage(cgImage: (img1?.cgImage)!, scale: (img1?.scale)!, orientation: UIImage.Orientation.upMirrored)
                        prevcell?.rightView.image=UIImage(cgImage: (img1?.cgImage)!, scale: (img1?.scale)!, orientation: UIImage.Orientation.upMirrored)
                    }
                    else
                    {
                        cell?.rightView.image=UIImage(cgImage: (img1?.cgImage)!, scale: (img1?.scale)!, orientation: UIImage.Orientation.downMirrored)
                        prevcell?.rightView.image=UIImage(cgImage: (img1?.cgImage)!, scale: (img1?.scale)!, orientation: UIImage.Orientation.downMirrored)
                    }
                    prevcell?.lineView.isHidden = false
                    cell?.lineView.isHidden = false
                    self.dataSource.collapse(previouslySelectedHeaderIndex)
                }
                
                if self.previouslySelectedHeaderIndex != self.selectedHeaderIndex {
                    cell?.rightView.image = UIImage(named:"IQButtonBarArrowDown")
                    cell?.lineView.isHidden = true
                    self.dataSource.expand(self.selectedHeaderIndex!)
                } else {
                    self.selectedHeaderIndex = nil
                    self.previouslySelectedHeaderIndex = nil
                }
                self.mainTable.beginUpdates()
                self.mainTable.endUpdates()
                
            } else {
                self.sideDrawerViewController?.toggleDrawer()
                if let viewController =  UIStoryboard(name: "categorylayouts", bundle: nil).instantiateViewController(withIdentifier: "cedMageDefaultCollection") as?  cedMageDefaultCollection {
                    viewController.selectedCategory = item.categoryId
                    navigation.pushViewController(viewController, animated: true)
                }
                
                if (indexPath as NSIndexPath).row != self.selectedItemIndex {
                    if let selectedItemIndex = self.selectedItemIndex {
                        let previousCell = self.mainTable.cellForRow(at: IndexPath(row: selectedItemIndex, section: 0))
                        previousCell?.accessoryType = UITableViewCell.AccessoryType.none
                    }
                    self.selectedItemIndex = (indexPath as NSIndexPath).row
                }
            }
            
        }else{
            self.sideDrawerViewController?.toggleDrawer()
            let tabButton = tableView.cellForRow(at: indexPath) as? accountcell
            performClickEvent(buttonText: (tabButton?.contentView.subviews[0].subviews[0] as? UILabel)?.text ?? "")
            
            
//            if indexPath.row==1{
//                 self.sendRequest(url:  "mobiconnectstore/getlist", params: nil)
//            }else
//            if indexPath.row==0{
//                if let vc=UIStoryboard(name: "cedMageAccounts", bundle: nil).instantiateViewController(withIdentifier: "qrscan1") as? cedMageBarCodeScanner{
//                    navigation.pushViewController(vc, animated: true)
//                }
//                return
//            }
//            else if indexPath.row==2{
//                let url =  "";
//
//                let vc = UIActivityViewController(activityItems: [url], applicationActivities: nil)
//                if(UIDevice().model.lowercased() == "ipad".lowercased()){
//                    if let view = self.sideDrawerViewController?.mainViewController  as? cedMageHomeNavigation{
//                            vc.popoverPresentationController?.sourceView = view.view
//                            vc.popoverPresentationController?.sourceRect = CGRect(x: view.view.bounds.midX, y: view.view.bounds.midY, width: 0, height: 0)
//                        view.present(vc, animated: true, completion: nil);
//                    }
//                }
//                else {
//                    self.present(vc, animated: true, completion: nil);
//
//                }
//
//            }else{
//                akeWarningAlert()
//            }
        }
        
    }
    
    
    
    
    func performClickEvent(buttonText:String){
        let navigation = self.sideDrawerViewController?.mainViewController as! cedMageHomeNavigation
//        ["Home","My Shopping Bag","My Wishlist","My Account","My Stores","Invite Friends","Scan Products"]
        switch buttonText {
        case "Home":
            /*if let viewController = UIStoryboard(name: "homeLayouts", bundle: nil).instantiateViewController(withIdentifier: "baseHomepageController") as? baseHomepageController {
                navigation.setViewControllers([viewController], animated: true)
            } */
            navigation.setViewControllers([HomeController()], animated: true)
        case "My Shopping Bag":
            print(buttonText)
            let viewcontroll = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "cartViewController") as! CartViewController
            navigation.pushViewController(viewcontroll, animated: true)
            
        case "My Wishlist":
            print(buttonText)
            if(defaults.bool(forKey: "isLogin")){
                let viewcontroll = UIStoryboard(name: "cedMageAccounts", bundle: nil).instantiateViewController(withIdentifier: "mywishlist") as! cedMageWishList
                navigation.pushViewController(viewcontroll, animated: true)
            }else{
                //let viewcontroll = UIStoryboard(name: "cedMageLogin", bundle: nil).instantiateViewController(withIdentifier: "cedMageLogin") as! cedMageLogin
                navigation.pushViewController(LoginController(), animated: true)
                
            }
            
        case "My Account":
            print(buttonText)
            gotoAccount()
        case "My Stores":
            print(buttonText)
            self.sendRequest(url:  "mobiconnectstore/getlist", params: nil)
        case "Invite Friends":
            print(buttonText)
        //  self.sendRequest(url:  "mobiconnectstore/getlist", params: nil)
        case "Visit Sellers":
            let viewcontroll = UIStoryboard(name: "categorylayouts", bundle: nil).instantiateViewController(withIdentifier: "cedMageVendorShops") as! cedMageVendorShops
            navigation.pushViewController(viewcontroll, animated: true)
        case "Scan Products":
            print(buttonText)
            if let vc=UIStoryboard(name: "cedMageAccounts", bundle: nil).instantiateViewController(withIdentifier: "qrscan1") as? cedMageBarCodeScanner{
                navigation.pushViewController(vc, animated: true)
            }
        default:
            print(buttonText)
        }
    }
    
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 1 {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "account_cell") as? accountcell {
                cell.contentView.setThemeColor()
                cell.rightView.isHidden = true
                cell.lineView.isHidden = true
                cell.ImageView.image = UIImage(named:"")
                cell.label.text = "SHOP BY CATEGORIES"
                cell.label.font = UIFont.boldSystemFont(ofSize: 17)
                cell.label.textColor = Settings.themeTextColor
                cell.setThemeColor()
                cell.cell_view.setThemeColor()
                return cell.contentView
            }
        }else if section == 2{
            if let cell = tableView.dequeueReusableCell(withIdentifier: "account_cell") as? accountcell {
                cell.rightView.isHidden = true
                cell.lineView.isHidden = true
                cell.contentView.setThemeColor()
                cell.ImageView.image = UIImage(named:"")
                cell.label.text = "Your Account".localized
                cell.label.font = UIFont.boldSystemFont(ofSize: 17)
                cell.label.textColor = Settings.themeTextColor
                cell.setThemeColor()
                cell.cell_view.setThemeColor()
                return cell.contentView
            }
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 1 {
            return 45
        }else if section == 2{
            return 45
        }
        return 0
    }
    
    
    
    //Mark:Show Stores
    func showStores(){
        let actionsheet = UIAlertController(title: "Select Store".localized, message: nil, preferredStyle: .actionSheet)
        
        for buttons in self.Stores {
            actionsheet.addAction(UIAlertAction(title: buttons["name"], style: UIAlertAction.Style.default,handler: {
                action -> Void in
                self.selectStore(store: buttons["store_id"])
            }))
        }
        actionsheet.addAction(UIAlertAction(title: "Cancel".localized, style: UIAlertAction.Style.cancel, handler: {
            action -> Void in
        }))
        if(UIDevice().model.lowercased() == "iPad".lowercased()){
            actionsheet.popoverPresentationController?.sourceView = self.mainTable.cellForRow(at: IndexPath(row: 5, section: 1))
            
        }
        self.present(actionsheet, animated: true, completion: nil)
    }
    
    func selectStore(store:String?){
        //let params = ["store_id":store! as String]
        defaults.setValue(store, forKey: "storeId")
        self.sendRequest(url: "mobiconnectstore/setstore/"+store!, params: nil)
    }
    
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 70))
        //footerView.backgroundColor = .white
        footerView.text = "APP VERSION  " + (Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? "1.0" ) + "\n © "+(Bundle.main.object(forInfoDictionaryKey: "CFBundleName") as? String ?? "")
        footerView.contentMode = .center
        footerView.textAlignment = .center
        footerView.numberOfLines=0
        footerView.font = UIFont(fontName: "Montserrat-Bold", fontSize: 14.0)
        return footerView
    }
    
    // set height for footer
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    //    MARK: LOGOUT FUNCTION
    
    
    func akeWarningAlert() {
        let alert = UIAlertController(title: "", message: "Are You Sure You Want To logout ", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Yes", style: .destructive , handler:{ (UIAlertAction)in
            print("User click Delete button")
            self.doLogout()
        }))
        
        alert.addAction(UIAlertAction(title: "No", style: .default, handler:{ (UIAlertAction)in
            print("User click Dismiss button")
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    
    @objc func doLogout(){
        if(defaults.bool(forKey: "isLogin") == true)
        {
            let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
            let hashKey = userInfoDict["hashKey"]!;
            let customerId = userInfoDict["customerId"]!;
            let postString = ["hashkey":hashKey,"customer_id":customerId];
            self.sendRequest(url: "mobiconnect/customer/logout", params: postString)
        }
    }
    
    
}

