//
//  myRmaListingViewController.swift
//  MageNative Magento Platinum
//
//  Created by Macmini on 13/12/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class myRmaListingViewController: MagenativeUIViewController ,UITableViewDelegate, UITableViewDataSource{
   
    @IBOutlet weak var mainTableView: UITableView!
     var rmaData = [[String:String]]()
        var custid = String()
        override func viewDidLoad() {
            super.viewDidLoad()
            
            self.navigationController?.navigationBar.isHidden = false
            
            
            mainTableView.delegate = self
            mainTableView.dataSource = self
            mainTableView.separatorStyle = .none
            // Do any additional setup after loading the view.
            if defaults.bool(forKey: "isLogin") {
                userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
                if let custId = userInfoDict["customerId"] {
                    custid = custId
                    self.sendRequest(url: "mobiconnect/mobirma/listrma", params: ["customer_id":custId] , store: false)
                }
            }
        }
    override func recieveResponse(data: Data?, requestUrl: String?, response: URLResponse?) {
        if let data = data {
            do {
            var json = try JSON(data:data)
            json  = json[0]
            if requestUrl  == "mobiconnect/mobirma/cancelrma" {
                    print(json)
                if json["data"]["success"].stringValue == "true" {
                    
                    self.sendRequest(url: "mobiconnect/mobirma/listrma", params: ["customer_id":custid],store:false)
                }else{
                    cedMageHttpException.showAlertView(me: self, msg: json["data"]["message"].stringValue, title: "Error".localized)
                }
            }else{
                print(json)
                if json["data"]["success"].stringValue == "true" {
                    if rmaData.count > 0 {
                        rmaData.removeAll()
                    }
                    for rma in json["data"]["rma_info"].arrayValue {
                        let rma_id = rma["rma_id"].stringValue
                        let vendor = rma["vendor"].stringValue
                        let status = rma["status"].stringValue
                        let order = rma["order"].stringValue
                        let date = rma["date"].stringValue
                        let bill_to = rma["bill_to"].stringValue
                        let rma_increment_id = rma["rma_increment_id"].stringValue
                        self.rmaData.append(["rma_id":rma_id,"vendor":vendor,"status":status,"order":order,"date":date,"rma_increment_id":rma_increment_id,"bill_to":bill_to])
                    }
                    mainTableView.reloadData()
                    
                }else{
                    cedMageHttpException.showAlertView(me: self, msg: json["data"]["message"].stringValue, title: "Error".localized)

                }
            }
            }catch let error {
                print(error.localizedDescription)
            }
        }
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rmaData.count
       }
       
       func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = mainTableView.dequeueReusableCell(withIdentifier: "myRmaCell", for: indexPath) as! myRmaCell
        let rdata = rmaData[indexPath.row]
        cell.contentView.cardView()
        cell.rmaIdLabel.text = rdata["rma_increment_id"]
        cell.vendorLabel.text  = rdata["vendor"]
        cell.orderLabel.text = rdata["order"]
        cell.dateLabel.text = rdata["date"]
        cell.billToLabel.text = rdata["bill_to"]
        cell.statusLabel.text = rdata["status"]
        if rdata["rma_increment_id"] != "" {
                cell.deleteButton.setThemeColor()
               // cell.deleteButton.addTarget(self, action: #selector(myRmaListingViewController.cancelRMA(sender:)), for: .touchUpInside)
                cell.deleteButton.setTitle("Cancel", for: .normal)
                cell.deleteButton.tag = Int(rdata["rma_id"]!)!
            }
            return cell
        }
        
        @objc func cancelRMA(sender:UIButton){
            let id = String(sender.tag)
            self.sendRequest(url: "mobiconnect/mobirma/cancelrma", params: ["rma_id":id,"customer_id":custid],store:false)
        }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
       if  let rma_id = rmaData[indexPath.row]["rma_id"] {
       let viewcontroll = UIStoryboard(name: "cedMageAccounts", bundle: nil).instantiateViewController(withIdentifier: "rmaProductView") as! rmaProductViewController
           print("RMAID")
           print(rma_id)
        viewcontroll.rma_id = rma_id
       self.navigationController?.pushViewController(viewcontroll, animated: true)
       }
    }
}
