/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */

import UIKit
import CoreData
import UserNotifications
import FirebaseMessaging
import Firebase
import GoogleMaps
import GooglePlaces
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    var externalWindow: UIWindow?
    let notificationDelegate = SampleNotificationDelegate()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        //selecting_local.DoTheSwizzling()
        //cedMage.checkmoduless()
        FirebaseApp.configure()
        cedMage().getCartCount()
        cedMage().getHyperlocalType()
        GMSServices.provideAPIKey("AIzaSyCgdNouyfJSUCTlZmG_NgQ9vFAvpU1P8nU")
        GMSPlacesClient.provideAPIKey("AIzaSyCgdNouyfJSUCTlZmG_NgQ9vFAvpU1P8nU")
       // BTAppSwitch.setReturnURLScheme("com.cedcommerce.magenativemagentotwo")
        /*let notificationTypes: UIUserNotificationType = [UIUserNotificationType.alert, UIUserNotificationType.badge, UIUserNotificationType.sound]
        let pushNotificationSettings = UIUserNotificationSettings(types: notificationTypes, categories: nil)
        application.registerUserNotificationSettings(pushNotificationSettings)
        application.registerForRemoteNotifications()*/
        registerForPushNotification(application: application)
        UITextField().keyboardAppearance =   UIKeyboardAppearance.dark
        UIApplication.shared.statusBarStyle = .lightContent
        UIApplication.shared.isStatusBarHidden = false
        return true
    }
    
    func reloadHome(){
        let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "mainView") as! cedMageSideDrawer
        
        self.window?.rootViewController = viewController
        self.window?.makeKeyAndVisible()
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data)
    {
        var tokenString: String = ""
        for i in 0..<deviceToken.count {
            tokenString += String(format: "%02.2hhx", deviceToken[i] as CVarArg)
        }
        var uniqueId = ""
        if let uniqId = UIDevice.current.identifierForVendor {
            uniqueId = uniqId.uuidString
        }
        UserDefaults.standard.setValue(["token":tokenString,"unique":uniqueId], forKey: "notificationdetails")
        if !UserDefaults.standard.bool(forKey: "isLogin"){
            if(tokenString != ""){
                var url = Settings.baseUrl
            url += "mobinotifications/setdevice"
            var postData = [String:String]()
            postData["Token"] = tokenString
            postData["type"] = "1"
            postData["email"] = "Guest User"
            postData["unique_id"] = uniqueId
            
            let postString = ["parameters":postData].convtToJson() as String
            var request = URLRequest(url: URL(string:url)!)
            request.httpMethod = "POST";
            print(postString)
            if UserDefaults.standard.bool(forKey: "isLogin"){
                if let user = UserDefaults.standard.object(forKey: "userInfoDict") as? [String:String] {
                    request.setValue(user["hashKey"], forHTTPHeaderField: "hashkey")
                }
                
            }
            request.httpBody = postString.data(using: String.Encoding.utf8)
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            let task = URLSession.shared.dataTask(with: request) {
                data, response, error in
                print("TestingAfter");
                print(url);
                print(postString);
                
                // check for http errors
                if let httpStatus = response as? HTTPURLResponse , httpStatus.statusCode != 200
                {
                    print("statusCode should be 200, but is \(httpStatus.statusCode)")
                    print("response = \(String(describing: response))")
                    return;
                }
                DispatchQueue.main.async{
                    if let data  = data
                    {
                        let dataTemp = NSString(data: data, encoding:String.Encoding.utf8.rawValue);
                        print(dataTemp as Any)
                        guard let json = try? JSON(data: data)else{return;}
                        print(json);
                    }
                };
            }
            
            task.resume()
            }
        }
        
    }
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
         let sourceApplication: String? = options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String
        if(url.scheme!.contains("fb")){
            return ApplicationDelegate.shared.application(
                app,
                open: url,
                sourceApplication: sourceApplication,
                annotation: nil)
        }
        /*else if url.scheme?.localizedCaseInsensitiveCompare("com.magenative.com") == .orderedSame {
             return BTAppSwitch.handleOpen(url, options: options)
        }*/
        else{
            return GIDSignIn.sharedInstance().handle(url)
        }
    }

    /*func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data)
    {
        var tokenString: String = ""
        for i in 0..<deviceToken.count {
            tokenString += String(format: "%02.2hhx", deviceToken[i] as CVarArg)
        }
        if(tokenString != ""){
            var url = Settings.baseUrl
        url += "mobinotifications/setdevice"
        var postData = [String:String]()
        postData["Token"] = tokenString
        postData["type"] = "1"
        let postString = ["parameters":postData].convtToJson() as String
        var request = URLRequest(url: URL(string:url)!)
        request.httpMethod = "POST";
            let requestHeader = Settings.headerKey
        request.setValue(requestHeader, forHTTPHeaderField: "Mobiconnectheader")
        request.httpBody = postString.data(using: String.Encoding.utf8)
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let task = URLSession.shared.dataTask(with: request) {
            data, response, error in
            print("TestingAfter");
            print(url);
            print(postString);
            
            // check for http errors
            if let httpStatus = response as? HTTPURLResponse , httpStatus.statusCode != 200
            {
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(String(describing: response))")
                return;
            }
            DispatchQueue.main.async{
                if let data  = data
                {
                    let dataTemp = NSString(data: data, encoding:String.Encoding.utf8.rawValue);
                    print(dataTemp as Any)
                    guard let json = try? JSON(data: data)else{return;}
                    print(json);
                }
            };
        }
        
        task.resume()
        }
    }
    
    func application(_ application: UIApplication, didRegister notificationSettings: UIUserNotificationSettings) {
        
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        
    }*/
    
    /*func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        print(userInfo)
        let state = UIApplication.shared.applicationState
        guard let test=userInfo["aps"] as? Dictionary<String,Any>
            else{
            print("not able to convert (userInfo[aps])")
            return
        }
        guard let main=test["alert"] as? Dictionary<String,Any>
            else{
                print("not able to convert (userInfo[alert])")
            return
        }
        if state == .active {
            let alert=UIAlertController(title: main["title"] as? String, message: main["body"] as? String, preferredStyle: .alert)
            let action=UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
            })
            let action_cancel=UIAlertAction(title: "Cancel", style: .destructive, handler: { (action: UIAlertAction!) in
                return
            })
            alert.addAction(action)
            alert.addAction(action_cancel)
            alert.modalPresentationStyle = .fullScreen;
            self.window?.rootViewController?.present(alert, animated: true, completion: nil)
            return
        }else{
            self.openPages(main:main)
        }
    }*/
    
    func openPages(main:Dictionary<String,Any>){
        print(main)
        UserDefaults.standard.set(main, forKey: "NotificationData")
        let drawer=UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "mainView") as! cedMageSideDrawer
        
        self.window?.rootViewController=drawer
        self.window?.makeKeyAndVisible()
    }
    
    
    func configureNotification() {
        if #available(iOS 10.0, *) {
            let center = UNUserNotificationCenter.current()
            center.requestAuthorization(options:[.badge, .alert, .sound]){ (granted, error) in }
            center.delegate = notificationDelegate
            let openAction = UNNotificationAction(identifier: "OpenNotification", title: NSLocalizedString("Abrir", comment: ""), options: UNNotificationActionOptions.foreground)
            let deafultCategory = UNNotificationCategory(identifier: "CustomSamplePush", actions: [openAction], intentIdentifiers: [], options: [])
            center.setNotificationCategories(Set([deafultCategory]))
        } else {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil))
        }
        UIApplication.shared.registerForRemoteNotifications()
    }
    
    
    
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        //self.saveContext()
    }

}
extension URLSession {
    
    class  func getImage(url:URL,completionHaner:@escaping (Data?,Error?)->()){
        
        let task = URLSession.shared.dataTask(with: url as URL, completionHandler: {
            
            data,response,error in
            DispatchQueue.main.async {
              completionHaner(data,error)
                return
            }
            
        })
        task.resume()
    }
}
