//
//  AdditionalInfoView.swift
//  MageNative Magento Platinum
//
//  Created by cedcoss on 10/08/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import Foundation

class AdditionalInfoView:UIView{
    var view : UIView!
    @IBOutlet weak var expandButton: UIImageView!
    
    @IBOutlet weak var additionalInfoBtn: UIButton!
    @IBOutlet weak var label: UILabel!
    
    @IBOutlet weak var value: UILabel!
    @IBOutlet weak var vStack: UIStackView!
    
    @IBOutlet weak var hStack: UIStackView!
    override init(frame: CGRect){
    super.init(frame: frame)
    xibSetup()
    }

    required init?(coder: NSCoder) {
    super.init(coder: coder)
    xibSetup()
    }
    func xibSetup(){
    view = loadViewFromNib()
    view.frame = bounds
    view.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth,UIView.AutoresizingMask.flexibleHeight]
    
    addSubview(view)
    }
    func loadViewFromNib() -> UIView{
    let bundle = Bundle(for: type(of: self))
    let nib = UINib(nibName: "AdditionalInfoView", bundle: bundle)
    let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
    return view
    }
}
