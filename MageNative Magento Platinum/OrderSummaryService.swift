//
//  OrderSummaryService.swift
//  MageNative Magento Platinum
//
//  Created by Hamza Usmani on 06/09/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import Foundation

class OrderSummaryService {
    
    static let shared = OrderSummaryService()
    
    func getSummaryData(controller: UIViewController, completion: @escaping (OrderSummaryModel) -> Void) {
        var param = [String:String]()
        
        if UserDefaults.standard.bool(forKey: "isLogin") {
            let userInfoDict = UserDefaults.standard.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
            param["hashkey"] = userInfoDict["hashKey"]!;
            param["customer_id"] = userInfoDict["customerId"]!;
        } else {
            param["cart_id"] = UserDefaults.standard.value(forKey: "cartId") as? String ?? "0"
        }
        
        ApiHandler.handle.request(with: "mobiconnect/checkout/viewcart", params: param, requestType: .POST, controller: controller, completion: { data, error in
            guard let data = data else { return }
            guard let json = try? JSON(data: data) else { return }
            print(json)
            
            let result = OrderSummaryModel(json: json)
            completion(result)
        })
    }
}
