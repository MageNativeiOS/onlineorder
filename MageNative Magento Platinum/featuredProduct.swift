//
//  featuredProduct.swift
//  MageNative Magento Platinum
//
//  Created by Macmini on 30/10/18.
//  Copyright © 2018 CEDCOSS Technologies Private Limited. All rights reserved.
//
import UIKit
import AVFoundation
class featuredProduct: UITableViewCell,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UIScrollViewDelegate {
    
    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var products  = [[String:String]]()
    var loadMoreData = true
    var currentpage = 1
    var flag=false
    var no_pro_check=false
    
    
    var parentViewCont = UIViewController()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.isScrollEnabled = false
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let height = CGFloat(235)
        if UIDevice.current.userInterfaceIdiom == .pad{
            let width = UIWindow().frame.size.width/3 - 15
            return CGSize(width:width, height:height);
        }else{
            let width = UIWindow().frame.size.width/2 - 15
            return CGSize(width:width, height:height);
        }
    }
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return products.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        if indexPath.section > dealGroup.count  {
//            //let productInfo = products[indexPath.row];
            let productview = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "productSinglePageViewController") as! ProductSinglePageViewController;
        productview.product_id = products[indexPath.row]["product_id"]!
            parentViewCont.navigationController?.pushViewController(productview
                , animated: true);
//        }else{
//            if indexPath.row != 0 {
//                _ = UIStoryboard(name: "Main", bundle: nil)
//
//                if let deal = dealsPro[String(indexPath.section - 1)]?[indexPath.row - 1] {
//
//                    if(deal["deal_type"] ==  "1"){
//                        let productview = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: +                        productview.pageData = [["product_id":deal["relative_link"]]]
//                        let instance = cedMage.singletonInstance
//                        instance.storeParameterInteger(parameter: 0)
//                        parentViewController.navigationController?.pushViewController(productview
//                            , animated: true)
//                    }else if(deal["deal_type"] ==  "2"){
//                        let viewcontoller = UIStoryboard(name: "categorylayouts", bundle: nil).instantiateViewController(withIdentifier: "cedMageDefaultCollection") as? cedMageDefaultCollection
//                        viewcontoller?.selectedCategory = deal["relative_link"]!
//                        parentViewController.navigationController?.pushViewController(viewcontoller!, animated: true)
//                    }else if(deal["deal_type"] ==  "3"){
//                        let   StoryBoard = UIStoryboard(name: "cedMageAccounts", bundle: nil)
//                        let viewControl = StoryBoard.instantiateViewController(withIdentifier: "cmsWebView") as! cedMageCmsWebView
//                        viewControl.pageUrl = deal["relative_link"]!
//                        parentViewController.navigationController?.pushViewController(viewControl, animated: true)
//                    }
//                }
//            }
//        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionviewdefault", for: indexPath) as? defaultGridCell {
            let product = products[indexPath.row]
            var productName = NSMutableAttributedString()
            productName = NSMutableAttributedString(string: product["product_name"]!)
            
            if(product["starting_from"] != ""){
                productName.append(NSAttributedString(string:"\n"+"Starting At : ".localized+product["starting_from"]!+"\n"));
                if(product["from_price"] != ""){
                    productName.append(NSAttributedString(string: "As Low As : ".localized+product["from_price"]!));
                }
            }
            else{
                
                if(product["special_price"] != "no_special"){
                    let attr = [NSAttributedString.Key.strikethroughStyle:1]
                    if let regPrice = product["regular_price"]{
                        let attribute = NSAttributedString(string:  "\n"+regPrice, attributes: attr)
                        productName.append(attribute)
                    }
                    let attr1 = [NSAttributedString.Key.foregroundColor:UIColor.red]
                    let attribute1 = NSAttributedString(string:  product["special_price"]!, attributes: attr1)
                    productName.append(NSAttributedString(string:"\n"));
                    productName.append(attribute1)
                    
                }else{
                    productName.append(NSAttributedString(string: "\n"+product["regular_price"]!));
                }
                
            }
            cell.productTitle.attributedText = productName
            //                cell.offerText
            if product["offerText"] != "" {
                cell.offerView.isHidden = false
                cell.offerText.text = product["offerText"]! + "% OFF".localized
            }else{
                cell.offerView.isHidden = true
            }
            if(product["Inwishlist"] != "OUT"){
                cell.wishlistButton.setImage(UIImage(named:"LikeFilled"), for: UIControl.State.normal);
                
            }else{
                cell.wishlistButton.isHidden = true
            }
            cell.productImage.image = UIImage(named: "placeholder")
            if  let urlToRequest = product["product_image"]{
                if(urlToRequest != "false"){
                    
                    SDWebImageManager.shared.loadImage(with: NSURL(string: urlToRequest) as URL?, options: .continueInBackground, progress: {
                        (receivedSize :Int, ExpectedSize :Int, newSize: URL?) in
                        
                    }, completed: {
                        (image : UIImage?, data: Data?, error : Error?, cacheType : SDImageCacheType, finished : Bool, url : URL?) in
                        let size=cell.productImage.bounds.size.width
                        if let image = image {
                            let image_up=self.resizeImage(image: image, newWidth: size)
                            cell.productImage.image=image_up
                        }
                    })
                    //cell.productImage.sd_setImage(with: URL(string: urlToRequest), placeholderImage: UIImage(named: "placeholder"))
                    
                }else{
                    if let image = UIImage(named: "placeholder") {
                        cell.productImage.image = image
                    }
                }
            }else{
                if let image = UIImage(named: "placeholder") {
                    cell.productImage.image = image
                }
            }
            cell.cardView()
            if (indexPath.row) == (products.count/2)
            {
                flag=true
            }
            
            //                MARK : SetFont COLOR
            cell.productTitle.fontColorTool()
            
            
            return cell
        }
        return UICollectionViewCell()
    }
    
    func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage {
        
        //let image = UIImage(contentsOfFile: img_url)
        
        let size = (image.size).applying(CGAffineTransform(scaleX: 0.5, y: 0.5))
        let hasAlpha = false
        let scale: CGFloat = 0.0 // Automatically use scale factor of main screen
        
        UIGraphicsBeginImageContextWithOptions(size, !hasAlpha, scale)
        image.draw(in: CGRect(origin: CGPoint.zero, size: size))
        
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        
        return scaledImage!
        
    }
    
    
}
