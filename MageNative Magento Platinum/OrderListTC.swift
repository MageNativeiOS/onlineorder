//
//  OrderListTC.swift
//  MageNative Magento Platinum
//
//  Created by Hamza Usmani on 04/09/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class OrderListTC: UITableViewCell {
    
    //MARK:- Properties
    
    static var reuseID:String = "OrderListTC"
    private var headingArray: [String] = ["Order Id", "Ship To", "Status"]
    var order = [String:String]()
    
    lazy var containerView:UIView = {
        let view = UIView()
        view.backgroundColor = .mageSystemBackground
        return view
    }()
    
    lazy var orderIdLabel:UILabel = {
        let label = UILabel()
        label.textColor = .mageLabel
        label.text = "ORDER # 000000000"
        label.font = .systemFont(ofSize: 16, weight: .bold)
        return label
    }()
    
    lazy var priceLabel:UILabel = {
        let label = UILabel()
        label.textColor = .mageLabel
        label.text = "$ 199.80"
        label.font = .systemFont(ofSize: 14, weight: .semibold)
        return label
    }()
    
    lazy var orderDateLabel:UILabel = {
        let label = UILabel()
        label.textColor = .mageSecondaryLabel
        label.text = "Placed on 4 Sept 2020"
        label.font = .systemFont(ofSize: 13, weight: .medium)
        return label
    }()
    
    lazy var orderStack:UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.distribution = .fillEqually
        stack.spacing = 3.0
        return stack
    }()
    
    
    //MARK:- Lifecycle
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        backgroundColor = .clear
        
        addSubview(containerView)
        containerView.anchor(top: topAnchor, left: leadingAnchor, bottom: bottomAnchor, right: trailingAnchor,
                             paddingTop: 16, paddingLeft: 16, paddingBottom: 3, paddingRight: 16)
    
        containerView.addSubview(orderIdLabel)
        orderIdLabel.anchor(top: containerView.topAnchor, left: containerView.leadingAnchor, paddingTop: 5, paddingLeft: 6)
        
        containerView.addSubview(priceLabel)
        priceLabel.anchor(right: containerView.trailingAnchor, paddingRight: 6)
        priceLabel.centerY(inView: orderIdLabel)
    
        containerView.addSubview(orderDateLabel)
        orderDateLabel.anchor(top: orderIdLabel.bottomAnchor, left: containerView.leadingAnchor, paddingTop: 8, paddingLeft: 6)
        
        containerView.addSubview(orderStack)
        orderStack.anchor(top: orderDateLabel.bottomAnchor, left: containerView.leadingAnchor, right: containerView.trailingAnchor,
                          paddingTop: 8, paddingLeft: 6, paddingRight: 6)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        containerView.layer.cornerRadius = 5
        containerView.layer.shadowColor = UIColor.mageLabel.cgColor
        containerView.layer.shadowOffset = .zero//CGSize(width: 2, height: 2)
        containerView.layer.shadowRadius = 2.0
        containerView.layer.shadowOpacity = 0.3
    }
    
    //MARK:- Helper Functions
    
    func populate(withOrder order: [String:String]) {
        self.order = order
        
        orderIdLabel.text = "ORDER #\(order["number"] ?? "")"
        priceLabel.text = order["totalAmount"] ?? ""
        orderDateLabel.text = "Placed on \(order["date"] ?? "")"
        
        orderStack.subviews.forEach({ $0.removeFromSuperview() })
        
        for heading in headingArray {
            let labelOne = UILabel()
            labelOne.text = heading
            labelOne.font = .systemFont(ofSize: 13, weight: .semibold)
            
            let labelTwo = UILabel()
            labelTwo.font = .systemFont(ofSize: 13, weight: .medium)
            labelTwo.textColor = .mageSecondaryLabel

            switch heading {
            case "Order Id":
                labelTwo.text = order["orderId"]
            case "Ship To":
                labelTwo.text = order["shipTo"]
            default:
                labelTwo.text = order["orderStatus"]
            }
            
            let stack = UIStackView(arrangedSubviews: [labelOne, labelTwo])
            stack.axis = .horizontal
            stack.distribution = .fillEqually
            stack.anchor(height: 15)
            
            orderStack.addArrangedSubview(stack)
        }
    }
    
}
