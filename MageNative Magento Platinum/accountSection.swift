/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */


import UIKit

class cedMageAccounts: cedMageViewController,UITableViewDelegate,UITableViewDataSource,UIActionSheetDelegate {
    
  
    @IBOutlet weak var accountDefaultTable: UITableView!
    var accountArray = [[String:String]]()
    var titleView: UIView!
    var Stores = [[String:String]]()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        accountDefaultTable.tableFooterView = UIView()
        accountDefaultTable.delegate = self
        accountDefaultTable.dataSource = self
        loadData()
        
    }
  
    
    func loadData(){
        accountArray = cedMage.getInfoPlist(fileName: "cedMage",indexString: "Account" )as! [[String : String]]
       
        if(!cedMage.checkModule(string: "MageNative_MobiWishlist")){
            let index = accountArray.firstIndex(where: {$0 == ["img": "whislist", "name": "My WishList"]})
            accountArray.remove(at: index!)
            
        }
        /*if(!cedMage.checkModule(string: "MageNative_MobiCms")){
            let index = accountArray.index(where: {$0 == ["img": "business", "name": "Company"]})
            accountArray.remove(at: index!)
            
        }*/
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func recieveResponse(data: Data?, requestUrl: String?, response: URLResponse?) {
        if (requestUrl == "mobiconnectstore/getlist") {
            self.Stores.removeAll()
            guard let json = try? JSON(data: data!) else{return;}
            print("^*^%^&%^&%&^%^&\(json)")
                for store in  json[0]["store_data"].arrayValue{
                    let group_id = store["group_id"].stringValue
                    //print(group_id)
                    let code = store["code"].stringValue
                    let store_id = store["store_id"].stringValue
                    let name = store["name"].stringValue
                    let is_active = store["is_active"].stringValue
                    let storeobject = ["group_id":group_id,"code":code,"store_id":store_id,"name":name,"is_active":is_active]
                    self.Stores.append(storeobject)
                }
         
            self.showStores()
        }else{

            LoginManager().logOut()
            GIDSignIn.sharedInstance().signOut()

            self.defaults.set(false, forKey: "isLogin");
            self.defaults.removeObject(forKey: "userInfoDict");
            self.defaults.removeObject(forKey: "selectedAddressId");
            defaults.setValue("Magenative App For Magento 2", forKey: "name")
            UserDefaults.standard.set(nil,forKey: "LIAccessToken")
            UserDefaults.standard.synchronize();
            defaults.synchronize()
            self.defaults.removeObject(forKey: "selectedAddressDescription");
            
            self.defaults.removeObject(forKey: "cartId");
            self.defaults.removeObject(forKey: "cart_summary");
            self.defaults.removeObject(forKey: "guestEmail");
            self.defaults.set("0", forKey: "items_count");
            self.defaults.removeObject(forKey: "name")
            self.setCartCount(view: self, items: "0")
            self.navigationController?.navigationBar.isHidden = false
            //let viewControl = UIStoryboard(name: "homeLayouts", bundle: nil).instantiateViewController(withIdentifier: "baseHomepageController") as? baseHomepageController
            //self.navigationController?.setViewControllers([viewControl!], animated: true);
            navigationController?.popToRootViewController(animated: true)//setViewControllers([HomeController()], animated: true)
            tabBarController?.selectedIndex = 0
            let vc=cedMageMainDrawer()
            let table = vc.mainTable;
           // let label=cell.viewWithTag(1258) as? UILabel
           // label?.text="MageNative"
            table?.reloadData();
           
            
          NotificationCenter.default.post(name: NSNotification.Name("loadDrawerAgain"), object: nil)

        }
    }
    
    //MARK: Delegate Function for TableView
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }
        return accountArray.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.section == 0){
            let cell = accountDefaultTable.dequeueReusableCell(withIdentifier: "header_view") as? accountcell
            cell?.MYAccountLable.fontColorTool()
            cell?.MYAccountLable.text = "My Account".localized
            cell?.circleView.layer.cornerRadius = (cell?.circleView.frame.width)!/2
            cell?.circleView.layer.borderColor = UIColor.black.cgColor
            cell?.circleView.layer.borderWidth = 2
            cell?.editProfile.addTarget(self, action: #selector(cedMageAccounts.gotoEditProfile(sender: )), for: .touchUpInside)
            let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
            print(userInfoDict)
            if let name = defaults.value(forKey: "name") as? String {
                let label = cell?.viewWithTag(852) as? UILabel
                label?.text = "\(name)" + "\n" +  userInfoDict["email"]!;
                label?.fontColorTool()
            }
            return cell!
        }
        if(indexPath.row < accountArray.count){
            let cell = accountDefaultTable.dequeueReusableCell(withIdentifier: "account_cell") as! accountcell
            cell.label?.text = NSLocalizedString(accountArray[indexPath.row]["name"]!, comment: "profile")
            cell.label?.fontColorTool()
            cell.imageView?.fontColorTool()
            //cell.label.font = UIFont(name: , size: 15.0)
            if(accountArray[indexPath.row]["img"] != nil){
                cell.ImageView.image = UIImage(named: accountArray[indexPath.row]["img"]!)
            }
            cell.cell_view.cardView()
            return cell
        }else{
            let cell = accountDefaultTable.dequeueReusableCell(withIdentifier: "logOut") as! accountcell
            let logoutButton = cell.viewWithTag(1230) as? UIButton
            logoutButton?.setThemeColor()
            logoutButton?.fontColorTool()
            logoutButton?.setTitle("LogOut".localized, for: .normal)
            logoutButton?.addTarget(self, action: #selector(cedMageAccounts.doLogout(sender:)), for: .touchUpInside)
             cell.cell_view.cardView()
            return cell
        }
    }
    
    //Mark: Edit profile Page
    @objc func gotoEditProfile(sender:UIButton){
        let viewcontroll = storyboard?.instantiateViewController(withIdentifier: "profileView") as! cedMageProfileView
        self.navigationController?.pushViewController(viewcontroll, animated: true)
    }
    
    //Mark:Do Logout
    
    @objc func doLogout(sender:UIButton){
        if(defaults.bool(forKey: "isLogin") == true)
        {
            let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
            let hashKey = userInfoDict["hashKey"]!;
            let customerId = userInfoDict["customerId"]!;
            let postString = ["hashkey":hashKey,"customer_id":customerId];
            self.sendRequest(url: "mobiconnect/customer/logout", params: postString)
        }
    }
    
    //Mark:Show Stores
    func showStores(){
        let actionsheet = UIAlertController(title: "Select Store".localized, message: nil, preferredStyle: .actionSheet)
        print("%^%^%^*(**^&%&%*)_*^&^")
        for buttons in self.Stores {
            actionsheet.addAction(UIAlertAction(title: buttons["name"], style: UIAlertAction.Style.default,handler: {
                action -> Void in
                print(action.title as Any)
                self.selectStore(store: action.title)
                }))
            
        }
        actionsheet.addAction(UIAlertAction(title: "Cancel".localized, style: UIAlertAction.Style.cancel, handler: {
            action -> Void in
        }))
        if(UIDevice().model.lowercased() == "iPad".lowercased()){
           actionsheet.popoverPresentationController?.sourceView = self.accountDefaultTable.cellForRow(at: IndexPath(row: 5, section: 0))
         
        }
        actionsheet.modalPresentationStyle = .fullScreen;
        self.present(actionsheet, animated: true, completion: nil)
    }
    
    func selectStore(store:String?){
        if store=="arabic"
        {
            UserDefaults.standard.set(["ar","en","fr"], forKey: "mageAppLang")
            
            self.view.reloadInputViews()
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
        }
        else
        {
            UserDefaults.standard.set([store,"ar","fr"], forKey: "mageAppLang")
            self.view.reloadInputViews()
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
        }
        load_app()
    }
    
    
    func load_app()
    {
        let rootviewcontroller: UIWindow = ((UIApplication.shared.delegate?.window)!)!
        
        rootviewcontroller.rootViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "mainView")
        
        let mainwindow = (UIApplication.shared.delegate?.window!)!
        
        mainwindow.backgroundColor = UIColor(hue: 0.6477, saturation: 0.6314, brightness: 0.6077, alpha: 0.8)
        
        UIView.transition(with: mainwindow, duration: 0.55001, options: .transitionFlipFromLeft, animations: { () -> Void in
            
        }) { (finished) -> Void in
            
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(indexPath.section == 0){
            return 230
        }
        if(indexPath.row < accountArray.count){
            return 60
        }else{
            return 45
        }
    }
  
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(indexPath.section == 0){
            return
        }
        let select = accountArray[indexPath.row]
        if(select["name"] == "Profile"){
            
            let viewcontroll = storyboard?.instantiateViewController(withIdentifier: "profileView") as! cedMageProfileView
            self.navigationController?.pushViewController(viewcontroll, animated: true)
            
        }
        else if(select["name"] == "My WishList"){
            
            let viewcontroll = storyboard?.instantiateViewController(withIdentifier: "mywishlist") as! cedMageWishList
            self.navigationController?.pushViewController(viewcontroll, animated: true)
            
        }
        else if(select["name"] == "Logout")
        {
         
        }else if(select["name"] == "Company"){
            let viewcontroll = UIStoryboard(name: "cedMageAccounts", bundle: nil).instantiateViewController(withIdentifier: "company") as! cedMageCompanyPage
            self.navigationController?.pushViewController(viewcontroll, animated: true)
            
        }
        else if(select["name"] == "Address Book"){
            
            let viewcontroll = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "addressListingViewController") as! AddressListingViewController
            self.navigationController?.pushViewController(viewcontroll, animated: true)
            
        } else if(select["name"] == "My Orders"){
            
            let viewcontroll = UIStoryboard(name: "cedMageAccounts", bundle: nil).instantiateViewController(withIdentifier: "cedMageOrders") as! cedMageMyOrders
            self.navigationController?.pushViewController(viewcontroll, animated: true)
            
        }else if(select["name"] == "My Downloads"){
            
            let viewcontroll = UIStoryboard(name: "cedMageAccounts", bundle: nil).instantiateViewController(withIdentifier: "mydownloads") as! cedMageMyDownloadView
            self.navigationController?.pushViewController(viewcontroll, animated: true)
            
        }
        else if(select["name"] == "Contact Us"){
            
            let viewcontroll = UIStoryboard(name: "cedMageAccounts", bundle: nil).instantiateViewController(withIdentifier: "contactUs") as! cedMageContactUs
            self.navigationController?.pushViewController(viewcontroll, animated: true)
            
        }
        else if(select["name"] == "Compare Products"){
            let vc = UIStoryboard(name: "categorylayouts", bundle: nil).instantiateViewController(withIdentifier: "lcgproCompareController") as! lcgproCompareController
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if(select["name"] == "Notification Centre"){
            let vc = UIStoryboard(name: "categorylayouts", bundle: nil).instantiateViewController(withIdentifier: "NotificationCentreVC") as! NotificationCentreVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if(select["name"] == "My Stores"){
            self.sendRequest(url:  "mobiconnectstore/getlist", params: nil)
           
        }
        
        else if(select["name"] == "My Returns"){
            let viewcontroll = UIStoryboard(name: "cedMageAccounts", bundle: nil).instantiateViewController(withIdentifier: "myRmaListing") as! myRmaListingViewController
            self.navigationController?.pushViewController(viewcontroll, animated: true)
        }
        
        else if(select["name"] == "My Ticket"){
             
             let viewcontroll = UIStoryboard(name: "cedMageAccounts", bundle: nil).instantiateViewController(withIdentifier: "cedMageSupportTicketListing") as! cedMageSupportTicketListing
             viewcontroll.title = "My Tickets"
             self.navigationController?.pushViewController(viewcontroll, animated: true)
           }
           else if(select["name"] == "Create Ticket"){
             let viewcontroll = UIStoryboard(name: "cedMageAccounts", bundle: nil).instantiateViewController(withIdentifier: "cedMageSupportCreateTicket") as! cedMageSupportCreateTicket
             viewcontroll.title = "Create New Ticket"
             self.navigationController?.pushViewController(viewcontroll, animated: true)
           }
           else if(select["name"] == "Admin Inbox"){
                
                let viewcontroll = UIStoryboard(name: "cedMageAccounts", bundle: nil).instantiateViewController(withIdentifier: "adminInboxController") as! adminInboxController
                viewcontroll.title = "Admin Inbox"
                self.navigationController?.pushViewController(viewcontroll, animated: true)
              }
              else if(select["name"] == "Vendor Inbox"){
                let viewcontroll = UIStoryboard(name: "cedMageAccounts", bundle: nil).instantiateViewController(withIdentifier: "vendorInboxController") as! vendorInboxController
                viewcontroll.title = "Vendor Inbox"
                self.navigationController?.pushViewController(viewcontroll, animated: true)
              }
              
              else if(select["name"] == "My Wallet"){
                  let viewcontroll = UIStoryboard(name: "cedMageAccounts", bundle: nil).instantiateViewController(withIdentifier: "cedMageAddWallet") as! cedMageAddWallet
                  self.navigationController?.pushViewController(viewcontroll, animated: true)
              }
              else if(select["name"] == "Refer a Friend"){
                  let viewcontroll = UIStoryboard(name: "cedMageAccounts", bundle: nil).instantiateViewController(withIdentifier: "referViewController") as! referViewController
                  self.navigationController?.pushViewController(viewcontroll, animated: true)
              }
              else if(select["name"] == "My Reward Points"){
                let viewcontroll = UIStoryboard(name: "cedMageAccounts", bundle: nil).instantiateViewController(withIdentifier: "cedMageReward") as! cedMageRewardViewController
                self.navigationController?.pushViewController(viewcontroll, animated: true)
              }

                
    }
    
    

    
}
