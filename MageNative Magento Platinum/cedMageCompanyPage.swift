/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */

import UIKit

class cedMageCompanyPage: cedMageViewController,UITableViewDelegate,UITableViewDataSource {
    
    var cmsPages = [[String:String]]()
    @IBOutlet weak var cmsBlockView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        cmsBlockView.delegate = self
        cmsBlockView.dataSource = self
        
        self.sendRequest(url: "mobiconnectcms/cms/getCmsPages", params: nil)
        // Do any additional setup after loading the view.
        
    }
    private func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cmsPages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = cmsBlockView.dequeueReusableCell(withIdentifier: "cmsblock", for: indexPath as IndexPath)
        let label = cell.viewWithTag(1110) as? UILabel
        label?.fontColorTool()
        label?.text = cmsPages[indexPath.row]["pagetitle"]!
        cell.textLabel?.textAlignment = .center
        label?.cardView()
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let url = cmsPages[indexPath.row]["pageUrl"]
        let view = UIStoryboard(name: "cedMageAccounts", bundle: nil)
        let viewControl = view.instantiateViewController(withIdentifier: "cmsWebView") as! cedMageCmsWebView
        viewControl.pageUrl = url!
        self.navigationController?.pushViewController(viewControl, animated: true)
        
    }
    
    override func recieveResponse(data: Data?, requestUrl: String?, response: URLResponse?) {
        guard let json = try? JSON(data:data!) else{return;}
        if(json["data"]["status"].stringValue == "success"){
            for cmsblock in json["data"]["cmsblocks"].arrayValue {
                let pageid = cmsblock["page-id"].stringValue
                let pageTitle = cmsblock["page-title"].stringValue
                let pageUrl = cmsblock["page-url"].stringValue
                let blockObject = ["pageId":pageid,"pagetitle":pageTitle,"pageUrl":pageUrl]
                self.cmsPages.append(blockObject)
            }
            self.cmsBlockView.reloadData()
        }else{
            
           cedMageHttpException.showAlertView(me: self, msg: json["data"]["message"].stringValue, title: "Error")
        }
    }
}
