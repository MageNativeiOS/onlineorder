/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */

import UIKit

extension ProductSinglePageViewController{
  
  func renderProductReviewPopupSection(){
    reviewPopupView = ReviewPopupView();
    reviewPopupView.ratingPoints.fontColorTool()
    if(productInfoArray["review"] != nil && productInfoArray["review"] != ""){
      reviewPopupView.ratingPoints.text = productInfoArray["review"];
    }
    else{
      reviewPopupView.ratingPoints.text = "0";
    }
    if(productInfoArray["review_count"] == "" || productInfoArray["review_count"] == "0"){
      reviewPopupView.numOfReviews.text = "No Reviews".localized;
    }
    else if(productInfoArray["review_count"] == "1"){
      reviewPopupView.numOfReviews.text = productInfoArray["review_count"]!+" Review";
    }
    else{
      reviewPopupView.numOfReviews.text = productInfoArray["review_count"]!+" Reviews";
    }
    reviewPopupView.writeReviewButton.addTarget(self, action: #selector(ProductSinglePageViewController.showProductReviewSection(_:)), for: UIControl.Event.touchUpInside);
    
    reviewPopupView.translatesAutoresizingMaskIntoConstraints = false;
    let reviewPopupViewHeight = translateAccordingToDevice(CGFloat(120.0));
    let reviewPopupViewWidth = translateAccordingToDevice(CGFloat(300.0));
    self.view.addSubview(reviewPopupView);
    self.view.addConstraint(NSLayoutConstraint(item: reviewPopupView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: reviewPopupViewHeight));
    self.view.addConstraint(NSLayoutConstraint(item: reviewPopupView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: reviewPopupViewWidth));
    self.view.addConstraint(NSLayoutConstraint(item: reviewPopupView, attribute: NSLayoutConstraint.Attribute.centerY, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.centerY, multiplier: 1, constant: 0));
    self.view.addConstraint(NSLayoutConstraint(item: reviewPopupView, attribute: NSLayoutConstraint.Attribute.centerX, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.centerX, multiplier: 1, constant: 0));
    reviewPopupView.isHidden = true;
  }
  
  
  
  
  @objc func showProductReviewSection(_ sender: UIButton){
    self.imageViewAsOverlay.isHidden = true;
    self.floatingActionButton.isHidden = false;
    
    self.reviewPopupView.isHidden = true;
    
    let tapGesture = UITapGestureRecognizer(target: self, action: #selector(ProductSinglePageViewController.dismissFloatingButtonSection(_:)));
    imageViewAsOverlay.addGestureRecognizer(tapGesture);
    tapGesture.delegate=self
    
    print("showProductReviewSection");
    print(self.productInfoArray["product-id"] as Any);
    print(self.productInfoArray["product-id"] as Any);
    let storyboard = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil);
    let viewController = storyboard.instantiateViewController(withIdentifier: "productReviewListingViewController") as! ProductReviewListingViewController;
    viewController.product_id = self.productInfoArray["product-id"]!;
    self.navigationController?.pushViewController(viewController, animated: true);
  }
  
  
  @objc func dismissSimilarProductsSection(_ recognizer: UITapGestureRecognizer){
    print("dismissSimilarProductsSection");
    UIView.animate(withDuration: 1.0, delay: 0, options: UIView.AnimationOptions.curveLinear, animations: {
      self.similarProductView.center.y += self.view.bounds.height;
    }, completion: {completed in
      self.imageViewAsOverlay.isHidden = true;
      self.similarProductView.removeFromSuperview();
      for recognizer in self.imageViewAsOverlay.gestureRecognizers! {
        print(recognizer);
        if recognizer is UITapGestureRecognizer{
          self.imageViewAsOverlay.removeGestureRecognizer(recognizer);
        }
      }
      let imageViewAsOverlay_tapGesture = UITapGestureRecognizer(target: self, action: #selector(ProductSinglePageViewController.dismissFloatingButtonSection(_:)));
      self.imageViewAsOverlay.addGestureRecognizer(imageViewAsOverlay_tapGesture);
        imageViewAsOverlay_tapGesture.delegate=self 
    }
    );
  }
  
  @objc func showSimilarProductsButtonPressed(_ sender:UIButton){
    //        print("showSimilarProductsButtonPressed");
    //
    //        imageViewAsOverlay.isHidden = false;
    //        imageViewAsOverlay.isUserInteractionEnabled = true;
    //        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(ProductSinglePageViewController.dismissSimilarProductsSection(_:)));
    //        imageViewAsOverlay.addGestureRecognizer(tapGesture);
    //        tapGesture.delegate=self;
    //
    //
    //        self.view.addConstraint(NSLayoutConstraint(item: similarProductView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: similarProductViewHeight));
    //        self.view.addConstraint(NSLayoutConstraint(item: similarProductView, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 10));
    //        self.view.addConstraint(NSLayoutConstraint(item: similarProductView, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: -10));
    //        self.view.addConstraint(NSLayoutConstraint(item: similarProductView, attribute: NSLayoutConstraint.Attribute.bottom, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: -(10)));
    //
    //
    //        UIView.animate(withDuration: 1.0, delay: 0, options: UIView.AnimationOptions.curveLinear, animations: {
    //            self.similarProductView.center.y -= self.view.bounds.height;
    //            }, completion: nil
    //        );
    
  }
}
