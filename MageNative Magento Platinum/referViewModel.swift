//
//  referViewModel.swift
//  MageNative Magento Platinum
//
//  Created by cedcoss on 19/03/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import Foundation

class referViewModel {
    var controller                          : referViewController?
    var referData                           : referDataModel? {didSet{ configureTable()}}
    var numberOfRows                        : Int?
   
    func getDataFromApi(with url: String, for Controller: UIViewController, completion:@escaping (referViewModel)-> Void) {
        
        if let controller                   = Controller as? referViewController { self.controller = controller }
      
        ApiHandler.handle.request(with: url, params: [:], requestType: .GET, controller: Controller) { (data, error) in
            guard let data                  = data else { return }
            do {
                print( try JSON(data: data ))
                
                let js = try JSON(data:data)
                if !js[0]["success"].boolValue { self.controller?.view.makeToast(js[0]["message"].stringValue); return }
                
                
                let json                    = try JSON(data: data)
                let referData                = try json[0].rawData(options: [])
                self.referData               = try JSONDecoder().decode(referDataModel.self, from: referData)
                completion(self)
            } catch {
                print(error.localizedDescription)
            }
        }
    }
    
       func configureTable() {
           registerNib()
           self.controller?.referTable.separatorStyle   = .none
           self.controller?.referTable.delegate         = self.controller
           self.controller?.referTable.dataSource       = self.controller
           self.controller?.referTable.reloadData()
       }
       
       // regitering nib for cart Table
       
       func registerNib() {
           let nib             = UINib(nibName: "referTableCell", bundle: nil)
           self.controller?.referTable.register(nib, forCellReuseIdentifier: "referTableCell")
       }
    func share(message: String, link: String) {
        if let link = NSURL(string: link) {
            let objectsToShare = [message,link] as [Any]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            self.controller?.present(activityVC, animated: true, completion: nil)
        }
    }
    func submitButtonPressed(data:[String:String]) {
        var postData = [String:String]()
        let userDict                    = UserDefaults.standard.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]()
        postData["customerId"] = userDict["customerId"]
        postData["emailids"] = data["emailId"]
        postData["message"] = data["message"]
        
        
        ApiHandler.handle.request(with: "mobireward/send/invitations", params: postData, requestType: .POST, controller: self.controller!) { (data, error) in
            
            guard let data = data else{return}
            do{
                var json = try JSON(data:data)
                print(json)
                json = json[0]
                cedMageLoaders.removeLoadingIndicator(me: self.controller!);
                if json["success"].boolValue == true {
                    self.controller?.view?.makeToast(json["msg"].stringValue, duration: 2.0, position: .bottom)
                }
                else{
                    self.controller?.view?.makeToast(json["msg"].stringValue, duration: 2.0, position: .bottom)
                }
                
            }catch let error {
                print(error.localizedDescription)
            }
        }
    }
    
}
