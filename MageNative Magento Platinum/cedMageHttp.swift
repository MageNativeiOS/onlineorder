//
//  cedMageHttp.swift
//  MageNative Magento Platinum
//
//  Created by CEDCOSS Technologies Private Limited on 12/08/16.
//  Copyright © 2016 CEDCOSS Technologies Private Limited. All rights reserved.
//

import Foundation
import UIKit
class cedMageViewController: UIViewController {
    var externalWindow : UIWindow?
    let defaults = UserDefaults.standard
    let color = cedMage.getInfoPlist(fileName: "cedMage", indexString: "themeColor")
    var themecolor = UIColor()
    
    override func viewDidLoad() {
        setupNav()
        themecolor = cedMage.UIColorFromRGB(colorCode: color as! String)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.backIndicatorImage = UIImage(named: "backArrow")
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = UIImage(named: "back")
//        self.navigationController?.navigationBar.backIndicatorImage.=
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        self.navigationItem.backBarButtonItem?.tintColor = .white
        let barButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.search, target: self, action: #selector(cedMageViewController.gotoSearch(sender:)))
        barButton.tintColor = .white
        //Mark: Cart Button Item
        let buttonWidth = 30;
        let buttonHeight = 35;
        let label = UILabel();
        let button: UIButton = UIButton();
        label.tag = 123;
        label.font = label.font.withSize(10)
        label.textAlignment = .center;
        label.textColor = UIColor.black
        label.backgroundColor = UIColor.white
        if let cartval = defaults.value(forKey: "items_count"){
            label.text = String(describing: cartval);
            print(cartval)
        }
        else{
            label.text = "0";
        }
        button.frame = CGRect(x: 0, y: 0, width: CGFloat(buttonWidth), height: CGFloat(buttonHeight))
        label.frame.origin.x = 18
        label.frame.origin.y = -1
        label.frame.size = CGSize(width: 16, height: 16)
        label.layer.cornerRadius = 8
        label.layer.masksToBounds = true
        let imageView = UIImageView(image: UIImage(named: "cart"))
        imageView.frame.size = button.frame.size
        imageView.center = button.center
        imageView.contentMode = .scaleAspectFit
        imageView.tintColor = UIColor.white
        button.addSubview(imageView)
        button.layer.cornerRadius = 5
        button.backgroundColor = .clear
        button.addTarget(self, action:#selector(cedMageViewController.gotoCart(sender:)) , for: UIControl.Event.touchUpInside)
        button.addSubview(label);
        let cartButton = UIBarButtonItem(customView: button)
        
        if let themeColor = cedMage.getInfoPlist(fileName: "cedMage", indexString: "fontColor") as? String{
//            button.tintColor =  cedMage.UIColorFromRGB(colorCode:themeColor)
             cartButton.tintColor =  cedMage.UIColorFromRGB(colorCode:themeColor)
        }
        //Mark: more Button Item
        let titleView = UIView()
        titleView.frame = CGRect(x: 0, y: 0, width: 130, height: 40)
        self.navigationItem.setRightBarButtonItems([cartButton,barButton], animated: true)
        let navigationTitle = UIImageView(frame: CGRect(x: 0, y: 0, width: 140, height: 65))
        navigationTitle.center = titleView.center
        navigationTitle.image = UIImage(named: "header-Title")
        navigationTitle.contentMode = .scaleAspectFit
        titleView.addSubview(navigationTitle)
        self.navigationItem.titleView = titleView
        cedMage().setcartCount(tabBar: self.tabBarController)
        cedMageHomeNavigation().addToggleButton(me: self)
    }
    
    
    
    
    func intializeExternalScreen(external:UIScreen){
        self.externalWindow = UIWindow(frame: external.bounds)
        self.externalWindow?.screen = external
        self.externalWindow?.isHidden = false
        let view  = UIView(frame: (self.externalWindow?.frame)!)
        let imageView = UIImageView(image: UIImage(named: "splash"))
        imageView.frame = view.frame
        imageView.contentMode = .scaleAspectFit
        view.addSubview(imageView)
        view.backgroundColor = UIColor.white
        self.externalWindow?.addSubview(view)
        self.externalWindow?.isHidden = false
    }
    
    func getRequest(url:String,store:Bool)
    {
        cedMageLoaders.addDefaultLoader(me: self)
        let storeId = defaults.value(forKey: "storeId")
        var postString = String()
        let httpUrl = Settings.baseUrl
        if store {
            if(storeId != nil){
                postString += (storeId! as! String)
            }
        }
        
        let reqUrl = httpUrl+url+postString
        var makeRequest = URLRequest(url: URL(string: reqUrl)!)
        //let requestHeader = cedMage.getInfoPlist(fileName:"cedMage",indexString: "requestheader") as! String
        
        makeRequest.httpMethod = "GET"
        
        print(reqUrl)
        print(postString)
        //makeRequest.httpBody = postString.data(using: String.Encoding.utf8)
        //makeRequest.setValue(requestHeader, forHTTPHeaderField: "Mobiconnectheader")
        let task = URLSession.shared.dataTask(with: makeRequest, completionHandler: {data,response,error in
            
            
            // check for http errors
            if let httpStatus = response as? HTTPURLResponse , httpStatus.statusCode != 200
            {
                
                DispatchQueue.main.async
                    {
                        cedMageLoaders.removeLoadingIndicator(me: self)
                        self.renderNoDataImage(view: self, imageName: "no_module")
                        print("statusCode should be 200, but is \(httpStatus.statusCode)")
                        
                        print("response = \(response)")
                }
                return;
            }
            
            // code to fetch values from response :: start
            
            guard error == nil && data != nil else
            {
                DispatchQueue.main.async
                    {
                        cedMageLoaders.removeLoadingIndicator(me: self)
                        cedMageHttpException.showAlertView(me: self, msg: error?.localizedDescription, title: "Error".localized )
                        print("error=\(error)")
                }
                return ;
            }
            
            DispatchQueue.main.async
                {
                    cedMageLoaders.removeLoadingIndicator(me: self)
                    
                    self.recieveResponse(data: data, requestUrl: url, response: response)
                  
            }
        })
        
        task.resume()
    }
    func send_req_pagination(url:String,store:Bool)
    {
        let indicator=UIActivityIndicatorView(style: .gray)
        indicator.frame=CGRect(x: 0, y: 0, width: 40, height: 40)
        indicator.center=self.view.center
        self.view.addSubview(indicator)
        indicator.bringSubviewToFront(self.view)
        indicator.startAnimating()
        //cedMageLoaders.addDefaultLoader(me: self)
        let storeId = defaults.value(forKey: "storeId")
        var postString = String()
        let httpUrl = Settings.baseUrl
        if store {
            if(storeId != nil){
                postString += (storeId! as! String)
            }
        }
        
        let reqUrl = httpUrl+url+postString
        var makeRequest = URLRequest(url: URL(string: reqUrl)!)
        //let requestHeader = cedMage.getInfoPlist(fileName:"cedMage",indexString: "requestheader") as! String
        
        makeRequest.httpMethod = "GET"
        
        print(reqUrl)
        print(postString)
        //makeRequest.httpBody = postString.data(using: String.Encoding.utf8)
        //makeRequest.setValue(requestHeader, forHTTPHeaderField: "Mobiconnectheader")
        let task = URLSession.shared.dataTask(with: makeRequest, completionHandler: {data,response,error in
            
            
            // check for http errors
            if let httpStatus = response as? HTTPURLResponse , httpStatus.statusCode != 200
            {
                
                DispatchQueue.main.async
                    {
                        indicator.stopAnimating()
                        //cedMageLoaders.removeLoadingIndicator(me: self)
                        self.renderNoDataImage(view: self, imageName: "no_module")
                        print("statusCode should be 200, but is \(httpStatus.statusCode)")
                        
                        print("response = \(response)")
                }
                return;
            }
            
            // code to fetch values from response :: start
            
            
            guard error == nil && data != nil else
            {
                DispatchQueue.main.async
                    {
                        indicator.stopAnimating()
                        //cedMageLoaders.removeLoadingIndicator(me: self)
                        cedMageHttpException.showAlertView(me: self, msg: error?.localizedDescription, title: "Error".localized )
                        print("error=\(error)")
                }
                return ;
            }
            
            DispatchQueue.main.async
                {
                    indicator.stopAnimating()
                    //cedMageLoaders.removeLoadingIndicator(me: self)
                    
                    self.recieveResponse(data: data, requestUrl: url, response: response)
                    let screen = UIScreen.screens
                    if(screen.count > 1){
                        let mainScreen = UIScreen.main
                        for scrn in screen {
                            if(scrn != mainScreen){
                                //scrn = mainScreen
                                self.intializeExternalScreen(external: scrn)
                            }else{
                                //  self.window?.makeKeyAndVisible()
                            }
                        }
                    }
            }
        })
        
        task.resume()
    }
    
    
    
    //MArk :MageNative Wheel code controller basis
    func sendWheelRequest(url:String,params:String){
        
        cedMageLoaders.addDefaultLoader(me: self)
        let baseUrl = Settings.sellerUrl
        let reqUrl = baseUrl+url
        
        var postString=params
        
        var makeRequest = URLRequest(url: URL(string: reqUrl)!)
        
        let requestHeader = Settings.headerKey
        let storeId = defaults.value(forKey: "storeId") as? String
        
        if(params != ""){
            makeRequest.httpMethod = "POST"
            if storeId != nil {
                postString += "&store_id="+storeId!
            }
            
        }
        
        print(reqUrl)
        print(postString)
        makeRequest.httpBody = postString.data(using: String.Encoding.utf8)
        if UserDefaults.standard.bool(forKey: "isLogin"){
            if let user = UserDefaults.standard.object(forKey: "userInfoDict") as? [String:String] {
                makeRequest.setValue(user["hashKey"], forHTTPHeaderField: "hashkey")
            }
            
        }
        let task = URLSession.shared.dataTask(with: makeRequest, completionHandler: {data,response,error in
            
            
            // check for http errors
            if let httpStatus = response as? HTTPURLResponse , httpStatus.statusCode != 200
            {
                
                DispatchQueue.main.async
                    {
                        cedMageLoaders.removeLoadingIndicator(me: self)
                        self.renderNoDataImage(view: self, imageName: "no_module")
                        print("poststring=\(postString)")
                        print("statusCode should be 200, but is \(httpStatus.statusCode)")
                        
                        print("response = \(response)")
                }
                return;
            }
            
            // code to fetch values from response :: start
            
            
            guard error == nil && data != nil else
            {
                DispatchQueue.main.async
                    {
                        cedMageLoaders.removeLoadingIndicator(me: self)
                        cedMageHttpException.showAlertView(me: self, msg: error?.localizedDescription, title: "Error".localized )
                        print("error=\(error)")
                }
                return ;
            }
            
            DispatchQueue.main.async
                {
                    cedMageLoaders.removeLoadingIndicator(me: self)
                    
                    self.recieveResponse(data: data, requestUrl: url, response: response)
                    
            }
        })
        
        task.resume()
    }
    
    //Mark:
   func sendRequest(url:String,params:Dictionary<String,String>?,store:Bool = true,buyer:Bool = false)
    {
        
        cedMageLoaders.addDefaultLoader(me: self)
        
        var httpUrl = Settings.baseUrl
        if buyer {
            httpUrl = Settings.sellerUrl
        }
        let reqUrl = httpUrl+url
        
        var postString=Dictionary<String,Dictionary<String,String>>()
        var postString1=""
        print(reqUrl)
        var makeRequest = URLRequest(url: URL(string: reqUrl)!)
        
        //_ = cedMage.getInfoPlist(fileName:"cedMage",indexString: "requestheader") as! String
        let storeId = defaults.value(forKey: "storeId") as? String
        
        if(params != nil){
            makeRequest.httpMethod = "POST"
            if buyer {
                for (key,value) in params!
                {
                    postString1 += "&" + key + "=" + value
                }
                if store {
                    if storeId != nil {
                        if(url != "mobinotifications/listNotification"){
                            postString1 += "&store_id=" + storeId!
                        }
                        
                        
                    }
                }
            }else{
                postString=["parameters":[:]]
                for (key,value) in params!
                {
                    _ = postString["parameters"]?.updateValue(value, forKey:key)
                }
                if store {
                    if storeId != nil {
                        if(url != "mobinotifications/listNotification"){
                            _ = postString["parameters"]?.updateValue(storeId!, forKey:"store_id")
                        }
                        
                    }
                }
                postString1=postString.convtToJson() as String
                makeRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
            }
        }
    if UserDefaults.standard.bool(forKey: "isLogin"){
        if let user = UserDefaults.standard.object(forKey: "userInfoDict") as? [String:String] {
            makeRequest.setValue(user["hashKey"], forHTTPHeaderField: "hashkey")
        }
        
    }
        print(reqUrl)
        print(postString)
        makeRequest.httpBody = postString1.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
        
        let task = URLSession.shared.dataTask(with: makeRequest, completionHandler: {data,response,error in
            //cedMageLoaders.removeLoadingIndicator(me: self)
      //      print(NSString(data: data!, encoding: String.Encoding.utf8.rawValue)!)
            // check for http errors
            if let httpStatus = response as? HTTPURLResponse , httpStatus.statusCode != 200
            {
                
                DispatchQueue.main.async
                    {
                        cedMageLoaders.removeLoadingIndicator(me: self)
                        
                        print("poststring=\(postString1)")
                        print(NSString(data: data!, encoding: String.Encoding.utf8.rawValue) ?? "")
                        print("statusCode should be 200, but is \(httpStatus.statusCode)")
                        
                        print("response = \(response)")
                        self.recieveResponse(data: data, requestUrl: url, response: response)
                }
                return;
            }
            
            // code to fetch values from response :: start
            
            
            guard error == nil && data != nil else
            {
                DispatchQueue.main.async
                    {
                        
                        cedMageLoaders.removeLoadingIndicator(me: self)
                        cedMageHttpException.showAlertView(me: self, msg: error?.localizedDescription, title: "Error".localized )
                        
                        print("error=\(error)")
                        self.recieveResponse(data: data, requestUrl: url, response: response)
                }
                return ;
            }
            
            DispatchQueue.main.async
                {
                    cedMageLoaders.removeLoadingIndicator(me: self)
                    
                    self.recieveResponse(data: data, requestUrl: url, response: response)
                    
            }
        })
        
        task.resume()
        }
    
    
    
    func failedWithError(data:Data?,requestUrl:String?,response:URLResponse?){
        
    }
    
    
    
    
    //Mark:
    func sendCheckoutRequest(url:String,params:Dictionary<String,String>?){
        
        cedMageLoaders.addDefaultLoader(me: self)
        let httpUrl = "http://demo.cedcommerce.com/magento2/mage-native/"
        let reqUrl = httpUrl+url
        
        var postString=Dictionary<String,Dictionary<String,String>>()
        var postString1:String=""
        print(reqUrl)
        var makeRequest = URLRequest(url: URL(string: reqUrl)!)
        
        //_ = cedMage.getInfoPlist(fileName:"cedMage",indexString: "requestheader") as! String
        let storeId = defaults.value(forKey: "storeId")
        
        if(params != nil){
            makeRequest.httpMethod = "POST"
            postString=["parameters":[:]]
            for (key,value) in params!
            {
                _ = postString["parameters"]?.updateValue(value, forKey:key)
            }
            if storeId != nil {
                _ = postString["parameters"]?.updateValue(storeId as! String, forKey:"store_id")
            }
            postString1=postString.convtToJson() as String
        }
        if UserDefaults.standard.bool(forKey: "isLogin"){
            if let user = UserDefaults.standard.object(forKey: "userInfoDict") as? [String:String] {
                makeRequest.setValue(user["hashKey"], forHTTPHeaderField: "hashkey")
            }
            
        }
        print(reqUrl)
        print(postString)
        //makeRequest.httpBody = postString1.data(using: String.Encoding.utf8.rawValue)
        //makeRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let task = URLSession.shared.dataTask(with: makeRequest, completionHandler: {data,response,error in
            // check for http errors
            if let httpStatus = response as? HTTPURLResponse , httpStatus.statusCode != 200
            {
                
                DispatchQueue.main.async
                    {
                        cedMageLoaders.removeLoadingIndicator(me: self)
                        
                        
                        self.renderNoDataImage(view: self, imageName: "no_module")
                        
                        print("poststring=\(postString1)")
                        print("statusCode should be 200, but is \(httpStatus.statusCode)")
                        
                        print("response = \(response)")
                }
                return;
            }
            
            // code to fetch values from response :: start
            
            
            guard error == nil && data != nil else
            {
                DispatchQueue.main.async
                    {
                        cedMageLoaders.removeLoadingIndicator(me: self)
                        cedMageHttpException.showAlertView(me: self, msg: error?.localizedDescription, title: "Error".localized )
                        print("error=\(error)")
                }
                return ;
            }
            
            DispatchQueue.main.async
                {
                    cedMageLoaders.removeLoadingIndicator(me: self)
                    
                    self.recieveResponse(data: data, requestUrl: url, response: response)
                    
            }
        })
        
        task.resume()
    }
    
    
    
    func recieveResponse(data:Data?,requestUrl:String?,response:URLResponse?){
        
    }
    
    
    //SetUPNavigationBarColor
    
    func setupNav(){
        if let themeColor = cedMage.getInfoPlist(fileName: "cedMage", indexString: "themeColor") as? String{
            self.navigationController?.navigationBar.barTintColor = cedMage.UIColorFromRGB(colorCode:themeColor)
//             self.navigationController?.navigationBar.barTintColor = UIColor.black
        self.navigationController?.navigationBar.tintColor = cedMage.UIColorFromRGB(colorCode:themeColor)
        }
        
    }
    
    //Mark:Goto Cart Page
    
    @objc func gotoCart(sender:UIButton){
        let cartPage = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "cartViewController") as! CartViewController
        if !(self.navigationController?.viewControllers.last is CartViewController) {
            self.navigationController?.pushViewController(cartPage, animated: true)
        }
    }
    
    func updateCartCount(){
        print("updateCartCount");
        let cartCountSection = self.navigationItem.rightBarButtonItem;
        let cartCountLabel = cartCountSection?.customView?.viewWithTag(123) as? UILabel;
        print("cartCountLabel???");
        
        
        cartCountLabel?.text = "$";
        print(cartCountLabel?.text ?? "cartCountLabel");
    }
    
    //Mark:goto Search page
    
    @objc func gotoSearch(sender:UIBarButtonItem){
        let searchPage = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "searchView") as! cedMageSearchPage
        if !(self.navigationController?.viewControllers.last is cedMageSearchPage) {
            self.navigationController?.pushViewController(searchPage, animated: true)
        }
       // self.navigationController?.pushViewController(searchPage, animated: true)
    }
    
    func setCartCount(view:UIViewController,items:String){
        let vv = view.navigationItem.rightBarButtonItem;
        let label = vv?.customView?.viewWithTag(123) as? UILabel;
        if(items != ""){
            label?.text = items;
        }
        
    }
    func renderNoDataImage(view:UIViewController,imageName:String){
        let noDataImageView = nodataImageView();
        noDataImageView.translatesAutoresizingMaskIntoConstraints = false;
        if imageName == "noProduct"{
            noDataImageView.topLabel.text = "no products in this category.".uppercased()
        }else if imageName == "noWishlist" {
            noDataImageView.topLabel.text = "You have no items in your wishlist.".localized.uppercased()
        }else if imageName == "no_order" {
            noDataImageView.topLabel.text = "You have no Orders.".localized.uppercased()
        }else if imageName == "noAddress" {
            noDataImageView.topLabel.text = "Looks Like you don't have any saved addresses.".localized.uppercased()
        }else if imageName == "nodownloads" {
            noDataImageView.topLabel.text = "You have not purchased any downloadable products yet".localized.uppercased()
        }else if imageName == "noDeals"{
            noDataImageView.topLabel.text = "No Exiciting Offers to show.".localized.uppercased()
            noDataImageView.continueShopping.isHidden = true
        }else if imageName == "noReviews"{
            noDataImageView.topLabel.text = "No Reviews Yet Be The First One To Review.".localized.uppercased()
            noDataImageView.continueShopping.isHidden = true
        }else if imageName == "no_module" {
            noDataImageView.topLabel.text = "Can't Connect Please check your Network.".localized.uppercased()
            noDataImageView.continueShopping.isHidden = true
        }
        
        noDataImageView.continueShopping.setTitle("Continue Shopping".localized, for: .normal)
        noDataImageView.fontColorTool()
        
        noDataImageView.continueShopping.setThemeColor()
        noDataImageView.continueShopping.addTarget(self, action: #selector(cedMageViewController.continueShopping), for: .touchUpInside)
        noDataImageView.contentMode = UIView.ContentMode.scaleAspectFit;
        view.view.addSubview(noDataImageView);
        view.view.addConstraint(NSLayoutConstraint(item: noDataImageView, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view.view, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 0));
        view.view.addConstraint(NSLayoutConstraint(item: noDataImageView, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view.view, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 0));
        view.view.addConstraint(NSLayoutConstraint(item: noDataImageView, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view.view, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1, constant: 0));
        view.view.addConstraint(NSLayoutConstraint(item: noDataImageView, attribute: NSLayoutConstraint.Attribute.bottom, relatedBy: NSLayoutConstraint.Relation.equal, toItem: view.view, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: 0));
        
    }
    
    @objc func continueShopping(_ sender:UIButton){
        /*if let viewController = UIStoryboard(name: "homeLayouts", bundle: nil).instantiateViewController(withIdentifier: "baseHomepageController") as? baseHomepageController {
            self.navigationController?.setViewControllers([viewController], animated: true)
        }*/
        navigationController?.setViewControllers([HomeController()], animated: true)
    }
    
    //Mark: POP ViewController
    
    @objc func backfunc(sender:UIButton){
        let mainview = self.sideDrawerViewController?.mainViewController  as? homedefaultNavigation
        _ =  mainview?.popViewController(animated: true)
        
    }
}

