/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */

import UIKit
import AVFoundation

class cedMageBuyerProfile: MagenativeUIViewController,UIScrollViewDelegate,UIGestureRecognizerDelegate,AVCaptureMetadataOutputObjectsDelegate {
    var selectedId:String?
    var vendor_profile = [String:String]();
    var banner_url = String();
    var products = [[String:String]]();
    var browseByJSONToBeSend : JSON!;
    var filterByJSONToBeSend : JSON!;
    var catfilter = String();
    /* sortBy related variables*/
    var sortApplied = false;
    var appliedSort = "";
    var sortOrder = "";
    var sortVal = "";
    var  sortArray = [[String : AnyObject]]();
    var sortByArray = [String:String]();
    /* sortBy related variables*/
    
    /* filter related variables*/
    var filtersStringForServer = "";
    /* filter related variables*/
    var currentPage = 1
    
    
    var captureSession:AVCaptureSession?
    var videoPreviewLayer:AVCaptureVideoPreviewLayer?
    var qrCodeFrameView:UIView?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.basicFoundationToRenderView(bottomMargin: 45);
        scrollView.delegate = self;
        let postData = ["vendor_id":selectedId! as String,"page":String(currentPage)]
        print(postData)
        self.sendRequest(url: "vendorapi/vproducts/vshops", params: postData,buyer:false)
          NotificationCenter.default.addObserver(self, selector: #selector(cedMageBuyerProfile.loadDataAgain(_:)), name: NSNotification.Name(rawValue: "loadProductsAgainId"), object: nil);
        // Do any additional setup after loading the view.
    }
    
    @objc func loadDataAgain(_ notification: NSNotification){
        
        clearViewAndVariables()
        
        if(defaults.object(forKey: "filtersToSend") != nil)
        {
            let tempString = defaults.object(forKey: "filtersToSend") as! String;
            self.filtersStringForServer = tempString;
            self.filtersStringForServer = self.filtersStringForServer.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
        }
        
        if(defaults.object(forKey: "previousBrowseByIds") != nil)
        {
            let selectedFiltersIds = defaults.object(forKey: "previousBrowseByIds") as! [Int];
            self.catfilter = String();
            for val in selectedFiltersIds
            {
                self.catfilter += String(val)+"#";
            }
            if(self.catfilter != ""){
                self.catfilter = self.catfilter.substring(to: self.catfilter.endIndex)
            }
        }
        var postString = ["vendor_id":selectedId! as String,"page":String(currentPage)]
        
        if(self.catfilter != "")
        {
            //postString += "&catfilter="+self.catfilter;
            postString.updateValue(self.catfilter, forKey: "catFilter")
        }
        
        if(sortApplied)
        {
            //postString += "&order="+self.sortOrder;
            //postString += "&dir="+self.sortVal;
            postString.updateValue(self.sortOrder, forKey: "order")
            postString.updateValue(self.sortVal, forKey: "dir")
        }
        
        if(filtersStringForServer != "")
        {
            //postString += "&multi_filter="+filtersStringForServer;
            postString.updateValue(filtersStringForServer, forKey: "multi_filter")
        }
        
     
        self.sendRequest(url: "vendorapi/vproducts/vshops", params: postString,buyer:false)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func clearViewAndVariables(){
        stackView.subviews.forEach({ $0.removeFromSuperview() });
        products = [[String:String]]();
        currentPage = 1;
        
    }
    func renderPofilepage(){
        makeSomeSpaceInStackView(height: 0.0)
        let cedMagePro = cedMageProfile()
        cedMagePro.heightAnchor.constraint(equalToConstant: 380).isActive = true
        if banner_url != "" {
        cedMageImageLoader().loadImgFromUrl(urlString: banner_url, completionHandler: {
            image,url in
            if let image = image {
            cedMagePro.bannerImage.image = image
            }
        })
        }
        cedMagePro.callUs.addTarget(self, action: #selector(cedMageBuyerProfile.callUsPressed(sender:)), for: .touchUpInside)
        cedMagePro.mailUs.addTarget(self, action: #selector(cedMageBuyerProfile.mailUsPressed(sender:)), for: .touchUpInside)
        if let shopName = self.vendor_profile["public_name"] {
            cedMagePro.shopName.text = shopName
        }
        if let reviewCount = self.vendor_profile["vendor_review"]{
            cedMagePro.rating.text = reviewCount
            
        }
        if let review = vendor_profile["review_count"]{
            let addres = (self.vendor_profile["city"] ?? "")+", "+(self.vendor_profile["country_id"] ?? "");
            cedMagePro.addressReview.text = addres + "\n" + review + " Reviews.".localized
        }
        
        let dataForQRCode=selectedId
        let data = dataForQRCode?.data(using: String.Encoding.isoLatin1, allowLossyConversion: false)
        
        let filter = CIFilter(name: "CIQRCodeGenerator")
        
        filter?.setValue(data, forKey: "inputMessage")
        filter!.setValue("Q", forKey: "inputCorrectionLevel")
        let colorFilter = CIFilter(name: "CIFalseColor")
        colorFilter?.setValue(filter?.outputImage, forKey: "inputImage")
        colorFilter?.setValue(CIColor(red: 1, green: 1, blue: 1), forKey: "inputColor1")
        colorFilter?.setValue(CIColor(color: UIColor.black), forKey: "inputColor0")
        
        let qrCodeImage = colorFilter?.outputImage
        
        let scaleX = (cedMagePro.qrImageView.frame.size.width) / (qrCodeImage?.extent.size.width)!
        let scaleY = (cedMagePro.qrImageView.frame.size.height) / (qrCodeImage?.extent.size.height)!
        
        let transformedImage = colorFilter?.outputImage?.transformed(by: CGAffineTransform(scaleX: scaleX, y: scaleY))
        cedMagePro.qrImageView.image=UIImage(ciImage: transformedImage!)
        
        stackView.addArrangedSubview(cedMagePro)
        self.setLeadingAndTralingSpaceFormParentView(cedMagePro, parentView: stackView, padding: 5)
        
        let cedMageShopDetail = cedMageShopdetailView()
        if let name = self.vendor_profile["public_name"]{
            cedMageShopDetail.shopName.text = name
        }
        if let mail = self.vendor_profile["email"]{
            cedMageShopDetail.mailadd.text = mail
        }
        if let compnyName = self.vendor_profile["company_name"]{
            cedMageShopDetail.company.text = "Company"
            cedMageShopDetail.companyName.text = compnyName
        }
        if let createdAt = self.vendor_profile["created_at"]{
            cedMageShopDetail.date.text = createdAt
        }
        if let profilePic = self.vendor_profile["profile_picture"]{
            cedMageImageLoader().loadImgFromUrl(urlString: profilePic, completionHandler: {
                image,url in
                cedMageShopDetail.imageView.image = image
            })
        }
        cedMageShopDetail.writeAreview.addTarget(self, action: #selector(cedMageBuyerProfile.writeAreview(_:)), for: .touchUpInside)
        cedMageShopDetail.heightAnchor.constraint(equalToConstant:250).isActive = true
        stackView.addArrangedSubview(cedMageShopDetail)
        self.setLeadingAndTralingSpaceFormParentView(cedMageShopDetail, parentView: stackView, padding: 5)
        
        
        if products.count > 0{
            var counter = 0
            for _ in products{
                let productView = productImageView();
                productView.tag = counter
                let tapGesture = UITapGestureRecognizer(target: self, action: #selector(productCardTapped(_:)));
                productView.addGestureRecognizer(tapGesture);
                tapGesture.delegate=self;
                productView.translatesAutoresizingMaskIntoConstraints = false;
                stackView.addArrangedSubview(productView);
                
                if(products[counter]["Inwishlist"] == "OUT"){
                    productView.wishList.setImage(UIImage(named:"LikeEmpty"), for: .normal);
                }
                else{
                    productView.wishList.setImage(UIImage(named:"LikeFilled"), for: .normal);
                }
                if let productId = products[counter]["product_id"]{
                   if let inVal = Int(productId){
                       productView.wishList.tag = inVal;
                    }
                }
                
               
                //module check
                
                productView.wishList.isHidden = true;
                let urlToRequest = products[counter]["product_image"]!;
                cedMageImageLoader().loadImgFromUrl(urlString: urlToRequest, completionHandler: {
                    image,url in
                    if let image = image {
                        productView.imageView.image = image
                    }
                })
                
                productView.heightAnchor.constraint(equalToConstant: 225).isActive = true
                self.setLeadingAndTralingSpaceFormParentView(productView, parentView:stackView);
                productView.produtName.text = products[counter]["product_name"]!;
                if products[counter]["review"] != "" {
                productView.rating.text = products[counter]["review"]!;
                }else{
                     productView.startView.isHidden = true
                }
            
                if(products[counter]["special_price"] != "no_special"){
                    let attr = [NSAttributedString.Key.strikethroughStyle:2]
                    let attribute = NSAttributedString(string:  products[counter]["regular_price"]!, attributes: attr)
                    
                    
                    productView.productPrice.attributedText = attribute ;
                    productView.productPrice2.text = products[counter]["special_price"]!;
                }
                else{
                    productView.productPrice.text = products[counter]["regular_price"]!;productView.productPrice2.isHidden = true;
                    productView.specialPriceHeight.constant = 0
                }
                
                counter = counter+1;
                
            }
            
            let browseByButton = cedMageShopBrowseBy()
            browseByButton.filterBy.addTarget(self, action: #selector(cedMageBuyerProfile.selectedFilters(_:)), for: .touchUpInside)
            browseByButton.browseBy.addTarget(self, action: #selector(cedMageBuyerProfile.browseBy(_:)), for: .touchUpInside)
            browseByButton.sortBy.addTarget(self, action: #selector(cedMageBuyerProfile.sortbyButtonPressed(_:)), for: .touchUpInside)
            browseByButton.translatesAutoresizingMaskIntoConstraints = false;
            browseByButton.heightAnchor.constraint(equalToConstant: 45).isActive = true;
            self.view.addSubview(browseByButton);
            
            self.setLeadingAndTralingSpaceFormParentView(browseByButton, parentView: self.view, padding: 0)
            self.view.addConstraint(NSLayoutConstraint(item: browseByButton, attribute: NSLayoutConstraint.Attribute.bottom, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: 0));
        }
        
        
    }
    
    @objc func writeAreview(_ sender:UIButton){
        if let page = UIStoryboard(name: "categorylayouts", bundle: nil).instantiateViewController(withIdentifier: "cedVendorReviewListingController") as? cedVendorReviewListingController{
          page.vendor_id = selectedId!
         page.vendorShopInfo = vendor_profile
        self.navigationController?.pushViewController(page, animated: true)
        }
    }
    
    
    @objc func selectedFilters(_ sender:UIButton){
        let storyboard = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil);
        if let viewController = storyboard.instantiateViewController(withIdentifier: "productFiltersViewController") as? ProductFiltersViewController{
        viewController.jsonForFilters = filterByJSONToBeSend;
        self.navigationController?.pushViewController(viewController, animated: true);
        }
    }
    
   
    
    @objc func browseBy(_ sender:UIButton){
        let storyboard = UIStoryboard(name: "categorylayouts", bundle: nil);
        if let viewController = storyboard.instantiateViewController(withIdentifier: "browseByViewController") as? BrowseByViewController{
        viewController.browseByJSON = browseByJSONToBeSend
        self.navigationController?.pushViewController(viewController, animated: true);
        }
    }
    
    
    override  func recieveResponse(data: Data?, requestUrl: String?, response: URLResponse?) {
        if let responseData = data {
            
            var jsonData = try! JSON(data:responseData)
            jsonData = jsonData[0]
            print(jsonData)
            if(jsonData.stringValue.lowercased() != "NO_PRODUCTS".lowercased()){
            //if jsonData["data"]["products"].count > 0 {
            self.banner_url = jsonData["data"]["banner_url"].stringValue;
            for (key,val) in jsonData["data"]["vendor_profile"]
            {
                self.vendor_profile[key] = val.stringValue;
            }
            
//            if(jsonData["data"]["review_count"] != nil)
//            {
//                self.vendor_profile["review_count"] = jsonData["data"]["review_count"].stringValue;
//            }
//            else
//            {
                self.vendor_profile["review_count"] = "0";
//            }
//            if(jsonData["data"]["vendor_review"] != nil)
//            {
//                self.vendor_profile["vendor_review"] = jsonData["data"]["vendor_review"].stringValue;
//            }
//            else
//            {
                self.vendor_profile["vendor_review"] = "0";
//            }
            
            for (_,val) in jsonData["data"]["products"]
            {
                var temp = [String:String]();
                temp["product_name"] = val["product_name"].stringValue;
                temp["show-both-price"] = val["show-both-price"].stringValue;
                temp["Inwishlist"] = val["Inwishlist"][0].stringValue;
                temp["type"] = val["type"].stringValue;
                temp["product_id"] = val["product_id"].stringValue;
                temp["price-including-tax"] = val["price-including-tax"].stringValue;
                temp["special_price"] = val["special_price"].stringValue;
                temp["regular_price"] = val["regular_price"].stringValue;
                temp["product_image"] = val["product_image"].stringValue;
                temp["review"] = val["review"].stringValue;
                temp["stock_status"] = val["stock_status"].stringValue;
                
                self.products.append(temp);
            }
            
            for (_,val) in jsonData["data"]["sort"]
            {
                for (keyInr,valInr) in val{
                    self.sortByArray[keyInr] = valInr[0].stringValue;
                }
                
            }
            self.browseByJSONToBeSend = jsonData["data"]["category-filter"];
            
            self.filterByJSONToBeSend = jsonData;
            self.renderPofilepage()
            //}
            }else{
                
                clearViewAndVariables()
            }
                
            
        }
        
    }
    
    @objc
    func productCardTapped(_ recognizer: UITapGestureRecognizer){
        if let index = recognizer.view?.tag {
            print(index);
            let productInfo = products[index];
            print(productInfo["product_id"] as Any);
            let productview = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "rootPageViewController") as! cedMageProductViewRoot;
            productview.pageData = products as NSArray;
            let instance = cedMage.singletonInstance;
            instance.storeParameterInteger(parameter: index);
            self.navigationController?.pushViewController(productview
                , animated: true);
        }
    }
    
    @objc func callUsPressed(sender:UIButton)
    {
        if(vendor_profile["support_number"] != nil && vendor_profile["support_number"] != ""){
            print("callUsPressed");
            let busPhone = vendor_profile["support_number"]
            if let url = URL(string: "tel://\(busPhone)") {
                UIApplication.shared.openURL(url)
            }
        }
        else
        {
            
        }
    }
    
    @objc func mailUsPressed(sender:UIButton)
    {
        if(vendor_profile["support_email"] != nil || vendor_profile["support_email"] != ""){
            print("mailUsPressed");
            let email = vendor_profile["support_email"]
            if let url = URL(string: "mailto://\(email)"){
                UIApplication.shared.openURL(url)
            }
        }
        else
        {
            
        }
    }
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        // Check if the metadataObjects array is not nil and it contains at least one object.
        if metadataObjects.count == 0 {
            qrCodeFrameView?.frame = CGRect.zero
           
            return
        }
        
        // Get the metadata object.
        let metadataObj = metadataObjects[0] as! AVMetadataMachineReadableCodeObject
        
        if metadataObj.type == AVMetadataObject.ObjectType.qr {
            // If the found metadata is equal to the QR code metadata then update the status label's text and set the bounds
            let barCodeObject = videoPreviewLayer?.transformedMetadataObject(for: metadataObj)
            qrCodeFrameView?.frame = barCodeObject!.bounds
            
            if metadataObj.stringValue != nil {
//                messageLabel.text = metadataObj.stringValue
            }
        }
    }
    
}
