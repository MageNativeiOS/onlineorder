/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */


import UIKit

extension ProductSinglePageViewController{

    func renderCustomOptionsView(){
        
        let customOptionContainerStackView = UIStackView(); // outermost stackview
        customOptionContainerStackView.translatesAutoresizingMaskIntoConstraints = false;
        customOptionContainerStackView.axis  = NSLayoutConstraint.Axis.vertical;
        customOptionContainerStackView.distribution  = UIStackView.Distribution.equalSpacing;
        customOptionContainerStackView.alignment = UIStackView.Alignment.center;
        customOptionContainerStackView.spacing   = 0.0;
        stackView.addArrangedSubview(customOptionContainerStackView);
        stackView.addConstraint(NSLayoutConstraint(item: customOptionContainerStackView, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: stackView, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: padding/2));
        stackView.addConstraint(NSLayoutConstraint(item: customOptionContainerStackView, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: stackView, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: -padding/2));
        
        
        let backgroundViewForStackView = UIView(); // uiview for covering stackview
        backgroundViewForStackView.translatesAutoresizingMaskIntoConstraints = false;
        //backgroundViewForStackView.backgroundColor = UIColor.white;
        //backgroundViewForStackView.makeCard(backgroundViewForStackView, cornerRadius: 2, color: UIColor.black, shadowOpacity: 0.4);
        customOptionContainerStackView.addArrangedSubview(backgroundViewForStackView); // adding uiview to stackview
        customOptionContainerStackView.addConstraint(NSLayoutConstraint(item: backgroundViewForStackView, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: customOptionContainerStackView, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 0));
        customOptionContainerStackView.addConstraint(NSLayoutConstraint(item: backgroundViewForStackView, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: customOptionContainerStackView, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 0));
        
        customOptionsStackView = UIStackView(); // main actual stackview that will contain all custom-options
        customOptionsStackView.translatesAutoresizingMaskIntoConstraints = false;
        customOptionsStackView.axis  = NSLayoutConstraint.Axis.vertical;
        customOptionsStackView.distribution  = UIStackView.Distribution.equalSpacing;
        customOptionsStackView.alignment = UIStackView.Alignment.center;
        customOptionsStackView.spacing   = 10.0;
        backgroundViewForStackView.addSubview(customOptionsStackView); // adding stackview to uiview
        backgroundViewForStackView.addConstraint(NSLayoutConstraint(item: customOptionsStackView!, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: backgroundViewForStackView, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: padding/2));
        backgroundViewForStackView.addConstraint(NSLayoutConstraint(item: customOptionsStackView!, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: backgroundViewForStackView, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: -padding/2));
        backgroundViewForStackView.addConstraint(NSLayoutConstraint(item: customOptionsStackView!, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: backgroundViewForStackView, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1, constant: padding/2));
        backgroundViewForStackView.addConstraint(NSLayoutConstraint(item: customOptionsStackView!, attribute: NSLayoutConstraint.Attribute.bottom, relatedBy: NSLayoutConstraint.Relation.equal, toItem: backgroundViewForStackView, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: -padding/2));
        
        var heightOfCustomOption = CGFloat(0.0);
        for (key,val) in custom_option{
            
            switch(val["type"]!){
                
            case "drop_down","radio" :
                let customOptionDropDownView = CustomOptionDropDownView();
                customOptionDropDownView.translatesAutoresizingMaskIntoConstraints = false;
                customOptionDropDownView.topLabel.text = val["title"]!;
                customOptionDropDownView.dropDownButton.fontColorTool()
                customOptionDropDownView.dropDownButton.setTitle("-- Select --".localized, for: UIControl.State());
                customOptionDropDownView.dropDownButton.addTarget(self, action: #selector(ProductSinglePageViewController.showCustomOptionDropdown(_:)), for: UIControl.Event.touchUpInside);
                
                let tag = Int(val["option_id"]!)!;
                customOptionDropDownView.dropDownButton.tag = tag;
                let tempArray = self.custom_option_options[key]!;
                for (val) in tempArray{
                    let tag = Int(val["option_type_id"]!)!;
                    self.custom_price_array[tag] = val["price"]!;
                }
                
                customOptionsStackView.addArrangedSubview(customOptionDropDownView);
                let customOptionDropDownViewHeight = translateAccordingToDevice(CGFloat(100.0));
                customOptionDropDownView.heightAnchor.constraint(equalToConstant: customOptionDropDownViewHeight).isActive = true;
                heightOfCustomOption = heightOfCustomOption+customOptionDropDownViewHeight;
                self.setLeadingAndTralingSpaceFormParentView(customOptionDropDownView,parentView:customOptionsStackView);
                
                break;
                
            case "field" :
                let customOptionTextFieldView = CustomOptionTextFieldView();
                customOptionTextFieldView.translatesAutoresizingMaskIntoConstraints = false;
                customOptionTextFieldView.topLabel.text = val["title"]!+" + "+productInfoArray["currency_symbol"]!+val["price"]!;
                
                let tag = Int(val["option_id"]!)!;
                customOptionTextFieldView.textView.tag = tag;
                self.custom_price_array[tag] = val["price"]!;
                
                customOptionTextFieldView.textView.delegate = self;
                
                customOptionsStackView.addArrangedSubview(customOptionTextFieldView);
                let customOptionTextFieldViewHeight = translateAccordingToDevice(CGFloat(100.0));
                customOptionTextFieldView.heightAnchor.constraint(equalToConstant: customOptionTextFieldViewHeight).isActive = true;
                heightOfCustomOption = heightOfCustomOption+customOptionTextFieldViewHeight;
                self.setLeadingAndTralingSpaceFormParentView(customOptionTextFieldView,parentView:customOptionsStackView);
                break;
                
            case "area" :
                let customOptionTextViewView = CustomOptionTextViewView();
                customOptionTextViewView.translatesAutoresizingMaskIntoConstraints = false;
                customOptionTextViewView.topLabel.text = val["title"]!+" + "+productInfoArray["currency_symbol"]!+val["price"]!;
                
                let tag = Int(val["option_id"]!)!;
                customOptionTextViewView.textView.tag = tag;
                self.custom_price_array[tag] = val["price"]!;
                
                customOptionTextViewView.textView.delegate = self;
                
                customOptionsStackView.addArrangedSubview(customOptionTextViewView);
                let customOptionTextViewViewHeight = translateAccordingToDevice(CGFloat(180.0));
                customOptionTextViewView.heightAnchor.constraint(equalToConstant: customOptionTextViewViewHeight).isActive = true;
                heightOfCustomOption = heightOfCustomOption+customOptionTextViewViewHeight;
                self.setLeadingAndTralingSpaceFormParentView(customOptionTextViewView,parentView:customOptionsStackView);
                break;
                
            case "time","date_time","date" :
                let customOptionTextFieldView = CustomOptionTextFieldView();
                customOptionTextFieldView.translatesAutoresizingMaskIntoConstraints = false;
                customOptionTextFieldView.topLabel.text = val["title"]!+" + "+productInfoArray["currency_symbol"]!+val["price"]!;
                customOptionTextFieldView.textView.delegate = self;
                
                let tag = Int(val["option_id"]!)!;
                customOptionTextFieldView.textView.tag = tag;
                self.datePickerTagsArray.append(tag);
                self.custom_price_array[tag] = val["price"]!;
                
                customOptionsStackView.addArrangedSubview(customOptionTextFieldView);
                let customOptionTextFieldViewHeight = translateAccordingToDevice(CGFloat(100.0));
                customOptionTextFieldView.heightAnchor.constraint(equalToConstant: customOptionTextFieldViewHeight).isActive = true;
                heightOfCustomOption = heightOfCustomOption+customOptionTextFieldViewHeight;
                self.setLeadingAndTralingSpaceFormParentView(customOptionTextFieldView,parentView:customOptionsStackView);
                break;
                
            case "multiple","checkbox" :
                let customOptionMultiSelectView = CustomOptionMultiSelectView();
                customOptionMultiSelectView.stackViewContainer.translatesAutoresizingMaskIntoConstraints = false;
                customOptionMultiSelectView.stackViewContainer.axis  = NSLayoutConstraint.Axis.vertical;
                customOptionMultiSelectView.stackViewContainer.distribution  = UIStackView.Distribution.equalSpacing;
                customOptionMultiSelectView.stackViewContainer.alignment = UIStackView.Alignment.center;
                customOptionMultiSelectView.stackViewContainer.spacing   = 0.0;
                
                let tempArray = self.custom_option_options[key]!;
                
                customOptionMultiSelectView.topLabel.text = val["title"]!;
                
                var checkboxViewStackViewContainerHeight = CGFloat(0);
                for (val) in tempArray{
                    
                    let checkboxView = CheckboxView();
                    checkboxView.translatesAutoresizingMaskIntoConstraints = false;
                    let checkboxViewHeight = translateAccordingToDevice(CGFloat(40.0));
                    checkboxView.backgroundColor  = UIColor.blue;
                    checkboxView.heightAnchor.constraint(equalToConstant: checkboxViewHeight).isActive = true;
                    customOptionMultiSelectView.stackViewContainer.addArrangedSubview(checkboxView);
                    customOptionMultiSelectView.stackViewContainer.addConstraint(NSLayoutConstraint(item: checkboxView, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: customOptionMultiSelectView.stackViewContainer, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 0));
                    customOptionMultiSelectView.stackViewContainer.addConstraint(NSLayoutConstraint(item: checkboxView, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: customOptionMultiSelectView.stackViewContainer, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 0));
                    
                    checkboxView.checkboxButton.setTitle(val["title"]!+" + "+productInfoArray["currency_symbol"]!+val["price"]!, for: UIControl.State());
                    checkboxView.checkboxButton.addTarget(self, action: #selector(ProductSinglePageViewController.checkCustomOptionCheckbox(_:)), for: UIControl.Event.touchUpInside);
                    
                    let tag = Int(val["option_type_id"]!)!;
                    checkboxView.checkboxButton.tag = tag;
                    self.custom_price_array[tag] = val["price"]!;
                    
                }
                
                customOptionMultiSelectView.heightOfLabel.constant = translateAccordingToDevice(30);
                checkboxViewStackViewContainerHeight = translateAccordingToDevice(30)+(CGFloat(tempArray.count)*translateAccordingToDevice(40))+20;
                heightOfCustomOption = heightOfCustomOption+checkboxViewStackViewContainerHeight;
                
                customOptionsStackView.addArrangedSubview(customOptionMultiSelectView);
                customOptionMultiSelectView.heightAnchor.constraint(equalToConstant: checkboxViewStackViewContainerHeight).isActive = true;
                customOptionsStackView.addConstraint(NSLayoutConstraint(item: customOptionMultiSelectView, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: customOptionsStackView, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 0));
                customOptionsStackView.addConstraint(NSLayoutConstraint(item: customOptionMultiSelectView, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: customOptionsStackView, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 0));
                break;
                
            default:
                break;
                
            }
            
        }
        
        heightOfCustomOption = heightOfCustomOption+CGFloat((10*custom_option.count));
        backgroundViewForStackView.heightAnchor.constraint(equalToConstant: heightOfCustomOption).isActive = true;
    }
    
    func textViewDidBeginEditing(_ textField: UITextView) {
        
        if let _ = self.datePickerTagsArray.firstIndex(of: textField.tag) {
        }
        else{
            return;
        }
        
        textField.resignFirstResponder();// to avoid keyboard to appear
        globalTextFieldTag = textField.tag;
        
        
        let visualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .light));
        visualEffectView.backgroundColor = UIColor(hexString: "#ECF0F1");
        visualEffectView.translatesAutoresizingMaskIntoConstraints = false;
        self.view.addSubview(visualEffectView);
        self.view.addConstraint(NSLayoutConstraint(item: visualEffectView, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 0));
        self.view.addConstraint(NSLayoutConstraint(item: visualEffectView, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 0));
        self.view.addConstraint(NSLayoutConstraint(item: visualEffectView, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1, constant: 0));
        self.view.addConstraint(NSLayoutConstraint(item: visualEffectView, attribute: NSLayoutConstraint.Attribute.bottom, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: 0));
        
        if( self.custom_option[String(textField.tag)]!["type"]! != "date_time" ){
            
            let customOptionDatePickerView = CustomOptionDatePickerView();
            customOptionDatePickerView.translatesAutoresizingMaskIntoConstraints = false;
            
            if( self.custom_option[String(textField.tag)]!["type"]! == "time" ){
                customOptionDatePickerView.introLabel.text = "Select Time".localized.uppercased();
                customOptionDatePickerView.datePicker.datePickerMode = UIDatePicker.Mode.time;
            }
            else{
                customOptionDatePickerView.introLabel.text = "Select Date".localized.uppercased();
                customOptionDatePickerView.datePicker.datePickerMode = UIDatePicker.Mode.date;
            }
            customOptionDatePickerView.topLabel.text = self.custom_option[String(globalTextFieldTag)]!["title"]!;
            customOptionDatePickerView.cancelButton.addTarget(self, action: #selector(ProductSinglePageViewController.dismissDatePickerSection(_:)), for: UIControl.Event.touchUpInside);
            customOptionDatePickerView.selectButton.addTarget(self, action: #selector(ProductSinglePageViewController.selectDateAndDismissDatePickerSection(_:)), for: UIControl.Event.touchUpInside);
            
            visualEffectView.addSubview(customOptionDatePickerView);
            visualEffectView.addConstraint(NSLayoutConstraint(item: customOptionDatePickerView, attribute: NSLayoutConstraint.Attribute.centerX, relatedBy: NSLayoutConstraint.Relation.equal, toItem: visualEffectView, attribute: NSLayoutConstraint.Attribute.centerX, multiplier: 1, constant: 0));
            visualEffectView.addConstraint(NSLayoutConstraint(item: customOptionDatePickerView, attribute: NSLayoutConstraint.Attribute.centerY, relatedBy: NSLayoutConstraint.Relation.equal, toItem: visualEffectView, attribute: NSLayoutConstraint.Attribute.centerY, multiplier: 1, constant: 0));
            visualEffectView.addConstraint(NSLayoutConstraint(item: customOptionDatePickerView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: translateAccordingToDevice(280)));
            visualEffectView.addConstraint(NSLayoutConstraint(item: customOptionDatePickerView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: translateAccordingToDevice(300)));
            
        }
        else{
            
            let customOptionDatePickerView = CustomOptionDateAndTimePickerView();
            customOptionDatePickerView.translatesAutoresizingMaskIntoConstraints = false;
            
            customOptionDatePickerView.introLabel.text = "Select Date And Time".localized.uppercased();
            customOptionDatePickerView.topLabel.text = self.custom_option[String(globalTextFieldTag)]!["title"]!;
            
            customOptionDatePickerView.cancelButton.addTarget(self, action: #selector(ProductSinglePageViewController.dismissDatePickerSection(_:)), for: UIControl.Event.touchUpInside);
            customOptionDatePickerView.selectButton.addTarget(self, action: #selector(ProductSinglePageViewController.selectDateAndDismissDatePickerSection(_:)), for: UIControl.Event.touchUpInside);
            
            visualEffectView.addSubview(customOptionDatePickerView);
            visualEffectView.addConstraint(NSLayoutConstraint(item: customOptionDatePickerView, attribute: NSLayoutConstraint.Attribute.centerX, relatedBy: NSLayoutConstraint.Relation.equal, toItem: visualEffectView, attribute: NSLayoutConstraint.Attribute.centerX, multiplier: 1, constant: 0));
            visualEffectView.addConstraint(NSLayoutConstraint(item: customOptionDatePickerView, attribute: NSLayoutConstraint.Attribute.centerY, relatedBy: NSLayoutConstraint.Relation.equal, toItem: visualEffectView, attribute: NSLayoutConstraint.Attribute.centerY, multiplier: 1, constant: 0));
            visualEffectView.addConstraint(NSLayoutConstraint(item: customOptionDatePickerView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: translateAccordingToDevice(280)));
            visualEffectView.addConstraint(NSLayoutConstraint(item: customOptionDatePickerView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: translateAccordingToDevice(400)));
        }
    }
    @objc func dismissDatePickerSection(_ sender:UIButton){
        if let visualEffectView = sender.superview?.superview?.superview as? UIVisualEffectView{
            visualEffectView.removeFromSuperview();
        }
    }
    @objc func selectDateAndDismissDatePickerSection(_ sender:UIButton){
        if let textField = self.view.viewWithTag(globalTextFieldTag) as? UITextView{
            if let customOptionDateAndTimePickerView = sender.superview?.superview as? CustomOptionDateAndTimePickerView{
                
                let dateFormatter = DateFormatter();
                let timeFormatter = DateFormatter();
                timeFormatter.timeStyle = DateFormatter.Style.medium;
                dateFormatter.dateFormat = "MM/dd/yyyy";
                let date_string = dateFormatter.string(from: customOptionDateAndTimePickerView.datePicker.date);
                let time_string = timeFormatter.string(from: customOptionDateAndTimePickerView.timePicker.date);
                textField.text = date_string+" || "+time_string;
                
                let key = customOptionDateAndTimePickerView.topLabel.text!;
                let price = self.custom_price_array[globalTextFieldTag]!;
                self.priceCalculationArray[key] = Float(price);
                self.updateProductPrice();
            }
            else if let customOptionDatePickerView = sender.superview?.superview as? CustomOptionDatePickerView{
                print(self.custom_option[String(globalTextFieldTag)] as Any);
                let dateFormatter = DateFormatter();
                let timeFormatter = DateFormatter();
                timeFormatter.timeStyle = DateFormatter.Style.medium;
                dateFormatter.dateFormat = "MM/dd/yyyy";
                
                var date_string = dateFormatter.string(from: customOptionDatePickerView.datePicker.date);
                if(self.custom_option[String(globalTextFieldTag)]!["type"] == "time"){
                    date_string = timeFormatter.string(from: customOptionDatePickerView.datePicker.date);
                }
                textField.text = date_string;
                
                let key = customOptionDatePickerView.topLabel.text!;
                let price = self.custom_price_array[globalTextFieldTag]!;
                self.priceCalculationArray[key] = Float(price);
                self.updateProductPrice();
            }
            
        }
        
        if let visualEffectView = sender.superview?.superview?.superview as? UIVisualEffectView{
            visualEffectView.removeFromSuperview();
        }
        
    }
    
    func textViewDidChange(_ textView: UITextView) {
        let key = self.custom_option[String(textView.tag)]!["title"]!;
        if(textView.text.count > 0){
            let price = self.custom_price_array[textView.tag]!;
            self.priceCalculationArray[key] = Float(price);
        }
        else{
            self.priceCalculationArray[key] = Float(0.0);
        }
        self.updateProductPrice();
    }
    
    
    @objc func checkCustomOptionCheckbox(_ sender:UIButton){
        
        let buttonText = (sender.titleLabel?.text)!;
        let price = self.custom_price_array[sender.tag]!;
        
        if let checkboxView = sender.superview?.superview as? CheckboxView {
            
            if(checkboxView.checkboxButtonImage!.image == UIImage(named:"UncheckedCheckbox")){
                checkboxView.checkboxButtonImage.image = UIImage(named: "CheckedCheckbox");
                self.priceCalculationArray[buttonText] = Float(price);
            }
            else{
                checkboxView.checkboxButtonImage.image = UIImage(named: "UncheckedCheckbox");
                self.priceCalculationArray[buttonText] = Float(0.0);
            }
        }
        self.updateProductPrice();
    }
    
    
    @objc func showCustomOptionDropdown(_ sender:UIButton){
        
       
        let tempArray = self.custom_option_options[String(sender.tag)]!;
        var dropDownArray = [String:String]();
        
        for (val) in tempArray{
            dropDownArray[val["title"]!+" + "+productInfoArray["currency_symbol"]!+val["price"]!] = val["option_type_id"]!;
        }
        
        var ArrayToUse = ["-- Select --".localized];
        ArrayToUse += Array(dropDownArray.keys);
        
        dropDown.dataSource = ArrayToUse;
        dropDown.selectionAction = {(index, item) in
            sender.setTitle(item, for: UIControl.State());
            
            if let customOptionDropDownView = sender.superview?.superview as? CustomOptionDropDownView{
                if let key = customOptionDropDownView.topLabel.text{
                    if(dropDownArray[item] != nil){
                        let option_type_id = Int(dropDownArray[item]!)!;
                        let price = self.custom_price_array[option_type_id]!
                        self.dropDownTrackingArray[sender.tag] = option_type_id;
                        self.priceCalculationArray[key] = Float(price);
                    }
                    else{
                        self.dropDownTrackingArray[sender.tag] = -1;
                        self.priceCalculationArray[key] = Float(0.0);
                    }
                }
            }
            self.updateProductPrice();
        }
        
        dropDown.anchorView = sender
        dropDown.bottomOffset = CGPoint(x: 0, y:sender.bounds.height)
        if dropDown.isHidden {
            let _ = dropDown.show();
        } else {
            dropDown.hide();
        }
    }
    
    func fetchCustomOptionsToPost()->String{
        
        if(self.custom_option.count == 0){
            return "{}";
        }
        var counter = 0;
        var custom_options_string = "{";
        
        for (key,val) in self.custom_option{
            custom_options_string += "\""+key+"\":";
            switch(val["type"]!){
            case "file":
                break;
                
            case "field":
                if let customOptionTextFieldView = customOptionsStackView.arrangedSubviews[counter] as? CustomOptionTextFieldView {
                    let text = customOptionTextFieldView.textView.text!;
                    custom_options_string += "\""+text+"\",";
                }
                break;
                
            case "area":
                if let customOptionTextViewView = customOptionsStackView.arrangedSubviews[counter] as? CustomOptionTextViewView {
                    let text = customOptionTextViewView.textView.text!;
                    custom_options_string += "\""+text+"\",";
                }
                break;
                
            case "date_time":
                if let CustomOptionTextFieldView = customOptionsStackView.arrangedSubviews[counter] as? CustomOptionTextFieldView {
                    let text = CustomOptionTextFieldView.textView.text!;
                    if(text != ""){
                        let textArr = text.components(separatedBy: " || ");
                        let dateArr = textArr[0].components(separatedBy: "/");
                        let timeArrInitial = textArr[1].components(separatedBy: " ");
                        let timeArr = timeArrInitial[0].components(separatedBy: ":");
                        
                        custom_options_string += "{";
                        custom_options_string += "\""+"month"+"\":\""+dateArr[0]+"\",";
                        custom_options_string += "\""+"day"+"\":\""+dateArr[1]+"\",";
                        custom_options_string += "\""+"year"+"\":\""+dateArr[2]+"\",";
                        
                        custom_options_string += "\""+"hour"+"\":\""+timeArr[0]+"\",";
                        custom_options_string += "\""+"minute"+"\":\""+timeArr[1]+"\",";
                        custom_options_string += "\""+"day_part"+"\":\""+timeArrInitial[1].lowercased()+"\"";
                        
                        custom_options_string += "},";
                    }
                    else{
                        custom_options_string += "{},";
                    }
                }
                break;
                
            case "date":
                if let CustomOptionTextFieldView = customOptionsStackView.arrangedSubviews[counter] as? CustomOptionTextFieldView {
                    let text = CustomOptionTextFieldView.textView.text!;
                    if(text != ""){
                        let dateArr = text.components(separatedBy: "/");
                        
                        custom_options_string += "{";
                        custom_options_string += "\""+"month"+"\":\""+dateArr[0]+"\",";
                        custom_options_string += "\""+"day"+"\":\""+dateArr[1]+"\",";
                        custom_options_string += "\""+"year"+"\":\""+dateArr[2]+"\"";
                        
                        custom_options_string += "},";
                    }
                    else{
                        custom_options_string += "{},";
                    }
                }
                break;
                
            case "time":
                if let CustomOptionTextFieldView = customOptionsStackView.arrangedSubviews[counter] as? CustomOptionTextFieldView {
                    let text = CustomOptionTextFieldView.textView.text!;
                    if(text != ""){
                        let timeArrInitial = text.components(separatedBy: " ");
                        let timeArr = timeArrInitial[0].components(separatedBy: ":");
                        
                        custom_options_string += "{";
                        custom_options_string += "\""+"hour"+"\":\""+timeArr[0]+"\",";
                        custom_options_string += "\""+"minute"+"\":\""+timeArr[1]+"\",";
                        custom_options_string += "\""+"day_part"+"\":\""+timeArrInitial[1].lowercased()+"\"";
                        
                        custom_options_string += "},";
                    }
                    else{
                        custom_options_string = custom_options_string + "{},";
                    }
                }
                break;
                
            case "drop_down", "radio":
                if let _ = customOptionsStackView.arrangedSubviews[counter] as? CustomOptionDropDownView {
                    
                    if(self.dropDownTrackingArray[Int(key)!] != nil && self.dropDownTrackingArray[Int(key)!] != -1)
                    {
                        custom_options_string = custom_options_string + "\""+String(self.dropDownTrackingArray[Int(key)!]!)+"\",";
                    }
                    else
                    {
                        custom_options_string = custom_options_string + "\"\",";
                    }
                    
                }
                break;
                
            case "multiple", "checkbox":
                if let customOptionMultiSelectView = customOptionsStackView.arrangedSubviews[counter] as? CustomOptionMultiSelectView{
                    
                    let stackView = customOptionMultiSelectView.stackViewContainer as UIStackView
                        
                        var inrCounter = 0;
                        custom_options_string = custom_options_string + "[";
                        for (tempVal) in self.custom_option_options[key]!{
                            let key = Int(tempVal["option_type_id"]!)!;
                            if let checkboxView = stackView.arrangedSubviews[inrCounter] as? CheckboxView{
                                if(checkboxView.checkboxButtonImage!.image == UIImage(named:"CheckedCheckbox")){
                                    custom_options_string += "\""+String(key)+"\",";
                                }
                            }
                            inrCounter += 1;
                        }
                        
                        if(custom_options_string.last! == ","){
                            custom_options_string = custom_options_string.substring(to: custom_options_string.index(before: custom_options_string.endIndex));
                        }
                        custom_options_string = custom_options_string + "],";
                        
                    
                    
                }
                break;
                
            default:
                break;
            }
            counter += 1;
        }
        
        if(custom_options_string.last! == ","){
            custom_options_string = custom_options_string.substring(to: custom_options_string.index(before: custom_options_string.endIndex));
        }
        custom_options_string += "}";
        print("custom_options_string");
        print(custom_options_string);
        return custom_options_string;
    }


}
