/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */
import UIKit

class cedMage: NSObject {
    static let singletonInstance = cedMage()
    var selectedWidgetIndexe = NSInteger()
    
    /*
     Get data from plist file cedMage Common File
     */
    class func getInfoPlist(fileName:String?,indexString:NSString) ->AnyObject?{
        
        
        let path = Bundle.main.path(forResource: fileName, ofType: "plist")
        let storedvalues = NSDictionary(contentsOfFile: path!)
        let response: AnyObject? = storedvalues?.object(forKey: indexString) as AnyObject?
        return response
    }
    
    /*
     store the slider Index siglet
     */
    func storeParameterInteger(parameter:NSInteger){
        selectedWidgetIndexe = parameter
    }
    
    /*
     Return sigleton instance
     */
    func getInfo() -> NSInteger{
        return selectedWidgetIndexe
    }
    
    /**
     UIcolor From color Code
     */
    class func UIColorFromRGB( colorCode: String, alpha: Float = 1.0) -> UIColor {
        var colorCode = colorCode
        colorCode = colorCode.components(separatedBy: "#").last!
        let scanner = Scanner(string:colorCode)
        var color:UInt32 = 0;
        scanner.scanHexInt32(&color)
        let mask = 0x000000FF
        let r = CGFloat(Float(Int(color >> 16) & mask)/255.0)
        let g = CGFloat(Float(Int(color >> 8) & mask)/255.0)
        let b = CGFloat(Float(Int(color) & mask)/255.0)
        return UIColor(red: r, green: g, blue: b, alpha: CGFloat(alpha))
    }
    
    class func delay(delay:Double, closure: @escaping ()->()) {
        DispatchQueue.main.asyncAfter(deadline: .now() + delay) {
            closure()
        }
        
    }
    //Mark: Get random Color
    class func getRandomColor() -> UIColor{
        let randomRed:CGFloat = CGFloat(drand48())
        let randomGreen:CGFloat = CGFloat(drand48())
        let randomBlue:CGFloat = CGFloat(drand48())
        return UIColor(red: randomRed, green: randomGreen, blue: randomBlue, alpha: 1.0)
        
    }
    
    
    //MArk: Get Modules From Url
    
    class func checkmoduless() {
        var url = Settings.baseUrl
        url += "mobiconnectadvcart/getmodulelist"
        UserDefaults.standard.set(String(describing:"true"), forKey: "featureEnabled");
        var request = URLRequest(url: URL(string:url)!)
        //let requestHeader = cedMage.getInfoPlist(fileName:"cedMage",indexString:"requestheader") as! String
        let version = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String
        let postString = ["parameters":["app_version":version!]].convtToJson()
        print(postString)
        request.httpMethod = "POST"
        request.httpBody = postString.data(using: String.Encoding.utf8.rawValue)
        if UserDefaults.standard.bool(forKey: "isLogin"){
            if let user = UserDefaults.standard.object(forKey: "userInfoDict") as? [String:String] {
                request.setValue(user["hashKey"], forHTTPHeaderField: "hashkey")
            }
            
        }
        // request.setValue(requestHeader, forHTTPHeaderField: "Mobiconnectheader")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let task = URLSession.shared.dataTask(with: request) {
            data, response, error in
            guard error == nil && data != nil else
            {
                return;
            }
            
            // check for http errors
            if let httpStatus = response as? HTTPURLResponse , httpStatus.statusCode != 200
            {
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(response)")
                return;
            }
            // code to fetch values from response :: start
            DispatchQueue.main.async
                {
                    
                    
                    guard let json = try? JSON(data: data!) else{return;}
                    if(json["update"].stringValue == "true"){
                        let updateView = UIStoryboard(name: "cedMageLogin", bundle: nil).instantiateViewController(withIdentifier: "cedMageFupdate") as? cedMageForceUpdate
                        
                        let app = UIApplication.shared
                        app.windows.first?.rootViewController = updateView
                        app.windows.first?.makeKeyAndVisible()
                        
                        return
                    }
                    print(json);
                    print("----\(json[0]["is_featured_enable"])")
                    //let defaults = UserDefaults.standard
                    UserDefaults.standard.set(String(describing: json[0]["is_featured_enable"]), forKey: "featureEnabled");
                    print("feature enabled-------\(UserDefaults.standard.value(forKey: "featureEnabled"))")
                    NotificationCenter.default.post(name: NSNotification.Name("loadHomePageAgain"), object: nil)
                    //makeEncodedString();
                    let moduleString = "VFdGblpVNWhkR2wyWlY5TmIySnBjbVYyYVdWM1RXRm5aVTVoZEdsMlpWOU5iMkpwY21WMmFXVjNUV0ZuWlU1aGRHbDJaVjlOYjJKcFkyOXVibVZqZEVGa2RrMWhaMlZPWVhScGRtVmZUVzlpYVdOdmJtNWxZM1JCWkhaTllXZGxUbUYwYVhabFgwMXZZbWxrWldGc2MwMWhaMlZPWVhScGRtVmZUVzlpYVdSbFlXeHpUV0ZuWlU1aGRHbDJaVjlOYjJKcGJtOTBhV1pwWTJGMGFXOXVjMDFoWjJWT1lYUnBkbVZmVFc5aWFXNXZkR2xtYVdOaGRHbHZibk5OWVdkbFRtRjBhWFpsWDAxdlltbGpiMjV1WldOMFRXRm5aVTVoZEdsMlpWOU5iMkpwWTI5dWJtVmpkRTFoWjJWT1lYUnBkbVZmVFc5aWFXTm9aV05yYjNWMFRXRm5aVTVoZEdsMlpWOU5iMkpwWTJobFkydHZkWFJOWVdkbFRtRjBhWFpsWDAxdlltbERiWE5OWVdkbFRtRjBhWFpsWDAxdlltbERiWE5OWVdkbFRtRjBhWFpsWDAxdlltbFRiMk5wWVd4TWIyZHBiazFoWjJWT1lYUnBkbVZmVFc5aWFWTnZZMmxoYkV4dloybHVUV0ZuWlU1aGRHbDJaVjlOYjJKcFYybHphR3hwYzNSTllXZGxUbUYwYVhabFgwMXZZbWxYYVhOb2JHbHpkRTFoWjJWT1lYUnBkbVZmVFc5aWFYTjBiM0psVFdGblpVNWhkR2wyWlY5TmIySnBjM1J2Y21VPW1vYmljb25uZWN0c2VjcmV0aGFzaA=="
                    let data =  Data(base64Encoded: moduleString, options: Data.Base64DecodingOptions(rawValue:0))
                    //  let data =  Data(base64EncodedString: moduleString, options:   NSData.Base64DecodingOptions(rawValue: 0))
                    
                    let headerkey =  Settings.headerKey
                    
                    let string =  NSString(data: data!, encoding: String.Encoding.utf8.rawValue)?.replacingOccurrences(of: headerkey, with: "")
                    
                    
                    let againdecode = Data(base64Encoded: string!, options: Data.Base64DecodingOptions(rawValue:0))
                    let savedModulearray = NSString(data: againdecode!, encoding: String.Encoding.utf8.rawValue)?.components(separatedBy: ",")
                    print("-----modules------")
                    //print(savedModulearray)
                    var activeModules = [String]()
                    let getmoduleString = NSMutableDictionary()
                    let defaults = UserDefaults.standard
                    print(json)
                    //                    for (key,value) in json
                    //                    {
                    //                        print(value)
                    //                        for(keys,values) in value
                    //                        {
                    //                            print(keys)
                    //                        }
                    //                    }
                    for (key,value) in json[0]["data"]["modules"]{
                        print(key)
                        getmoduleString[key] = value.stringValue
                        for string in savedModulearray! {
                            if  string.contains(value.stringValue){
                                activeModules.append((value.stringValue))
                                defaults.set(true, forKey: key)
                            }else{
                                defaults.set(true, forKey: key)
                            }
                        }
                        
                    }
                    defaults.set(activeModules, forKey: "activeModules")
                    print(activeModules);
            }
            
        }
        task.resume()
        
    }
    
    
    //Mark:func check Module String
    class func checkModule(string:String)->Bool{
        let defaults = UserDefaults.standard
        let mod = defaults.object(forKey: "activeModules") as? [String]
        if(mod != nil){
            if  mod!.contains(string){
                return true
            }else{
                return false
            }
        }else{
            return true
        }
    }
    
    
    //Mark: make base64 encoded string
    
    func makeEncodedString(){
        
        let url = Settings.baseUrl;
        let  moduleurl = url + "mobiconnectadvcart/getmodulelist"
        var request = URLRequest(url: URL(string:moduleurl)!)
        //_ = cedMage.getInfoPlist(fileName:"cedMage",indexString:"requestheader") as! String
        //request.setValue(requestHeader, forHTTPHeaderField: "Mobiconnectheader")
        let version = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String
        let postString = ["parameters":["app_version":version!]].convtToJson()
        print(postString)
        request.httpMethod = "POST"
        request.httpBody = postString.data(using: String.Encoding.utf8.rawValue)
        if UserDefaults.standard.bool(forKey: "isLogin"){
            if let user = UserDefaults.standard.object(forKey: "userInfoDict") as? [String:String] {
                request.setValue(user["hashKey"], forHTTPHeaderField: "hashkey")
            }
            
        }
        // request.setValue(requestHeader, forHTTPHeaderField: "Mobiconnectheader")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let task = URLSession.shared.dataTask(with: request) {
            data, response, error in
            guard error == nil && data != nil else
            {
                return;
            }
            
            // check for http errors
            if let httpStatus = response as? HTTPURLResponse , httpStatus.statusCode != 200
            {
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(response)")
                return;
            }
            
            // code to fetch values from response :: start
            guard var json = try? JSON(data: data!) else{
                return;
            }
            json = json[0]
            // print(json["data"]["modules"]);
            var moduleString = String()
            for (key,value) in json["data"]["modules"]{
                moduleString += key+value.stringValue
            }
            let headerkey =  Settings.headerKey
            
            moduleString = (moduleString.data(using: String.Encoding.utf8)?.base64EncodedString())!
            print("----moduleString----")
            print(moduleString)
            let headerAppendkey = moduleString.appending(headerkey)
            
            let encodedstring = headerAppendkey.data(using: String.Encoding.utf8)?.base64EncodedString()
            print("DoubleEncoded Stringdksh")
            print(encodedstring as Any)
            let data =  Data(base64Encoded: encodedstring!, options: Data.Base64DecodingOptions(rawValue:0))//Data(base64EncodedString: encodedstring!, options:   Data.Base64DecodingOptions(rawValue: 0))
            
            let string =  NSString(data: data!, encoding: String.Encoding.utf8.rawValue)?.replacingOccurrences(of: headerkey, with: "")
            
            let againdecode = Data(base64Encoded: string!, options: Data.Base64DecodingOptions(rawValue:0))
            print( NSString(data: againdecode! as Data, encoding: String.Encoding.utf8.rawValue)?.replacingOccurrences(of: headerkey, with: "") as Any)
            // mobi_common.writeinfoplist("modulelist", datastr: headerAppendkey)
        }
        task.resume()
    }
    
    func getHyperlocalType() {
        let baseURL = Settings.baseUrl
        let endpoint = "mobihyperlocal/location/selection"
        guard let finalURL = URL(string: baseURL + endpoint) else {return}
 
        URLSession.shared.dataTask(with: finalURL) { (data, _, error) in
            guard let data = data else { return }
            do {
                print(finalURL)
                let json = try JSON(data:data)
                print(json)
                UserDefaults.standard.set(json[0]["data"]["filter_location_type"].stringValue, forKey: "filterType")
                UserDefaults.standard.set(json[0]["data"]["filter_location_title"].stringValue, forKey: "filterTitle")
            } catch {
                print(error.localizedDescription)
            }
        }.resume()
    
    }
    //MARK: Get Cart Count
    func getCartCount(){
        let url = Settings.baseUrl;
        let getAccount = url + "mobiconnect/checkout/getcartcount/"
        var cartReq = URLRequest(url: URL(string: getAccount)! )
        let defaults = UserDefaults.standard
        var cartId = String()
        
        var postString = "";
        var postData = [String:String]()
        if(!defaults.bool(forKey: "isLogin")){
            let v = defaults.object(forKey: "cartId") as? String
            if(v != nil){
                cartId = v!
                postData["cart_id"] = cartId;
            }else{
                postData["cart_id"] = "0"
                
            }
        }
        else if(defaults.bool(forKey: "isLogin")){
            let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
            print(userInfoDict["hashKey"] as Any)
            let hashKey = userInfoDict["hashKey"]
            let customerId = userInfoDict["customerId"]
            postData["hashkey"] = hashKey;
            postData["customer_id"] = customerId;
            
            
            if(defaults.object(forKey: "userCartId") != nil)
            {
                let cart_id = defaults.object(forKey: "userCartId") as? String;
                postData["cart_id"] = cart_id;
            }else{
                postData["cart_id"] = "0"
            }
            
        }
        
        
        postString = ["parameters":postData].convtToJson() as String
        print(postString)
        cartReq.httpMethod = "POST"
        cartReq.httpBody = postString.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue));
        let requestHeader =  Settings.headerKey//cedMage.getInfoPlist(fileName:"cedMage",indexString:"requestheader") as! String
        cartReq.setValue("application/json", forHTTPHeaderField: "Content-Type")
        cartReq.setValue(requestHeader, forHTTPHeaderField: "Mobiconnectheader")
        if UserDefaults.standard.bool(forKey: "isLogin"){
            if let user = UserDefaults.standard.object(forKey: "userInfoDict") as? [String:String] {
                cartReq.setValue(user["hashKey"], forHTTPHeaderField: "hashkey")
            }
            
        }
        print(postString)
        print(getAccount)
        //var error: NSError?
        var response:URLResponse?
        do {
            if #available(iOS 9, *){
                let data = try NSURLConnection.sendSynchronousRequest(cartReq, returning: &response)
                //checking for network error
                let httpResponse = response as! HTTPURLResponse
                // print(httpResponse)
                if(httpResponse.statusCode != 200)
                {
                    DispatchQueue.main.async {
                        
                    }
                }
                
                // code to fetch values from response :: start
                guard let json = try? JSON(data: data) else{return;}
                print("json----\(json)")
                if(json[0]["success"].stringValue == "true"){
                    if(json[0]["items_count"].stringValue == ""){
                        defaults.set("0", forKey: "items_count")
                    }else{
                        defaults.set(json[0]["items_count"].stringValue, forKey: "items_count")
                    }
                    defaults.setValue(json[0]["gender"].stringValue, forKey: "gender")
                    defaults.setValue(json[0]["name"].stringValue, forKey: "name")
                    if (defaults.value(forKey: "storeId") == nil){
                        defaults.setValue(json[0]["default_store"].stringValue, forKey: "storeId")
                    }
                    defaults.setValue(json[0]["webview_checkout"].stringValue, forKey: "checkoutEnable")
                    if (UserDefaults.standard.value(forKey: "mageAppLang") == nil){
                        let lang=json[0]["locale"].stringValue
                        let language=lang.components(separatedBy: "_")
                        
                        if language[0]=="ar"
                        {
                            UserDefaults.standard.set(["ar","en","fr"], forKey: "mageAppLang")
                        }
                        else
                        {
                            UserDefaults.standard.set([language[0],"ar","fr"], forKey: "mageAppLang")
                        }
                    }
                    
                    if let locale = UserDefaults.standard.value(forKey: "mageAppLang") as? [String] {
                        if locale[0]=="ar"
                        {
                            UIView.appearance().semanticContentAttribute = .forceRightToLeft
                        }
                        else
                        {
                            UIView.appearance().semanticContentAttribute = .forceLeftToRight
                        }
                    }
                    
                    //load_app()
                }
            }
            
        }catch {
            print("Error")
        }
        
        //        let vtask = URLSession.shared.dataTask(with: cartReq){
        //            data,response,error in
        //            guard error == nil && data != nil else
        //            {
        //                return;
        //            }
        //            // check for http errors
        //            if let httpStatus = response as? HTTPURLResponse , httpStatus.statusCode != 200
        //            {
        //                print("statusCode should be 200, but is \(httpStatus.statusCode)")
        //                print("response = \(response)")
        //                return;
        //            }
        //
        //            // code to fetch values from response :: start
        //            let json = JSON(data: data!);
        //            if(json["items_count"].stringValue == ""){
        //                defaults.set("0", forKey: "cart_items")
        //            }else{
        //                defaults.set(json["items_count"].stringValue, forKey: "cart_items")
        //            }
        //
        //            defaults.setValue(json["default_store"].stringValue, forKey: "storeId")
        //        }
        //        vtask.resume()
    }
    func setcartCount(tabBar:UITabBarController?){
        let cart_items = UserDefaults.standard.string(forKey: "items_count")
        tabBar?.tabBar.items?[2].badgeValue = cart_items
    }
    
    
    func createFolder(name:String)->String?{
        let paths = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
        let documentsDirectory: String = paths[0]
        let dataPath = documentsDirectory.appending(name)
        print("**\(dataPath)")
        
        do {
            try FileManager.default.createDirectory(atPath: dataPath , withIntermediateDirectories: false, attributes: nil)
        } catch let error as NSError {
            print(error.localizedDescription);
        }
        
        let writePath = dataPath.appending("/test.png")
        do { try UIImage(named: "login")!.pngData()?.write(to: URL(fileURLWithPath:writePath), options: NSData.WritingOptions.completeFileProtectionUnlessOpen)
        }
        catch{
            
        }
        return dataPath
        
    }
    func reSizeImageAccording2View(image: UIImage, targetSize: CGSize) -> UIImage? {
        let size = image.size
        
        let widthRatio  = targetSize.width  / image.size.width
        let heightRatio = targetSize.height / image.size.height
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width:size.width * heightRatio, height:size.height * heightRatio)
        } else {
            newSize = CGSize(width:size.width * widthRatio,  height:size.height * widthRatio)
        }
        
        let rect = CGRect(x:0, y:0, width:newSize.width, height:newSize.height)
        
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
    
    func addGradient(view:UIView){
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = view.frame
        gradient.colors = [UIColor.green.cgColor, UIColor.black.cgColor,UIColor.blue.cgColor]
        gradient.locations = [0.0, 0.25, 0.75, 1.0]
        view.layer.addSublayer(gradient)
    }
//    func sendRequest(url:String,params:Dictionary<String,String>?,store:Bool = true){
//        let httpUrl = cedMage.getInfoPlist(fileName:"cedMage",indexString: "cedBaseUrl") as! String
//        let reqUrl = httpUrl+url
//
//        let postString=Dictionary<String,Dictionary<String,String>>()
//        let postString1:NSString=""
//        print(reqUrl)
//        var makeRequest = URLRequest(url: URL(string: reqUrl)!)
//
//        _ = cedMage.getInfoPlist(fileName:"cedMage",indexString: "requestheader") as! String
//
//
//
//
//        print(reqUrl)
//        print(postString)
//        makeRequest.httpBody = postString1.data(using: String.Encoding.utf8.rawValue)
//        makeRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
//        let task = URLSession.shared.dataTask(with: makeRequest, completionHandler: {data,response,error in
//            // check for http errors
//            if let httpStatus = response as? HTTPURLResponse , httpStatus.statusCode != 200
//            {
//                DispatchQueue.main.async
//                    {
//                        self.sendRequest(url: url,params:params,store:store)
//                        print("poststring=\(postString1)")
//                        print(NSString(data: data!, encoding: String.Encoding.utf8.rawValue) ?? "")
//                        print("statusCode should be 200, but is \(httpStatus.statusCode)")
//
//                        print("response = \(response)")
//                        self.recieveResponse(data: data, requestUrl: url, response: response)
//                }
//                return;
//            }
//
//            // code to fetch values from response :: start
//
//
//            guard error == nil && data != nil else
//            {
//                DispatchQueue.main.async
//                    {
//                        //                        cedMageHttpException.showAlertView(me: self, msg: error?.localizedDescription, title: "Error".localized )
//                        self.sendRequest(url: url,params:params,store:store)
//                        print("error=\(error)")
//
//                }
//                return ;
//            }
//
//            DispatchQueue.main.async
//                {
//
//                    self.recieveResponse(data: data, requestUrl: url, response: response)
//
//            }
//        })
//
//        task.resume()
//    }
//
//
//    func recieveResponse(data:Data?,requestUrl:String?,response:URLResponse?){
//
//
//        if (requestUrl == "mobiconnectstore/getlist") {
//            let json = JSON(data: data!);
//            print("^*^%^&%^&%&^%^&\(json)")
//            if json[0]["store_data"].arrayValue.count == 1{
//                let store_id = json[0]["store_data"][0]["store_id"].stringValue
//                UserDefaults.standard.setValue(store_id, forKey: "storeId")
//                selectStore(store: store_id)
//            }else{
////                for store in  json[0]["store_data"].arrayValue{
////                    let group_id = store["group_id"].stringValue
////                    //print(group_id)
////                    let code = store["code"].stringValue
////                    let store_id = store["store_id"].stringValue
////                    let name = store["name"].stringValue
////                    let is_active = store["is_active"].stringValue
////                    let storeobject = ["group_id":group_id,"code":code,"store_id":store_id,"name":name,"is_active":is_active]
////                    self.Stores.append(storeobject)
////
////                }
////                self.showStores()
//            }
//
//        }
//        else
//        {
//            let view = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "mainView") as? cedMageSideDrawer
//            self.present(view!, animated: true, completion: nil)
//        }
//
//
//    }
//
//    func selectStore(store:String?){
//        self.sendRequest(url: "mobiconnectstore/setstore/"+store!, params: nil)
//    }
}


extension UILabel {
    func setFont(fontFamily:String,fontSize:CGFloat){
        self.font = UIFont(name: fontFamily, size: fontSize)
        
    }
}
extension Dictionary{
    func convtToJson() -> NSString {
        do {
            let json = try JSONSerialization.data(withJSONObject: self, options: JSONSerialization.WritingOptions.prettyPrinted)
            return NSString(data: json, encoding: String.Encoding.utf8.rawValue)!
        }catch {
            return ""
        }
    }
}
extension UIView{
    func setThemeColor(){
        let color = Settings.themeColor
        self.backgroundColor = color;
        /*if color == "#FFFFFF" {
            if self is UIButton {
                
                self.backgroundColor = .black
                return
            }
            if self is UILabel {
                if let label = self as? UILabel {
                    label.textColor = .black
                }
            }
          return;
        }*/
        //        if color == "#000000" {
        //            if self is UIButton {
        //
        //                self.backgroundColor = .black
        //                if let buttom = self as? UIButton{
        //                    buttom.setTitleColor(UIColor(hexString: "#d99110"), for: .normal)
        //                }
        //                return
        //            }
        //            if self is UILabel {
        //                if let label = self as? UILabel {
        //                    label.textColor = UIColor(hexString: "#d99110")
        //                }
        //            }
        //        }
        //self.backgroundColor = cedMage.UIColorFromRGB(colorCode: color)
    }
}

//MARK: Overload Print function
func print(items:Any...){
  
}

func print(_ items: Any..., separator: String = " ", terminator: String = "\n") {
    let debug = Settings.debugMode
    if(debug){
        Swift.print(items[0], separator:separator, terminator: terminator)
    }
    let layoutdebug = Settings.layoutDebug
    if(layoutdebug){
        UserDefaults.standard.setValue(false, forKey: "_UIConstraintBasedLayoutLogUnsatisfiable")
    }
}


@IBDesignable class GradientView: UIView {
    @IBInspectable var topColor: UIColor = UIColor.white
    @IBInspectable var bottomColor: UIColor = UIColor.white
    
    override class var layerClass: AnyClass {
        return CAGradientLayer.self
    }
    
    override func layoutSubviews() {
        (layer as! CAGradientLayer).startPoint = CGPoint(x: 0.0, y: 0.5)
        (layer as! CAGradientLayer).endPoint = CGPoint(x: 1.0, y: 0.5)
        (layer as! CAGradientLayer).colors = [topColor.cgColor, bottomColor.cgColor]
    }
}


@IBDesignable class GradientViewLabel: UILabel {
    @IBInspectable var topColor: UIColor = UIColor.white
    @IBInspectable var bottomColor: UIColor = UIColor.white
    
    override class var layerClass: AnyClass {
        return CAGradientLayer.self
    }
    
    override func layoutSubviews() {
        (layer as! CAGradientLayer).startPoint = CGPoint(x: 0.0, y: 0.5)
        (layer as! CAGradientLayer).endPoint = CGPoint(x: 1.0, y: 0.5)
        (layer as! CAGradientLayer).colors = [topColor.cgColor, bottomColor.cgColor]
    }
}


