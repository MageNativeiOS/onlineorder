/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */


import UIKit

extension ProductSinglePageViewController{
  
  func fetchAllProductTypeData(jsonResponse:JSON){
    
    let jsonResponse = jsonResponse[0]
    
    if(jsonResponse.stringValue == "return"){
      self.renderNoDataImage(view: self, imageName: "noProduct")
      return
    }
    
    //fetching product-images :: starts
    for image in jsonResponse["data"]["product-image"].arrayValue {
      var imageData = [String:String]()
      for (key,val) in image {
        imageData[key] = val.stringValue
      }
      self.productImgsArray.append(imageData)
    }
    
    //fetching product-images :: ends
    
    //fetching product-general-information :: starts
    self.productInfoArray["product-id"] = (jsonResponse["data"]["product-id"][0]).stringValue;
    self.productInfoArray["type"] = (jsonResponse["data"]["type"][0]).stringValue;
    self.productInfoArray["product-url"] = (jsonResponse["data"]["product-url"][0].stringValue)
    self.productInfoArray["item_id"] = (jsonResponse["data"]["item_id"][0]).stringValue;
    self.productInfoArray["product-name"] = (jsonResponse["data"]["product-name"][0]).stringValue;
    self.productInfoArray["main-prod-img"] = (jsonResponse["data"]["main-prod-img"]).stringValue;
    self.productInfoArray["Inwishlist"] = (jsonResponse["data"]["Inwishlist"][0]).stringValue;
    self.productInfoArray["status"] = (jsonResponse["data"]["status"]).stringValue;
    self.productInfoArray["available"] = (jsonResponse["data"]["available"]).stringValue;
    self.more_infoArray["more_info"] = (jsonResponse["data"]["more_info"]).array;
    self.productInfoArray["currency_symbol"] = (jsonResponse["data"]["currency_symbol"]).stringValue;
    self.productInfoArray["stock"] = (jsonResponse["data"]["stock"][0]).stringValue;
    self.productInfoArray["show-both-price"] = (jsonResponse["data"]["show-both-price"]).stringValue;
    self.productInfoArray["regular_price"] = (jsonResponse["data"]["price"][0]["regular_price"][0]).stringValue;
    self.productInfoArray["special_price"] = (jsonResponse["data"]["price"][0]["special_price"][0]).stringValue;
    self.productInfoArray["review_count"] = (jsonResponse["data"]["review_count"][0]).stringValue;
    self.productInfoArray["review"] = (jsonResponse["data"]["review"][0]).stringValue;
    
    self.productInfoArray["short-description"] = (jsonResponse["data"]["short-description"][0]).stringValue;
    self.productInfoArray["description"] = (jsonResponse["data"]["description"][0]).stringValue;
    
    self.productInfoArray["cms-content"]  = jsonResponse["data"]["cms-content"].stringValue
    self.productInfoArray["cms-title"]  = jsonResponse["data"]["cms-title"].stringValue
    
    self.productInfoArray["offer"] = (jsonResponse["data"]["offer"]).stringValue;
    //fetching product-general-information :: ends
    
    
    //Fetching ProductReview data start
    if(jsonResponse["data"]["product_review"] != nil ){
      if(jsonResponse["data"]["product_review"].stringValue.lowercased() != "no_review".lowercased()){
        
        for (_,val) in jsonResponse["data"]["product_review"]{
          var temp = [String:String]();
          temp["posted_on"] = val["posted_on"].stringValue;
          temp["review-title"] = val["review-title"].stringValue;
          temp["reviewDescription"] = val["review-description"].stringValue;
          temp["review-by"] = val["review-by"].stringValue
          temp["review_avg"] = val["review_avg"].stringValue
          productReviews.append(temp)
          
        }
      }
    }
    
    
    
    //Fetching Productreview Data ends
    
    
    //fetching related products :: start
    if(jsonResponse["data"]["related_product"] != nil){
      for (_,val) in jsonResponse["data"]["related_product"]{
        var temp = [String:String]();
        temp["product_id"] = val["product_id"].stringValue;
        temp["related_product-name"] = val["product_name"].stringValue;
        temp["related_product_image"] = val["product_image"].stringValue;
        temp["regular_price"] = val["price"]["regular_price"][0].stringValue;
        temp["special_price"] = val["price"]["special_price"][0].stringValue;
        temp["related_currency_symbol"] = val["currency_symbol"].stringValue;
        similarProductArray.append(temp);
      }
    }
    //fetching related products :: ends
    
    //fetching custom-options :: starts
    if(jsonResponse["data"]["custom_option"] != nil){
      for (_,val) in jsonResponse["data"]["custom_option"]{
        var temp_custom_option = [String:String]();
        var temp_custom_option_options = [[String:String]]();
        var temp_key = "";
        
        for (k,v) in val{
          if(k == "option_id"){
            temp_key = v.stringValue;
          }
          if(k == "option"){
            for (_,tempV) in v{
              var tempArray = [String:String]();
              
              for(inrK,inrV) in tempV{
                tempArray[inrK] = inrV.stringValue;
              }
              temp_custom_option_options.append(tempArray);
            }
            continue;
          }
          temp_custom_option[k] = v.stringValue;
        }
        
        custom_option[temp_key] = temp_custom_option;
        custom_option_options[temp_key] = temp_custom_option_options;
        
      }
    }
    //fetching custom-options :: ends
    
    
    // downloadable products
    if(self.productInfoArray["type"] == "downloadable"){
      
      self.downloadRelatedData["is_selection_required"] = jsonResponse["data"]["download-data"]["is_selection_required"].stringValue;
      self.downloadRelatedData["links_purchased_separately"] = jsonResponse["data"]["download-data"]["links_purchased_separately"].stringValue;
      self.downloadRelatedData["link_title"] = jsonResponse["data"]["download-data"]["link_title"].stringValue;
      self.downloadRelatedData["samples_title"] = jsonResponse["data"]["download-data"]["samples_title"].stringValue;
      
      for (_,linkData) in jsonResponse["data"]["download-data"]["links"]{
        var link_id = "";
        var tempLinkData = [String:String]();
        for (key,value) in linkData{
          if(key == "link-id"){
            link_id = value.stringValue;
          }
          tempLinkData[key] = value.stringValue;
        }
        self.links[link_id] = tempLinkData;
      }
      
      for (_,samplesData) in jsonResponse["data"]["download-data"]["samples"]{
        var sample_id = "";
        var tempSamplesData = [String:String]();
        for (key,value) in samplesData{
          if(key == "sample-id"){
            sample_id = value.stringValue;
          }
          tempSamplesData[key] = value.stringValue;
        }
        self.samples[sample_id] = tempSamplesData;
      }
    }
    // downloadable products
    
    // configurable products
    
    if(self.productInfoArray["type"] == "configurable"){
      var options = [[String:String]]()
      var i = 0
      for (value) in jsonResponse["data"]["config-data"]["attributes"] {
        
        self.attributes[i.description] = ["code":value.1["code"].stringValue,"id":value.1["id"].stringValue,"label":value.1["label"].stringValue]
        
        //FetchingOptions
        options.removeAll()
        for (val) in value.1["options"] {
          options.append(["id":val.1["id"].stringValue,"label":val.1["label"].stringValue,"value":val.1["value"].stringValue,"thumb":val.1["thumb"].stringValue])
          
        }
        self.config_attribute_array[value.1["id"].stringValue] = options
        i+=1
      }
      print(config_attribute_array)
      print(attributes)
      self.configIndexJson = jsonResponse["data"]["config-data"]["index"]
      //Fetching Ends
      
    }
    
    if(self.productInfoArray["type"] == "grouped"){
      
      let grouped_product = jsonResponse["data"]["grouped_product"];
      
      for grpPro in grouped_product{
        var temp = [String:String]();
        temp["regular_price"] = grpPro.1["price"]["regular_price"].stringValue;
        temp["special_price"] = grpPro.1["price"]["special_price"].stringValue;
        temp["group-description"] = grpPro.1["group-description"].stringValue;
        temp["group-product-name"] = grpPro.1["group-product-name"].stringValue;
        temp["griup_product_type"] = grpPro.1["griup_product_type"].stringValue;
        temp["stock"] = grpPro.1["stock"].stringValue;
        temp["group-short-description"] = grpPro.1["group-short-description"].stringValue;
        temp["group-prod-img"] = grpPro.1["group-prod-img"].stringValue;
        temp["group-product-id"] = grpPro.1["group-product-id"].stringValue;
        
        self.grouped_product_array.append(temp);
      }
    }
    
    if(self.productInfoArray["type"] == "bundle"){
      
      self.bundledpriceRange["from_price"] =  jsonResponse["data"]["bundleddata"]["from_price"].stringValue;
      self.bundledpriceRange["to_price"] =  jsonResponse["data"]["bundleddata"]["to_price"].stringValue;
      
      for (key,data) in jsonResponse["data"]["bundleddata"]["option name"]{
        var selection_array = [String:[String:String]]();
        for (key1,data1) in data["selection_name"]{
          var tempArray = [String:String]();
          tempArray["default_qty"] = data1["default_qty"].stringValue;
          tempArray["user_can_change_qty"] = data1["user_can_change_qty"].stringValue;
          tempArray["selection_is_default"] = data1["selection_is_default"].stringValue;
          tempArray["in_stock"] = data1["in_stock"].stringValue;
          tempArray["selection_id"] = data1["selection_id"].stringValue;
          tempArray["selection_price"] = data1["selection_price"].stringValue;
          
          selection_array[key1] = tempArray;
        }
        
        var tempExtra = [String:String]();
        tempExtra["required"] = data["required"].stringValue;
        tempExtra["type"] = data["type"].stringValue;
        tempExtra["id"] = data["id"].stringValue;
        
        self.bundleddata_extraInfo_array[key] = tempExtra;
        
        self.bundleddata_array[key] = selection_array;
        self.topLabelArray.append(key);
        self.bundlePriceArr[key]=[String:Float]();
        
      }
    }
    //MARK:-Giftcard Section Data
    
    if(self.productInfoArray["type"] == "giftcard"){
        self.productURLarray.append(contentsOf: jsonResponse["data"]["product-url"].arrayValue.map {
            $0.stringValue
        })
        var dictData = [String:String]()
            for (k,v) in jsonResponse["data"]["gift"]["gift_template"]["images"].dictionaryValue{
                for (u,w) in v.dictionaryValue{
                    dictData[u] = w.stringValue
                }
                
                self.giftData[k] = dictData
            }

    }
    self.renderProductSinglePage();
  }
}
