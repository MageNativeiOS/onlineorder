//
//  cedMageCheckTicketViewController.swift
//  ZeoMarket
//
//  Created by cedcoss on 04/04/19.
//  Copyright © 2019 MageNative. All rights reserved.
//

import UIKit

class cedMageCheckTicketViewController: cedMageViewController {
  @IBOutlet weak var emailField: UITextField!
  
  @IBOutlet weak var submit: UIButton!
  @IBOutlet weak var ticketId: UITextField!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    submit.setThemeColor()
    
  }
  
  @IBAction func CheckStatus(_ sender: UIButton) {
    if let email = emailField.text {
      if email  != "" {
        if let ticketId = ticketId.text {
          let ticket = ["ticket_id":ticketId,"email":email]
          if let viewcontrol = UIStoryboard(name: "cedMageAccounts", bundle: nil).instantiateViewController(withIdentifier: "cedMageSupportViewController") as? cedMageSupportViewController {
            viewcontrol.ticket = ticket
            self.navigationController?.pushViewController(viewcontrol, animated: true)
          }
        }else {
          cedMageHttpException.showAlertView(me: self, msg: "TicketId Is Required.", title: "Error")
        }
      }else {
        cedMageHttpException.showAlertView(me: self, msg: "Email Is Required.", title: "Error")
      }
    }
  }
}
