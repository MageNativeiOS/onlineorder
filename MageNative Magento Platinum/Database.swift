//
//  Database.swift
//  MageNative Magento Platinum
//
//  Created by Manohar Singh Rawat on 08/05/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

class User: NSObject {
    @objc dynamic var email = ""
    @objc dynamic var customerId = ""
    @objc dynamic var hashkey = ""
}
