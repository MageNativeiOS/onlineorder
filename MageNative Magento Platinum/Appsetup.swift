//
//  Appsetup.swift
//  MageNative Magento Platinum
//
//  Created by Manohar Singh Rawat on 26/05/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import Foundation
struct Settings{
    static var messageTopic = "magenativeios";
//    "http://demo.cedcommerce.com/magento4/vrushank/rest//V1/"
    static var baseUrl = "https://onlineorder.pl/rest/V1/";
    static var sellerUrl = "https://onlineorder.pl/"//"http://demo.cedcommerce.com/magento4/vrushank/";
    static var headerKey = "hashkey:3xNOO2C66mmJrJqV4R5xKVDskCQ0gu79f";
    static var themeColor: UIColor{
        if #available(iOS 13.0, *) {
        
          return UIColor { (UITraitCollection) -> UIColor in
            if UITraitCollection.userInterfaceStyle == .dark { return UIColor(hexString: "#001A27")! }
            else { return UIColor(hexString: "#001A27")! }
          }
        } else {
            return UIColor(hexString: "#001A27")!
        }
    } //= "#001A27";
    static var themeTextColor: UIColor{
        if #available(iOS 13.0, *) {
        
          return UIColor { (UITraitCollection) -> UIColor in
            if UITraitCollection.userInterfaceStyle == .dark { return UIColor(hexString: "#FFFFFF")! }
            else { return UIColor(hexString: "#FFFFFF")! }
          }
        } else {
            return UIColor(hexString: "#FFFFFF")!
        }
    } //= "#001A27";
    static var currentLocation: [String:String]? {
        return UserDefaults.standard.value(forKey: "userCurrentLocation") as? [String:String]
    }
    static var textColor = "#000000";
    static var debugMode = true;
    static var layoutDebug = true;
}
