//
//  cedMageMyWallet.swift
//  MageNative Magento Platinum
//
//  Created by Macmini on 12/12/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class cedMageMyWallet: MagenativeUIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    var walletData = [[String:String]]()
    var walletAmount = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        tableView.delegate = self
        tableView.dataSource = self
        tableView.estimatedRowHeight = 500
        tableView.separatorStyle = .none
        tableView.rowHeight = UITableView.automaticDimension
        let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
        let  customerId = userInfoDict["customerId"]!;
        let postString = ["customer_id":customerId];
        self.sendRequest(url: "mobiconnect/mobiwallet/wallettransaction", params: postString, store: false)
    }
    
   override func recieveResponse(data: Data?, requestUrl: String?, response: URLResponse?) {
        if let data = data {
            do {
            var json = try JSON(data:data)
            json = json[0]
        print(json)
            if json["data"]["status"].stringValue == "true" {
                for wallet in json["data"]["wallet_info"].arrayValue {
                    var tempArray  = [String:String]()
                    tempArray["amount"] = wallet["amount"].stringValue
                    tempArray["id"] = wallet["id"].stringValue
                    tempArray["order_id"] = wallet["order_id"].stringValue
                    tempArray["time"] = wallet["time"].stringValue
                    tempArray["action"] = wallet["action"].stringValue
                    tempArray["comment"] = wallet["comment"].stringValue
                    tempArray["transaction"] = wallet["transaction_with"].stringValue
                    self.walletData.append(tempArray)
                    
                }
                walletAmount = json["data"]["wallet_amount"].stringValue
                tableView.reloadData()
            }else{
                  tableView.reloadData()
                cedMageHttpException.showAlertView(me: self, msg: json["data"]["message"].stringValue, title: "Error")
            }
            }catch let error {
                print(error.localizedDescription)
            }
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
       
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(section == 1)
        {
            return walletData.count
        }
        return 1;
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.section == 0){
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "headerCell", for: indexPath) as! cedMageWalletHeaderCell
            if walletAmount != ""{
            cell.amountInWallet.text = "You have " + walletAmount + " in your wallet"
            }
        else{
            cell.amountInWallet.text = "You have $0 in your wallet"
        }
            cell.backgroundColor = UIColor.init(hexString: "#f2f2f2")

            return cell
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "myWalletCell", for: indexPath) as! cedMageMyWalletCell
            let amount = walletData[indexPath.row]
            if amount["action"] == "0" {
                cell.amountAction.text = "Amount Credited"
            }
            else{
                cell.amountAction.text = "Amount Debited"
            }
            cell.ammount.text = amount["amount"]
            cell.orderId.text = amount["order_id"]
            cell.placedOn.text = amount["time"]
            cell.transactionLabel.text = amount["transaction"]
            cell.commentLabel.text = amount["comment"]
            cell.placedOn.numberOfLines = 0
            cell.cellView.cardView();
            return cell
            
        }
        return UITableViewCell()
    }
 
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(indexPath.section==1)
        {
            return 170;
        }
        return 50
    }

    func addMoney(sender:UIButton){
        if let viewcontroll = UIStoryboard(name: "cedMageAccounts", bundle: nil).instantiateViewController(withIdentifier: "cedMageAddmoney") as? cedMageAddmoney {
        self.navigationController?.pushViewController(viewcontroll, animated: true)
        }
    }
}
