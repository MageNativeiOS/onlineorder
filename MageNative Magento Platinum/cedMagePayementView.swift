//
//  cedMagePayementView.swift
//  MageNative Magento Platinum
//
//  Created by CEDCOSS Technologies Private Limited on 20/07/17.
//  Copyright © 2017 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class cedMagePayementView: MagenativeUIViewController,UITableViewDelegate,UITableViewDataSource {
     //var razorpay: RazorpayCheckout!
    var isOrderReview = false;// to be used in case of order review
    var orderEmail = "";// to be used in case of order review
    var all_pro_qty=[String]()
    var cart_id = "";
    
    var products = [[String: String]]();
    var options_selected = [String:[String:String]]();
    var bundle_options = [String:[String:String]]();
    var bundle_options_values = [String:[String:[String]]]();
    var total = [String: String]();
    var items_count = "";
    var items_qty = "";
    var allowed_guest_checkout = "";
    //PayMent method
    var selectedPaymentMethod = String()
    
    var orderStatusData = [String:String]()
    var flag=false
    //payment checkout
    let clientTokenUrl  =     "mobiconnect/mobibrain/generatetokenpost"
    let trasactionUrl   =     "mobiconnect/mobibrain/transaction"
    let finalOrderCheck =     "mobiconnect/checkout/additionalinfo"
    let saveOrder       =     "mobiconnect/checkout/saveorder"
   // var apiClient : BTAPIClient!
    //var braintreeClient: BTAPIClient!
    var previousMethods = [[String:String]]()
    
    @IBOutlet weak var tableView: UITableView!
    var paymentsDetail = [[String:String]]()
    var clientToken = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.view.makeToast("Order is processing...", duration: 1, position: .center, title: nil, image: nil, style: nil, completion: nil)
        if(defaults.object(forKey: "cartId") != nil){
        cart_id = (defaults.object(forKey: "cartId") as? String)!;
        }
        self.loadData()
        

    }
    
    
    
    func loadData() {
        userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
        var urlToRequest = "mobiconnect/checkout/saveorder";
        let requestHeader = Settings.headerKey
        let baseURL = Settings.baseUrl
        
        urlToRequest = baseURL+urlToRequest;
        
        var postString = "";
        var postData = [String:String]()
        if cart_id == ""{
            postData["cart_id"] = "0";
        }else{
            postData["cart_id"] = cart_id;
        }
        
        postData["email"] = orderEmail;
        
        if defaults.bool(forKey: "isLogin") {
            if let hashKey = userInfoDict["hashKey"] {
            postData["hashkey"] = hashKey
            }
            
            if let customId = userInfoDict["customerId"] {
                postData["customer_id"] = customId
            }
        }
        postString = ["parameters":postData].convtToJson() as String
        print(postString)
        var request = URLRequest(url: URL(string: "\(urlToRequest)")!);
        request.httpMethod = "POST";
        request.httpBody = postString.data(using: String.Encoding.utf8);
        if UserDefaults.standard.bool(forKey: "isLogin"){
            if let user = UserDefaults.standard.object(forKey: "userInfoDict") as? [String:String] {
                request.setValue(user["hashKey"], forHTTPHeaderField: "hashkey")
            }
            
        }
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        cedMageLoaders.addDefaultLoader(me: self);
        let task = URLSession.shared.dataTask(with: request){
            
            // check for fundamental networking error
            data, response, error in
            guard error == nil && data != nil else{
                print("error=\(error)")
                DispatchQueue.main.async{
                    print(error?.localizedDescription ?? "localizedDescription");
                    cedMageLoaders.removeLoadingIndicator(me: self);
                }
                return;
            }
            
            // check for http errors
            if let httpStatus = response as? HTTPURLResponse , httpStatus.statusCode != 200{
                
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(response)")
                DispatchQueue.main.async{
                    cedMageLoaders.removeLoadingIndicator(me: self);
                    if let data = data {
                        if let string = NSString(data:data,encoding:String.Encoding.utf8.rawValue) as? String {
                        self.view.makeToast(string, duration: 2, position: .center, title: nil, image: nil, style: nil, completion: nil)
                        }
                    }
                    
                }
                
                return;
            }
            
            // code to fetch values from response :: start
            guard var jsonResponse = try? JSON(data: data!) else{return}
            jsonResponse = jsonResponse[0]
            if(jsonResponse != nil){
                
                DispatchQueue.main.async{
                    print(jsonResponse);
                    cedMageLoaders.removeLoadingIndicator(me: self);
                    
                    
                    if(jsonResponse["success"].stringValue == "true"){
                        self.orderStatusData["orderId"] = jsonResponse["order_id"].stringValue
                        self.orderStatusData["orderStatus"] = jsonResponse["success"].stringValue
                        self.total["grandtotal"] = jsonResponse["grandtotal"].stringValue
                       
                        print(self.selectedPaymentMethod)
                        if self.selectedPaymentMethod.lowercased() == "Braintree".lowercased(){
                            self.getClientToken()
                        }else if(self.selectedPaymentMethod.lowercased()) == "Razorpay".lowercased(){
                            
                            //self.loadRazorPay()
                        }
                        else{
                            self.defaults.removeObject(forKey: "guestEmail");
                            self.defaults.removeObject(forKey: "cartId");
                            self.defaults.setValue("0", forKey: "items_count")
                            self.view.makeToast("Order Placed Successfully.", duration: 2, position: .center, title: nil, image: nil, style: nil, completion: {
                                Void in
                                let storyboard = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil);
                                let viewController = storyboard.instantiateViewController(withIdentifier: "orderPlacedViewController") as! OrderPlacedViewController;
                                viewController.order_id = self.orderStatusData["orderId"]!
                                self.navigationController?.viewControllers = [viewController];
                            })
                        }
                        //self.navigationController?.present(viewController, animated: true, completion: {});
                        
                    }
                }
            }
        }
        task.resume();
    }
    
    override func recieveResponse(data: Data?, requestUrl: String?, response: URLResponse?) {
        if let data = data {
            guard var json = try? JSON(data:data) else{return;}
            print(json)
            json = json[0]
            print(json)
            if requestUrl == trasactionUrl {
                let paymentId = json["message"].stringValue
                print(NSString(data:data,encoding:String.Encoding.utf8.rawValue))
                if json["success"].stringValue == "true"{
                    self.afterPayment(payment_id: paymentId, failure: "false")
                }else{
                    
                    self.afterPayment(payment_id: paymentId, failure: "true")
                    
                }
                
            }else if requestUrl == clientTokenUrl {
                
                if json["success"].stringValue == "true" {
                    let clientToken = json["message"].stringValue
                    if json["creditCards"].arrayValue.count > 0 {
                        for creditCard in json["creditCards"].arrayValue {
                            let maskedNumber = creditCard["maskedNumber"].stringValue
                            let imgUrl = creditCard["imageUrl"].stringValue
                            let token  = creditCard["token"].stringValue
                            self.previousMethods.append(["maskedNumber":maskedNumber,"imgUrl":imgUrl,"token":token])
                        }
                    }
                    self.clientToken = clientToken
                    if previousMethods.count > 0 {
                        self.tableView.reloadData()
                        showDropIn(clientTokenOrTokenizationKey: clientToken)
                    }else{
                        showDropIn(clientTokenOrTokenizationKey: clientToken)
                    }
                }
                
            }else if requestUrl == finalOrderCheck{
                if json["success"].stringValue != "false" {
                    self.defaults.removeObject(forKey: "guestEmail");
                    self.defaults.removeObject(forKey: "cartId");
                    self.defaults.setValue("0", forKey: "items_count")
                    self.view.makeToast("Payment Successful.", duration: 2, position: .center, title: nil, image: nil, style: nil, completion: {
                        Void in
                        let storyboard = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil);
                        let viewController = storyboard.instantiateViewController(withIdentifier: "orderPlacedViewController") as! OrderPlacedViewController;
                        viewController.order_id = self.orderStatusData["orderId"]!
                        self.navigationController?.viewControllers = [viewController];
                    })
                }else if json["success"].stringValue == "false"{
                    
                    self.view.makeToast("Order Payment Failure.", duration: 2, position: .center, title: nil, image: nil, style: nil, completion: {
                        Void in
                        if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "cedMageMainDrawer") as? cedMageMainDrawer {
                            self.navigationController?.setViewControllers([viewController], animated: true)
                        }
                    })
                }
                
            }
        }
        
    }
    
    
    
    func userDidCancelPayment() {
        self.dismiss(animated: true, completion: nil)
        self.afterPayment(payment_id: "", failure: "true")
    }
    
    /* func drop(_ viewController: BTDropInViewController, didSucceedWithTokenization paymentMethodNonce: BTPaymentMethodNonce) {
        
    } */
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return paymentsDetail.count + 1
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row > paymentsDetail.count {
            let cell = UITableViewCell()
            cell.textLabel?.text = "ADD NEW PAYMENT METHOD"
            cell.textLabel?.fontColorTool()
            return cell
        }else{
            if let cell = tableView.dequeueReusableCell(withIdentifier: "mageWooProductCell1") as? cedMagepaymentmethodCell {
                if let imgUrl = paymentsDetail[indexPath.row]["imgUrl"] {
                    cell.paymentImage.sd_setImage(with: URL(string: imgUrl), placeholderImage: UIImage(named: "placeholder"))
                }
                cell.title.text = paymentsDetail[indexPath.row]["maskedNumber"]
                cell.title.fontColorTool()
                return cell
            }
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row < previousMethods.count {
         let paymethod = previousMethods[indexPath.row]
            if let token = paymethod["token"] {
                self.postNonceToServer(paymentMethodNonce: "",user:true,token:token)
            }
        }else{
            self.showDropIn(clientTokenOrTokenizationKey: clientToken)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
    }
    
    override func failedWithError(data:Data?,requestUrl:String?,response:URLResponse?){
        if requestUrl == finalOrderCheck{
            self.afterPayment(payment_id: "", failure: "true")
        }else if requestUrl == clientTokenUrl {
            self.showDropIn(clientTokenOrTokenizationKey: clientToken)
        }
    }


}
