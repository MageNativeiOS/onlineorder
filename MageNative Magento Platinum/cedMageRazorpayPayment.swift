//
//  cedMageRazorpayPayment.swift
//  MageNative Magento Platinum
//
//  Created by cedcoss on 11/06/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import Foundation

/*extension cedMagePayementView:RazorpayPaymentCompletionProtocol{
       func loadRazorPay(){
           // Do any additional setup after loading the view, typically from a nib.//
           razorpay = RazorpayCheckout.initWithKey("rzp_test_JmQfYqlnYHQW2O", andDelegate: self)
          showPaymentForm()
        
        
    }
    
    
    internal func showPaymentForm(){
        
        guard let currency = total["currency_symbol"] else{return}
        guard let total = self.total["grandtotal"] else {
            return;
        }
        guard let orderId = orderStatusData["orderId"] else{return}
        guard let totalDouble = Double(total) else{return;}
       
        let options: [String:Any] = [
         
           
                "amount": "\(totalDouble*100.00)"
                    , //This is in currency subunits. 100 = 100 paise= INR 1.
                    "currency":  "INR",//We support more that 92 international currencies.
                   // "description": "purchase description",
                   // "order_id": orderId,
                    "image": "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAhFBMVEX7+/v///8AAAD8/PxMTExYWFjr6+v19fWlpaWsrKzy8vK2trbn5+fW1tbFxcV+fn7d3d0pKSl0dHRjY2Ompqbg4OBHR0caGhpdXV3Ly8vS0tKwsLC5ubmcnJw/Pz+Li4tpaWkkJCQTExOQkJB6enowMDA3NzeDg4OdnZ0LCwtJSUkRERGeemDAAAAKUElEQVR4nO2dbXuiOhCGiWlBUNGqaBXUWu3a3fP//99JeAcnEJS8efF86bWKkHszk5kkE7XQq8tS3QDhGgjN10BovgZC8zUQmq+B0HwNhOZrIDRfA6H5GgjN10BovgZC8zUQpsJ6qidCjC19hVshWgl1xotFGC37CULV7ecRRt70AcLYyLXvwEQYLdiILMLEi1U3nVfY85ld1UiouuEdtJ91JMTUgTMbVR0TGpQTTicWI3bAhOSTRReqpmhU1kYnChjhkUXo5F2Il5s3XbVZ5r242zE6ESa0nJwQ481IX22yXsSzicNggbvQszIjxfhNNUaD3nLCIPTgTgQJLceuEm7febTa0oceua7tQ9syoe3acCdCL5IunFq5G1LCd7D/6wop4MrhurYPvVcI1zbciSxCp0bYmt3gBPDdkpUH4RrheNqJMKgTtj4QuXEPSgMkT3yC0LG7EuIU0JGYydYJA9vhJSQDzaITIbYSE/2UCQgRgjR9EKYmKs8Hk4c+R+h1IZTvg/FT5RFmYULybLJOuBBFKD1MZKoR/nYk5PfDxEQ/pQNKI1QQJrInyyDEqnyQ6glCzE+oygepAML+I76aMJE9XLyVYoUmakkhVJGqlR8vmhArCxOphBMq9cG4AYIJ1fpg3AKhhHmYULguLpZQtQ/GbRBJqC5VKzdCHCFWHCZSCczaVKZq5WaIytqw8jCRSpiVqg8TWUPEEGY+qLwHhRHqECaypoggVLHwy5QIQtZ0SUzRBm4pJBARLeAwga2r3z8j4fOvjc4gIFowwgS2tqM/odMrI0ZO+Ge07Ur4lJViZqqGrQ/y+s/B7q08BSH78EPu+SGXMEvV7jASwtFoewt6YUQouMW7yeIIIT/E7M2XjJDoa/asQxJPmn1lt+tM+IwfNqRqJULySP8ZhyTu55cKI6RaaUOqViEcjY6u9yAjRt76WL6VRMJVmqrBi4pVwtHofLG7GysxMPtyrt5IImHs+J+MYuE7QqLlohsj4Vss7+4ikZBtogxCcvnO4mbEyNqtgHvIJWTnojAhefqEb9Ahw8sErruSGC0aZ/QsQpIF/DqtERIh5/eH8XmJ0aJxRs8mJDo1ZwEkup/YH5Zopcf7TIaTkAw6e9agQ/7H9/fDixrCxkqFEuEZbOgnOPWgk4dP8Pr8LlKzNj7Cq30D23w/9UgmD5Bu9vVhQkE1USXCJULeARw3zpWpB508gP39c/AQyi1XQytd0jmIF4JVxd+3RcpIovvtG7pkE9KET3fCOH7776ABftFBhwwvX+C7736SI+hPmEyB4EHyX+RF/8B3lvnEywRCKwl0W4jkG3pxWw6bhhDGcwVmslLVz29lLiKB8PFosaxcRyPCEYYq6ViPJE8QyogWtbeYUT0TkA2YYqUF4/4KwxFdoYzOMELaEjSFs+vTFMzKDfLD4grk/daz849f1nKOUX5YYrTCchz8F7JXAHS00nMrYZLpZCsVK79phUNDK8VLDsIi01m2LBvrZ6XkWcH8u53QigedMTy89EMo8LwFyV/+cBDSjmxdnNKSMFswayfkkH5+mIr4wC5STCjMD1O1bUtzSlMr7VEDYX6dRD98blP0ruUa+mG/lRg6Wult1yejflZKq03eIo5QLp5Q6NzivPZ6qjbRz0qz2dP50lO1ibaEo9Hf69PVJnIIH5/jj0afu6cdUlc/zLXh3NwWQSh+JSrRn9+nStzQ47trsghpidv0cYfE3vissR/m+mJubrcjIhy9aeyHud4b15ualezwn/W10mzQcR8fdOjm1UZrK00Ub24/CImQ0/y+citNdQ0ed8iuhKqqTb6ezwK0IVwGc5Cxz6lHH4TP7Mwg+wJubv+M+etqOb8SVtXODEaOC278fl9aV7vT+wV8/xkqq02cCKw24Zx6oOUH18kGtdUmeA9VxNKpRzsjzbz/m7ePwIp3SBGazv9CjMfWqUc6t/j0Ww6KK98hJYxwidtPy9Qjnz0dJ81f3qR+H5+WuIGDzvnUZIPF/FDYuaf+9vHpxi9cwt1wukbrOT5cbdJcqNcnobJqk6Cp2PIFCJOCWTDT+TgAi6xar7U1VpvAJW73gc8wPywxYkaJW33qYaKVZoyswwfvUXnQMdNKs0ahKaOYf11kOqZaaXotQh6jrvaULUCaa6U5pDOBS73ns3hgNZ4wHlh38KCzolMPo/2wYIQOVFIdJ4b7Yal9aHoCF64+1o+f5dbFSrPPIXsMH33iIkTOUXfCZNBp+l2CJkJs0VUEbf2wxIjAE8CthNiKl4J09sNcCM3g9Y4mQuwk/y+6W2nOOIUXWZmEGaABVprdAnlrYPODRYhxHE63hlhpxoiiu/UOBiGyYsBwZYyVZox498VDmJpoqH88BBjRYt5KmISJkVv/BQ/BdW29EFp00PFKgw5EmIYJF9V/wUNk9SU+/dcXYZwFjH/YhMhJfJCeF5ZXfYnz1e1eavWL78m4J0x90I2/t0pmFTRt1LEvwmTQWUGEuQ/Sf0g+jZCs/H71VQ+O6XrH93eNMPXBMHmK9NMI9FjTrsetbDL1iGqvZGECswiF1+r3VEKbE1Vvl/tg2jLpZ2ZECyeZjFtsUco/MyNUKWBYLKyae6IEVO6DxUuvRZiHidJQ9lJ+WA0TqV7JD7FT90GqF7LSIlWr6HWs9C5MpHoZK0WZD9bzpVexUnQfJrJ3XoOwMpuo6jX8MPdBqGbjFfwwDxPQm69gpbXpUk0vQNjgg1Tm+yFmhYlUxvthMZvgJzTKShmpWkmGWykrVSvJbCtt80Eqo60UmNHfy2TCljCRymA/zDdfmi8z1w+LVK15fdlYK60v/DJlKiGfD1L1/QseAqHK4gkT6ZV9/4KHJGVhgkdVwnFHK63+8vj2XZbicvftiuva+2qTLlZaJdRWBWFw6Gil+WlIjJtq61RrkxPuDx2tNPt2cfLZ5eZNV22W+YAUuR0JZ8X5AKyzsjZ6h6gj4S4oxmvVFA3KY8fstutK6NuyQmAPwo572HcZSy072Pu+rbrdHbSfR/vAtnjHUmx5hDD07ebvpdBGGAeXg78PvC6E05k/GUeG9CKeHk6RP5vyEyaOGIWHSWBAL2IrOMzDiDXQwIQkqSFmOnEv473N+x0OioSxtz/d3AkxUjilYRBSMyWd6F5u7p58UFshy5uNlxeXdCHLSGFCaqa0E0N3fJpfXH8WTLXUYhdelrexGyZdyGABXyWdSEPiJFyvx5fTdX6ba6rbZbymgMQLGV3IICSeSO2U9uKaaqyn4rbFgMRGYS9kExI7JUbgR4TR1ZjQJXwR6cEpsdFuhCkiyWwIY0gotRRpGeHz902ATEKK6E2D2Y70I9FER9GG+bsdGQc9NmADIUUk3Tjb7wilniIt289IB1LA7oQ5YxAsFjM9tVgEQQtfIyFFJIyebduqAx9DpGUe4WsEbCRMGC2HytNPcbusFr42whhS48Q0ztxa1EqYYqL4XvHfyh+Eiz/0peK68jvMP7zXlS6Pn5D+i0d8hCZrIDRfA6H5GgjN10BovgZC8zUQmq//ATQYdOSzBIXQAAAAAElFTkSuQmCC",
//                    "prefill": [
//                        "contact": "9797979797",
//                        "email": "foo@bar.com"
//                    ],
                    "theme": [
                        "color": "#001A27"
                    ]
                ]
        let currentViewController:  UIViewController? = UIApplication.shared.keyWindow?.rootViewController
        razorpay.open(options, displayController: currentViewController ?? self)
        
    }
    override func viewDidAppear(_ animated: Bool) {
          //showPaymentForm()
      }
    func onPaymentError(_ code: Int32, description str: String) {
           print("error\(str)")
        self.view.makeToast("Payment Failed", duration: 2.0, position: .center)
        cedMage.delay(delay: 2.0) {
            self.afterPayment(payment_id: "", failure: "true")
        }
       }
       
       func onPaymentSuccess(_ payment_id: String) {
           print("success")
         self.view.makeToast("Payment Successful", duration: 2.0, position: .center)
               cedMage.delay(delay: 2.0) {
                   self.afterPayment(payment_id: payment_id, failure: "false")
               }
       }
  
}*/
extension UIApplication{
     func getTopViewController() -> UIViewController? {
         var topController: UIViewController? = UIApplication.shared.keyWindow?.rootViewController
         while topController?.presentedViewController != nil {
             topController = topController?.presentedViewController
         }
         return topController
     }
}
