//
//  SignupController.swift
//  MageNative Magento Platinum
//
//  Created by Hamza Usmani on 08/02/21.
//  Copyright © 2021 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

final class SignupController: MagenativeUIViewController {
    
    // MARK:- Properties
    
    private var isNewsletterSubscribed: Bool = false
    private var name: String = ""
    
    // MARK:- Views
    
    lazy private var scroll: UIScrollView = {
        let view = UIScrollView(frame: .zero)
        view.frame = self.view.bounds
        view.contentSize = CGSize(width: self.view.frame.width, height: 800)
        view.alwaysBounceVertical = false
        return view
    }()
    
    lazy private var container:UIView = {
        let view = UIView()
        view.backgroundColor = .mageSystemBackground
        view.frame.size = CGSize(width: self.view.frame.width, height: 800)
        return view
    }()
    
    lazy private var headingLabel: UILabel = {
        let label: UILabel = .init()
        label.font = .avenirBold(withSize: 16)
        label.text = "Create an account"
        return label
    }()
    
    lazy private var maleButton: UIButton = {
        let button: UIButton = .init(type: .system)
        button.setTitle("M", for: .normal)
        button.titleLabel?.font = .avenirBold(withSize: 17)
        button.backgroundColor = .mageSecondarySystemBackground
        button.anchor(height: 40)
        button.layer.cornerRadius = 20.0
        return button
    }()
    
    lazy private var femaleButton: UIButton = {
        let button: UIButton = .init(type: .system)
        button.setTitle("F", for: .normal)
        button.titleLabel?.font = .avenirBold(withSize: 17)
        button.backgroundColor = .mageSecondarySystemBackground
        button.anchor(height: 40)
        button.layer.cornerRadius = 20.0
        return button
    }()
    
    lazy private var firstnameField: SkyFloatingLabelTextField = {
        let field = createTextField(withPlaceholder: "First Name")
        return field
    }()
    
    lazy private var lastnameField: SkyFloatingLabelTextField = {
        let field = createTextField(withPlaceholder: "Last Name")
        return field
    }()
    
    lazy var emailField: SkyFloatingLabelTextField = {
        let field = createTextField(withPlaceholder: "Your Email")
        return field
    }()
    
    lazy var mobileField: SkyFloatingLabelTextField = {
        let field = createTextField(withPlaceholder: "Your Phone number")
        return field
    }()
    
    lazy private var passwordField: SkyFloatingLabelTextField = {
        let field = createTextField(withPlaceholder: "Password")
        return field
    }()
    
    lazy private var confirmPasswordField: SkyFloatingLabelTextField = {
        let field = createTextField(withPlaceholder: "Confirm Password")
        return field
    }()
    
    lazy private var newsletterButton: UIButton = {
        let button: UIButton = .init(type: .system)
        button.setTitle("Signup for newsletter", for: .normal)
        button.contentHorizontalAlignment = .leading
        button.titleLabel?.font = .avenirSemibold(withSize: 15)
        button.setImage(UIImage(named: "icon-newsletter-unchecked"), for: .normal)
        button.titleEdgeInsets = .init(top: 0, left: 8, bottom: 0, right: 0)
        button.addTarget(self, action: #selector(newsletterTapped), for: .touchUpInside)
        return button
    }()
    
    private var signupButton: UIButton = {
        let button: UIButton = .init(type: .system)
        button.backgroundColor = Settings.themeColor
        button.setTitle("Submit", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font =  .avenirSemibold(withSize: 16)
        button.layer.cornerRadius = 8.0
        button.anchor(height: 40)
        button.addTarget(self, action: #selector(submitTapped), for: .touchUpInside)
        return button
    }()
    
    
    // MARK:- Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
    }
    
    
    // MARK:- Helper Methods
    
    private func configureUI() {
        view.backgroundColor = .mageSystemBackground
        
        // Adding subviews to the controller's view
        view.addSubview(scroll)
        scroll.addSubview(container)
        
        container.addSubview(headingLabel)
        headingLabel.anchor(top: container.safeAreaLayoutGuide.topAnchor, paddingTop: 16)
        headingLabel.centerX(inView: container)
        
        // Horizontal stack for gender buttons
        let genderHstack: UIStackView = .init(arrangedSubviews: [maleButton, femaleButton])
        genderHstack.axis = .horizontal
        genderHstack.spacing = 8.0
        genderHstack.distribution = .fillEqually
        
        // Horizontal stack for firstname and lastname fields
        let hstack: UIStackView = .init(arrangedSubviews: [firstnameField, lastnameField])
        hstack.axis = .horizontal
        hstack.spacing = 8.0
        hstack.distribution = .fillEqually
        
        // Vertical stack for all the remaining fields
        let vstack: UIStackView = .init(arrangedSubviews: [hstack, emailField, mobileField, passwordField, confirmPasswordField, newsletterButton, signupButton])
        vstack.axis = .vertical
        vstack.spacing = 16.0
        vstack.setCustomSpacing(32, after: confirmPasswordField)
        vstack.setCustomSpacing(32, after: newsletterButton)
        
        container.addSubview(vstack)
        vstack.anchor(top: headingLabel.bottomAnchor, left: container.leadingAnchor, right: container.trailingAnchor, paddingTop: 32, paddingLeft: 16, paddingRight: 16)
        
        
    }
    
    private func createTextField(withPlaceholder placeholder: String) -> SkyFloatingLabelTextField {
        let field = SkyFloatingLabelTextField()
        field.placeholder = placeholder
        field.selectedLineColor = Settings.themeColor
        field.lineColor = Settings.themeColor
        field.selectedTitleColor = Settings.themeColor
        field.titleColor = Settings.themeColor
        field.font = .avenirMedium(withSize: 14)
        return field
    }
    
    // MARK:- API Calls
    
    private func verify(number: String, completion:@escaping (Bool) -> Void) {
        ApiHandler.handle.request(with: "mobiconnect/customer/validateNumber", params: ["mobile": number, "type": "register"], requestType: .POST, controller: self) { [weak self] data, error in
            guard let data = data else { return }
            guard let json = try? JSON(data: data) else { return }
            print(json)
            DispatchQueue.main.async {
                self?.view.makeToast(json[0]["data"]["customer"][0]["message"].stringValue)
            }
            completion(json[0]["data"]["customer"][0]["status"].boolValue)
        }
    }
    
    private func sendOTP(toNumber number: String) {
        let countryID: String = Locale.current.regionCode ?? ""
        ApiHandler.handle.request(with: "mobiconnect/customer/sendOtp", params: ["mobile": number, "country_id": countryID], requestType: .POST, controller: self) { [weak self] data, _ in
            guard let data = data else { return }
            guard let json = try? JSON(data: data) else { return }
            print(json)
            
            self?.view.makeToast(json[0]["data"]["customer"][0]["message"].stringValue)
            // if OTP is successfully sent show the OTP popup to verify OTP
            if json[0]["data"]["customer"][0]["status"].boolValue {
            
                let popup        = AuthenticationView(otp: "1234")
                guard let window = UIApplication.shared.keyWindow else { return }
                window.addSubview(popup)
                popup.frame = window.bounds
                
                // Handle the success state of OTP validation
                popup.onSuccess = { otp in
                   self?.verifyOTP(on: number, otp: otp)
                }
                
                // Handle the failure state of OTP validation
                popup.onFailure = { message in
                    self?.view.makeToast(message)
                    return
                }
                
                // Handle the resend OTP Action
                popup.onResend = {
                    self?.sendOTP(toNumber: number)
                }
            }
        }
    }
    
    private func verifyOTP(on number: String, otp: String) {
        let param = ["mobile": number, "country_id": Locale.current.regionCode ?? "", "otp": otp]
        ApiHandler.handle.request(with: "mobiconnect/customer/verifyOtp", params: param, requestType: .POST, controller: self) { [weak self] data, _ in
            guard let data = data else { return }
            guard let json = try? JSON(data: data) else { return }
            print(json)
            
            self?.view.makeToast(json[0]["data"]["customer"][0]["message"].stringValue)
            // if OTP is successfully verified
            if json[0]["data"]["customer"][0]["status"].boolValue  {
                let fname = self?.firstnameField.text ?? ""
                let lname = self?.lastnameField.text ?? ""
                let email = self?.emailField.text ?? ""
                let mobile = self?.mobileField.text ?? ""
                let password  = self?.passwordField.text ?? ""
                
                // Create param and send register API Request
                let newsletter = self?.isNewsletterSubscribed ?? false ? "1" : "0"
                let storeID = UserDefaults.standard.value(forKey: "storeId")  as? String ?? "1"
                let extensionString = ["mobile": mobile]
                
                let param = ["firstname":fname,"lastname":lname,"email":email,"password":password,"gender":"1","is_subscribed":newsletter,"store_id":storeID,
                             "extension_attributes": extensionString] as [String : Any]
                self?.registerUser(with: param)
            }
        }
    }
    
    private func registerUser(with param: [String:Any]) {
        ApiHandler.handle.requestDict(with: "mobiconnect/customer/register", params: param, requestType: .POST, controller: self) { [weak self] (data, error) in
            guard let data = data else { return }
            guard let json = try? JSON(data: data) else { return }
            print(json)
            
            if json[0]["data"]["customer"][0]["status"].stringValue != "success" {
                DispatchQueue.main.async {
                    self?.view.makeToast(json[0]["data"]["customer"][0]["message"].stringValue)
                }
                return
            }
            
            if json[0]["data"]["customer"][0]["isConfirmationRequired"].stringValue != "NO" {
                cedMageHttpException.showAlertView(me: self ?? UIViewController(), msg: "Registration Successful".localized, title: "Success".localized)
                cedMage.delay(delay: 2, closure: {
                    self?.navigationController?.popToRootViewController(animated: true)
                    self?.tabBarController?.selectedIndex = 0
                })
                return
            }
            
            
            self?.name = param["firstname"] as? String ?? ""
            self?.loginUser(withEmail: param["email"] as? String ?? "", andPassword: param["password"] as? String ?? "")
        }
    }
    
    
    
    private func loginUser(withEmail email: String, andPassword password: String) {
        let email = email.trimmingCharacters(in: .whitespacesAndNewlines)
        let password = password.trimmingCharacters(in: .whitespacesAndNewlines)
        let extensionString = ["login_type":"email"]
        let param = ["store_id": UserDefaults.standard.value(forKey: "storeId") as? String ?? "" ,"email":email,"password":password, "extension_attributes": extensionString] as [String : Any]
        
        ApiHandler.handle.requestDict(with: "mobiconnect/customer/login", params: param, requestType: .POST, controller: self) { [weak self] (data, error) in
            guard let data = data else { return }
            guard let json = try? JSON(data: data) else { return }
            print(json)
            
            if json[0]["data"]["customer"][0]["status"].stringValue.localized ==  "exception".localized {
                let message = json[0]["data"]["customer"][0]["message"].stringValue
                self?.view.makeToast(message)
                return
            }
            
            UserDefaults.standard.setValue(self?.name, forKey: "name")
            UserDefaults.standard.set(true,forKey: "isLogin")
            DispatchQueue.main.async {
                let vc=cedMageMainDrawer()
                let table = vc.mainTable
                table?.reloadData();
            }
            
            let customer_id = json[0]["data"]["customer"][0]["customer_id"].stringValue;
            let hashKey = json[0]["data"]["customer"][0]["hash"].stringValue;
            let cart_summary = json[0]["data"]["customer"][0]["cart_summary"].intValue;
            let dict = ["email": email, "customerId": customer_id, "hashKey": hashKey]
            UserDefaults.standard.set(dict, forKey: "userInfoDict");
            UserDefaults.standard.set(cart_summary, forKey: "cart_summary");
            UserDefaults.standard.setValue( json[0]["data"]["customer"][0]["gender"].stringValue, forKey: "gender")
            DispatchQueue.main.async {
                self?.view.makeToast("Login Success")
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                self?.navigationController?.popToRootViewController(animated: true)
                self?.tabBarController?.selectedIndex = 0
            }
        }
    }
    
    
    // MARK:- Selectors
    
    @objc private func newsletterTapped() {
        isNewsletterSubscribed.toggle()
        let image: UIImage? = isNewsletterSubscribed ? UIImage(named: "icon-newsletter-checked") : UIImage(named: "icon-newsletter-unchecked")
        newsletterButton.setImage(image, for: .normal)
    }
    
    @objc private func submitTapped() {
        let fname = firstnameField.text ?? ""
        let lname = lastnameField.text ?? ""
        let email = emailField.text ?? ""
        let mobile = mobileField.text ?? ""
        let password  = passwordField.text ?? ""
        let cpassword = confirmPasswordField.text ?? ""
        
        // check for empty fields
        if fname.isEmpty || lname.isEmpty || email.isEmpty || password.isEmpty || cpassword.isEmpty || mobile.isEmpty {
            view.makeToast("Fields cannot be left Empty !")
            return
        }
        
        // check for valid email id
        if !isValidEmail(email: email) {
            view.makeToast("Invalid Email Provided")
            return
        }
        
        // check for password and confirm password matched or not
        if password != cpassword {
            view.makeToast("Password and confirm password does not match!")
            return
        }
        
        verify(number: mobile) { [weak self] success in
            if success {
                self?.sendOTP(toNumber: mobile)
            }
        }
    }
   
}
