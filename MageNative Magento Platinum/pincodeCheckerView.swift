//
//  pincodeCheckerView.swift
//  MageNative Magento Platinum
//
//  Created by cedcoss on 09/10/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import Foundation

class pincodeCheckerView:UIView{
    var view:UIView!
    @IBOutlet weak var outerView: UIView!
    
    @IBOutlet weak var pincodeResultLbl: UILabel!
    @IBOutlet weak var checkBtn: UIButton!
    @IBOutlet weak var pincodeLbl: UILabel!
    
    @IBOutlet weak var pinTextField: UITextField!
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        xibSetup()
    }
     func xibSetup()
        {
            
            
            view = loadViewFromNib()
            
            // use bounds not frame or it'll be offset
            view.frame = bounds
            
            // Make the view stretch with containing view
            view.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
            
            //extra setup
           
          
            
            // Adding custom subview on top of our view (over any custom drawing > see note below)
            addSubview(view)
        }
        
        func loadViewFromNib() -> UIView
        {
            let bundle = Bundle(for: type(of: self))
            let nib = UINib(nibName: "pincodeCheckerView", bundle: bundle)
            
            // Assumes UIView is top level and only object in CustomView.xib file
            let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
            return view
        }
     
    
    
}
