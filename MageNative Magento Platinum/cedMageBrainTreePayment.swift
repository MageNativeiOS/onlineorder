//
//  cedMageCustomPayment.swift
//  MageNative Magento Platinum
//
//  Created by CEDCOSS Technologies Private Limited on 31/03/17.
//  Copyright © 2017 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

extension  cedMagePayementView {
    
    func getClientToken(){
        var customerd = "0"
        if defaults.bool(forKey: "isLogin") {
            if  let customerId = userInfoDict["customerId"]{
               customerd = customerId
            }
        }
       
        self.sendRequest(url: clientTokenUrl, params: ["customer_id":customerd],store:false)
    }
    
    func fetchExistingPaymentMethod(clientToken: String) {
        let actionsheet = UIAlertController(title: "Select Payment To Proceed".localized, message: nil, preferredStyle: .actionSheet)
        print("%^%^%^*(**^&%&%*)_*^&^")
        for buttons in self.previousMethods {
            let action  = UIAlertAction(title: buttons["maskedNumber"], style: UIAlertAction.Style.default,handler: {
                action -> Void in
                
                print(action.title as Any)
                
            })
            cedMageImageLoader.shared.loadImgFromUrl(urlString: "", completionHandler: {
                image,string in
                 action.setValue(image, forKey: "image")
            })
            actionsheet.addAction(action)
            
            
        }
        actionsheet.addAction(UIAlertAction(title: "Add New".localized, style: UIAlertAction.Style.cancel, handler: {
            action -> Void in
           // self.showDropIn("")
        }))
        actionsheet.addAction(UIAlertAction(title: "Cancel".localized, style: UIAlertAction.Style.cancel, handler: {
            action -> Void in
        }))
        if(UIDevice().model.lowercased() == "iPad".lowercased()){
            actionsheet.popoverPresentationController?.sourceView = self.view
        }
        actionsheet.modalPresentationStyle = .fullScreen;
        self.present(actionsheet, animated: true, completion: nil)
    }
    
    func showpreviousPaymethods(){
        
    }
  
    
    func showDropIn(clientTokenOrTokenizationKey: String) {
     /*   let request =  BTDropInRequest()
        let dropIn = BTDropInController(authorization: clientTokenOrTokenizationKey, request: request)
        { (controller, result, error) in
            if (error != nil) {
                print("ERROR")
                self.afterPayment(payment_id: "", failure: "true")
            } else if (result?.isCancelled == true) {
                print("CANCELLED")
                self.afterPayment(payment_id: "", failure: "true")
            } else if let result = result {
                // Use the BTDropInResult properties to update your UI
                let out = result.paymentMethod!
                print("nonce -----\(out.nonce)")
                
                self.postNonceToServer(paymentMethodNonce: out.nonce)
            }
            controller.dismiss(animated: true, completion: nil)
        }
        dropIn?.modalPresentationStyle = .fullScreen;
        self.present(dropIn!, animated: true, completion: nil) */
    }
    
    
    func postNonceToServer(paymentMethodNonce: String,user:Bool=false,token:String="") {
        if let currencySymbol = total["currency_symbol"]{
            if let ammount = total["grandtotal"]?.replacingOccurrences(of: currencySymbol, with: ""){
                print(ammount)
                if let order_id = orderStatusData["orderId"] {
                      var params = ["pay_amt":ammount,"order_id":order_id]
                    if user {
                        params["token"] = token
                    }else{
                        params["payment_method_nonce"] = paymentMethodNonce
                    }
                  
                    if defaults.bool(forKey:"isLogin"){
                        if let custId =  userInfoDict["customerId"] {
                            params["customer_id"] = custId
                        }
                    }
                    self.sendRequest(url: trasactionUrl, params: params,store:false)
                }
            }
        }
        
        
    }
    
    func afterPayment(payment_id:String,failure:String,order_id:String = ""){
          if let order_id = orderStatusData["orderId"] {
        self.sendRequest(url:finalOrderCheck, params: ["order_id":order_id,"additional_info":payment_id,"failure":failure])
        }
    }
 
    

    
   
    
    
}
