/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */


import UIKit

class CartProductListView: UIView {
    
    // Our custom view from the XIB file
    var view: UIView!
    
    //outlets
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productDescription: UILabel!
    @IBOutlet weak var editProductOptionsButton: UIButton!
    
    @IBOutlet weak var quantityEditView: UIView!
    @IBOutlet weak var decrement: UIButton!
    @IBOutlet weak var increment: UIButton!
    @IBOutlet weak var qty: UITextField!
    
    @IBOutlet weak var updateCart: UIButton!
    @IBOutlet weak var delete: UIButton!
    override init(frame: CGRect)
    {
        // 1. setup any properties here
        
        // 2. call super.init(frame:)
        super.init(frame: frame)
        
        // 3. Setup view from .xib file
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        // 1. setup any properties here
        
        // 2. call super.init(coder:)
        super.init(coder: aDecoder)
        
        // 3. Setup view from .xib file
        xibSetup()
    }
    
    func xibSetup()
    {
        
        
       
        view = loadViewFromNib()
        
        // use bounds not frame or it'll be offset
        view.frame = bounds
        productImage.fontColorTool()
        productDescription.fontColorTool()
//        editProductOptionsButton.fontColorTool()
        quantityEditView.fontColorTool()
        decrement.fontColorTool()
        increment.fontColorTool()
        qty.fontColorTool()
        updateCart.fontColorTool()
        delete.fontColorTool()
        // Make the view stretch with containing view
        view.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        
        //extra setup
        
        //   editProductOptionsButton.isHidden = true;
        //extra setup
        updateCart.setTitle("UPDATE".localized, for: .normal)
        delete.setTitle("DELETE".localized, for: .normal)
        updateCart.setTitleColor(UIColor.black, for: .normal)
        delete.setTitleColor(UIColor.black, for: .normal)
        increment.addTarget(self, action: #selector(CartProductListView.incrementProductQty(_:)), for: UIControl.Event.touchUpInside);
        decrement.addTarget(self, action: #selector(CartProductListView.decrementProductQty(_:)), for: UIControl.Event.touchUpInside);
        
        
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView
    {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "CartProductListView", bundle: bundle)
        
        // Assumes UIView is top level and only object in CustomView.xib file
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    
    @objc func incrementProductQty(_ sender:UIButton){
        
        if(qty.text == ""){
            qty.text = String("1");
            return;
        }
        if(qty.text != ""){
            var currentQty = Int(qty.text!)!;
            currentQty = currentQty+1;
            qty.text = String(currentQty);
        }
        
    }
    
    @objc func decrementProductQty(_ sender:UIButton){
        
        if(qty.text != "" && qty.text != "1"){
            var currentQty = Int(qty.text!)!;
            currentQty = currentQty-1;
            qty.text = String(currentQty);
        }
        
    }
    
}
