/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */


import UIKit

class cedMageCategoryTheme2: cedMageViewController,UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate,KIImagePagerDelegate,KIImagePagerDataSource {
   


    @IBOutlet weak var tableView: UITableView!    
    @IBOutlet weak var searchBar: UISearchBar!
    var serachedSubCategoryData = [[String:String]]()
    var searchedData =  [[String:String]]()
    var categoryMainData    = [Int:[[String:String]]]()
    var categoryId = String()
    var bannerArray = [[String:String]]()
    var product_array = [[String:String]]()
    var category_array = [[String:String]]()
    var banner_image_array = [String]()
    var selectedCategory = String()
    let bounds = UIScreen.main.bounds
    override func viewDidLoad() {
        super.viewDidLoad()
        clearFilterRelatedData();
        tableView.delegate = self
        tableView.dataSource = self
        searchBar.delegate = self
        
        searchBar.placeholder = "Enter Your String".localized
        searchBar.scopeButtonTitles?[0] = "Title".localized
        searchBar.scopeButtonTitles?[1] = "Title".localized
        let colorString = Settings.themeColor
//        self.searchBar.barTintColor = cedMage.UIColorFromRGB(colorCode: colorString)
        self.searchBar.barTintColor = .white
        self.sendRequest(url: "mobiconnectadvcart/category/getallcategories/theme/1/", params: nil)
        self.sendScreenView(name: "Category".localized)

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Mark: SearchBar Functions
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
     searchedData =  category_array.filter({ result in
          return  (result["category_name"]?.lowercased().contains(searchText.lowercased()))!
        
        })
        print("?"+searchText)
        print("?\(searchedData)")
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if(searchedData.count != 0){
            return "Matched Categories".localized
        }
        return ""
    }
    
    //Mark: TableViewFunctions
    
    override func recieveResponse(data: Data?, requestUrl: String?, response: URLResponse?) {
        guard let json = try? JSON(data: data!) else {
            return;
        }
        if(requestUrl == "mobiconnectadvcart/category/getallcategories/theme/1/"){
             self.parse_product(json: json, index: "categories")
        }else{
            self.parse_product(json: json, index: "categories")
            print(json)
        }
        self.banner_image_array = [String]()
        for image in self.bannerArray{
            self.banner_image_array.append(image["banner_image"]!)
        }
       print(banner_image_array)
        tableView.reloadData()
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        if(searchedData.count != 0 ){
            return 1
        }
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(searchedData.count != 0 ){
            return searchedData.count
        }else{
        if(section == 1){
            return category_array.count
        }
        if(banner_image_array.count != 0){
                
             return 1
            }
        return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(searchedData.count != 0){
            let cell = tableView.dequeueReusableCell(withIdentifier: "collectionViewCell") as! ced_categoryView
            cell.topLabel.text = searchedData[indexPath.row]["category_name"]
             cell.topLabel.fontColorTool()
            let cateId = category_array[indexPath.row]["categoryId"]
            cell.viewControl = self
            if(categoryMainData[Int(cateId!)!]!.count != 0){
                cell.collectionView.isHidden = false
                cell.categoryDataSource = categoryMainData[Int(cateId!)!]!
            }else{
                cell.collectionView.isHidden = true
                cell.categoryDataSource  = [[String:String]]()
            }
            
        cedMageImageLoader.shared.loadImgFromUrl(urlString: searchedData[indexPath.row]["category_image"]!,completionHandler: { (image: UIImage?, url: String) in
                DispatchQueue.main.async {
                     cell.categoryView.image = image
                }
            })
            
            
            return cell
        }
        
        if(indexPath.section == 0){
            let cell = tableView.dequeueReusableCell(withIdentifier: "bannerCell") as! homePageBannerCell
            
            cell.banner_image_array = banner_image_array
            cell.bannerPage.delegate = self
            cell.bannerPage.dataSource = self
            cell.bannerPage.slideshowTimeInterval = UInt(2.5)
            cell.bannerPage.imageCounterDisabled = true
            cell.bannerPage.indicatorDisabled = true
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "collectionViewCell") as! ced_categoryView
        cell.topLabel.text = category_array[indexPath.row]["category_name"]
        cell.topLabel.fontColorTool()
        cedMageImageLoader.shared.loadImgFromUrl(urlString: category_array[indexPath.row]["category_image"]!,completionHandler: { (image: UIImage?, url: String) in
            DispatchQueue.main.async {
                cell.categoryView.image = image
            }
        })
       let cateId = category_array[indexPath.row]["categoryId"]
        cell.viewControl = self
        if(categoryMainData[Int(cateId!)!]!.count != 0){
            cell.collectionView.isHidden = false
          cell.categoryDataSource = categoryMainData[Int(cateId!)!]!
        }else{
            cell.collectionView.isHidden = true
            cell.categoryDataSource  = [[String:String]]()
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(searchedData.count != 0){
            let cateId = category_array[indexPath.row]["categoryId"]
            print(cateId as Any)
            if(categoryMainData[Int(cateId!)!]!.count != 0){
                return CGFloat(150+180*categoryMainData[Int(cateId!)!]!.count/3)
            }else{
                return 50
            }
        }
        if(indexPath.section == 0){
            let height = UIWindow().frame.size.height/2.5
            return height
        }
        let cateId = category_array[indexPath.row]["categoryId"]
        print(cateId as Any)
        if(categoryMainData[Int(cateId!)!]!.count != 0){
        return CGFloat(150+180*categoryMainData[Int(cateId!)!]!.count/3)
        }else{
           
            return 70
        }
    }
    
    
    func parse_product(json:JSON,index:String){
        if(json["data"]["banner"].arrayValue.count > 0){
            for result in json["data"]["banner"].arrayValue {
                let product_id = result["product_id"].stringValue
                let id = result["id"].stringValue
                let title = result["title"].stringValue
                let link_to = result["link_to"].stringValue
                let banner_image = result["banner_image"].stringValue
                let products_obj = ["product_id":product_id, "title":title,"link_to":link_to,"banner_image":banner_image,"id":id]
                bannerArray.append(products_obj)
            }
        }
      //  print(json["data"][index].arrayValue)
          var i = 0
        for (_,subJson) in json["data"]["categories"][0] {
         
            let maincatename = subJson["main_category_name"].stringValue
              let maincateimage = subJson["main_category_image"].stringValue
             let maincateId = subJson["main_category_id"].stringValue
            category_array.append(["category_name":maincatename,"category_image":maincateimage,"categoryId":maincateId])
          
           //print(subJson["sub_cats"][0])
            var temp = [[String:String]]()
            for (_,subcategory)  in subJson["sub_cats"]{
                
                var tem2 = [String:String]()
                tem2["sub_category_id"] = subcategory["sub_category_id"].stringValue
                tem2["sub_category_name"] = subcategory["sub_category_name"].stringValue
                tem2["sub_category_image"] = subcategory["sub_category_image"].stringValue
               temp.append(tem2)
               
               
            }
            
            categoryMainData[Int(maincateId)!] = temp
            temp.removeAll()
            i += 1
            //Do something you want
        }
       
    
        if(json["data"]["product"].arrayValue.count > 0){
            
            for result in json["data"]["product"].arrayValue {
                let product_id = result["product_id"].stringValue
                let product_name = result["product_name"].stringValue
                let product_price = result["product_price"].stringValue
                let product_image = result["product_image"].stringValue
                let products_obj = ["product_id":product_id, "product_name":product_name,"product_image":product_image,"product_price":product_price]
                product_array.append(products_obj)
                
            }
        }
        return
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cateId = category_array[indexPath.row]["categoryId"]
        print(cateId as Any)
        if(categoryMainData[Int(cateId!)!]!.count == 0){
            let contol = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "productListingViewController") as! ProductListingViewController
            contol.categoryId = cateId
            self.navigationController?.pushViewController(contol, animated: true)
        }
    }
    
    //MARK: Delegate for KIImage Pager
    
    public func array(withImages pager: KIImagePager!) -> [Any]! {
        if banner_image_array.count == 0 {
            return ["placeholder" as AnyObject]
        }
        return banner_image_array as [AnyObject]?
    }
  /*  func array(withImages pager: KIImagePager!) -> [AnyObject]! {
        if banner_image_array.count == 0 {
            return ["placeholder" as AnyObject]
        }
        return banner_image_array as [AnyObject]!
    }*/
    
    func contentMode(forImage image: UInt, in pager: KIImagePager!) -> UIView.ContentMode {
        return UIView.ContentMode.scaleToFill
    }
    
    func placeHolderImage(for pager: KIImagePager!) -> UIImage! {
        return UIImage(named: "placeholder")
    }
    
    func imagePager(_ imagePager: KIImagePager!, didSelectImageAt index: UInt) {
        
    }

    

}
