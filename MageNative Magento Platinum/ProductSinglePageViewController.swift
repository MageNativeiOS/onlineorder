/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */


import UIKit
import FSCalendar
class ProductSinglePageViewController: MagenativeUIViewController,UITextViewDelegate,UIScrollViewDelegate,UIGestureRecognizerDelegate,WKNavigationDelegate
{
    
    /* */
    let PincodeCheckerView = pincodeCheckerView();
    var dateUsed=Date()
    var productURLarray = [String]()
    var arrayToKeepTrackForAdditionalImage = [String:String]();
    let calendarView=CalendarView()
    var giftData = [String:[String:String]]()
    var addToCartButton : UIButton!;
    var addTocartLabel:UILabel!
    var buyNowButton:UIButton!
    var parentView: UIViewController?
    /* Magento 2 floating action button */
    var floatingActionButton : UIButton!;
    var stackViewForChildFloatingButton : UIStackView!;
    let additionalInfo = AdditionalInfoView()
    let imageViewAsOverlay = UIImageView();
    
    var isChildOptionsVisible = false;
    var childOptionsArray = [0:["Share This Product".localized:"Share"],1:["Detail & Description".localized:"DetailDescription"],2:["Write The Review".localized:"WriteReview"]];
    /* Magento 2 floating action button */
    // downloadable product
    var links = [String:[String:String]]();
    var samples = [String:[String:String]]();
    var downloadRelatedData = [String:String]();
    var downloadableProductOptionsStackView:UIStackView!;
    var difference = CGFloat(0.0);
    var reviewPopupView : ReviewPopupView!;
    var similarProductView : SimilarProductView!;
    var additionalInfoHiddenCheck = true;
    var productCardSection = newProductCardSection()
    var dataObject = NSDictionary();
    var product_id = "";// to be use in case of edit options in cart :: magento2
    var previousQty = "";// to be use in case of edit options in cart :: magento2
    var item_id = "";// to be use in case of edit options in cart :: magento2
    var previous_options_selected = [String:String]();
    var productImageView : productPageImagesView!;
    var productNamePriceView : ProductNamePriceView!;
    var productShareView:cedMageShareAndWish!;
    var productQtyView : ProductQtyView! // Product Qty View
    var customOptionsStackView : UIStackView!; // stackview as container for custom-options view
    var productInfoArray = [String:String]();
    var productImgsArray = [[String:String]]();
    var additionalProductImgsInCaseOfConfig = "";
    var priceCalculationArray = [String:Float]();
    var similarProductArray = [[String:String]]();
    var upsellProductArray = [[String:String]]();
    var crosssellProductArray = [[String:String]]();
    var productReviews = [[String:String]]();
    var moreInfoData = [[String:String]]();
    
    var custom_option = [String:[String:String]]();
    var custom_option_options = [String:[[String:String]]]();
    var dropDownTrackingArray = [Int:Int]();//keeps track of dropdown id along with its selected option_id
    var custom_price_array = [Int:String]();//keeping track of all option_ids with their price
    var datePickerTagsArray = [Int]();
    var globalTextFieldTag = Int();
    var custom_options_string = "";
    var more_infoArray  = [String:[JSON]]();
    
    var configOptionsStackView : UIStackView!;
    var attributes = [String:[String:String]]();
    var configIndexJson:JSON!
    var config_attribute_array = [String:[[String:String]]]();
    var config_availability_array = [String:[String:[String:[String:String]]]]();
    var option_price = [String:[String:[String:String]]]();
    var firstConfigAttributeSelected = false;
    
    var grouped_product_array = [[String:String]]();
    var groupedProductsStackView : UIStackView!;
    var bundledpriceRange = [String:String]();
    var bundleddata_array = [String:[String:[String:String]]]();
    var bundleddata_extraInfo_array = [String:[String:String]]();
    var topLabelArray = [String]();
    var bundlePriceArr = [String:[String:Float]]();
    var bundleProductOptionsStackView : UIStackView!;
    var attributesCode = [String:String]()
    
    var isFromList = false
    var selectedId = String()
    var selectedConfigString = [String:String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if self.parentView == nil {
            parentView = self
        }
        
        //.setRightBarButtonItems([cartButton,barButton], animated: true)
        // Do any additional setp after loading the view.
        self.basicFoundationToRenderView(bottomMargin: translateAccordingToDevice(CGFloat(40.0))+85);
        
        if(dataObject.count > 0){
            let productID = dataObject.object(forKey: "product_id") as! String;
            self.makeRequestToAPI("mobiconnect/catalog/view/",dataToPost: ["prodID":productID]);
        }
        else if(product_id != ""){
            self.makeRequestToAPI("mobiconnect/catalog/view/",dataToPost: ["prodID":product_id]);
        }
        var param = [String:String]()
        if let filterType = defaults.value(forKey: "filterType") as? String {
            if filterType == "zipcode" {
                param["postcode"] = Settings.currentLocation?["zipcode"] ?? ""
            } else if filterType == "city_state_country" {
                param["city"] = Settings.currentLocation?["city"] ?? ""
                param["state"] = Settings.currentLocation?["state"] ?? ""
                param["country_id"] = Settings.currentLocation?["country"] ?? ""
            } else {
                param["latitude"] = Settings.currentLocation?["latitude"] ?? ""
                param["longitude"] = Settings.currentLocation?["longitude"] ?? ""
            }
        }
        let productID = dataObject.object(forKey: "product_id") as! String;
        if(dataObject.count > 0){
            param["prodID"] = productID
            self.makeRequestToAPI("mobiconnect/catalog/view/",dataToPost:param);
        }
        else if(product_id != ""){
            param["prodID"] = product_id
            self.makeRequestToAPI("mobiconnect/catalog/view/",dataToPost: param);
        }
        
        
    }
    
    
    override func recieveResponse(data: Data?, requestUrl: String?, response: URLResponse?) {
        guard let data                  = data else { return }
        do {
            //   print( try JSON(data: data ))
            let json                    = try JSON(data: data)
            print(json)
            if(requestUrl == "mobiconnect/addtocompare"){
                if json[0]["data"]["status"].stringValue == "true"{
                    self.view.makeToast(json[0]["data"]["message"].stringValue, duration: 2.0, position: .bottom)
                }
                else{
                    self.view.makeToast(json[0]["data"]["message"].stringValue, duration: 2.0, position: .bottom)
                }
            }
            else{
                if json[0]["data"]["status"].stringValue == "true"{
                    self.view.makeToast(json[0]["data"]["message"].stringValue, duration: 2.0, position: .bottom)
                    var myMutableString = NSMutableAttributedString()
                    var text = json["data"]["cod"].stringValue
                    text += "\n"
                    text += json["data"]["days"].stringValue
                    
                    let attrs1 = [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 14), NSAttributedString.Key.foregroundColor : UIColor.init(hexString: "#649568")]
                    
                    let attrs2 = [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 14), NSAttributedString.Key.foregroundColor : UIColor.init(hexString:Settings.textColor)]
                    
                    let attributedString1 = NSMutableAttributedString(string:json["data"]["cod"].stringValue, attributes:attrs2)
                    
                    let attributedString2 = NSMutableAttributedString(string:"\n"+json["data"]["days"].stringValue, attributes:attrs1)
                    
                    attributedString1.append(attributedString2)
                    self.PincodeCheckerView.pincodeResultLbl.attributedText = attributedString1
                    self.PincodeCheckerView.pincodeResultLbl.numberOfLines = 0
                }
                else{
                    //    self.view.makeToast(json[0]["data"]["message"].stringValue, duration: 2.0, position: .bottom)
                    self.PincodeCheckerView.pincodeResultLbl.text = json["data"]["message"].stringValue
                    self.PincodeCheckerView.pincodeResultLbl.numberOfLines = 0
                }
            }
            
        } catch {
            print(error.localizedDescription)
        }
    }
    
    override func parseResponseReceivedFromAPI(_ jsonResponse:JSON){
        
        print("jsonResponse");
        print(jsonResponse);
        self.fetchAllProductTypeData(jsonResponse: jsonResponse);
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        for view in webView.subviews as [UIView] {
            if let activityIndicator = view as? UIActivityIndicatorView {
                activityIndicator.removeFromSuperview();
            }
        }
    }
    
    @objc func renderProductSinglePage(){
        
        /* self.makeSomeSpaceInStackView(height: CGFloat(0.0));
         // Product Image And Gallery Section
         productImageView = ProductParallaxImageView();
         let productImageViewHeight = translateAccordingToDevice(CGFloat(375.0));
         productImageView.heightAnchor.constraint(equalToConstant: productImageViewHeight).isActive = true;
         productImageView.rootPage = self
         
         if productInfoArray["offer"] != "" {
         productImageView.offerText.isHidden = false
         productImageView.offerText.text = productInfoArray["offer"]! + "% OFF"
         }else{
         productImageView.offerText.isHidden = true
         }
         stackView.addArrangedSubview(productImageView);
         self.setLeadingAndTralingSpace(productImageView);
         
         let tapGesture = UITapGestureRecognizer(target: self, action: #selector(ProductSinglePageViewController.productImageTapped(_:)));
         productImageView.productMainImage.addGestureRecognizer(tapGesture);
         tapGesture.delegate=self */
        
        self.makeSomeSpaceInStackView(height: CGFloat(0.0));
        // Product Image And Gallery Section
        productImageView = productPageImagesView();
        let productImageViewHeight = translateAccordingToDevice(CGFloat(375.0));
        productImageView.heightAnchor.constraint(equalToConstant: productImageViewHeight).isActive = true;
        // productImageView.rootPage = self
        
        if productInfoArray["offer"] != "" {
            productImageView.offerText.isHidden = false
            productImageView.offerText.text = productInfoArray["offer"]! + "% OFF"
        }else{
            productImageView.offerText.isHidden = true
        }
        stackView.addArrangedSubview(productImageView);
        self.setLeadingAndTralingSpace(productImageView);
        
        if(productInfoArray["stock"] != "IN STOCK"){
            productImageView.outOfStock.isHidden = false;
        }else {
            productImageView.outOfStock.isHidden = true;
        }
        
        
        //    if(productInfoArray["stock"] != "IN STOCK"){
        //      productImageView.productMainImage.alpha = 0.5;
        //      productImageView.outOfStockImage.isHidden = false;
        //    }
        
        // productImageView.showSimilarProductsButton.addTarget(self, action: #selector(ProductSinglePageViewController.showSimilarProductsButtonPressed(_:)), for: UIControl.Event.touchUpInside);
        if(productInfoArray["main-prod-img"] != nil){
            productImageView.productMainImage.sd_setImage(with: URL(string: productInfoArray["main-prod-img"]!), placeholderImage: UIImage(named: "placeHolder.gif"))
            // downloadImageFromServer(productImageView.productMainImage,urlToRequest:productInfoArray["main-prod-img"]!);
        }
        
        // productImageView.wishlistButton.addTarget(self, action: #selector(ProductSinglePageViewController.wishlistButtonPressed(_:)), for: UIControl.Event.touchUpInside);
        print("DEVENDRA::productImgsArray");
        print(productImgsArray);
        productImageView.productImages = self.productImgsArray;
        
        //module check
        if(!cedMage.checkModule(string:"Ced_MobiconnectWishlist")){
            //  productImageView.wishlistButton.isHidden = true;
        }
        
        // Product Name And Price Section
        productNamePriceView = ProductNamePriceView();
        var productNamePriceViewHeight = translateAccordingToDevice(CGFloat(70.0));
        stackView.addArrangedSubview(productNamePriceView);
        productNamePriceView.productName.text = productInfoArray["product-name"]!;
        //
        productNamePriceView.productName.fontColorTool()
        if(self.productInfoArray["type"] != "grouped"){
            productNamePriceView.regularPriceLabel.text = "PRICE : ".localized+productInfoArray["currency_symbol"]!+productInfoArray["regular_price"]!;
            productNamePriceView.regularPriceLabel.fontColorTool()
            if(productInfoArray["special_price"] != nil && productInfoArray["special_price"] != ""){
                let attr = [NSAttributedString.Key.strikethroughStyle:1]
                let regular_price = NSAttributedString(string:productInfoArray["currency_symbol"]!+productInfoArray["regular_price"]!,attributes: attr)
                productNamePriceView.regularPriceLabel.attributedText = regular_price;
                
                let color_attribute = [ NSAttributedString.Key.foregroundColor: UIColor.red ];
                let special_price = NSMutableAttributedString(string: productInfoArray["currency_symbol"]!+productInfoArray["special_price"]!, attributes: color_attribute)
                
                productNamePriceView.specialPriceLabel.attributedText = special_price;
                
            }
            else{
                productNamePriceView.specialPriceLabel.isHidden = true;
            }
        }
        else{
            productNamePriceView.regularPriceLabel.removeFromSuperview();
            productNamePriceView.specialPriceLabel.removeFromSuperview();
            productNamePriceViewHeight = translateAccordingToDevice(CGFloat(35.0));
        }
        productNamePriceView.heightAnchor.constraint(equalToConstant: productNamePriceViewHeight).isActive = true;
        self.setLeadingAndTralingSpace(productNamePriceView);
        
        // render configurable product section
        if(self.productInfoArray["type"] == "configurable"){
            self.renderConfigurableProductSection();
        }
        
        // render bundle product section
        if(self.productInfoArray["type"] == "bundle"){
            self.renderBundleProductOptions();
        }
        
        // render grouped product section
        if(self.productInfoArray["type"] == "grouped"){
            self.renderGroupedProducts();
        }
        
        // render downloadable product section
        if(self.productInfoArray["type"] == "downloadable"){
            self.renderDownloadableProductSection();
        }
        
        // render custom options section
        if(custom_option.count > 0){
            self.renderCustomOptionsView();
        }
        //MARK:- PinCode CHecker
        
        let PincodeCheckerViewHeight = translateAccordingToDevice(CGFloat(130.0));
        //productQtyView.backgroundColor  = UIColor.blue;
        self.PincodeCheckerView.heightAnchor.constraint(equalToConstant: PincodeCheckerViewHeight).isActive = true;
        stackView.addArrangedSubview(PincodeCheckerView);
        self.PincodeCheckerView.checkBtn.backgroundColor = Settings.themeColor
        self.PincodeCheckerView.checkBtn.addTarget(self, action: #selector(checkPincodeTapped(_:)), for: .touchUpInside)
        self.PincodeCheckerView.pincodeLbl.text = productInfoArray["pincode_label"]!
        self.PincodeCheckerView.pincodeResultLbl.text = ""
        self.setLeadingAndTralingSpace(self.PincodeCheckerView);
        // Product Quantity Section
        if(self.productInfoArray["type"] != "grouped"){
            productQtyView = ProductQtyView();
            let productQtyViewHeight = translateAccordingToDevice(CGFloat(100.0));
            //productQtyView.backgroundColor  = UIColor.blue;
            productQtyView.heightAnchor.constraint(equalToConstant: productQtyViewHeight).isActive = true;
            stackView.addArrangedSubview(productQtyView);
            
            if(self.item_id != ""){
                productQtyView.productQty.text = self.previousQty;
                productQtyView.productQty.fontColorTool()
            }
            self.setLeadingAndTralingSpace(productQtyView);
        }
        
        //Product Share Section
        productShareView = cedMageShareAndWish()
        productShareView.wishListLabel.fontColorTool()
        productShareView.shareButton.fontColorTool()
        
        if(self.productInfoArray["Inwishlist"] == "OUT"){
            productShareView.wishListImage.image = UIImage(named:"LikeEmpty")
            productShareView.wishListLabel.text = "WISHLIST".localized
        }
        else{
            productShareView.wishListLabel.text = "WISHLIST".localized
            productShareView.wishListImage.image = UIImage(named:"LikeFilled")
        }
        var productShareViewHeight = CGFloat()
        if (self.productInfoArray["compareBool"] == "true" || self.productInfoArray["wishBool"] == "true"){
            productShareViewHeight  =
                translateAccordingToDevice(CGFloat(0.0));
            productShareView.wishListLabel.isHidden = true
            productShareView.wishListImage.isHidden=true
            productShareView.sharelabel.isHidden=true
        }else{
            productShareViewHeight =
                translateAccordingToDevice(CGFloat(50.0));
            productShareView.wishListLabel.isHidden = false
            productShareView.wishListImage.isHidden=false
            productShareView.sharelabel.isHidden=false
        }
        productShareView.sharelabel.text = "Add to compare".localized
        //productShareView.shareButton.setTitle("Share".localized, for: .normal)
        productShareView.shareButton.addTarget(self, action: #selector(ProductSinglePageViewController.addToCompareTapped(_:)), for: .touchUpInside)
        productShareView.wishListButton.addTarget(self, action: #selector(ProductSinglePageViewController.wishlistButtonPressed(_:)), for: .touchUpInside)
        
        //let productShareViewHeight =
        //translateAccordingToDevice(CGFloat(50.0));
        productShareView.backgroundColor  = UIColor.blue;
        productShareView.heightAnchor.constraint(equalToConstant: productShareViewHeight).isActive = true;
        stackView.addArrangedSubview(productShareView);
        self.setLeadingAndTralingSpace(productShareView);
        
        //Rendering detailView
        if let description = productInfoArray["description"]{
            if(description != ""){
                
                let color = Settings.textColor
                let detailsView = detailViewExpand()
                detailsView.clickButton.setTitle("Specification".localized, for: .normal)
                
                let htmlString = "<body text="+color+">" + description + "</body>"
                do {
                    detailsView.expandLabel.font = UIFont.systemFont(ofSize: 12)
                    detailsView.expandLabel.attributedText = htmlString.html2AttributedString
                    stackView.addArrangedSubview(detailsView);
                    self.setLeadingAndTralingSpace(detailsView);
                    detailsView.expandLabel.layoutIfNeeded()
                    detailsView.layoutIfNeeded()
                }
            }
        }
        
        //MARK:- product card section
        print(self.productInfoArray)
        
        
        print(self.giftData)
        productCardSection.priceTextField.layer.borderColor = UIColor.gray.cgColor
        productCardSection.fromTextField.layer.borderColor = UIColor.gray.cgColor
        productCardSection.nameTextField.layer.borderColor = UIColor.gray.cgColor
        productCardSection.dateTextField.layer.borderColor = UIColor.gray.cgColor
        productCardSection.emailTextField.layer.borderColor = UIColor.gray.cgColor
        productCardSection.textView.layer.borderColor = UIColor.gray.cgColor
        
        print(self.productURLarray)
        if(self.productInfoArray["type"] == "giftcard"){
            let productCardSectionHeight = translateAccordingToDevice(CGFloat(750.0))
            self.productCardSection.heightAnchor.constraint(equalToConstant: productCardSectionHeight).isActive = true
            productCardSection.productImgsArray = productURLarray
            productCardSection.calendarButton.addTarget(self,action: #selector(buttonTapped(_:)), for: .touchUpInside);
            stackView.addArrangedSubview(productCardSection)
            self.setLeadingAndTralingSpace(self.productCardSection)
        }
        
        
        //MARK: Render AdditionalInfoView
        
        if(self.moreInfoData.count != 0){
            
            //  let color = cedMage.getInfoPlist(fileName:"cedMage",indexString: "fontColor") as! String
            
            self.additionalInfo.additionalInfoBtn.setTitle("Additional Info".localized, for: .normal)
            
            self.additionalInfo.expandButton.image = UIImage(named:"IQButtonBarArrowRight")
            //                self.additionalInfo.vStack.isHidden = true
            //                let additionalInfoHeight = translateAccordingToDevice(CGFloat(50.0));
            //                self.additionalInfo.heightAnchor.constraint(equalToConstant: additionalInfoHeight).isActive = true
            
            // additionalInfo.additionalInfoBtn.addTarget(self, action:#selector(hideShowAdditionalInfo(_:)) , for: .touchUpInside)
            stackView.addArrangedSubview(additionalInfo);
            self.setLeadingAndTralingSpace(additionalInfo);
            additionalInfo.vStack.layoutIfNeeded()
            additionalInfo.layoutIfNeeded()
            
            self.additionalInfo.additionalInfoBtn.addTarget(self, action: #selector(additionalInfoButtonClicked(_:)), for: .touchUpInside)
        }
        //MoreInfo Button
        let moreView = RevealProductInfoView();
        let moreViewheight = translateAccordingToDevice(CGFloat(50.0));
        moreView.heightAnchor.constraint(equalToConstant: moreViewheight).isActive = true;
        stackView.addArrangedSubview(moreView);
        moreView.revealButton.setTitle("More Info".localized, for: UIControl.State.normal);
        moreView.revealButton.addTarget(self, action: #selector(ProductSinglePageViewController.viewDetailDescriptionTapped), for: UIControl.Event.touchUpInside);
        self.setLeadingAndTralingSpace(moreView);
        
        //Rendering Product Reviews
        if self.productInfoArray["review_count"] != "0"{
            if(cedMage.checkModule(string:"MageNative_Mobireview")){
                let reviewView = cedMageReviewView()
                reviewView.avgReview.fontColorTool()
                reviewView.totalReviews.fontColorTool()
                reviewView.reviewNRatingLabel.text = "Review & Rating".localized
                reviewView.avgReview.text = self.productInfoArray["review"]
                reviewView.totalReviews.text  = self.productInfoArray["review_count"]! +
                    " " + "Reviews".localized
                let reviewViewHeight = translateAccordingToDevice(CGFloat(150.0));
                reviewView.backgroundColor  = UIColor.blue;
                reviewView.heightAnchor.constraint(equalToConstant: reviewViewHeight).isActive = true;
                stackView.addArrangedSubview(reviewView);
                _ = translateAccordingToDevice(CGFloat(200.0));
                self.setLeadingAndTralingSpace(reviewView);
                for review in productReviews{
                    let reviewRating = productReviewListing()
                    reviewRating.reviewTitle.text = review["review-title"]
                    reviewRating.reviewDescription.text = review["reviewDescription"]
                    reviewRating.reviewDescription.sizeToFit()
                    reviewRating.postedBy.text =  review["review-by"]! + "\t" + review["posted_on"]!
                    reviewRating.avgReview.text = review["review_avg"]
                    self.colorRatingView(rating: review["review_avg"]!,view: reviewRating.reviewView)
                    stackView.addArrangedSubview(reviewRating);
                    self.setLeadingAndTralingSpace(reviewRating);
                    //                    reviewRating.avgReview.fontColorTool()
                    //                    reviewRating.reviewTitle.fontColorTool()
                    //                    reviewRating.reviewDescription.fontColorTool()
                    //                    reviewRating.postedBy.fontColorTool()
                }
                
                
            }
            
        }
        let more_reviewsButton = RevealProductInfoView();
        let more_reviews_height = translateAccordingToDevice(CGFloat(50.0));
        more_reviewsButton.heightAnchor.constraint(equalToConstant: more_reviews_height).isActive = true;
        stackView.addArrangedSubview(more_reviewsButton);
        more_reviewsButton.revealButton.setTitle("More Reviews".localized, for: UIControl.State.normal);
        //        more_reviewsButton.revealButton.fontColorTool()
        more_reviewsButton.revealButton.addTarget(self, action: #selector(ProductSinglePageViewController.show_more_reviews_page(_:)), for: UIControl.Event.touchUpInside);
        self.setLeadingAndTralingSpace(more_reviewsButton);
        //        if(cedMage.checkModule(string:"Ced_MobiconnectCms")){
        //            if(self.productInfoArray["cms-title"] != nil && self.productInfoArray["cms-title"] != ""){
        //                let cms_section = RevealProductInfoView();
        //                let cms_section_height = translateAccordingToDevice(CGFloat(50.0));
        //                cms_section.heightAnchor.constraint(equalToConstant: cms_section_height).isActive = true;
        //                stackView.addArrangedSubview(cms_section);
        //                cms_section.revealButton.setTitle(self.productInfoArray["cms-title"], for: UIControlState.normal);
        //                cms_section.revealButton.addTarget(self, action: #selector(ProductSinglePageViewController.show_cms_page(_:)), for: UIControlEvents.touchUpInside);
        //                self.setLeadingAndTralingSpace(cms_section);
        //            }
        //        }
        //        self.makeSomeSpaceInStackView(height: CGFloat(0.0));
        
        // render add to cart button
        self.render_addToCart_button();
        
        
        //MARK: render Similar Product Section
        if(similarProductArray.count > 0){
            similarProductView = SimilarProductView();
            similarProductView.translatesAutoresizingMaskIntoConstraints = false;
            similarProductView.productSinglePageViewControllerRef = self;
            similarProductView.topLabel.text = "Related Products".localized//.uppercased();
            similarProductView.topLabel.backgroundColor = .clear//setThemeColor();
            //        similarProductView.topLabel.textColor = UIColor.white;
            similarProductView.similarProductArray = similarProductArray;
            
            similarProductView.currency_symbol = productInfoArray["currency_symbol"]!;
            let similarProductViewHeight = translateAccordingToDevice(CGFloat(300.0));
            similarProductView.backgroundColor  = UIColor.blue;
            stackView.addArrangedSubview(similarProductView);
            self.setLeadingAndTralingSpace(similarProductView);
            similarProductView.heightAnchor.constraint(equalToConstant: similarProductViewHeight).isActive = true;
        }
        
        
        //MARK: rendering upsell product
        if self.upsellProductArray.count != 0 {
            
            let upsellCollection = SimilarProductView()
            upsellCollection.translatesAutoresizingMaskIntoConstraints = false;
            let upsellViewHeight = translateAccordingToDevice(CGFloat(300.0));
            upsellCollection.heightAnchor.constraint(equalToConstant: upsellViewHeight).isActive = true
            stackView.addArrangedSubview(upsellCollection)
            upsellCollection.productSinglePageViewControllerRef = self
            upsellCollection.topLabel.text = "Upsell Products"
            upsellCollection.topLabel.backgroundColor = .clear
            upsellCollection.currency_symbol = productInfoArray["currency_symbol"]!
            upsellCollection.similarProductArray = upsellProductArray;
            self.setLeadingAndTralingSpace(upsellCollection)
            
        }
        
        // rendering crosssell product
        if self.crosssellProductArray.count != 0 {
            
            let crosssellCollection = SimilarProductView()
            crosssellCollection.translatesAutoresizingMaskIntoConstraints = false;
            let crosssellViewHeight = translateAccordingToDevice(CGFloat(300.0));
            crosssellCollection.heightAnchor.constraint(equalToConstant: crosssellViewHeight).isActive = true
            stackView.addArrangedSubview(crosssellCollection)
            crosssellCollection.productSinglePageViewControllerRef = self
            crosssellCollection.currency_symbol = productInfoArray["currency_symbol"]!
            crosssellCollection.topLabel.text = "Crosssell Products"
            crosssellCollection.topLabel.backgroundColor = .clear
            crosssellCollection.similarProductArray = crosssellProductArray;
            self.setLeadingAndTralingSpace(crosssellCollection)
            
        }
        
        // self.updateCartCount();
        // overlay image
        imageViewAsOverlay.isUserInteractionEnabled = true;
        let imageViewAsOverlay_tapGesture = UITapGestureRecognizer(target: self, action: #selector(ProductSinglePageViewController.dismissFloatingButtonSection(_:)));
        imageViewAsOverlay.addGestureRecognizer(imageViewAsOverlay_tapGesture);
        imageViewAsOverlay_tapGesture.delegate=self
        imageViewAsOverlay.translatesAutoresizingMaskIntoConstraints = false;
        self.view.addSubview(imageViewAsOverlay);
        imageViewAsOverlay.isHidden = true;
        imageViewAsOverlay.backgroundColor = UIColor(hexString: "#D2D7D3");
        imageViewAsOverlay.alpha = 0.95;
        
        // render Floating button section
        // self.renderFloatingActionButton();
        
        // Review Popup Section
        self.renderProductReviewPopupSection();
        
    }
    
    @objc func buttonTapped(_ sender: UIButton) {
        print("-------")
        let calendarView = CalendarView(frame: CGRect(x: 0, y: 0, width: 350, height: 300))
        calendarView.center = CGPoint(x: view.frame.size.width  / 2, y: view.frame.size.height/2-20)
        
        
        calendarView.cancel.addTarget(self, action: #selector(cancelbuttonTapped(_:)), for: .touchUpInside)
        calendarView.done.addTarget(self, action: #selector(selectButtonTapped(_:)), for: .touchUpInside)
        calendarView.calendarView.dataSource = self
        calendarView.calendarView.delegate = self
        self.view.addSubview(calendarView)
        
        
        
    }
    
    
    
    @objc func cancelbuttonTapped(_ sender:UIButton)
    {
        self.view.viewWithTag(256)?.removeFromSuperview()
    }
    
    
    @objc func selectButtonTapped(_ sender:UIButton)
    {
        print(calendarView)
        print(calendarView.calendarView)
        let dateFormatter = DateFormatter()
        
        
        dateFormatter.dateFormat = "MM/dd/YYYY";
        
        //           var selectedDate = Date()
        //            //print(selectedDate)
        //            if(dateUsed == nil) //&& dateJson["enable_calender"].stringValue == "1")
        //            {
        //                dateUsed = Date();
        //            }
        //            else if dateUsed != nil
        //            {
        //                print(calendarView.calendarView.selectedDate)
        //                dateUsed = calendarView.calendarView.selectedDate!
        //                print("*******")
        //                print(dateUsed)
        //            }
        //            else{
        //                self.view.makeToast("Selected date not available!", duration: 2.0, position: .center)
        //                return;
        //            }
        //    print(selectedDate)
        print(dateFormatter.string(from: dateUsed))
        var date = dateFormatter.string(from: dateUsed)
        print(date)
        productCardSection.dateTextField.text = date
        self.view.viewWithTag(256)?.removeFromSuperview()
        
    }
    @objc func checkPincodeTapped(_ sender:UIButton){
        
        var postData = ["postcode":self.PincodeCheckerView.pinTextField.text ?? "" ]
        if(dataObject.count > 0){
            postData["prodID"] = dataObject.object(forKey: "product_id") as! String;
            self.sendRequest(url: "mobivpincode/checkpin", params: postData, store: false)
        }}
    
    @objc func additionalInfoButtonClicked(_ sender: UIButton){
        additionalInfoHiddenCheck = !additionalInfoHiddenCheck
        if(additionalInfoHiddenCheck)
        {
            additionalInfo.vStack.subviews.forEach { (nestedView) in
                nestedView.removeFromSuperview()
                self.additionalInfo.expandButton.image = UIImage(named:"IQButtonBarArrowRight")
            }
        }
        else{
            for t in 0..<self.moreInfoData.count{
                for (i,v) in self.moreInfoData[t]{
                    let lblKey = UILabel()
                    let lblValue = UILabel()
                    //                    lblKey.textAlignment = .center
                    //                    lblValue.textAlignment = .center
                    lblKey.textColor = .lightGray
                    lblValue.textColor = .lightGray
                    lblKey.text = i
                    lblValue.text = v
                    
                    lblKey.font = UIFont.boldSystemFont(ofSize: 13)
                    lblValue.font = UIFont.systemFont(ofSize: 13)
                    
                    let hStack = UIStackView()
                    hStack.alignment = .firstBaseline
                    hStack.distribution = .fillEqually
                    hStack.axis = .horizontal
                    hStack.spacing = 5.0
                    
                    hStack.addArrangedSubview(lblKey)
                    hStack.addArrangedSubview(lblValue)
                    additionalInfo.vStack.addArrangedSubview(hStack)
                    additionalInfo.vStack.spacing = 1.0
                    self.additionalInfo.expandButton.image = UIImage(named:"IQButtonBarArrowDown")
                }
            }
        }
        
    }
    
    func render_addToCart_button(){
        
        let addToCartButton = addToCartAndWish()
        addToCartButton.translatesAutoresizingMaskIntoConstraints = false;
        addToCartButton.addToCartLabel.text = "ADD TO CART".localized
        addToCartButton.buyNowLabel.text = "BUY NOW".localized
        //  addToCartButton.AddToWish.setTitle("BUY NOW".localized, for: .normal)
        addToCartButton.AddToWish.addTarget(self, action: #selector(ProductSinglePageViewController.addToCart(_:)), for: UIControl.Event.touchUpInside);
        addToCartButton.addToCart.addTarget(self, action: #selector(ProductSinglePageViewController.addToCart(_:)), for: UIControl.Event.touchUpInside);
        self.buyNowButton = addToCartButton.AddToWish
        self.addTocartLabel = addToCartButton.addToCartLabel
        self.addToCartButton =  addToCartButton.addToCart
        let addToCartButtonHeight = translateAccordingToDevice(CGFloat(50));
        addToCartButton.heightAnchor.constraint(equalToConstant: addToCartButtonHeight).isActive = true;
        self.view.addSubview(addToCartButton);
        
        self.setLeadingAndTralingSpaceFormParentView(addToCartButton, parentView: self.view, padding: 0)
        /* if #available(iOS 11.0, *) {
         let window = UIApplication.shared.keyWindow
         if let bottomPadding = window?.safeAreaInsets.bottom {
         self.view.addConstraint(NSLayoutConstraint(item: addToCartButton, attribute: NSLayoutConstraint.Attribute.bottom, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: -(bottomPadding)));
         }
         }else {
         self.view.addConstraint(NSLayoutConstraint(item: addToCartButton, attribute: NSLayoutConstraint.Attribute.bottom, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: 0));
         } */
        addToCartButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
    }
    
    @objc func show_more_reviews_page(_ sender:UIButton){
        print("show_cms_page Tapped");
        let storyboard = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil);
        let viewController = storyboard.instantiateViewController(withIdentifier: "productReviewListingViewController") as! ProductReviewListingViewController;
        viewController.product_id = self.productInfoArray["product-id"]!
        self.navigationController?.pushViewController(viewController, animated: true);
    }
    var isFixed=false
    func updateProductPrice(){
        var updatedPrice = Float(0.0);
        var regular_price = Float(0.0);
        if(productInfoArray["regular_price"] != nil && productInfoArray["regular_price"] != ""){
            regular_price = Float(productInfoArray["regular_price"]!)!;
        }
        print(priceCalculationArray)
        for(_,val) in priceCalculationArray{
            
            if !self.isFixed{
                updatedPrice = updatedPrice+val;
                updatedPrice = updatedPrice+regular_price;
            }
            else
            {
                updatedPrice = val;
            }
        }
        
        if !self.isFixed{
            updatedPrice = updatedPrice+regular_price;
        }
        productNamePriceView.regularPriceLabel.text = "PRICE : ".localized+productInfoArray["currency_symbol"]!+String(updatedPrice);
        productNamePriceView.regularPriceLabel.fontColorTool()
    }
    
    @objc func addToCart(_ sender:UIButton){
        
        if(self.productInfoArray["stock"]?.lowercased() == "OUT OF STOCK".lowercased()){
            let msg = "Product Is Out Of Stock".localized;
            self.view.makeToast(msg, duration: 2.0, position: .bottom, title: nil, image: nil, style: nil, completion: nil);
            return;
        }
        
        self.addToCartButton.isEnabled = false
        self.buyNowButton.isEnabled = false
        var qtyToBuy = "1";
        if(self.productInfoArray["type"] != "grouped"){
            if(productQtyView.productQty.text != ""){
                qtyToBuy = productQtyView.productQty.text!;
            }
        }
        // go and deal with custom-options
        self.custom_options_string = self.fetchCustomOptionsToPost();
        
        // for simple product and for virtual product
        if(self.productInfoArray["type"] == "simple" || self.productInfoArray["type"] == "virtual"){
            makeAddToCartRequestToAPI("mobiconnect/checkout/add/", dataToPost:["type":self.productInfoArray["type"]!, "product_id":self.productInfoArray["product-id"]!, "qty":qtyToBuy],button:sender);
        }
        // for simple product and for virtual product
        if(self.productInfoArray["type"] == "downloadable"){
            
            var links = "{";
            var linkCounter = 0;
            
            if(self.downloadRelatedData["links_purchased_separately"] == "0"){
                for (key,_) in self.links{
                    links += "\""+String(linkCounter)+"\":"+"\""+key+"\",";
                    linkCounter += 1;
                }
            }
            else{
                var currentIndex = 1;
                for (key,_) in self.links{
                    if let checkboxView = downloadableProductOptionsStackView.arrangedSubviews[currentIndex] as? CheckboxView {
                        if(checkboxView.checkboxButtonImage.image == UIImage(named: "CheckedCheckbox")){
                            links += "\""+String(linkCounter)+"\":"+"\""+key+"\",";
                            linkCounter += 1;
                        }
                    }
                    currentIndex += 1;
                }
            }
            
            if(links.last! == ","){
                links = links.substring(to: links.index(before: links.endIndex));
            }
            links += "}";
            
            makeAddToCartRequestToAPI("mobiconnect/checkout/add/", dataToPost:["type":self.productInfoArray["type"]!, "product_id":self.productInfoArray["product-id"]!, "qty":qtyToBuy, "links":links],button:sender);
        }
        
        // for configurable product
        if(self.productInfoArray["type"] == "configurable"){
            
            var super_attribute = "{";
            var i = 0
            for view in configOptionsStackView.subviews{
                if let customOptionDropDownView = view as? cedMageConfigProductView {
                    
                    let options = config_attribute_array[self.attributes["\(i)"]!["id"]!]
                    
                    if customOptionDropDownView.collectionView.indexPathsForSelectedItems != nil{
                        if(customOptionDropDownView.collectionView.indexPathsForSelectedItems?.count == 0){
                            let msg = "Please select the Desired product option"
                            self.view.makeToast(msg, duration: 2.0, position: .center, title: nil, image: nil, style: nil, completion: nil);
                            sender.isEnabled = true
                            //self.isEnabled = true
                            //self.addTocartLabel.text = "ADD TO CART"
                            return;
                        }
                        if let seletedAttributedIndex = customOptionDropDownView.collectionView.indexPathsForSelectedItems?.first {
                            let selectedOptions = options![seletedAttributedIndex.row]
                            print(selectedOptions)
                            super_attribute += "\""+self.attributes["\(i)"]!["id"]!+"\":";
                            super_attribute += "\""+selectedOptions["id"]!+"\",";
                        }
                    }
                    i = i+1
                }
            }
            
            if(super_attribute.last! == ","){
                super_attribute = super_attribute.substring(to: super_attribute.index(before: super_attribute.endIndex));
            }
            super_attribute += "}";
            print(super_attribute);
            
            if(self.item_id != ""){
                
                makeAddToCartRequestToAPI("mobiconnect/checkout/updatecartoptions", dataToPost:["item_id":self.item_id, "product":self.productInfoArray["product-id"]!, "type":self.productInfoArray["type"]!, "product_id":self.productInfoArray["product-id"]!, "super_attribute":super_attribute, "qty":qtyToBuy],button:sender);
            }
            else{
                
                
                makeAddToCartRequestToAPI("mobiconnect/checkout/add", dataToPost:["type":self.productInfoArray["type"]!, "product_id":self.productInfoArray["product-id"]!, "super_attribute":super_attribute, "qty":qtyToBuy],button:sender);
                
            }
            
        }
        
        // for bundle product
        if(self.productInfoArray["type"] == "bundle"){
            
            var bundle_option = "{";
            var bundle_option_qty = "{";
            for view in bundleProductOptionsStackView.subviews{
                
                if let bundleProductDropdownView = view as? BundleProductDropdownView{
                    let key = bundleProductDropdownView.topLabel.text!;
                    let selectionName = (bundleProductDropdownView.bundleProDropdownButton.titleLabel?.text)!;
                    let titleID = bundleddata_extraInfo_array[key]!["id"]!;
                    if(bundleddata_array[key]![selectionName] == nil){
                        let msg = "Please Select Option".localized;
                        self.view.makeToast(msg, duration: 2.0, position: .bottom, title: nil, image: nil, style: nil, completion: nil);
                        self.addToCartButton.isEnabled = true
                        self.buyNowButton.isEnabled = true
                        self.addTocartLabel.fontColorTool()
                        self.addTocartLabel.text = "ADD TO CART"
                        return;
                    }
                    let selectionID = bundleddata_array[key]![selectionName]!["selection_id"]!;
                    let qty = bundleProductDropdownView.qtyToBuy.text!;
                    
                    bundle_option += "\""+titleID+"\":";
                    bundle_option += "\""+selectionID+"\",";
                    
                    bundle_option_qty += "\""+titleID+"\":";
                    bundle_option_qty += "\""+qty+"\",";
                    
                }
                else{
                    print(view.subviews[0].subviews[0]);
                    var tempCounter = 0;
                    var key = "";
                    if let stackView = (view.subviews[0].subviews[0]) as? UIStackView{
                        for view in stackView.subviews{
                            
                            
                            if let label = view as? UILabel{
                                key = label.text!;
                                let titleID = bundleddata_extraInfo_array[key]!["id"]!;
                                bundle_option += "\""+titleID+"\":{";
                            }
                            if let bundleProductCheckboxView = view as? BundleProductCheckboxView{
                                if(bundleProductCheckboxView.bundleProCheckboxButtonImg!.image == UIImage(named:"CheckedCheckbox")){
                                    let val = (bundleProductCheckboxView.bundleProCheckboxButton.titleLabel?.text)!;
                                    let selection_id = bundleddata_array[key]![val]!["selection_id"]!;
                                    bundle_option += "\""+String(tempCounter)+"\":";
                                    bundle_option += "\""+selection_id+"\",";
                                    tempCounter = tempCounter+1;
                                }
                            }
                        }
                        if(bundle_option.last! != "{"){
                            bundle_option = bundle_option.substring(to: bundle_option.index(before: bundle_option.endIndex));
                        }
                        
                        bundle_option += "},";
                    }
                }
                
            }
            bundle_option = bundle_option.substring(to: bundle_option.index(before: bundle_option.endIndex));
            bundle_option += "}";
            print(bundle_option);
            
            bundle_option_qty = bundle_option_qty.substring(to: bundle_option_qty.index(before: bundle_option_qty.endIndex));
            bundle_option_qty += "}";
            print(bundle_option_qty);
            
            if(self.item_id != ""){
                makeAddToCartRequestToAPI("mobiconnect/checkout/updateoptions", dataToPost:["item_id":self.item_id, "product":self.productInfoArray["product-id"]!, "type":self.productInfoArray["type"]!, "bundle_option_qty":bundle_option_qty, "bundle_option":bundle_option, "qty":qtyToBuy],button:sender);
            }
            else{
                makeAddToCartRequestToAPI("mobiconnect/checkout/add/", dataToPost:["type":self.productInfoArray["type"]!, "product_id":self.productInfoArray["product-id"]!, "bundle_option_qty":bundle_option_qty, "bundle_option":bundle_option, "qty":qtyToBuy],button:sender);
            }
            
            
        }
        
        // for grouped product
        if(self.productInfoArray["type"] == "grouped"){
            
            var super_group = "{";
            var i = 0;
            for view in groupedProductsStackView.subviews{
                if let groupedProductView = view as? GroupedProductView{
                    let qtyToBuy = groupedProductView.qtyToBuy.text!;
                    let pro_id = grouped_product_array[i]["group-product-id"]!;
                    if(qtyToBuy != ""){
                        super_group += "\""+pro_id+"\":"+"\""+qtyToBuy+"\",";
                    }
                    else{
                        super_group += "\""+pro_id+"\":"+"\""+qtyToBuy+"\",";
                    }
                }
                i = i+1;
            }
            super_group = super_group.substring(to: super_group.index(before: super_group.endIndex));
            super_group += "}";
            
            makeAddToCartRequestToAPI("mobiconnect/checkout/add/", dataToPost:["type":self.productInfoArray["type"]!, "product_id":self.productInfoArray["product-id"]!, "super_group":super_group, "qty":qtyToBuy],button:sender);
            
        }
    }
    
    
    func makeAddToCartRequestToAPI(_ urlToRequest:String, dataToPost:[String:String],button:UIButton){
        
        var urlToRequest = urlToRequest
        let requestHeader = Settings.headerKey;
        let baseURL = Settings.baseUrl
        
        urlToRequest = baseURL+urlToRequest;
        
        var postString = "";
        var postData = [String:String]()
        if defaults.bool(forKey: "isLogin") {
            postData["hashkey"] = userInfoDict["hashKey"]
            postData["customer_id"] = userInfoDict["customerId"]
            for (key,val) in dataToPost{
                postData[key] = val;
            }
        }
        else{
            if(defaults.object(forKey: "cartId") != nil){
                let cart_id = defaults.object(forKey: "cartId") as! String;
                postData["cart_id"] = cart_id;
            }
            for (key,val) in dataToPost{
                postData[key] = val;
            }
        }
        
        if let filterType = UserDefaults.standard.value(forKey: "filterType") as? String {
            if filterType == "zipcode" {
                postData["postcode"] = Settings.currentLocation?["zipcode"] ?? ""
            } else if filterType == "city_state_country" {
                postData["city"] = Settings.currentLocation?["city"] ?? ""
                postData["state"] = Settings.currentLocation?["state"] ?? ""
                postData["country_id"] = Settings.currentLocation?["country"] ?? ""
            } else {
                postData["latitude"] = Settings.currentLocation?["latitude"] ?? ""
                postData["longitude"] = Settings.currentLocation?["longitude"] ?? ""
            }
        }
        if(self.custom_option.count > 0){
            postData["Custom"] = "{\"options\":"+self.custom_options_string+"}";// in case of custom-options
        }
        if(self.datePickerTagsArray.count > 0){
            for (val) in self.datePickerTagsArray{
                postData["validate_datetime_"] = String(val)+"=\" \"";
            }
        }
        postString = ["parameters":postData].convtToJson() as String
        
        
        print(urlToRequest);
        print(postString);
        var request = URLRequest(url: URL(string: "\(urlToRequest)")!);
        request.httpMethod = "POST";
        request.httpBody = postString.data(using: String.Encoding.utf8);
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        if UserDefaults.standard.bool(forKey: "isLogin"){
            if let user = UserDefaults.standard.object(forKey: "userInfoDict") as? [String:String] {
                request.setValue(user["hashKey"], forHTTPHeaderField: "hashkey")
            }
            
        }
        
        cedMageLoaders.addDefaultLoader(me: self);
        let task = URLSession.shared.dataTask(with: request){
            
            // check for fundamental networking error
            data, response, error in
            guard error == nil && data != nil else{
                print("error=\(error)")
                DispatchQueue.main.async{
                    print(error?.localizedDescription as Any);
                    cedMageLoaders.removeLoadingIndicator(me: self);
                    self.addToCartButton.isEnabled = true
                    self.buyNowButton.isEnabled = true
                    self.addToCartButton.setTitle("ADD TO CART".localized,for:.normal)
                    self.addToCartButton.fontColorTool()
                    self.addTocartLabel.text = "ADD TO CART".localized
                    self.addTocartLabel.fontColorTool()
                }
                return;
            }
            
            // check for http errors
            if let httpStatus = response as? HTTPURLResponse , httpStatus.statusCode != 200{
                
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(response)")
                DispatchQueue.main.async{
                    self.addToCartButton.isEnabled = true
                    self.buyNowButton.isEnabled = true
                    
                    self.addTocartLabel.text = "ADD TO CART".localized
                    self.addTocartLabel.fontColorTool()
                    cedMageLoaders.removeLoadingIndicator(me: self);
                    
                }
                return;
            }
            
            // code to fetch values from response :: start
            guard let jsonResponse = try? JSON(data: data!) else{return;}
            if(jsonResponse != nil){
                
                DispatchQueue.main.async{
                    
                    print(jsonResponse);
                    self.addToCartButton.isEnabled = true
                    self.buyNowButton.isEnabled = true
                    self.addTocartLabel.text = "ADD TO CART".localized
                    cedMageLoaders.removeLoadingIndicator(me: self);
                    
                    if(self.item_id != ""){
                        let msg = jsonResponse["message"].stringValue;
                        self.view.makeToast(msg, duration: 2.0, position: .bottom, title: nil, image: nil, style: nil, completion: nil);
                        
                        if(jsonResponse["success"].stringValue == "true"){
                            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + .seconds(1)) {
                                let storyboard = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil);
                                let viewController = storyboard.instantiateViewController(withIdentifier: "cartViewController") as! CartViewController;
                                self.navigationController?.viewControllers = [viewController];
                                if(jsonResponse[0]["cart_id"]["success"].stringValue == "true"){
                                    self.defaults.set(jsonResponse[0]["cart_id"]["cart_id"].stringValue, forKey: "cartId");
                                    self.defaults.set(jsonResponse[0]["cart_id"]["items_count"].stringValue, forKey: "items_count");
                                    self.renderAddToCartAnimation(button:button);
                                }
                            }
                        }
                    }
                    else{
                        
                        let msg = jsonResponse[0]["cart_id"]["message"].stringValue;
                        print(msg)
                        if(msg != ""){
                            self.view.makeToast(msg)
                            if(jsonResponse[0]["cart_id"]["success"].stringValue == "true"){
                                self.defaults.set(jsonResponse[0]["cart_id"]["cart_id"].stringValue, forKey: "cartId");
                                self.defaults.set(jsonResponse[0]["cart_id"]["items_count"].stringValue, forKey: "items_count");
                                
                                self.renderAddToCartAnimation(button:button);
                            }
                        }
                        else{
                            let msg = jsonResponse[0]["message"].stringValue;
                            self.view.makeToast(msg, duration: 2.0, position: .bottom, title: nil, image: nil, style: nil, completion: nil);
                        }
                    }
                }
            }
            
        }
        task.resume();
    }
    
    
    
    func setLeadingAndTralingSpace(_ view:UIView){
        
        stackView.addConstraint(NSLayoutConstraint(item: view, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: stackView, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: padding/2));
        stackView.addConstraint(NSLayoutConstraint(item: view, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: stackView, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: -padding/2));
        
    }
    
    
    @objc func productImageTapped(_ recognizer: UITapGestureRecognizer){
        let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "imageRoot") as! imageSliderRoot
        cedMage().storeParameterInteger(parameter: 0)
        viewController.pageData = productImgsArray as NSArray
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func description_detail_tapped(_ recognizer: UITapGestureRecognizer){
        
        print("description_detail_tapped");
        let storyboard = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil);
        let viewController = storyboard.instantiateViewController(withIdentifier: "productDetailDescriptionViewController") as! ProductDetailDescriptionViewController;
        viewController.productDetail = self.productInfoArray["short-description"]!;
        viewController.productDescription = self.productInfoArray["description"]!;
        viewController.cmsPage = self.productInfoArray["cms-title"]!
        viewController.cmsUrl = self.productInfoArray["cms-url"]!
        
        self.navigationController?.pushViewController(viewController, animated: true);
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func colorRatingView(rating:String,view:UIView){
        let rate = Float(rating)!
        if(rate < 3) {
            view.backgroundColor = UIColor.red
        }else if(rate >= 3 && rate < 4 ){
            view.setThemeColor()
        }else{
            view.backgroundColor = UIColor.green
        }
    }
}
extension ProductSinglePageViewController: FSCalendarDataSource , FSCalendarDelegate{
    
    func maximumDate(for calendar: FSCalendar) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        return dateFormatter.date(from: "01-01-2050") ?? Date()
    }
    
    func minimumDate(for calendar: FSCalendar) -> Date {
        return Date()
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        print(date)
        dateUsed = date
    }
}

extension Data {
    var html2AttributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: self, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            print("error:", error)
            return  nil
        }
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
}

extension String {
    var html2AttributedString: NSMutableAttributedString? {
        return Data(utf8).normalAttributedString
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
}
extension Data{
    var normalAttributedString: NSMutableAttributedString? {
        do {
            
            
            let string = try NSMutableAttributedString(data: self, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
            if #available(iOS 13.0, *) {
                string.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.systemGray, range: NSMakeRange(0, string.length))
            } else {
                string.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range: NSMakeRange(0, string.length))
            }
            if(UIDevice.current.userInterfaceIdiom == .pad){
                string.addAttribute(NSAttributedString.Key.font, value: UIFont.systemFont(ofSize: 18), range: NSMakeRange(0, string.length))
            }
            else{
                string.addAttribute(NSAttributedString.Key.font, value: UIFont.systemFont(ofSize: 14), range: NSMakeRange(0, string.length))
            }
            return string;
            
        } catch {
            print("error:", error)
            return  nil
        }
    }
}


