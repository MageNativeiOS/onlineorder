//
//  referTableCell.swift
//  MageNative Magento Platinum
//
//  Created by cedcoss on 19/03/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class referTableCell: UITableViewCell {
    @IBOutlet weak var urlTxtField: UITextView!
    @IBOutlet weak var codeTextField: UITextView!
    @IBOutlet weak var emailTextField: UITextView!
    @IBOutlet weak var messageTextField: UITextView!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var submitButton: UIButton!
    var indexPath: IndexPath?
    var data = [String:String]()
    var viewModel: referViewModel? {
        didSet {
            urlTxtField.text = viewModel?.referData?.url
            let text = viewModel?.referData?.url
            print(text)
            if text != ""{
                if text == nil{
                   codeTextField.text = ""
                }else{
            let  code = text!.substring(from: text!.range(of: "refer_code")!.upperBound)
            
                    codeTextField.text = code.replacingOccurrences(of: "/", with: "")}}
            messageTextField.text = viewModel?.referData?.msg
            
           
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        urlTxtField.isEditable = false
        codeTextField.isEditable = false
        urlTxtField.layer.cornerRadius = 5.0
        codeTextField.layer.cornerRadius = 5.0
        emailTextField.layer.cornerRadius = 5.0
        messageTextField.layer.cornerRadius = 5.0
        urlTxtField.layer.borderWidth = 1.0
        codeTextField.layer.borderWidth = 1.0
        emailTextField.layer.borderWidth = 1.0
        messageTextField.layer.borderWidth = 1.0
        messageTextField.textAlignment = NSTextAlignment.left
        
        submitButton.layer.cornerRadius = 5.0
        submitButton.layer.borderWidth = 1.0
        submitButton.layer.borderColor = Settings.themeColor.cgColor
        submitButton.setTitleColor(.white, for: .normal)
        submitButton.backgroundColor = Settings.themeColor.withAlphaComponent(0.8)
        
        shareButton.layer.cornerRadius = 5.0
        shareButton.layer.borderWidth = 1.0
        shareButton.layer.borderColor = Settings.themeColor.cgColor
        shareButton.setTitleColor(Settings.themeColor, for: .normal)
        
        
        shareButton.addTarget(self, action: #selector(shareButtonTapped(_ :)), for: .touchUpInside)
        submitButton.addTarget(self, action: #selector(submitButtontapped(_:)), for: .touchUpInside)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    func populateData(for indexPath: IndexPath, with viewModel: referViewModel) {
           self.indexPath = indexPath
        self.viewModel = viewModel
       }
   @objc
    func submitButtontapped(_ sender: UIButton){
    data["emailId"] = emailTextField.text
    data["message"] = messageTextField.text
    data["url"] = urlTxtField.text
    viewModel?.submitButtonPressed(data: data)
    }
    @objc func shareButtonTapped(_ sender: UIButton) {
        viewModel?.share(message: "#Sainath" , link: urlTxtField.text!)
    }
    
}
