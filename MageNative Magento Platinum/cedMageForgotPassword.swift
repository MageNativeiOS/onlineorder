/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */


import UIKit

class cedMageForgotPassword: cedMageViewController {

    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var Save: UIButton!
    
    @IBOutlet weak var enterEmailLabel: UILabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
      //  topLabel.setThemeColor()
        topLabel.text = "Forgot Password".localized
        Save.setThemeColor()
        Save.addTarget(self, action: #selector(cedMageForgotPassword.updatePassword(sender:)), for: UIControl.Event.touchUpInside)
        Save.setTitle("Send".localized, for: UIControl.State.normal)
        Save.fontColorTool()
        enterEmailLabel.text = "Enter your Email here".localized
        email.placeholder = "Email(*)".localized
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func updatePassword(sender:UIButton){
      
        var email = self.email.text;
        email = email!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        if(email == "")
        {
            let title = "Error".localized
            let msg = "Email field is required!".localized
            cedMageHttpException.showAlertView(me: self, msg: msg, title: title)
      
            return;
        }
        
        // code to check email format is proper or not :: start
        let validEmail = EmailVerifier.isValidEmail(testStr: email!);
        if(!validEmail)
        {
            let title = "Error".localized
            let msg = "Invalid Email Address.".localized
           cedMageHttpException.showAlertView(me: self, msg: msg, title: title)
            
            return;
        }
        let postString = ["email":email!]
        self.sendRequest(url: "mobiconnect/customer/forgotpassword", params: postString)
        
    }

    override func recieveResponse(data: Data?, requestUrl: String?, response: URLResponse?) {
        guard var json = try? JSON(data:data!) else{return;}
        json = json[0]
        print(json)
        let status = json["data"]["customer"][0]["status"];
         if(status == "success"){
              _ =  self.navigationController?.popToRootViewController(animated: true)
            self.view.makeToast(json["data"]["customer"][0]["message"].stringValue, duration: 2, position: .center, title: "Success".localized, image: nil, style: nil, completion: nil)
            
         }else{
            let title = "Error".localized
            let msg = json["data"]["customer"][0]["message"].stringValue
            cedMageHttpException.showAlertView(me: self, msg: msg, title: title)
        }
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
