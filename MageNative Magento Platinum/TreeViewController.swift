//
//  TreeViewController.swift
//  RATreeViewExamples
//
//  Created by Rafal Augustyniak on 21/11/15.
//  Copyright © 2015 com.Augustyniak. All rights reserved.
//


import UIKit
import RATreeView

class TreeViewController: MagenativeUIViewController, RATreeViewDelegate, RATreeViewDataSource {

    var treeView : RATreeView!
    var data : [DataObject] = []

    convenience init() {
        self.init(nibName : nil, bundle: nil)
    }

    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        //data = TreeViewController.commonInit()
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }

    required init?(coder aDecoder: NSCoder) {
        //data = TreeViewController.commonInit()
        super.init(coder: aDecoder)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        //view.backgroundColor = .white

        if  let storeId = UserDefaults.standard.value(forKey: "storeId")  as? String {
            self.sendRequest(url: "mobiconnect/catalog/subcategories", params: ["store_id":storeId],store:false)
        }
        //title = "Things"
        //setupTreeView()
    }

    override func recieveResponse(data: Data?, requestUrl: String?, response: URLResponse?) {
        /*if let data = data {
            let jsonData = JSON(data:data)
            //print(jsonData)
            var dataSource=[DataObject]()
            let json=jsonData[0]["category_info"].dictionary
            print(json as Any)
            for (key,value) in json!{
                
                let maincatename=key
                let maincateimage=value["icon"].stringValue
                let maincateId=value["id"].stringValue
                var hasChildd="false"
                if value["child"].dictionary != nil{
                    hasChildd="true"
                }
                
                
                
                
                
                var subDataSource=[DataObject]()
                if hasChildd=="true"{
                    let level2=value["child"].dictionary
                    for (key,value) in level2!{
                        let categoryName=key
                        let categoryImage=value["icon"].stringValue
                        let categoryId=value["id"].stringValue
                        var childFlag="false"
                        if value["child"].dictionary != nil{
                            childFlag="true"
                        }
                        
                        
                        
                        
                        var childDataSource=[DataObject]()
                        if childFlag=="true"{
                            let level3=value["child"].dictionary
                            for (key,value) in level3!{
                                let cateName=key
                                let cateImage=value["icon"].stringValue
                                let cateId=value["id"].stringValue
                                var childFlag="false"
                                if value["child"].dictionary != nil{
                                    childFlag="true"
                                }
                                
                                
                                
                                
                                
                                var cellDataSource=[DataObject]()
                                if childFlag=="true"{
                                    
                                    
                                    let level4=value["child"].dictionary
                                    for (key,value) in level4!{
                                        
                                        let cateName=key
                                        let cateImage=value["icon"].stringValue
                                        let cateId=value["id"].stringValue
                                        var childFlag="false"
                                        if value["child"].dictionary != nil{
                                            childFlag="true"
                                        }
                                        
                                        if childFlag=="true"{
                                            
                                        }
                                        else
                                        {
                                            let data=DataObject(name: cateName,id: cateId,image:cateImage)
                                            cellDataSource.append(data)
                                        }
                                        
                                        
                                    }
                                }
                                
                                
                                let data=DataObject(name: cateName, children: cellDataSource,id: cateId,image:cateImage)
                                childDataSource.append(data)
                                
                                
                            }
                        }
                        
                        let data=DataObject(name: categoryName, children: childDataSource,id: categoryId,image:categoryImage)
                        subDataSource.append(data)
                        
                    }
                }
                let data=DataObject(name: maincatename, children: subDataSource, id: maincateId,image:maincateimage)
                dataSource.append(data)
            }
            self.data=dataSource
            //table
            setupTreeView()
        }*/
        if let data = data {
            guard let jsonData = try? JSON(data:data) else{return;}
            //print(jsonData)
            var dataSource=[DataObject]()
            let json=jsonData[0]["category_info"].dictionary
            print(json as Any)
            for (key,value) in json!{
                
                let maincatename=value["name"].stringValue
                let maincateimage=value["icon"].stringValue
                let maincateId=value["id"].stringValue
                var hasChildd="false"
                if value["child"].dictionary != nil{
                    hasChildd="true"
                }
                
                var subDataSource=[DataObject]()
                if hasChildd=="true"{
                    subDataSource=addDataToDataSource(value: value, cateName: maincatename, cateId: maincateId, cateImage: maincateimage)
                    //dataSource=subDataSource
                    let data=DataObject(name: maincatename, children: subDataSource, id: maincateId,image:maincateimage)
                    dataSource.append(data)
                }
                else
                {
                    //let data=DataObject(name: maincatename, children: subDataSource, id: maincateId,image:maincateimage)
                    //dataSource.append(data)
                }
                
            }
            self.data=dataSource
            //table
            setupTreeView()
        }
    }
    
    
    func addDataToDataSource(value:JSON, cateName: String, cateId: String, cateImage: String)->[DataObject]{
        
            
        var cellDataSource=[DataObject]()
        let level4=value["child"].dictionary
        for (key,value) in level4!{
            
            let cateName=value["name"].stringValue//key
            let cateImage=value["icon"].stringValue
            let cateId=value["id"].stringValue
            var childFlag="false"
            if value["child"].dictionary != nil{
                childFlag="true"
            }
            
            if childFlag=="true"{
                
                let dataSource=addDataToDataSource(value: value, cateName: cateName, cateId: cateId, cateImage: cateImage)
                
                let data=DataObject(name: cateName, children: dataSource ,id: cateId,image:cateImage)
                cellDataSource.append(data)
            }
            else
            {
                let data=DataObject(name: cateName,id: cateId,image:cateImage)
                cellDataSource.append(data)
            }
            
        }
        return cellDataSource
        
    }
    
    func setupTreeView() -> Void {
        let label=UILabel(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 40))
        label.text="Shop By Categories"
        label.textAlignment = .center
        label.textColor=Settings.themeTextColor
//        label.fontColorTool()
        label.setThemeColor()
        self.view.addSubview(label)
        treeView = RATreeView(frame: CGRect(x: 0, y: 45, width: self.view.frame.width, height: self.view.frame.height-50))
        treeView.register(UINib(nibName: String(describing: TreeTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: TreeTableViewCell.self))
        treeView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        treeView.delegate = self;
        treeView.dataSource = self;
        treeView.treeFooterView = UIView()
        if #available(iOS 13.0, *) {
            treeView.backgroundColor = .systemBackground
        } else {
            treeView.backgroundColor = .clear
        }
        view.addSubview(treeView)
        //treeView.expandRow(forItem: data[0])
    }

    

    

    //MARK: RATreeView data source

    func treeView(_ treeView: RATreeView, numberOfChildrenOfItem item: Any?) -> Int {
        if let item = item as? DataObject {
            return item.children.count
        } else {
            return self.data.count
        }
    }

    var checker:DataObject?=nil
    func treeView(_ treeView: RATreeView, child index: Int, ofItem item: Any?) -> Any {
        print("^^^^^^")
        if let item = item as? DataObject {
            return item.children[index]
        } else {
            
            return data[index] as AnyObject
            
        }
    }

    func treeView(_ treeView: RATreeView, cellForItem item: Any?) -> UITableViewCell {
        print("CELLCREATED")
        let cell = treeView.dequeueReusableCell(withIdentifier: String(describing: TreeTableViewCell.self)) as! TreeTableViewCell
        let item = item as! DataObject

        print(item)
        let level = treeView.levelForCell(forItem: item)
        let detailsText = "Number of children \(item.children.count)"
        cell.selectionStyle = .none
        
        cell.setup(withTitle: item.name, detailsText: detailsText, level: level, additionalButtonHidden: false,img: item.image)
        
        //treeView.expandRow(forItem: <#T##Any?#>, expandChildren: <#T##Bool#>, with: <#T##RATreeViewRowAnimation#>)
        /*cell.additionButtonActionBlock = { [weak treeView] cell in
            guard let treeView = treeView else {
                return;
            }
            let item = treeView.item(for: cell) as! DataObject
            let newItem = DataObject(name: "Added value", )
            item.addChild(newItem)
            treeView.insertItems(at: IndexSet(integer: item.children.count-1), inParent: item, with: RATreeViewRowAnimationNone);
            treeView.reloadRows(forItems: [item], with: RATreeViewRowAnimationNone)
        }*/
        return cell
    }

    //MARK: RATreeView delegate

    func treeView(_ treeView: RATreeView, commit editingStyle: UITableViewCell.EditingStyle, forRowForItem item: Any) {
        
        print("CELL")
        
        guard editingStyle == .delete else { return; }
        let item = item as! DataObject
        let parent = treeView.parent(forItem: item) as? DataObject

        let index: Int
        if let parent = parent {
            index = parent.children.firstIndex(where: { dataObject in
                return dataObject === item
            })!
            parent.removeChild(item)

        } else {
            index = self.data.firstIndex(where: { dataObject in
                return dataObject === item;
            })!
            self.data.remove(at: index)
        }

        self.treeView.deleteItems(at: IndexSet(integer: index), inParent: parent, with: RATreeViewRowAnimationRight)
        if let parent = parent {
            self.treeView.reloadRows(forItems: [parent], with: RATreeViewRowAnimationNone)
        }
    }
    func treeView(_ treeView: RATreeView, didSelectRowForItem item: Any) {
       
       /* if let cell=treeView.cell(forItem: item) as? TreeTableViewCell{
            let image=cell.rightImage.image
            let checkImage=UIImage(named: "IQButtonBarArrowRight")
            
            
            
            if image==checkImage{
                cell.rightImage.image=UIImage(named: "IQButtonBarArrowDown")
            }
            else
            {
                cell.rightImage.image=UIImage(named: "IQButtonBarArrowRight")
                
            }
            if let data=item as? DataObject{
                if data.children.count == 0{
                    cell.rightImage.image=nil
                }
            }
            cell.rightImage.tintColor=UIColor.black
        }*/
        
        if let data=item as? DataObject{
            if data.children.count == 0{
                
                let selectedId=data.id
                if let viewController =  UIStoryboard(name: "categorylayouts", bundle: nil).instantiateViewController(withIdentifier: "cedMageDefaultCollection") as?  cedMageDefaultCollection {
                    viewController.selectedCategory = selectedId
                    self.navigationController?.pushViewController(viewController, animated: true)
                }
                
            }
        }
    }
}


private extension TreeViewController {

    static func commonInit() -> [DataObject] {
        /*let phone1 = DataObject(name: "Phone 1")
        let phone2 = DataObject(name: "Phone 2")
        let phone3 = DataObject(name: "Phone 3")
        let phone4 = DataObject(name: "Phone 4")
        let phones = DataObject(name: "Phones", children: [phone1, phone2, phone3, phone4])

        let notebook1 = DataObject(name: "Notebook 1")
        let notebook2 = DataObject(name: "Notebook 2")

        let computer1 = DataObject(name: "Computer 1", children: [notebook1, notebook2])
        let computer2 = DataObject(name: "Computer 2")
        let computer3 = DataObject(name: "Computer 3")
        let computers = DataObject(name: "Computers", children: [computer1, computer2, computer3])

        let cars = DataObject(name: "Cars")
        let bikes = DataObject(name: "Bikes")
        let houses = DataObject(name: "Houses")
        let flats = DataObject(name: "Flats")
        let motorbikes = DataObject(name: "motorbikes")
        let drinks = DataObject(name: "Drinks")
        let food = DataObject(name: "Food")
        let sweets = DataObject(name: "Sweets")
        let watches = DataObject(name: "Watches")
        let walls = DataObject(name: "Walls")

        return [phones, computers, cars, bikes, houses, flats, motorbikes, drinks, food, sweets, watches, walls]*/
        return []
    }

}

