/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */

import UIKit
class cedVendorReviewListingController: MagenativeUIViewController,UIScrollViewDelegate {
    
    var vendorShopInfo = [String:String]();
    var vendor_id = String();
    var productReviews = [String:[String:String]]();
    var currentPage = 1;
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.basicFoundationToRenderView(topMargin: 5)
        scrollView.delegate = self;
        let postString = ["page":String(self.currentPage),"vendor_id":vendor_id];
      //  self.sendRequest(url: "vreviewapi/index/getVendorReview", params: postString)
        self.sendRequest(url: "vreviewapi/index/getVendorReview", params: postString,buyer:false)
        
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func recieveResponse(data: Data?, requestUrl: String?, response: URLResponse?) {
        if let responseData = data {
            guard let jsonData = try? JSON(data:responseData) else{return;}
            print(jsonData);
            if(jsonData["data"]["success"].stringValue == "true") {
                for review in jsonData["data"]["productreview"].arrayValue {
                    var tempArray = [String:String]();
                    tempArray["review-by"] = review["review-by"].stringValue;
                    tempArray["posted_on"] = review["posted_on"].stringValue;
                    tempArray["review-title"] = review["review-title"].stringValue;
                    tempArray["review-description"] = review["review-description"].stringValue;
                    tempArray["review-id"] = review["review-id"].stringValue;
                    let key = review["review-id"].stringValue;
                    tempArray["v_review"] = review["v_review"].stringValue;
                    self.productReviews[key] = tempArray;
                    //self.productReviews.append(tempArray);
                }
                print(self.productReviews);
                
               
            }
             self.renderReviewListing();
        }
    }
    
    
    func renderReviewListing() {
        self.makeSomeSpaceInStackView(height: CGFloat(0.0));
        
     
        let vendorShopHeaderView = cedVendorShopHeaderView();
        vendorShopHeaderView.translatesAutoresizingMaskIntoConstraints = false;
        if let profilepic = vendorShopInfo["profile_picture"]{
            cedMageImageLoader().loadImgFromUrl(urlString: profilepic, completionHandler: {
                image,url in
                vendorShopHeaderView.imagView.image = image
            })
        }
        
        vendorShopHeaderView.shopTitle.text = vendorShopInfo["public_name"];
        vendorShopHeaderView.shopTitle.fontColorTool()
        vendorShopHeaderView.shopAddress.text = vendorShopInfo["company_address"];
        vendorShopHeaderView.shopAddress.fontColorTool()
        vendorShopHeaderView.shopRating.text = vendorShopInfo["vendor_review"]
        vendorShopHeaderView.shopRating.fontColorTool()
        vendorShopHeaderView.shopReviews.text = vendorShopInfo["shopReviews"];
        vendorShopHeaderView.shopReviews.fontColorTool()
        stackView.addArrangedSubview(vendorShopHeaderView);
        let vendorShopHeaderViewHeight = CGFloat(200);
        vendorShopHeaderView.heightAnchor.constraint(equalToConstant: vendorShopHeaderViewHeight).isActive = true;
        self.setLeadingAndTralingSpaceFormParentView(vendorShopHeaderView, parentView:stackView);
        
        if self.productReviews.count > 0 {
            for (_,val) in self.productReviews {
                let productRatingListView = cedVendorReviewList();
                productRatingListView.translatesAutoresizingMaskIntoConstraints = false;
                
                
                
                productRatingListView.reviewTitle.text = val["review-title"]!;
                productRatingListView.reviewTitle.fontColorTool()
//                let color = cedMage.getInfoPlist(fileName: "cedMage", indexString: "themeColor")
//                productRatingListView.reviewTitle.textColor = cedMage.UIColorFromRGB(colorCode: color as! String)
                
                
                productRatingListView.reviewPostedBy.text = val["review-by"]!;
                
                productRatingListView.ratedTextLabel.text = "RATED".localized;
                productRatingListView.ratingLabel.text = val["v_review"]!;
                productRatingListView.reviewPostedOn.text = val["posted_on"]!;
                productRatingListView.reviewDescription.text = val["review-description"]!;
                
                productRatingListView.reviewPostedBy.fontColorTool()
                
                productRatingListView.ratedTextLabel.fontColorTool()
                productRatingListView.ratingLabel.fontColorTool()
                productRatingListView.reviewPostedOn.fontColorTool()
                productRatingListView.reviewDescription.fontColorTool()
                
                productRatingListView.reviewDescription.sizeToFit()
                stackView.addArrangedSubview(productRatingListView);
                productRatingListView.layoutIfNeeded()
                
                
                self.setLeadingAndTralingSpaceFormParentView(productRatingListView, parentView:stackView);
                
            }
        }
        else
        {
            self.renderNoDataImage(view: self, imageName: "noReviews")
            
            self.makeSomeSpaceInStackView(height: CGFloat(0.0));
        }
        
        self.renderFloatingButton();
    }
    
    
    func renderFloatingButton(){
        let floatingButton = UIButton();
        floatingButton.translatesAutoresizingMaskIntoConstraints = false;
        floatingButton.setThemeColor();
        self.view.addSubview(floatingButton);
        floatingButton.setImage(UIImage(named: "AddFileWhite"), for: UIControl.State.normal);
        floatingButton.addTarget(self, action: #selector(cedVendorReviewListingController.addNewReview(_:)), for: UIControl.Event.touchUpInside);
        floatingButton.imageEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10);
        self.view.addConstraint(NSLayoutConstraint(item: floatingButton, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: translateAccordingToDevice(50)));
        self.view.addConstraint(NSLayoutConstraint(item: floatingButton, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: translateAccordingToDevice(50)));
        self.view.addConstraint(NSLayoutConstraint(item: floatingButton, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: -10));
        self.view.addConstraint(NSLayoutConstraint(item: floatingButton, attribute: NSLayoutConstraint.Attribute.bottom, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: -(translateAccordingToDevice(50)+10)));
        floatingButton.makeViewCircled(size: translateAccordingToDevice(50));
    }
    
    @objc func addNewReview(_ sender:UIButton) {
        let storyboard = UIStoryboard(name: "categorylayouts", bundle: nil);
        let viewController = storyboard.instantiateViewController(withIdentifier: "cedAddVendorReviewController") as! cedAddVendorReviewController;
        self.navigationController?.pushViewController(viewController, animated: true);
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
