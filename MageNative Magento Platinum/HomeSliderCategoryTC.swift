//
//  HomeSliderCategoryTC.swift
//  MageNative Magento Platinum
//
//  Created by Hamza Usmani on 02/09/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class HomeSliderCategoryTC: UITableViewCell {
    
    //MARK:- Properties
    
    static var reuseId:String = "HomeSliderCategoryTC"
    var categories: [HomeCategory]? = nil
    weak var parent: UIViewController?
    var categoryType: String?
    
    lazy var topHeading:UILabel = {
        let label = UILabel()
        label.textColor = .mageLabel
        label.text = "Top Categories"
        label.font = .systemFont(ofSize: 17, weight: .semibold)
        return label
    }()
    
    lazy var categorySlider:UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.backgroundColor = UIColor.clear
        collectionView.register(HomeSliderCategoryCC.self, forCellWithReuseIdentifier: HomeSliderCategoryCC.reuseId)
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.dataSource = self
        collectionView.delegate = self
        return collectionView
    }()
    
    //MARK:- Lifecycle
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle          = .none
        backgroundColor         = .clear
        contentView.isMultipleTouchEnabled  = true
        
        
        addSubview(topHeading)
        topHeading.anchor(top: topAnchor, left: leadingAnchor, right: trailingAnchor, paddingTop: 8, paddingLeft: 8, paddingRight: 8, height: 0)
        
        addSubview(categorySlider)
        categorySlider.anchor(top: topHeading.bottomAnchor, left: leadingAnchor, bottom: bottomAnchor, right: trailingAnchor, paddingTop: 2, paddingLeft: 8,
                              paddingBottom: 8 ,paddingRight: 8)
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func populate(with data: [HomeCategory], categoryType: String?, parent: UIViewController) {
        self.categories = data
        self.categoryType = categoryType
        self.parent = parent
        categorySlider.reloadData()
    }
}

//MARK:- UICollectionViewDelegate/UICollectionViewDelegateFlowLayout/UICollectionViewDataSource
extension HomeSliderCategoryTC: UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categories?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: HomeSliderCategoryCC.reuseId, for: indexPath) as! HomeSliderCategoryCC
        cell.populate(with: categories![indexPath.item], type: categoryType)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return .init(width: collectionView.frame.width / 4 - 15, height: collectionView.frame.height)//66
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = UIStoryboard(name: "categorylayouts", bundle: nil).instantiateViewController(withIdentifier: "cedMageDefaultCollection") as! cedMageDefaultCollection
        vc.selectedCategory = categories?[indexPath.item].id ?? ""
        parent?.navigationController?.pushViewController(vc, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
}
