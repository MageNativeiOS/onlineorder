/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */
import UIKit

class homedefaultNavigation: UINavigationController,UINavigationControllerDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.navigationBar.shadowImage = UIImage()
        
        // Do any additional setup after loading the view.
        //self.navigationItem.title = "MageNative"
        self.navigationController?.navigationBar.titleTextAttributes = [ NSAttributedString.Key.font: UIFont(name: "CaviarDreams", size: 20)!]
        
        
        let barsButton = UIBarButtonItem(image: UIImage(named:"hamp"), style: UIBarButtonItem.Style.plain, target: self, action: nil)
        barsButton.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = barsButton
        if let data = UserDefaults.standard.value(forKey: "NotificationData") as? [String:Any]
        {
            UserDefaults.standard.removeObject(forKey: "NotificationData")
            print(data)
            if let datalink_type = data["link_type"] as? String{
                if let link_id = data["link_id"] as? String{
                    if(datalink_type == "2"){
                        let viewcontoller = UIStoryboard(name: "categorylayouts", bundle: nil).instantiateViewController(withIdentifier: "cedMageDefaultCollection") as? cedMageDefaultCollection
                            viewcontoller?.selectedCategory = link_id
                        self.pushViewController(viewcontoller!, animated: true)
                    }else if (datalink_type == "1"){
                        let productview = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "rootPageViewController") as! cedMageProductViewRoot
                        productview.pageData = [["product_id":link_id]]
                        let instance = cedMage.singletonInstance
                        instance.storeParameterInteger(parameter: 0)
                        self.pushViewController(productview
                            , animated: true)
                        
                    }else if (datalink_type == "3"){
                        let view = UIStoryboard(name: "cedMageAccounts", bundle: nil)
                        let viewControl = view.instantiateViewController(withIdentifier: "cmsWebView") as! cedMageCmsWebView
                        let url = link_id
                        viewControl.pageUrl = url
                        self.pushViewController(viewControl, animated: true)
                    }
                }
                
            }
            
        }
        
        navigationBar.isTranslucent = false
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    @objc func backfunc(sender:UIButton){
      let mainview = self//.sideDrawerViewController?.mainViewController  as? homedefaultNavigation
        let result =  mainview.popViewController(animated: true)
      print(result as Any)
    }
    
    @objc func toggleDrawer() {
      if let sideDrawerViewController = self.sideDrawerViewController {
        sideDrawerViewController.toggleDrawer()
      }
    }
    
    
}
extension UIImage{
    class func imageFromColor(color: UIColor, frame: CGRect) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(frame.size, false, 0)
        color.setFill()
        UIRectFill(frame)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
}

