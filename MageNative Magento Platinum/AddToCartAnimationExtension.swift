/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */

import UIKit

extension ProductSinglePageViewController{
  
  func renderAddToCartAnimation(button:UIButton){
    
    //productImageView.productMainImage.shake();
    if  let snapshot = productImageView.productMainImage.snapshotView(afterScreenUpdates: false){
      snapshot.frame = productImageView.productMainImage.frame;
      self.view.addSubview(snapshot);
      
      _ = snapshot.bounds
      let smallFrame = snapshot.frame.insetBy(dx: (snapshot.frame.size.width) / 4, dy: (snapshot.frame.size.height) / 4)
      
      
      let vv = self.navigationItem.rightBarButtonItem;
      var finalFrame =  vv?.customView?.frame
      
      finalFrame?.origin.x = self.view.frame.width
      UIView.animateKeyframes(withDuration: 1, delay: 0, options: .calculationModeCubic, animations: {
        
        UIView.addKeyframe(withRelativeStartTime: 0.0, relativeDuration: 0.5) {
          snapshot.frame = smallFrame
          UIView.animate(withDuration: 0.5, delay: 0.0, options: [], animations: {
            snapshot.alpha = 0.7;
          }, completion: nil
          );
        }
        
        UIView.addKeyframe(withRelativeStartTime: 0.5, relativeDuration: 0.5) {
          snapshot.frame = finalFrame!
          UIView.animate(withDuration: 0.5, delay: 0.0, options: [], animations: {
            snapshot.alpha = 0.3;
          }, completion: nil
          );
        }
      }, completion: {completed in
        
        UIView.animate(withDuration: 0.5, delay: 0.0, options: [], animations: {
          snapshot.alpha = 0.0;
        }, completion: {completed in
          snapshot.removeFromSuperview();
          
          let cartCount = self.defaults.value(forKey: "items_count") as! String;
          print("cartcount")
          print(cartCount)
          cedMageViewController().setCartCount(view:self.parentView!,items: cartCount)
          
          if button.tag == 450{
            let cartid=UserDefaults.standard.value(forKey: "cartId") as! String
            //self.sendRequest(url: "mobiconnect/checkout/viewcart",params: "cart_id="+cartid);
            let urlToRequest=Settings.baseUrl+"mobiconnect/checkout/viewcart"
            let requestHeader = Settings.headerKey//cedMage.getInfoPlist(fileName:"cedMage",indexString: "requestheader") as! String
            let postString = "cart_id="+cartid;
            var request = URLRequest(url: URL(string: "\(urlToRequest)")!);
            request.httpMethod = "POST";
            request.httpBody = postString.data(using: String.Encoding.utf8);
            if UserDefaults.standard.bool(forKey: "isLogin"){
                if let user = UserDefaults.standard.object(forKey: "userInfoDict") as? [String:String] {
                    request.setValue(user["hashKey"], forHTTPHeaderField: "hashkey")
                }
                
            }
            
            print("NSURLSession");
            
            URLSession.shared.dataTask(with: request){
              
              // check for fundamental networking error
              data, response, error in
              guard error == nil && data != nil else{
                print("error=\(String(describing: error))")
                //DispatchQueue.main.async{
                print(error?.localizedDescription as Any);
                cedMageLoaders.removeLoadingIndicator(me: self);
                cedMageHttpException.showAlertView(me: self, msg: error?.localizedDescription, title: "Error".localized )
                //}
                return;
              }
              
              // check for http errors
              if let httpStatus = response as? HTTPURLResponse , httpStatus.statusCode != 200{
                
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(String(describing: response))")
                //DispatchQueue.main.async{
                cedMageLoaders.removeLoadingIndicator(me: self);
                cedMageHttpException.showHttpErrorImage(me: self, img: "no_module")
                
                //}
                return;
              }
              
              // code to fetch values from response :: start
              
              let datastring = NSString(data: data!, encoding: String.Encoding.utf8.rawValue);
              //NSString(data: NSData!, encoding: String.Encoding.utf8)
              print("datastring");
              print(datastring as Any);
              
              guard let jsonResponse = try? JSON(data: data!)else{
                return;
              }
              if(jsonResponse != nil){
                
                
                
                //print(jsonResponse);
                cedMageLoaders.removeLoadingIndicator(me: self);
                //let jsonResponse = JSON(data: data!);
                print(jsonResponse);
                var flag=false
                var isVirtual=false
                print(jsonResponse)
                for cartProInfo in jsonResponse[0]["data"]["products"].arrayValue{
                  var tempData = [String:String]();
                  
                  tempData["product_type"] = cartProInfo["product_type"].stringValue;
                  if(tempData["product_type"] != "downloadable")
                  {
                    flag=true
                  }
                  if(tempData["product_type"] != "virtual")
                  {
                    isVirtual=true
                  }
                  tempData["quantity"] = cartProInfo["quantity"].stringValue;
                  tempData["product-name"] = cartProInfo["product-name"].stringValue;
                  tempData["sub-total"] = cartProInfo["sub-total"].stringValue;
                  tempData["product_image"] = cartProInfo["product_image"].stringValue;
                  tempData["product_id"] = cartProInfo["product_id"].stringValue;
                  tempData["item_id"] = cartProInfo["item_id"].stringValue;
                }
                
                if flag {
                  UserDefaults.standard.set(true, forKey: "flag_downloadable")
                  
                }
                
                if isVirtual{
                  UserDefaults.standard.set(true, forKey: "flag_virtual")
                }
                
              }
              
            }
            //task.resume();
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            if self.defaults.bool(forKey: "isLogin") == true {
              if(cedMage.checkModule(string:"MageNative_Mobicheckout")){
                let storyboard = UIStoryboard(name: "cedMageAccounts", bundle: nil);
                let viewController = storyboard.instantiateViewController(withIdentifier: "cedMageAdvancedCheckout") as! cedMageAdvancedCheckout;
                self.navigationController?.pushViewController(viewController, animated: true);
                //                                let storyboard = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil);
                //                                let viewController = storyboard.instantiateViewController(withIdentifier: "checkoutStep2ViewController") as! CheckoutStep2ViewController;
                //                                self.navigationController?.pushViewController(viewController, animated: true);
              }
              else{
                let storyboard = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil);
                let viewController = storyboard.instantiateViewController(withIdentifier: "checkoutStep2ViewController") as! CheckoutStep2ViewController;
                self.navigationController?.pushViewController(viewController, animated: true);
              }
            }else{
              let storyboard = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil);
              let viewController = storyboard.instantiateViewController(withIdentifier: "cartViewController") as! CartViewController;
              self.navigationController?.pushViewController(viewController, animated: true)
            }
          }
        }
        );
      }
      );
    }
  }
  
  
  
  func performAddToCartAnimation(){
    
    let snapshot = productImageView.productMainImage.snapshotView(afterScreenUpdates: false)
    snapshot?.frame = productImageView.productMainImage.frame
    productImageView.productMainImage.addSubview(snapshot!)
    
    self.setRotation(viewToRotate:snapshot!);
    
    let bounds = snapshot?.bounds
    let smallFrame = snapshot?.frame.insetBy(dx: (snapshot?.frame.size.width)! / 4, dy: (snapshot?.frame.size.height)! / 4)
    let finalFrame = smallFrame?.offsetBy(dx: 0, dy: (bounds?.size.height)!)
    
    UIView.animateKeyframes(withDuration: 4, delay: 0, options: .calculationModeCubic, animations: {
      
      UIView.addKeyframe(withRelativeStartTime: 0.0, relativeDuration: 0.5) {
        snapshot?.frame = smallFrame!
        self.setRotation(viewToRotate:snapshot!);
      }
      
      UIView.addKeyframe(withRelativeStartTime: 0.5, relativeDuration: 0.5) {
        snapshot?.frame = finalFrame!
        self.setRotation(viewToRotate:snapshot!);
      }
    }, completion: nil
    );
    
  }
  
  func setRotation(viewToRotate:UIView){
    
    // angles in iOS are measured as radians PI is 180 degrees so PI × 2 is 360 degrees
    let fullRotation = CGFloat(Double.pi * 2)
    
    let duration = 2.0
    let delay = 0.0
    let options = UIView.KeyframeAnimationOptions.calculationModeLinear
    
    UIView.animateKeyframes(withDuration: duration, delay: delay, options: options, animations: {
      // each keyframe needs to be added here
      // within each keyframe the relativeStartTime and relativeDuration need to be values between 0.0 and 1.0
      
      UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 1/3, animations: {
        // start at 0.00s (5s × 0)
        // duration 1.67s (5s × 1/3)
        // end at   1.67s (0.00s + 1.67s)
        viewToRotate.transform = CGAffineTransform(rotationAngle: 1/3 * fullRotation)
      })
      UIView.addKeyframe(withRelativeStartTime: 1/3, relativeDuration: 1/3, animations: {
        viewToRotate.transform = CGAffineTransform(rotationAngle: 2/3 * fullRotation)
      })
      UIView.addKeyframe(withRelativeStartTime: 2/3, relativeDuration: 1/3, animations: {
        viewToRotate.transform = CGAffineTransform(rotationAngle: 3/3 * fullRotation)
      })
      
    }, completion: {finished in
      // any code entered here will be applied
      // once the animation has completed
      
    })
    
  }
  
  
}
