/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */

import UIKit

class BundleProductCheckboxView: UIView {

    // Our custom view from the XIB file
    var view: UIView!
    
    //outlets
    @IBOutlet weak var bundleProCheckboxButton: UIButton!
    @IBOutlet weak var bundleProCheckboxButtonImg: UIImageView!
    @IBOutlet weak var qtyLabel: UILabel!
    @IBOutlet weak var qtyToBuy: UITextField!
    
    
    override init(frame: CGRect) {
        // 1. setup any properties here
        
        // 2. call super.init(frame:)
        super.init(frame: frame)
        
        // 3. Setup view from .xib file
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        // 1. setup any properties here
        
        // 2. call super.init(coder:)
        super.init(coder: aDecoder)
        
        // 3. Setup view from .xib file
        xibSetup()
    }
    
    func xibSetup() {
        view = loadViewFromNib()
        
        
        bundleProCheckboxButton.fontColorTool()
        bundleProCheckboxButtonImg.fontColorTool()
        qtyLabel.fontColorTool()
        qtyToBuy.fontColorTool()
        
        // use bounds not frame or it'll be offset
        view.frame = bounds
        
        // Make the view stretch with containing view
        view.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        
        
        //extra setup
        bundleProCheckboxButton.titleLabel!.font = UIFont(fontName: "", fontSize: CGFloat(15.0));
        
        qtyLabel.font = UIFont(fontName: "", fontSize: CGFloat(15.0));
        qtyToBuy.font = UIFont(fontName: "", fontSize: CGFloat(15.0));
//        self.makeCard(self, cornerRadius: 2, color: UIColor.black, shadowOpacity: 0.4);
        //extra setup
        
        
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "BundleProductCheckboxView", bundle: bundle)
        
        // Assumes UIView is top level and only object in CustomView.xib file
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    

}
