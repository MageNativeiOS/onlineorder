/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */


import UIKit

class bannerCell: UIView,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UIScrollViewDelegate,KIImagePagerDataSource,KIImagePagerDelegate {

    @IBOutlet weak var constantSpace: NSLayoutConstraint!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var collectionView: UICollectionView!
    var view : UIView?
    @IBOutlet weak var bannerView: KIImagePager!
    var dataSource = [String]()
    var fullData = [[String:String]]()
    var parent:UIViewController?
    
    override init(frame: CGRect)
    {
        // 1. setup any properties here
        
        // 2. call super.init(frame:)
        super.init(frame: frame)
        
        // 3. Setup view from .xib file
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        // 1. setup any properties here
        
        // 2. call super.init(coder:)
        super.init(coder: aDecoder)
        
        // 3. Setup view from .xib file
        xibSetup()
    }
    
    func xibSetup()
    {
        view = loadViewFromNib()
        
        // use bounds not frame or it'll be offset
        view?.frame = bounds
        
        // Make the view stretch with containing view
        view?.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
   
      collectionView.delegate = self
        collectionView.dataSource = self
        bannerView.delegate = self
        bannerView.dataSource = self
     
        bannerView.cardView()
        pageControl.currentPageIndicatorTintColor = cedMage.UIColorFromRGB(colorCode: (cedMage.getInfoPlist(fileName: "cedMage", indexString: "themeColor")) as! String)
          collectionView.register(UINib(nibName: "headerCell", bundle: nil), forCellWithReuseIdentifier: "headercell")
        
        if(pageControl.isHidden){
            constantSpace.constant = 0
        }
        collectionView.cardView()
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(view!)
    }
    
    func loadViewFromNib() -> UIView
    {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "bannerView", bundle: bundle)
        
        // Assumes UIView is top level and only object in CustomView.xib file
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }

    //MARK: Delegate for KIImage Pager
    
    public func array(withImages pager: KIImagePager!) -> [Any]! {
        if dataSource.count == 0 {
            return ["placeholder" as AnyObject]
        }
        return dataSource as [AnyObject]?
    }
    
    func contentMode(forPlaceHolder pager: KIImagePager!) -> UIView.ContentMode {
        return UIView.ContentMode.scaleToFill
    }
    
    func contentMode(forImage image: UInt, in pager: KIImagePager!) -> UIView.ContentMode {
        return UIView.ContentMode.scaleToFill
    }
    
    func placeHolderImage(for pager: KIImagePager!) -> UIImage! {
        return UIImage(named: "bannerplaceholder")
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func imagePager(_ imagePager: KIImagePager!, didSelectImageAt index: UInt) {
         let int = Int(index)
        if fullData.count > 0 {
        let data = fullData[int]
        if data["link_to"] == "category"{
            if let contol = UIStoryboard(name: "categorylayouts", bundle: nil).instantiateViewController(withIdentifier: "cedMageDefaultCollection") as?  cedMageDefaultCollection{//instantiateViewController(withIdentifier: "cedMageNewCate") as! cedMageSubcategoryListing
            contol.selectedCategory = data["product_id"]!
            parent?.navigationController?.pushViewController(contol, animated: true)
            }
        }else if data["link_to"] == "product"{
            
        }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        pageControl.numberOfPages = dataSource.count
        return dataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "headercell", for: indexPath) as! headerCollect
        
        cedMageImageLoader().loadImgFromUrl(urlString: dataSource[indexPath.row], completionHandler: {
            image,url in
            cell.imageView.image = image
        })
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height)
    }
  
   
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        pageControl.currentPage = Int(self.collectionView.contentOffset.x) / Int(self.collectionView.frame.size.width);
    }
    
    


}
