//
//  cedMageDefaultCollection.swift
//  MageNative Magento Platinum
//
//  Created by CEDCOSS Technologies Private Limited on 18/01/17.
//  Copyright © 2017 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class cedMageDefaultCollection: MagenativeUIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UIScrollViewDelegate, UIGestureRecognizerDelegate {
    
    @IBOutlet weak var collectionView: UICollectionView!
    var selectedCategory = String()
    var subCateGories = [[String:JSON]]()
    var products  = [[String:String]]()
    var sortByArray = [String:String]();
    var currentView = "grid"
    var searchString = ""; // variable to handle search case
    var jsonResponse:JSON?
    var loadMoreData = true
    var currentpage = 1
    //var curr_page=1
    var flag=false
    var imagView = UIImageView()
    var no_pro_check=false
    var homePage = Bool()
    var eanCode = String()
    var subCatflag=false
    var sortValuePicked = "";
    var val = ""
    
    private var cache = NSCache<AnyObject, AnyObject>()
    override func viewDidLoad() {
        //self.basicFoundationToRenderView(topMargin: 5)
        //NotificationCenter.default.addObserver(self, selector: #selector(cedMageDefaultCollection.reloadData(_:)), name: NSNotification.Name(rawValue: "loadProductsAgainId"), object: nil);
        
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        loadtopView()
        
        var param = [String:String]()
        
        if let filterType = UserDefaults.standard.value(forKey: "filterType") as? String {
            if filterType == "zipcode" {
                param["postcode"] = Settings.currentLocation?["zipcode"] ?? ""
            } else if filterType == "city_state_country" {
                param["city"] = Settings.currentLocation?["city"] ?? ""
                param["state"] = Settings.currentLocation?["state"] ?? ""
                param["country_id"] = Settings.currentLocation?["country"] ?? ""
            } else {
                param["latitude"] = Settings.currentLocation?["latitude"] ?? ""
                param["longitude"] = Settings.currentLocation?["longitude"] ?? ""
            }
        }
        
        if(searchString != ""){
            param["q"] = searchString
            param["page"] = "\(currentpage)"
            self.makeRequestToAPI("mobiconnect/catalog/search",dataToPost: param);
        }
        else if(eanCode != "") {
            param["ean"] = eanCode
            param["page"] = "\(currentpage)"
            self.makeRequestToAPI("mobiconnect/catalog/search",dataToPost: param);
        }else if homePage{
            print("featured/category/products/")
            param["page"] = "\(currentpage)"
            self.makeRequestToAPI("featured/category/products/",dataToPost: param);
        }
        else{
            param["id"] = selectedCategory
            param["page"] = "\(currentpage)"
            param["theme"] = "1"
            self.makeRequestToAPI("mobiconnect/catalog/products/",dataToPost: param);
        }
    }
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if flag {
            flag = false;
            if(self.loadMoreData){
                currentpage += 1
                if(sortValuePicked != "")
                {
                    if(searchString != ""){
                        self.makeRequestToAPI("mobiconnect/catalog/search/",dataToPost: ["q":searchString, "page":String(describing: currentpage), "order":(val.components(separatedBy: "#").first!), "dir":sortValuePicked,"theme":"1"]);
                    }
                    else{
                        
                        self.makeRequestToAPI("mobiconnect/catalog/products/",dataToPost: ["id":selectedCategory, "page":String(describing: currentpage), "order":(val.components(separatedBy: "#").first!), "dir":sortValuePicked,"theme":"1"]);
                    }
                }
                else{
                    if(searchString != ""){
                        self.makeRequestToAPI("mobiconnect/catalog/search/",dataToPost: ["q":searchString, "page":String(describing: currentpage), "theme":"1"]);
                    }
                    else{
                        
                        self.makeRequestToAPI("mobiconnect/catalog/products/",dataToPost: ["id":selectedCategory, "page":String(describing: currentpage), "theme":"1"]);
                    }
                }
                
                //self.makeRequestToAPI("mobiconnect/catalog/products/",dataToPost: ["id":selectedCategory, "page":String(currentpage),"theme":"1"]);
            }
        }
    }
    func loadtopView(){
        let topView = cedMageSubCateTopSect()
        topView.switchView.addTarget(self, action: #selector(cedMageDefaultCollection.switchViews(sender:)), for: .touchUpInside)
        topView.sortbyButton.addTarget(self, action: #selector(cedMageDefaultCollection.sortbyButtonPressed(sender:)), for: .touchUpInside)
        topView.filterButton.addTarget(self, action: #selector(cedMageDefaultCollection.filterButtonPressed(_:)), for: .touchUpInside)
        topView.heightAnchor.constraint(equalToConstant: 50).isActive = true;
        topView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 50)
        self.view.addSubview(topView)
        UIView.animate(withDuration: 4, delay: 1, options: [UIView.AnimationOptions.transitionFlipFromRight,  UIView.AnimationOptions.showHideTransitionViews], animations: {
            if(self.currentView == "grid"){
                topView.switchImage.rotate360Degrees()
                topView.switchImage.image = UIImage(named: "grid")
            }else{
                topView.switchImage.rotate360Degrees()
                topView.switchImage.image = UIImage(named: "list")
            }
        }, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func reloadData(){
        clearViewAndVariables()
        print(searchString)
        if(searchString != ""){
            self.makeRequestToAPI("mobiconnect/catalog/search",dataToPost: ["q":searchString, "page":String(currentpage)]);
        }
        else{
            self.makeRequestToAPI("mobiconnect/catalog/products/",dataToPost: ["id":selectedCategory, "page":String(currentpage)]);
        }
    }
    
    func clearViewAndVariables(){
        self.showHttpErorImage(me: self, img: "Empty")
        products = [[String:String]]();
        sortByArray = [String:String]();
        subCateGories = [[String:JSON]]();
        sortValuePicked = ""
        val = ""
        currentpage = 1;
        loadMoreData = true;
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section==0
        {
            return subCateGories.count
        }
        return products.count
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if indexPath.section==0{
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "subcategoriesCell", for: indexPath) as! defaultGridCell
            let gradient: CAGradientLayer = CAGradientLayer()
            gradient.frame = cell.subCategoryView.frame
            gradient.colors = [UIColor(hexString:"#2cb200")?.cgColor ?? "", UIColor(hexString: "#7FB200")?.cgColor ?? ""]
            gradient.locations = [0.0, 0.5, 1.0]
            gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
            gradient.endPoint = CGPoint(x: 1.0, y: 0.5)
            gradient.cornerRadius=5
            //cell.subCategoryView.layer.insertSublayer(gradient, at: 0)
            
            cell.subCategoryLabel.numberOfLines=0
            
            cell.subCategoryLabel.backgroundColor=UIColor.clear
            cell.subCategoryLabel.text=subCateGories[indexPath.row]["category_name"]?.stringValue
            cell.subCategoryLabel.textColor=UIColor.white
            cell.subCategoryLabel.numberOfLines=0
            
            
            return cell
        }
        
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionviewdefault", for: indexPath) as? defaultGridCell {
            let product = products[indexPath.row]
            
            var productName = NSMutableAttributedString()
            productName = NSMutableAttributedString(string: product["product_name"]!)
            
            if(product["starting_from"] != ""){
                productName.append(NSAttributedString(string:"\n"+"Starting At : ".localized+product["starting_from"]!+"\n"));
                if(product["from_price"] != ""){
                    productName.append(NSAttributedString(string: "As Low As : ".localized+product["from_price"]!));
                }
            }
            else{
                
                if(product["special_price"] != "no_special"){
                    let attr = [NSAttributedString.Key.strikethroughStyle:1]
                    if let regPrice = product["regular_price"]{
                        let attribute = NSAttributedString(string:  "\n"+regPrice, attributes: attr)
                        productName.append(attribute)
                    }
                    let attr1 = [NSAttributedString.Key.foregroundColor:UIColor.red]
                    let attribute1 = NSAttributedString(string:  product["special_price"]!, attributes: attr1)
                    productName.append(NSAttributedString(string:"\n"));
                    productName.append(attribute1)
                    
                }else{
                    productName.append(NSAttributedString(string: "\n"+product["regular_price"]!));
                }
            }
            if product["review"]  != "" {
                if product["review"] != "0" {
                    cell.rating.text = product["review"]
                    cell.rating.fontColorTool()
                    cell.starLabel.sd_setImage(with: nil, placeholderImage: UIImage(named: "StarFilled"))
                    cell.ratingView.isHidden = false
                }else{
                    cell.ratingView.isHidden = true
                }
            }else{
                cell.ratingView.isHidden = true
            }
            cell.offerText.fontColorTool()
            if product["offerText"] != "" {
                cell.offerView.isHidden = false
                cell.offerText.text = product["offerText"]! + "% OFF".localized
            }else{
                cell.offerView.isHidden = true
            }
            cell.productTitle.attributedText = productName
            cell.productTitle.fontColorTool()
            
            if product["stock_status"] == "false"{
                cell.stockView.isHidden = false
            }else{
                cell.stockView.isHidden = true
            }
            
            
            cell.wishlistButton.fontColorTool()
            if(product["Inwishlist"] != "OUT"){
                cell.wishlistButton.setImage(UIImage(named:"LikeFilled"), for: UIControl.State.normal);
                cell.wishlistButton.isHidden = false
                cell.wishlistButton.tintColor = UIColor.red
                
            }else{
                cell.wishlistButton.isHidden = true
                cell.wishlistButton.tintColor = UIColor.red
                cell.wishlistButton.setImage(UIImage(named:"LikeEmpty"), for: UIControl.State.normal);
            }
            
            if  let urlToRequest = product["product_image"]{
                if(urlToRequest != "false"){
                    cell.productImage.sd_setImage(with: URL(string: urlToRequest), placeholderImage: UIImage(named: "placeholder"))
                }
            }else{
                cell.productImage.image = UIImage(named: "placeholder")
            }
            
            cell.cardView()
            if (indexPath.row) == (products.count/2)
            {
                flag=true
            }
            return cell
        }else{
            return UICollectionViewCell()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.section==0{
            
            let data=subCateGories[indexPath.row]["category_id"]?.stringValue
            if let viewController =  UIStoryboard(name: "categorylayouts", bundle: nil).instantiateViewController(withIdentifier: "cedMageDefaultCollection") as?  cedMageDefaultCollection {
                viewController.selectedCategory = data!
                self.navigationController?.pushViewController(viewController, animated: true)
                return
            }
        }
        
        _ = products[indexPath.row];
        
        let productview = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "rootPageViewController") as! cedMageProductViewRoot;
        productview.pageData = products as NSArray;
        let instance = cedMage.singletonInstance;
        instance.storeParameterInteger(parameter: indexPath.row);
        self.navigationController?.pushViewController(productview
            , animated: true);
    }
    
    
    @objc func switchViews(sender:UIButton){
        if let imageView = self.view.viewWithTag(258) as? UIImageView {
            if(self.currentView == "grid"){
                
                self.currentView = "list"
                imageView.image = UIImage(named:"list")
            }else{
                self.currentView = "grid"
                imageView.image = UIImage(named:"grid")
            }
            self.collectionView.reloadData()
        }
        
    }
    
    //Mark: Filter Functions
    
    @objc func filterButtonPressed(_ sender:UIButton){
        print("*****!@!@!@")
        
        if(self.searchString != ""){
            let msg = "No Filters Found".localized;
            self.view.makeToast(msg, duration: 2.0, position: .bottom, title: nil, image: nil, style: nil, completion: nil);
            return;
        }
        print("*****$%$$$%%$%$%")
        let storyboard = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil);
        let viewController = storyboard.instantiateViewController(withIdentifier: "productFiltersViewController") as! ProductFiltersViewController;
        viewController.jsonForFilters = self.jsonResponse;
        viewController.defColl = self;
        self.navigationController?.pushViewController(viewController, animated: true);
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.section==0{
            return CGSize(width: collectionView.frame.width/3-10, height: 50)
            
        }
        if self.currentView == "grid" {
            let height = CGFloat(300)
            let width = UIWindow().frame.size.width/2 - 10
            return CGSize(width:width, height:height);
        }else{
            let height = CGFloat(300)
            let width = UIWindow().frame.size.width - 10
            return CGSize(width:width, height:height);
        }
    }
    
    
    //Mark: parseData
    
    func parseData(json: JSON, index: String){
        let json = json[0]
        
        if(json.stringValue.lowercased() == "NO_PRODUCTS".lowercased()){
            //no_pro_check=true
            if(self.products.count == 0){
                self.renderNoDataImage(view:self,imageName:"noProduct");
                return;
            }
            else{
                self.loadMoreData = false;
            }
            return
        }
        self.jsonResponse = json
        for (key,result) in json["data"]["products"] {
            print(key)
            print(result)
            let product_id = result["product_id"].stringValue;
            let regular_price = result["regular_price"].stringValue;
            let special_price = result["special_price"].stringValue;
            let product_name = result["product_name"].stringValue;
            let product_image = result["product_image"].stringValue;
            let type = result["type"].stringValue;
            let review = result["review"].stringValue;
            let show_both_price = result["show-both-price"].stringValue;
            let Inwishlist = result["Inwishlist"].stringValue
            let starting_from = result["starting_from"].stringValue;
            let from_price = result["from_price"].stringValue;
            let offerText  = result["offer"].stringValue
                 let stock_status  = result["stock_status"].stringValue
            var wishlistItemId = "-1";
            if(result["wishlist-item-id"] != nil){
                wishlistItemId = result["wishlist-item-id"].stringValue;
            }
            if(self.products.count > 0 && searchString != "")
            {
                if(searchString == product_name)
                {
                    self.products.insert(["product_id":product_id,"regular_price":regular_price, "special_price":special_price,"product_name":product_name,"product_image":product_image,"type":type,"review":review,"show-both-price":show_both_price,"starting_from":starting_from,"from_price":from_price,"Inwishlist":Inwishlist,"wishlist-item-id":wishlistItemId,"offerText":offerText,"stock_status":stock_status], at: 0)
                }
                else{
                    self.products.append(["product_id":product_id,"regular_price":regular_price, "special_price":special_price,"product_name":product_name,"product_image":product_image,"type":type,"review":review,"show-both-price":show_both_price,"starting_from":starting_from,"from_price":from_price,"Inwishlist":Inwishlist,"wishlist-item-id":wishlistItemId,"offerText":offerText,"stock_status":stock_status]);
                }
            }
            else{
                self.products.append(["product_id":product_id,"regular_price":regular_price, "special_price":special_price,"product_name":product_name,"product_image":product_image,"type":type,"review":review,"show-both-price":show_both_price,"starting_from":starting_from,"from_price":from_price,"Inwishlist":Inwishlist,"wishlist-item-id":wishlistItemId,"offerText":offerText,"stock_status":stock_status]);
            }
            
        }
        
        if !subCatflag{
            subCatflag=true
            if let sub_category=json["data"]["sub_category"].array{
                for node in sub_category {
                    if let subCateData=node.dictionary{
                        subCateGories.append(subCateData)
                    }
                }
            }
        }
        
        
        
        //        if(subCateGories.count == 0){
        //            for data in json["data"]["sub_category"][0].arrayValue{
        //                let sucateName = data["category_name"].stringValue
        //                let subcateId = data["category_id"].stringValue
        //                let has_child = data["has_child"].stringValue
        //                let subcateImage = data["category_image"].stringValue
        //                subCateGories.append(["subCateName":sucateName,"subcateId":subcateId,"has_child":has_child,"subcatImg":subcateImage])
        //
        //            }
        //        }
        for (key,val) in json["data"]["sort"]{
            self.sortByArray[key] = val.stringValue;
            /*for (keyInr,valInr) in val{
                self.sortByArray[keyInr] = valInr[0].stringValue;
            }*/
        }
        if self.products.count > 0 {
            self.collectionView.reloadData()
        }
        //  stackView.subviews.forEach({ $0.removeFromSuperview() });
        
    }
    
    
    
    override func makeRequestToAPI(_ urlToRequest:String, dataToPost:[String:String]?){
        if let sortBtn = self.view.viewWithTag(1000) as? UIButton
        {
            sortBtn.isEnabled = false
        }
        if urlToRequest=="mobiconnect/catalog/products/"
        {
            if currentpage==1
            {
                cedMageLoaders.addDefaultLoader_withlockedbackground(me: self)
                products = [[String:String]]()
            }
        }
        else
        {
            cedMageLoaders.addDefaultLoader_withlockedbackground(me: self)
        }
        let urlToRequest = urlToRequest
        if(defaults.object(forKey: "userInfoDict") != nil){
            userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
        }
        
        let requestHeader = Settings.headerKey//cedMage.getInfoPlist(fileName:"cedMage",indexString: "requestheader") as! String
        let baseURL = Settings.baseUrl//cedMage.getInfoPlist(fileName:"cedMage",indexString: "cedBaseUrl") as! String;
        let urlRequest = baseURL+urlToRequest;
        
        var postString = "";
        var postData = [String:String]()
        if defaults.bool(forKey: "isLogin") {
            postData["hashkey"] = userInfoDict["hashKey"]
            postData["customer_id"] = userInfoDict["customerId"]
        }
        if let data = dataToPost{
            for (key,val) in data{
                postData[key] = val;
            }
        }
        if(defaults.object(forKey: "filtersToSend") != nil){
            let filtersToSend = defaults.object(forKey: "filtersToSend") as! String;
            print(filtersToSend)
            postData["multi_filter"] = filtersToSend;
        }
        let storeId = defaults.value(forKey: "storeId")
        if storeId != nil {
            postData["store_id"] = (storeId as! String)
        }
        postString = ["parameters":postData].convtToJson() as String
        print(urlRequest)
        print(postString)
        var request = URLRequest(url: URL(string: "\(urlRequest)")!);
        request.httpMethod = "POST";
        request.httpBody = postString.data(using: String.Encoding.utf8);
        if UserDefaults.standard.bool(forKey: "isLogin"){
            if let user = UserDefaults.standard.object(forKey: "userInfoDict") as? [String:String] {
                request.setValue(user["hashKey"], forHTTPHeaderField: "hashkey")
            }
            
        }
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        collectionView.isHidden = false
        let task = URLSession.shared.dataTask(with: request){
            data, response, error in
            guard error == nil && data != nil else{
                print("error=\(String(describing: error))")
                DispatchQueue.main.async{
                    print(error?.localizedDescription as Any);
                    if urlToRequest=="mobiconnect/catalog/products/"
                    {
                        print(self.currentpage)
                        if self.currentpage==1
                        {
                            cedMageLoaders.removeLoadingIndicator(me: self);
                        }
                        else
                        {
                            cedMageLoaders.removeLoadingIndicator(me: self);
                        }
                    }
                    else
                    {
                        cedMageLoaders.removeLoadingIndicator(me: self);
                    }
                    if let sortBtn = self.view.viewWithTag(1000) as? UIButton
                    {
                        sortBtn.isEnabled = true
                    }
                    if let filterBtn = self.view.viewWithTag(1001) as? UIButton
                    {
                        filterBtn.isEnabled = true
                    }
                    self.makeRequestToAPI(urlToRequest, dataToPost:dataToPost);
                }
                return;
            }
            
            // check for http errors
            if let httpStatus = response as? HTTPURLResponse , httpStatus.statusCode != 200{
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(response)")
                DispatchQueue.main.async{
                    if urlToRequest=="mobiconnect/catalog/products/"
                    {
                         print(self.currentpage)
                        if self.currentpage==1
                        {
                            cedMageLoaders.removeLoadingIndicator(me: self);
                        }
                        else
                        {
                            cedMageLoaders.removeLoadingIndicator(me: self);
                        }
                    }
                    else
                    {
                        cedMageLoaders.removeLoadingIndicator(me: self);
                    }
                    if(self.products.count == 0){
                        self.showHttpErorImage(me: self, img: "no_module")
                        self.collectionView.isHidden = true
                        
                    }
                    if let sortBtn = self.view.viewWithTag(1000) as? UIButton
                    {
                        sortBtn.isEnabled = true
                    }
                    if let filterBtn = self.view.viewWithTag(1001) as? UIButton
                    {
                        filterBtn.isEnabled = true
                    }
                    
                }
                return;
            }
            
            // code to fetch values from response :: start
            
            let datastring = NSString(data: data!, encoding: String.Encoding.utf8.rawValue);
            print(datastring as Any);
            guard let jsonNostring = try? JSON(data: data!) else{return;}
            if(jsonNostring[0].stringValue == "NO_PRODUCTS"){
                DispatchQueue.main.async{
                    if urlToRequest=="mobiconnect/catalog/products/"
                    {
                         print(self.currentpage)
                        if self.currentpage==1
                        {
                            cedMageLoaders.removeLoadingIndicator(me: self);
                        }
                        else
                        {
                            cedMageLoaders.removeLoadingIndicator(me: self);
                        }
                    }
                    else
                    {
                        cedMageLoaders.removeLoadingIndicator(me: self);
                    }
                    //cedMageLoaders.removeLoadingIndicator(me: self);
                    if(self.products.count == 0){
                        self.showHttpErorImage(me: self, img: "no_product")
                        self.collectionView.isHidden = true
                        return;
                    }
                    else{
                        self.loadMoreData = false;
                    }
                    if let sortBtn = self.view.viewWithTag(1000) as? UIButton
                    {
                        sortBtn.isEnabled = true
                    }
                    if let filterBtn = self.view.viewWithTag(1001) as? UIButton
                    {
                        filterBtn.isEnabled = true
                    }
                }
                return;
            }
            
            guard let jsonResponse = try? JSON(data: data!) else{return;}
            if(jsonResponse != nil){
                DispatchQueue.main.async{
                    print(jsonResponse);
                    self.collectionView.isHidden = false
                    self.showHttpErorImage(me: self, img: "Empty")
                    if urlToRequest=="mobiconnect/catalog/products/"
                    {
                         print(self.currentpage)
                        if self.currentpage==1
                        {
                            cedMageLoaders.removeLoadingIndicator(me: self);
                        }
                        else
                        {
                            cedMageLoaders.removeLoadingIndicator(me: self);
                        }
                    }
                    else
                    {
                        cedMageLoaders.removeLoadingIndicator(me: self);
                    }
                    //cedMageLoaders.removeLoadingIndicator(me: self);
                    if self.homePage{
                        self.parseData(json: jsonResponse, index: "products");
                    }else{
                        self.parseData(json: jsonResponse, index: "category");
                    }
                    
                    if let sortBtn = self.view.viewWithTag(1000) as? UIButton
                    {
                        sortBtn.isEnabled = true
                    }
                    if let filterBtn = self.view.viewWithTag(1001) as? UIButton
                    {
                        filterBtn.isEnabled = true
                    }
                    
                }
            }
        }
        task.resume();
    }
  
    func showHttpErorImage(me:UIViewController,img:String){
        
        if img == "Empty"
        {
            imagView.removeFromSuperview()
            return
        }
        
        let bounds = UIScreen.main.bounds
        imagView = UIImageView(frame: bounds)
        imagView.image = UIImage(named: img)
        imagView.contentMode = .scaleAspectFit
        me.view.addSubview(imagView)
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

