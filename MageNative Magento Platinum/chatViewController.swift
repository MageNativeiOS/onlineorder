//
//  chatViewController.swift
//  MageNative Magento Platinum
//
//  Created by Macmini on 26/11/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class chatViewController: MagenativeUIViewController {
    
    var customerId: String!
    var id: String!
    var currentPage = 1
    var chatPageData: chatData?
    var vendorComposedImage = vendorComposeMsgController()
    @IBOutlet weak var messageTextField: UITextView!
    
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var sendButton: UIButton!
    
    @IBOutlet weak var chatTableView: UITableView!
    var convertedFileString = ""
    var isFromVendor = false
    var selectedVendorEmail = String()
    var vendorId = String()
    var picker = UIImagePickerController()
    var attachedData = [[String : String]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
        guard let custId = userInfoDict["customerId"] else {return}
        self.customerId = custId
        
        //        if isFromVendor{
        //        self.sendRequest(url: "mobimessaging/view", params: ["customer_id":custId,"id":id,"page":"\(currentPage)"], store: true)
        //        }else{
        //            self.sendRequest(url: "mobimessaging/adminview", params: ["customer_id":custId,"id":id,"page":"\(currentPage)"], store: true)
        //        }
        chatDesign()
        // Do any additional setup after loading the view.
    }
    
    func chatDesign(){
        
        // if isFromVendor {
        sendRequestForVendorMessage()
        // }
        
        messageTextField.cardView()
        // addButton.layer.cornerRadius = 20
        sendButton.layer.cornerRadius = 5
        sendButton.setThemeColor()
        addButton.addTarget(self, action: #selector(addButtonTapped(_:)), for: .touchUpInside)
        sendButton.addTarget(self, action: #selector(sendMessageAction(_:)), for: .touchUpInside)
    }
    
    func sendRequestForVendorMessage() {
        let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
        guard let custId = userInfoDict["customerId"] else {return}
        self.customerId = custId
        
        //  self.sendRequest(url: "mobimessaging/view", params: ["customer_id":custId,"id":id,"page":"\(currentPage)"], store: true)
        
        if isFromVendor{
            self.sendRequest(url: "mobimessaging/view", params: ["customer_id":custId,"id":id,"page":"\(currentPage)"], store: true)
        }else{
            self.sendRequest(url: "mobimessaging/adminview", params: ["customer_id":custId,"id":id,"page":"\(currentPage)"], store: true)
        }
    }
    
    
    @objc func addButtonTapped(_ sender: UIButton) {
        
        let actionSheetControllerIOS8: UIAlertController = UIAlertController(title: "Please select", message: nil, preferredStyle: .actionSheet)
        
        let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { _ in
            print("Cancel")
        }
        actionSheetControllerIOS8.addAction(cancelActionButton)
        
        let saveActionButton = UIAlertAction(title: "Upload Image", style: .default)
        { _ in
            print("upload image")
            self.image_picker()
        }
        actionSheetControllerIOS8.addAction(saveActionButton)
        
        let uploadpdf = UIAlertAction(title: "Upload Document", style: .default)
        { _ in
            print("Delete")
            self.browseButtonPressed()
        }
        actionSheetControllerIOS8.addAction(uploadpdf)
        self.present(actionSheetControllerIOS8, animated: true, completion: nil)
    }
    
    func image_picker(){
        print("browseButtonPressed")
        picker.delegate=self
        picker.allowsEditing = false
        picker.sourceType = .photoLibrary
        self.present(picker, animated: true, completion: nil)
    }
    func browseButtonPressed(){
        
        let doc = UIDocumentPickerViewController(documentTypes: ["public.text","public.content","public.image","public.data"], in: .import)
        doc.delegate=self
        doc.modalPresentationStyle = .popover
        self.present(doc, animated: true, completion: nil)
    }
    
    @objc func sendMessageAction(_ sender: UIButton)
    {
        let messageText = self.messageTextField.text
        
        if messageText != ""
        {
            guard let userinfo = UserDefaults.standard.value(forKey: "userInfoDict") as? [String:String] else {print("user nil");return}
            let storeId = UserDefaults.standard.value(forKey: "storeId") as! String
            guard let customerId = userinfo["customerId"] else {print("empty customer");return}
            if isFromVendor{
                self.sendRequest(url: "mobimessaging/compose", params: ["message":messageText!,"email":selectedVendorEmail,"subject": "Message","page":"1","customer_id":customerId,"store_id":storeId, "image":"[\(convertedFileString)]", "vendor_id": vendorId])
            }
            else{
                self.sendRequest(url: "mobimessaging/admincompose", params:
                                    ["message":messageText!,"subject": "Message","page":"1","customer_id":customerId,"store_id":storeId, "image":"[\(convertedFileString)]"])
                //                "email":selectedVendorEmail,
            }
        }
        else{
            self.view.makeToast("Enter Some message to send!", duration: 1.0, position: .center)
        }
        
    }
    override func recieveResponse(data: Data?, requestUrl: String?, response: URLResponse?) {
        guard let data = data else {return}
        guard var json = try? JSON(data: data) else {return}
        json = json[0]
        print(json)
        
        if (requestUrl == "mobimessaging/view" ||  requestUrl == "mobimessaging/adminview")
        {
            do {
                if json["response"]["status"].stringValue == "false"{
                    self.view.makeToast(json["response"]["message"].stringValue, duration: 2.0, position: .center)
                    return
                }else{
                    // let message = try json["response"]["view"].rawData(options: [])
                    self.chatPageData = chatData(json: json["response"])
                    print(self.chatPageData)
                    chatTableView.delegate = self
                    chatTableView.dataSource = self
                    chatTableView.separatorStyle = .none
                    chatTableView.reloadData()
                }
            } catch let error{
                print(error.localizedDescription)
                self.view.makeToast(json["response"]["message"].stringValue, duration: 2.0, position: .center)
                // cedMageHttpException.showAlertView(me: self, msg: json["response"]["message"].stringValue, title: "Error")
            }
        }
        else if requestUrl == "mobimessaging/admincompose"
        {
            if json["response"]["status"].stringValue == "true"{
                self.view.makeToast(json["response"]["message"].stringValue, duration: 1.0, position: .center)
                
                messageTextField.text = ""
                self.attachedData.removeAll()
                let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
                guard let custId = userInfoDict["customerId"] else {return}
                self.customerId = custId
                self.sendRequest(url: "mobimessaging/view", params: ["customer_id":custId,"id":id,"page":"\(currentPage)"], store: true)
                
            }
            print("admin Send")
        }
        else if requestUrl == "mobimessaging/compose"        // for send data through api
        {
            if json["response"]["status"].stringValue == "true"{
                self.view.makeToast(json["response"]["message"].stringValue, duration: 1.0, position: .center)
                
                messageTextField.text = ""
                self.attachedData.removeAll()
                let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
                guard let custId = userInfoDict["customerId"] else {return}
                self.customerId = custId
                self.sendRequest(url: "mobimessaging/view", params: ["customer_id":custId,"id":id,"page":"\(currentPage)"], store: true)
            }
            print("vendor send")
        }
        
    }
}

extension chatViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return chatPageData?.view.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = chatTableView.dequeueReusableCell(withIdentifier: "chatControllerCell", for: indexPath) as! chatControllerCell
        //   let cell = chatTableView.dequeueReusableCell(withIdentifier: "chatControllerCell", for : indexPath)
        cell.dateLabel.text = chatPageData?.view[indexPath.row].created_at
        cell.messageLabel.text = chatPageData?.view[indexPath.row].message
        cell.imageHeight.constant = 0.0
        if chatPageData?.view[indexPath.row].image != ""{
            cell.imageHeight.constant = 150.0
            //cell.msgImage.imageView?.sd_setImage(with: URL(string: chatPageData?.view[indexPath.row].image ?? ""), placeholderImage: UIImage(named: "placeholder"))
            //cell.msgImage.buttonType = .cu
            cell.msgImage.sd_setImage(with: URL(string: chatPageData?.view[indexPath.row].image ?? ""), for: .normal)
        }
        //         cell.messageLabel.layer.cornerRadius = 10
        //        cell.messageLabel.clipsToBounds = true
        cell.messageLabel.numberOfLines = 0
        /* if chatPageData?.view[indexPath.row].sender == "customer"
         {
         cell.messageLabel.backgroundColor = .lightGray
         cell.messageLabel.textColor = .white
         cell.messageView.translatesAutoresizingMaskIntoConstraints = false
         cell.messageView.leadingAnchor.constraint(equalTo: cell.contentView.leadingAnchor, constant: 5).isActive = true
         cell.messageView.topAnchor.constraint(equalTo: cell.contentView.topAnchor, constant: 5).isActive = true
         cell.messageView.bottomAnchor.constraint(equalTo: cell.contentView.bottomAnchor, constant: -5).isActive = true
         cell.messageView.widthAnchor.constraint(lessThanOrEqualToConstant: 200).isActive = true
         
         }
         else
         {
         cell.messageLabel.backgroundColor = .lightGray
         cell.messageLabel.textColor = .black
         
         cell.messageView.translatesAutoresizingMaskIntoConstraints = false
         cell.messageView.trailingAnchor.constraint(equalTo: cell.contentView.trailingAnchor, constant: -5).isActive = true
         cell.messageView.topAnchor.constraint(equalTo: cell.contentView.topAnchor, constant: 5).isActive = true
         cell.messageView.bottomAnchor.constraint(equalTo: cell.contentView.bottomAnchor, constant: -5).isActive = true
         cell.messageView.widthAnchor.constraint(lessThanOrEqualToConstant: 200).isActive = true
         }*/
        
        cell.messageView.backgroundColor  = UIColor.init(hexString: "#B8E1F3")
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension chatViewController:UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIDocumentPickerDelegate {
    
    
    //   UIDocumentMenuDelegate
    
    
    //func documentMenu(_ documentMenu: UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
    //documentPicker.delegate=self
    //documentPicker.modalPresentationStyle = .fullScreen
    //self.present(documentPicker, animated: true, completion: nil)
    //}
    
    
    
    
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        
        let fileName=url.absoluteString.components(separatedBy: "/")
        // docUrlLabel.text=url.absoluteString
        print("didPickDocumentAt")
        // FileSelectedOrNot.text = fileName[0]
        
        let isSecuredURL = url.startAccessingSecurityScopedResource() == true
        let coordinator = NSFileCoordinator()
        var error: NSError? = nil
        coordinator.coordinate(readingItemAt: url, options: [], error: &error) { (url) -> Void in
            do{
                let temp=try Data(contentsOf: url)
                //self.trainingDocData=temp.base64EncodedString()
                
                let temp1=fileName[fileName.count-1].components(separatedBy: ".")
                print("The file you have choosed is not in correct format")
                // print(temp1)
                if (temp1[1] == "pdf" || temp1[1] == "zip" )
                {
                    print(temp1)
                    let dataPdf = temp.base64EncodedString()
                    self.attachedData.append(["name":"xyz.pdf", "base64_encoded_data": dataPdf])
                }
                else
                {
                    let msg = "The file you have choosed is not in correct format"
                    print(msg)
                    return
                    
                }
            }catch let error{
                
                print(error.localizedDescription)
                
            }
        }
        if (isSecuredURL) {
            url.stopAccessingSecurityScopedResource()
        }
        print(url)
        controller.dismiss(animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        // print(info)
        //    if let image = info[UIImagePickerController.InfoKey.originalImage.rawValue] as? UIImage {
        //
        //
        //        let imgData=image.pngData()
        //let imageData=(imgData?.base64EncodedString())!
        //        let values = info[UIImagePickerController.InfoKey.referenceURL.rawValue] as? NSURL
        //let imgName = values?.lastPathComponent
        
        /*let image = info[UIImagePickerControllerOriginalImage] as? UIImage
         let imgsize:NSData = UIImagePNGRepresentation(image!)! as NSData
         let fileSize = Double(imgsize.length / 1024 / 1024)
         totalSize += fileSize
         if totalSize > 5.00 {
         picker.dismiss(animated: true, completion: nil)
         self.view.makeToast("Please upload a file of less than 5 Mb", duration: 2.0, position: .center)
         }else{ */
        
        //attachedData.append(["name":imgName ?? "xyz.png", "base64_encoded_data": imageData])
        //        print(attachedData)
        //picker.dismiss(animated: true, completion: nil)
        //}
        
        let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)
        print(info)
        self.view.makeToast("Selected", duration: 2.0, position: .center)
        //    self.imageNameLabel.isHidden = false
        //    self.imageNameLabel.text = "Image Added"
        //    self.imageNameToSend = self.imageNameLabel.text!
        let image = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage
        if let tempData=(image ?? UIImage()).jpegData(compressionQuality: 0.5)
        {
            let dateFormatter: DateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MM/dd/yyyy hh:mm a"
            var selectedDate: String = dateFormatter.string(from: Date())
            selectedDate = selectedDate.replacingOccurrences(of: " ", with: "")
            let chat_file=(tempData.base64EncodedString())
            let dictionary = ["name":"124\(selectedDate).jpg","base64_encoded_data":chat_file]//,"type":"file"
            self.convertedFileString=convertDicTostring(str: dictionary)
            print(convertedFileString)
            
        }
        //self.imageNameLabel.text = info
        //Showing image in imageview after image is selected in ImagePickerController
        //  if let img = self.view.viewWithTag(currentTagForFileUpload) as? UIImageView
        //    {
        //        img.image = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage
        //
        //        if let tempData=(img.image ?? UIImage()).jpegData(compressionQuality: 0.5)
        //        {
        //            let dateFormatter: DateFormatter = DateFormatter()
        //            dateFormatter.dateFormat = "MM/dd/yyyy hh:mm a"
        //            var selectedDate: String = dateFormatter.string(from: Date())
        //            selectedDate = selectedDate.replacingOccurrences(of: " ", with: "")
        //            let chat_file=(tempData.base64EncodedString())
        //            let dictionary = ["name":"124\(selectedDate).jpg","base64_encoded_data":chat_file,"type":"image"]
        //            self.convertString=convertDicTostring(str: dictionary)
        //            print(convertString)
        //
        //        }
        //        imagViewArray.updateValue(img, forKey: "image_document")
        //        print(imagViewArray["name"])
        //    }
        self.dismiss(animated: true, completion: nil);
        
    }
    
    func documentMenuWasCancelled(_ documentMenu: UIDocumentMenuViewController) {
        documentMenu.dismiss(animated: true, completion: nil)
    }
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
}

fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
    return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
    return input.rawValue
}
