//
//  productPageImagesView.swift
//  MageNative Magento Platinum
//
//  Created by cedcoss on 20/03/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class productPageImagesView: UIView,UICollectionViewDataSource,UICollectionViewDelegate {
  
  
  @IBOutlet weak var productMainImage: UIImageView!
  
  @IBOutlet weak var offerText: UILabel!
  @IBOutlet weak var collectionView: UICollectionView!
  var productImages = [[String:String]]()
  
  var view = UIView()
  
  @IBOutlet weak var outOfStock: UIImageView!
  @IBOutlet weak var pageControl: UIPageControl!
  override init(frame: CGRect)
  {
    // 1. setup any properties here
    
    // 2. call super.init(frame:)
    super.init(frame: frame)
    
    // 3. Setup view from .xib file
    xibSetup()
  }
  
  required init?(coder aDecoder: NSCoder)
  {
    // 1. setup any properties here
    
    // 2. call super.init(coder:)
    super.init(coder: aDecoder)
    
    // 3. Setup view from .xib file
    xibSetup()
  }
  
  func xibSetup()
  {
    view = loadViewFromNib()
    
    // use bounds not frame or it'll be offset
    view.frame = bounds
    
    // Make the view stretch with containing view
    view.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
    
    //extra setup
    //imageWrapperView.makeCard(imageWrapperView, cornerRadius: 2, color: UIColor.black, shadowOpacity: 0.4);
    
    //          productMainImage.isUserInteractionEnabled = true;
    //          outOfStockImage.isHidden = true;
    collectionView.backgroundView?.backgroundColor = .clear
    collectionView.backgroundColor = .clear
    collectionView.delegate = self;
    collectionView.dataSource = self;
    collectionView.register(UINib(nibName: "productImageVideoCell", bundle: nil), forCellWithReuseIdentifier: "producImageVideoCell");
    
    //  heightOfImagesSection.constant = translateAccordingToDevice(CGFloat(50.0));
    
    //showSimilarProductsButton.makeCard(showSimilarProductsButton.imageView!, cornerRadius: 2, color: UIColor.black, shadowOpacity: 0.4);
    //showSimilarProductsButton.makeViewCircled(size: CGFloat(30.0));
    //extra setup
    
    
    
    // Adding custom subview on top of our view (over any custom drawing > see note below)
    addSubview(view);
  }
  
  @objc func loadViewFromNib() -> UIView
  {
    let bundle = Bundle(for: type(of: self))
    let nib = UINib(nibName: "productPageImagesView", bundle: bundle)
    
    // Assumes UIView is top level and only object in CustomView.xib file
    let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
    return view
  }
  
  /* colection view function for product images */
  @objc func numberOfSections(in collectionView: UICollectionView) -> Int {
    pageControl.numberOfPages = productImages.count
    return 1
  }
  
  @objc  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return productImages.count;
  }
  
  @objc func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "producImageVideoCell", for: indexPath) as! producImageVideoCell;
//    cell.mediaView.delegate = self
    let productImage = self.productImages[indexPath.row]
    print(self.productImages[indexPath.row])
    if productImage["media_type"] == "image" {
      cell.productImage.isHidden=false
      cell.videoView.isHidden=true
      if let url = productImage["url"] {
        cell.productImage.sd_setImage(with: URL(string: url), placeholderImage: UIImage(named: "placeholder"))
      }
    }
    else if productImage["media_type"] == "external-video"
    {
     cell.productImage.isHidden=true
     cell.videoView.isHidden=false
      guard let video_link = productImage["video_url"]  else {return UICollectionViewCell()}
      guard let video_url = URL(string:video_link)  else {return UICollectionViewCell()}
      cell.videoView.loadVideoURL(video_url)
    }

    return cell
  }
  
  //     @objc func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
  //          let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "imageRoot") as! imageSliderRoot
  //          cedMage().storeParameterInteger(parameter: 0)
  //          viewController.pageData = productImgsArray as NSArray
  //          self.rootPage.navigationController?.pushViewController(viewController, animated: true)
  //      }
  
  
  @objc func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
    //let width = (collectionView.bounds.size.width - 5 * 3) / 2 //some width
    let height = collectionView.bounds.size.height //ratio
    let width = collectionView.bounds.size.width //some width
    
    return CGSize(width: width, height: height);
  }
  
  @objc func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
    return UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0);
  }
  
  
  /* colection view function for product images */
  @objc func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
    pageControl.currentPage = Int(self.collectionView.contentOffset.x) / Int(self.collectionView.frame.size.width);
  }
  
}
