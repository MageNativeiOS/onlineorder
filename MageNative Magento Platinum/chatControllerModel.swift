//
//  File.swift
//  MageNative Magento Platinum
//
//  Created by Macmini on 27/11/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import Foundation

struct chatData{
    let subject  : String
    let status  : String
    let message  : String
    let view  : [chatView]
    let inbox_list : String
    init(json:JSON) {
        subject = json["subject"].stringValue
        status = json["status"].stringValue
        message = json["message"].stringValue
        view = json["view"].arrayValue.map({chatView(json: $0)})
        inbox_list = json["inbox_list"].stringValue
    }
}

struct chatView  {
    let created_at : String
    let image : String
    let sender_name : String
    let message : String
    let sender : String
    
    init(json:JSON) {
        created_at = json["created_at"].stringValue
        image = json["image"][0].stringValue
        sender_name = json["sender_name"].stringValue
        message = json["message"].stringValue
        sender = json["sender"].stringValue
    }
}
