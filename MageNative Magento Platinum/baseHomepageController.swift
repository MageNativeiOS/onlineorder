//
//  baseHomepageController.swift
//  MageNative Magento Platinum
//
//  Created by cedcoss on 16/11/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

struct homepageBanner{
    var banner_image: String?
    var link_to: String?
    var product_id: String?
    var type: String?
    var title: String?
    var id: String?
}
struct homepageDeal {
    var store_id: String?
    var group_status: String?
    var deal_link: String?
    var group_image_name: String?
    var view_all_status: String?
    var content: [dealContent]?
    var title: String?
    var group_end_date: String?
    var created_time: String?
    var group_start_date: String?
    var timer_status: String?
    var deal_duration: String?
    var update_time: String?
    var group_id: String?
    var is_static: String?
}
struct dealContent {
    var store_id: String?
    var status: String?
    var deal_title: String?
    var product_link: String?
    var deal_link: String?
    var start_date: String?
    var deal_image_name: String?
    var deal_type: String?
    var offer_text: String?
    var static_link: String?
    var category_link: String?
    var relative_link: String?
    var id: String?
    var end_date: String?
}
struct homepageProduct {
    var product_name: String?
    var description: String?
    var product_image: String?
    var stock_status: String?
    var type: String?
    var product_id: String?
    var special_price: String?
    var wishlist_item_id: String?
    var selected_quantity:String?
    var regular_price: String?
    var review: String?
    var Inwishlist: String?
}

class baseHomepageController: MagenativeUIViewController {
    
    @IBOutlet weak var homeTable: UITableView!
    
    var widgetBanners: [homepageBanner] = []
    var widgetBannerImages: [String] = []
    var dealsData: [homepageDeal] = []
    var featuredProducts: [homepageProduct] = []
    var currentPage: Int = 1
    var isLoadingMore = true // flag
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupHomepage()
        // Do any additional setup after loading the view.
    }
    
    //    override func viewWillAppear(_ animated: Bool) {
    //        self.tabBarController?.tabBar.isHidden = false
    //    }
    
    
    func setupHomepage() {
        sendRequestForBanner()
        sendRequestForDeals()
        sendRequestForFeaturedProducts()
    }
    
    func setupTableView() {
        homeTable.delegate = self
        homeTable.dataSource = self
        homeTable.separatorStyle = .none
        self.isLoadingMore = true
        DispatchQueue.main.async {
            self.homeTable.reloadData()
        }
    }
    
    func sendRequestForBanner() {
        self.getRequest(url: "mobiconnect/module/gethomepage/1/",store:true)
    }
    func sendRequestForDeals() {
        self.getRequest(url: "mobiconnectdeals/getdealgroup/",store:true)
    }
    
    func sendRequestForFeaturedProducts() {
        self.getRequest(url: "mobiconnect/home/featured/page/\(currentPage)/store/",store:true)
    }
    
    
    
    override func recieveResponse(data: Data?, requestUrl: String?, response: URLResponse?) {
        guard let data = data else {return}
        do {
            var json = try JSON(data: data)
            json = json[0]
            print(json)
            
            if (requestUrl?.contains("mobiconnect/module/gethomepage"))! {
                for ban in json["data"]["banner"].arrayValue {
                    self.widgetBannerImages.append(ban["banner_image"].stringValue)
                    let temp = homepageBanner(banner_image: ban["banner_image"].stringValue,
                                              link_to:  ban["link_to"].stringValue,
                                              product_id:  ban["product_id"].stringValue,
                                              type:  ban["type"].stringValue,
                                              title:  ban["title"].stringValue,
                                              id:  ban["id"].stringValue)
                    self.widgetBanners.append(temp)
                }
            }
            else if requestUrl == "mobiconnectdeals/getdealgroup/" {
                var contentArray = [dealContent]()
                for deal in json["data"]["deal_products"].arrayValue {
                    for cont in deal["content"].arrayValue {
                        let temp = dealContent(store_id: cont["store_id"].stringValue,
                                               status:  cont["status"].stringValue,
                                               deal_title:  cont["deal_title"].stringValue,
                                               product_link:  cont["product_link"].stringValue,
                                               deal_link:  cont["deal_link"].stringValue,
                                               start_date:  cont["start_date"].stringValue,
                                               deal_image_name:  cont["deal_image_name"].stringValue,
                                               deal_type:  cont["deal_type"].stringValue,
                                               offer_text:  cont["offer_text"].stringValue,
                                               static_link:  cont["static_link"].stringValue,
                                               category_link:  cont["category_link"].stringValue,
                                               relative_link:  cont["relative_link"].stringValue,
                                               id:  cont["id"].stringValue,
                                               end_date:  cont["end_date"].stringValue)
                        contentArray.append(temp)
                    }
                    let temp = homepageDeal(store_id: deal["store_id"].stringValue,
                                            group_status: deal["group_status"].stringValue,
                                            deal_link: deal["deal_link"].stringValue,
                                            group_image_name: deal["group_image_name"].stringValue,
                                            view_all_status: deal["view_all_status"].stringValue,
                                            content: contentArray,
                                            title: deal["title"].stringValue,
                                            group_end_date: deal["group_end_date"].stringValue,
                                            
                                            created_time: deal["created_time"].stringValue,
                                            group_start_date: deal["group_start_date"].stringValue,
                                            timer_status: deal["timer_status"].stringValue,
                                            deal_duration: deal["deal_duration"].stringValue,
                                            update_time: deal["update_time"].stringValue,
                                            group_id: deal["group_id"].stringValue,
                                            is_static: deal["is_static"].stringValue)
                    self.dealsData.append(temp)
                    contentArray.removeAll()
                }
                
                
                
            }
            else {
                if json.stringValue == "NO_PRODUCTS" {print("no more Products");self.isLoadingMore = false;return}
                for prod in json["featured_products"].arrayValue {
                    let temp = homepageProduct(product_name: prod["product_name"].stringValue,
                                               description:  prod["description"].stringValue,
                                               product_image:  prod["product_image"].stringValue,
                                               stock_status:  prod["stock_status"].stringValue,
                                               type:  prod["type"].stringValue,
                                               product_id:  prod["product_id"].stringValue,
                                               special_price:  prod["special_price"].stringValue,
                                               wishlist_item_id:  prod["wishlist_item_id"].stringValue, selected_quantity:prod["selected_quantity"].stringValue,
                                               regular_price:  prod["regular_price"].stringValue,
                                               review:  prod["Inwishlist"].stringValue,
                                               Inwishlist:  prod["Inwishlist"].stringValue)
                    self.featuredProducts.append(temp)
                }
                
            }
            
            setupTableView()
            
        }catch let error {
            print(error.localizedDescription)
        }
    }
}

extension baseHomepageController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
//<<<<<<< HEAD
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        case 1:
            if dealsData.count > 0 { return dealsData.count } else {return 0}
        case 2:
            return 1
        default:
            return 0
        }
    }
/*=======
    switch indexPath.section {
    case 0:
      let cell = tableView.dequeueReusableCell(withIdentifier: "homepageWidgetbannerCell", for: indexPath) as! homepageWidgetbannerCell
      cell.images = self.widgetBannerImages
      cell.bannerData = self.widgetBanners
      cell.parent = self
      return cell
    case 1:
      let cell = tableView.dequeueReusableCell(withIdentifier: "homepageDealsTableCell", for: indexPath) as! homepageDealsTableCell
      cell.topLabel.text = dealsData[indexPath.row].title?.uppercased()
      cell.topLabel.clipsToBounds = true
      cell.topBackgroundView.setThemeColor()
      if let dealTime = dealsData[indexPath.row].deal_duration {
        let timeinterval : TimeInterval = (dealTime as NSString).doubleValue
        cell.startTime = timeinterval
      }
      if let timer = dealsData[indexPath.row].timer_status {
        if timer != "1" {
          cell.dealTimer.isHidden = true
          cell.dealTimer.text = ""
        }
      }
      cell.dealsContent = dealsData[indexPath.row].content!
      cell.parent = self
      return cell
    case 2:
      let cell = tableView.dequeueReusableCell(withIdentifier: "homepageFeaturedCell", for: indexPath) as! homepageFeaturedCell
      cell.products = self.featuredProducts
      cell.parent = self
      return cell
    default:
      return UITableViewCell()
>>>>>>> 4e648e2112ac5623cdefd593e5c414ebe5a30f69
    }*/
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "homepageWidgetbannerCell", for: indexPath) as! homepageWidgetbannerCell
            cell.images = self.widgetBannerImages
            cell.bannerData = self.widgetBanners
            cell.parent = self
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "homepageDealsTableCell", for: indexPath) as! homepageDealsTableCell
            cell.topLabel.text = dealsData[indexPath.row].title?.uppercased()
            cell.topLabel.clipsToBounds = true
            cell.topBackgroundView.setThemeColor()
            if let dealTime = dealsData[indexPath.row].deal_duration {
                let timeinterval : TimeInterval = (dealTime as NSString).doubleValue
                cell.startTime = timeinterval
            }
            //cell.timerStatus = dealsData[indexPath.row].timer_status ?? "2"
            if let timer = dealsData[indexPath.row].timer_status {
                
                if timer != "1" {
                    
                    cell.dealTimer.isHidden = true
                    
                    cell.dealTimer.text = ""
                    
                }
                
            }
            cell.dealsContent = dealsData[indexPath.row].content!
            cell.parent = self
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "homepageFeaturedCell", for: indexPath) as! homepageFeaturedCell
            cell.products = self.featuredProducts
            cell.parent = self
            return cell
        default:
            return UITableViewCell()
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return UIScreen.main.bounds.width * (3.5/7)
        case 1:
            return CGFloat(returnHeightForDeal(dealsData[indexPath.row].content!) * 160 + 50)
        case 2:
            return CGFloat(returnHeightForFeatured(featuredProducts) * 250 + 10)
        default:
            return 0
        }
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 2 {
            
            let headerView = UIView()
            if #available(iOS 13.0, *) {
                headerView.backgroundColor = UIColor.systemBackground
            } else {
                // Fallback on earlier versions
            }
            
            let label = UILabel()
            label.text = "Featured Products"
            label.textAlignment = .center
            label.font = UIFont.boldSystemFont(ofSize: 22)
            label.setThemeColor()
            label.textColor = Settings.themeTextColor
            headerView.addSubview(label)
            label.translatesAutoresizingMaskIntoConstraints = false
            label.leadingAnchor.constraint(equalTo: headerView.leadingAnchor, constant: 10).isActive = true
            label.trailingAnchor.constraint(equalTo: headerView.trailingAnchor, constant: -10).isActive = true
            label.centerXAnchor.constraint(equalTo: headerView.centerXAnchor, constant: 0).isActive = true
            label.centerYAnchor.constraint(equalTo: headerView.centerYAnchor, constant: 0).isActive = true
            label.heightAnchor.constraint(equalToConstant: 35).isActive = true
            label.clipsToBounds = true
            label.layer.cornerRadius = 8.0
            return headerView
        }else {
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 2 {
            return 60
        }else {
            return 0
        }
    }
    
    func returnHeightForDeal(_ subcategory:[dealContent]) -> Int {
        if subcategory.count % 2 != 0{
            return subcategory.count / 2 + 1
        }
        return subcategory.count / 2
        
    }
    
    func returnHeightForFeatured(_ product:[homepageProduct]) -> Int {
        if product.count % 2 != 0{
            return product.count / 2 + 1
        }
        return product.count / 2
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let contentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height;
        let diffrence = maximumOffset - contentOffset
        if isLoadingMore && (diffrence <= CGFloat(40)) {
            // sending request to get more data for featured Product
            self.isLoadingMore = false
            self.currentPage += 1
            sendRequestForFeaturedProducts()
        }
    }
}


