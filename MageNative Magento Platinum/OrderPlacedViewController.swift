/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */

import UIKit

class OrderPlacedViewController: MagenativeUIViewController {
    
    var order_id = "";
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.basicFoundationToRenderView(bottomMargin: CGFloat(0.0));
        self.renderOrderSuccessPage();
    }
    
    func renderOrderSuccessPage(){
        
        self.makeSomeSpaceInStackView(height: CGFloat(0.0));
        
        let color = Settings.themeColor
        let theme_color = color
        
        
        
        let successImageView = UIImageView(image: UIImage(named: "orderPlaced"));
        successImageView.translatesAutoresizingMaskIntoConstraints = false;
        stackView.addArrangedSubview(successImageView);
        let successImageViewHeight = translateAccordingToDevice(CGFloat(150.0));
        successImageView.heightAnchor.constraint(equalToConstant: successImageViewHeight).isActive = true;
        successImageView.widthAnchor.constraint(equalToConstant: successImageViewHeight).isActive = true;
        
        
        let successMessageLabel = UILabel();
        successMessageLabel.translatesAutoresizingMaskIntoConstraints = false;
        
        let successMessage = "Your Order Has Been Placed Successfully".localized;
        successMessageLabel.text = successMessage;
        if #available(iOS 13.0, *) {
            successMessageLabel.textColor = UIColor.label
        } else {
            successMessageLabel.textColor = UIColor.black
        }
        successMessageLabel.textAlignment = NSTextAlignment.center;
        if #available(iOS 13.0, *) {
            successMessageLabel.backgroundColor = UIColor.systemBackground
        } else {
            successMessageLabel.backgroundColor = UIColor.white
        };
     
        let fontToApply = UIFont(fontName: "", fontSize: CGFloat(15.0))!;
        successMessageLabel.font = fontToApply;
        let textAttributes = [NSAttributedString.Key.font: fontToApply];
        var rect = successMessage.boundingRect(with: CGSize(width: screenSize.width, height: CGFloat.greatestFiniteMagnitude), options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: textAttributes, context: nil);
        stackView.addArrangedSubview(successMessageLabel);
        let successMessageLabelHeight = translateAccordingToDevice(CGFloat(rect.height))+20;
        successMessageLabel.heightAnchor.constraint(equalToConstant: successMessageLabelHeight).isActive = true;
        self.setLeadingAndTralingSpaceFormParentView(successMessageLabel, parentView:stackView);
        
        
        let orderIdLabel = UILabel();
        orderIdLabel.translatesAutoresizingMaskIntoConstraints = false;
        let orderIdText = "Your Order Id Is : ".localized+self.order_id;
        orderIdLabel.text = orderIdText;
        
        orderIdLabel.textAlignment = NSTextAlignment.center;
        if #available(iOS 13.0, *) {
            orderIdLabel.backgroundColor = UIColor.systemBackground
            orderIdLabel.textColor = UIColor.label
        } else {
            orderIdLabel.backgroundColor = UIColor.white
            orderIdLabel.textColor = UIColor.black
        };
       
        orderIdLabel.font = fontToApply;
        rect = orderIdText.boundingRect(with: CGSize(width: screenSize.width, height: CGFloat.greatestFiniteMagnitude), options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: textAttributes, context: nil);
        stackView.addArrangedSubview(orderIdLabel);
        let orderIdLabelHeight = translateAccordingToDevice(CGFloat(rect.height))+20;
        orderIdLabel.heightAnchor.constraint(equalToConstant: orderIdLabelHeight).isActive = true;
        self.setLeadingAndTralingSpaceFormParentView(orderIdLabel, parentView:stackView);
        
        
        let pickAddressButton = UIButton();
        pickAddressButton.translatesAutoresizingMaskIntoConstraints = false;
        pickAddressButton.setTitle("Countinue Shopping".localized, for: UIControl.State.normal);
        pickAddressButton.setThemeColor()
        pickAddressButton.setTitleColor(Settings.themeTextColor, for: UIControl.State.normal);
        pickAddressButton.makeCornerRounded(cornerRadius: translateAccordingToDevice(CGFloat(5.0)));
        pickAddressButton.layer.borderWidth =  CGFloat(2);
        pickAddressButton.layer.borderColor = theme_color.cgColor;
        pickAddressButton.titleLabel?.font = UIFont(fontName: "HelveticaNeue-Medium", fontSize: CGFloat(15.0));
        pickAddressButton.addTarget(self, action: #selector(OrderPlacedViewController.continueShoppingPressed(_:)), for: UIControl.Event.touchUpInside);
        stackView.addArrangedSubview(pickAddressButton);
        let pickAddressButtonHeight = translateAccordingToDevice(CGFloat(40.0));
        pickAddressButton.heightAnchor.constraint(equalToConstant: pickAddressButtonHeight).isActive = true;
        self.setLeadingAndTralingSpaceFormParentView(pickAddressButton, parentView:stackView);
        
        self.makeSomeSpaceInStackView(height: CGFloat(0.0));
    }
    
    @objc func continueShoppingPressed(_ sender:UIButton){
        /*if let viewController = UIStoryboard(name: "homeLayouts", bundle: nil).instantiateViewController(withIdentifier: "baseHomepageController") as? baseHomepageController {
            self.navigationController?.setViewControllers([viewController], animated: true)
        }*/
        navigationController?.setViewControllers([HomeController()], animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
