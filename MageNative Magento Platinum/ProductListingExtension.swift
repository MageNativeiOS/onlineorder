/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */


import UIKit

extension ProductListingViewController{
    
    func renderProductListingPage(){
        
        self.makeSomeSpaceInStackView(height: CGFloat(0.0));
        var counter = 0;
        while counter <= productArray.count-1 {
            let productGridViewHeight = translateAccordingToDevice(CGFloat(300.0));
            
            let productView = ProductListInfoOverImageView();
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(ProductListingViewController.productCardTapped(_:)));
            productView.addGestureRecognizer(tapGesture);
            tapGesture.delegate=self;
            productView.translatesAutoresizingMaskIntoConstraints = false;
            stackView.addArrangedSubview(productView);
            productView.heightAnchor.constraint(equalToConstant: productGridViewHeight).isActive = true;
            
            if(productArray[counter]["Inwishlist"] == "OUT"){
                productView.wishlistButton.setImage(UIImage(named:"LikeEmpty"), for: UIControl.State.normal);
            }
            else{
                productView.wishlistButton.setImage(UIImage(named:"LikeFilled"), for: UIControl.State.normal);
            }
            productView.wishlistButton.tag = Int(productArray[counter]["product_id"]!)!;
         //   productView.wishlistButton.addTarget(self, action: #selector(cedMageSubcategoryListing.wishlistButtonPressed(_:)), for: UIControlEvents.touchUpInside);
            //module check
            if(!cedMage.checkModule(string:"Ced_MobiconnectWishlist")){
                productView.wishlistButton.isHidden = true;
            }
            
            let urlToRequest = productArray[counter]["product_image"]!;
            
            if(imageCacheContainer[urlToRequest] != nil){
                productView.productImage.image = imageCacheContainer[urlToRequest];
            }
            else{
                print("NO Cacahe Available");
                downloadImageFromServer(productView.productImage,urlToRequest:urlToRequest);
            }
            self.setLeadingAndTralingSpaceFormParentView(productView, parentView:stackView);
            
           
            let productInfoView = ProductInfoView();
            productInfoView.productName.text = productArray[counter]["product_name"]!;
            productInfoView.productName.fontColorTool()
            productInfoView.ratingPointsLabel.text = productArray[counter]["review"]!;
            productInfoView.translatesAutoresizingMaskIntoConstraints = false;
            let productInfoViewHeight = translateAccordingToDevice(CGFloat(120.0));
            productInfoView.productRegularPrice.fontColorTool()
            productInfoView.productSpecialPrice.fontColorTool()
            if(productArray[counter]["starting_from"] != ""){
                productInfoView.productRegularPrice.text = "Starting At : ".localized+productArray[counter]["starting_from"]!;
                if(productArray[counter]["from_price"] != ""){
                    productInfoView.productSpecialPrice.text = "As Low As : "+productArray[counter]["from_price"]!;
                }
                else{
                    productInfoView.productSpecialPrice.isHidden = true;
                }
            }
            else{
                productInfoView.productRegularPrice.text = "Price : ".localized+productArray[counter]["regular_price"]!;
                if(productArray[counter]["special_price"] != "no_special"){
                    productInfoView.productSpecialPrice.text = "New Price : "+productArray[counter]["special_price"]!;
                }
                else{
                    productInfoView.productSpecialPrice.isHidden = true;
                }
            }
            productView.addSubview(productInfoView);
            productView.addConstraint(NSLayoutConstraint(item: productInfoView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: productInfoViewHeight));
            productView.addConstraint(NSLayoutConstraint(item: productInfoView, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: productView, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 0));
            productView.addConstraint(NSLayoutConstraint(item: productInfoView, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: productView, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 0));
            productView.addConstraint(NSLayoutConstraint(item: productInfoView, attribute: NSLayoutConstraint.Attribute.bottom, relatedBy: NSLayoutConstraint.Relation.equal, toItem: productView, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: 0));
            //productInfoView.isHidden = true;
            
            // render show ProductInfoView Button
            let showProductInfoViewButton = UIButton();
            showProductInfoViewButton.setImage(UIImage(named:"showInfo"), for: UIControl.State.normal);
            showProductInfoViewButton.translatesAutoresizingMaskIntoConstraints = false;
            showProductInfoViewButton.addTarget(self, action: #selector(ProductListingViewController.showProductInfoViewButtonPressed(_:)), for: UIControl.Event.touchUpInside);
            let showProductInfoViewButtonHeight = translateAccordingToDevice(CGFloat(30.0));
            productView.addSubview(showProductInfoViewButton);
            productView.addConstraint(NSLayoutConstraint(item: showProductInfoViewButton, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: showProductInfoViewButtonHeight));
            productView.addConstraint(NSLayoutConstraint(item: showProductInfoViewButton, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: showProductInfoViewButtonHeight));
            productView.addConstraint(NSLayoutConstraint(item: showProductInfoViewButton, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: productView, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 5));
            productView.addConstraint(NSLayoutConstraint(item: showProductInfoViewButton, attribute: NSLayoutConstraint.Attribute.bottom, relatedBy: NSLayoutConstraint.Relation.equal, toItem: productInfoView, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: -5));
            showProductInfoViewButton.isHidden = true;
            
            // render hide ProductInfoView Button
            let hideProductInfoViewButton = UIButton();
            hideProductInfoViewButton.setImage(UIImage(named:"hideInfo"), for: UIControl.State.normal);
            hideProductInfoViewButton.translatesAutoresizingMaskIntoConstraints = false;
            hideProductInfoViewButton.addTarget(self, action: #selector(ProductListingViewController.hideProductInfoViewButtonPressed(_:)), for: UIControl.Event.touchUpInside);
            let hideProductInfoViewButtonHeight = translateAccordingToDevice(CGFloat(30.0));
            productView.addSubview(hideProductInfoViewButton);
            productView.addConstraint(NSLayoutConstraint(item: hideProductInfoViewButton, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: hideProductInfoViewButtonHeight));
            productView.addConstraint(NSLayoutConstraint(item: hideProductInfoViewButton, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: hideProductInfoViewButtonHeight));
            productView.addConstraint(NSLayoutConstraint(item: hideProductInfoViewButton, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: productView, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 5));
            productView.addConstraint(NSLayoutConstraint(item: hideProductInfoViewButton, attribute: NSLayoutConstraint.Attribute.bottom, relatedBy: NSLayoutConstraint.Relation.equal, toItem: productInfoView, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1, constant: -5));
            
            
            counter = counter+1;
        }
        
        self.makeSomeSpaceInStackView(height: CGFloat(0.0));
        self.animateProductListingSection();
    }
    
    @objc func showProductInfoViewButtonPressed(_ sender:UIButton){
        if let productView = sender.superview as? ProductListInfoOverImageView{
            for view in productView.subviews{
                if let productInfoView = view as? ProductInfoView{
                    productInfoView.isHidden = false;
                    productInfoView.center.y += (productInfoView.superview?.bounds.height)!+translateAccordingToDevice(30.0);
                    UIView.animate(withDuration: 1.0, delay: 0, options: UIView.AnimationOptions.curveLinear, animations: {
                            productInfoView.center.y -= ((productInfoView.superview?.bounds.height)!+translateAccordingToDevice(30.0));
                        }, completion: nil
                    );
                }
                else if let button = view as? UIButton{
                    if(button == sender){
                        button.isHidden = true;
                    }
                    else{
                        button.isHidden = false;
                        button.center.y += (button.superview?.bounds.height)!;
                        UIView.animate(withDuration: 1.0, delay: 0, options: UIView.AnimationOptions.curveLinear, animations: {
                            button.center.y -= (button.superview?.bounds.height)!;
                            }, completion: nil
                        );
                    }
                }
            }
        }
    }
    
    @objc func hideProductInfoViewButtonPressed(_ sender:UIButton){
        if let productView = sender.superview as? ProductListInfoOverImageView{
            for view in productView.subviews{
                if let productInfoView = view as? ProductInfoView{
                    UIView.animate(withDuration: 1.0, delay: 0, options: UIView.AnimationOptions.curveLinear, animations: {
                            productInfoView.center.y += ((productInfoView.superview?.bounds.height)!+translateAccordingToDevice(30.0));
                        }, completion: {completed in
                            productInfoView.isHidden = true;
                            productInfoView.center.y -= ((productInfoView.superview?.bounds.height)!+translateAccordingToDevice(30.0));
                        }
                    );
                }
                else if let button = view as? UIButton{
                    if(button == sender){
                        UIView.animate(withDuration: 1.0, delay: 0, options: UIView.AnimationOptions.curveLinear, animations: {
                            button.center.y += (button.superview?.bounds.height)!;
                            }, completion: {completed in
                                button.isHidden = true;
                                button.center.y -= (button.superview?.bounds.height)!;
                            }
                        );
                    }
                    else{
                        button.isHidden = false;
                        button.alpha = 0.0;
                        UIView.animate(withDuration: 1.0, delay: 0.5, options: UIView.AnimationOptions.curveLinear, animations: {
                                button.alpha = 1.0;
                            }, completion: nil
                        );
                    }
                }
            }
        }
    }
    
    
    /*func transitionCurlUp(){
        
        // set a transition style
        let transitionOptions = UIViewAnimationOptions.TransitionCurlUp
        
        UIView.transitionWithView(self.container, duration: 1.0, options: transitionOptions, animations: {
            // remove the front object...
            views.frontView.removeFromSuperview()
            
            // ... and add the other object
            self.container.addSubview(views.backView)
            
            }, completion: { finished in
                // any code entered here will be applied
                // .once the animation has completed
        })
    }*/
    

}
