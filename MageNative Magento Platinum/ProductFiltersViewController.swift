/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */



import UIKit

class ProductFiltersViewController: MagenativeUIViewController,UIScrollViewDelegate,UITableViewDelegate,UITableViewDataSource {
    
    var jsonForFilters:JSON!;
    @IBOutlet weak var clearFilterButton: UIButton!
    @IBOutlet weak var applyFilterButton: UIButton!
    @IBOutlet weak var leftView: UIView!
    @IBOutlet weak var rightView: UIView!
    var scrollViewLeft = UIScrollView()
    var scrollViewRight = UIScrollView()
    var stackViewLeft = UIStackView();
    var stackViewRight : UIStackView!;
    
    let heightOfChildFilterCheckbox = translateAccordingToDevice(CGFloat(40));
    
    var filter_label = [String:String]();
    var filterArray = [String:[String]]();
    var selectedFiltersArray = [String:[String]]();
    var currentParentFilter = "";
    var currentParentFilterIndex = -1;
    
    let defaultColor = UIColor.lightGray;
    let currentTappedColor = UIColor.darkGray;
    var selectedColor : UIColor!;
    
    var currentArray = [String]()
    var defColl = cedMageDefaultCollection()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        selectedColor = cedMage.UIColorFromRGB(colorCode: "#F9C53D");
        if(defaults.object(forKey: "previousSelectedFilters") != nil){
            self.selectedFiltersArray = (defaults.object(forKey: "previousSelectedFilters") as? [String:[String]])!
        }
        
        print(jsonForFilters)
        if jsonForFilters != nil {
            
            if jsonForFilters.count ==  0 {
                cedMageHttpException.showAlertView(me: self, msg: "No Filters available", title: "Error")
                return
            }
            
            for (_,val) in jsonForFilters["data"]["filter_label"]{
                self.filter_label[val["att_label"].stringValue] = val["att_code"].stringValue;
            }
            if  jsonForFilters["data"]["filter"].count > 0 {
                
                for (_,val) in jsonForFilters["data"]["filter"]{
                    for(key,dataArray) in val{
                        self.filterArray[key] = dataArray.arrayObject as? [String];
                        if(defaults.object(forKey: "previousSelectedFilters") == nil){
                            print("selectedfilteer")
                            self.selectedFiltersArray[key] = [String]();
                        }
                    }
                }
            }
        }
        
        applyFilterButton.layer.borderColor = UIColor(hexString: "#407A52")?.cgColor
        applyFilterButton.cardView()
        clearFilterButton.cardView()
        applyFilterButton.layer.borderWidth = 2
//        applyFilterButton.fontColorTool()
        //applyFilterButton.setTitleColor(UIColor.black, for: UIControl.State.normal);
        applyFilterButton.setTitle("Apply Filter".localized, for: UIControl.State.normal);
        applyFilterButton.addTarget(self, action: #selector(ProductFiltersViewController.applyFilterButtonPressed(_:)), for: UIControl.Event.touchUpInside);
        
        clearFilterButton.layer.borderWidth = 2
        clearFilterButton.layer.borderColor = UIColor(hexString: "#DC3023")?.cgColor
        //clearFilterButton.setTitleColor(UIColor.black, for: UIControl.State.normal);
//        clearFilterButton.fontColorTool()
        clearFilterButton.setTitle("Clear Filter".localized, for: UIControl.State.normal);
        clearFilterButton.addTarget(self, action: #selector(ProductFiltersViewController.clearFilterButtonPressed(_:)), for: UIControl.Event.touchUpInside);
        
        if(self.filter_label.count > 0){
            self.basicFoundationToRenderView(bottomMargin: CGFloat(0.0));
        }
        else{
            self.renderNoDataImage(imageName: "");
        }
        scrollViewRight.delegate = self
        
    }
    
        func renderNoDataImage(imageName:String){
        let noDataImageView = UIImageView();
        noDataImageView.translatesAutoresizingMaskIntoConstraints = false;
        noDataImageView.image = UIImage(named: imageName);
        self.view.addSubview(noDataImageView);
        self.view.addConstraint(NSLayoutConstraint(item: noDataImageView, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 0));
        self.view.addConstraint(NSLayoutConstraint(item: noDataImageView, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 0));
        self.view.addConstraint(NSLayoutConstraint(item: noDataImageView, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1, constant: 0));
        self.view.addConstraint(NSLayoutConstraint(item: noDataImageView, attribute: NSLayoutConstraint.Attribute.bottom, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: 0));
    }
    
    override func basicFoundationToRenderView(bottomMargin:CGFloat){
        
        // left side section
        scrollViewLeft = UIScrollView();
        scrollViewLeft.translatesAutoresizingMaskIntoConstraints = false;
        scrollViewLeft.showsHorizontalScrollIndicator = false;
        scrollViewLeft.showsVerticalScrollIndicator = false;
        self.leftView.addSubview(scrollViewLeft);
        self.leftView.addConstraint(NSLayoutConstraint(item: scrollViewLeft, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.leftView, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 0));
        self.leftView.addConstraint(NSLayoutConstraint(item: scrollViewLeft, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.leftView, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 0));
        self.leftView.addConstraint(NSLayoutConstraint(item: scrollViewLeft, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.leftView, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1, constant: 0));
        self.leftView.addConstraint(NSLayoutConstraint(item: scrollViewLeft, attribute: NSLayoutConstraint.Attribute.bottom, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.leftView, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: -(bottomMargin)));
        
        // adding stackview for left side
        stackViewLeft = UIStackView();
        stackViewLeft.translatesAutoresizingMaskIntoConstraints = false;
        stackViewLeft.axis  = NSLayoutConstraint.Axis.vertical;
        stackViewLeft.distribution  = UIStackView.Distribution.equalSpacing;
        stackViewLeft.alignment = UIStackView.Alignment.center;
        stackViewLeft.spacing   = 2.0;
        scrollViewLeft.addSubview(stackViewLeft);
        self.leftView.addConstraint(NSLayoutConstraint(item: stackViewLeft, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.leftView, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 0));
        self.leftView.addConstraint(NSLayoutConstraint(item: stackViewLeft, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.leftView, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 0));
        scrollViewLeft.addConstraint(NSLayoutConstraint(item: stackViewLeft, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: scrollViewLeft, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1, constant: 0));
        scrollViewLeft.addConstraint(NSLayoutConstraint(item: stackViewLeft, attribute: NSLayoutConstraint.Attribute.bottom, relatedBy: NSLayoutConstraint.Relation.equal, toItem: scrollViewLeft, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: 0));
        
        
        // right side section
        scrollViewRight = UIScrollView();
        scrollViewRight.translatesAutoresizingMaskIntoConstraints = false;
        scrollViewRight.showsHorizontalScrollIndicator = false;
        scrollViewRight.showsVerticalScrollIndicator = false;
        self.rightView.addSubview(scrollViewRight);
        self.rightView.addConstraint(NSLayoutConstraint(item: scrollViewRight, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.rightView, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 0));
        self.rightView.addConstraint(NSLayoutConstraint(item: scrollViewRight, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.rightView, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 0));
        self.rightView.addConstraint(NSLayoutConstraint(item: scrollViewRight, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.rightView, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1, constant: 0));
        self.rightView.addConstraint(NSLayoutConstraint(item: scrollViewRight, attribute: NSLayoutConstraint.Attribute.bottom, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.rightView, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: -(bottomMargin)));
        
        // adding stackview for right side
        stackViewRight = UIStackView();
        stackViewRight.translatesAutoresizingMaskIntoConstraints = false;
        stackViewRight.axis  = NSLayoutConstraint.Axis.vertical;
        stackViewRight.distribution  = UIStackView.Distribution.equalSpacing;
        stackViewRight.alignment = UIStackView.Alignment.center;
        stackViewRight.spacing   = 2.0;
        scrollViewRight.addSubview(stackViewRight);
        self.rightView.addConstraint(NSLayoutConstraint(item: stackViewRight!, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.rightView, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 0));
        self.rightView.addConstraint(NSLayoutConstraint(item: stackViewRight!, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.rightView, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 0));
        scrollViewRight.addConstraint(NSLayoutConstraint(item: stackViewRight!, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: scrollViewRight, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1, constant: 0));
        scrollViewRight.addConstraint(NSLayoutConstraint(item: stackViewRight!, attribute: NSLayoutConstraint.Attribute.bottom, relatedBy: NSLayoutConstraint.Relation.equal, toItem: scrollViewRight, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: 0));
        
        var firstParentFilter = "";
        var currentIndex = 0;
        for (key,_) in filter_label{
            if(self.filterArray[filter_label[key]!] == nil){
                continue;
            }
            
            let button = UIButton();
            button.translatesAutoresizingMaskIntoConstraints = false;
            button.titleLabel?.font = UIFont(fontName: "", fontSize: CGFloat(14.0));
            
            let keyToUse = (filter_label[key]!);
            print("filter_label[key]");
            print(keyToUse);
            print(selectedFiltersArray);
            print(self.selectedFiltersArray[keyToUse]?.count as Any);
            
            if(firstParentFilter == ""){
                button.backgroundColor = currentTappedColor;
            }
            else{
                button.backgroundColor = defaultColor;
            }
            if(firstParentFilter == ""){
                firstParentFilter = filter_label[key]!;
                self.currentParentFilter = firstParentFilter;
                self.currentParentFilterIndex = currentIndex;
            }
            currentIndex = currentIndex+1;
            
            if let count  = self.selectedFiltersArray[keyToUse]?.count {
                if(count > 0 ){
                    button.backgroundColor = selectedColor;
                }
            }
            if #available(iOS 13.0, *) {
                button.setTitleColor(UIColor.label, for: UIControl.State.normal)
            } else {
                button.setTitleColor(UIColor.black, for: UIControl.State.normal)
            };
//            button.fontColorTool()
            
            button.setTitle(key, for: UIControl.State.normal);
            button.addTarget(self, action: #selector(ProductFiltersViewController.parentFilterSelected(_:)), for: UIControl.Event.touchUpInside);
            stackViewLeft.addArrangedSubview(button);
            button.heightAnchor.constraint(equalToConstant: 40).isActive = true;
            self.setLeadingAndTralingSpaceFormParentView(button, parentView:stackViewLeft);
        }
        
        let arrayToLoopForSubfilters = self.filterArray[firstParentFilter]!;
        if(arrayToLoopForSubfilters.count == 0){
            let msg = "No Filter Found".localized;
            self.view.makeToast(msg, duration: 2.0, position: .bottom, title: nil, image: nil, style: nil, completion: nil);
            return;
        }
        
        self.currentArray = arrayToLoopForSubfilters
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false;
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.separatorStyle = .none
        self.stackViewRight.addArrangedSubview(tableView);
        tableView.heightAnchor.constraint(equalToConstant: self.view.frame.height-20).isActive = true;
        self.setLeadingAndTralingSpaceFormParentView(tableView, parentView:self.stackViewRight);
        tableView.reloadData()
        
//        for (key) in arrayToLoopForSubfilters{
//
//            let hiddenLabel = UILabel();
//            hiddenLabel.translatesAutoresizingMaskIntoConstraints = false;
//            hiddenLabel.text = key;
//            stackViewRight.addArrangedSubview(hiddenLabel);
//            hiddenLabel.heightAnchor.constraint(equalToConstant: heightOfChildFilterCheckbox).isActive = true;
//            self.setLeadingAndTralingSpaceFormParentView(hiddenLabel, parentView:stackViewRight);
//            hiddenLabel.isHidden = true;
//
//            let checkboxView = productfilterView();
//            checkboxView.translatesAutoresizingMaskIntoConstraints = false;
//
//            checkboxView.labelName.textAlignment = NSTextAlignment.left
//            let titleToSet = key.components(separatedBy: "#").last!;
//            print(titleToSet);
//            print(selectedFiltersArray);
//            print(currentParentFilter);
//            if let _ = selectedFiltersArray[self.currentParentFilter]?.index(of: key) {
//                checkboxView.checkBoxImage.image = UIImage(named: "CheckedCheckbox");
//            }
//            do  {
//                let string = try NSMutableAttributedString(data: (titleToSet.data(using: String.Encoding.unicode)!), options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,NSFontAttributeName: UIFont.systemFont(ofSize: 13)], documentAttributes: nil)
//                checkboxView.labelName.attributedText = string
//            } catch {
//
//            }
//
//
//            checkboxView.filterButton.addTarget(self, action: #selector(ProductFiltersViewController.childFilterSelected(_:)), for: UIControlEvents.touchUpInside);
//            stackViewRight.addArrangedSubview(checkboxView);
//            checkboxView.heightAnchor.constraint(equalToConstant: heightOfChildFilterCheckbox).isActive = true;
//            self.setLeadingAndTralingSpaceFormParentView(checkboxView, parentView:stackViewRight);
//        }
        scrollViewLeft.contentSize = CGSize(width: stackViewLeft.frame.width, height: stackViewLeft.frame.height);
        scrollViewRight.contentSize = CGSize(width: stackViewRight.frame.width, height: stackViewRight.frame.height);
        
    }
    
    @objc func parentFilterSelected(_ sender:UIButton){
        print("parentFilterSelected");
        
        print(currentParentFilterIndex);
        if let previousParentFilter = stackViewLeft.arrangedSubviews[currentParentFilterIndex] as? UIButton {
            print(previousParentFilter.backgroundColor as Any);
            if(previousParentFilter.backgroundColor == currentTappedColor){
                previousParentFilter.backgroundColor = defaultColor;
            }
        }
        
        if let index = stackViewLeft.arrangedSubviews.firstIndex(of: sender){
            self.currentParentFilterIndex = index;
        }
        sender.backgroundColor = currentTappedColor;
        scrollViewRight.isScrollEnabled = false
    stackViewRight.subviews.forEach({ $0.removeFromSuperview() });
        
        let key = (sender.titleLabel?.text)!;
        let keyToUse = self.filter_label[key]!;
        
        self.currentParentFilter = keyToUse;
        let arrayToLoopForSubfilters = self.filterArray[keyToUse]!;
        self.currentArray = arrayToLoopForSubfilters
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false;
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.separatorStyle = .none
        self.stackViewRight.addArrangedSubview(tableView);
        tableView.heightAnchor.constraint(equalToConstant: self.view.frame.height-20).isActive = true;
    self.setLeadingAndTralingSpaceFormParentView(tableView, parentView:self.stackViewRight);
        tableView.reloadData()
        
        //        cedMageLoaders.addDefaultLoader(me: self)
        //
        //        for (key) in arrayToLoopForSubfilters{
        //            let hiddenLabel = UILabel();
        //            hiddenLabel.translatesAutoresizingMaskIntoConstraints = false;
        //            hiddenLabel.text = key;
        //            self.stackViewRight.addArrangedSubview(hiddenLabel);
        //            hiddenLabel.heightAnchor.constraint(equalToConstant: self.heightOfChildFilterCheckbox).isActive = true;
        //            self.setLeadingAndTralingSpaceFormParentView(hiddenLabel, parentView:self.stackViewRight);
        //            hiddenLabel.isHidden = true;
        //
        //            let checkboxView = productfilterView();
        //            checkboxView.translatesAutoresizingMaskIntoConstraints = false;
        //            checkboxView.labelName.font = UIFont(fontName: "", fontSize: CGFloat(12.0));
        //            checkboxView.labelName.textAlignment = NSTextAlignment.left;
        //            let titleToSet = key.components(separatedBy: "#").last!;
        //
        //            do {
        //                let title = try? NSAttributedString(string: titleToSet, attributes: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType])
        //                checkboxView.labelName.attributedText = title
        //            }
        //            catch {
        //
        //            }
        //
        //           // checkboxView.labelName.text = titleToSet
        //
        //            if let _ = self.selectedFiltersArray[self.currentParentFilter]?.index(of: key) {
        //                checkboxView.checkBoxImage.image = UIImage(named: "CheckedCheckbox");
        //                sender.backgroundColor = self.selectedColor;
        //            }
        //
        //            checkboxView.filterButton.addTarget(self, action: #selector(ProductFiltersViewController.childFilterSelected(_:)), for: UIControlEvents.touchUpInside);
        //            self.stackViewRight.addArrangedSubview(checkboxView);
        //            checkboxView.heightAnchor.constraint(equalToConstant: self.heightOfChildFilterCheckbox).isActive = true;
        //            self.setLeadingAndTralingSpaceFormParentView(checkboxView, parentView:self.stackViewRight);
        //        }
        //        self.scrollViewRight.contentSize = CGSize(width: self.stackViewRight.frame.width, height: self.stackViewRight.frame.height);
        //
        //                cedMageLoaders.removeLoadingIndicator(me: self)
        //
        
        
        
    }
    
    @objc func childFilterSelected(_ sender:UIButton){
        print("childFilterSelected");
        if let checkboxView = sender.superview?.superview as? productfilterView {
            
            var textToUse = "";
            if let index = checkboxView.labelName?.tag {
                
                //print(stackView.arrangedSubviews[index])
//                if let hiddenLabel = self.currentArray[index] as? UILabel {
//                    textToUse = hiddenLabel.text!;
//                }
                textToUse = self.currentArray[index]
            }
            print(textToUse);
            if(checkboxView.checkBoxImage!.image == UIImage(named:"UncheckedCheckbox")){
                checkboxView.checkBoxImage.image = UIImage(named: "CheckedCheckbox");
            }
            else{
                checkboxView.checkBoxImage.image = UIImage(named: "UncheckedCheckbox");
            }
            
            print(selectedFiltersArray);
            print(self.currentParentFilter);
            if let index = selectedFiltersArray[self.currentParentFilter]?.firstIndex(of: textToUse) {
                selectedFiltersArray[self.currentParentFilter]?.remove(at: index);
            }
            else{
                print(textToUse);
                selectedFiltersArray[self.currentParentFilter]?.append(textToUse);
            }
            
            print(stackViewLeft.arrangedSubviews[self.currentParentFilterIndex]);
            
            if let parentFilterButton = stackViewLeft.arrangedSubviews[self.currentParentFilterIndex] as? UIButton {
                if(selectedFiltersArray[self.currentParentFilter]) != nil
                {
                    if((selectedFiltersArray[self.currentParentFilter]?.count)! > 0){
                        parentFilterButton.backgroundColor = selectedColor;
                    }
                    else{
                        parentFilterButton.backgroundColor = currentTappedColor;
                    }
                }
            }
            
        }
    }
    
    @objc func applyFilterButtonPressed(_ sender:UIButton){
        print("applyFilterButtonPressed");
        var filtersToSend = "{";
        for (key,val) in selectedFiltersArray{
            if(val.count == 0){
                continue;
            }
            filtersToSend += "\""+key+"\":{";
            for innerVal in val{
                
                let tempData = innerVal.components(separatedBy: "#");
                filtersToSend += "\""+tempData.first!+"\":"+"\""+tempData.last!+"\",";
            }
            if(filtersToSend.last! == ","){
                filtersToSend = filtersToSend.substring(to: filtersToSend.index(before: filtersToSend.endIndex));
            }
            filtersToSend += "},";
        }
        if(filtersToSend.last! == ","){
            filtersToSend = filtersToSend.substring(to: filtersToSend.index(before: filtersToSend.endIndex));
        }
        filtersToSend += "}";
        
        print("filtersToSend");
        print(filtersToSend);
        
        defaults.set(self.selectedFiltersArray, forKey: "previousSelectedFilters");
        defaults.set(filtersToSend, forKey: "filtersToSend");
        //NotificationCenter.default.post(name:  NSNotification.Name("loadProductsAgainId"), object: nil);
        self.navigationController?.popViewController(completion: {
            self.defColl.reloadData()
        })
    }
    
    @objc func clearFilterButtonPressed(_ sender:UIButton){
       print("clearFilterButtonPressed");
        defaults.removeObject(forKey: "previousSelectedFilters");
        defaults.removeObject(forKey: "filtersToSend");
        NotificationCenter.default.post(name: NSNotification.Name("loadProductsAgainId"), object: nil);
        self.navigationController?.popViewController(completion: {
            self.defColl.reloadData()
        })
    }
    
    
    override func setLeadingAndTralingSpaceFormParentView(_ view:UIView,parentView:UIView){
        parentView.addConstraint(NSLayoutConstraint(item: view, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: parentView, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 0));
        parentView.addConstraint(NSLayoutConstraint(item: view, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: parentView, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 0));
    }
    override func viewDidLayoutSubviews() {
        //super.viewDidLayoutSubviews()
        //scrollView.contentSize = CGSize(width: stackView.frame.width, height: stackView.frame.height)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currentArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        print("parenT")
        print(currentParentFilter)
        let cell = UITableViewCell()
        cell.selectionStyle = .none
        cell.contentView.isMultipleTouchEnabled = true
        let checkboxView = productfilterView();
        checkboxView.translatesAutoresizingMaskIntoConstraints = false;
        checkboxView.labelName.font = UIFont(fontName: "", fontSize: CGFloat(12.0));
        checkboxView.labelName.textAlignment = NSTextAlignment.left;
        checkboxView.labelName.fontColorTool()
        let key = self.currentArray[indexPath.row]
        let titleToSet = key.components(separatedBy: "#").last!;
        checkboxView.labelName.text = titleToSet
        checkboxView.frame = cell.frame
        checkboxView.labelName.tag = indexPath.row
        if let _ = self.selectedFiltersArray[self.currentParentFilter]?.firstIndex(of: key) {
            checkboxView.checkBoxImage.image = UIImage(named: "CheckedCheckbox");
            // sender.backgroundColor = self.selectedColor;
        }
      
        if let parentFilterButton = stackViewLeft.arrangedSubviews[self.currentParentFilterIndex] as? UIButton {
           
            if(selectedFiltersArray[self.currentParentFilter]) != nil
            {
                
                if((selectedFiltersArray[self.currentParentFilter]?.count)! > 0){
                    parentFilterButton.backgroundColor = selectedColor;
                }
                else{
                    parentFilterButton.backgroundColor = currentTappedColor;
                }
            }
        }
        let hiddenLabel = UILabel();
        hiddenLabel.translatesAutoresizingMaskIntoConstraints = false;
        hiddenLabel.text = key;
        hiddenLabel.fontColorTool()
        hiddenLabel.isHidden = true;
          cell.addSubview(hiddenLabel)
        checkboxView.filterButton.addTarget(self, action: #selector(ProductFiltersViewController.childFilterSelected(_:)), for: UIControl.Event.touchUpInside);
        cell.addSubview(checkboxView)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
        
    }
}
