/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */


import UIKit

extension ProductSinglePageViewController{

    @objc func wishlistButtonPressed(_ sender:UIButton){
        print("wishlistButtonPressed");
        var urlToRequest = "";
        if(self.productInfoArray["Inwishlist"] == "OUT"){
            urlToRequest = "mobiconnect/wishlist/add/";
        }
        else{
            urlToRequest = "mobiconnect/wishlist/remove/";
        }
        
        cedMageLoaders.addDefaultLoader(me: self);
        
        if(defaults.object(forKey: "userInfoDict") != nil){
            userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
        }
        else{
            let msg = "Please Login First".localized;
            self.view.makeToast(msg, duration: 2.0, position: .bottom, title: nil, image: nil, style: nil, completion: nil);
            cedMageLoaders.removeLoadingIndicator(me: self)
            return;
        }
        
        let requestHeader = Settings.headerKey;
        let baseURL = Settings.baseUrl
        
        urlToRequest = baseURL+urlToRequest;
        
        var postString = "";
        var postData = [String:String]()
        if(defaults.object(forKey: "userInfoDict") != nil){
            postData["hashkey"]=userInfoDict["hashKey"]!
            postData["customer_id"]=userInfoDict["customerId"]!;
            postData["prodID"]=productInfoArray["product-id"]!;
           // postData["item_id"]=productInfoArray["item_id"]!;
        }
        else{
           postData["prodID"]=productInfoArray["product-id"]!;
            
        }
        if productInfoArray["item_id"] !=  "" {
            postData["item_id"]=productInfoArray["item_id"]!;
        }
        
        postString = ["parameters":postData].convtToJson() as String
        print(self.productInfoArray["Inwishlist"] as Any);
        print(urlToRequest);
        print(postString);
        
        var request = URLRequest(url: URL(string: "\(urlToRequest)")!);
        request.httpMethod = "POST";
        request.httpBody = postString.data(using: String.Encoding.utf8);
        if UserDefaults.standard.bool(forKey: "isLogin"){
            if let user = UserDefaults.standard.object(forKey: "userInfoDict") as? [String:String] {
                request.setValue(user["hashKey"], forHTTPHeaderField: "hashkey")
            }
            
        }
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let task = URLSession.shared.dataTask(with: request){
            
            // check for fundamental networking error
            data, response, error in
            guard error == nil && data != nil else{
                print("error=\(error)")
                DispatchQueue.main.async{
                    print(error?.localizedDescription as Any);
                    cedMageLoaders.removeLoadingIndicator(me: self);
                }
                return;
            }
            
            // check for http errors
            if let httpStatus = response as? HTTPURLResponse , httpStatus.statusCode != 200{
                
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(response)")
                DispatchQueue.main.async{
                    cedMageLoaders.removeLoadingIndicator(me: self);
                    
                }
                return;
            }
            
            // code to fetch values from response :: start
            
            _ = NSString(data: data!, encoding: String.Encoding.utf8.rawValue);
            guard var jsonResponse = try? JSON(data: data!) else{return;}
            jsonResponse = jsonResponse[0]
            if(jsonResponse != nil){
                
                DispatchQueue.main.async{
                    
                    print(jsonResponse);
                    cedMageLoaders.removeLoadingIndicator(me: self);
                    
                    let msg = jsonResponse["message"].stringValue;
                    if(jsonResponse["status"].stringValue == "true"){
                        
                        if(self.productInfoArray["Inwishlist"] == "OUT"){
                            self.productInfoArray["Inwishlist"] = "IN";
                           // self.productImageView.wishlistButton.setImage(UIImage(named:"LikeFilled"), for: UIControl.State.normal);
                            self.productInfoArray["item_id"] = jsonResponse["wishlist-item-id"].stringValue;
                            self.productShareView.wishListLabel.text = "WISHLIST".localized
                            self.productShareView.wishListImage.image = UIImage(named:"LikeFilled")
                        }
                        else{
                            self.productInfoArray["Inwishlist"] = "OUT";
                            self.productShareView.wishListLabel.text = "WISHLIST".localized
                            self.productShareView.wishListImage.image = UIImage(named:"LikeEmpty")
                        }
                    }
                    self.view.makeToast(msg, duration: 2.0, position: .bottom, title: nil, image: nil, style: nil, completion: nil);
                    print(self.productInfoArray["Inwishlist"] as Any);
                }
            }
            
        }
        task.resume();
    }

}
