//
//  sentBoxPopUpView.swift
//  MageNative Magento Platinum
//
//  Created by MacMini on 13/06/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit
//
//class sentBoxReplyPopUp: UIView {
//
//
//    @IBOutlet var myView: UIView!
//    @IBOutlet weak var subjectTextField: UITextField!
//    @IBOutlet weak var messageTextField: UITextField!
//    @IBOutlet weak var sendButton: UIButton!
//
//    override init(frame: CGRect) {
//        super.init(frame: frame)
//        commonInit()
//    }
//
//    required init?(coder aDecoder: NSCoder) {
//        super.init(coder: aDecoder)
//        commonInit()
//    }
//
//    private func commonInit() {
//        Bundle.main.loadNibNamed("sentBoxReplyPopUp", owner: self, options: nil)
//        addSubview(myView)
//        myView.bounds = self.bounds
//        myView.autoresizingMask = [.flexibleWidth,.flexibleHeight]
//    }
//}

class sentBoxReplyPopUp: UIView {

    var view: UIView!

    @IBOutlet weak var loanStack: UIStackView!

    @IBOutlet weak var loanApply: UIButton!

    @IBOutlet weak var loanReset: UIButton!

    override init(frame: CGRect)
    {
        // 1. setup any properties here

        // 2. call super.init(frame:)
        super.init(frame: frame)

        // 3. Setup view from .xib file
        xibSetup()
    }

    required init?(coder aDecoder: NSCoder)
    {
        // 1. setup any properties here

        // 2. call super.init(coder:)
        super.init(coder: aDecoder)

        // 3. Setup view from .xib file
        xibSetup()
    }

    func xibSetup()
    {
        view = loadViewFromNib()

        // use bounds not frame or it'll be offset
        view.frame = bounds

        // Make the view stretch with containing view
      view.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]

        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(view)
    }

    func loadViewFromNib() -> UIView
    {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName:"sentBoxReplyPopUp", bundle: bundle)

        // Assumes UIView is top level and only object in CustomView.xib file
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }


}
