/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */

import UIKit
import AVFoundation
class cedMageVendorShops: MagenativeUIViewController,UIScrollViewDelegate,UIGestureRecognizerDelegate {
    var vendorShopListData = [[String:String]]()
    var countryDict = [[String: String]]();
    var countryList = [String]();
    var banner_url = String()
    var currentPage = 1
    
    //previous popup values
    var stateTextField = "";
    var cityTextField = "";
    var zipCodeTextField = "";
    var vendorNameTextField = "";
    var selectedCountry = "";
    var selectedState = "";
    //previous popup values
    var stateDict = [[String: String]]();
    var stateList = [String]();
    var pickStateFromButton = false;
    var selectedCountryCode = String();
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.basicFoundationToRenderView(topMargin: 5)
        scrollView.delegate = self;
        let postString = ["page":String(self.currentPage)];
        self.sendRequest(url: "vendorapi/index/item", params: postString,store: false,buyer:false)
        self.getRequest(url: "mobiconnect/module/getcountry/",store: false)
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = false
    }
    func addgesturesTohideView(v:UIView)
    {
        //code to add gestures to dismiss the popover
        let tapGesture = UITapGestureRecognizer (target: self, action:#selector(cedMageVendorShops.hideView(_:)))
        v.addGestureRecognizer(tapGesture);
        tapGesture.delegate=self;
        
        let upSwipe = UISwipeGestureRecognizer(target: self, action: #selector(cedMageVendorShops.hideView(_:)));
        upSwipe.direction = .up;
        v.addGestureRecognizer(upSwipe)
        upSwipe.delegate=self;
        
        let downSwipe = UISwipeGestureRecognizer(target: self, action: #selector(cedMageVendorShops.hideView(_:)));
        downSwipe.direction = .down;
        v.addGestureRecognizer(downSwipe)
        downSwipe.delegate=self;
        
        
        let rightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(cedMageVendorShops.hideView(_:)));
        rightSwipe.direction = .right;
        v.addGestureRecognizer(rightSwipe)
        rightSwipe.delegate=self;
        
        let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(cedMageVendorShops.hideView(_:)));
        leftSwipe.direction = .left;
        v.addGestureRecognizer(leftSwipe)
        leftSwipe.delegate=self;
    }
    
    //function to dismiss the custom popover
    @objc func hideView(_ recognizer: UITapGestureRecognizer)
    {
   
        UIApplication.shared.keyWindow?.viewWithTag(121)?.removeFromSuperview();
    }
    
    //delegate function to handle touch events on the custom popover
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool
    {
        if let innerView = UIApplication.shared.keyWindow?.viewWithTag(131) as? SearchPopupView {
            if(touch.view!.isDescendant(of: innerView))
            {
                return false;
            }
            return true;
        }
        return true;
    }

    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func renderVendorPage(){
        stackView.subviews.forEach({ $0.removeFromSuperview() });
        let banner = bannerCell()
        let bannerHeight = translateAccordingToDevice(225)
        banner.heightAnchor.constraint(equalToConstant: bannerHeight).isActive = true;
        banner.dataSource = [banner_url]
        banner.parent = self
        //banner.fullData = nil
        stackView.addArrangedSubview(banner)
        self.setLeadingAndTralingSpaceFormParentView(banner, parentView: stackView, padding: 5)
        
        self.makeSomeSpaceInStackView(height: 5)
        let sortView = cedMageVendorTopView()
        sortView.filterButton.addTarget(self, action: #selector(cedMageVendorShops.vendorSearch(_:)), for: .touchUpInside)
        sortView.sort.addTarget(self, action: #selector(cedMageVendorShops.performSortingByShopName(_:)), for: .touchUpInside)
        sortView.heightAnchor.constraint(equalToConstant: 45).isActive = true;
        stackView.addArrangedSubview(sortView)
        self.setLeadingAndTralingSpaceFormParentView(sortView, parentView: stackView, padding: 5)
        
        self.makeSomeSpaceInStackView(height: 5)
        if vendorShopListData.count > 0{
            
            for vendorShop in vendorShopListData {
                 self.makeSomeSpaceInStackView(height: 5)
                let shopView = cedMageVendorView()
                if let entityId = vendorShop["entity_id"]{
                    if let tag = Int(entityId){
                        
                        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(cedMageVendorShops.shopVendorClicked(_:)));
                        shopView.tag = tag
                        shopView.addGestureRecognizer(tapGesture)
                    }
                    
                }
                if let shopUrl = vendorShop["company_banner"]{
                    cedMageImageLoader().loadImgFromUrl(urlString: shopUrl, completionHandler: {
                        image,url in
                        if let image = image {
                            shopView.vendorShopImage.image = image
                        }
                    })
                }
                shopView.heightAnchor.constraint(equalToConstant: 220).isActive = true
                if let shopname = vendorShop["public_name"]{
                    shopView.shopTitle.text = shopname
                }
                if let shopCity = vendorShop["city"]{
                    var datastring = shopCity + " ,"
                    if let review = vendorShop["vendor_review_count"]{
                        datastring += "\n" + review + " Reviews".localized
                        shopView.address.text = datastring
                    }
                }
                if let rating = vendorShop["vendor_review"]{
                    shopView.starRating.text = rating
                }
                stackView.addArrangedSubview(shopView)
                self.setLeadingAndTralingSpaceFormParentView(shopView, parentView: stackView, padding: 5)
            }
            
            
        }
    }
    
    @objc func vendorSearch(_ sender:UIButton){
        /* background view */
        let bgCView = UIView();
        bgCView.tag=121;
        bgCView.frame = UIScreen.main.bounds;
        bgCView.autoresizingMask = [UIView.AutoresizingMask.flexibleHeight , UIView.AutoresizingMask.flexibleWidth];
        bgCView.backgroundColor = UIColor(red:0, green:0, blue:0, alpha:0.5);
        
        self.addgesturesTohideView(v: bgCView);
        
        
        let searchPopupView = SearchPopupView();
        searchPopupView.tag = 131;
        searchPopupView.autoresizingMask = [UIView.AutoresizingMask.flexibleLeftMargin,UIView.AutoresizingMask.flexibleRightMargin];
        
        searchPopupView.frame = CGRect(x:0, y:CGFloat(0),width:screenSize.width-50,height:375);
        
        searchPopupView.center = CGPoint(x:bgCView.frame.size.width  / 2,
                                         y:bgCView.frame.size.height / 2);
        
        searchPopupView.logoImg.image = UIImage(named: "login-logo");
        
        searchPopupView.topText.text = "Search Vendors".localized;
        searchPopupView.stateButton.isHidden = true;
        searchPopupView.stateDownImg.isHidden = true;
        searchPopupView.stateTextField.isHidden = false;
       
        searchPopupView.stateTextField.placeholder = "State".localized;
        if(stateTextField != "")
        {
            searchPopupView.stateTextField.text =  stateTextField;
        }
        
        searchPopupView.cityTextField.placeholder = "City".localized;
        if(cityTextField != "")
        {
            searchPopupView.cityTextField.text =  cityTextField;
        }
        
        searchPopupView.zipCodeTextField.placeholder = "Zip/Postal Code".localized;
        if(zipCodeTextField != "")
        {
            searchPopupView.zipCodeTextField.text =  zipCodeTextField;
        }
        
        searchPopupView.vendorNameTextField.placeholder = "Vendor Name".localized;
        if(vendorNameTextField != "")
        {
            searchPopupView.vendorNameTextField.text =  vendorNameTextField;
        }
        
        searchPopupView.countryDropdown.addTarget(self, action: #selector(cedMageVendorShops.showCountryDropdownFunction(_:)), for: UIControl.Event.touchUpInside);
        if(selectedCountry == "")
        {
            searchPopupView.countryDropdown.setTitle("Country".localized, for: UIControl.State.normal);
        }
        else
        {
            searchPopupView.countryDropdown.setTitle(selectedCountry, for: UIControl.State.normal);
        }
        
        searchPopupView.stateButton.addTarget(self, action: #selector(cedMageVendorShops.showStateDropdownFunction(_:)), for: UIControl.Event.touchUpInside);
        searchPopupView.stateButton.setTitle("State".localized, for: UIControl.State.normal);
        
        searchPopupView.submitButton.addTarget(self, action: #selector(cedMageVendorShops.searchVendorShopFunction(_:)), for: UIControl.Event.touchUpInside);
        searchPopupView.submitButton.setTitle("Submit".localized.uppercased(), for: UIControl.State.normal);
        searchPopupView.submitButton.setThemeColor()
        
        bgCView.addSubview(searchPopupView);
        //self.view.addSubview(bgCView);
        
        UIApplication.shared.keyWindow?.addSubview(bgCView);
        
        if(countryList.count == 0)
        {
            self.getRequest(url: "mobiconnect/module/getcountry/",store: false)
        }
    }
    
    
    
    override func recieveResponse(data: Data?, requestUrl: String?, response: URLResponse?) {
        if let responseData = data {
            do {
                let jsonData = try JSON(data:responseData)
                print(jsonData)
                if requestUrl == "mobiconnect/module/getcountry/"{
                    let countryListing = jsonData[0]["country"].arrayValue;
                    for country in countryListing
                    {
                        let key = country["value"].stringValue;
                        
                        let value = country["label"].stringValue;
                        let v = ["key":key,"value":value];
                        self.countryDict.append(v);
                        self.countryList.append(value);
                    }
                    
                    print("countryDict");
                    print(self.countryDict);
                    print("countryDict");
                }else {
                    if(jsonData[0]["data"]["success"].stringValue == "true")
                    {
                        self.banner_url = jsonData[0]["data"]["banner_url"].stringValue;
                        
                        print(jsonData);
                        
                        for sellers in jsonData[0]["data"]["sellers"].arrayValue
                        {
                            var tempArray = [String:String]();
                            tempArray["entity_id"] = sellers["entity_id"].stringValue;
                            tempArray["public_name"] = sellers["public_name"].stringValue;
                            tempArray["company_banner"] = sellers["company_banner"].stringValue;
                            tempArray["store_id"] = sellers["store_id"].stringValue;
                            tempArray["vendor_review_count"] = sellers["vendor_review_count"].stringValue;
                            tempArray["vendor_review"] = sellers["vendor_review"].stringValue;
                            
                            if(sellers["city"].exists())
                            {
                                tempArray["city"] = sellers["city"].stringValue;
                            }
                            else
                            {
                                tempArray["city"] = "";
                            }
                            
                            if(sellers["country_id"].exists())
                            {
                                print("sellersstringValue");
                                print(sellers["country_id"].stringValue);
                                
                                let countryCode = sellers["country_id"].stringValue;
                                for (data) in self.countryDict
                                {
                                    print(data["key"] as Any);
                                    if(data["key"]! == countryCode)
                                    {
                                        tempArray["country_id"] = data["value"]!;
                                        break;
                                    }
                                    else
                                    {
                                        tempArray["country_id"] = countryCode;
                                    }
                                }
                                tempArray["country_id"] = sellers["country_id"].stringValue;
                            }
                            else
                            {
                                tempArray["country_id"] = "";
                            }
                            self.vendorShopListData.append(tempArray);
                        }
                        self.renderVendorPage()
                    }
                    else{
                        self.view.makeToast(jsonData[0]["data"]["message"].stringValue, duration: 2.0, position: .bottom)
                    }
                }
            } catch let error {
                print(error.localizedDescription)
            }
        }
    }
    
    
    @objc func shopVendorClicked(_ recognizer: UITapGestureRecognizer){
        if let buyerprofile = UIStoryboard(name: "categorylayouts", bundle: nil).instantiateViewController(withIdentifier: "cedMageBuyerProfile") as? cedMageBuyerProfile{
            buyerprofile.selectedId = String(describing: recognizer.view!.tag)
            self.navigationController?.pushViewController(buyerprofile, animated: true)
        }
    }
    
    
    @objc func showCountryDropdownFunction(_ sender: UIButton)
    {
        dropDown.dataSource = countryList;
        dropDown.selectionAction = {(index, item) in
            sender.setTitle(item, for: .normal);
            self.fectchStateListing(selectedCountry: item);
        }
        
        dropDown.anchorView = sender
        dropDown.bottomOffset = CGPoint(x: 0, y:sender.bounds.height)
        
        if dropDown.isHidden {
            _=dropDown.show();
        } else {
            dropDown.hide();
        }
    }
    
    
    @objc  func showStateDropdownFunction(_ sender: UIButton)
    {
        dropDown.dataSource = stateList;
        dropDown.selectionAction = {(index, item) in
            sender.setTitle(item, for: .normal);
        }
        
        dropDown.anchorView = sender
        dropDown.bottomOffset = CGPoint(x: 0, y:sender.bounds.height)
        
        if dropDown.isHidden {
            _=dropDown.show();
        } else {
            dropDown.hide();
        }
    }
    
    func fectchStateListing( selectedCountry:String)
    {
        //Alert_File.addLoadingIndicator(self, msg: "Loading..")
        var selectedCountry = selectedCountry
        self.stateDict = [[String: String]]();
        self.stateList = [String]();
        
        selectedCountry = selectedCountry.trimmingCharacters(in: .whitespacesAndNewlines)
        
        if(countryDict.count == 0){
            //Alert_File.removeLoadingIndicator(self)
            return
        }
        
        for (data) in countryDict
        {
            if(data["value"] == selectedCountry)
            {
                selectedCountry = data["key"]!;
                self.selectedCountryCode = selectedCountry;
                break;
            }
        }
        
        var baseURL = Settings.baseUrl//cedMage.getInfoPlist(fileName:"cedMage",indexString: "cedBaseUrl") as! String
        baseURL += "mobiconnect/module/getcountry/" + selectedCountry
        var request = URLRequest(url: URL(string: baseURL)!);
        
        let requestHeader = cedMage.getInfoPlist(fileName:"cedMage",indexString: "requestheader") as! String
        request.setValue(requestHeader, forHTTPHeaderField: "Mobiconnectheader")
 
        let task = URLSession.shared.dataTask(with: request)
        {
            // check for fundamental networking error
            data, response, error in
            
            // check for http errors
            if let httpStatus = response as? HTTPURLResponse , httpStatus.statusCode != 200
            {
                
                DispatchQueue.main.async
                    {
                        cedMageLoaders.removeLoadingIndicator(me: self)
                        cedMageHttpException.showHttpErrorImage(me: self, img: "no_module")
                        print("statusCode should be 200, but is \(httpStatus.statusCode)")
                        
                        print("response = \(response)")
                }
                return;
            }
            
            // code to fetch values from response :: start
            
            
            guard error == nil && data != nil else
            {
                DispatchQueue.main.async
                    {
                        cedMageLoaders.removeLoadingIndicator(me: self)
                        cedMageHttpException.showAlertView(me: self, msg: error?.localizedDescription, title: "Error".localized )
                        print("error=\(error)")
                }
                return ;
            }
            
            DispatchQueue.main.async
                {
                    
                    do {
                        let json = try JSON(data:data!)
                        let stateListing = json[0]["states"].arrayValue;
                        for state in stateListing
                        {
                            let key = state["region_id"].stringValue;
                            
                            let value = state["default_name"].stringValue;
                            
                            let v = ["key":key,"value":value];
                            self.stateDict.append(v);
                            self.stateList.append(value);
                        }
                        print(self.stateDict)
                        if(self.stateDict.count != 0)
                        {
                            self.pickStateFromButton = true;
                            if let searchPopupView = UIApplication.shared.keyWindow?.viewWithTag(131) as? SearchPopupView
                            {
                                searchPopupView.stateButton.isHidden = false;
                                searchPopupView.stateDownImg.isHidden = false;
                                searchPopupView.stateTextField.isHidden = true;
                            }
                        }
                        else
                        {
                            self.pickStateFromButton = false;
                            if let searchPopupView = UIApplication.shared.keyWindow?.viewWithTag(131) as? SearchPopupView
                            {
                                searchPopupView.stateButton.isHidden = true;
                                searchPopupView.stateDownImg.isHidden = true;
                                searchPopupView.stateTextField.isHidden = false;
                            }
                        }
                    } catch let error {
                        print(error.localizedDescription)
                    }
                    
                    

            }
        }
        task.resume();
    }
    

    @objc func searchVendorShopFunction(_ sender:UIButton){

        if let searchPopupView = UIApplication.shared.keyWindow?.viewWithTag(131) as? SearchPopupView
        {
            let selectedCountry = searchPopupView.countryDropdown.titleLabel?.text!;
            self.selectedCountry = selectedCountry!;
            
            var selectedState = String();
            if(self.pickStateFromButton)
            {
                selectedState = (searchPopupView.stateButton.titleLabel?.text!)!;
                self.selectedState = selectedState;
            }
            else
            {
                selectedState = searchPopupView.stateTextField.text!;
            }
            
            let selectedCity = searchPopupView.cityTextField.text!;
            let selectedZipCode = searchPopupView.zipCodeTextField.text!;
            let selectedVendorName = searchPopupView.vendorNameTextField.text!;
            
            if(self.selectedCountryCode != "")
            {
                
            }
            stateTextField = selectedState;
            cityTextField = selectedCity;
            zipCodeTextField = selectedZipCode;
            vendorNameTextField = selectedVendorName;
      
            var sellersearch = "{";
            sellersearch += "\"state\":\""+selectedState+"\",";
            sellersearch += "\"zip\":\""+selectedZipCode+"\",";
            sellersearch += "\"vendorname\":\""+selectedVendorName+"\",";
            sellersearch += "\"estimate_city\":\""+selectedCity+"\",";
            sellersearch += "\"country\":\""+self.selectedCountryCode+"\"";
            sellersearch += "}";
        
            let postString = ["sellersearch":sellersearch];
        UIApplication.shared.keyWindow?.viewWithTag(121)?.removeFromSuperview();
            vendorShopListData.removeAll()
            self.sendRequest(url: "vendorapi/index/item/", params: postString, buyer:true)

        }
        

    }
    
    @objc func performSortingByShopName(_ sender:UIButton)
    {
        if(sender.titleLabel?.text == "A TO Z".localized)
        {
            sender.setTitle("Z TO A".localized, for: .normal)
            let sortedResults = vendorShopListData.sorted{
                (dictOne, dictTwo) -> Bool in
         
                return Int((dictOne["public_name"]?.lowercased())!)! > Int((dictTwo["public_name"]?.lowercased())!)!
            };
        
            self.vendorShopListData = sortedResults;
            self.renderVendorPage()
        }
        else if(sender.titleLabel?.text == "Z TO A")
        {
              sender.setTitle("A TO Z", for: .normal)
            let sortedResults = vendorShopListData.sorted{
                (dictOne, dictTwo) -> Bool in
                return Int((dictOne["public_name"]?.lowercased())!)! > Int((dictTwo["public_name"]?.lowercased())!)!
            };
            
            self.vendorShopListData = sortedResults;
            self.renderVendorPage()
            
        }
    }

    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        print("didEndDragging")
        
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        
        if (maximumOffset - currentOffset) <= 40 {
            currentPage += 1
            let postData = ["page":String(self.currentPage)]
            self.sendRequest(url: "vendorapi/index/item", params: postData,buyer:true)
        }
        
    }
    
    
    
}
