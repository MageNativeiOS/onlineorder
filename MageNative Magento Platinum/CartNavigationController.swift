//
//  CartNavigationController.swift
//  MageNative Magento Platinum
//
//  Created by Hamza Usmani on 27/01/21.
//  Copyright © 2021 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class CartNavigationController: homedefaultNavigation {

    override func viewDidLoad() {
        super.viewDidLoad()

        let cartController = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "cartViewController") as! CartViewController
        setViewControllers([cartController], animated: true)
    }
    

   

}
