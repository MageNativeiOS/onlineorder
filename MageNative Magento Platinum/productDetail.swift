/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */

import UIKit

class productDetail: UIView {

    @IBOutlet weak var productView1: UIView!
    
    @IBOutlet weak var productView2: UIView!
    @IBOutlet weak var imageView1: UIImageView!
    
    @IBOutlet weak var productName1: UILabel!
    
    @IBOutlet weak var wishList1: UIButton!
    
    @IBOutlet weak var starView1: UIView!
    
    @IBOutlet weak var starFilled1: UIImageView!
    @IBOutlet weak var rating1: UILabel!
   
    
    
    @IBOutlet weak var imageView2: UIImageView!
    
    @IBOutlet weak var productName2: UILabel!
    
    @IBOutlet weak var wishList2: UIButton!
    
    @IBOutlet weak var starView2: UIView!
    
    @IBOutlet weak var starFilled2: UIImageView!
    @IBOutlet weak var rating2: UILabel!
    
    var view :UIView!
    var dataSource = [[String:String]]()
    override init(frame: CGRect)
    {
        // 1. setup any properties here
        
        // 2. call super.init(frame:)
        super.init(frame: frame)
        
        // 3. Setup view from .xib file
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        // 1. setup any properties here
        
        // 2. call super.init(coder:)
        super.init(coder: aDecoder)
        
        // 3. Setup view from .xib file
        xibSetup()
    }
    
    func xibSetup()
    {
        view = loadViewFromNib()
        
        // use bounds not frame or it'll be offset
        view.frame = bounds
        
        // Make the view stretch with containing view
        view.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]

        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(view);
    }
    
    func loadViewFromNib() -> UIView
    {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "productViewGrid", bundle: bundle)
        
        // Assumes UIView is top level and only object in CustomView.xib file
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    
  
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "headercell", for: indexPath) as! headerCollect
        cedMageImageLoader().loadImgFromUrl(urlString: dataSource[indexPath.row]["product_image"]!, completionHandler: {
            image,url in
            cell.imageView.image = image
        })
        return cell

    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width/2, height: 200)
    }

}
