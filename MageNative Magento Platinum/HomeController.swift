//
//  HomeController.swift
//  MageNative Magento Platinum
//
//  Created by Hamza Usmani on 02/09/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class HomeController: MagenativeUIViewController {
    
    //MARK:- Properties
    
    var homeData = [HomeDataModel]()
    lazy var addressView:UIView = {
        let view = UIView()
        view.backgroundColor = .yellow
        
        let image = UIImageView(image: UIImage(named: "location_icon"))
        image.setDimensions(width: 20, height: 20)
        view.addSubview(image)
        image.anchor(left: view.leadingAnchor, paddingLeft: 8)
        image.centerY(inView: view)
        
        let label = UILabel()
        label.text = Settings.currentLocation?["fullAddress"]
        label.font = UIFont(fontName: "Avenir", fontSize: 12)
        label.numberOfLines = 0
        
        view.addSubview(label)
        label.anchor(top: view.topAnchor, left: image.trailingAnchor, bottom: view.bottomAnchor, right: view.trailingAnchor, paddingLeft: 8, paddingRight: 8)
        
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(showMapController)))
        
        return view
    }()
    
    
    lazy var homeTable:UITableView = {
        let table = UITableView()
        table.separatorStyle = .none
        table.register(HomeDealTC.self, forCellReuseIdentifier: HomeDealTC.reuseId)
        table.register(HomeSearchTC.self, forCellReuseIdentifier: HomeSearchTC.reuseId)
        table.register(HomeBannerTC.self, forCellReuseIdentifier: HomeBannerTC.reuseId)
        table.register(HomeProductSliderTC.self, forCellReuseIdentifier: HomeProductSliderTC.reuseId)
        table.register(HomeSliderCategoryTC.self, forCellReuseIdentifier: HomeSliderCategoryTC.reuseId)
        table.delegate = self
        table.dataSource = self
        return table
    }()
    
    //MARK:- Lifecycles
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden=false
        navigationController?.navigationBar.isHidden=false
        redirectPushNotification()
        configureUI()
        fetchData()
    }
    @objc private func showMapController() {
        let vc = MapController()
        vc.modalPresentationStyle = .fullScreen
        present(vc, animated: true)
    }
  
    
    
    fileprivate func redirectPushNotification(){
        let seconds = 3.0
        DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
            if UserDefaults.standard.value(forKey: "MageNotificationData") != nil
            {
                let data=UserDefaults.standard.value(forKey: "MageNotificationData") as! [String:Any]
                UserDefaults.standard.removeObject(forKey: "MageNotificationData")
                
                //let notificationData = UserDefaults.standard.value(forKey: "NotificationData") as! [String:String]
                let linkType = data["link_type"] as! String
                if (linkType == "1"){
                    let vc = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "productSinglePageViewController") as! ProductSinglePageViewController
                    vc.product_id = data["link_id"]! as! String
                    self.navigationController?.pushViewController(vc, animated: true)
                    /*let productview:ProductViewController = self.storyboard!.instantiateViewController()
                    self.productId = notificationData["product"]
                    productview.productId = self.productId!
                    productview.isProductLoading = true;
                    self.navigationController?.pushViewController(productview
                        , animated: true)*/
                    
                }else  if (linkType == "2"){
                    let link_id = data["link_id"] as! String
                    /*let coll = collection(id: link_id, title: "collection")
                    
                    let viewControl:ProductListViewController = self.storyboard!.instantiateViewController()
                    viewControl.isfromHome = true
                    
                    viewControl.collect = coll
                    
                    self.navigationController?.pushViewController(viewControl, animated: true)*/
                    let vc = UIStoryboard(name: "categorylayouts", bundle: nil).instantiateViewController(withIdentifier: "cedMageDefaultCollection") as! cedMageDefaultCollection
                    vc.selectedCategory = link_id
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                }else  if (linkType == "3"){
                    let view = UIStoryboard(name: "cedMageAccounts", bundle: nil)
                    let vc = view.instantiateViewController(withIdentifier: "cmsWebView") as! cedMageCmsWebView
                    
                    vc.pageUrl = data["link_id"] as! String
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                    
                }
            }
        }
    }
    
    //MARK:- API's
    
    fileprivate func fetchData() {
        HomeService.shared.getHomepageData(controller: self, page: "1", completion: { result in
            guard let result    = result else { return }
            self.homeData       = result
                        
            DispatchQueue.main.async {
                self.homeTable.reloadData()
            }
        })
    }
    
    //MARK:- Selectors
    
    @objc private func handleCustomerSupport() {
        let vc = UIStoryboard(name: "cedMageAccounts", bundle: nil).instantiateViewController(withIdentifier: "cmsWebView") as! cedMageCmsWebView
        vc.pageUrl = "https://cedcommerce.com/magenativeapp.html?name=Magenativemagento2"
        navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK:- Helper Functions
    
    fileprivate func configureUI() {
        view.addSubview(addressView)
       addressView.anchor(top: view.safeAreaLayoutGuide.topAnchor, left: view.leadingAnchor, right: view.trailingAnchor, height: 40)
        view.addSubview(homeTable)
      
        homeTable.anchor(top: view.safeAreaLayoutGuide.topAnchor, left: view.leadingAnchor, bottom: view.bottomAnchor, right: view.trailingAnchor, paddingTop: 40, paddingLeft: 0, paddingBottom: 0, paddingRight: 0)
    }
    
    
}

//MARK:- UITableViewDataSource

extension HomeController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return homeData.count + 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        default:
            if(homeData[section-1].link_type == "banner_slider"){
                if(homeData[section-1].banner_orientiation == "vertical"){
                    return homeData[section-1].widget?.count ?? 0
                }
            }
            return 1
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: HomeSearchTC.reuseId, for: indexPath) as! HomeSearchTC
            cell.populate(with: self)
            return cell
        default:
            let data = homeData[indexPath.section-1]
            switch homeData[indexPath.section - 1].link_type {
            case "banner_slider":
                let cell = tableView.dequeueReusableCell(withIdentifier: HomeBannerTC.reuseId, for: indexPath) as! HomeBannerTC
                if(data.banner_orientiation == "vertical"){
                    cell.populate(with: [data.widget![indexPath.row]], isMediumBanner: false, parent: self)
                }
                else{
                    cell.populate(with: data.widget ?? [], isMediumBanner: false, parent: self)
                }
                return cell
            case "animation_banner":
                let cell = tableView.dequeueReusableCell(withIdentifier: HomeBannerTC.reuseId, for: indexPath) as! HomeBannerTC
                cell.populate(with: [data.banner!] , isMediumBanner: true, parent: self)
                return cell
            case "category_slider":
                let cell = tableView.dequeueReusableCell(withIdentifier: HomeSliderCategoryTC.reuseId, for: indexPath) as! HomeSliderCategoryTC
                cell.populate(with: data.categories ?? [], categoryType: data.category_type, parent: self)
                return cell
            case "products_slider":
                let cell = tableView.dequeueReusableCell(withIdentifier: HomeProductSliderTC.reuseId, for: indexPath) as! HomeProductSliderTC
                cell.populate(with: data.products ?? [], categoryId: "", headingText: data.title ?? "", parent: self)
                return cell
            case "dealgroup_slider":
                let cell = tableView.dequeueReusableCell(withIdentifier: HomeDealTC.reuseId, for: indexPath) as! HomeDealTC
                cell.populate(with: data.deal_group?.content ?? [], headingText: data.deal_group?.title ?? "", parent: self, dur: data.deal_group?.deal_duration ?? 0)
                return cell
            default:
                return .init()
            }
        }
    }
}

//MARK:- UITableViewDelegate

extension HomeController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return 50
        default:
            switch homeData[indexPath.section-1].link_type {
            case "banner_slider":
                return 200
            case "animation_banner":
                switch homeData[indexPath.section-1].type_of_banner {
                case "small_size":
                    return 140
                case "medium_size":
                    return 200
                default:
                    return 230
                }
            case "category_slider":
                return 150
            case "products_slider":
                return 280 + 26
            case "dealgroup_slider":
                return 160
            default:
                return 0
            }
        }
    }
}
