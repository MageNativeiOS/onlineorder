/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */


import UIKit

class ProductDetailDescriptionViewController: cedMageViewController, WKNavigationDelegate {

    var productDetail = "";
    var productDescription = "";
    var cmsPage = ""
    var cmsUrl = ""
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    //var segmentedControl : UISegmentedControl!;
    var productDetailWebView : WKWebView!;
    var productDescriptionWebView : WKWebView!;
    var cmsPageWebView : WKWebView!;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        let color  = cedMage.getInfoPlist(fileName: "cedMage", indexString: "themeColor") as! String
        segmentedControl.tintColor = cedMage.UIColorFromRGB(colorCode: color)
        segmentedControl.setTitle("First".localized, forSegmentAt: 0)
        segmentedControl.setTitle("Second".localized, forSegmentAt: 1)
        renderProductDetailDescriptionPage();
        
    }

    func renderProductDetailDescriptionPage(){
        
        // Initialize UISegmentedControl
        var items = ["Detail".localized, "Description".localized];
        if cmsPage != "" {
            items.append(cmsPage)
            segmentedControl.insertSegment(withTitle: items[2], at: 0, animated: true);
        }
        //let segmentedControl = UISegmentedControl(items: items);
        segmentedControl.removeAllSegments();
        
        segmentedControl.insertSegment(withTitle: items[1], at: 0, animated: true);
        segmentedControl.insertSegment(withTitle: items[0], at: 0, animated: true);
        
        segmentedControl.translatesAutoresizingMaskIntoConstraints = false;
        
        //let attr = NSDictionary(object: UIFont(name: "HelveticaNeue", size: 15.0)!, forKey: NSFontAttributeName);
        //segmentedControl.setTitleTextAttributes(attr as [NSObject : AnyObject] , for: UIControlState());
        
        // Add target action method
        segmentedControl.addTarget(self, action: #selector(ProductDetailDescriptionViewController.segmentedControlChanged(_:)), for: .valueChanged);

        segmentedControl.selectedSegmentIndex = 0;
        //self.view.addSubview(segmentedControl);
        
        // add layout constraints
        /*self.view.addConstraint(NSLayoutConstraint(item: segmentedControl, attribute: NSLayoutAttribute.leading, relatedBy: NSLayoutRelation.equal, toItem: self.view, attribute: NSLayoutAttribute.leading, multiplier: 1, constant: 5));
        self.view.addConstraint(NSLayoutConstraint(item: segmentedControl, attribute: NSLayoutAttribute.trailing, relatedBy: NSLayoutRelation.equal, toItem: self.view, attribute: NSLayoutAttribute.trailing, multiplier: 1, constant: -5));
        self.view.addConstraint(NSLayoutConstraint(item: segmentedControl, attribute: NSLayoutAttribute.top, relatedBy: NSLayoutRelation.equal, toItem: self.view, attribute: NSLayoutAttribute.top, multiplier: 1, constant: 5));
        self.view.addConstraint(NSLayoutConstraint(item: segmentedControl, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 40));
        */
        productDetailWebView = WKWebView();
        productDetailWebView.translatesAutoresizingMaskIntoConstraints = false;
        productDetailWebView.navigationDelegate = self;
        self.view.addSubview(productDetailWebView);
        
        // add layout constraints
        self.view.addConstraint(NSLayoutConstraint(item: productDetailWebView, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 5));
        self.view.addConstraint(NSLayoutConstraint(item: productDetailWebView, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: -5));
        self.view.addConstraint(NSLayoutConstraint(item: productDetailWebView, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: segmentedControl, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: 5));
        self.view.addConstraint(NSLayoutConstraint(item: productDetailWebView, attribute: NSLayoutConstraint.Attribute.bottom, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: -5));
        
        
        productDescriptionWebView = WKWebView();
        productDescriptionWebView.translatesAutoresizingMaskIntoConstraints = false;
        productDescriptionWebView.navigationDelegate = self;
        self.view.addSubview(productDescriptionWebView);
        
        // add layout constraints
        self.view.addConstraint(NSLayoutConstraint(item: productDescriptionWebView, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 5));
        self.view.addConstraint(NSLayoutConstraint(item: productDescriptionWebView, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: -5));
        self.view.addConstraint(NSLayoutConstraint(item: productDescriptionWebView, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: segmentedControl, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: 5));
        self.view.addConstraint(NSLayoutConstraint(item: productDescriptionWebView, attribute: NSLayoutConstraint.Attribute.bottom, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: -5));
        
        
        cmsPageWebView = WKWebView();
        cmsPageWebView.translatesAutoresizingMaskIntoConstraints = false;
        cmsPageWebView.navigationDelegate = self;
        self.view.addSubview(cmsPageWebView);
        
        // add layout constraints
        self.view.addConstraint(NSLayoutConstraint(item: cmsPageWebView, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 5));
        self.view.addConstraint(NSLayoutConstraint(item: cmsPageWebView, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: -5));
        self.view.addConstraint(NSLayoutConstraint(item: cmsPageWebView, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: segmentedControl, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: 5));
        self.view.addConstraint(NSLayoutConstraint(item: cmsPageWebView, attribute: NSLayoutConstraint.Attribute.bottom, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: -5));
        
        if(cmsUrl != ""){
        let request = URLRequest(url: URL(string: cmsUrl)!)
        self.cmsPageWebView.load(request)
            if #available(iOS 13.0, *) {
                //self.cmsPageWebView.backgroundColor = UIColor.systemBackground
                self.productDetailWebView.backgroundColor = UIColor.systemBackground;
                self.productDescriptionWebView.backgroundColor = UIColor.systemBackground;
            } else {
                //self.cmsPageWebView.backgroundColor = .white
                self.productDescriptionWebView.backgroundColor = UIColor.white;
                self.productDetailWebView.backgroundColor = UIColor.white;
            };
        }
        self.productDetailWebView.loadHTMLString("<html><head><meta name='viewport' content='width=device-width,initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0'><link rel='stylesheet' type='text/css' href='webfile.css'><source media='(prefers-color-scheme: dark)'><source media='(prefers-color-scheme: dark)'></head><body>\(productDetail)</body></html>", baseURL: URL(fileURLWithPath: Bundle.main.path(forResource: "webfile", ofType: "css") ?? "" ));
        self.productDescriptionWebView.loadHTMLString("<html><head><meta name='viewport' content='width=device-width,initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0'><link rel='stylesheet' type='text/css' href='webfile.css'><source media='(prefers-color-scheme: dark)'><source media='(prefers-color-scheme: dark)'></head><body>\(productDescription)</body></html>", baseURL: URL(fileURLWithPath: Bundle.main.path(forResource: "webfile", ofType: "css") ?? "" ));
        productDetailWebView.isHidden = false;
        productDescriptionWebView.isHidden = true;
        cmsPageWebView.isHidden = true
    }
    
    @objc func segmentedControlChanged(_ sender: UISegmentedControl){
        switch sender.selectedSegmentIndex {
        case 0:
            productDetailWebView.isHidden = false;
            productDescriptionWebView.isHidden = true;
            cmsPageWebView.isHidden = true
            
        case 1:
            productDetailWebView.isHidden = true;
            productDescriptionWebView.isHidden = false;
            cmsPageWebView.isHidden = true
        case 2:
            productDetailWebView.isHidden = true;
            productDescriptionWebView.isHidden = true
            cmsPageWebView.isHidden = false
            
        default:
            productDetailWebView.isHidden = false;
            productDescriptionWebView.isHidden = true;
            cmsPageWebView.isHidden = true
        }
    }
    
    
    
    
    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
        print("Start loading")
        var activityIndicator = UIActivityIndicatorView();
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false;
        activityIndicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.whiteLarge);
        activityIndicator.frame = CGRect(x: 0, y: 0, width: 50, height: 50);
        activityIndicator.startAnimating();
        activityIndicator.color = UIColor.black;
        activityIndicator.center.x = webView.bounds.width/2;
        activityIndicator.center.y = webView.bounds.height/2;
        webView.addSubview(activityIndicator);
    }

    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        for view in webView.subviews as [UIView] {
            if let activityIndicator = view as? UIActivityIndicatorView {
                activityIndicator.removeFromSuperview();
            }
        }
    }

    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
