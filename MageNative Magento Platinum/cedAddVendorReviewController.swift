/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */

import UIKit
class cedAddVendorReviewController: MagenativeUIViewController,UIScrollViewDelegate {
    
    var product_id = "";
    var vendor_id = String();
    
    var ratingViews = [Int:[String:String]]();
    let textFieldsToRender = ["What's Your Nickname?".localized, "Summary Of Your Review".localized, "Let Us Know Your Thoughts".localized];
    
    
    override func viewDidLoad() {
        vendor_id = "1";
        super.viewDidLoad();
        self.basicFoundationToRenderView(topMargin: 5);
        //let postString = "";
   //    self.sendRequest(url: "vreviewapi/index/getRatingOption", params: nil);
        
        self.sendRequest(url: "vreviewapi/index/getRatingOption", params: nil,buyer:true);
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func recieveResponse(data: Data?, requestUrl: String?, response: URLResponse?) {
        if let responseData = data {
            guard let jsonData = try? JSON(data:responseData) else{return;}
            print(jsonData);
            if(jsonData["data"]["status"].stringValue == "success") {
                var key = 1;
                for review in jsonData["data"]["rating-option"].arrayValue {
                    var tempArray = [String:String]();
                    tempArray["label"] = review["label"].stringValue;
                    tempArray["rating_code"] = review["rating_code"].stringValue;
                    tempArray["rating_name"] = review["rating_name"].stringValue;
                    
                    self.ratingViews[key] = (tempArray);
                    key = key+1;
               }
               self.renderWriteProductReviewPage();
                
            }
        }
    }
    
    
    func renderWriteProductReviewPage(){
        
        self.makeSomeSpaceInStackView(height: CGFloat(0.0));
        
        let color = Settings.themeColor
        let theme_color = color
        
        if(self.ratingViews.count != 0){
            for c in 1 ..< self.ratingViews.count+1{
                print(self.ratingViews[c] as Any);
                let starRatingView = StarRatingView();
                starRatingView.tag = c;
                starRatingView.translatesAutoresizingMaskIntoConstraints = false;
                starRatingView.topLabel.font = UIFont(fontName: "HelveticaNeue-Medium", fontSize: CGFloat(15.0));
                print("star rate")
                print(self.ratingViews[c]!["rating_name"]!)
                starRatingView.topLabel.text = self.ratingViews[c]!["rating_name"]!.localized;
                starRatingView.topLabel.fontColorTool()
                stackView.addArrangedSubview(starRatingView);
                let starRatingViewHeight = translateAccordingToDevice(CGFloat(70.0));
                starRatingView.heightAnchor.constraint(equalToConstant: starRatingViewHeight).isActive = true;
                self.setLeadingAndTralingSpaceFormParentView(starRatingView, parentView:stackView);
            }
        }
        
        for (val) in self.textFieldsToRender{
            let customOptionTextFieldView = CustomOptionTextFieldView();
            customOptionTextFieldView.translatesAutoresizingMaskIntoConstraints = false;
            customOptionTextFieldView.topLabel.font = UIFont(fontName: "HelveticaNeue-Medium", fontSize: CGFloat(15.0));
            customOptionTextFieldView.topLabel.text = val;
            customOptionTextFieldView.topLabel.fontColorTool()
            stackView.addArrangedSubview(customOptionTextFieldView);
            let customOptionTextFieldViewHeight = translateAccordingToDevice(CGFloat(100.0));
            customOptionTextFieldView.heightAnchor.constraint(equalToConstant: customOptionTextFieldViewHeight).isActive = true;
            self.setLeadingAndTralingSpaceFormParentView(customOptionTextFieldView, parentView:stackView);
        }
        
        let submitButton = UIButton();
        submitButton.translatesAutoresizingMaskIntoConstraints = false;
        submitButton.setTitle("Submit Review".localized, for: UIControl.State.normal);
        submitButton.fontColorTool()
//        submitButton.setTitleColor(theme_color, for: UIControlState.normal);
        submitButton.makeCornerRounded(cornerRadius: translateAccordingToDevice(CGFloat(5.0)));
        submitButton.layer.borderWidth =  CGFloat(2);
        submitButton.titleLabel?.font = UIFont(fontName: "HelveticaNeue-Medium", fontSize: CGFloat(15.0));
        submitButton.addTarget(self, action: #selector(WriteProductReviewViewController.submitReviewButtonPressed(_:)), for: UIControl.Event.touchUpInside);
        stackView.addArrangedSubview(submitButton);
        let submitButtonHeight = translateAccordingToDevice(CGFloat(40.0));
        submitButton.heightAnchor.constraint(equalToConstant: submitButtonHeight).isActive = true;
        self.setLeadingAndTralingSpaceFormParentView(submitButton, parentView:stackView);
        
        
        self.makeSomeSpaceInStackView(height: CGFloat(0.0));
        
    }
    
    func submitReviewButtonPressed(_ sender:UIButton){

        var postString = "";
        var rating_string = "{";
        var textFieldsToRenderCounter = 1;
        
        for view in stackView.subviews{
            if let starRatingView = view as? StarRatingView{
                if(starRatingView.ratingView.rating == 0){
                    let message = "Please Give Rating First!".localized;
                    self.view.makeToast(message, duration: 2, position: .center, title: "Error".localized, image: nil, style: nil, completion: nil);
                    print("Error Handling");
                    return;
                }
                let ratingToSend = String((starRatingView.ratingView.rating)*20);
                let ratingKey = self.ratingViews[starRatingView.tag]?["rating_code"];
                rating_string += "\""+ratingKey!+"\":\""+(ratingToSend)+"\",";
            }
            
            if let customOptionTextFieldView = view as? CustomOptionTextFieldView{
                var textToSend = customOptionTextFieldView.textView.text!;
                
                textToSend = textToSend.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines);
                if(textToSend == "") {
                    print("Error Handling");
                    let message = "Please Fill All Data!".localized;
                    self.view.makeToast(message, duration: 2, position: .center, title: "Error".localized, image: nil, style: nil, completion: nil);
                    return;
                }
                if(textFieldsToRenderCounter == 1){
                    postString += "&customer_name=";
                }
                else if(textFieldsToRenderCounter == 2){
                    postString += "&subject=";
                }
                else if(textFieldsToRenderCounter == 3){
                    postString += "&review=";
                }
                postString += textToSend;
                textFieldsToRenderCounter = textFieldsToRenderCounter+1;
            }
        }
        
        if(rating_string.last! == ","){
            rating_string = rating_string.substring(to: rating_string.index(before: rating_string.endIndex));
        }
        rating_string += "}";
        print(rating_string);
        postString += "&ratings="+rating_string;
        postString += "&vendor_id="+vendor_id;
        print(postString);
        
        var urlToRequest = "vreviewapi/index/addReview";
        let requestHeader = Settings.headerKey;
        let baseURL = Settings.baseUrl
        urlToRequest = baseURL+urlToRequest;
        
        var request = URLRequest(url: URL(string: "\(urlToRequest)")!);
        request.httpMethod = "POST";
        request.httpBody = postString.data(using: String.Encoding.utf8);
        if UserDefaults.standard.bool(forKey: "isLogin"){
            if let user = UserDefaults.standard.object(forKey: "userInfoDict") as? [String:String] {
                request.setValue(user["hashKey"], forHTTPHeaderField: "hashkey")
            }
            
        }
        
        cedMageLoaders.addDefaultLoader(me: self);
        let task = URLSession.shared.dataTask(with: request){
            
            // check for fundamental networking error
            data, response, error in
            guard error == nil && data != nil else{
                print("error=\(error)")
                DispatchQueue.main.async{
                    print(error?.localizedDescription as Any);
                    cedMageLoaders.removeLoadingIndicator(me: self);
                }
                return;
            }
            
            // check for http errors
            if let httpStatus = response as? HTTPURLResponse , httpStatus.statusCode != 200{
                
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(response)")
                DispatchQueue.main.async{
                    cedMageLoaders.removeLoadingIndicator(me: self);
                    
                }
                return;
            }
            
            // code to fetch values from response :: start
            guard let jsonResponse = try? JSON(data: data!) else{return;}
            if(jsonResponse != nil){
                
                DispatchQueue.main.async{
                    cedMageLoaders.removeLoadingIndicator(me: self);
                    print(jsonResponse);
                    if(jsonResponse["data"]["success"].stringValue == "true"){
                        print(jsonResponse["data"]["message"].stringValue);
                        self.view.makeToast(jsonResponse["data"]["message"].stringValue, duration: 2, position: .center, title: "Success", image: nil, style: nil, completion: nil)
                    }
                    else{
                        self.view.makeToast(jsonResponse["data"]["message"].stringValue, duration: 2, position: .center, title: "Error", image: nil, style: nil, completion: nil)
                        print(jsonResponse["data"]["message"].stringValue);
                    }
                    
                }
            }
        }
        task.resume();
        
    }

    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
