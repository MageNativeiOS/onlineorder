/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */


import UIKit

class cedMageMyDownloadView: cedMageViewController,UITableViewDelegate,UITableViewDataSource,URLSessionDownloadDelegate {
    
    @IBOutlet weak var emptyDownload: UIImageView!
    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var cedMageMyDownloads: UITableView!
    var downloadedOrder = [[String:String]]()
    lazy var downloadsSession: URLSession = {
        let configuration = URLSessionConfiguration.default
        let session = URLSession(configuration: configuration, delegate: self, delegateQueue: nil)
        
        return session
    }()
    
    var activeDownloads = [String: Download]()
    var filename: String!
    override func viewDidLoad() {
        super.viewDidLoad()
        topLabel.fontColorTool()
        cedMageMyDownloads.delegate = self
        cedMageMyDownloads.dataSource = self
        let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
        let hashKey = userInfoDict["hashKey"]!;
        let  customerId = userInfoDict["customerId"]!;
        let postString = ["hashkey":hashKey,"customer_id":customerId];
        self.sendRequest(url: "mobiconnectadvcart/customer/download", params: postString)
        // Do any additional setup after loading the view.
    }
    
    
    override func recieveResponse(data: Data?, requestUrl: String?, response: URLResponse?) {
        guard var json = try? JSON(data: data!) else{return;}
        json = json[0]
        print(NSString(data: data!, encoding: String.Encoding.utf8.rawValue) ?? "default")
        if(json["data"]["status"].stringValue == "false"){
            if(downloadedOrder.count == 0){
                 self.renderNoDataImage(view: self, imageName: "nodownloads")
                self.emptyDownload.isHidden = false
                self.cedMageMyDownloads.isHidden = true
                return
            }
        }
        if(json["data"]["status"] == "true"){
            let downloadcount = json["data"]["item_count"].stringValue
            topLabel.setThemeColor()
            topLabel.textColor = Settings.themeTextColor;
//            topLabel.textColor = UIColor.white
            topLabel.text = "Total \(downloadcount) Products"
            topLabel.cardView()
            for result in json["data"]["downloadable-product"].arrayValue {
                let status = result["status"].stringValue
                let order_id = result["order_id"].stringValue
                let link_title = result["link_title"].stringValue
                let download_url = result["download_url"].stringValue
                let title = result["title"].stringValue
                let date = result["date"].stringValue
                let filename = result["file_name"].stringValue
                let remaining_dowload = result["remaining_dowload"].stringValue
                let products_obj = ["status":status, "title":title,"link_title":link_title,"download_url":download_url,"order_id":order_id,"date":date,"filename":filename,"remaining_dowload":remaining_dowload]
                self.downloadedOrder.append(products_obj)
            }
        }
        cedMageMyDownloads.reloadData()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return downloadedOrder.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "downloadCell") as! cedMageMydownloadcell
        let order = downloadedOrder[indexPath.row]
        var string =  " Order Id: "+order["order_id"]!
        string += "\n\n"+" Date: "+order["date"]!+"\n\n"
        string += " Title: "+order["title"]!+"\n\n"+" Link Title: "+order["link_title"]!+"\n\n"
        cell.textView.text = "\(string)"+" Status: "+order["status"]!+"\n\n"+" Remaining Downloads:"+order["remaining_dowload"]!
        cell.clickToDownload.tag=indexPath.row;
        cell.clickToDownload.addTarget(self, action: #selector(cedMageMyDownloadView.ClickTodownload(sender:)), for: UIControl.Event.touchUpInside)
        cell.clickToDownload.setTitle("Click Here To Download".localized, for: UIControl.State.normal)
        cell.clickToDownload.fontColorTool()
        
        return cell
    }
    
    @objc func ClickTodownload(sender:UIButton){
        cedMageLoaders.addDefaultLoader(me: self)
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
        let urlString = downloadedOrder[sender.tag]["download_url"]!
        let url = URL(string: urlString)
        var request = URLRequest(url:url!)
        request.httpMethod = "GET"
        let task = session.dataTask(with: request, completionHandler: {
            data,response,error in
            
            if (error == nil) {
                // Success
                let statusCode = (response as! HTTPURLResponse).statusCode
                print("Success: \(statusCode)")
                
                // This is your file-variable:
                // data
                 DispatchQueue.main.async{
                    cedMageLoaders.removeLoadingIndicator(me: self)
                print(response as Any)
                let httpStatus = response as? HTTPURLResponse
                let Headerresponse = httpStatus?.allHeaderFields["Set-Success"] as? String
                print(Headerresponse as Any)
                let data = Headerresponse?.data(using: String.Encoding.utf8)
                    if(data != nil){
                        guard let json = try? JSON(data:data!) else{return;}
                        print(json["success"].stringValue)
                        print(json["message"].stringValue)
                        if(json["success"].stringValue == "false"){
                            let msg = json["messgae"].stringValue
                            cedMageHttpException.showAlertView(me: self, msg: msg, title: "Error")
                            return
                        }else if(json["success"].stringValue == "true"){
                            let download = Download(url: urlString,file: self.downloadedOrder[sender.tag]["filename"]!)
                            self.filename = self.downloadedOrder[sender.tag]["filename"]
                            download.downloadTask = self.downloadsSession.downloadTask(with: request)
                            download.downloadTask?.resume()
                            download.isDownloading = true
                            let bgCView = UIView();
                            bgCView.tag=151;
                            bgCView.frame = UIScreen.main.bounds;
                            bgCView.autoresizingMask = [UIView.AutoresizingMask.flexibleHeight , UIView.AutoresizingMask.flexibleWidth];
                            bgCView.backgroundColor = UIColor(red:0, green:0, blue:0, alpha:0.5);
                            let filterbyView = ced_downloadView();
                            filterbyView.tag = 181;
                            filterbyView.progressView.progress = 0
                            let colorString = cedMage.getInfoPlist(fileName:"cedMage",indexString:"themeColor") as! String
                            filterbyView.toplabel.backgroundColor = cedMage.UIColorFromRGB(colorCode: colorString)
                            filterbyView.backgroundColor = UIColor.black;
                            filterbyView.frame = CGRect(x:20, y:20, width:self.view.frame.width - 40, height:200)
                            filterbyView.center = self.view.center
                            bgCView.addSubview(filterbyView)
                            self.view.addSubview(bgCView);
                            
                        }
                    }
                }
                
            }
            else {
                // Failure
                print("Failure: %@", error?.localizedDescription as Any);
            }
        })
        task.resume()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.viewWithTag(151)?.removeFromSuperview()
    }
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        if let originalURL = downloadTask.originalRequest?.url?.absoluteString,
            let destinationURL = localFilePathForUrl(previewUrl: originalURL) {
            print(originalURL)
            let baseUrl =  destinationURL.deletingLastPathComponent
            let newurl =  baseUrl?.appendingPathComponent(filename)
            DispatchQueue.main.async
                {
                    let view =  self.view.viewWithTag(181) as? ced_downloadView
                    view?.progressView.progress = 1
                    view?.downloadLabel.text = "Download Complete".localized
                    self.view.makeToast(self.filename + " download complete.".localized, duration: 1.5, position: .center, title: nil, image: nil, style: nil, completion: nil)
            }
            // 2
            let fileManager = FileManager.default
            do {
                try fileManager.removeItem(at: destinationURL as URL)
            } catch {
                // Non-fatal: file probably doesn't exist
            }
            do {
                try fileManager.copyItem(at: location, to: newurl!)
                print(location)
            } catch let error as NSError {
                print("Could not copy file to disk: \(error.localizedDescription)")
            }
        }
        
        
    }
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
        if let downloadUrl = downloadTask.originalRequest?.url?.absoluteString,
            let download = activeDownloads[downloadUrl] {
            // 2
            download.progress = Float(totalBytesWritten)/Float(totalBytesExpectedToWrite)
            DispatchQueue.main.async
                {
                    let view =  self.view.viewWithTag(181) as? ced_downloadView
                    view?.progressView.progress =  download.progress
                    view?.downloadLabel.text = "Downloading...".localized
            }
        }
        
    }
    
    func localFilePathForUrl(previewUrl: String) -> NSURL? {
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString
        if let url = NSURL(string: previewUrl), let lastPathComponent = url.lastPathComponent {
            let fullPath = documentsPath.appendingPathComponent(lastPathComponent)
            return NSURL(fileURLWithPath:fullPath)
        }
        return nil
    }
    
    
    func localFileExistsForTrack(track: String?) -> Bool {
        if let urlString = track, let localUrl = localFilePathForUrl(previewUrl: urlString) {
            var isDir : ObjCBool = false
            if let path = localUrl.path {
                return FileManager.default.fileExists(atPath: path, isDirectory: &isDir)
            }
        }
        return false
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let url = downloadedOrder[indexPath.row]["download_url"]
        if localFileExistsForTrack(track: url!) {
            UIApplication.shared.openURL(URL(string: url!)!)
        }
        tableView.deselectRow(at: indexPath as IndexPath, animated: true)
    }
    
}
