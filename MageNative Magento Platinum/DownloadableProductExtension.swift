/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */

import UIKit

extension ProductSinglePageViewController{

    func renderDownloadableProductSection(){
        
        let bundleProductContainerStackView = UIStackView(); // outermost stackview
        bundleProductContainerStackView.translatesAutoresizingMaskIntoConstraints = false;
        bundleProductContainerStackView.axis  = NSLayoutConstraint.Axis.vertical;
        bundleProductContainerStackView.distribution  = UIStackView.Distribution.equalSpacing;
        bundleProductContainerStackView.alignment = UIStackView.Alignment.center;
        bundleProductContainerStackView.spacing   = 0.0;
        stackView.addArrangedSubview(bundleProductContainerStackView);
        stackView.addConstraint(NSLayoutConstraint(item: bundleProductContainerStackView, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: stackView, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: padding/2));
        stackView.addConstraint(NSLayoutConstraint(item: bundleProductContainerStackView, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: stackView, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: -padding/2));
        
        
        let backgroundViewForStackView = UIView(); // uiview for covering stackview
        backgroundViewForStackView.translatesAutoresizingMaskIntoConstraints = false;
        //backgroundViewForStackView.backgroundColor = UIColor.white;
        //backgroundViewForStackView.makeCard(backgroundViewForStackView, cornerRadius: 2, color: UIColor.black, shadowOpacity: 0.4);
        bundleProductContainerStackView.addArrangedSubview(backgroundViewForStackView); // adding uiview to stackview
        bundleProductContainerStackView.addConstraint(NSLayoutConstraint(item: backgroundViewForStackView, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: bundleProductContainerStackView, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 0));
        bundleProductContainerStackView.addConstraint(NSLayoutConstraint(item: backgroundViewForStackView, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: bundleProductContainerStackView, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 0));
        
        downloadableProductOptionsStackView = UIStackView(); // main actual stackview that will contain all bundle-options
        downloadableProductOptionsStackView.translatesAutoresizingMaskIntoConstraints = false;
        downloadableProductOptionsStackView.axis  = NSLayoutConstraint.Axis.vertical;
        downloadableProductOptionsStackView.distribution  = UIStackView.Distribution.equalSpacing;
        downloadableProductOptionsStackView.alignment = UIStackView.Alignment.center;
        downloadableProductOptionsStackView.spacing   = 5.0;
        backgroundViewForStackView.addSubview(downloadableProductOptionsStackView); // adding stackview to uiview
        backgroundViewForStackView.addConstraint(NSLayoutConstraint(item: downloadableProductOptionsStackView!, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: backgroundViewForStackView, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: padding/2));
        backgroundViewForStackView.addConstraint(NSLayoutConstraint(item: downloadableProductOptionsStackView!, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: backgroundViewForStackView, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: -padding/2));
        backgroundViewForStackView.addConstraint(NSLayoutConstraint(item: downloadableProductOptionsStackView!, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: backgroundViewForStackView, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1, constant: padding/2));
        backgroundViewForStackView.addConstraint(NSLayoutConstraint(item: downloadableProductOptionsStackView!, attribute: NSLayoutConstraint.Attribute.bottom, relatedBy: NSLayoutConstraint.Relation.equal, toItem: backgroundViewForStackView, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: -padding/2));
        
        
        
        if(self.links.count > 0){
            
            let link_title_name = self.downloadRelatedData["link_title"]!;
            
            let link_title = UILabel();
            link_title.font = UIFont(fontName: "HelveticaNeue-Medium", fontSize: CGFloat(15.0));
            link_title.fontColorTool()
            link_title.translatesAutoresizingMaskIntoConstraints = false;
            link_title.text = link_title_name;
            downloadableProductOptionsStackView.addArrangedSubview(link_title);
            let link_title_height = translateAccordingToDevice(CGFloat(30.0));
            link_title.heightAnchor.constraint(equalToConstant: link_title_height).isActive = true;
            self.setLeadingAndTralingSpaceFormParentView(link_title,parentView:downloadableProductOptionsStackView, padding:padding/4);
            
            for (key,val) in self.links{
            
                if(self.downloadRelatedData["links_purchased_separately"] == "0"){
                    let downloadable_link = UIButton();
                    downloadable_link.tag = Int(val["link_id"]!)!;
                    
                    downloadable_link.translatesAutoresizingMaskIntoConstraints = false;
                    downloadable_link.setTitle(val["link_title"]!, for: UIControl.State.normal);
                    downloadable_link.setTitleColor(UIColor(hexString: "#C3272B"), for: UIControl.State.normal);
                    downloadable_link.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.left;
                    downloadable_link.titleLabel?.font = UIFont(fontName: "", fontSize: CGFloat(13.0));
                    downloadableProductOptionsStackView.addArrangedSubview(downloadable_link);
                    let downloadable_link_height = translateAccordingToDevice(CGFloat(30.0));
                    downloadable_link.heightAnchor.constraint(equalToConstant: downloadable_link_height).isActive = true;
                    self.setLeadingAndTralingSpaceFormParentView(downloadable_link,parentView:downloadableProductOptionsStackView, padding:padding/4);
                }
                else{
                    let downloadable_link = CheckboxView();
                   downloadable_link.tag = Int(val["link_id"]!)!;
                    
                    downloadable_link.translatesAutoresizingMaskIntoConstraints = false;
                    downloadable_link.checkboxButton.setTitle(val["link_title"]!, for: UIControl.State.normal);
                    downloadable_link.checkboxButton.setTitleColor(UIColor(hexString: "#C3272B"), for: UIControl.State.normal);
//                    downloadable_link.checkboxButton.fontColorTool()
                    downloadable_link.checkboxButton.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.left;
                    downloadable_link.checkboxButton.titleLabel?.font = UIFont(fontName: "", fontSize: CGFloat(13.0));
                    downloadable_link.checkboxButton.addTarget(self, action: #selector(ProductSinglePageViewController.downloadable_link_checkbox_pressed(_:)), for: UIControl.Event.touchUpInside);
                    downloadableProductOptionsStackView.addArrangedSubview(downloadable_link);
                    let downloadable_link_height = translateAccordingToDevice(CGFloat(30.0));
                    downloadable_link.heightAnchor.constraint(equalToConstant: downloadable_link_height).isActive = true;
                    self.setLeadingAndTralingSpaceFormParentView(downloadable_link,parentView:downloadableProductOptionsStackView, padding:padding/4);
                }
                
            }
        }
        
        /*
        if(self.samples.count > 0){
            
            let link_title_name = self.downloadRelatedData["samples_title"]!;
            
            let link_title = UILabel();
            link_title.translatesAutoresizingMaskIntoConstraints = false;
            link_title.text = link_title_name;
            downloadableProductOptionsStackView.addArrangedSubview(link_title);
            let link_title_height = translateAccordingToDevice(CGFloat(30.0));
            link_title.heightAnchor.constraint(equalToConstant: link_title_height).isActive = true;
            self.setLeadingAndTralingSpaceFormParentView(link_title,parentView:downloadableProductOptionsStackView, padding:padding/4);
            
            
            for (key,val) in self.samples{
                let downloadable_link = UIButton();
                downloadable_link.tag = Int(key)!;
                downloadable_link.translatesAutoresizingMaskIntoConstraints = false;
                downloadable_link.setTitle(val["sample-title"]!, for: UIControlState.normal);
                downloadable_link.setTitleColor(UIColor(hexString: "#C3272B"), for: UIControlState.normal);
                downloadable_link.contentHorizontalAlignment = UIControlContentHorizontalAlignment.left;
                downloadableProductOptionsStackView.addArrangedSubview(downloadable_link);
                let downloadable_link_height = translateAccordingToDevice(CGFloat(30.0));
                downloadable_link.heightAnchor.constraint(equalToConstant: downloadable_link_height).isActive = true;
                self.setLeadingAndTralingSpaceFormParentView(downloadable_link,parentView:downloadableProductOptionsStackView, padding:padding/4);
            }
        }
        */
        
    }
    
    @objc func downloadable_link_checkbox_pressed(_ sender:UIButton){
        print("downloadable_link_checkbox_pressed");
        
        if let checkboxView = sender.superview?.superview as? CheckboxView {
            
            if(checkboxView.checkboxButtonImage!.image == UIImage(named:"UncheckedCheckbox")){
                checkboxView.checkboxButtonImage.image = UIImage(named: "CheckedCheckbox");
            }
            else{
                checkboxView.checkboxButtonImage.image = UIImage(named: "UncheckedCheckbox");
            }
        }
    }

}
