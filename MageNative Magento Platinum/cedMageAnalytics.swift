/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */
import UIKit
extension UIViewController  {
    
    func sendScreenView(name:String) {
        let GAIname = name
        let tracker = GAI.sharedInstance().defaultTracker
        if(tracker == nil){
            return
        }
        tracker?.set(kGAIScreenName, value: GAIname)
        tracker?.send(GAIDictionaryBuilder.createScreenView().build() as NSDictionary  as [NSObject : AnyObject])
    }
    
    func trackEvent(Eventcategory: String, Eventaction: String, label: String, value: NSNumber?) {
        let tracker = GAI.sharedInstance().defaultTracker
        if(tracker == nil){
            return
        }
        let trackDictionary = GAIDictionaryBuilder.createEvent(withCategory: Eventcategory, action: Eventaction, label: label, value: value).build() as NSDictionary
        tracker?.send(trackDictionary as [NSObject : AnyObject])
    }
    
    func viewProduct(productId:String,productName:String,Price:NSNumber){
        let tracker = GAI.sharedInstance().defaultTracker
        if(tracker == nil){
            return
        }
        let product = GAIEcommerceProduct()
        product.setId(productId)
        product.setName(productName)
        product.setPrice(Price)
        let action = GAIEcommerceProductAction()
        action.setAction("Product Views")
        let builder = GAIDictionaryBuilder()
        builder.setProductAction(action)
        builder.add(product)
        tracker?.set(kGAIScreenName, value: "Product View")
        tracker?.send(builder.build() as [NSObject : AnyObject])
    }
    
}
