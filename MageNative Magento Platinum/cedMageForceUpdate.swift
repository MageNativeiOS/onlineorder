/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */

import UIKit

class cedMageForceUpdate: MagenativeUIViewController {
    
    @IBOutlet weak var upgradeNow: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        upgradeNow.addTarget(self, action: #selector(cedMageForceUpdate.gotoStore(sender:)), for: .touchUpInside)
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func gotoStore(sender:UIButton){
        let appUrl = "" //Live App  URL
        if let Url = URL(string: appUrl){
            if(UIApplication.shared.canOpenURL(Url)){
                UIApplication.shared.openURL(Url)
            }
        }
    }
    
    
    
}
