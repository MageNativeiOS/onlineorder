//
//  sentBoxViewController.swift
//  MageNative Magento Platinum
//
//  Created by Macmini on 10/04/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit


class sentBoxViewController: MagenativeUIViewController {
  
  @IBOutlet weak var tableView: UITableView!
  var customerEmailData = [[String:String]]()
  override func viewDidLoad() {
    super.viewDidLoad()
    
    tableView.separatorStyle = .none
    if defaults.bool(forKey: "isLogin") {
      let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
      guard let custId = userInfoDict["customerId"] else {return}
      self.sendRequest(url: "mobimessaging/getsentmessages", params: ["customer_id":custId],store: true)
    }
    
    tableView.delegate = self
    tableView.dataSource = self
  }
  
  
  override func recieveResponse(data: Data?, requestUrl: String?, response: URLResponse?) {
    
    guard let data = data else{return}
    
    guard var jsonData = try? JSON(data: data) else {return;}
    jsonData = jsonData[0]
    print(jsonData)
    if jsonData["data"]["status"].stringValue == "true" {
      for dt in jsonData["data"]["customer_sent_list"].arrayValue {
        let temp = ["receiver_name":dt["receiver_name"].stringValue,"id":dt["id"].stringValue,"subject":dt["subject"].stringValue,"message":dt["message"].stringValue,"date":dt["date"].stringValue]
        self.customerEmailData.append(temp)
        tableView.reloadData()
      }
    }else{
      self.view.makeToast(jsonData["data"]["message"].stringValue, duration: 2.0, position: .bottom)
    }
  }
  
}

extension sentBoxViewController: UITableViewDelegate,UITableViewDataSource {
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return customerEmailData.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "sentBoxTableCell", for: indexPath) as! sentBoxTableCell
    let dataToshow = customerEmailData[indexPath.row]
    cell.subjectLabel.text = dataToshow["subject"]
    cell.messageLabel.text = dataToshow["message"]
    cell.recieverNameLabel.text = dataToshow["receiver_name"]
    cell.dateLabel.text = dataToshow["date"]
    cell.recieverNameLabel.textColor = UIColor.red
    cell.insideView.cardView()
    cell.selectionStyle = .none
    return cell
  }
  
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let vc = UIStoryboard(name: "cedMageAccounts", bundle: nil).instantiateViewController(withIdentifier: "sentBoxReplyController") as! sentBoxReplyController
    self.navigationController?.pushViewController(vc, animated: true)
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 130
  }
}
