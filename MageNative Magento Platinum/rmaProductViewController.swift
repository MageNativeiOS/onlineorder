//
//  rmaProductViewController.swift
//  MageNative Magento Platinum
//
//  Created by Macmini on 13/12/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class rmaProductViewController: MagenativeUIViewController,UITableViewDataSource,UITableViewDelegate{
    
    
    @IBOutlet weak var rmaId: UILabel!
    @IBOutlet weak var tableView: UITableView!
    var rma_id = String()
    var rmaItemData = [[String:String]]()
    var orderData = [String:String]()
    var customerData=[String:String]()
    override func viewDidLoad() {
        super.viewDidLoad()
      
        // Do any additional setup after loading the view.
        if defaults.bool(forKey: "isLogin") {
                   userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
                   if let custId = userInfoDict["customerId"] {
                       self.sendRequest(url: "mobiconnect/mobirma/viewrma", params: ["customer_id":custId,"rma_id":rma_id],store:true)
                   }
               }
    }
    override func recieveResponse(data: Data?, requestUrl: String?, response: URLResponse?) {
        
        if let data = data {
            do {
            var json = try JSON(data:data)
            json = json[0]
            print(json)
            if json["data"]["success"].stringValue == "true" {
                for rmaItemInfo in json["data"]["rma_data"]["rma_item_info"].arrayValue {
                     rmaItemData.append(["rma_qty":rmaItemInfo["rma_qty"].stringValue,"row_total":rmaItemInfo["row_total"].stringValue,"ordered_qty":rmaItemInfo["ordered_qty"].stringValue,"name":rmaItemInfo["name"].stringValue,"price":rmaItemInfo["price"].stringValue,"sku":rmaItemInfo["sku"].stringValue])
                }
                
                let order_id=json["data"]["rma_data"]["rma_order_info"]["order_id"].stringValue
                let status=json["data"]["rma_data"]["rma_order_info"]["status"].stringValue
                let condition=json["data"]["rma_data"]["rma_order_info"]["pk_condition"].stringValue
                let reson=json["data"]["rma_data"]["rma_order_info"]["reason"].stringValue
                let resolution=json["data"]["rma_data"]["rma_order_info"]["resolution"].stringValue
                let increment_id=json["data"]["rma_data"]["rma_order_info"]["rma_increment_id"].stringValue
                rmaId.text = increment_id;
                self.orderData=["order_id":order_id,"status":status,"condition":condition,"resolution":resolution,"increment_id":increment_id]
                
                let customer_name=json["data"]["rma_data"]["rma_customer_info"]["customer_name"].stringValue
                let streetArray=json["data"]["rma_data"]["rma_customer_info"]["billing_address"]["street"].arrayValue
                var street=""
                for index in streetArray
                {
                    street+=index.stringValue+" "
                }
                let region=json["data"]["rma_data"]["rma_customer_info"]["billing_address"]["region"].stringValue
                let city=json["data"]["rma_data"]["rma_customer_info"]["billing_address"]["city"].stringValue
                let postCode=json["data"]["rma_data"]["rma_customer_info"]["billing_address"]["postcode"].stringValue
                let telephone=json["data"]["rma_data"]["rma_customer_info"]["billing_address"]["telephone"].stringValue
                let country=json["data"]["rma_data"]["rma_customer_info"]["billing_address"]["country_id"].stringValue
                let customer_email=json["data"]["rma_data"]["rma_customer_info"]["customer_email"].stringValue
                self.customerData=["customer_name":customer_name,"region":region,"street":street,"city":city,"postcode":postCode,"telephone":telephone,"country_id":country,"customer_email":customer_email]
                
                print(rmaItemData)
                tableView.delegate = self
                      tableView.dataSource = self
                      tableView.separatorStyle = .none
                tableView.reloadData()
            }
            }catch let error {
                print(error.localizedDescription)
            }
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 5
    }
   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section==0{
                  return rmaItemData.count
              }
              return 1
   }
   
   func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    if indexPath.section == 0 {
    let cell  = tableView.dequeueReusableCell(withIdentifier: "productInfo", for: indexPath) as! rmaProductViewCell
         let product = rmaItemData[indexPath.row]
        cell.productLabel.text = product["name"]
        cell.productLabel.numberOfLines = 0
        cell.skuLabel.text = product["sku"]
        cell.rmaQtyLabel.text = product["rma_qty"]
        cell.orderedQtyLabel.text = product["ordered_qty"]
        cell.priceLabel.text = product["price"]
        cell.prodView.cardView()
        return cell
   
    }
    else if indexPath.section == 1{
        let cell = tableView.dequeueReusableCell(withIdentifier: "orderInfo", for: indexPath) as! rmaProductViewCell
        cell.orderIdLabel.text=self.orderData["order_id"]
        cell.orderStatus.text=self.orderData["status"]
        cell.orderView.cardView()
        return cell
    }
    else if indexPath.section == 2{
        let cell = tableView.dequeueReusableCell(withIdentifier: "shippingAddress", for: indexPath) as! rmaProductViewCell
        cell.addressLabel.numberOfLines = 0
      //  cell.addressLabel.text = ""
//        cell.addressLabel.text +=  self.customerData["customer_name"] + "\n" + self.customerData["street"] + "\n" + self.customerData["city"] + "\n" + self.customerData["region"] + "\n" + self.customerData["postcode"] + "\n" + self.customerData["country_id"] + "\n" + self.customerData["telephone"]
        
        var address = String()
        address += customerData["customer_name"]!
        address += "\n"
        address += customerData["street"]!
        address += "\n"
        address += customerData["city"]!
        address += "\n"
        address += customerData["region"]!
        address += "\n"
        address += customerData["postcode"]!
        address += "\n"
        address += customerData["country_id"]!
        address += "\n"
        address += customerData["telephone"]!
       
        
        
        cell.addressLabel.text = address
        cell.shippingView.cardView()
        return cell
    }
    else if indexPath.section == 3 {
        let cell = tableView.dequeueReusableCell(withIdentifier: "customerInfo", for: indexPath) as! rmaProductViewCell
        cell.customerInfoLabel.numberOfLines = 0
        var customer = String()
        customer += customerData["customer_email"]!
        customer += "\n"
        customer += customerData["customer_name"]!
        cell.customerInfoLabel.text = customer
        cell.customerView.cardView()
        return cell
    }
    else if indexPath.section == 4 {
        let cell = tableView.dequeueReusableCell(withIdentifier: "generalInfo", for: indexPath) as! rmaProductViewCell
        cell.reasonLabel.text=self.orderData["reason"]
        cell.packageLabel.text=self.orderData["condition"]
        cell.resolutionLabel.text=self.orderData["resolution"]
        cell.generalView.cardView()
        return cell
    }
    return UITableViewCell();
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section==0
        {
            return 200
        }
        else if indexPath.section==1
        {
            return 120
        }
        else if indexPath.section==2
        {
            return 250
        }
        else if indexPath.section==3
        {
            return 120
        }
        else
        {
            return 160
        }
    }
}
