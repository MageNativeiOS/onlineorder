/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */


import UIKit

class WriteProductReviewViewController: MagenativeUIViewController {
    
    var product_id = "";
    
    var ratingViews = [Int:[String:String]]();
    let textFieldsToRender = ["What's Your Nickname?".localized, "Summary Of Your Review".localized, "Let Us Know Your Thoughts".localized];
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.basicFoundationToRenderView(bottomMargin: CGFloat(0.0));
        self.getRequest(url: "mobiconnect/review/ratingoption/", store: false)
    }
    
    override func recieveResponse(data: Data?, requestUrl: String?, response: URLResponse?) {
        if let data = data {
            guard var jsonResponse = try? JSON(data:data)else{return;}
            jsonResponse = jsonResponse[0]
            print(jsonResponse)
        if(jsonResponse["data"]["status"].string == "success"){
            var i = 0
            for option in jsonResponse["data"]["rating-option"].arrayValue{
                print(option)
                var tempArray = [String:String]();
                let key = option["rating-id"].stringValue;
                print(key)
                print(option["rating-code"].stringValue)
                tempArray["rating_code"] = key;
                tempArray["rating_name"] = option["rating-code"].stringValue;
                self.ratingViews[i] = (tempArray);
                i=i+1
            }
        }
        
        self.renderWriteProductReviewPage();
        }
    }
    
    func renderWriteProductReviewPage(){
        
        self.makeSomeSpaceInStackView(height: CGFloat(0.0));
        
        let color = Settings.themeColor
        let theme_color = color;
        
        if(self.ratingViews.count != 0){
            for c in 0 ..< self.ratingViews.count{
                print(self.ratingViews);
                let starRatingView = StarRatingView();
                print("star rate 1")
                print(self.ratingViews[c]!["rating_name"]!)
                starRatingView.tag = Int(self.ratingViews[c]!["rating_code"]!)!;
                starRatingView.translatesAutoresizingMaskIntoConstraints = false;
                starRatingView.topLabel.font = UIFont(fontName: "HelveticaNeue-Medium", fontSize: CGFloat(15.0));
                starRatingView.topLabel.text = self.ratingViews[c]!["rating_name"]!.localized;
                starRatingView.topLabel.fontColorTool()
                stackView.addArrangedSubview(starRatingView);
                let starRatingViewHeight = translateAccordingToDevice(CGFloat(70.0));
                starRatingView.heightAnchor.constraint(equalToConstant: starRatingViewHeight).isActive = true;
                self.setLeadingAndTralingSpaceFormParentView(starRatingView, parentView:stackView);
            }
        }
        
        for (val) in self.textFieldsToRender{
            let customOptionTextFieldView = CustomOptionTextFieldView();
            customOptionTextFieldView.translatesAutoresizingMaskIntoConstraints = false;
            customOptionTextFieldView.topLabel.font = UIFont(fontName: "HelveticaNeue-Medium", fontSize: CGFloat(15.0));
            customOptionTextFieldView.topLabel.fontColorTool()
            customOptionTextFieldView.topLabel.text = val;
            stackView.addArrangedSubview(customOptionTextFieldView);
            let customOptionTextFieldViewHeight = translateAccordingToDevice(CGFloat(100.0));
            customOptionTextFieldView.heightAnchor.constraint(equalToConstant: customOptionTextFieldViewHeight).isActive = true;
            self.setLeadingAndTralingSpaceFormParentView(customOptionTextFieldView, parentView:stackView);
        }
        
        let submitButton = UIButton();
        submitButton.translatesAutoresizingMaskIntoConstraints = false;
        submitButton.setTitle("Submit Review".localized, for: UIControl.State.normal);
        if #available(iOS 13.0, *) {
            submitButton.setTitleColor(UIColor.label, for: UIControl.State.normal)
        } else {
            submitButton.setTitleColor(UIColor.black, for: UIControl.State.normal)
        };
        submitButton.makeCornerRounded(cornerRadius: translateAccordingToDevice(CGFloat(5.0)));
        submitButton.layer.borderWidth =  CGFloat(2);
        submitButton.layer.borderColor = UIColor.gray.cgColor
        submitButton.titleLabel?.font = UIFont(fontName: "HelveticaNeue-Medium", fontSize: CGFloat(15.0));
        submitButton.addTarget(self, action: #selector(WriteProductReviewViewController.submitReviewButtonPressed(_:)), for: UIControl.Event.touchUpInside);
        stackView.addArrangedSubview(submitButton);
        let submitButtonHeight = translateAccordingToDevice(CGFloat(40.0));
        submitButton.heightAnchor.constraint(equalToConstant: submitButtonHeight).isActive = true;
        self.setLeadingAndTralingSpaceFormParentView(submitButton, parentView:stackView);
        
        
        self.makeSomeSpaceInStackView(height: CGFloat(0.0));
        
    }
    
    @objc func submitReviewButtonPressed(_ sender:UIButton){
        print("writeReviewButtonPressed");
        
        var postString = "";
        var postData = [String:String]()
        if defaults.bool(forKey: "isLogin") {
            let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
            postData["hashkey"]=userInfoDict["hashKey"]!
            postData["customer_id"]=userInfoDict["customerId"]!;
        }
        postData["prodID"]=product_id;
        var rating_string = "{";
        var textFieldsToRenderCounter = 1;
        for view in stackView.subviews{
            
            if let starRatingView = view as? StarRatingView{
                if(starRatingView.ratingView.rating == 0){
                    print("Error Handling");
                    return;
                }
                let ratingToSend = Float((starRatingView.tag-1)*5)+(starRatingView.ratingView.rating);
                rating_string += "\""+String(starRatingView.tag)+"\":\""+String(ratingToSend)+"\",";
            }
            
            if let customOptionTextFieldView = view as? CustomOptionTextFieldView{
                var textToSend = customOptionTextFieldView.textView.text!;
                textToSend = textToSend.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines);
                if(textToSend == ""){
                    print("Error Handling");
                    return;
                }
                if(textFieldsToRenderCounter == 1){
                    postData["nickname"] = textToSend
                }
                else if(textFieldsToRenderCounter == 2){
                    postData["title"] = textToSend;
                }
                else if(textFieldsToRenderCounter == 3){
                    postData["detail"] = textToSend;
                }
               
                textFieldsToRenderCounter = textFieldsToRenderCounter+1;
            }
        }
        
        if(rating_string.last! == ","){
            rating_string = rating_string.substring(to: rating_string.index(before: rating_string.endIndex));
        }
        rating_string += "}";
        print(rating_string);
        postData["ratings"] = rating_string;
        postString = ["parameters":postData].convtToJson() as String
        print(postString);
        
        var urlToRequest = "mobiconnect/review/add";
        let requestHeader = Settings.headerKey
        let baseURL = Settings.baseUrl
        urlToRequest = baseURL+urlToRequest;
        
        var request = URLRequest(url: URL(string: "\(urlToRequest)")!);
        request.httpMethod = "POST";
        request.httpBody = postString.data(using: String.Encoding.utf8);
        if UserDefaults.standard.bool(forKey: "isLogin"){
            if let user = UserDefaults.standard.object(forKey: "userInfoDict") as? [String:String] {
                request.setValue(user["hashKey"], forHTTPHeaderField: "hashkey")
            }
            
        }
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        cedMageLoaders.addDefaultLoader(me: self);
        let task = URLSession.shared.dataTask(with: request){
            
            // check for fundamental networking error
            data, response, error in
            guard error == nil && data != nil else{
                print("error=\(error)")
                DispatchQueue.main.async{
                    print(error?.localizedDescription ?? "error localisation");
                    cedMageLoaders.removeLoadingIndicator(me: self);
                }
                return;
            }
            
            // check for http errors
            if let httpStatus = response as? HTTPURLResponse , httpStatus.statusCode != 200{
                
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(response)")
                DispatchQueue.main.async{
                    cedMageLoaders.removeLoadingIndicator(me: self);
                    
                }
                return;
            }
            
            // code to fetch values from response :: start
            guard var jsonResponse = try? JSON(data: data!) else{return;}
            jsonResponse = jsonResponse[0]
            if(jsonResponse != nil){
                
                DispatchQueue.main.async{
                    cedMageLoaders.removeLoadingIndicator(me: self);
                    print(jsonResponse);
                    if(jsonResponse["data"]["status"].stringValue == "success"){
                        print(jsonResponse["data"]["message"].stringValue);
                        self.view.makeToast(jsonResponse["data"]["message"].stringValue, duration: 2, position: .center, title: "Success".localized, image: nil, style: nil, completion: {
                            Void in
                           _ = self.navigationController?.popViewController(animated: true)
                        })
                    }
                    else{
                    self.view.makeToast(jsonResponse["data"]["message"].stringValue, duration: 2, position: .center, title: "Error".localized, image: nil, style: nil, completion: nil)
                        print(jsonResponse["data"]["message"].stringValue);
                    }
                    
                }
            }
        }
        task.resume();
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
