//
//  AffiliateService.swift
//  MageNative Magento Platinum
//
//  Created by Hamza Usmani on 25/08/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import Foundation

class AffiliateService {
    
    static let shared = AffiliateService()
    var customerId: String? {
        let userDict   = UserDefaults.standard.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]()
        return userDict["customerId"]
    }
    //MARK:- Affiliate Dashboard
 /*   func getAffiliateDashboardDetails(controller: UIViewController, completion: @escaping (AffiliateDashboardModel) -> Void) {
        
        ApiHandler.handle.request(with: "affiliate/account/index/\(customerId ?? "")", requestType: .GET, controller: controller) { (data, error) in
            guard let data = data else { return }
            guard let json = try? JSON(data: data) else { return }
            print(json)
            let info = AffiliateDashboardModel(json: json)
            completion(info)
        }
    }
    //MARK:- Banners & Links
    
    func getAffliliateBannersDetails(controller: UIViewController, completion: @escaping (BannerLinksModel) -> Void){
     
        ApiHandler.handle.request(with: "affiliate/banner/index/\(customerId ?? "")", requestType: .GET, controller: controller) { (data, error) in
            guard let data = data else { return }
            guard let json = try? JSON(data: data) else { return }
            print(json)
            let info = BannerLinksModel(json: json)
            completion(info)
        }
    }
    //MARK:- Refferal Report
    func getAffliliateRefferalReports(controller: UIViewController, completion: @escaping (referralReportModel) -> Void){
      
        
        ApiHandler.handle.request(with: "affiliate/referral/lists/\(customerId ?? "")", requestType: .GET, controller: controller) { (data, error) in
            guard let data = data else { return }
            guard let json = try? JSON(data: data) else { return }
            print(json)
          let info = referralReportModel(json: json)
            completion(info)
        }
    }
    //MARK:-Payout Discount
    func getAffliliatePayoutDiscountCoupon(controller: UIViewController, completion: @escaping (payoutDiscountModel) -> Void){
          
          ApiHandler.handle.request(with: "affiliate/referral/payout/\(customerId ?? "")", requestType: .GET, controller: controller) { (data, error) in
              guard let data = data else { return }
              guard let json = try? JSON(data: data) else { return }
              print(json)
              let info = payoutDiscountModel(json: json)
              completion(info)
          }
      }
 
// MARK:-Referral Transaction List
    func getAffliliateRefTransactionList(controller: UIViewController, completion: @escaping (refTransactionModel) -> Void){
     
        ApiHandler.handle.request(with: "affiliate/referral/summary/\(customerId ?? "")", requestType: .GET, controller: controller) { (data, error) in
            guard let data = data else { return }
            guard let json = try? JSON(data: data) else { return }
            print(json)
            let info = refTransactionModel(json: json)
            completion(info)
        }
    }
    
    //MARK:- Commission Section
    
    func getAffliliateCommissionData(controller: UIViewController, completion: @escaping (commissionDataModel) -> Void){
       
        ApiHandler.handle.request(with: "affiliate/comission/index/\(customerId ?? "")", requestType: .GET, controller: controller) { (data, error) in
            guard let data = data else { return }
            guard let json = try? JSON(data: data) else { return }
            print(json)
            let info = commissionDataModel(json: json)
            completion(info)
        }
    } */
    
    //MARK:- MyWallet Section
       
       func getAffliliateMyWallet(controller: UIViewController, completion: @escaping (myWalletModel) -> Void){
           ApiHandler.handle.request(with: "affiliate/wallet/index/\(customerId ?? "")", requestType: .GET, controller: controller) { (data, error) in
               guard let data = data else { return }
               guard let json = try? JSON(data: data) else { return }
               print(json)
               let info = myWalletModel(json: json)
               completion(info)
           }
       }
    //MARK:- TransactionSummary
    
   /* func getAffliliateTransactionSummary(controller: UIViewController, completion: @escaping (transSummaryModel) -> Void){
        
        
        ApiHandler.handle.request(with: "affiliate/transaction/index/\(customerId ?? "")", requestType: .GET, controller: controller) { (data, error) in
            guard let data = data else { return }
            guard let json = try? JSON(data: data) else { return }
            print(json)
//            let info = transSummaryModel(json: json)
//            completion(info)
        }
    }
    
    //MARK:- Refer A Friend
       func getInvitationData(controller: UIViewController, completion: @escaping (ReferralInvitationModel) -> Void) {
           ApiHandler.handle.request(with: "affiliate/referral/index/\(customerId ?? "")", requestType: .GET, controller: controller) { (data, error) in
               guard let data = data else { return }
               guard let json = try? JSON(data: data) else { return }
               print(json)
               let info = ReferralInvitationModel(json: json)
               completion(info)
           }
       }
       
       //MARK:- Withdrawls Section
    
       func getWithdrawlsData(controller: UIViewController, completion: @escaping ([AffiliateWithdrawlModel]) -> Void) {
           ApiHandler.handle.request(with: "affiliate/withdrawl/index/\(customerId ?? "")", requestType: .GET, controller: controller) { (data, error) in
               guard let data = data else { return }
               guard let json = try? JSON(data: data) else { return }
               print(json)
               
               var resultArray = [AffiliateWithdrawlModel]()
               
               for res in json[0]["affiliate_withdrawl"].arrayValue {
                   let temp = AffiliateWithdrawlModel(json: res)
                   resultArray.append(temp)
               }
               
               print("DEBUG: array count is \(resultArray.count)")
               
               completion(resultArray)
           }
       }
       //MARK:- Payment Setting
    func getPaymentSettingData(controller: UIViewController, completion: @escaping ([AffiliatePaymentSettingModel]) -> Void) {
          ApiHandler.handle.request(with: "affiliate/paymentsetting/index/\(customerId ?? "")", requestType: .GET, controller: controller) { (data, error) in
              guard let data = data else { return }
              guard let json = try? JSON(data: data) else { return }
              print(json)
              
              var resultArray = [AffiliatePaymentSettingModel]()
              
              for method in json[0]["data"]["fieldset"].arrayValue {
                  let temp = AffiliatePaymentSettingModel(json: method)
                  resultArray.append(temp)
              }
              
              print("DEBUG: Resulkt array is \(resultArray.count)")
              completion(resultArray)
          }
      }*/
    
}
