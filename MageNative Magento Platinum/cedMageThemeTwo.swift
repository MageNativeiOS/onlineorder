/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */

import UIKit
import AVKit

class cedMageThemeTwo: cedMageViewController,UITableViewDelegate,UITableViewDataSource,KIImagePagerDataSource,KIImagePagerDelegate {
 

    
    @IBOutlet weak var tableView: UITableView!
    var bannerImages       = [[String:String]]()
    var banner_image_array = [String]()
    var layoutImages       = [[String:String]]()
    let bounds             = UIScreen.main.bounds
  
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        self.sendRequest(url: "mobiconnectadvcart/index/getSimpleLayout", params: nil)
        self.sendRequest(url: "mobiconnect/index/gethomepagebanner", params: nil)
        // Do any additional setup after loading the view.
    
        
    }
    

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func recieveResponse(data: Data?, requestUrl:String? , response: URLResponse?) {
        if(requestUrl == "mobiconnect/index/gethomepagebanner"){
            guard let json = try? JSON(data:data!) else{return;}
            print(json)
            if(json["data"]["status"].stringValue == "enabled"){
            self.parseJsonData(json: json, index: "banner")
            self.banner_image_array = [String]()
            for image in self.bannerImages{                self.banner_image_array.append(image["banner_image"]!)
            }
           }
        }
        if(requestUrl == "mobiconnectadvcart/index/getSimpleLayout"){
            guard let json = try? JSON(data: data!) else{return;}
            self.parseJsonData(json: json, index: "banner1")
           
        }
        tableView.reloadData()
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(section == 0){
            if(banner_image_array.count == 0){
                return 0
            }
            return 1
        }
        if(section == 1){
            if(layoutImages.count == 0){
                return 0
            }
            return 1
        }
        return 1
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.section == 0){
        let cell = tableView.dequeueReusableCell(withIdentifier: "bannerCell") as! homePageBannerCell
        cell.banner_image_array = banner_image_array
        cell.bannerPage.delegate = self
        cell.bannerPage.dataSource = self
        cell.bannerPage.slideshowTimeInterval = UInt(2.5)
        cell.bannerPage.imageCounterDisabled = true
        return cell
        }else {
           let cell = tableView.dequeueReusableCell(withIdentifier: "themeTwoCell") as! cedMagethemetwotable
            cell.datasource = layoutImages
            print("VV\(layoutImages)")
            cell.sbounds = bounds
            cell.parent = self
            return cell
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(indexPath.section == 0){
           return bounds.height/2
        }else{
            return bounds.width/2*3
        }
    }
    
    //MARK: Delegate for KIImage Pager
    public func array(withImages pager: KIImagePager!) -> [Any]! {
        if banner_image_array.count == 0 {
            return ["placeholder" as AnyObject]
        }
        return banner_image_array as [AnyObject]?
    }
    
    
    func contentMode(forImage image: UInt, in pager: KIImagePager!) -> UIView.ContentMode {
        return UIView.ContentMode.scaleToFill
    }
    
    func placeHolderImage(for pager: KIImagePager!) -> UIImage! {
        return UIImage(named: "placeholder")
    }
    
    func imagePager(_ imagePager: KIImagePager!, didSelectImageAt index: UInt) {
        let banner = bannerImages[Int(index)]
        print(banner)
        if(banner["link_to"] == "category"){
            
            let viewcontrol = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "defaultCategory") as! cedMageDefaultCategory
            viewcontrol.selectedCategory = banner["product_id"]!
            self.navigationController?.pushViewController(viewcontrol, animated: true)
        }else if (banner["link_to"] == "product"){
            
            let productview = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "rootPageViewController") as! cedMageProductViewRoot
            productview.pageData = [["product_id":banner["product_id"]!]]
            let instance = cedMage.singletonInstance
            instance.storeParameterInteger(parameter: 0)
            self.navigationController?.pushViewController(productview
                , animated: true)
        }else if (banner["link_to"] == "website"){
            let url = banner["product_id"]
            let viewcontrol = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "cmsWebView") as! cedMageCmsWebView
            viewcontrol.pageUrl = url!
            self.navigationController?.pushViewController(viewcontrol, animated: true)
            
            
        }
 
    }
    
    //MARK: Parse Product Data from json response
    private func parseJsonData(json:JSON,index:String){
        
        if(index == "banner"){
            for result in json["data"][index].arrayValue {
                let product_id = result["product_id"].stringValue
                let id = result["id"].stringValue
                let title = result["title"].stringValue
                let link_to = result["link_to"].stringValue
                let banner_image = result["banner_image"].stringValue
                let products_obj = ["product_id":product_id, "title":title,"link_to":link_to,"banner_image":banner_image,"id":id]
                bannerImages.append(products_obj)
                
            }
        }
          else if(index == "banner1") {
                for result in json["data"]["banner"].arrayValue {
                let product_id = result["product_id"].stringValue
                let id = result["id"].stringValue
                let title = result["title"].stringValue
                let link_to = result["link_to"].stringValue
                let banner_image = result["banner_image"].stringValue
                let products_obj = ["product_id":product_id, "title":title,"link_to":link_to,"banner_image":banner_image,"id":id]
                self.layoutImages.append(products_obj)
                }
            }
            return
            
        
        
        
    }
    
    
    
}
