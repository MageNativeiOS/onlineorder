//
//  OrderReviewController.swift
//  MageNative Magento Platinum
//
//  Created by Hamza Usmani on 04/09/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class OrderReviewController: MagenativeUIViewController {
    
    //MARK: - Properties
    var payment_method = ""
    var email_id = ""
    var totalOrderData = [String:String]()
    lazy var headingLabel:UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.backgroundColor = Settings.themeColor
        label.text = "Order Summary"
        label.font = .systemFont(ofSize: 16, weight: .semibold)
        label.layer.cornerRadius = 5.0
        label.clipsToBounds = true
        label.textAlignment = .center
        return label
    }()
    
    lazy var summaryTable:UITableView = {
        let table = UITableView()
        table.separatorStyle = .none
        table.backgroundColor = .mageSecondarySystemBackground
        table.register(OrderSummaryItemTC.self, forCellReuseIdentifier: OrderSummaryItemTC.reuseID)
        table.register(OrderSummaryTotalTC.self, forCellReuseIdentifier: OrderSummaryTotalTC.reuseID)
        table.delegate =  self
        table.dataSource =  self
        return table
    }()
    
    lazy var footerView:UIView = {
        let view = UIView()
        view.backgroundColor = .mageSystemBackground
        
        let label = UILabel()
        label.text = "* VAT Included where applicable"
        label.textColor = .mageSecondaryLabel
        label.font = .systemFont(ofSize: 11, weight: .semibold)
        
        view.addSubview(label)
        label.anchor(top: view.topAnchor, left: view.leadingAnchor, paddingTop: 8, paddingLeft: 16)
        
        view.addSubview(proceedButton)
        proceedButton.anchor(left: view.leadingAnchor, bottom: view.bottomAnchor, right: view.trailingAnchor, paddingLeft: 16, paddingBottom: 16, paddingRight: 16, height: 35)
        return view
    }()
    
    lazy var proceedButton:UIButton = {
        let button = UIButton(type: .system)
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = Settings.themeColor
        button.titleLabel?.font = .systemFont(ofSize: 15, weight: .semibold)
        button.layer.cornerRadius = 5.0
        button.addTarget(self, action: #selector(proceedBtnTapped(_:)), for: .touchUpInside)
        return button
    }()
    
    var summaryModel: OrderSummaryModel?
    
    //MARK: - Lifecycles
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchOrderSummary()
        configureUI()
    }
    
    
    init(payment_method:String,email_id:String,totalOrderData:[String:String]) {
        
        self.payment_method = payment_method
        self.email_id = email_id
        self.totalOrderData = totalOrderData
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    //MARK: - Api Methods
    
    fileprivate func fetchOrderSummary() {
        OrderSummaryService.shared.getSummaryData(controller: self, completion: { summary in
            self.summaryModel = summary
            DispatchQueue.main.async {
                self.summaryTable.reloadData()
                for dt in self.summaryModel?.segments ?? []{
                    if dt.label == "Grand Total"{
                        self.proceedButton.setTitle("Complete Checkout (\(dt.value))", for: .normal)
                    }
                }
            }
        })
    }
    
    //MARK: - Selectors
    
    
    //MARK: - Helper Methods
    
    private func configureUI() {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: summaryTable.frame.width, height: 50))
        headerView.addSubview(headingLabel)
        headingLabel.anchor(top: headerView.topAnchor, left: headerView.leadingAnchor, bottom: headerView.bottomAnchor, right: headerView.trailingAnchor,
                            paddingTop: 10, paddingLeft: 16, paddingBottom: 10, paddingRight: 16)
        summaryTable.tableHeaderView = headerView
        
        view.addSubview(summaryTable)
        summaryTable.addConstraintsToFillView(view)
        
        view.addSubview(footerView)
        footerView.anchor(left: view.leadingAnchor, bottom: view.bottomAnchor, right: view.trailingAnchor, height: 80)
    }
}

//MARK:- UITableViewDatasSource

extension OrderReviewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 1 ? (summaryModel?.products.count ?? 0) : 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: OrderSummaryItemTC.reuseID, for: indexPath) as! OrderSummaryItemTC
            cell.populate(with: summaryModel!.products[indexPath.row], totalItems: summaryModel?.products.count ?? 0)
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: OrderSummaryTotalTC.reuseID, for: indexPath) as! OrderSummaryTotalTC
            cell.populate(with: summaryModel?.segments ?? [])
            return cell
        }
    }
}

//MARK:- UITableViewDelegate

extension OrderReviewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return indexPath.section == 0 ? 145 : 160
    }
    
    @objc func proceedBtnTapped(_ sender:UIButton){
        
        let storyboard = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil);
        let viewController = storyboard.instantiateViewController(withIdentifier: "paymentView") as! cedMagePayementView;
        viewController.isOrderReview = true;
        viewController.selectedPaymentMethod = self.payment_method
        viewController.orderEmail = self.email_id;
        viewController.total = self.totalOrderData
        self.navigationController?.pushViewController(viewController, animated: true);
        
    }
}
