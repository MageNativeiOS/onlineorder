//
//  lcgproCompareController.swift
//  MageNative Magento Platinum
//
//  Created by cedcoss on 16/05/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class lcgproCompareController: MagenativeUIViewController {

    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    var compareData = compareDataModel()
    var compareKeys = [String]()
    //var defaults = UserDefaults.standard
          //let userInfoDict                        = UserDefaults.standard.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
             let storeId                             = UserDefaults.standard.value(forKey: "storeId") as? String
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        loadCompareController()
        //reloadViews()
        headingLabel.text = "COMPARE YOUR PRODUCTS"
    }
    func reloadViews(){
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.reloadData()
    }
    
    func loadCompareController(){
        var param = [String:String]()
               
                         
               if UserDefaults.standard.bool(forKey: "isLogin"){
                   if(UserDefaults.standard.object(forKey: "userInfoDict") != nil){
                       let userInfoDict = UserDefaults.standard.object(forKey: "userInfoDict") as! [String:String]
                   param["customer_id"] = userInfoDict["customerId"] ?? ""
                   }
                }else{
                            print("not login")
                    }
        self.sendRequest(url: "mobiconnect/listcompare", params: param)
              // param["store_id"] = "1"
        //self.makeRequestToAPI("mobiconnect/listcompare",dataToPost: param);
        
        
               /*mageNetwork.shared.sendrequestForData(with: "mobiconnect/listcompare", params:param, requestType: .POST, controller: self) { (data, error, response) in
                   guard let data                  = data else { return }
                   do {
                       //   print( try JSON(data: data ))
                       let json                    = try JSON(data: data)
                       print(json)
                      let data                = try json[0]["data"].rawData(options: [])
                      self.compareData               = try JSONDecoder().decode(compareDataModel.self, from: data)
                      print(self.compareData)
                    self.reloadViews()
                   } catch {
                       print(error.localizedDescription)
                   }
               }*/
           
        //  if(jsonResponse.stringValue == "return"){
//            self.renderNoDataImage(view: self, imageName: "noProduct")
//            return
//          }
    }
    
    override func recieveResponse(data: Data?, requestUrl: String?, response: URLResponse?) {
        if let data = data{
            do{
                if(requestUrl == "mobiconnect/listcompare"){
                    let json                    = try JSON(data: data)
                    print(json)
                   let data                = try json[0]["data"].rawData(options: [])
                   self.compareData               = try JSONDecoder().decode(compareDataModel.self, from: data)
                   print(self.compareData)
                    if(self.compareData.comparable_attributes?.count ?? 0 > 0){
                        
                        self.compareKeys = Array(self.compareData.comparable_attributes![0].keys)
                    }
                    if(self.compareData.products?.count == 0 || self.compareData.products == nil){
                        self.view.makeToast("No products to compare", duration: 2.0, position: .center);
                        return;
                    }
                 self.reloadViews()
                }
                else if(requestUrl == "mobiconnect/remove"){
                    let json                    = try JSON(data: data)
                    print(json)
                    if json[0]["status"].stringValue == "true"{
                        self.view.makeToast(json[0]["data"]["message"].stringValue, duration: 2.0, position: .bottom)
                    }else{
                       self.view.makeToast(json[0]["data"]["message"].stringValue, duration: 2.0, position: .bottom)
                    }
                    self.loadCompareController()
                }
                else if(requestUrl == "mobiconnect/checkout/add/"){
                    
                        var json = try JSON(data:data)
                        print(json)
                        json = json[0]
                     cedMageLoaders.removeLoadingIndicator(me: self);
                        if json["cart_id"]["success"].boolValue == true {
                         self.view?.makeToast(json["cart_id"]["message"].stringValue, duration: 2.0, position: .bottom)
                            UserDefaults.standard.setValue(json["cart_id"]["cart_id"].stringValue, forKey: "cartId")
                            UserDefaults.standard.setValue(json["cart_id"]["items_count"].stringValue, forKey: "items_count")
                            //cedMage().setcartCount(tabBar: self.controller?.tabBarController)
                        cedMageViewController().setCartCount(view:self,items: json["cart_id"]["items_count"].stringValue)
                         
                        }
                        else{
                            self.view?.makeToast(json["cart_id"]["message"].stringValue, duration: 2.0, position: .bottom)
                        }
                    
                }
            }
            catch{
                
            }
            
        }
    }

}
extension lcgproCompareController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch section{
            case 0:
                return 1
            default:
                return self.compareData.products?.count ?? 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch indexPath.section{
        case 0:
             let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "compareCC", for: indexPath) as! compareCC
             //cell.skuLabel.text = "SKU"
             //cell.skuPrice.text = "PRIX PAR KG"
             cell.hiddenLabel.text = "Compare Your Products".uppercased()
             cell.productImage.isHidden = true
             cell.productNameLabel.isHidden = true
             cell.priceLabel.isHidden = true
             cell.hiddenLabel.isHidden = false
             cell.addToCartBtn.isHidden = true
             cell.removeProdBtn.isHidden = true
            cell.wrapperView.layer.borderColor = UIColor(hexString: Settings.textColor)?.cgColor
             cell.wrapperView.layer.borderWidth = 0.8
             cell.wrapperView.layer.cornerRadius = 3
            for index in cell.stack.arrangedSubviews{
                index.removeFromSuperview()
            }
        
            
            let customIndex = compareData.comparable_attributes![indexPath.row]
            for(key) in compareKeys{
                let label = UILabel()
                cell.stack.addArrangedSubview(label)
                label.translatesAutoresizingMaskIntoConstraints = false;
                label.heightAnchor.constraint(equalToConstant: 55).isActive = true;
                label.text = key
                label.numberOfLines = 0
                label.font = .boldSystemFont(ofSize: 15)
                label.textAlignment = .center
                let lineView = UIView()
                cell.stack.addArrangedSubview(lineView)
                lineView.translatesAutoresizingMaskIntoConstraints = false;
                lineView.backgroundColor = .black
                lineView.heightAnchor.constraint(equalToConstant: 1).isActive = true;
            }
            return cell
        default:
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "compareCC", for: indexPath) as! compareCC
        cell.productImage.sd_setImage(with: URL(string:self.compareData.products?[indexPath.row].product_image ?? ""), placeholderImage: UIImage(named:"placeholder"))
        cell.priceLabel.text = self.compareData.products?[indexPath.row].regular_price
        cell.productNameLabel.text = self.compareData.products?[indexPath.row].product_name
               cell.productNameLabel.numberOfLines = 0
            cell.productNameLabel.textColor = UIColor(hexString: Settings.textColor)
            for index in cell.stack.arrangedSubviews{
                index.removeFromSuperview()
            }
            cell.productNameLabel.font = .boldSystemFont(ofSize: 12)
        cell.addToCartBtn.setTitleColor(.white ,for: .normal)
            cell.addToCartBtn.backgroundColor = Settings.themeColor
            let customIndex = compareData.comparable_attributes![indexPath.row]
            for(key) in compareKeys{
                let label = UILabel()
                cell.stack.addArrangedSubview(label)
                label.translatesAutoresizingMaskIntoConstraints = false;
                label.heightAnchor.constraint(equalToConstant: 55).isActive = true;
                label.text = customIndex[key]
                label.numberOfLines = 0
                label.font = .systemFont(ofSize: 12)
                label.textAlignment = .center
                let lineView = UIView()
                cell.stack.addArrangedSubview(lineView)
                lineView.translatesAutoresizingMaskIntoConstraints = false;
                lineView.backgroundColor = .black
                lineView.heightAnchor.constraint(equalToConstant: 1).isActive = true;
            }
//        cell.skuLabel.text = self.compareData.comparable_attributes?[indexPath.row].SKU
//        cell.skuLabel.numberOfLines = 0
//        cell.skuPrice.text = self.compareData.comparable_attributes?[indexPath.row].prixKg
        cell.addToCartBtn.tag = indexPath.row
        if (self.compareData.products?[indexPath.row].type == "simple" || self.compareData.products?[indexPath.row].type == "virtual"){
            
            cell.addToCartBtn.addTarget(self, action: #selector(addToCart(_:)), for: .touchUpInside)
        }else{
           
            cell.addToCartBtn.addTarget(self, action: #selector(goToProductView(_:)), for: .touchUpInside)
        }
        cell.wrapperView.layer.cornerRadius = 3
            cell.wrapperView.layer.borderColor = UIColor(hexString: Settings.textColor)?.cgColor
                    cell.wrapperView.layer.borderWidth = 0.8
        cell.removeProdBtn.tag = indexPath.row
        cell.removeProdBtn.addTarget(self, action: #selector(removeProductTapped(_:)), for: .touchUpInside)
        return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.frame.width / 2.5 , height: CGFloat(300 + (compareData.comparable_attributes![indexPath.row].values.count*60)))
    }
    //MARK:- Remove Product
    @objc
    func removeProductTapped(_ sender:UIButton){
        var param = [String:String]()
        
        
        if UserDefaults.standard.bool(forKey: "isLogin"){
            if(UserDefaults.standard.object(forKey: "userInfoDict") != nil){
                let userInfoDict = UserDefaults.standard.object(forKey: "userInfoDict") as! [String:String]
                param["customer_id"] = userInfoDict["customerId"] ?? ""
            }}
        
        param["prodID"] = self.compareData.products?[sender.tag].product_id ?? ""
        sendRequest(url: "mobiconnect/remove", params: param)
        /*mageNetwork.shared.sendrequestForData(with: "mobiconnect/remove", params:param, requestType: .POST, controller: self) { (data, error, response) in
            guard let data                  = data else { return }
            do {
                //   print( try JSON(data: data ))
                let json                    = try JSON(data: data)
                print(json)
                if json[0]["status"].stringValue == "true"{
                    self.view.makeToast(json[0]["data"]["message"].stringValue, duration: 2.0, position: .bottom)
                }else{
                   self.view.makeToast(json[0]["data"]["message"].stringValue, duration: 2.0, position: .bottom)
                }
                self.loadCompareController()
                
            } catch {
                print(error.localizedDescription)
            }
            
        }*/
    }
    
    //MARK:- AddToCart
    @objc
    func addToCart(_ sender:UIButton){
     if self.compareData.products?[sender.tag].stock_status == false{
        self.view.makeToast("Product you are try to add is not available!".localized, duration: 2.0, position: .bottom)
         return;
     }
     
        var param = [String:String]()
        //         "qty" : "1"
        let cart_id = UserDefaults.standard.value(forKey: "cartId") as? String
        if(defaults.object(forKey: "userInfoDict") != nil){
            param["hashkey"]=userInfoDict["hashKey"]!
            param["customer_id"]=userInfoDict["customerId"]!;
        }
        param["qty"] = "1"
     param["type"] = self.compareData.products?[sender.tag].type
     param["product_id"] = self.compareData.products?[sender.tag].product_id
        if cart_id != "" {
            param["cart_id"] = cart_id
        }else{param["cart_id"] = "0"}
        sendRequest(url: "mobiconnect/checkout/add/", params: param)
        /*mageNetwork.shared.sendrequestForData(with: "mobiconnect/checkout/add/", params: param, requestType: .POST, controller: self) { (data, error, response) in
            
            guard let data = data else{return}
            do{
                var json = try JSON(data:data)
                print(json)
                json = json[0]
             cedMageLoaders.removeLoadingIndicator(me: self);
                if json["cart_id"]["success"].boolValue == true {
                 self.view?.makeToast(json["cart_id"]["message"].stringValue, duration: 2.0, position: .bottom)
                    UserDefaults.standard.setValue(json["cart_id"]["cart_id"].stringValue, forKey: "cartId")
                    UserDefaults.standard.setValue(json["cart_id"]["items_count"].stringValue, forKey: "items_count")
                    //cedMage().setcartCount(tabBar: self.controller?.tabBarController)
                cedMageViewController().setCartCount(view:self,items: json["cart_id"]["items_count"].stringValue)
                 
                }
                else{
                    self.view?.makeToast(json["cart_id"]["message"].stringValue, duration: 2.0, position: .bottom)
                }
            }catch let error {
                print(error.localizedDescription)
            }
        }*/
    }
    @objc func goToProductView(_ sender:UIButton){
          let storyboard = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil);
               let viewController = storyboard.instantiateViewController(withIdentifier: "productSinglePageViewController") as! ProductSinglePageViewController;
          viewController.product_id = self.compareData.products?[sender.tag].product_id ?? ""
               self.navigationController?.pushViewController(viewController, animated: true)
      }
}
