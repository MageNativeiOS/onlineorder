//
//  compareCC.swift
//  MageNative Magento Platinum
//
//  Created by cedcoss on 16/05/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class compareCC: UICollectionViewCell {
    @IBOutlet weak var wrapperView: UIView!
    
    @IBOutlet weak var priceLabel: UILabel!
       @IBOutlet weak var productNameLabel: UILabel!
       @IBOutlet weak var productImage: UIImageView!
       @IBOutlet weak var addToCartBtn: UIButton!
    
    @IBOutlet weak var removeProdBtn: UIButton!
    @IBOutlet weak var hiddenLabel: UILabel!
    
    @IBOutlet weak var v1: UIView!
    
    @IBOutlet weak var stack: UIStackView!
}
