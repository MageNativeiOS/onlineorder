//
//  compareModel.swift
//  MageNative Magento Platinum
//
//  Created by cedcoss on 16/05/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import Foundation

struct compareDataModel:Decodable{
     var status : String?
     var products :[compareProduct]?
    var comparable_attributes : [[String:String]]?
}
struct compareProduct:Decodable{
    var Inwishlist : String?
    var type : String?
    var special_price : String?
    var stock_status : Bool?
    var product_id : String?
    var regular_price :  String?
    var description : String?
    //var review : Int?
    var product_name : String?
    var product_image : String?
    var wishlist_item_id : String?
}

