/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */


import UIKit

class cedMageHomeNavigation: homedefaultNavigation {
  
    var currentViewController:UIViewController?
    
    lazy var customerSupportButton:UIButton = {
        let button = UIButton(type: .system)
        button.setImage(UIImage(named:"support_icon")?.withRenderingMode(.alwaysTemplate), for: .normal)
        button.tintColor = .white
        button.layer.cornerRadius = 25
        button.backgroundColor = Settings.themeColor
        button.addTarget(self, action: #selector(handleCustomerSupport), for: .touchUpInside)
        return button
    }()
    
    
    
  override func viewDidLoad() {
    super.viewDidLoad()
    view.addSubview(customerSupportButton)
    customerSupportButton.anchor(left: view.leadingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, paddingLeft: 16, paddingBottom: 48,
                                 width: 50, height: 50)
    
    customerSupportButton.transform = CGAffineTransform(scaleX: 0.2, y: 0.2)
    customerSupportButton.alpha = 0
    
    UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.4, initialSpringVelocity: 1.0, options: .curveEaseInOut, animations: {
        self.customerSupportButton.transform = CGAffineTransform.identity
        self.customerSupportButton.alpha = 1
    })
    
    
    
    if UserDefaults.standard.value(forKey: "userCurrentLocation") == nil {
        setViewControllers([MapController()], animated: true)
    } else {
        let vc = HomeController()
        setViewControllers([vc], animated: true)
    }
    
    
    
  }
    override func viewWillAppear(_ animated: Bool) {
        if let _ = Settings.currentLocation {
            let vc = HomeController()
            setViewControllers([vc], animated: true)
        } else {
            let vc = MapController()
            setViewControllers([vc], animated: true)
        }
    }
  
    @objc private func handleCustomerSupport() {
        let vc = UIStoryboard(name: "cedMageAccounts", bundle: nil).instantiateViewController(withIdentifier: "cmsWebView") as! cedMageCmsWebView
        vc.pageUrl = "https://cedcommerce.com/magenativeapp.html?name=Magenativemagento2"
        pushViewController(vc, animated: true)
    }
    
    
  
  func addToggleButton(me:UIViewController){
    let toglebut = UIButton()
    toglebut.frame.size = CGSize(width:25, height:25)
    toglebut.setBackgroundImage(UIImage(named: "hamp"), for: UIControl.State.normal)
    toglebut.tintColor = UIColor.white
    
    toglebut.addTarget(self, action: #selector(toggleDrawer), for: UIControl.Event.touchUpInside)
    let togglebutton = UIBarButtonItem(customView: toglebut)
    let backButton = UIButton(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
    backButton.addTarget(self, action: #selector(backfunc(sender:)), for: .touchUpInside)
    let img1=UIImage(named:"backArrow")
    let defaults=UserDefaults.standard
    let value=defaults.value(forKey: "mageAppLang") as! [String]
    if value[0]=="ar"
    {
      //cell.rightView.image=UIImage(cgImage: (img1?.cgImage)!, scale: (img1?.scale)!, orientation: UIImageOrientation.downMirrored)
      
      backButton.setImage(UIImage(cgImage: (img1?.cgImage)!, scale: (img1?.scale)!, orientation: UIImage.Orientation.upMirrored), for: UIControl.State.normal)
      backButton.backgroundColor=UIColor.black
    }
    else
    {
      backButton.setImage(UIImage(named:"backArrow"), for: .normal)
      backButton.backgroundColor=UIColor.clear
    }
    currentViewController = me
    backButton.tintColor = .white
    let backBarButton = UIBarButtonItem(customView: backButton)
    if let me = (me as? HomeController) {
      me.navigationItem.leftBarButtonItem = togglebutton
    } else if let me = me as? CartViewController {
        me.navigationItem.leftBarButtonItem = togglebutton
    } else if let me = me as? TreeViewController {
        me.navigationItem.leftBarButtonItem = togglebutton
    } else if let me = me as? cedMageWishList {
        me.navigationItem.leftBarButtonItem = togglebutton
    } else if let me = (me as? OrderPlacedViewController) {
      me.navigationItem.leftBarButtonItem = togglebutton      
    } else {
      me.navigationItem.leftBarButtonItems = [backBarButton,togglebutton]
    }
  }
  
  

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
}
