//
//  adminInboxController.swift
//  MageNative Magento Platinum
//
//  Created by Saumya Kashyap on 25/11/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class adminInboxController: MagenativeUIViewController ,UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var adminComposeButton: UIButton!
   
	@IBOutlet var tableView: UITableView!
   var currentPage = 1
    
    
	var adminInboxArray = [adminInbox]()
	
	
    override func viewDidLoad() {
        super.viewDidLoad()
//		tableView.delegate = self
//		tableView.dataSource = self
		adminRequest()
        adminAction()
		
       
    }
    
    func adminAction(){
        adminComposeButton.layer.cornerRadius = 5
        adminComposeButton.setThemeColor()
        adminComposeButton.addTarget(self, action: #selector(adminComposeAction(_:)), for: .touchUpInside)
    }
    @objc func adminComposeAction(_ sender : UIButton){
        let viewController = UIStoryboard(name: "cedMageAccounts" ,bundle:  nil).instantiateViewController(withIdentifier: "vendorComposeMsgController") as! vendorComposeMsgController
        viewController.isFromAdmin = true
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    func adminRequest(){
        let storeId = UserDefaults.standard.value(forKey: "storeId") as! String
        let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
        let customerId = userInfoDict["customerId"]!;
        self.sendRequest(url: "mobimessaging/getadmininbox", params: ["customer_id":customerId,"store_id":storeId,"page":"\(currentPage)"])
    }
	override func recieveResponse(data: Data?, requestUrl: String?, response: URLResponse?) {
		guard let data = data else {return}
		
         do {
                   var json = try JSON(data: data)
                   print(json)
                   json = json[0]

            if json["response"]["status"].stringValue == "false" {
                self.view.makeToast(json["response"]["message"].stringValue, duration: 1.0, position: .center)
                tableView.isHidden = true
                return
            }
            
            for list in json["response"]["inbox_list"].arrayValue {
                let temp = adminInbox(sender_name: list["sender_name"].stringValue,
                                      sender: list["sender"].stringValue,
                                      id: list["id"].stringValue,
                                      vendor_id: list["vendor_id"].stringValue,
                                      created_at: list["created_at"].stringValue,
                                      subject: list["subject"].stringValue,
                                      message: list["message"].stringValue,
                                      new_message: list["new_message"].stringValue,
                                      updated_at: list["updated_at"].stringValue,
                                      receiver_name: list["receiver_name"].stringValue)
                
                adminInboxArray.append(temp)
            }
            tableView.delegate = self
            tableView.dataSource = self
            tableView.reloadData()
            tableView.isHidden = false
         } catch let error {
            print(error.localizedDescription)
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return adminInboxArray.count
       }
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		
		let cell = tableView.dequeueReusableCell(withIdentifier: "admininboxcell", for: indexPath) as! adminInboxCell
		cell.cellCardView.cardView()
       
        cell.adminSenderLabel.text = adminInboxArray[indexPath.row].sender
        cell.adminRecieverLabel.text = adminInboxArray[indexPath.row].receiver_name
        cell.adminCreatedByLabel.text = adminInboxArray[indexPath.row].created_at
        cell.adminUpdatedAtLabel.text = adminInboxArray[indexPath.row].updated_at
        cell.adminSubjectLabel.text = adminInboxArray[indexPath.row].subject
        cell.newMessageLabel.text = adminInboxArray[indexPath.row].new_message
        cell.adminViewBtn.addTarget(self, action: #selector(viewButtontapped(_:)), for: .touchUpInside)
        cell.adminViewBtn.setThemeColor()
        cell.adminViewBtn.tag = Int(adminInboxArray[indexPath.row].id) ?? 0
		return cell
		
		
	}
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return 260
	}
	
    @objc func viewButtontapped(_ sender: UIButton) {
       let viewController = UIStoryboard(name: "cedMageAccounts" ,bundle:  nil).instantiateViewController(withIdentifier: "chatViewController") as! chatViewController
        viewController.id = "\(sender.tag)"
        viewController.isFromVendor = false
        self.navigationController?.pushViewController(viewController, animated: true)
    }
	
}

