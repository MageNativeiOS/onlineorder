/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */

import UIKit

extension CartViewController {

    func renderCouponAndPriceSectionOfCartPage(){
        
        let couponAndPriceSectionContainerStackView = UIStackView(); // outermost stackview
        couponAndPriceSectionContainerStackView.translatesAutoresizingMaskIntoConstraints = false;
        couponAndPriceSectionContainerStackView.axis  = NSLayoutConstraint.Axis.vertical;
        couponAndPriceSectionContainerStackView.distribution  = UIStackView.Distribution.equalSpacing;
        couponAndPriceSectionContainerStackView.alignment = UIStackView.Alignment.center;
        couponAndPriceSectionContainerStackView.spacing   = 0.0;
        stackView.addArrangedSubview(couponAndPriceSectionContainerStackView);
        stackView.addConstraint(NSLayoutConstraint(item: couponAndPriceSectionContainerStackView, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: stackView, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: padding/2));
        stackView.addConstraint(NSLayoutConstraint(item: couponAndPriceSectionContainerStackView, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: stackView, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: -padding/2));
        
        
        let backgroundViewForStackView = UIView(); // uiview for covering stackview
        backgroundViewForStackView.translatesAutoresizingMaskIntoConstraints = false;
       // backgroundViewForStackView.backgroundColor = UIColor(hexString: "#BFBFBF");
     
        couponAndPriceSectionContainerStackView.addArrangedSubview(backgroundViewForStackView); // adding uiview to stackview
        couponAndPriceSectionContainerStackView.addConstraint(NSLayoutConstraint(item: backgroundViewForStackView, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: couponAndPriceSectionContainerStackView, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 0));
        couponAndPriceSectionContainerStackView.addConstraint(NSLayoutConstraint(item: backgroundViewForStackView, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: couponAndPriceSectionContainerStackView, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 0));
        
        let couponAndPriceSectionStackView = UIStackView(); // main actual stackview that will contain all couponAndPriceSection-options
        couponAndPriceSectionStackView.translatesAutoresizingMaskIntoConstraints = false;
        couponAndPriceSectionStackView.axis  = NSLayoutConstraint.Axis.vertical;
        couponAndPriceSectionStackView.distribution  = UIStackView.Distribution.equalSpacing;
        couponAndPriceSectionStackView.alignment = UIStackView.Alignment.center;
        couponAndPriceSectionStackView.spacing   = 0.0;
        backgroundViewForStackView.addSubview(couponAndPriceSectionStackView); // adding stackview to uiview
        backgroundViewForStackView.addConstraint(NSLayoutConstraint(item: couponAndPriceSectionStackView, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: backgroundViewForStackView, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 0));
        backgroundViewForStackView.addConstraint(NSLayoutConstraint(item: couponAndPriceSectionStackView, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: backgroundViewForStackView, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 0));
        backgroundViewForStackView.addConstraint(NSLayoutConstraint(item: couponAndPriceSectionStackView, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: backgroundViewForStackView, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1, constant: padding/2));
        backgroundViewForStackView.addConstraint(NSLayoutConstraint(item: couponAndPriceSectionStackView, attribute: NSLayoutConstraint.Attribute.bottom, relatedBy: NSLayoutConstraint.Relation.equal, toItem: backgroundViewForStackView, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: -padding/2));
        
        // render apply coupon section
        let applyCouponOnCartView = ApplyCouponOnCartView();
        applyCouponOnCartView.translatesAutoresizingMaskIntoConstraints = false;
        applyCouponOnCartView.applyCouponButton.fontColorTool()
        applyCouponOnCartView.couponTextField.layer.borderColor = UIColor.gray.cgColor;
        applyCouponOnCartView.couponTextField.layer.borderWidth = 1.0;
        if(self.defaults.object(forKey: "appliedCoupon") == nil){
            applyCouponOnCartView.applyCouponButton.setTitle("Apply".localized, for: UIControl.State.normal);
        }
        else{
            applyCouponOnCartView.applyCouponButton.setTitle("Remove".localized, for: UIControl.State.normal);
        }
        
        applyCouponOnCartView.applyCouponButton.titleLabel?.font = UIFont(fontName: "HelveticaNeue-Medium", fontSize: CGFloat(17.0));
        applyCouponOnCartView.applyCouponButton.addTarget(self, action: #selector(CartViewController.applyCouponOnCart(_:)), for: UIControl.Event.touchUpInside);
        
        couponAndPriceSectionStackView.addArrangedSubview(applyCouponOnCartView);
        let applyCouponOnCartViewHeight = translateAccordingToDevice(CGFloat(40.0));
        applyCouponOnCartView.heightAnchor.constraint(equalToConstant: applyCouponOnCartViewHeight).isActive = true;
        self.setLeadingAndTralingSpaceFormParentView(applyCouponOnCartView, parentView:couponAndPriceSectionStackView);
        if(isOrderReview){
            applyCouponOnCartView.isHidden = true;
        }
        
        let priceView = UIStackView()
        priceView.distribution = .equalSpacing
        priceView.spacing = 5
        priceView.axis = .vertical
        for index in priceArray{
            let label = UILabel()
            label.translatesAutoresizingMaskIntoConstraints = false
            label.textAlignment = .right
            label.font = .boldSystemFont(ofSize: 18)
            priceView.addArrangedSubview(label)
            label.heightAnchor.constraint(equalToConstant: 40).isActive = true;
            label.text = index["label"]!+":"+index["value"]!
        }
        /*for (key,totalValue) in total {
            //            let priceRelatedLabel = UILabel();
            //            priceRelatedLabel.fontColorTool()
            //            priceRelatedLabel.translatesAutoresizingMaskIntoConstraints = false;
            if(key == "amounttopay"){
                priceView.SubTotal.text = "SubTotal".localized+" : "+totalValue;
            }
            else if(key == "shipping_amount"){
                priceView.shippingAmount.text = "Shipping Amount".localized+" : "+totalValue;
            }
            else if(key == "tax_amount"){
                priceView.taxPrice.text = "Tax Price".localized+" : "+totalValue;
            }
            else if(key == "discount_amount"){
                priceView.discount.text = "Discount".localized+" : "+totalValue;
            }
            else if(key == "grandtotal"){
                priceView.grandTotal.text = "Grand Total".localized+" : "+totalValue;
            }
            //            priceRelatedLabel.textAlignment = NSTextAlignment.right;
            //            priceRelatedLabel.font = UIFont(fontName: "HelveticaNeue-Medium", fontSize: CGFloat(15.0));
            
        }*/
        couponAndPriceSectionStackView.addArrangedSubview(priceView);
        let priceRelatedLabelHeight = translateAccordingToDevice(CGFloat(priceArray.count*40));
        priceView.heightAnchor.constraint(equalToConstant: priceRelatedLabelHeight).isActive = true;
        self.setLeadingAndTralingSpaceFormParentView(priceView, parentView:couponAndPriceSectionStackView);
        
    }
    
    
    @objc func applyCouponOnCart(_ sender:UIButton){
        
        if let applyCouponOnCartView = sender.superview?.superview as? ApplyCouponOnCartView{
            
            let couponCode = applyCouponOnCartView.couponTextField.text!;
            if(self.defaults.object(forKey: "appliedCoupon") == nil){
                if(couponCode == ""){
                    self.view.makeToast("Please Enter Coupon".localized, duration: 2.0, position: .bottom, title: nil, image: nil, style: nil, completion: nil);
                    return;
                }
            }
            
            var urlToRequest = "mobiconnect/checkout/coupon";
            let requestHeader = Settings.headerKey
            let baseURL = Settings.baseUrl
            urlToRequest = baseURL+urlToRequest;
            
            var postString = "";
            var postData = [String:String]()
            if defaults.bool(forKey: "isLogin") {
                postData["hashkey"] = userInfoDict["hashKey"]!
                postData["customer_id"] = userInfoDict["customerId"]!;
                postData["cart_id"] = cart_id;
            }
            else{
                postData["cart_id"] = cart_id;
            }
            
            if(self.defaults.object(forKey: "appliedCoupon") == nil){
                postData["coupon_code"] = couponCode;
            }
            else{
                postData["remove"] = "1";
            }
            
            postString = ["parameters":postData].convtToJson() as String
            
            var request = URLRequest(url: URL(string: "\(urlToRequest)")!);
            request.httpMethod = "POST";
            request.httpBody = postString.data(using: String.Encoding.utf8);
            if UserDefaults.standard.bool(forKey: "isLogin"){
                if let user = UserDefaults.standard.object(forKey: "userInfoDict") as? [String:String] {
                    request.setValue(user["hashKey"], forHTTPHeaderField: "hashkey")
                }
            }
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            cedMageLoaders.addDefaultLoader(me: self);
            let task = URLSession.shared.dataTask(with: request){
                
                // check for fundamental networking error
                data, response, error in
                guard error == nil && data != nil else{
                    print("error=\(error)")
                    DispatchQueue.main.async{
                        print(error?.localizedDescription as Any);
                        cedMageLoaders.removeLoadingIndicator(me: self);
                    }
                    return;
                }
                
                // check for http errors
                if let httpStatus = response as? HTTPURLResponse , httpStatus.statusCode != 200{
                    
                    print("statusCode should be 200, but is \(httpStatus.statusCode)")
                    print("response = \(response)")
                    DispatchQueue.main.async{
                        cedMageLoaders.removeLoadingIndicator(me: self);
                    }
                    return;
                }
                
                // code to fetch values from response :: start
                guard var jsonResponse = try? JSON(data: data!) else{return;}
                if(jsonResponse != nil){
                    
                    DispatchQueue.main.async{
                        jsonResponse = jsonResponse[0]
                        print(jsonResponse);
                        cedMageLoaders.removeLoadingIndicator(me: self);
                        if(jsonResponse["cart_id"]["success"].stringValue == "true"){
                            self.view.makeToast(jsonResponse["cart_id"]["message"].stringValue, duration: 2, position: .center, title: nil, image: nil, style: nil, completion: {
                                Void in
                                print(jsonResponse["cart_id"]["message"].stringValue);
                                if(self.defaults.object(forKey: "appliedCoupon") == nil){
                                    self.defaults.setValue(couponCode, forKey: "appliedCoupon");
                                }
                                else{
                                    self.defaults.removeObject(forKey: "appliedCoupon");
                                }
                                
                                self.resetViewAndVariables();
                                self.getCartData()
                                //self.makeRequestToAPI("mobiconnect/checkout/viewcart",dataToPost: ["cart_id":self.cart_id]);
                            })
                          
                        }
                        else{
                            cedMageHttpException.showAlertView(me: self, msg: jsonResponse["cart_id"]["message"].stringValue, title: "Error".localized)
                            print(jsonResponse["cart_id"]["message"].stringValue);
                            applyCouponOnCartView.couponTextField.text = "";
                        }
                    }
                }
            }
            task.resume();
        }
        
    }

}
