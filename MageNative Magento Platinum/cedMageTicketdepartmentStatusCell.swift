//
//  cedMageTicketdepartmentStatusCell.swift
//  ZeoMarket
//
//  Created by cedcoss on 03/04/19.
//  Copyright © 2019 MageNative. All rights reserved.
//

import UIKit

class cedMageTicketdepartmentStatusCell: UITableViewCell {

    @IBOutlet weak var order: UILabel!
    @IBOutlet weak var ticketCreated: UILabel!
    @IBOutlet weak var departmentValue: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
