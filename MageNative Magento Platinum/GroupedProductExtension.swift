/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */

import UIKit

extension ProductSinglePageViewController{

    func renderGroupedProducts(){
        
        let groupedProductContainerStackView = UIStackView(); // outermost stackview
        groupedProductContainerStackView.translatesAutoresizingMaskIntoConstraints = false;
        groupedProductContainerStackView.axis  = NSLayoutConstraint.Axis.vertical;
        groupedProductContainerStackView.distribution  = UIStackView.Distribution.equalSpacing;
        groupedProductContainerStackView.alignment = UIStackView.Alignment.center;
        groupedProductContainerStackView.spacing   = 0.0;
        stackView.addArrangedSubview(groupedProductContainerStackView);
        stackView.addConstraint(NSLayoutConstraint(item: groupedProductContainerStackView, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: stackView, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 0));
        stackView.addConstraint(NSLayoutConstraint(item: groupedProductContainerStackView, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: stackView, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 0));
        
        
        let backgroundViewForStackView = UIView(); // uiview for covering stackview
        backgroundViewForStackView.translatesAutoresizingMaskIntoConstraints = false;
        backgroundViewForStackView.backgroundColor = UIColor.white;
        //backgroundViewForStackView.makeCard(backgroundViewForStackView, cornerRadius: 2, color: UIColor.black, shadowOpacity: 0.4);
        groupedProductContainerStackView.addArrangedSubview(backgroundViewForStackView); // adding uiview to stackview
        groupedProductContainerStackView.addConstraint(NSLayoutConstraint(item: backgroundViewForStackView, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: groupedProductContainerStackView, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 0));
        groupedProductContainerStackView.addConstraint(NSLayoutConstraint(item: backgroundViewForStackView, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: groupedProductContainerStackView, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 0));
        
        groupedProductsStackView = UIStackView(); // main actual stackview that will contain all grouped-options
        groupedProductsStackView.translatesAutoresizingMaskIntoConstraints = false;
        groupedProductsStackView.axis  = NSLayoutConstraint.Axis.vertical;
        groupedProductsStackView.distribution  = UIStackView.Distribution.equalSpacing;
        groupedProductsStackView.alignment = UIStackView.Alignment.center;
        groupedProductsStackView.spacing   = 10.0;
        backgroundViewForStackView.addSubview(groupedProductsStackView); // adding stackview to uiview
        backgroundViewForStackView.addConstraint(NSLayoutConstraint(item: groupedProductsStackView!, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: backgroundViewForStackView, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: padding/2));
        backgroundViewForStackView.addConstraint(NSLayoutConstraint(item: groupedProductsStackView!, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: backgroundViewForStackView, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: -padding/2));
        backgroundViewForStackView.addConstraint(NSLayoutConstraint(item: groupedProductsStackView!, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: backgroundViewForStackView, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1, constant: padding/2));
        backgroundViewForStackView.addConstraint(NSLayoutConstraint(item: groupedProductsStackView!, attribute: NSLayoutConstraint.Attribute.bottom, relatedBy: NSLayoutConstraint.Relation.equal, toItem: backgroundViewForStackView, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: -padding/2));
        
        for counter in 0  ..< grouped_product_array.count {
            
            // Grouped Product View
            let groupedProductView = GroupedProductView();
            groupedProductView.translatesAutoresizingMaskIntoConstraints = false;
            
            groupedProductView.grpProName.text = grouped_product_array[counter]["group-product-name"];
            groupedProductView.grpProPrice.text = grouped_product_array[counter]["regular_price"];
            groupedProductView.grpProStock.text = grouped_product_array[counter]["stock"];
            if(grouped_product_array[counter]["group-prod-img"] != nil){
                downloadImageFromServer(groupedProductView.grpProImg,urlToRequest:grouped_product_array[counter]["group-prod-img"]!);
            }
            
            groupedProductsStackView.addArrangedSubview(groupedProductView);
            let groupedProductViewHeight = translateAccordingToDevice(CGFloat(150.0));
            groupedProductView.heightAnchor.constraint(equalToConstant: groupedProductViewHeight).isActive = true;
            self.setLeadingAndTralingSpaceFormParentView(groupedProductView,parentView:groupedProductsStackView);
            
        }
        
    }

}
