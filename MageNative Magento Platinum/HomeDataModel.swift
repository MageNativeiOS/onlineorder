//
//  HomeDataModel.swift
//  MageNative Magento Platinum
//
//  Created by Hamza Usmani on 02/09/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import Foundation

struct HomeDataModel: Decodable {
    let title: String?
    let link_type: String?
    let widget: [HomeBanner]?
    let categories: [HomeCategory]?
    let products: [HomeProduct]?
    let banner: HomeBanner?
    let deal_group: HomeDeal?
    let category_id: String?
    let type_of_banner: String?
    let type_of_products: String?
    let category_type: String?
    let banner_orientiation: String?
}

struct HomeBanner: Decodable {
    let product_id: String?
    let banner_image: String?
    let title: String?
    let id: String?
    let link_to: String?
}

struct HomeCategory: Decodable {
    let id:String?
    let image:String?
    let name:String?
}

struct HomeProduct: Decodable {
    let product_image:String?
    let product_id:String?
    let stock_status:Bool?
    let product_name:String?
    let type:String?
    //let review:Int?
    //let offer:Int?
    let wishlist_item_id:String?
    let Inwishlist:String?
    let special_price:String?
    let regular_price:String?
    let description:String?
}

struct HomeDeal: Decodable {
    let content:[HomeDealContent]?
    let deal_duration:Int?
    let title:String?
}

struct HomeDealContent: Decodable {
    let product_id:String?
    let link_to:String?
    let deal_image_name:String?
}
