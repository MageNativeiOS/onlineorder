/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */


import UIKit
//import SkyFloatingLabelTextField
class CheckoutStep2ViewController: MagenativeUIViewController {
  
  
  @IBOutlet weak var imageViewOnTop: UIImageView!
  
  @IBOutlet weak var imageViewOnTopHeight: NSLayoutConstraint!
  
  var checkoutAs = "USER";
  var isPasswordVisible = false;
  
  var isPreviousAddressIsShowing = false;
  
  var flag=false
  var email_id = "test@test.com";
  var indexOfStateField = -1;
  //var addressFieldsArray = ["email", "firstname", "lastname","country", "state", "street", "city", "postcode", "telephone"];
  var addressFieldsArray = ["email","prefix", "firstname","middlename", "lastname","suffix","country", "state", "street", "city", "postcode", "telephone","company"];
  var addressInfoArray = ["email":"Email".localized,"prefix":"Prefix(*)".localized, "firstname":"First Name(*)".localized,"middlename":"Middle Name(*)".localized, "lastname":"Last Name(*)".localized,"suffix":"Suffix(*)".localized, "country":"Country(*)".localized, "state":"State(*)".localized, "street":"Street(*)".localized, "city":"City(*)".localized, "postcode":"Postcode(*)".localized, "telephone":"Phone Number(*)".localized,"company":"Company".localized];
  //var addressInfoArray = ["email":"Email".localized, "firstname":"First Name(*)".localized, "lastname":"Last Name(*)".localized, "country":"Country(*)".localized, "state":"State(*)".localized, "street":"Street(*)".localized, "city":"City(*)".localized, "postcode":"Pincode(*)".localized, "telephone":"Phone Number(*)".localized];
  
  var addressDescriptionArray = ["email":"", "firstname":"", "lastname":"", "country":"", "state":"", "street":"", "city":"", "postcode":"", "telephone":""];
  
  var countryDict = [String: String]();
  var stateDict = [String:String]();
  
  //Order data
  var totalOrderData = [String:String]()
  
  
  
  
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    if let c_id=UserDefaults.standard.value(forKey: "userInfoDict") as? [String:String] {
      
      // Do any additional setup after loading the view.
      if(self.checkoutAs == "USER"){
        if(defaults.object(forKey: "userInfoDict") != nil){
          userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
          self.email_id = userInfoDict["email"]!;
        }
      }
    }
    self.basicFoundationToRenderView(bottomMargin: CGFloat(0.0));
    if(self.checkoutAs == "GUEST" || self.checkoutAs == "USER" && defaults.object(forKey: "selectedAddressId") == nil){
      self.getRequest(url: "mobiconnect/module/getcountry/",store:false)
      
      self.sendRequest(url: "mobiconnect/customer/getRequiredFields/", params: nil)
      // self.makeRequestToAPI("mobiconnect/module/getcountry/",dataToPost: [:]);
    }
    else{
      self.renderCheckoutStep2View();
    }
    
    NotificationCenter.default.addObserver(self, selector: #selector(CheckoutStep2ViewController.addressSelectedOrChanged(_:)), name: NSNotification.Name(rawValue: "addressSelectedOrChangedId"), object: nil);
  }
  var prefix_data:Dictionary<String,String> = [:]
  var suffix_data:Dictionary<String,String> = [:]
  var values_fields:Dictionary<String,JSON>=[:]
  var dropdown_options:Dictionary<String,JSON>=[:]
  var hide_check:Dictionary<String,String> = [:]
  
  @objc func addressSelectedOrChanged(_ notification: NSNotification){
    
    print("addressSelectedOrChanged");
    print(self.isPreviousAddressIsShowing);
    
    if(self.isPreviousAddressIsShowing){
      if let addressDescriptionLabelOld = stackView.arrangedSubviews[2] as? UILabel {
        
        stackView.removeArrangedSubview(addressDescriptionLabelOld);
        addressDescriptionLabelOld.removeFromSuperview();
        let addressDescriptionLabel = UILabel();
        addressDescriptionLabel.numberOfLines = 0;
        addressDescriptionLabel.translatesAutoresizingMaskIntoConstraints = false;
        var addressDescription = defaults.object(forKey: "selectedAddressDescription") as! String;
        print("addressDescription");
        print(addressDescription);
        let addressDescriptionElementsArray = addressDescription.components(separatedBy: "\n");
        addressDescription = "";
        for val in addressDescriptionElementsArray{
          addressDescription += "  "+val+"\n";
        }
        if(addressDescription.last! == "\n"){
          addressDescription = addressDescription.substring(to: addressDescription.index(before: addressDescription.endIndex));
        }
        
        addressDescriptionLabel.text = addressDescription;
        if #available(iOS 13.0, *) {
            addressDescriptionLabel.backgroundColor = UIColor.systemBackground
        } else {
            addressDescriptionLabel.backgroundColor = UIColor.white
        };
        //                addressDescriptionLabel.fontColorTool()
        //addressDescriptionLabel.makeCard(addressDescriptionLabel, cornerRadius: 2, color: UIColor.black, shadowOpacity: 0.4);
        let fontToApply = UIFont(fontName: "", fontSize: CGFloat(13.0))!;
        addressDescriptionLabel.font = fontToApply;
        let textAttributes = [NSAttributedString.Key.font: fontToApply];
       
        let rect = addressDescription.boundingRect(with: CGSize(width: screenSize.width, height: CGFloat.greatestFiniteMagnitude), options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: textAttributes, context: nil);
        
        stackView.insertArrangedSubview(addressDescriptionLabel, at: 2);
        let addressDescriptionLabelHeight = translateAccordingToDevice(CGFloat(rect.height))+20;
        addressDescriptionLabel.heightAnchor.constraint(equalToConstant: addressDescriptionLabelHeight).isActive = true;
        self.setLeadingAndTralingSpaceFormParentView(addressDescriptionLabel, parentView:stackView);
      }
    }
    else{
      
      self.isPreviousAddressIsShowing = true;
      
        let color = Settings.themeColor//cedMage.getInfoPlist(fileName:"cedMage",indexString: "themeColor") as! String
      let theme_color = color//cedMage.UIColorFromRGB(colorCode: color);
      
      if let pickAddressButton = stackView.arrangedSubviews[1] as? UIButton {
        stackView.removeArrangedSubview(pickAddressButton);
        pickAddressButton.removeFromSuperview();
        
        let addressLabel = UILabel();
        addressLabel.translatesAutoresizingMaskIntoConstraints = false;
        addressLabel.font = UIFont(fontName: "HelveticaNeue-Medium", fontSize: CGFloat(15.0));
        //                addressLabel.textColor = theme_color;
        addressLabel.fontColorTool()
        addressLabel.text = "Selected Address".localized+" :";
        stackView.insertArrangedSubview(addressLabel, at: 1);
        let addressLabelHeight = translateAccordingToDevice(CGFloat(30.0));
        addressLabel.heightAnchor.constraint(equalToConstant: addressLabelHeight).isActive = true;
        self.setLeadingAndTralingSpaceFormParentView(addressLabel, parentView:stackView);
        
        let addressDescriptionLabel = UILabel();
        addressDescriptionLabel.numberOfLines = 0;
        addressDescriptionLabel.translatesAutoresizingMaskIntoConstraints = false;
        var addressDescription = defaults.object(forKey: "selectedAddressDescription") as! String;
        print("addressDescription");
        print(addressDescription);
        let addressDescriptionElementsArray = addressDescription.components(separatedBy: "\n");
        addressDescription = "";
        for val in addressDescriptionElementsArray{
          addressDescription += "  "+val+"\n";
        }
        if(addressDescription.last! == "\n"){
          addressDescription = addressDescription.substring(to: addressDescription.index(before: addressDescription.endIndex));
        }
        
        addressDescriptionLabel.text = addressDescription;
        if #available(iOS 13.0, *) {
            addressDescriptionLabel.backgroundColor = UIColor.systemBackground
        } else {
            addressDescriptionLabel.backgroundColor = UIColor.white
        };
        // addressDescriptionLabel.makeCard(addressDescriptionLabel, cornerRadius: 2, color: UIColor.black, shadowOpacity: 0.4);
        let fontToApply = UIFont(fontName: "", fontSize: CGFloat(13.0))!;
        addressDescriptionLabel.font = fontToApply;
        var textAttributes = [NSAttributedString.Key.font: fontToApply];
        let rect = addressDescription.boundingRect(with: CGSize(width: screenSize.width, height: CGFloat.greatestFiniteMagnitude), options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: textAttributes, context: nil);
        
        stackView.insertArrangedSubview(addressDescriptionLabel, at: 2);
        let addressDescriptionLabelHeight = translateAccordingToDevice(CGFloat(rect.height))+20;
        addressDescriptionLabel.heightAnchor.constraint(equalToConstant: addressDescriptionLabelHeight).isActive = true;
        self.setLeadingAndTralingSpaceFormParentView(addressDescriptionLabel, parentView:stackView);
        
        let changeAddressButton = UIButton();
        changeAddressButton.translatesAutoresizingMaskIntoConstraints = false;
        changeAddressButton.setTitle("Change Address".localized, for: UIControl.State.normal);
        changeAddressButton.setTitleColor(theme_color, for: UIControl.State.normal);
        changeAddressButton.setTitleColor(.black, for: .normal)
        //                changeAddressButton.fontColorTool()
        changeAddressButton.makeCornerRounded(cornerRadius: translateAccordingToDevice(CGFloat(5.0)));
        changeAddressButton.layer.borderWidth =  CGFloat(2);
        changeAddressButton.layer.borderColor = theme_color.cgColor;
        changeAddressButton.titleLabel?.font = UIFont(fontName: "HelveticaNeue-Medium", fontSize: CGFloat(15.0));
        changeAddressButton.addTarget(self, action: #selector(CheckoutStep2ViewController.pickAddressButtonPressed(_:)), for: UIControl.Event.touchUpInside);
        stackView.insertArrangedSubview(changeAddressButton, at: 3);
        let changeAddressButtonHeight = translateAccordingToDevice(CGFloat(40.0));
        changeAddressButton.heightAnchor.constraint(equalToConstant: changeAddressButtonHeight).isActive = true;
        self.setLeadingAndTralingSpaceFormParentView(changeAddressButton, parentView:stackView);
        
        var currentIndex = 4;
        while (currentIndex < stackView.subviews.count-2){
          stackView.arrangedSubviews[currentIndex].isHidden = true;
          currentIndex = currentIndex+1;
        }
      }
    }
  }
  
  override func recieveResponse(data: Data?, requestUrl: String?, response: URLResponse?) {
    if requestUrl=="mobiconnect/module/getcountry/"
    {
      if let data = data {
        guard let json = try? JSON(data:data)else{return;}
        let jsonResponse = json[0]
        print(jsonResponse);
        let countryListing = jsonResponse["country"].arrayValue;
        
        for d in 0..<countryListing.count {
          let key = countryListing[d]["value"].stringValue;
          let value = countryListing[d]["label"].stringValue;
          self.countryDict[value] = key;
        }
        print(countryDict);
        
      }
    }
    else
    {
      guard let json=try? JSON(data: data!)else{return;}
      if json[0]["success"] == true
      {
        
        //first_name_data=String(describing: json[0]["firstname"])
        //last_name_data=String(describing: json[0]["lastname"])
        for (_,field) in json[0]["data"]
        {
          
          for (keys,values) in field
          {
            if keys=="prefix"
            {
              let v=String(describing: values);
              if v=="true"
              {
                values_fields.updateValue(field["value"], forKey: "prefix")
              }
            }else if keys=="middlename"
            {
              let v=String(describing: values);
              if v=="true"
              {
                values_fields.updateValue(field["value"], forKey: "middlename")
              }
            }else if keys=="taxvat"
            {
              let v=String(describing: values);
              if v=="true"
              {
                values_fields.updateValue(field["value"], forKey: "company")
              }
            }else if keys=="suffix"
            {
              let v=String(describing: values);
              if v=="true"
              {
                values_fields.updateValue(field["value"], forKey: "suffix")
              }
            }
            
            
            if(keys=="name")
            {
              let v=String(describing: values);
              if(field[v]==false)
              {
                hide_check.updateValue("false", forKey: v)
              }
              else
              {
                hide_check.updateValue("true", forKey: v)
                
                if(field["type"]=="dropdown")
                {
                  if(values=="suffix")
                  {
                    //suffix_data=field["suffix_options"] as Dictionary<String,String>
                    for (index,val) in field["suffix_options"]
                    {
                      suffix_data.updateValue(String(describing: val), forKey: index)
                    }
                    dropdown_options.updateValue(field["suffix_options"], forKey: "suffix")
                  }
                  else if(values=="prefix")
                  {
                    for (index,val) in field["prefix_options"]
                    {
                      prefix_data.updateValue(String(describing: val), forKey: index)
                    }
                    dropdown_options.updateValue(field["prefix_options"], forKey: "prefix")
                  }
                }
              }
            }
          }
        }
      }
      print("prefix_data:::::::::\(prefix_data)")
      print("suffix_data:::::::::\(suffix_data)")
      print("values_fields:::::::::\(values_fields)")
      print("dropdown_options:::::::::\(dropdown_options)")
      print("hide_check:::::::::\(hide_check)")
      
      
      self.addressInfoArray=[:]
      self.addressFieldsArray=[]
      self.addressFieldsArray = ["email"];
        self.addressInfoArray=["email":"Email".localized]
      if hide_check["prefix"]=="true"
      {
        //indexOfStateField+=1
        addressInfoArray.updateValue("Prefix(*)".localized, forKey: "prefix")
        addressFieldsArray.append("prefix")
        
      }
      addressFieldsArray.append("firstname")
      addressInfoArray.updateValue("First Name(*)".localized, forKey: "firstname")
      if hide_check["middlename"]=="true"
      {
        //indexOfStateField+=1
        addressInfoArray.updateValue("Middle Name(*)".localized, forKey: "middlename")
        addressFieldsArray.append("middlename")
      }
      addressFieldsArray.append("lastname")
      addressInfoArray.updateValue("Last Name(*)".localized, forKey: "lastname")
      if hide_check["suffix"]=="true"
      {
        //indexOfStateField+=1
        addressInfoArray.updateValue("Suffix(*)".localized, forKey: "suffix")
        addressFieldsArray.append("suffix")
      }
      addressFieldsArray.append("country")
      addressInfoArray.updateValue("Country(*)".localized, forKey: "country")
      addressFieldsArray.append("state")
      addressInfoArray.updateValue("State(*)".localized, forKey: "state")
      addressFieldsArray.append("street")
      addressInfoArray.updateValue("Street(*)".localized, forKey: "street")
      addressFieldsArray.append("city")
      addressInfoArray.updateValue("City(*)".localized, forKey: "city")
      addressFieldsArray.append("postcode")
      addressInfoArray.updateValue("PostCode(*)".localized, forKey: "postcode")
      addressFieldsArray.append("telephone")
      addressInfoArray.updateValue("Mobile(*)".localized, forKey: "telephone")
      
      if hide_check["taxvat"]=="true"
      {
        addressInfoArray.updateValue("Company".localized, forKey: "company")
        addressFieldsArray.append("company")
      }
      self.renderCheckoutStep2View();
    }
  }
  
  func renderCheckoutStep2View(){
    
    self.makeSomeSpaceInStackView(height: CGFloat(0.0));
    
    let color = Settings.themeColor//cedMage.getInfoPlist(fileName:"cedMage",indexString: "themeColor") as! String
    let theme_color = color//cedMage.UIColorFromRGB(colorCode: color);
    
    if(self.checkoutAs == "USER" && defaults.object(forKey: "selectedAddressId") == nil){
      let pickAddressButton = UIButton();
      pickAddressButton.translatesAutoresizingMaskIntoConstraints = false;
      pickAddressButton.setTitle("Pick Address From Address Book".localized, for: UIControl.State.normal);
      //            pickAddressButton.fontColorTool()
    pickAddressButton.setTitleColor(UIColor.gray, for: .normal);
      pickAddressButton.makeCornerRounded(cornerRadius: translateAccordingToDevice(CGFloat(5.0)));
      pickAddressButton.layer.borderWidth =  CGFloat(2);
      pickAddressButton.layer.borderColor = theme_color.cgColor;
      pickAddressButton.titleLabel?.font = UIFont(fontName: "HelveticaNeue-Medium", fontSize: CGFloat(15.0));
      pickAddressButton.addTarget(self, action: #selector(CheckoutStep2ViewController.pickAddressButtonPressed(_:)), for: UIControl.Event.touchUpInside);
      stackView.addArrangedSubview(pickAddressButton);
      let pickAddressButtonHeight = translateAccordingToDevice(CGFloat(40.0));
      pickAddressButton.heightAnchor.constraint(equalToConstant: pickAddressButtonHeight).isActive = true;
      self.setLeadingAndTralingSpaceFormParentView(pickAddressButton, parentView:stackView);
    }
    else if(self.checkoutAs == "USER"){
      
      self.isPreviousAddressIsShowing = true;
      
      let addressLabel = UILabel();
      addressLabel.translatesAutoresizingMaskIntoConstraints = false;
      addressLabel.font = UIFont(fontName: "HelveticaNeue-Medium", fontSize: CGFloat(15.0));
      //            addressLabel.textColor = theme_color;
      //            addressLabel.fontColorTool()
      addressLabel.text = "Selected Address".localized+" :";
      stackView.addArrangedSubview(addressLabel);
      let addressLabelHeight = translateAccordingToDevice(CGFloat(30.0));
      addressLabel.heightAnchor.constraint(equalToConstant: addressLabelHeight).isActive = true;
      self.setLeadingAndTralingSpaceFormParentView(addressLabel, parentView:stackView);
      
      
      let addressDescriptionLabel = UILabel();
      addressDescriptionLabel.numberOfLines = 0;
      addressDescriptionLabel.translatesAutoresizingMaskIntoConstraints = false;
       var address = UserDefaults.standard.value(forKey: "selectedAddress") as! [String:String]
             var addressDescription = address["address"]!//defaults.object(forKey: "selectedAddressDescription") as! String;
      print("addressDescription");
      print(addressDescription);
      let addressDescriptionElementsArray = addressDescription.components(separatedBy: "\n");
      addressDescription = "";
      for val in addressDescriptionElementsArray{
        addressDescription += "  "+val+"\n";
      }
      if(addressDescription.last! == "\n"){
        addressDescription = addressDescription.substring(to: addressDescription.index(before: addressDescription.endIndex));
      }
      
      addressDescriptionLabel.text = addressDescription;
      //            addressDescriptionLabel.backgroundColor = UIColor.white;
      addressDescriptionLabel.fontColorTool()
      // addressDescriptionLabel.makeCard(addressDescriptionLabel, cornerRadius: 2, color: UIColor.black, shadowOpacity: 0.4);
      let fontToApply = UIFont(fontName: "", fontSize: CGFloat(13.0))!;
      addressDescriptionLabel.font = fontToApply;
      let textAttributes = [NSAttributedString.Key.font: fontToApply];
      let rect = addressDescription.boundingRect(with: CGSize(width: screenSize.width, height: CGFloat.greatestFiniteMagnitude), options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: textAttributes, context: nil);
      
      
      stackView.addArrangedSubview(addressDescriptionLabel);
      let addressDescriptionLabelHeight = translateAccordingToDevice(CGFloat(rect.height))+20;
      addressDescriptionLabel.heightAnchor.constraint(equalToConstant: addressDescriptionLabelHeight).isActive = true;
      self.setLeadingAndTralingSpaceFormParentView(addressDescriptionLabel, parentView:stackView);
      
      
      
      let changeAddressButton = UIButton();
      changeAddressButton.translatesAutoresizingMaskIntoConstraints = false;
      changeAddressButton.setTitle("Change Address".localized, for: UIControl.State.normal);
      changeAddressButton.fontColorTool()
        changeAddressButton.setTitleColor(UIColor.gray, for: .normal);
      changeAddressButton.makeCornerRounded(cornerRadius: translateAccordingToDevice(CGFloat(5.0)));
      changeAddressButton.layer.borderWidth =  CGFloat(2);
      changeAddressButton.layer.borderColor = theme_color.cgColor;
      changeAddressButton.titleLabel?.font = UIFont(fontName: "HelveticaNeue-Medium", fontSize: CGFloat(15.0));
      changeAddressButton.addTarget(self, action: #selector(CheckoutStep2ViewController.pickAddressButtonPressed(_:)), for: UIControl.Event.touchUpInside);
      stackView.addArrangedSubview(changeAddressButton);
      let changeAddressButtonHeight = translateAccordingToDevice(CGFloat(40.0));
      changeAddressButton.heightAnchor.constraint(equalToConstant: changeAddressButtonHeight).isActive = true;
      self.setLeadingAndTralingSpaceFormParentView(changeAddressButton, parentView:stackView);
    }
    
    
    
    if(self.checkoutAs == "GUEST" || self.checkoutAs == "USER" && defaults.object(forKey: "selectedAddressId") == nil){
      for (key) in addressFieldsArray {
        if(key == "prefix" || key=="suffix")
        {
          let dropDownButtonView = DropDownButtonView();
          dropDownButtonView.translatesAutoresizingMaskIntoConstraints = false;
          dropDownButtonView.dropDownButton.titleLabel?.font = UIFont.systemFont(ofSize: 12)
          //                    dropDownButtonView.dropDownButton.setTitle(addressInfoArray[key], for: UIControlState.normal);
          dropDownButtonView.dropDownButton.fontColorTool()
          if key=="prefix"
          {
            dropDownButtonView.dropDownButton.addTarget(self, action: #selector(show_prefix_option(_:)), for: UIControl.Event.touchUpInside);
          }
          else
          {
            dropDownButtonView.dropDownButton.addTarget(self, action: #selector(show_suffix_option(_:)), for: UIControl.Event.touchUpInside);
          }
          
          
          stackView.addArrangedSubview(dropDownButtonView);
          let dropDownButtonViewHeight = translateAccordingToDevice(CGFloat(40.0));
          dropDownButtonView.heightAnchor.constraint(equalToConstant: dropDownButtonViewHeight).isActive = true;
          
          self.setLeadingAndTralingSpaceFormParentView(dropDownButtonView, parentView:stackView);
          continue;
        }
        if(key == "country"){
          let dropDownButtonView = DropDownButtonView();
          dropDownButtonView.translatesAutoresizingMaskIntoConstraints = false;
          dropDownButtonView.dropDownButton.titleLabel?.font = UIFont.systemFont(ofSize: 12)
          dropDownButtonView.dropDownButton.setTitle(addressInfoArray[key], for: UIControl.State.normal);
          dropDownButtonView.dropDownButton.addTarget(self, action: #selector(showCountryDropdown(_:)), for: UIControl.Event.touchUpInside);
          
          stackView.addArrangedSubview(dropDownButtonView);
          let dropDownButtonViewHeight = translateAccordingToDevice(CGFloat(40.0));
          dropDownButtonView.heightAnchor.constraint(equalToConstant: dropDownButtonViewHeight).isActive = true;
          
          self.setLeadingAndTralingSpaceFormParentView(dropDownButtonView, parentView:stackView);
          if let index = stackView.arrangedSubviews.firstIndex(of: dropDownButtonView) {
            self.indexOfStateField = index+1;
            print("self.indexOfStateField");
            print(self.indexOfStateField);
          }
          continue;
        }
        //UIColor(hexString: "#3d3d3d")!
        let textField = SkyFloatingLabelTextField();
        
        textField.titleColor=UIColor.systemGray
        textField.tintColor=UIColor.systemGray
        textField.selectedLineColor=UIColor.systemGray
        textField.selectedTitleColor=UIColor.systemGray
        if #available(iOS 13.0, *) {
            textField.textColor = UIColor.label
        } else {
            textField.textColor = UIColor.black
        }
        
        //                textField.textColor=UIColor.darkGray
        textField.fontColorTool()
        /*textField.tintColor=UIColor(hexString: "#3d3d3d")!
        textField.selectedLineColor=UIColor(hexString: "#3d3d3d")!
        textField.selectedTitleColor=UIColor(hexString: "#3d3d3d")!*/
        textField.translatesAutoresizingMaskIntoConstraints = false;
        textField.placeholder = addressInfoArray[key];
        if(key == "email"){
          textField.text = userInfoDict["email"];
          // textField.isUserInteractionEnabled = false;
        }
        
        
        textField.font = UIFont.systemFont(ofSize: 12)
        stackView.addArrangedSubview(textField);
        let textFieldHeight = translateAccordingToDevice(CGFloat(40.0));
        textField.heightAnchor.constraint(equalToConstant: textFieldHeight).isActive = true;
        self.setLeadingAndTralingSpaceFormParentView(textField, parentView:stackView);
      }
      /*for (key) in addressFieldsArray {
       if(key == "country"){
       let dropDownButtonView = DropDownButtonView();
       dropDownButtonView.translatesAutoresizingMaskIntoConstraints = false;
       dropDownButtonView.dropDownButton.titleLabel?.font = UIFont(fontName: "", fontSize: CGFloat(15.0));
       dropDownButtonView.dropDownButton.setTitle(addressInfoArray[key], for: UIControlState.normal);
       dropDownButtonView.dropDownButton.addTarget(self, action: #selector(CheckoutStep2ViewController.showCountryDropdown(_:)), for: UIControlEvents.touchUpInside);
       
       stackView.addArrangedSubview(dropDownButtonView);
       let dropDownButtonViewHeight = translateAccordingToDevice(CGFloat(40.0));
       dropDownButtonView.heightAnchor.constraint(equalToConstant: dropDownButtonViewHeight).isActive = true;
       
       self.setLeadingAndTralingSpaceFormParentView(dropDownButtonView, parentView:stackView);
       
       if let index = stackView.arrangedSubviews.index(of: dropDownButtonView) {
       self.indexOfStateField = index+1;
       print("self.indexOfStateField");
       print(self.indexOfStateField);
       }
       continue;
       }
       
       let textField = UITextField();
       textField.borderStyle = UITextBorderStyle.roundedRect;
       textField.translatesAutoresizingMaskIntoConstraints = false;
       textField.placeholder = addressInfoArray[key];
       if(key == "email"){
       textField.text = email_id;
       textField.isUserInteractionEnabled = false;
       textField.backgroundColor = UIColor(hexString: "#BFBFBF");
       }
       textField.font = UIFont(fontName: "", fontSize: CGFloat(13.0));
       stackView.addArrangedSubview(textField);
       let textFieldHeight = translateAccordingToDevice(CGFloat(40.0));
       textField.heightAnchor.constraint(equalToConstant: textFieldHeight).isActive = true;
       self.setLeadingAndTralingSpaceFormParentView(textField, parentView:stackView);
       }*/
    }
    
    
    let proceddToNextStepButton = UIButton();
    proceddToNextStepButton.translatesAutoresizingMaskIntoConstraints = false;
    proceddToNextStepButton.setThemeColor();
    proceddToNextStepButton.setTitleColor(UIColor.white, for: UIControl.State.normal);
    proceddToNextStepButton.setTitle("Proceed".localized, for: UIControl.State.normal);
    proceddToNextStepButton.titleLabel?.font = UIFont(fontName: "", fontSize: CGFloat(17.0));
    proceddToNextStepButton.addTarget(self, action: #selector(CheckoutStep2ViewController.proceddToNextStepButtonPressed(_:)), for: UIControl.Event.touchUpInside);
    stackView.addArrangedSubview(proceddToNextStepButton);
    proceddToNextStepButton.makeCornerRounded(cornerRadius: translateAccordingToDevice(CGFloat(5.0)));
    let proceddToNextStepButtonHeight = translateAccordingToDevice(CGFloat(40.0));
    proceddToNextStepButton.heightAnchor.constraint(equalToConstant: proceddToNextStepButtonHeight).isActive = true;
    self.setLeadingAndTralingSpaceFormParentView(proceddToNextStepButton, parentView:stackView);
    
    self.makeSomeSpaceInStackView(height: CGFloat(0.0));
  }
  @objc func show_prefix_option(_ sender : UIButton)
  {
    //print(self.countryDict);
    
    var temp_arr=[String]()
    if let temp=dropdown_options["prefix"] {
      for (key,_) in temp
      {
        temp_arr.append(key)
      }
    }
    var tempDataSource = Array(temp_arr);
    print(tempDataSource);
    tempDataSource = tempDataSource.sorted { $0.localizedCaseInsensitiveCompare($1) == ComparisonResult.orderedAscending }
    dropDown.dataSource = tempDataSource;
    
    dropDown.selectionAction = {(index, item) in
      sender.setTitle(item, for: UIControl.State());
      //self.fetchStatesCorrespondingToCountry(current_country_code: self.countryDict[item]!);
    }
    
    dropDown.anchorView = sender
    dropDown.bottomOffset = CGPoint(x: 0, y:sender.bounds.height)
    if dropDown.isHidden {
      let _ = dropDown.show();
    } else {
      dropDown.hide();
    }
  }
  @objc func show_suffix_option(_ sender : UIButton)
  {
    
    var temp_arr=[String]()
    if let temp=dropdown_options["suffix"] {
      for (key,_) in temp
      {
        temp_arr.append(key)
      }
    }
    var tempDataSource = Array(temp_arr);
    print(tempDataSource);
    tempDataSource = tempDataSource.sorted { $0.localizedCaseInsensitiveCompare($1) == ComparisonResult.orderedAscending }
    dropDown.dataSource = tempDataSource;
    
    dropDown.selectionAction = {(index, item) in
      sender.setTitle(item, for: UIControl.State());
      //self.fetchStatesCorrespondingToCountry(current_country_code: self.countryDict[item]!);
    }
    
    dropDown.anchorView = sender
    dropDown.bottomOffset = CGPoint(x: 0, y:sender.bounds.height)
    if dropDown.isHidden {
      let _ = dropDown.show();
    } else {
      dropDown.hide();
    }
  }
  
    @objc func proceddToNextStepButtonPressed(_ sender:UIButton){
        print("proceddToNextStepButtonPressed");
        var postString = "";
        var postData = [String:String]()
        let address = UserDefaults.standard.value(forKey: "selectedAddress") as? [String:String]
        let addressID = address?["id"]
        var addressToBeSend = "{";
    //    defaults.object(forKey: "selectedAddressId")
        if(self.checkoutAs == "GUEST" || self.checkoutAs == "USER" &&  addressID == ""){
          
          print(self.checkoutAs);
          var currentIndex = 2;
          if(self.checkoutAs == "GUEST"){
            currentIndex = 1;
          }
          print(addressFieldsArray);
          for key in addressFieldsArray{
            
            print(stackView.arrangedSubviews[currentIndex]);
            
            if let textView = stackView.arrangedSubviews[currentIndex] as? SkyFloatingLabelTextField {
              if key != "company" {
                if(textView.text == ""){
                  let msg = "Please Fill All Fields".localized;
                  self.view.makeToast(msg, duration: 2.0, position: .bottom, title: nil, image: nil, style: nil, completion: nil);
                  return;
                }
              }
              if(key == "email"){
                self.email_id = textView.text!
                addressToBeSend += "\""+key+"\":"+"\""+textView.text!+"\",";
              }
              else if(key == "street"){
                addressToBeSend += "\""+key+"\":{\"0\":"+"\""+textView.text!+"\"},";
              }
              else if(key == "state"){
                addressToBeSend += "\""+"region"+"\":"+"\""+textView.text!+"\",";
              }
              else{
                addressToBeSend += "\""+key+"\":"+"\""+textView.text!+"\",";
              }
              self.addressDescriptionArray[key] = textView.text!;
            }
            else if let dropDownButtonView = stackView.arrangedSubviews[currentIndex] as? DropDownButtonView {
              let text = dropDownButtonView.dropDownButton.titleLabel?.text;
              print(key);
              if key=="prefix"
              {
                if text=="Prefix(*)"
                {
                  let msg = "All Fields Are Required !".localized;
                  self.view.makeToast(msg, duration: 2.0, position: .bottom, title: nil, image: nil, style: nil, completion: nil);
                  return;
                }
                addressToBeSend += "\""+"prefix"+"\":";
                addressToBeSend += "\""+prefix_data[text!]!+"\",";
                //postData["prefix"] = prefix_data[text!]
              }
              if key=="suffix"
              {
                if text=="Suffix(*)"
                {
                  let msg = "All Fields Are Required !".localized;
                  self.view.makeToast(msg, duration: 2.0, position: .bottom, title: nil, image: nil, style: nil, completion: nil);
                  return;
                }
                addressToBeSend += "\""+"suffix"+"\":";
                addressToBeSend += "\""+suffix_data[text!]!+"\",";
                
                //postData["suffix"] = suffix_data[text!]
              }
              if(key == "country"){
                if(countryDict[text!] == nil){
                  let msg = "Please Select Country".localized;
                  self.view.makeToast(msg, duration: 2.0, position: .bottom, title: nil, image: nil, style: nil, completion: nil);
                  return;
                }
                addressToBeSend += "\""+"country_id"+"\":"+"\""+countryDict[text!]!+"\",";
              }
              if(key == "state"){
                if(stateDict[text!] == nil){
                  let msg = "Please Select State".localized;
                  self.view.makeToast(msg, duration: 2.0, position: .bottom, title: nil, image: nil, style: nil, completion: nil);
                  return;
                }
                addressToBeSend += "\""+"region_id"+"\":"+"\""+stateDict[text!]!+"\",";
              }
              self.addressDescriptionArray[key] = text!;
            }
            
            currentIndex = currentIndex+1;
          }
          if(addressToBeSend.last! == ","){
            addressToBeSend = addressToBeSend.substring(to: addressToBeSend.index(before: addressToBeSend.endIndex));
          }
          addressToBeSend += "}";
          
          print(addressToBeSend);
          
        }
        
        
        if(self.checkoutAs == "GUEST"){
          if(defaults.object(forKey: "cartId") != nil){
            let cart_id = defaults.object(forKey: "cartId") as! String;
            postData["cart_id"] = cart_id;
          }
          postData["Role"] = self.checkoutAs;
          postData["billingaddress"] = addressToBeSend;
        }
        else if(self.checkoutAs == "USER"){
        /*  if(defaults.object(forKey: "cartId") != nil){
            let cart_id = defaults.object(forKey: "cartId") as! String;
            postData["cart_id"] = cart_id;
          }*/
          postData["Role"] = self.checkoutAs;
          if(defaults.object(forKey: "userInfoDict") != nil){
            userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
            postData["hashkey"]=userInfoDict["hashKey"]!
            postData["customer_id"] = userInfoDict["customerId"]!;
          }
          if(defaults.object(forKey: "selectedAddressId") != nil){
            let address = UserDefaults.standard.value(forKey: "selectedAddress") as! [String:String]
             
            let selectedAddressId = address["id"]!//defaults.object(forKey: "selectedAddressId") as! String;
            postData["address_id"] = selectedAddressId;
          }
          else{
            postData["billingaddress"] = addressToBeSend;
          }
          
        }
       // postData["email"] = self.email_id;
        
       
        postString = ["parameters":postData].convtToJson() as String
        print(postString);
        
        
        var urlToRequest = "mobiconnect/checkout/savebillingshipping";
        let requestHeader = Settings.headerKey//cedMage.getInfoPlist(fileName:"cedMage",indexString: "requestheader") as! String
        let baseURL = Settings.baseUrl//cedMage.getInfoPlist(fileName:"cedMage",indexString: "cedBaseUrl") as! String;
        urlToRequest = baseURL+urlToRequest;
        
        
        print(urlToRequest);
        print(postString);
        
        var request = URLRequest(url: URL(string: "\(urlToRequest)")!);
        request.httpMethod = "POST";
        request.httpBody = postString.data(using: String.Encoding.utf8);
        if UserDefaults.standard.bool(forKey: "isLogin"){
            if let user = UserDefaults.standard.object(forKey: "userInfoDict") as? [String:String] {
                request.setValue(user["hashKey"], forHTTPHeaderField: "hashkey")
            }
            
        }
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        cedMageLoaders.addDefaultLoader(me: self);
        let task = URLSession.shared.dataTask(with: request){
          
          // check for fundamental networking error
          data, response, error in
          guard error == nil && data != nil else{
            print("error=\(error)")
            DispatchQueue.main.async{
              print(error?.localizedDescription as Any);
              cedMageLoaders.removeLoadingIndicator(me: self);
            }
            return;
          }
          
          // check for http errors
          if let httpStatus = response as? HTTPURLResponse , httpStatus.statusCode != 200{
            
            print("statusCode should be 200, but is \(httpStatus.statusCode)")
            print("response = \(response)")
            DispatchQueue.main.async{
              cedMageLoaders.removeLoadingIndicator(me: self);
              
            }
            return;
          }
          
          // code to fetch values from response :: start
          
          let datastring = NSString(data: data!, encoding: String.Encoding.utf8.rawValue);
          //NSString(data: NSData!, encoding: String.Encoding.utf8)
          print("datastring");
          print(datastring as Any);
          
          guard var jsonResponse = try? JSON(data: data!) else{return;}
          jsonResponse = jsonResponse[0]
          if(jsonResponse != nil){
            
            DispatchQueue.main.async{
              print(jsonResponse);
              cedMageLoaders.removeLoadingIndicator(me: self);
              if(jsonResponse["success"].stringValue == "true"){
                if(jsonResponse["address_id"] != nil){
                  var addressDescription = self.addressDescriptionArray["firstname"]!+" "+self.addressDescriptionArray["lastname"]!;
                  addressDescription += "\n\n";
                  addressDescription += self.addressDescriptionArray["street"]!+",\n"+self.addressDescriptionArray["city"]!+",\n"+self.addressDescriptionArray["state"]!+",\n"+self.addressDescriptionArray["country"]!+"-"+self.addressDescriptionArray["postcode"]!+"\n\n"+self.addressDescriptionArray["telephone"]!;
                  
                 // self.defaults.set(addressDescription, forKey: "selectedAddressDescription");
                 // self.defaults.set(jsonResponse["address_id"].stringValue, forKey: "selectedAddressId");
                }
                let bool =  self.defaults.bool(forKey: "flag_downloadable")
                if bool {
                  let storyboard = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil);
                  let vc=storyboard.instantiateViewController(withIdentifier: "checkoutStep3ViewController") as! CheckoutStep3ViewController
                  
                  vc.email_id=self.email_id
                  vc.checkoutAs=self.checkoutAs
                  vc.totalOrderData = self.totalOrderData
                  self.navigationController?.pushViewController(vc, animated: true)
                }else{
                  let storyboard = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil);
                  let viewController = storyboard.instantiateViewController(withIdentifier: "checkoutStep4ViewController") as! CheckoutStep4ViewController;
                  viewController.email_id = self.email_id;
                  viewController.totalOrderData = self.totalOrderData
                  viewController.checkoutAs = self.checkoutAs;
                  self.navigationController?.pushViewController(viewController, animated: true);
                }
              }
              else{
                let msg = jsonResponse["message"].stringValue;
                self.view.makeToast(msg, duration: 2.0, position: .bottom, title: nil, image: nil, style: nil, completion: nil);
              }
            }
          }
        }
        task.resume();
      }
  
  override func basicFoundationToRenderView(bottomMargin:CGFloat){
    
    
    
    //let tabBarHeight = self.tabBarController?.tabBar.frame.size.height;
    let tabBarHeight = CGFloat(0);
    // adding scrollview
    scrollView = UIScrollView();
    scrollView.translatesAutoresizingMaskIntoConstraints = false;
    scrollView.showsHorizontalScrollIndicator = false;
    scrollView.showsVerticalScrollIndicator = false;
    view.addSubview(scrollView);
    self.view.addConstraint(NSLayoutConstraint(item: scrollView, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 0));
    self.view.addConstraint(NSLayoutConstraint(item: scrollView, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 0));
    self.view.addConstraint(NSLayoutConstraint(item: scrollView, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.imageViewOnTop, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: 0));
    self.view.addConstraint(NSLayoutConstraint(item: scrollView, attribute: NSLayoutConstraint.Attribute.bottom, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.bottomLayoutGuide, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: -(bottomMargin+tabBarHeight)));
    
    // adding stackview
    stackView = UIStackView();
    stackView.translatesAutoresizingMaskIntoConstraints = false;
    stackView.axis  = NSLayoutConstraint.Axis.vertical;
    stackView.distribution  = UIStackView.Distribution.equalSpacing;
    stackView.alignment = UIStackView.Alignment.center;
    stackView.spacing   = 10.0;
    scrollView.addSubview(stackView);
    self.view.addConstraint(NSLayoutConstraint(item: stackView, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 0));
    self.view.addConstraint(NSLayoutConstraint(item: stackView, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 0));
    scrollView.addConstraint(NSLayoutConstraint(item: stackView, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: scrollView, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1, constant: 0));
    scrollView.addConstraint(NSLayoutConstraint(item: stackView, attribute: NSLayoutConstraint.Attribute.bottom, relatedBy: NSLayoutConstraint.Relation.equal, toItem: scrollView, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: 0));
    
  }
  
  @objc func pickAddressButtonPressed(_ sender:UIButton){
    self.makeRequestToAPI("mobiconnect/customer/address/",dataToPost: [:]);
  }
  
  
  override func parseResponseReceivedFromAPI(_ jsonResponse:JSON){
    print("addressbook--------\(jsonResponse)");
    let jsonResponse = jsonResponse[0]
    if(jsonResponse["data"]["status"].stringValue != "success"){
      flag=true
    }
    if flag==true
    {
      let alert=UIAlertController(title: "No Address", message: "You have empty addressbook.. please add an address from here...!", preferredStyle: .alert)
      let action=UIAlertAction(title: "Ok", style: .default, handler: nil)
      alert.addAction(action)
      alert.modalPresentationStyle = .fullScreen;
      self.present(alert, animated: true, completion: nil)
      return
    }
    else
    {
      print("pickAddressButtonPressed");
      let storyboard = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil);
      let viewController = storyboard.instantiateViewController(withIdentifier: "addressListingViewController") as! AddressListingViewController;
      viewController.isAddressPickMode = true;
      self.navigationController?.pushViewController(viewController, animated: true);
    }
  }
  
  @objc func showCountryDropdown(_ sender:UIButton){
    
    print(self.countryDict);
    var tempDataSource = Array(self.countryDict.keys);
    print(tempDataSource);
    tempDataSource = tempDataSource.sorted { $0.localizedCaseInsensitiveCompare($1) == ComparisonResult.orderedAscending }
    dropDown.dataSource = tempDataSource;
    
    dropDown.selectionAction = {(index, item) in
      sender.setTitle(item, for: UIControl.State());
      self.fetchStatesCorrespondingToCountry(current_country_code: self.countryDict[item]!);
    }
    dropDown.anchorView = sender
    dropDown.bottomOffset = CGPoint(x: 0, y:sender.bounds.height)
    if dropDown.isHidden {
      let _ = dropDown.show();
    } else {
      dropDown.hide();
    }
  }
  
  
  @objc func showStateDropdown(_ sender:UIButton){
    
    print(self.stateDict);
    var tempDataSource = Array(self.stateDict.keys);
    print(tempDataSource);
    tempDataSource = tempDataSource.sorted { $0.localizedCaseInsensitiveCompare($1) == ComparisonResult.orderedAscending }
    dropDown.dataSource = tempDataSource;
    
    dropDown.selectionAction = {(index, item) in
      sender.setTitle(item, for: UIControl.State());
    }
    dropDown.anchorView = sender
    dropDown.bottomOffset = CGPoint(x: 0, y:sender.bounds.height)
    if dropDown.isHidden {
      let _ = dropDown.show();
    } else {
      dropDown.hide();
    }
  }
  
  
  func fetchStatesCorrespondingToCountry(current_country_code:String){
    
    var urlToRequest = "mobiconnect/module/getcountry/"+current_country_code
    let requestHeader = Settings.headerKey//cedMage.getInfoPlist(fileName:"cedMage",indexString: "requestheader") as! String
    let baseURL = Settings.baseUrl//cedMage.getInfoPlist(fileName:"cedMage",indexString: "cedBaseUrl") as! String;
    urlToRequest = baseURL+urlToRequest;
    
    var request = URLRequest(url: URL(string: "\(urlToRequest)")!);
    request.httpMethod = "GET";
    // request.httpBody = postString.data(using: String.Encoding.utf8);
    request.setValue(requestHeader, forHTTPHeaderField: "Mobiconnectheader")
    
    cedMageLoaders.addDefaultLoader(me: self);
    let task = URLSession.shared.dataTask(with: request){
      
      // check for fundamental networking error
      data, response, error in
      guard error == nil && data != nil else{
        print("error=\(error)")
        DispatchQueue.main.async{
          print(error?.localizedDescription as Any);
          cedMageLoaders.removeLoadingIndicator(me: self);
        }
        return;
      }
      
      // check for http errors
      if let httpStatus = response as? HTTPURLResponse , httpStatus.statusCode != 200{
        
        print("statusCode should be 200, but is \(httpStatus.statusCode)")
        print("response = \(response)")
        DispatchQueue.main.async{
          cedMageLoaders.removeLoadingIndicator(me: self);
          
        }
        return;
      }
      
      // code to fetch values from response :: start
      guard var jsonResponse = try? JSON(data: data!) else{return;}
      
      jsonResponse = jsonResponse[0]
      if(jsonResponse != nil){
        
        DispatchQueue.main.async{
            print("---response--\(jsonResponse)")
          cedMageLoaders.removeLoadingIndicator(me: self);
          if(jsonResponse["success"].stringValue == "true"){
            let stateListing = jsonResponse["states"].arrayValue;
            
            for d in 0..<stateListing.count {
              let key = stateListing[d]["region_id"].stringValue;
              let value = stateListing[d]["default_name"].stringValue;
              self.stateDict[value] = key;
            }
            if let textField = self.stackView.arrangedSubviews[self.indexOfStateField] as? SkyFloatingLabelTextField {
              print(textField.text as Any);
              self.stackView.removeArrangedSubview(textField);
              textField.removeFromSuperview();
              
              let dropDownButtonView = DropDownButtonView();
              dropDownButtonView.translatesAutoresizingMaskIntoConstraints = false;
              dropDownButtonView.dropDownButton.titleLabel?.font = UIFont.systemFont(ofSize: 12)
              dropDownButtonView.dropDownButton.setTitle(self.addressInfoArray["state"], for: UIControl.State.normal);
              dropDownButtonView.dropDownButton.addTarget(self, action: #selector(CheckoutStep2ViewController.showStateDropdown(_:)), for: UIControl.Event.touchUpInside);
              
              self.stackView.insertArrangedSubview(dropDownButtonView, at: self.indexOfStateField)
              let dropDownButtonViewHeight = translateAccordingToDevice(CGFloat(40.0));
              dropDownButtonView.heightAnchor.constraint(equalToConstant: dropDownButtonViewHeight).isActive = true;
              self.setLeadingAndTralingSpaceFormParentView(dropDownButtonView, parentView:self.stackView);
              
            }
            else if let dropDownButtonView = self.stackView.arrangedSubviews[self.indexOfStateField] as? DropDownButtonView {
              dropDownButtonView.dropDownButton.setTitle(self.addressInfoArray["state"], for: UIControl.State.normal);
            }
          }
          else{
            if let dropDownButtonView = self.stackView.arrangedSubviews[self.indexOfStateField] as? DropDownButtonView {
              self.stackView.removeArrangedSubview(dropDownButtonView);
              dropDownButtonView.removeFromSuperview();
              let textField = SkyFloatingLabelTextField();
              textField.borderStyle = UITextField.BorderStyle.roundedRect;
              textField.translatesAutoresizingMaskIntoConstraints = false;
              textField.font = UIFont.systemFont(ofSize: 12)
              textField.text = "";
              textField.placeholder = self.addressInfoArray["state"];
              
              self.stackView.insertArrangedSubview(textField, at: self.indexOfStateField)
              let textFieldHeight = translateAccordingToDevice(CGFloat(40.0));
              textField.heightAnchor.constraint(equalToConstant: textFieldHeight).isActive = true;
              self.setLeadingAndTralingSpaceFormParentView(textField, parentView:self.stackView);
            }
            else if let textField = self.stackView.arrangedSubviews[self.indexOfStateField] as? SkyFloatingLabelTextField {
              textField.font = UIFont.systemFont(ofSize: 12)
              textField.text = "";
              textField.placeholder = self.addressInfoArray["state"];
            }
          }
        }
      }
    }
    task.resume();
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
}
