//
//  homepageFeauredCollCell.swift
//  MageNative Magento Platinum
//
//  Created by cedcoss on 16/11/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class homepageFeauredCollCell: UICollectionViewCell {
  
  @IBOutlet weak var insideView: UIView!
  @IBOutlet weak var productImage: UIImageView!
  @IBOutlet weak var productName: UILabel!
  @IBOutlet weak var productPrice: UILabel!
  @IBOutlet weak var oldPrice: UILabel!
  
    @IBOutlet weak var stockView: UIView!
    
  var singleProduct: homepageProduct! {
    didSet{
      productImage.sd_setImage(with: URL(string: singleProduct.product_image!), placeholderImage: UIImage(named: "placeholder"))
      
      productName.text = singleProduct.product_name
      productPrice.text = singleProduct.regular_price
        if singleProduct.stock_status == "false"{
                stockView.isHidden = false
            }else{
                stockView.isHidden = true
            }
      let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: singleProduct.regular_price!)
      attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 1, range: NSMakeRange(0, attributeString.length))
      if singleProduct.special_price != "no_special"
      {
        productPrice.text = singleProduct.special_price; oldPrice.attributedText = attributeString
      }
      else
      {
        productPrice.text = singleProduct.regular_price; oldPrice.isHidden = true
      }
      
      insideView.cardView()
      
    }
  }
  
  override func awakeFromNib() {
    super.awakeFromNib()
    
  }
  
}
