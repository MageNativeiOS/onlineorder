//
//  vendordatafile.swift
//  MageNative Magento Platinum
//
//  Created by Saumya Kashyap on 25/11/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import Foundation

struct vendorInbox {
    let sender_name: String
    let sender: String
    let id: String
    let vendor_id: String
    let created_at: String
    let subject: String
    let message: String
    let new_message: String
    let updated_at: String
    let receiver_name: String
}

struct vendorNameList {
    let name: String
    let email: String
    let id: String
}
