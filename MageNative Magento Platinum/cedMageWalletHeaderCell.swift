//
//  cedMageWalletHeaderCell.swift
//  MageNative Magento Platinum
//
//  Created by Macmini on 12/12/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class cedMageWalletHeaderCell: UITableViewCell {
    
    @IBOutlet weak var addAmount: UIButton!
    @IBOutlet weak var amountInWallet: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
