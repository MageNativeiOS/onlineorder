//
//  HomeService.swift
//  MageNative Magento Platinum
//
//  Created by Hamza Usmani on 02/09/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import Foundation

class HomeService {
    
    var storeID: String {
        return UserDefaults.standard.value(forKey: "storeId") as? String ?? ""
    }
    var customerID: String {
        let userInfoDict = UserDefaults.standard.value(forKey: "userInfoDict") as? [String: String] ?? [String: String]()
        return userInfoDict["customerId"]!;
    }
    var locationDict:[String:String] {
        let location=UserDefaults.standard.value(forKey: "userCurrentLocation") as? [String:String] ?? [String:String]()
        return ["city": location["city"]!,
                "state": location["state"]!,
                "country_id": location["country"]!,
                "zipcode":location["zipcode"]!,
                "latitude":location["latitude"]!,
                "longitude": location["longitude"]!,
                "location": location["fullAddress"]!,
                "postcode": location["zipcode"]!]
    }
    var location:[String:[String:String]]{
        return ["location":locationDict]
    }
    
    static let shared = HomeService()
    
    func getHomepageData(controller: UIViewController, page: String, completion: @escaping ([HomeDataModel]?)-> Void) {
        var params = [String : Any]()
        if UserDefaults.standard.bool(forKey: "isLogin") {
         params = ["store_id" : storeID, "page":page , "customer_id":customerID,"extension_attributes":location]
        }else{
             params = ["store_id" : storeID, "page":page ,"extension_attributes":location]
           
        }
        ApiHandler.handle.requestDict(with: "getNewHomepage", params: params, requestType: .POST, controller: controller, completion: { data, error in
            guard let data = data else { return }
            guard let json = try? JSON(data: data) else { return }
            print(json)
            
            do{
                let resultJson = try json[0]["data"]["design"].rawData(options: [])
                let result = try JSONDecoder().decode([HomeDataModel].self, from: resultJson)
                print("DEBUG: Result Count is \(result.count)")
                completion(result)
            } catch {
                completion(nil)
                print(error.localizedDescription)
            }
        })
    }
}
