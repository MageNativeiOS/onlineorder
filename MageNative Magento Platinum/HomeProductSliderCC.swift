//
//  HomeProductSliderCC.swift
//  MageNative Magento Platinum
//
//  Created by Hamza Usmani on 02/09/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class HomeProductSliderCC: UICollectionViewCell {
    
    //MARK:- Properties
    
    static var reuseId:String = "HomeSliderProductCC"
    var product: HomeProduct? = nil
    var parent:UIViewController? = nil
    
    lazy var container:UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.mageSystemBackground
        view.translatesAutoresizingMaskIntoConstraints = false
       
        return view
    }()
    
    lazy var productImage:UIImageView = {
        let imageView = UIImageView()
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFit
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    lazy var productName:UILabel = {
        let label = UILabel()
        label.textColor = UIColor.mageLabel
        label.numberOfLines = 3
        label.font = .systemFont(ofSize: 12, weight: .medium)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    lazy var priceAndOffer:UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        return label
    }()
    
    lazy var wishlistButton:UIButton = {
        let button = UIButton(type: .system)
        button.setImage(UIImage(named: "heartEmpty")?.withRenderingMode(.alwaysTemplate), for: .normal)
        button.tintColor = UIColor.mageSecondarySystemBackground
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(wishlistTapped(_:)), for: .touchUpInside)
        return button
    }()
    
    //MARK:- Lifecycle
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    

    fileprivate func commonInit() {
        addSubview(container)
        container.anchor(top: topAnchor, left: leadingAnchor, bottom: bottomAnchor, right: trailingAnchor,
                         paddingTop: 2, paddingLeft: 2, paddingBottom: 2, paddingRight: 2)
        
        container.addSubview(productImage)
        productImage.anchor(top: container.topAnchor, left: container.leadingAnchor, right: container.trailingAnchor,
                            paddingTop: 5, paddingLeft: 5, paddingRight: 5, height: 160)
        
        container.addSubview(productName)
        productName.anchor(top: productImage.bottomAnchor, left: container.leadingAnchor, right: container.trailingAnchor,
                           paddingTop: 5, paddingLeft: 3, paddingRight: 3)
        
        container.addSubview(priceAndOffer)
        priceAndOffer.anchor(top: productName.bottomAnchor, left: container.leadingAnchor, bottom: container.bottomAnchor, right: container.trailingAnchor,
                             paddingTop: 2, paddingLeft: 3, paddingBottom: 5, paddingRight: 3)
        
        container.addSubview(wishlistButton)
        wishlistButton.setDimensions(width: 44, height: 44)
        wishlistButton.anchor(top: container.topAnchor, right: container.trailingAnchor, paddingTop: 10, paddingRight: 10)
        
        container.bringSubviewToFront(wishlistButton)
        
    }
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        container.layer.shadowColor = UIColor.mageLabel.cgColor
        container.layer.shadowOffset = .zero
        container.layer.shadowOpacity = 0.3
        container.layer.shadowRadius = 2.0
    }
    
    func populate(with data: HomeProduct) {
        self.product = data
        self.productImage.sd_setImage(with: URL(string: product?.product_image ?? ""), placeholderImage: UIImage(named: "placeholder"))
        self.productName.text = product?.product_name ?? ""
        
        if product?.Inwishlist == "OUT" {
            wishlistButton.isHidden = true
        } else {
            wishlistButton.isHidden = false
        }
        
        
        let finalStr = NSMutableAttributedString()
        let currency = NSMutableAttributedString(string: "AED ", attributes: [.foregroundColor:UIColor.lightGray,.font: UIFont.systemFont(ofSize: 12, weight: .semibold)])
        
        if product?.special_price != "no_special" {
            let price = NSMutableAttributedString(string: (product?.special_price?.replacingOccurrences(of: "AED", with: ""))!, attributes: [.foregroundColor:UIColor.mageLabel,.font: UIFont.systemFont(ofSize: 13, weight: .bold)])
            
            let reg = NSMutableAttributedString(string: (product?.regular_price?.replacingOccurrences(of: "AED", with: ""))!, attributes: [.foregroundColor:UIColor.lightGray,.font: UIFont.systemFont(ofSize: 11, weight: .semibold),.strikethroughStyle: 1.0])
            
            let ofr = ""//" \(product?.offer ?? 0)"
            let offer = NSMutableAttributedString(string: "\n" + ofr + "% off", attributes: [.foregroundColor:UIColor.yellow,.font: UIFont.systemFont(ofSize: 11, weight: .semibold)])
            
            finalStr.append(currency)
            finalStr.append(price)
            finalStr.append(NSMutableAttributedString(string: "  "))
            finalStr.append(reg)
            finalStr.append(offer)
            
        } else {
            let price = NSMutableAttributedString(string: (product?.regular_price?.replacingOccurrences(of: "AED", with: ""))!, attributes: [.foregroundColor:UIColor.mageLabel,.font: UIFont.systemFont(ofSize: 13, weight: .bold)])
            
            finalStr.append(currency)
            finalStr.append(price)
        }
        self.priceAndOffer.attributedText = finalStr
        
      
    }
    
    @objc func wishlistTapped(_ sender: UIButton) {
        sender.tintColor = .red
    }
    
    
    
}
