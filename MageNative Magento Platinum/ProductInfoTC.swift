//
//  ProductInfoTC.swift
//  MageNative Magento Platinum
//
//  Created by Hamza Usmani on 22/08/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class ProductInfoTC: UITableViewCell {

    //MARK:- Porperties
    
    static var reuseID:String = "ProductInfoTC"
    var item: [String:String] = [:] {
        didSet { configureUI() }
    }
    
    var dataToShow = ["Product Name", "Product SKU", "Product Price", "Product Quantity", "Row Total", "RMA Quantity"]
    
    lazy var generalStack:UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.distribution = .fillEqually
        stack.alignment = .fill
        stack.spacing = 2.0
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()
    
    //MARK:- Lifecycle
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        selectionStyle = .none

        addSubview(generalStack)
        generalStack.topAnchor.constraint(equalTo: topAnchor, constant: 8).isActive = true
        generalStack.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16).isActive = true
        generalStack.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16).isActive = true
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK:- Helpers

    private func configureUI() {
        generalStack.subviews.forEach({ $0.removeFromSuperview() })
        
        for heading in dataToShow {
            let head = UILabel()
            head.font = .systemFont(ofSize: 14, weight: .semibold)
            let val = UILabel()
            val.font = .systemFont(ofSize: 14, weight: .medium)
            
            switch heading {
            case "Product Name":
                 head.text = heading
                 val.text = item["name"]
            case "Product SKU":
                head.text = heading
                val.text = item["product_sku"]
            case "Product Price":
                head.text = heading
                val.text = item["price"]
            case "Product Quantity":
                head.text = heading
                val.text = item["qty_ordered"]
            case "Row Total":
                head.text = heading
                val.text = item["row_total"]
            case "RMA Quantity":
                head.text = heading
                val.text = item["qty_ordered"]
            default:
                return
            }
            
            let stack = UIStackView(arrangedSubviews: [head,val])
            stack.axis = .horizontal
            stack.distribution = .fillEqually
            stack.spacing = 2.0
            
            generalStack.addArrangedSubview(stack)
        }
        
        
    }
}
