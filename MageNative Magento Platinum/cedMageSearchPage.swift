/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */


import UIKit

class cedMageSearchPage: cedMageViewController,UISearchBarDelegate,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var searchResults: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    var suggentionsArray = [[String:String]]()
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchBar.delegate = self
        searchResults.dataSource = self
        searchResults.delegate = self
        let color = cedMage.getInfoPlist(fileName: "cedMage", indexString: "themeColor") as! String
        searchBar.barTintColor = cedMage.UIColorFromRGB(colorCode: color)
       // searchBar.backgroundImage = UIImage.imageFromColor(color: cedMage.UIColorFromRGB(colorCode: color), frame: searchBar.frame)
        self.navigationItem.rightBarButtonItems?.last?.isEnabled = false
        searchBar.placeholder = "Enter Your String".localized
       searchBar.scopeButtonTitles?[0] = "Products".localized
        searchResults.isHidden = true
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidDisappear(_ animated: Bool) {
         self.dismiss(animated: true, completion: {})
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
    {
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(getHint), object: nil)
        self.perform(#selector(getHint), with: nil, afterDelay: 2)
    }
    
    @objc func getHint() {
        if((searchBar.text?.count)! >= 3)
        {
            let keyword = searchBar.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            var postString = ["keyword":keyword! as String]
            if let filterType = UserDefaults.standard.value(forKey: "filterType") as? String {
                if filterType == "zipcode" {
                    postString["postcode"] = Settings.currentLocation?["zipcode"] ?? ""
                } else if filterType == "city_state_country" {
                    postString["city"] = Settings.currentLocation?["city"] ?? ""
                    postString["state"] = Settings.currentLocation?["state"] ?? ""
                    postString["country_id"] = Settings.currentLocation?["country"] ?? ""
                } else {
                    postString["latitude"] = Settings.currentLocation?["latitude"] ?? ""
                    postString["longitude"] = Settings.currentLocation?["longitude"] ?? ""
                }
            }
            self.sendRequest(url: "mobiconnectadvcart/search/autocomplete", params: postString)
        }
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        if let viewController =  UIStoryboard(name: "categorylayouts", bundle: nil).instantiateViewController(withIdentifier: "cedMageDefaultCollection") as?  cedMageDefaultCollection {
            if let string = searchBar.text {
                viewController.searchString = string
            }
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        print(searchBar.text as Any)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        searchBar.resignFirstResponder()
    }
    
    override func recieveResponse(data: Data?, requestUrl: String?, response: URLResponse?) {
        if let data = data {
        searchBar.resignFirstResponder()
        
            guard let json = try? JSON(data:data)else{return;}
        print(json)
        print("*****((*(*(*((")
        if(json[0]["data"]["status"].stringValue != "empty")
        {
            print("****^^^^^^")
            self.suggentionsArray.removeAll()
            for (_,value) in json[0]["data"]["suggestion"]
            {
                
                var data = [String:String]()
                data["product_id"] = value["product_id"].stringValue
                data["product_name"] = value["product_name"].stringValue
                self.suggentionsArray.append(data)
            }
            searchResults.isHidden = false
        }
        else
        {
            print("*****______")
            searchResults.isHidden = true
            self.navigationController?.view.makeToast("No Suggestion Found!".localized, duration: 1, position: .center, title: nil, image: nil, style: nil, completion: nil)
        }
        self.searchResults.reloadData()
        self.activityIndicator.stopAnimating()
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return suggentionsArray.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "searchBarCell")
        cell?.textLabel?.text = suggentionsArray[indexPath.row]["product_name"]
        cell?.textLabel?.fontColorTool()
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let viewController =  UIStoryboard(name: "categorylayouts", bundle: nil).instantiateViewController(withIdentifier: "cedMageDefaultCollection") as?  cedMageDefaultCollection {
            var product_name = suggentionsArray[indexPath.row]["product_name"]!;
            product_name = product_name.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!;
            viewController.searchString = product_name;
            //viewController.categoryId = suggentionsArray[indexPath.row]["product_id"]!;
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
}
