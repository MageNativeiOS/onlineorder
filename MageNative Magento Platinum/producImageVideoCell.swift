//
//  producImageVideoCell.swift
//  MageNative Magento Platinum
//
//  Created by cedcoss on 23/03/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit


class producImageVideoCell: UICollectionViewCell {
  
  @IBOutlet weak var productImage: UIImageView! 
 
  @IBOutlet weak var videoView: YouTubePlayerView!
}
