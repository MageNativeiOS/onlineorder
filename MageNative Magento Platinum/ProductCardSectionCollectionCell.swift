//
//  ProductCardSectionCollectionCell.swift
//  MageNative Magento Platinum
//
//  Created by cedcoss on 19/01/21.
//  Copyright © 2021 CEDCOSS Technologies Private Limited. All rights reserved.
//

import Foundation
class ProductCardSectionCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
