//
//  NotificationCentreVC.swift
//  MageNative Magento Platinum
//
//  Created by Manohar Singh Rawat on 18/01/21.
//  Copyright © 2021 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class NotificationCentreVC: MagenativeUIViewController {

    @IBOutlet weak var notificationTableView: UITableView!
    
    var page = 1
    
    var listArray = [[String:String]]()
    var loadMore = true;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
        // Do any additional setup after loading the view.
    }
    
    func loadData(){
        var param = [String:String]()
        if UserDefaults.standard.bool(forKey: "isLogin"){
            if(UserDefaults.standard.object(forKey: "userInfoDict") != nil){
                let userInfoDict = UserDefaults.standard.object(forKey: "userInfoDict") as! [String:String]
            param["customer_id"] = userInfoDict["customerId"] ?? ""
            }
        }
        param["page"] = "\(page)"
        
        self.sendRequest(url: "mobinotifications/listNotification", params: param)
    }
    
    override func recieveResponse(data: Data?, requestUrl: String?, response: URLResponse?) {
        if let data = data{
            do{
                guard let json = try? JSON(data: data) else{
                    return;
                }
                let jsonVar = json[0]
                
                if(jsonVar["data"]["status"].stringValue == "true"){
                    for index in jsonVar["data"]["list"].arrayValue{
                        listArray.append(["title":index["title"].stringValue,"message_text":index["message_text"].stringValue,"link_id":index["link_id"].stringValue,"link_type":index["link_type"].stringValue,"image":index["image"].stringValue])
                    }
                    notificationTableView.delegate=self;
                    notificationTableView.dataSource = self;
                    notificationTableView.reloadData()
                }
                else{
                    loadMore = false;
                }
            }
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        print("didEndDragging")
        
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        
        if (maximumOffset - currentOffset) <= 40 {
            page += 1
            if(loadMore){
                loadData()
            }
        }
        
    }

}
extension NotificationCentreVC: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listArray.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationTableCell") as! NotificationTableCell
        cell.headingLabel.text = listArray[indexPath.row]["title"]!
        cell.contentLabel.text = listArray[indexPath.row]["message_text"]!
        if(listArray[indexPath.row]["message_text"]! != ""){
            cell.notificationImageView.sd_setImage(with: URL(string:listArray[indexPath.row]["message_text"]!), placeholderImage: UIImage(named:"placeholder"))
        }
        cell.cellView.cardView()
        return cell;
    }
}

extension NotificationCentreVC: UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data = listArray[indexPath.row]
        let linkType = data["link_type"]!
        if (linkType == "1"){
            let vc = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "productSinglePageViewController") as! ProductSinglePageViewController
            vc.product_id = data["link_id"]!
            self.navigationController?.pushViewController(vc, animated: true)
            /*let productview:ProductViewController = self.storyboard!.instantiateViewController()
            self.productId = notificationData["product"]
            productview.productId = self.productId!
            productview.isProductLoading = true;
            self.navigationController?.pushViewController(productview
                , animated: true)*/
            
        }else  if (linkType == "2"){
            let link_id = data["link_id"]
            /*let coll = collection(id: link_id, title: "collection")
            
            let viewControl:ProductListViewController = self.storyboard!.instantiateViewController()
            viewControl.isfromHome = true
            
            viewControl.collect = coll
            
            self.navigationController?.pushViewController(viewControl, animated: true)*/
            let vc = UIStoryboard(name: "categorylayouts", bundle: nil).instantiateViewController(withIdentifier: "cedMageDefaultCollection") as! cedMageDefaultCollection
            vc.selectedCategory = link_id!
            self.navigationController?.pushViewController(vc, animated: true)
            
        }else  if (linkType == "3"){
            let view = UIStoryboard(name: "cedMageAccounts", bundle: nil)
            let vc = view.instantiateViewController(withIdentifier: "cmsWebView") as! cedMageCmsWebView
            
            vc.pageUrl = data["link_id"]!
            self.navigationController?.pushViewController(vc, animated: true)
            
            
        }
    }
}
