//
//  newProductCardSection.swift
//  MageNative Magento Platinum
//
//  Created by cedcoss on 18/01/21.
//  Copyright © 2021 CEDCOSS Technologies Private Limited. All rights reserved.
//

import Foundation
import FSCalendar
class newProductCardSection: UIView, UICollectionViewDelegate, UICollectionViewDataSource, FSCalendarDelegate, FSCalendarDataSource {
   
    var stackView : UIStackView!
    var view = UIView()
  
    @IBOutlet weak var calendarButton: UIButton!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var priceTextField: UITextField!
    var productImgsArray = [String]();
    @IBOutlet weak var dateTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var fromTextField: UITextField!
    @IBOutlet weak var collectionView: UICollectionView!
    override init(frame: CGRect) {
           super.init(frame: frame)
           xibSetup()
       }
       
       required init?(coder: NSCoder) {
           super.init(coder: coder)
           xibSetup()
       }
      
       func xibSetup()
       {
        
           view = loadViewFromNib()
           view.frame = bounds
           view.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        priceTextField.layer.borderColor = UIColor.black.cgColor
        nameTextField.layer.borderColor = UIColor.black.cgColor
        fromTextField.layer.borderColor = UIColor.black.cgColor
        emailTextField.layer.borderColor = UIColor.black.cgColor
        dateTextField.layer.borderColor = UIColor.black.cgColor
        textView.layer.borderColor = UIColor.black.cgColor
        priceTextField.layer.borderWidth = 1.0
        nameTextField.layer.borderWidth = 1.0
        fromTextField.layer.borderWidth = 1.0
        emailTextField.layer.borderWidth = 1.0
        dateTextField.layer.borderWidth = 1.0
        textView.layer.borderWidth = 1.0
        dateTextField.isUserInteractionEnabled = false
        collectionView.dataSource = self
        collectionView.delegate = self
        //calendarView.root = self
        collectionView.register(UINib(nibName: "ProductCardSectionCollectionCell", bundle: nil),
                                forCellWithReuseIdentifier:"ProductCardSectionCollectionCell")
    addSubview(view)
       }
       
       @objc func loadViewFromNib() -> UIView
        {
            let bundle = Bundle(for: type(of: self))
            let nib = UINib(nibName: "newProductCardSection", bundle: bundle)
            
            // Assumes UIView is top level and only object in CustomView.xib file
            let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
            return view
        }
   

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return productImgsArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductCardSectionCollectionCell", for: indexPath) as! ProductCardSectionCollectionCell;
        
        cell.imageView.sd_setImage(with: URL(string: productImgsArray[(indexPath as NSIndexPath).row]), placeholderImage: UIImage(named: "placeholder"))
       
        return cell
    }
    
    @objc func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
         
          //let width = (collectionView.bounds.size.width - 5 * 3) / 2 //some width
          let height = collectionView.bounds.size.height-5 //ratio
          let width = collectionView.bounds.size.width/2 //some width
          
          return CGSize(width: width, height: height);
      }
      
     @objc func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
          return UIEdgeInsets.init(top: 5, left: 5, bottom: 5, right: 5);
      }
    
    
}
