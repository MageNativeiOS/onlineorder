//
//  RmaReasonController.swift
//  MageNative Magento Platinum
//
//  Created by Hamza Usmani on 21/08/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

struct ReturnData {
    let label: String
    let value: String
}

struct Address {
    let region: String
    let postcode: String
    let region_id: String
    let telephone: String
    let lastname: String
    let street: String
    let country_id: String
    let city: String
    let firstname: String
}



class RmaReasonController: MagenativeUIViewController {
    
    //MARK:- Properties
    lazy var reasonTable:UITableView = {
        let table = UITableView()
        table.separatorStyle = .singleLine
        table.backgroundColor = .white
        
        table.register(ProductInfoTC.self, forCellReuseIdentifier: ProductInfoTC.reuseID)
        table.register(AddressTC.self, forCellReuseIdentifier: AddressTC.reuseID)
        table.register(GeneralInfoTC.self, forCellReuseIdentifier: GeneralInfoTC.reuseID)
        table.translatesAutoresizingMaskIntoConstraints = false
        return table
    }()
    
    var json: JSON?
    var orderID: String = ""
    
    var resolutionData: [ReturnData] = []
    var packageCondition: [ReturnData] = []
    var reasons: [ReturnData] = []
    var orderedItems = [[String:String]]()
    var billingAddress: Address?
    var shippingAddress: Address?
    
    //MARK:- Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        parseJSON()
    }
    
    
    //MARK:- API Calls
    
    override func recieveResponse(data: Data?, requestUrl: String?, response: URLResponse?) {
        guard let data = data else { return }
        guard let json = try? JSON(data: data) else { return }
        
        print(json)
        
        if json[0]["data"]["success"].boolValue {
           // view.makeToast(json[0]["data"]["message"].stringValue)
            view.makeToast(json[0]["data"]["message"].stringValue, duration: 2.0, position: .center)
        }
        
        
        
    }
    
    
    
    
    func parseJSON() {
        guard let json = json else { return }
        print(json)
        
        for result in json["data"]["resolution"].arrayValue {
            let temp = ReturnData(label: result["label"].stringValue, value: result["value"].stringValue)
            resolutionData.append(temp)
        }
        
        for result in json["data"]["packageCondition"].arrayValue {
            let temp = ReturnData(label: result["label"].stringValue, value: result["value"].stringValue)
            packageCondition.append(temp)
        }
        
        for result in json["data"]["reason"].arrayValue {
            let temp = ReturnData(label: result["label"].stringValue, value: result["value"].stringValue)
            reasons.append(temp)
        }
        
        for product in json["data"]["order_data"]["order_items"].arrayValue {
            let temp = ["storeID": product["store_id"].stringValue, "price": product["price"].stringValue, "row_total":product["row_total"].stringValue,
                        "product_id":product["product_id"].stringValue, "product_sku":product["product_sku"].stringValue,"item_id":product["item_id"].stringValue,
                        "name": product["name"].stringValue,"qty_ordered": product["qty_ordered"].stringValue, "product_type":product["product_type"].stringValue]
            
            orderedItems.append(temp)
        }
        
        let billing = json["data"]["order_data"]["billing"]
        let temp = Address(region: billing["region"].stringValue, postcode: billing["postcode"].stringValue, region_id: billing["region_id"].stringValue,
                           telephone: billing["telephone"].stringValue, lastname: billing["lastname"].stringValue, street: billing["street"][0].stringValue,
                           country_id: billing["country_id"].stringValue, city: billing["city"].stringValue, firstname: billing["firstname"].stringValue)
        
        billingAddress = temp
        
        let shipping = json["data"]["order_data"]["shipping"]
        let tempTwo = Address(region: shipping["region"].stringValue, postcode: shipping["postcode"].stringValue, region_id: shipping["region_id"].stringValue,
                           telephone: shipping["telephone"].stringValue, lastname: shipping["lastname"].stringValue, street: shipping["street"][0].stringValue,
                           country_id: shipping["country_id"].stringValue, city: shipping["city"].stringValue, firstname: shipping["firstname"].stringValue)
        
        shippingAddress = tempTwo
        
        reasonTable.delegate = self
        reasonTable.dataSource = self
        reasonTable.reloadData()
    }
    
    
    //MARK:- Helpers
    
    private func configureUI() {
        view.addSubview(reasonTable)
        NSLayoutConstraint.activate([
            reasonTable.topAnchor.constraint(equalTo: view.topAnchor),
            reasonTable.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            reasonTable.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            reasonTable.bottomAnchor.constraint(equalTo: view.bottomAnchor),
        ])
    }
    
}

//MARK:- UITableViewDataSource / Delegate

extension RmaReasonController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: ProductInfoTC.reuseID, for: indexPath) as! ProductInfoTC
            cell.item = orderedItems[0]
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: AddressTC.reuseID, for: indexPath) as! AddressTC
            cell.address = shippingAddress
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: AddressTC.reuseID, for: indexPath) as! AddressTC
            cell.address = billingAddress
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: GeneralInfoTC.reuseID, for: indexPath) as! GeneralInfoTC
            cell.delegate = self
            cell.reasonArray = reasons
            cell.resolutionArray = resolutionData
            cell.conditionArray = packageCondition
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return 160
        case 1,2:
            return 150
        default:
            return 200
        }
    }
    
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return "ITEM INFORMATION"
        case 1:
            return "SHIPPING ADDRESS"
        case 2:
            return "BILLING ADDRESS"
        default:
            return "GENERAL INFORMATION"
        }
    }
    
}

//MARK:- RMADelegate

extension RmaReasonController: RMADelegate {
    func optionsSelected(by button: UIButton, atCell cell: UITableViewCell) {
        guard let cell = cell as? GeneralInfoTC else { return }
        
        guard let reason = cell.selectedReason else { view.makeToast("Select Options For Return "); return }
        guard let condition = cell.selectedCondition else { view.makeToast("Select Options For Return "); return }
        guard let resolution = cell.selectedResolution else { view.makeToast("Select Options For Return "); return }
        
        var param = [String:String]()
        
        let userInfoDict = defaults.object(forKey: "userInfoDict") as? [String: String] ?? [String: String]();
        guard let cust_id = userInfoDict["customerId"] else { return }
        
        param["customer_id"] = cust_id
        param["order_id"] = orderID
        param["reason"] = reason
        param["pk_condition"] = condition
        param["resolution"] = resolution
        
        var items = [String]()
        var itemIds = [String]()
        for item in orderedItems {
            items.append(item["qty_ordered"] ?? "")
            itemIds.append(item["item_id"] ?? "0")
        }
        param["rma_qty"] = "\(items)"
        param["item_id"] = "\(itemIds)"
        
        self.sendRequest(url: "mobiconnect/mobirma/saverma", params: param, store: false)
        
    }
    
    
}
