/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */


import UIKit

class cate_product_cell: UITableViewCell ,UICollectionViewDelegate,UICollectionViewDataSource,NSCacheDelegate{

    @IBOutlet weak var product_collection: UICollectionView!
    var product_array = [[String:String]]()

    var viewcontrol = UIViewController()
  
    var cat_id = String()
    override func awakeFromNib() {
        product_collection.delegate = self
        product_collection.dataSource = self
      
        super.awakeFromNib()
      
        // Initialization code
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
    
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return product_array.count + 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if(indexPath.row < product_array.count ){
            let cell = product_collection.dequeueReusableCell(withReuseIdentifier: "cat_product", for: indexPath as IndexPath) as! cate_product_collection_cell
            cell.product_name.fontColorTool()
            cell.product_name.text
                = product_array[indexPath.row]["product_name"]
            let price = product_array[indexPath.row]["product_price"]
            cell.product_price!.font = UIFont(name: "Roboto-Regular", size: 12.0)
            cell.product_name!.font = UIFont(name: "Roboto-Regular", size: 12.0)
            cell.product_price.text = "Price" + " \(price!)"
             cell.product_price.fontColorTool()
            let img_url = product_array[indexPath.row]["product_image"]
            cedMageImageLoader.shared.loadImgFromUrl(urlString:img_url!,completionHandler: { (image: UIImage?, url: String) in
                
                DispatchQueue.main.async {
                    cell.product_img.image = image
                }
                
            })
               cell.cardView()
            return cell
        }
        else {
            let cell = product_collection.dequeueReusableCell(withReuseIdentifier: "viewAll", for: indexPath as IndexPath) as! cate_product_collection_cell
           //--old cell.backgroundColor = UIColor.clear
            cell.backgroundColor = UIColor.blue
            cell.viewall.text = "View All".localized
            return cell
        }
        

        
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if(indexPath.row < product_array.count){
            let product = product_array[indexPath.row]
            print(product)
            let productview = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "rootPageViewController") as! cedMageProductViewRoot
            productview.pageData = product_array as NSArray
            let instance = cedMage.singletonInstance
            instance.storeParameterInteger(parameter: indexPath.row)
            viewcontrol.navigationController?.pushViewController(productview
                , animated: true)
        }
        else {
            let contol =  UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "productListingViewController") as! ProductListingViewController
            contol.categoryId = cat_id
            viewcontrol.navigationController?.pushViewController(contol, animated: true)
        }
        //self.navigationController?.pushViewController(productview, animated: true)

    }
    private func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        if(indexPath.row < product_array.count){
            return CGSize(width:150, height:200)
        }
        else{
            return CGSize(width:130, height:150)
        }
    }

}
