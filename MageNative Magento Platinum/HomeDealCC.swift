//
//  HomeDealCC.swift
//  MageNative Magento Platinum
//
//  Created by Hamza Usmani on 02/09/20.
//  Copyright © 2020 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class HomeDealCC: UICollectionViewCell {
    
    //MARK:- Properties
    
    static var reuseId:String = "HomeDealCC"
    
    lazy var dealImage:UIImageView = {
        let imageView = UIImageView()
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFit
        imageView.layer.cornerRadius = 5
        imageView.backgroundColor = .mageSecondarySystemBackground
        imageView.image = UIImage(named: "placeholder")
        return imageView
    }()
    
    //MARK:- Lifecycle
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(dealImage)
        dealImage.anchor(top: topAnchor, left: leadingAnchor, bottom: bottomAnchor, right: trailingAnchor, paddingTop: 8, paddingLeft: 2, paddingBottom: 8, paddingRight: 2)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK:- Helper Functions
    
    func populate(with image: String?) {
        dealImage.sd_setImage(with: URL(string: image ?? ""), placeholderImage: UIImage(named: "placeholder"))
    }
    
    
}
