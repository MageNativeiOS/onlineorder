//
//  rmaProductViewCell.swift
//  MageNative Magento Platinum
//
//  Created by Macmini on 13/12/19.
//  Copyright © 2019 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class rmaProductViewCell: UITableViewCell {
// product Info
    @IBOutlet weak var prodView: UIView!
    @IBOutlet weak var productLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var rmaQtyLabel: UILabel!
    @IBOutlet weak var skuLabel: UILabel!
    @IBOutlet weak var orderedQtyLabel: UILabel!
    // Order Info
    
    @IBOutlet weak var orderView: UIView!
    @IBOutlet weak var orderStatus: UILabel!
    @IBOutlet weak var orderIdLabel: UILabel!
    
    // ShippingAddress
    @IBOutlet weak var shippingView: UIView!
    @IBOutlet weak var addressLabel: UILabel!
    
    // CustomerInfo
    
    @IBOutlet weak var customerView: UIView!
    @IBOutlet weak var customerInfoLabel: UILabel!
    
    // General info
    
    @IBOutlet weak var generalView: UIView!
    @IBOutlet weak var reasonLabel: UILabel!
    @IBOutlet weak var packageLabel: UILabel!
    @IBOutlet weak var resolutionLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
//        prodView.cardView()
//        orderView.cardView()
//        shippingView.cardView()
//        customerView.cardView()
//        generalView.cardView()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
