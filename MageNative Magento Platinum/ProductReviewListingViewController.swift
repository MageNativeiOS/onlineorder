/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */

import UIKit

class ProductReviewListingViewController: MagenativeUIViewController,UIScrollViewDelegate {
    
    var product_id = "";
    var currentPageNum = 1;
    var loadMoreData = false;
    
    var ratingViews = [Int:[String:String]]();
    let textFieldsToRender = ["What's Your Nickname?".localized, "Summary Of Your Review".localized, "Let Us Know Your Thoughts".localized];
    
    var productreview = [String:[String:String]]();
    var vote = [String:[String:String]]();
    
    @IBOutlet weak var writeReviewButton: UIButton!
    @IBOutlet weak var writeReviewButtonHeight: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.makeWriteReviewButton();
        self.basicFoundationToRenderView(bottomMargin: CGFloat(0.0));
        scrollView.delegate = self;
       // , "page":String(currentPageNum)]
        self.makeRequestToAPI("mobiconnect/review/product",dataToPost: ["prodID":product_id,"show_all":"true"]);
        
    }
    
    override func parseResponseReceivedFromAPI(_ jsonResponse:JSON){
        print("jsonResponse :: parseResponseReceivedFromAPI");
        print(jsonResponse);
        let jsonResponse = jsonResponse[0]
        if(jsonResponse["data"]["status"].stringValue == "success"){
            
            for (_,val) in jsonResponse["data"]["productreview"]{
                var tempData = [String:String]();
                let key = val["review-id"].stringValue;
                tempData["review-id"] = val["review-id"].stringValue;
                tempData["review-by"] = val["review-by"].stringValue;
                tempData["review-description"] = val["review-description"].stringValue;
                tempData["posted_on"] = val["posted_on"].stringValue;
                tempData["review-title"] = val["review-title"].stringValue;
                self.productreview[key] = tempData;
                
                var voteArrTemp = [String:String]();
                for (_,valVote) in val["vote"]
                {
                    var key = "";
                    var value = "";
                    for(k,v) in valVote{
                        if(k == "rating-code"){
                            key = v.stringValue;
                        }
                        else{
                            value = v.stringValue;
                        }
                    }
                    voteArrTemp[key] = value;
                }
                self.vote[key] = voteArrTemp;
                
            }
            stackView.subviews.forEach({ $0.removeFromSuperview() });// helpful for second time review fetch
            self.renderAddressListingPage();
        }
        else{
            if(productreview.count > 0){
                self.loadMoreData = false;
            }
            else{
                self.renderNoDataImage(imageName: "noReviews");
            }
        }
        
    }
    
    
    func makeWriteReviewButton(){
        
        //let color = cedMage.getInfoPlist(fileName:"cedMage",indexString: "themeColor") as! String
        //let theme_color = cedMage.UIColorFromRGB(colorCode: color);
        
        writeReviewButton.setTitle("Write Review".localized, for: UIControl.State.normal);
      if #available(iOS 13.0, *) {
        writeReviewButton.setTitleColor(UIColor.label, for: UIControl.State.normal)
      } else {
        writeReviewButton.setTitleColor(.black, for: UIControl.State.normal)
      };
        writeReviewButton.fontColorTool()
        writeReviewButton.makeCornerRounded(cornerRadius: translateAccordingToDevice(CGFloat(5.0)));
        writeReviewButton.layer.borderWidth =  CGFloat(2);
        writeReviewButton.layer.borderColor = UIColor.gray.cgColor;
        writeReviewButton.titleLabel?.font = UIFont(fontName: "HelveticaNeue-Medium", fontSize: CGFloat(15.0));
        writeReviewButton.addTarget(self, action: #selector(ProductReviewListingViewController.writeReviewButtonPressed(_:)), for: UIControl.Event.touchUpInside);
        writeReviewButtonHeight.constant = translateAccordingToDevice(CGFloat(40.0));
    }
    
    
    func renderAddressListingPage(){
        
        self.makeSomeSpaceInStackView(height: CGFloat(0.0));
        
        for (key,val) in self.productreview{
            
            let productRatingListView = ProductRatingListView();
            productRatingListView.translatesAutoresizingMaskIntoConstraints = false;
            
            productRatingListView.reviewTitle.font = UIFont(fontName: "HelveticaNeue-Medium", fontSize: CGFloat(15.0));
            productRatingListView.reviewTitle.text = val["review-title"]!;
            productRatingListView.reviewTitle.fontColorTool()
            productRatingListView.reviewTitleHeight.constant = translateAccordingToDevice(CGFloat(30.0));
            
            productRatingListView.reviewByAndDate.font = UIFont(fontName: "", fontSize: CGFloat(15.0));
            productRatingListView.reviewByAndDate.text = "By : ".localized+val["review-by"]!+"\n"+val["posted_on"]!;
            productRatingListView.reviewByAndDate.fontColorTool()
            
            let fontToApply = UIFont(fontName: "", fontSize: CGFloat(15.0))!;
            productRatingListView.detailedReview.font = fontToApply;
            productRatingListView.detailedReview.text = val["review-description"]!;
            productRatingListView.detailedReview.fontColorTool()
            let textAttributes = [NSAttributedString.Key.font: fontToApply];
            let rect = val["review-description"]!.boundingRect(with: CGSize(width: screenSize.width, height: CGFloat.greatestFiniteMagnitude), options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: textAttributes, context: nil);
            let detailedReviewLabelHeight = translateAccordingToDevice(CGFloat(rect.height))+10;
            productRatingListView.detailedReviewHeight.constant = detailedReviewLabelHeight;
            
            productRatingListView.stackViewForStarRating.translatesAutoresizingMaskIntoConstraints = false;
            productRatingListView.stackViewForStarRating.axis  = NSLayoutConstraint.Axis.vertical;
            productRatingListView.stackViewForStarRating.distribution  = UIStackView.Distribution.equalSpacing;
            productRatingListView.stackViewForStarRating.alignment = UIStackView.Alignment.center;
            productRatingListView.stackViewForStarRating.spacing   = 0.0;
            
            for (key,val) in vote[key]!{
                print("rate key = \(key)")
                let ratingOnProductReviewListView = RatingOnProductReviewListView();
                ratingOnProductReviewListView.translatesAutoresizingMaskIntoConstraints = false;
                
                ratingOnProductReviewListView.topLabel.text = key.localized+" : ";
                ratingOnProductReviewListView.topLabel.fontColorTool()
                print(val);
                ratingOnProductReviewListView.ratingView.rating = Float(val)!;
                
                productRatingListView.stackViewForStarRating.addArrangedSubview(ratingOnProductReviewListView);
                let ratingOnProductReviewListViewHeight = translateAccordingToDevice(CGFloat(20.0));
                ratingOnProductReviewListView.heightAnchor.constraint(equalToConstant: ratingOnProductReviewListViewHeight).isActive = true;
                self.setLeadingAndTralingSpaceFormParentView(ratingOnProductReviewListView, parentView:productRatingListView.stackViewForStarRating);
            }
            
            var stackViewHeight = translateAccordingToDevice(CGFloat(20.0))*CGFloat((vote[key]?.count)!);
            if(stackViewHeight == 0){
                stackViewHeight = translateAccordingToDevice(CGFloat(20.0))*3;
            }
            else if(stackViewHeight < translateAccordingToDevice(CGFloat(40.0))){
                stackViewHeight = CGFloat(40.0);
                let ratingOnProductReviewListView = RatingOnProductReviewListView();
                ratingOnProductReviewListView.translatesAutoresizingMaskIntoConstraints = false;
                ratingOnProductReviewListView.topLabel.fontColorTool()
                ratingOnProductReviewListView.topLabel.text = "  ";
                print(val);
                ratingOnProductReviewListView.ratingView.isHidden = true;
                ratingOnProductReviewListView.ratingView.fontColorTool()
                productRatingListView.stackViewForStarRating.addArrangedSubview(ratingOnProductReviewListView);
                let ratingOnProductReviewListViewHeight = translateAccordingToDevice(CGFloat(20.0));
                ratingOnProductReviewListView.heightAnchor.constraint(equalToConstant: ratingOnProductReviewListViewHeight).isActive = true;
                self.setLeadingAndTralingSpaceFormParentView(ratingOnProductReviewListView, parentView:productRatingListView.stackViewForStarRating);
            }
            
            
            stackView.addArrangedSubview(productRatingListView);
            let productRatingListViewHeight = stackViewHeight+translateAccordingToDevice(CGFloat(30.0))+detailedReviewLabelHeight+15;
            productRatingListView.heightAnchor.constraint(equalToConstant: productRatingListViewHeight).isActive = true;
            self.setLeadingAndTralingSpaceFormParentView(productRatingListView, parentView:stackView);
            
        }
        
        self.makeSomeSpaceInStackView(height: CGFloat(0.0));
        
    }
    
    @objc func writeReviewButtonPressed(_ sender:UIButton){
        if(defaults.bool(forKey: "isLogin")){
        let storyboard = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil);
        let viewController = storyboard.instantiateViewController(withIdentifier: "writeProductReviewViewController") as! WriteProductReviewViewController;
        viewController.product_id = self.product_id;
        self.navigationController?.pushViewController(viewController, animated: true);
        }else{
            self.view.makeToast("Please Login First.".localized, duration: 2, position: .center, title: nil, image: nil, style: nil, completion: nil)
        }
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        print("scrollViewDidEndDragging");
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        if(!loadMoreData){
            return;
        }
        if (maximumOffset - currentOffset) <= 40 {
            print("Cool");
            currentPageNum = currentPageNum+1;
            self.makeRequestToAPI("mobiconnectreview/index/getProductReview",dataToPost: ["prodID":product_id, "page":String(currentPageNum)]);
        }
        
    }
    
    
    func renderNoDataImage(imageName:String){
        let noDatalabelView = nodataImageView();
        noDatalabelView.translatesAutoresizingMaskIntoConstraints = false;
        //noDataImageView.image = UIImage(named: imageName);
        noDatalabelView.topLabel.text = "No Reviews Yet Be The First One To Review.".localized.uppercased()
        noDatalabelView.topLabel.fontColorTool()
        noDatalabelView.continueShopping.isHidden = true
        noDatalabelView.contentMode = UIView.ContentMode.scaleAspectFit;
        self.view.addSubview(noDatalabelView);
        self.view.addConstraint(NSLayoutConstraint(item: noDatalabelView, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 0));
        self.view.addConstraint(NSLayoutConstraint(item: noDatalabelView, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 0));
        self.view.addConstraint(NSLayoutConstraint(item: noDatalabelView, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.writeReviewButton, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: 5));
        self.view.addConstraint(NSLayoutConstraint(item: noDatalabelView, attribute: NSLayoutConstraint.Attribute.bottom, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: 0));
    }
    
    func resetViewAndVariables(){
        //floatingButton.removeFromSuperview();
        stackView.subviews.forEach({ $0.removeFromSuperview() });
        //addressDict = [String:[String: String]]();
    }
    
    override func basicFoundationToRenderView(bottomMargin:CGFloat){
        
        // adding scrollview
        scrollView = UIScrollView();
        scrollView.translatesAutoresizingMaskIntoConstraints = false;
        scrollView.showsHorizontalScrollIndicator = false;
        scrollView.showsVerticalScrollIndicator = false;
        view.addSubview(scrollView);
        self.view.addConstraint(NSLayoutConstraint(item: scrollView!, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 0));
        self.view.addConstraint(NSLayoutConstraint(item: scrollView!, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 0));
        self.view.addConstraint(NSLayoutConstraint(item: scrollView!, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.writeReviewButton, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: 5));
        self.view.addConstraint(NSLayoutConstraint(item: scrollView!, attribute: NSLayoutConstraint.Attribute.bottom, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.bottomLayoutGuide, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: -(bottomMargin)));
        
        // adding stackview
        stackView = UIStackView();
        stackView.translatesAutoresizingMaskIntoConstraints = false;
        stackView.axis  = NSLayoutConstraint.Axis.vertical;
        stackView.distribution  = UIStackView.Distribution.equalSpacing;
        stackView.alignment = UIStackView.Alignment.center;
        stackView.spacing   = 10.0;
        scrollView.addSubview(stackView);
        self.view.addConstraint(NSLayoutConstraint(item: stackView!, attribute: NSLayoutConstraint.Attribute.leading, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.leading, multiplier: 1, constant: 0));
        self.view.addConstraint(NSLayoutConstraint(item: stackView!, attribute: NSLayoutConstraint.Attribute.trailing, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.view, attribute: NSLayoutConstraint.Attribute.trailing, multiplier: 1, constant: 0));
        scrollView.addConstraint(NSLayoutConstraint(item: stackView!, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: scrollView, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1, constant: 0));
        scrollView.addConstraint(NSLayoutConstraint(item: stackView!, attribute: NSLayoutConstraint.Attribute.bottom, relatedBy: NSLayoutConstraint.Relation.equal, toItem: scrollView, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: 0));
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
