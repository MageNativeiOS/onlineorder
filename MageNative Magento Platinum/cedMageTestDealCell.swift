/**
 * CedCommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End User License Agreement (EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://cedcommerce.com/license-agreement.txt
 *
 * @category  Ced
 * @package   MageNative
 * @author    CedCommerce Core Team <connect@cedcommerce.com >
 * @copyright Copyright CEDCOMMERCE (http://cedcommerce.com/)
 * @license      http://cedcommerce.com/license-agreement.txt
 */
import UIKit

class cedMageTestDealCell: UICollectionViewCell,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var dealTitle: UILabel!
    @IBOutlet weak var dealsCollectioncell: UICollectionView!
    var parent = UIViewController()
    
    var deals = [[String:String]]()
    var dealtime = 100
    var data = true
    var timer = Timer()
    var startTime = TimeInterval()
    var timerStatus = String()
    var prevtext:String?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        //        dealsCollectioncell.delegate = self
        //        dealsCollectioncell.dataSource = self
        dealTitle.setThemeColor()
        if(timerStatus == "2"){
            
        }else{
            Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(HomepageDealsCell.update), userInfo: nil, repeats: true)
        }
        
    }
    
    func update() {
        if(startTime>0){
            var time = NSInteger(startTime)
            time = time / 1000
            let seconds = time % 60
            let minutes = (time / 60) % 60
            let hours = (time / 3600)
            
            dealTitle.text = prevtext! + "\n" + String(format: "%0.2d H:%0.2d M:%0.2d S",hours,minutes,seconds)
            startTime = startTime - 1000
        }
        
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return deals.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = dealsCollectioncell.dequeueReusableCell(withReuseIdentifier: "collectionCell", for: indexPath as IndexPath) as! cedMageCollectionCell
        cell.label.text = deals[indexPath.row]["offer_text"]?.uppercased()
        let img_url = deals[indexPath.row]["deal_image_name"]
        
        cedMageImageLoader.shared.loadImgFromUrl(urlString: img_url!,completionHandler: { (image: UIImage?, url: String) in
            DispatchQueue.main.async {
                cell.imageViewProduct.image = image
            }
        })
        cell.layer.cornerRadius = cell.frame.width/2
        cell.layer.masksToBounds = false
        cell.cardView()
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let bounds = UIScreen.main.bounds
        return CGSize(width:bounds.width/2-5 , height: bounds.height/2.5)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        _ = UIStoryboard(name: "Main", bundle: nil)
        let deal = deals[indexPath.row]
        if(deal["deal_type"] ==  "1"){
            let productview = UIStoryboard(name: "eCommerceFlowStoryBoard", bundle: nil).instantiateViewController(withIdentifier: "rootPageViewController") as! cedMageProductViewRoot
            productview.pageData = [["product_id":deal["relative_link"]]]
            let instance = cedMage.singletonInstance
            instance.storeParameterInteger(parameter: 0)
            self.parent.navigationController?.pushViewController(productview
                , animated: true)
        }else if(deal["deal_type"] ==  "2"){
            let viewcontoller = UIStoryboard(name: "categorylayouts", bundle: nil).instantiateViewController(withIdentifier: "categoryTheme2") as? cedMageCategoryTheme2
            viewcontoller?.categoryId = deal["relative_link"]!
            self.parent.navigationController?.pushViewController(viewcontoller!, animated: true)
        }else if(deal["deal_type"] ==  "3"){
            let view = UIStoryboard(name: "cedMageAccounts", bundle: nil)
            let viewControl = view.instantiateViewController(withIdentifier: "cmsWebView") as! cedMageCmsWebView
            viewControl.pageUrl = deal["relative_link"]!
            self.parent.navigationController?.pushViewController(viewControl, animated: true)
        }
        
    }
    
    
}



class CenterCellCollectionViewFlowLayout: UICollectionViewFlowLayout {
    
    override func targetContentOffset(forProposedContentOffset proposedContentOffset: CGPoint, withScrollingVelocity velocity: CGPoint) -> CGPoint {
        
        if let cv = self.collectionView {
            
            let cvBounds = cv.bounds
            let halfWidth = cvBounds.size.width * 0.5;
            let proposedContentOffsetCenterX = proposedContentOffset.x + halfWidth;
            
            if  self.layoutAttributesForElements(in: cvBounds) != nil {
                let attributesForVisibleCells = self.layoutAttributesForElements(in: cvBounds)! as [UICollectionViewLayoutAttributes]
                
                var candidateAttributes : UICollectionViewLayoutAttributes?
                for attributes in attributesForVisibleCells {
                    
                    if attributes.representedElementCategory != UICollectionView.ElementCategory.cell {
                        continue
                    }
                    
                    if let candAttrs = candidateAttributes {
                        
                        let a = attributes.center.x - proposedContentOffsetCenterX
                        let b = candAttrs.center.x - proposedContentOffsetCenterX
                        
                        if fabsf(Float(a)) < fabsf(Float(b)) {
                            candidateAttributes = attributes;
                        }
                        
                    }
                    else { // == First time in the loop == //
                        
                        candidateAttributes = attributes;
                        continue;
                    }
                    
                    
                }
                
                return CGPoint(x: round(candidateAttributes!.center.x - halfWidth), y: proposedContentOffset.y)
                
            }
            else {
                print("Error")
            }
            
        }
        
        // Fallback
        return super.targetContentOffset(forProposedContentOffset: proposedContentOffset)
    }
    
}
