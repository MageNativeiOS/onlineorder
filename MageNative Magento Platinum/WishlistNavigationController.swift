//
//  WishlistNavigationController.swift
//  MageNative Magento Platinum
//
//  Created by Hamza Usmani on 27/01/21.
//  Copyright © 2021 CEDCOSS Technologies Private Limited. All rights reserved.
//

import UIKit

class WishlistNavigationController: homedefaultNavigation {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if UserDefaults.standard.bool(forKey: "isLogin") {
            let wishlistController = UIStoryboard(name: "cedMageAccounts", bundle: nil).instantiateViewController(withIdentifier: "mywishlist") as! cedMageWishList
            setViewControllers([wishlistController], animated: true)
        } else {
            setViewControllers([LoginController()], animated: true)
        }
    }
}
